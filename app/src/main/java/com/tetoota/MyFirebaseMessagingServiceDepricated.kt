package com.tetoota

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import android.text.TextUtils
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import org.json.JSONException
import org.json.JSONObject

/**
 *
 */
class MyFirebaseMessagingServiceDepricated : FirebaseMessagingService(), ProfileDetailContract.View {
    override fun onProfileSendEmailApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        val profileData: ProfileDataResponse = message?.get(0)!!
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MyFirebaseMessagingServiceDepricated)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        personData.tetoota_points = profileData.tetoota_points
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@MyFirebaseMessagingServiceDepricated)
    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
    }

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@MyFirebaseMessagingServiceDepricated)
    }
    private val TAG = MyFirebaseMessagingServiceDepricated::class.java.simpleName
    private var notificationUtils: NotificationUtils? = null
    private var tetootaApplication: TetootaApplication? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Log.e(TAG, "From: " + remoteMessage!!.from)
//        if (remoteMessage == null)
//            return

        // Check if message contains a pushNotificationDataResponse payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.e(TAG, "PushNotificationDataResponse Payload: " + remoteMessage.data.toString())
            try {
//                val json = JSONObject(remoteMessage.data)
                var title: String = remoteMessage.data.get("title").toString()
                var body: String = remoteMessage.data.get("body").toString()
                sendNotification(title, body)
                Log.e(TAG, title + " " + body)
//                showNotification(title,body)
//                val resultIntent = Intent(applicationContext, MainActivity::class.java)
////            val resultIntent = Intent(applicationContext, InboxFragment::class.java)
//                resultIntent.putExtra("notification", true)//                resultIntent.putExtra("message", body)
//
//                showNotificationMessage(applicationContext, title, body, resultIntent)
//                handleDataMessage(json)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }
        }
        // Check if message contains a notification payload.
        else if (remoteMessage.notification != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.notification!!.body!!)

        }
    }

    private fun showNotification(title: String?, body: String?) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun handleNotification(message: String, messageAlert: String) {
        if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
            // app is in foreground, broadcast the push message
            val pushNotification = Intent(Constant.PUSH_NOTIFICATION)
            pushNotification.putExtra("message", message)
            pushNotification.putExtra("messageAlert", messageAlert)
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)
            tetootaApplication = applicationContext as TetootaApplication
            callUserDetailAPI()
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private fun handleDataMessage(data: JSONObject) {
        Log.e(TAG, "push json: " + data.toString())
        try {
            //   val pushNotificationDataResponse = json.getJSONObject("pushNotificationDataResponse")
            val title = data.optString("sender_fullName")
            var imageUrl: String? = ""
            var message: String? = ""
            var category: String? = data.optString("category")
            if (null != category && category == "Proposal Action") {
                message = data.optString("message")
            } else if (null != category && category == "Message Alert") {
                message = data.optString("message")
            } else {
                Log.e(TAG, "imageUrl: " + imageUrl)
                imageUrl = data.optString("post_image")
                message = data.optString("proposal_title")
            }
            //  Log.e(TAG, "title: " + title)
            // Log.e(TAG, "message: " + message)
            val resultIntent = Intent(applicationContext, MainActivity::class.java)
//            val resultIntent = Intent(applicationContext, InboxFragment::class.java)
            resultIntent.putExtra("notification", true)
            resultIntent.putExtra("message", message)
            // check for image attachment
            if (TetootaApplication.isActivityVisible()) {
                // TODO VISIBLe
                handleNotification(message as String, data.optString("category"))
            } else {
                // NOT VISIBLE
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(applicationContext, title, message, resultIntent)
                } else {
                    // image is present, show notification with image
                    if (imageUrl != null) {
                        showNotificationMessageWithBigImage(applicationContext, title, message, resultIntent, imageUrl)
                    }
                }
                var totalCount: Int = Utils.Utils.loadInt(Constant.USER_PROPOSAL_COUNT) + 1
                Utils.Utils.saveInt(Constant.USER_PROPOSAL_COUNT.toString(), totalCount)
                handleNotification(message as String, data.optString("category"))
            }

            // }
        } catch (e: JSONException) {
            Log.e(TAG, "Json Exception: " + e.message)
        } catch (e: Exception) {
            Log.e(TAG, "Exception: " + e.message)
        }

    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(context: Context, title: String, message: String, intent: Intent) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        (notificationUtils as NotificationUtils).showNotificationMessage(title, message, intent)
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(context: Context, title: String, message: String, intent: Intent, imageUrl: String) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        (notificationUtils as NotificationUtils).showNotificationMessage(title, message, intent, imageUrl)
    }

    private fun callUserDetailAPI() {
        if (Utils.haveNetworkConnection(this@MyFirebaseMessagingServiceDepricated)) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MyFirebaseMessagingServiceDepricated)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileDataForService(this@MyFirebaseMessagingServiceDepricated, personData)
        }
    }


    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(title: String, messageBody: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "tetoota",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }
}