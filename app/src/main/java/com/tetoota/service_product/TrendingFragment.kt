package com.tetoota.service_product

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityOptions
import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.annotation.VisibleForTesting
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.MenuItemCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupMenu
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.appintro.DashboardSliderAdapter
import com.tetoota.customviews.PaginationScrollListener
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.customviews.ReportConversationDialog
import com.tetoota.customviews.StartConversationDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.filter.FilterActivity
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.dashboard.*
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.homescreen.DashboardPagerStateAdapter
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.floating_add_filter_layout.*
import kotlinx.android.synthetic.main.fragment_wishlist.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TrendingFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TrendingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TrendingFragment : BaseFragment(), HomeContract.View, ServiceContract.View, SearchContract.View, WishRecyclerAdapter.IAdapterClick,
        WishRecyclerAdapter.PaginationAdapterCallback, StartConversationDialog.IDialogListener, ReportConversationDialog.IDialogListener,
        DashboardPagerStateAdapter.OnFilterAppliedListener, ProfileCompletionDialog.IDialogListener, ViewPager.OnPageChangeListener {
    private var tetootaApplication: TetootaApplication? = null
    var link: String = "http://tetoota.com/service/?"
    lateinit var shortLink: Uri

    override fun onPageScrollStateChanged(p0: Int) {
    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {


    }

    override fun onPageSelected(p0: Int) {
        for (i in 0..dotsCount - 1) {
            try {
                dots[i]?.setImageDrawable(context?.let { ContextCompat.getDrawable(it, R.drawable.select_walkthorugh_item) })
                dots[p0]?.setImageDrawable(context?.let { ContextCompat.getDrawable(it, R.drawable.select_walkthorugh_item) })
                if (p0 + 1 == dotsCount) {
                } else {
                    // btn_next.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInvalidTextAPiFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDashboardApiSuccessResult(mSliderList: ArrayList<Any>, message: String) {
        if (message == "sliderList") {
            dotsCount = 0
            if (progress_view != null)
                progress_view.visibility = View.GONE
            service_rl_viewpager.visibility = View.GONE
            val mDashBoardSliderDataResponse: ArrayList<Any> = mSliderList
            dotsCount = mSliderList.size
            setUiPageViewController()
            mAdapter.setElements(mDashBoardSliderDataResponse as ArrayList<DashboardSliderDataResponse>)
            mAdapter.notifyDataSetChanged()
            service_pager_introduction.interval = 2000
            service_pager_introduction.startAutoScroll()
            service_pager_introduction.currentItem = Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % mSliderList.size

        } else if (message == "homeScreenList") {
            println("Home Screen List PushNotificationDataResponse")
            val mHomeScreenList = mSliderList as ArrayList<HomeDataResponse>
        }
    }


    override fun onDashboardApiFailureResult(message: String) {
        if (activity != null) {
            progress_view.visibility = View.GONE
            activity?.toast(message)
        }
    }

    private fun setUiPageViewController() {

        if (isTest == true) {

        } else {
            //Log.e("elseee ","" + isTest)
            dots = arrayOfNulls(dotsCount)
            for (i in 0..dotsCount - 1) {
                dots[i] = ImageView(activity)
                dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
                val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(4, 0, 4, 0)
                service_viewPagerCountDots.addView(dots[i], params)
            } // Commit Testing
            dots[0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
        }

    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = ProfileDetailActivity.newMainIntent(this.activity!!)
                ActivityStack.getInstance(this.activity!!)
                startActivity(intent)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    private var dotsCount: Int = 0
    var exchangePostType: String? = ""
    private var intent: Intent? = null
    private val FILTER_REQUEST: Int = 12
    private val EDITSERVICEPRODUCT_REQUEST: Int = 13
    private val ADDPOST_REQUEST: Int = 14
    public var isRefresh: Boolean? = false
    public var isSearch: Boolean? = false
    public var mQuery: String? = null
    override fun filterData(mTopRated: String, mCity: String) {
        print("mTopRated " + mTopRated + " mCity " + mCity)
    }

    val TAG: String = "ServiceFragment"
    private var isLoad = false
    public var isListLastPage = false
    private var isFirstLoading = false
    private var TOTAL_PAGES = 1
    private val RECORD_PER_PAGE = 10
    public var currentPage = 1
    var menu: Menu? = null
    var linearLayoutManager: LinearLayoutManager? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    var mLastLocation: Location? = null
    var isTest: Boolean? = false

    var dots = arrayOfNulls<ImageView>(dotsCount)

    val mAdapter: DashboardSliderAdapter by lazy {
        DashboardSliderAdapter(this!!.activity!!)
    }

    private lateinit var mWishRecyclerAdapter: WishRecyclerAdapter

    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@TrendingFragment)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@TrendingFragment)
    }
    private val mSearchPresenter: SearchPresenter by lazy {
        SearchPresenter(this@TrendingFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            var test = arguments!!.getString(ARG_PARAM1)
            Toast.makeText(activity, "" + arguments!!.getString(ARG_PARAM1), Toast.LENGTH_SHORT);

        }
        tetootaApplication = activity!!.applicationContext as TetootaApplication

        TetootaApplication.setTextValuesHashMap(Utils.getLangByCode(activity,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", activity)))


        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        //  getLastLocation(false)
        Log.e(TAG, "onCreateView")

        return inflater.inflate(R.layout.fragment_wishlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMultiLanguageText()
        service_rl_viewpager.visibility = View.VISIBLE
        setupRecyclerView()

        Log.e(TAG, "onViewCreated")

        /* var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
         toolTitle.visibility = View.VISIBLE
         toolTitle.text = "Tetoota"*/


        if (isAddFilterShow) {
            Log.e(TAG, "VISIBLE")

            ll_addHome.visibility = View.VISIBLE
        } else {
            Log.e(TAG, "GONE")

            ll_addHome.visibility = View.GONE
        }
        ll_addHome.visibility = View.GONE

        getLastLocation(false)
        view_list.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isListLastPage
            }

            override fun loadMoreItems() {
                isLoad = true
                currentPage += 1
                loadItemsLayout_recyclerView.visibility = View.VISIBLE
//                if (value == "haveUserId") {
//                    fetchWishListData(currentPage!!, true)
//                } else {
//                    fetchAllServiceData(currentPage!!, true)
//                }
                getLastLocation(true)

            }

            override fun getTotalPageCount(): Int {
                return TOTAL_PAGES
            }

            override fun isLoading(): Boolean {
                return isLoad
            }
        })
        ll_addpost.onClick {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)

            val intent = AddPostRequestActivity.newMainIntent(this.activity!!)
            intent!!.putExtra("Tab", "serviceTab")
            ActivityStack.getInstance(this.activity!!)
//                startActivity(intent)
            activity?.startActivityForResult(intent, ADDPOST_REQUEST)
            /* if (personData.complete_percentage!!.toInt() < 50) {
                 ProfileCompletionDialog(activity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                         this@ServiceFragment, getString(R.string.err_msg_mobile_number_limit)).show()
             } else {
                 val intent = AddPostRequestActivity.newMainIntent(this.activity!!)
                 intent!!.putExtra("Tab", "serviceTab")
                 ActivityStack.getInstance(this.activity!!)
 //                startActivity(intent)
                 activity?.startActivityForResult(intent, ADDPOST_REQUEST)
             }*/
        }
        ll_filter.onClick {
            val intent = FilterActivity.newMainIntent(this.activity!!)
            ActivityStack.getInstance(this.activity!!)
//            startActivity(intent)
            startActivityForResult(intent, FILTER_REQUEST)
        }
    }

    private fun setMultiLanguageText() {
        tv_add.text = Utils.getText(context, StringConstant.str_addpost)
        filter.text = Utils.getText(context, StringConstant.str_filter)
    }

    fun getAllDashboardList(): Unit {
        if (ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this!!.activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        if (Utils.haveNetworkConnection(this.activity!!)) {
            //   progress_view.visibility = View.VISIBLE
            mHomePresenter.getDashboardSliderData(this.activity!!, tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        service_pager_introduction.adapter = mAdapter
        service_pager_introduction.currentItem = 0
        service_pager_introduction.addOnPageChangeListener(this@TrendingFragment)
        getAllDashboardList();
    }

    private fun setupRecyclerView() {
        view_list.setHasFixedSize(true)
        view_list.itemAnimator = DefaultItemAnimator()
        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view_list.layoutManager = linearLayoutManager
        mWishRecyclerAdapter = WishRecyclerAdapter(activity!!, iAdapterClickListener = this,
                iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        // view_list.setDemoChildCount(4)
        view_list.adapter = mWishRecyclerAdapter
        view_list.setNestedScrollingEnabled(false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    private fun fetchWishListData(pagination: Int, isLoadMore: Boolean) {
        Log.e(TAG, "fetchWishListData")
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
                //  showProgressDialog("Please Wait...")
            }
            mServicePresenter.getServicesData(this.activity!!, "Services", pagination, Utils.loadPrefrence(Constant.USER_ID, "", context).toString())
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    private fun fetchAllServiceData(pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
                //  showProgressDialog("Please Wait...")
            }
            mServicePresenter.getAllServicesData(this.activity!!, "Trending", pagination, tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    private fun fetchSearchListData(mSearchString: String, pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
            }
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mSearchPresenter.getSubCategoriesData(mSearchString, personData.user_id!!, pagination, this.activity!!, "Trending")
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setIcon(R.drawable.abc_ic_search_api_material)
        val searchView = MenuItemCompat.getActionView(menu?.findItem(R.id.action_search)) as SearchView
        val searchManager = context!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(omponentName()))
        menu?.clear();

        searchView.setOnSearchClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                Log.i("SearchView", "Search CLick-ServiceFragment")
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {


            override fun onQueryTextChange(newText: String): Boolean {
//                Log.e("SearchView", "onQueryTextChange-ServiceFragment");
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.e("SearchView", "onQueryTextSubmit-ServiceFragment");
                isSearch = true
                isRefresh = false
                clearList()
                currentPage = 1
                isListLastPage = false
                mQuery = query
                getLastLocation(false)
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setIcon(R.drawable.abc_ic_search_api_material)
        val searchView = MenuItemCompat.getActionView(menu?.findItem(R.id.action_search)) as SearchView
        val searchManager = context!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnSearchClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                /*  var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
                  toolTitle.visibility = View.GONE
  */
                Log.i("SearchView", "Search CLick-ServiceFragment")
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                //Log.e("SearchView", "onQueryTextChange-ServiceFragment");
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.e("SearchView", "onQueryTextSubmit-ServiceFragment");
                isSearch = true
                isRefresh = false
                clearList()
                currentPage = 1
                isListLastPage = false
                mQuery = query
                getLastLocation(false)
                return false
            }
        })
        super.onPrepareOptionsMenu(menu)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mServicePresenter.cancelRequest(this.activity!!, "services")
    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .2f).toInt()
        return getTopMarginH
    }

    override fun onDetach() {
        super.onDetach()
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = ""
        private val ARG_PARAM2 = ""
        private val ARG_PARAM3 = "param3"
        private var value = "value"
        private var isAddFilterShow = true
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment ServiceFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String, param3: Boolean): TrendingFragment {
            val fragment = TrendingFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            args.putBoolean(ARG_PARAM3, param3)
            value = param1
            isAddFilterShow = param3
            fragment.arguments = args
            return fragment
        }
    }


    private fun mod(x: Int, y: Int): Int {
        if (x % y == 0) {
            return x / y
        } else {
            return (x / y) + 1
        }
    }

    private fun openDailog(mResponseMsg: String) {
        val alertDialog = AlertDialog.Builder(context).create() //Read Update
//      alertDialog.setTitle(header)
        alertDialog.setMessage(mResponseMsg)
        alertDialog.setButton("Ok", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                alertDialog.dismiss()
                val appPackageName = context!!.packageName // getPackageName() from Context or Activity object
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)))
                } catch (anfe: android.content.ActivityNotFoundException) {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)))
                }

            }
        })
        alertDialog.show()  //<-- See This!
    }

    override fun onWishListApiSuccessResult(mWishList: List<Any?>?, message: String) {
        if (view != null) {
//            clearList()
            if (mWishRecyclerAdapter.itemCount != mWishRecyclerAdapter.getTotalRecord()) {
                progress_view.visibility = View.GONE
                rl_nrf.visibility = View.GONE
                hideErrorView()
                loadItemsLayout_recyclerView.visibility = View.GONE
                if (activity != null && TrendingFragment() != null) {
                    if (mWishList != null) {
                        if (mWishList.size > 0) {

                            Log.e("TAG", "SIZE() = " + mWishList.size)
                            var sdfsd: ServicesDataResponse = mWishList.get(0) as ServicesDataResponse
                            if (!sdfsd.version.equals(Utils.loadPrefrence(Constant.VERSION, "1", context))) {
//                                openDailog("New version of this is available on play store.")
                            }
                        } else {
//                            rl_nrf.visibility = View.VISIBLE
                        }
                    }
                    Log.e(TAG, "onWishListApiSuccessResult " + mWishList.toString())
                    mWishRecyclerAdapter.addAll(mWishList as List<ServicesDataResponse?>)
                }
                isLoad = false
            } else {
                loadItemsLayout_recyclerView.visibility = View.GONE
                isListLastPage = true
                progress_view.visibility = View.GONE
                rl_nrf.visibility = View.VISIBLE
                hideErrorView()
            }
        }
    }

    override fun onWishListApiFailure(message: String, isServerError: Boolean) {
        super.onWishListApiFailure(message, isServerError)
        Log.e("asdasd", message)
//        clearList()
        loadItemsLayout_recyclerView.visibility = View.GONE
        isListLastPage = true
        progress_view.visibility = View.GONE
        hideErrorView()
    }

    override fun onSubCategoriesSuccessResult(mCategoriesList: List<ServicesDataResponse>, message: String) {
        if (view != null) {
//            clearList()
            if (mWishRecyclerAdapter.itemCount != mWishRecyclerAdapter.getTotalRecord()) {
                progress_view.visibility = View.GONE
                rl_nrf.visibility = View.GONE
                hideErrorView()
                loadItemsLayout_recyclerView.visibility = View.GONE
                if (activity != null && TrendingFragment() != null) {
                    rl_nrf.visibility = View.GONE
                    mWishRecyclerAdapter.addAll(mCategoriesList as List<ServicesDataResponse?>)
                }
                isLoad = false
            } else {
                loadItemsLayout_recyclerView.visibility = View.GONE
                isListLastPage = true
                rl_nrf.visibility = View.VISIBLE
                progress_view.visibility = View.GONE
                hideErrorView()
            }
        }
    }

    override fun onSubCatergoriesFailureResult(message: String) {
        loadItemsLayout_recyclerView.visibility = View.GONE
        isListLastPage = true
        rl_nrf.visibility = View.VISIBLE
        progress_view.visibility = View.GONE
        hideErrorView()
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        if (view != null) {
//            clearList()
            progress_view.visibility = View.GONE
            loadItemsLayout_recyclerView.visibility = View.GONE
            isLoad = false
            if (activity != null && TrendingFragment() != null) {
                if (!isServerError) {
                    isListLastPage = true
                }
                if (TOTAL_PAGES != 1) {
                    showErrorView(message)
                }
            }
        }
    }

    override fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        val mProductData = cellRow as ServicesDataResponse
        if (mString == "favorite") {
            showPopupMenu(mView, mProductData, mAttributeValue, p1)
        } else if (mString == "cellClick") {
            activity!!.toast("Under Development")
        } else if (mString == "shareClick") {
            showPopupMenu(mView, mProductData, mAttributeValue, p1)
        } else if (mString.equals("Cell Item click")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = Intent(context, ProductDetailActivity::class.java)
                intent.putExtra("mProductData", mProductData)
                intent.putExtra("productType", "services")
                intent.putExtra("position", p1)
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    val transitionActivityOptions: ActivityOptions =
                            ActivityOptions.makeSceneTransitionAnimation(activity,
                                    mView, "iv_image")
                    // startActivity(intent, transitionActivityOptions.toBundle())
                    startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST, transitionActivityOptions.toBundle())
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                } else {
//                   startActivity(intent)
                    startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST)
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                }
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        }
    }

    override fun retryPageLoad() {
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (view != null) {
            if (cellRow.is_favourite == 0) {
                cellRow.is_favourite = 1
                activity?.toast("Marked Favorite")
            } else {
                cellRow.is_favourite = 0
                activity?.toast("Marked Unfavorite")
            }
            mWishRecyclerAdapter.notifyItemChanged(p1)
        }
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation(isLoadMore: Boolean) {
        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient?.lastLocation?.addOnCompleteListener(this.activity!!) { task ->
                if (task.isSuccessful && task.result != null) {
                    mLastLocation = task.result
                    Log.e("mLastLocationnnnn", "" + mLastLocation)
                    Utils.savePreferences(Constant.USER_LOC_LAT, mLastLocation?.latitude.toString(), this.activity!!)
                    Utils.savePreferences(Constant.USER_LOC_LONG, mLastLocation?.longitude.toString(), this.activity!!)
                    if (isSearch!!) {
                        fetchSearchListData(mQuery!!, currentPage, isLoadMore)
                    } else {
                        if (value == "haveUserId") {
                            fetchWishListData(currentPage, isLoadMore)
                        } else {
                            fetchAllServiceData(currentPage, isLoadMore)
                        }
                    }
                } else {
                    //Log.w(TAG, "getLastLocation:exception", task.exception)
                }
            }
        } else {
            if (isSearch!!) {
                fetchSearchListData(mQuery!!, currentPage, isLoadMore)
            } else {
                if (value == "haveUserId") {
                    fetchWishListData(currentPage, isLoadMore)
                } else {
                    fetchAllServiceData(currentPage, isLoadMore)
                }
            }
        }
    }


    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            view_list.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    private fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            view_list.visibility = View.GONE
            err_title.visibility = View.GONE

        }
    }

    /**
     * Method To Create Popup Menu
     * for share, Favorite, Report
     */
    private fun showPopupMenu(view: View, mProductData: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val popup = PopupMenu(activity, view)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        if (mAttributeValue == "0") {
            favItem?.title = Utils.getText(context, StringConstant.home_unfavourite)
        } else {
            favItem?.title = Utils.getText(context, StringConstant.favorite)
        }
        menu?.findItem(R.id.menu_share)!!.title = Utils.getText(context, StringConstant.share_text)
        menu?.findItem(R.id.menu_report_this)!!.title = Utils.getText(context, StringConstant.report_this)

        var hideReport = activity?.intent?.getStringExtra("HideReport");

        if (hideReport.equals("HideReport")) {

            menu?.findItem(R.id.menu_report_this)!!.setVisible(false);
        }
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {

//                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
//                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//                    val postString: String = personData.first_name + " is offering " + '"' + mProductData.title!! + '"' + " on \n http://tetoota.com"
//                    var url: String = "";
//                    if (mProductData.image != null && mProductData.image_thumb != null) {
//                        url = Utils.getUrl(context, mProductData.image, mProductData.image_thumb, false)
//                    } else {
//                        url = Utils.getUrl(context, mProductData.image!!)
//                    }
//                    //  Log.e("postString - ", "" + postString + "url - " + url)
//
//                    //shareProduct(postString, url)
//                    shareService(postString, url)
                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    val postString: String = personData.first_name + " is offering " + '"' + mProductData?.title!! + '"' + " on http://tetoota.com"
                    var url: String = "";
                    if (mProductData!!.image != null && mProductData!!.image_thumb != null) {
                        url = Utils.getUrl(activity, mProductData!!.image!!, mProductData!!.image_thumb!!, false)
                    } else {
                        url = Utils.getUrl(activity, mProductData!!.image!!)
                    }
//                    shareService(postString, url)
                    var gson = Gson()
                    var jsonString = gson.toJson(mProductData)
                    Log.e("jsonString", jsonString + "");
                    val newDeepLink = buildDeepLink(link + jsonString, postString, Constant.ANDROID_MIN_VERSION)

                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this.activity!!)) {
                        mServicePresenter.markFavorite(this.activity!!, mProductData, mAttributeValue, p1)
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    ReportConversationDialog(context, Constant.DIALOG_LOGIN_FAILURE_ALERT, mProductData.id.toString(),
                            this@TrendingFragment, getString(R.string.str_report_issue)).show()
                }
            }
            true
        }
        popup.show()
    }

    /**
     * Method to Share Product
     * TODO Deep linking
     */


    /**
     * Build a Firebase Dynamic Link.
     * https://firebase.google.com/docs/dynamic-links/android/create#create-a-dynamic-link-from-parameters
     *
     * @param deepLink the deep link your app will open. This link must be a valid URL and use the
     * HTTP or HTTPS scheme.
     * @param minVersion the `versionCode` of the minimum version of your app that can open
     * the deep link. If the installed app is an older version, the user is taken
     * to the Play store to upgrade the app. Pass 0 if you do not
     * require a minimum version.
     * @return a [Uri] representing a properly formed deep link.
     */
    @VisibleForTesting
    fun buildDeepLink(jsonData: String, postString: String, minVersion: Int): Uri {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)

        // Set dynamic link parameters:
        //  * URI prefix (required)
        //  * Android Parameters (required)
        //  * Deep link
        // [START build_dynamic_link]
        val builder = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix(uriPrefix)
                .setAndroidParameters(DynamicLink.AndroidParameters.Builder()
                        .setMinimumVersion(minVersion)
                        .build())
                .setIosParameters(
                        DynamicLink.IosParameters.Builder("com.tetoota")
                                .setAppStoreId("1402450812")
                                .setMinimumVersion("1.2")
                                .build())
                .setLink(Uri.parse(jsonData))

        // Build the dynamic link
        val link = builder
//                .buildDynamicLink()
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                .addOnSuccessListener { result ->
                    // Short link created
                    shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    Log.e("flowchartLink", "" + shortLink)
                    shareService(activity, postString, shortLink.toString())
                }.addOnFailureListener {
                    Log.e("addOnFailureListener", "FAIL")
                }
        shareService(activity, postString, link.toString())

        // [END build_dynamic_link]
        // Return the dynamic link as a URI
        return Uri.parse("")
    }

    private fun shareDeepLink(deepLink: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link")
        intent.putExtra(Intent.EXTRA_TEXT, deepLink)

        startActivity(intent)
    }


    fun shareService(context: Context?, postString: String, deepLink: String) {
        mProgressDialog?.dismiss()
        try {
//            var ref = tv_referralCode.text.toString().trim()
//            var newUrl = inviteFriend_text.replace("@", ref)
//            val text = newUrl + "\n" + deepLink
//            Log.e(InviteFriendsFragment.TAG, ref + "\n\n" + "newUrl" + inviteFriend_text.replace("@", ref) + "\n\n" + text)
//            val urlToShare = StringConstant.app_link
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(context.getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            //  Log.e("imageUri - ","" + imageUri)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, postString + "\n" + deepLink)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            //  shareIntent.type = "image/*"
            // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
//            context?.startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    /* fun shareProduct(mTitle: String, mImageUrl: String) {
         val text = mTitle
         Log.e("text - ", "" + text)
         val pictureUri = Uri.parse(mImageUrl)
         val shareIntent = Intent()
         shareIntent.action = Intent.ACTION_SEND
         shareIntent.putExtra(Intent.EXTRA_TEXT, text)
         shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
         shareIntent.type = "image/*"
         shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
         startActivity(Intent.createChooser(shareIntent, "Share images..."))
     }*/*/


    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
        Utils.hideSoftKeyboard(this.activity!!)
        if (message == "report") {
            showProgressDialog(Utils.getText(context, StringConstant.please_wait))
            mHomePresenter.report(this.activity!!, mServiceId, mMesgDesc)
        }
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            activity!!.toast(message)
        } else {
            activity!!.toast(message)
        }
    }

    override fun onResume() {
        super.onResume()
        /*if (this!!.isRefresh!!) {
            clearList()
            currentPage = 1
            isListLastPage = false
            getLastLocation(false)
        }*/

    }

    fun clearList() {
        mWishRecyclerAdapter.removeAll()
        mWishRecyclerAdapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FILTER_REQUEST) {
                isRefresh = true
                refreshData()
            } else if (requestCode == EDITSERVICEPRODUCT_REQUEST) {
                refreshData()
            }
        } else {
            resetFilterData()
        }
    }

    private fun resetFilterData() {
        Utils.savePreferences(Constant.CATEGORY_ID, "", this.context!!)
        Utils.savePreferences(Constant.SUBCATEGORIES_ID, "", this.context!!)
        Utils.savePreferences(Constant.TOPRATED, "no", this.context!!)
        Utils.savePreferences(Constant.CITY, "", this.context!!)
    }

    public fun refreshData() {
        clearList()
        currentPage = 1
        isListLastPage = false
        getLastLocation(false)
    }

    fun onBackPressed() {
        Log.e("onBackPressed ", "service = ")
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}// Required empty public constructor
