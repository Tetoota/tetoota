package com.tetoota.service_product

import android.app.Activity
import com.tetoota.fragment.dashboard.ServicesDataResponse

/**
 * Created by charchit.kasliwal on 24-07-2017.
 */
class ServiceContract {
    interface View {
        fun onWishListApiSuccessResult(mCategoriesList: List<Any?>?, message: String)
        fun onWishListApiFailure(message: String, isServerError: Boolean){}
        fun onApiFailureResult(message: String, isServerError: Boolean)
        fun favoriteApiResult(message: String, cellRow : ServicesDataResponse, p1 : Int)
    }

    internal interface Presenter {
        fun getWishListData(mActivity : Activity)
        fun getServicesData(mActivity : Activity,postType : String,pagination : Int?,userId:String)
        fun getUserServiceList(mActivity : Activity, mUserId:String, postType : String,pagination : Int?)
        fun getAllServicesData(mActivity : Activity,postType : String,pagination : Int?,myLatitude: Double, myLongitude: Double)
        fun cancelRequest(mActivity: Activity,wishListType : String)
        fun markFavorite(mActivity: Activity,mProductId : ServicesDataResponse,mAttributeValue : String,p1 : Int)
    }

    interface serviceApiListener {
        fun onWishListApiSuccess(mDataList: List<Any?>?, message: String)
        fun onApiFailure(message: String, isServerError: Boolean)
        fun onWishListApiFailure(message: String, isServerError: Boolean){}
        fun onFavoriteApiSuccess(message: String,cellRow : ServicesDataResponse,p1 : Int)
    }
}