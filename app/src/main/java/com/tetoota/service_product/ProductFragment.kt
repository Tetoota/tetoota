package com.tetoota.service_product

import android.Manifest
import android.app.Activity
import android.app.ActivityOptions
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import android.widget.PopupMenu
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.maps.android.SphericalUtil
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.customviews.PaginationScrollListener
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.customviews.ReportConversationDialog
import com.tetoota.customviews.StartConversationDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.filter.FilterActivity
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.dashboard.HomeContract
import com.tetoota.fragment.dashboard.HomePresenter
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.dashboard.WishRecyclerAdapter
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.floating_add_filter_layout.*
import kotlinx.android.synthetic.main.fragment_wishlist.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ProductFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ProductFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProductFragment : BaseFragment(), ServiceContract.View, SearchContract.View, WishRecyclerAdapter.IAdapterClick,
        WishRecyclerAdapter.PaginationAdapterCallback, HomeContract.View, StartConversationDialog.IDialogListener, ReportConversationDialog.IDialogListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ProfileCompletionDialog.IDialogListener {
    private var tetootaApplication: TetootaApplication? = null

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInvalidTextAPiFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = ProfileDetailActivity.newMainIntent(this.activity!!)
                ActivityStack.getInstance(this.activity!!)
                startActivity(intent)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    private val mSearchPresenter: SearchPresenter by lazy {
        SearchPresenter(this@ProductFragment)
    }
    private var intent: Intent? = null
    var exchangePostType: String? = ""

    private val FILTER_REQUEST: Int = 12
    private val EDITSERVICEPRODUCT: Int = 13
    private val ADDPOST_REQUEST: Int = 14
    public var isRefresh: Boolean? = false
    public var isSearch: Boolean? = false
    public var mQuery: String? = null
    var menu: Menu? = null
    private var isLoad = false
    var isListLastPage = false
    private var isFirstLoading = false
    private var TOTAL_PAGES = 1
    private val RECORD_PER_PAGE = 10
    var currentPage = 1
    var linearLayoutManager: LinearLayoutManager? = null

    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var currentLatitude: Double = 0.toDouble()
    private var currentLongitude: Double = 0.toDouble()
    internal var oldLattitude = 0.0
    internal var oldLongitude = 0.0

    private lateinit var mWishRecyclerAdapter: WishRecyclerAdapter

    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@ProductFragment)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@ProductFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        tetootaApplication = activity!!.applicationContext as TetootaApplication
        TetootaApplication.setTextValuesHashMap(Utils.getLangByCode(activity,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", activity)))

        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient()
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wishlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setMultiLanguageText()

        /*      var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
              toolTitle.visibility = View.VISIBLE
              toolTitle.text = "Tetoota"
      */
        if (isAddFilterShow) {
            ll_filterlayout.visibility = View.VISIBLE
        } else {
            ll_filterlayout.visibility = View.GONE
        }
        if (mGoogleApiClient != null) {
            locationEnable(this.activity!!, mLocationRequest!!, mGoogleApiClient!!)
        }
        callApi(false)
        view_list.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isListLastPage
            }

            override fun loadMoreItems() {
                isLoad = true
                currentPage += 1
                loadItemsLayout_recyclerView.visibility = View.VISIBLE
                callApi(true)
            }

            override fun getTotalPageCount(): Int {
                return TOTAL_PAGES
            }

            override fun isLoading(): Boolean {
                return isLoad
            }
        })
        ll_addpost.onClick {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            val intent = AddPostRequestActivity.newMainIntent(this.activity!!)
            intent!!.putExtra("Tab", "serviceTab")
            ActivityStack.getInstance(this.activity!!)
//                startActivity(intent)
            startActivityForResult(intent, ADDPOST_REQUEST)

            /*  if (personData.complete_percentage!!.toInt() < 50) {
                  ProfileCompletionDialog(activity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                          this@ProductFragment, getString(R.string.err_msg_mobile_number_limit)).show()
              } else {
                  val intent = AddPostRequestActivity.newMainIntent(this.activity!!)
                  intent!!.putExtra("Tab", "serviceTab")
                  ActivityStack.getInstance(this.activity!!)
  //                startActivity(intent)
                  startActivityForResult(intent, ADDPOST_REQUEST)
              }*/
        }
        ll_filter.onClick {
            val intent = FilterActivity.newMainIntent(this.activity!!)
            ActivityStack.getInstance(this.activity!!)
//            startActivity(intent)
            startActivityForResult(intent, FILTER_REQUEST)
        }
    }

    private fun setupRecyclerView() {
        view_list.setHasFixedSize(true)
        view_list.itemAnimator = DefaultItemAnimator()
        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view_list.layoutManager = linearLayoutManager
        mWishRecyclerAdapter = WishRecyclerAdapter(activity!!, iAdapterClickListener = this,
                iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        view_list.adapter = mWishRecyclerAdapter
    }

    public fun callApi(isLoadMore: Boolean) {
        if (isSearch!!) {
            fetchSearchListData(mQuery!!, currentPage, isLoadMore)
        } else {
            if (value == "haveUserId") {
                fetchProductData(currentPage, isLoadMore)
            } else {
                fetchAllProductData(currentPage, isLoadMore)
            }
        }
    }

    private fun fetchProductData(pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
            }
            mServicePresenter.getServicesData(this.activity!!, "Products", pagination,Utils.loadPrefrence(Constant.USER_ID, "", context).toString())
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    private fun fetchAllProductData(pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
            }
            mServicePresenter.getAllServicesData(this.activity!!, "Products", pagination, tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    private fun fetchSearchListData(mSearchString: String, pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
            }
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mSearchPresenter.getSubCategoriesData(mSearchString, personData.user_id!!, pagination, this.activity!!,"Products")
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this!!.activity!!) * .2f).toInt()
        return getTopMarginH
    }

    override fun onDetach() {
        super.onDetach()
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        private val ARG_PARAM3 = "param3"
        private var value = "param2"
        private var isAddFilterShow = true

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment ProductFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String, param3: Boolean): ProductFragment {
            val fragment = ProductFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            args.putBoolean(ARG_PARAM3, param3)
            fragment.arguments = args
            value = param1
            isAddFilterShow = param3
            return fragment
        }
    }

    private fun mod(x: Int, y: Int): Int {
        if (x % y == 0) {
            return x / y
        } else {
            return (x / y) + 1
        }
    }

    // TODO PAGINATION
    override fun onWishListApiSuccessResult(mWishList: List<Any?>?, message: String) {
        if (view != null) {
            if (mWishRecyclerAdapter.itemCount != mWishRecyclerAdapter.getTotalRecord()) {
                progress_view.visibility = View.GONE
                hideErrorView()
                rl_nrf.visibility = View.GONE
                loadItemsLayout_recyclerView.visibility = View.GONE
                if (activity != null && ProductFragment() != null) {
                    mWishRecyclerAdapter.addAll(mWishList as List<ServicesDataResponse?>)
                }
                isLoad = false
            } else {
                loadItemsLayout_recyclerView.visibility = View.GONE
                isListLastPage = true
                progress_view.visibility = View.GONE
                rl_nrf.visibility = View.VISIBLE
                hideErrorView()
            }
        }
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        if (view != null) {
            progress_view.visibility = View.GONE
            loadItemsLayout_recyclerView.visibility = View.GONE
            isLoad = false
            if (activity != null && ProductFragment() != null) {
                if (isServerError) {
                    isListLastPage = true
                }
                if (TOTAL_PAGES != 1) {
                    showErrorView(message)
                }
            }
        }
    }

    override fun onSubCategoriesSuccessResult(mCategoriesList: List<ServicesDataResponse>, message: String) {
        if (view != null) {
            if (mWishRecyclerAdapter.itemCount != mWishRecyclerAdapter.getTotalRecord()) {
                progress_view.visibility = View.GONE
                rl_nrf.visibility = View.GONE
                hideErrorView()
                loadItemsLayout_recyclerView.visibility = View.GONE
                if (activity != null && ProductFragment() != null) {
                    mWishRecyclerAdapter.addAll(mCategoriesList as List<ServicesDataResponse?>)
                }
                isLoad = false
            } else {
                rl_nrf.visibility = View.VISIBLE
                loadItemsLayout_recyclerView.visibility = View.GONE
                isListLastPage = true
                progress_view.visibility = View.GONE
                hideErrorView()
            }
        }
    }

    override fun onSubCatergoriesFailureResult(message: String) {
        if (view != null) {
            progress_view.visibility = View.GONE
            rl_nrf.visibility = View.VISIBLE
            loadItemsLayout_recyclerView.visibility = View.GONE
            isLoad = false
            if (activity != null && ProductFragment() != null) {
//                if (isServerError) {
                isListLastPage = true
//                }
                if (TOTAL_PAGES != 1) {
                    showErrorView(message)
                }
            }
        }
    }

    override fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        val mProductData = cellRow as ServicesDataResponse
        if (mString == "favorite") {
            showPopupMenu(mView, mProductData, mAttributeValue, p1)

        } else if (mString == "cellClick") {
            activity!!.toast("Under Development")
        } else if (mString == "shareClick") {
            showPopupMenu(mView, mProductData, mAttributeValue, p1)
        } else if (mString.equals("Cell Item click")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = Intent(context, ProductDetailActivity::class.java)
                intent.putExtra("mProductData", mProductData)
                intent.putExtra("productType", "product")
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    val transitionActivityOptions: ActivityOptions =
                            ActivityOptions.makeSceneTransitionAnimation(activity,
                                    mView, "iv_image")
//                    startActivity(intent, transitionActivityOptions.toBundle())
                    startActivityForResult(intent, EDITSERVICEPRODUCT, transitionActivityOptions.toBundle())
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                } else {
//                    startActivity(intent)
                    startActivityForResult(intent, EDITSERVICEPRODUCT)
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                }
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        }
    }

    override fun retryPageLoad() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (cellRow.is_favourite == 0) {
            cellRow.is_favourite = 1
            activity?.toast("Marked Favorite")
        } else {
            cellRow.is_favourite = 0
            activity?.toast("Marked Unfavorite")
        }
        mWishRecyclerAdapter.notifyItemChanged(p1)
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest.create()
        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLocationChanged(location: Location?) {
        currentLatitude = location!!.latitude
        currentLongitude = location.longitude
        val d = SphericalUtil.computeDistanceBetween(
                LatLng(oldLattitude, oldLongitude), LatLng(location.latitude, location.longitude))
        if (d >= 500.00 || oldLattitude == 0.0) {
            oldLattitude = currentLatitude
            oldLongitude = currentLongitude
            Utils.savePreferences(Constant.USER_LOC_LAT, "" + currentLatitude, this.context!!)
            Utils.savePreferences(Constant.USER_LOC_LONG, "" + currentLongitude, this.context!!)
//            getAllDashboardList()
        }
    }

    fun locationEnable(context: Activity, mLocationRequest: LocationRequest, mGoogleApiClient: GoogleApiClient) {
        // Setting API to check the GPS on device is enabled or not
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)

        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            //                final LocationSettingsStates = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                    try {
                        //  getAllDashboardList()
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(
                                context,
                                100)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can
            // initialize location requests here.
            // Location settings are not satisfied. However, we have no way
            // to fix the settings so we won't show the dialog.
        }
    }

    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient?.isConnected!!) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                        this)
                mGoogleApiClient?.disconnect()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient?.isConnected!!) {
                stopLocationUpdates()
            }
        }
    }

    private fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this)
    }

    /**
     * Initialize the google api client
     */
    @Synchronized
    private fun buildGoogleApiClient() {
        mLocationRequest = LocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this.activity!!)
                .addConnectionCallbacks(this@ProductFragment)
                .addOnConnectionFailedListener(this@ProductFragment)
                .addApi(LocationServices.API)
                .build()

    }

    override fun onResume() {
        super.onResume()
        /*if (this!!.isRefresh!!) {
            currentPage = 1
            isListLastPage = false
            clearList()
            callApi(false)
        }*/
        if (mGoogleApiClient != null) {
            if (!mGoogleApiClient?.isConnected!!) {
                mGoogleApiClient!!.connect()
            }
        }
    }

    fun clearList() {
        mWishRecyclerAdapter.removeAll()
        mWishRecyclerAdapter.notifyDataSetChanged()
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            view_list.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            view_list.visibility = View.GONE
            err_title.visibility = View.GONE
        }
    }

    /**
     * Method To Create Popup Menu
     * for share, Favorite, Report
     */
    private fun showPopupMenu(view: View, mProductData: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val popup = PopupMenu(activity, view)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        if (mAttributeValue == "0") {
            favItem?.title = Utils.getText(context, StringConstant.home_unfavourite)
        } else {
            favItem?.title = Utils.getText(context, StringConstant.favorite)
        }
        menu?.findItem(R.id.menu_share)!!.title = Utils.getText(context, StringConstant.share_text)
        menu?.findItem(R.id.menu_report_this)!!.title = Utils.getText(context, StringConstant.report_this)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    val postString: String = personData.first_name + " is offering " + '"' + mProductData.title!! + '"' + " on \n http://tetoota.com"

                    var url: String = "";
                    if (mProductData.image != null && mProductData.image_thumb != null) {
                        url = Utils.getUrl(context, mProductData.image, mProductData.image_thumb, false)
                    } else {
                        url = Utils.getUrl(context, mProductData.image!!)
                    }
                    // shareProduct(mProductData.title!!,url)
                    shareService(postString, url)
                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this.activity!!)) {
                        mServicePresenter.markFavorite(this.activity!!, mProductData, mAttributeValue, p1)
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    ReportConversationDialog(context, Constant.DIALOG_LOGIN_FAILURE_ALERT, mProductData.id.toString(),
                            this@ProductFragment, getString(R.string.str_report_issue)).show()
                }
            }
            true
        }
        popup.show()
    }

    /**
     * Method to Share Product
     * TODO Deep linking
     */


    fun shareService(mTitle: String, mImageUrl: String) {
        try {
            val text = mTitle
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            //  shareIntent.type = "image/*"
            //  shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            //  startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
            startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: android.content.ActivityNotFoundException) {
        }
    }

/*
    fun shareProduct(mTitle: String, mImageUrl: String) {
        val text = mTitle
        val pictureUri = Uri.parse(mImageUrl)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share images..."))
    }
*/*/

    private fun setMultiLanguageText() {
        tv_add.text = Utils.getText(context, StringConstant.str_addpost)
        filter.text = Utils.getText(context, StringConstant.str_filter)
    }

    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
        Utils.hideSoftKeyboard(this.activity!!)
        if (message == "report") {
            showProgressDialog(Utils.getText(context, StringConstant.please_wait))
            mHomePresenter.report(this.activity!!, mServiceId, mMesgDesc)
        }
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            activity!!.toast(message)
        } else {
            activity!!.toast(message)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setIcon(R.drawable.abc_ic_search_api_material)
        val searchView = MenuItemCompat.getActionView(menu?.findItem(R.id.action_search)) as SearchView
        val searchManager = context!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        menu?.clear();
        searchView.setOnSearchClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                Log.i("SearchView", "Search CLick-ProductFragment")
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
//                Log.e("SearchView", "onQueryTextChange-ServiceFragment");
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.e("SearchView", "onQueryTextSubmit-ProductFragment");
                isSearch = true
                isRefresh = false
                clearList()
                currentPage = 1
                isListLastPage = false
                mQuery = query
                callApi(false)
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setIcon(R.drawable.abc_ic_search_api_material)
        val searchView = MenuItemCompat.getActionView(menu?.findItem(R.id.action_search)) as SearchView
        val searchManager = context!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        menu?.clear();
        searchView.setOnSearchClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                /*   var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
                   toolTitle.visibility = View.GONE*/
                Log.i("SearchView", "Search CLick-ProductFragment")
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
//                Log.e("SearchView", "onQueryTextChange-ServiceFragment");
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.e("SearchView", "onQueryTextSubmit-ProductFragment");
                isSearch = true
                isRefresh = false
                clearList()
                currentPage = 1
                isListLastPage = false
                mQuery = query
                callApi(false)
                return false
            }
        })
        super.onPrepareOptionsMenu(menu)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mServicePresenter.cancelRequest(this.activity!!, "products")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FILTER_REQUEST) {
                isRefresh = true
                refreshData()
            } else if (requestCode == EDITSERVICEPRODUCT) {
                refreshData()
            }
        } else {
            resetFilterData()
        }
    }

    private fun resetFilterData() {
        Utils.savePreferences(Constant.CATEGORY_ID, "", this.context!!)
        Utils.savePreferences(Constant.SUBCATEGORIES_ID, "", this.context!!)
        Utils.savePreferences(Constant.TOPRATED, "no", this.context!!)
        Utils.savePreferences(Constant.CITY, "", this.context!!)
    }

    public fun refreshData() {
        try {
            currentPage = 1
            isListLastPage = false
            clearList()
            callApi(false)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
