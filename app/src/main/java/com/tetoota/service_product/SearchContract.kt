package com.tetoota.service_product

import android.app.Activity
import com.tetoota.fragment.dashboard.ServicesDataResponse

/**
 * Created by jitendra.nandiya on 16-11-2017.
 */
class SearchContract {
    interface View {
        fun onSubCategoriesSuccessResult(mCategoriesList: List<ServicesDataResponse>, message: String)
        fun onSubCatergoriesFailureResult(message: String)
    }

    internal interface Presenter {
        fun getSubCategoriesData(mSearchString: String, user_id: String, mPageNumber: Int, mActivity: Activity, postType: String)
    }

    interface SearchApiListener {
        fun onCategoriesApiSuccess(mDataList: List<ServicesDataResponse?>?, message: String)
        fun onCategoriesApiFailure(message: String)
    }
}