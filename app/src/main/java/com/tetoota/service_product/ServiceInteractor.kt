package com.tetoota.service_product

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.dashboard.FavoriteResponse
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 24-07-2017.
 */
class ServiceInteractor {
    val sliderList: String? = "sliderList"
    var call: Call<UserServiceResponse>? = null
    var mWishListCall: Call<FavoriteListResponse>? = null
    var mServiceApiListener: ServiceContract.serviceApiListener

    constructor(mServiceApiListener: ServiceContract.serviceApiListener) {
        this.mServiceApiListener = mServiceApiListener
    }

    @Synchronized
    fun getServiceList(mActivity: Activity, postType: String, pagination: Int?, userId: String) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        call = TetootaApplication.getHeader()
                .getMultipleListType(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        personData.user_id.toString(), pagination!!, postType)
//        Log.e("HI ", personData.toString())
        call!!.enqueue(object : Callback<UserServiceResponse> {
            override fun onResponse(call: Call<UserServiceResponse>?,
                                    response: Response<UserServiceResponse>?) {
                var mDashboardSliderData: UserServiceResponse? = response?.body()
//                Log.e("getServiceList  ", "" + mDashboardSliderData)
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        if (mDashboardSliderData != null) {
                            Log.e("getServiceList  ", "if if")
                            mServiceApiListener.onWishListApiSuccess(mDashboardSliderData.data, "wishlist")
                        }
                    } else {
                        Log.e("getServiceList  ", "if else")
                        mServiceApiListener.onApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                } else {
                    Log.e("getServiceList  ", "else")
                    mServiceApiListener.onApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                Log.e("onFailure  ", t?.message.toString())

                mServiceApiListener.onApiFailure(t?.message.toString(), true)
            }
        })
    }

    @Synchronized
    fun getUserServiceList(mActivity: Activity, mUserId: String, postType: String, pagination: Int?) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        call = TetootaApplication.getHeader()
                .getMultipleListType(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId, pagination!!, postType)
        call!!.enqueue(object : Callback<UserServiceResponse> {
            override fun onResponse(call: Call<UserServiceResponse>?,
                                    response: Response<UserServiceResponse>?) {
                var mDashboardSliderData: UserServiceResponse? = response?.body()
                Log.e("getUserServiceList", "" + mDashboardSliderData)
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        if (mDashboardSliderData != null) {
                            Log.e("getUserServiceList  ", "if if")

                            mServiceApiListener.onWishListApiSuccess(mDashboardSliderData.data, "wishlist")
                        }
                    } else {
                        Log.e("getUserServiceList  ", "if el")

                        mServiceApiListener.onApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                } else {
                    Log.e("getUserServiceList  ", "el")

                    mServiceApiListener.onApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                mServiceApiListener.onApiFailure(t?.message.toString(), true)
            }
        })
    }


    @Synchronized
    fun getAllServiceList(mActivity: Activity, postType: String, pagination: Int?, myLatitude: Double, myLongitude: Double) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        val mCategoryId: String = Utils.loadPrefrence(Constant.CATEGORY_ID, "", mActivity)
        val mSubCategoriesId: String = Utils.loadPrefrence(Constant.SUBCATEGORIES_ID, "", mActivity)
        val mtTopRated = Utils.loadPrefrence(Constant.TOPRATED, "no", mActivity)
        val mCity = Utils.loadPrefrence(Constant.CITY, "", mActivity)
        call = TetootaApplication.getHeader()
                .dashboardAllApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), postType,
                        personData.user_id!!, pagination!!, mCategoryId, mSubCategoriesId, mtTopRated, mCity, myLatitude.toString(), myLongitude.toString())
        call!!.enqueue(object : Callback<UserServiceResponse> {
            override fun onResponse(call: Call<UserServiceResponse>?,
                                    response: Response<UserServiceResponse>?) {
                var mDashboardSliderData: UserServiceResponse? = response?.body()
//                Log.e("getAllServiceList ", "" + mDashboardSliderData)
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0 && response.body()?.data != null) {
                        if (mDashboardSliderData != null) {
                            mServiceApiListener.onWishListApiSuccess(mDashboardSliderData.data, postType)
                        }
                    } else {
                        mServiceApiListener.onApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                } else {
                    mServiceApiListener.onApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                mServiceApiListener.onApiFailure(t?.message.toString(), true)
            }
        })
    }

    @Synchronized
    fun getAllProductList(mActivity: Activity, postType: String, pagination: Int?, myLatitude: Double, myLongitude: Double) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        val mCategoryId: String = Utils.loadPrefrence(Constant.CATEGORY_ID, "", mActivity)
        val mSubCategoriesId: String = Utils.loadPrefrence(Constant.CATEGORY_ID, "", mActivity)
        val mtTopRated = Utils.loadPrefrence(Constant.TOPRATED, "", mActivity)
        val mCity = Utils.loadPrefrence(Constant.CITY, "", mActivity)
        call = TetootaApplication.getHeader()
                .dashboardAllApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), postType,
                        personData.user_id!!, pagination!!, mCategoryId, mSubCategoriesId, mtTopRated, mCity, myLatitude.toString(), myLongitude.toString())
        call!!.enqueue(object : Callback<UserServiceResponse> {
            override fun onResponse(call: Call<UserServiceResponse>?,
                                    response: Response<UserServiceResponse>?) {
                var mDashboardSliderData: UserServiceResponse? = response?.body()
                Log.e("getAllProductList ", "" + mDashboardSliderData)

                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0 && response.body()?.data != null) {
                        if (mDashboardSliderData != null) {
                            mServiceApiListener.onWishListApiSuccess(mDashboardSliderData.data, "wishlist")
                        }
                    } else {
                        mServiceApiListener.onApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                } else {
                    mServiceApiListener.onApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                mServiceApiListener.onApiFailure(t?.message.toString(), true)
            }
        })
    }

    @Synchronized
    fun getAllWishList(mActivity: Activity, postType: String, pagination: Int?, myLatitude: Double, myLongitude: Double) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        val mCategoryId: String = Utils.loadPrefrence(Constant.CATEGORY_ID, "", mActivity)
        val mSubCategoriesId: String = Utils.loadPrefrence(Constant.CATEGORY_ID, "", mActivity)
        val mtTopRated = Utils.loadPrefrence(Constant.TOPRATED, "", mActivity)
        val mCity = Utils.loadPrefrence(Constant.CITY, "", mActivity)
        call = TetootaApplication.getHeader()
                .dashboardAllApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), postType,
                        personData.user_id!!, pagination!!, mCategoryId, mSubCategoriesId, mtTopRated, mCity, myLatitude.toString(), myLongitude.toString())
        call!!.enqueue(object : Callback<UserServiceResponse> {
            override fun onResponse(call: Call<UserServiceResponse>?,
                                    response: Response<UserServiceResponse>?) {
                var mDashboardSliderData: UserServiceResponse? = response?.body()
//                Log.e("getAllWishList ", "" + mDashboardSliderData)
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        if (mDashboardSliderData != null) {
                            mServiceApiListener.onWishListApiSuccess(mDashboardSliderData.data, "wishlist")
                        }
                    } else {
                        mServiceApiListener.onApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                } else {
                    mServiceApiListener.onApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                mServiceApiListener.onApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun markFavorites(mActivity: Activity, cellRow: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val call: Call<FavoriteResponse> = TetootaApplication.getHeader()
                .favoriteApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), cellRow.id.toString(),
                        Utils.loadPrefrence(Constant.USER_ID, "", mActivity), "POST_FAV", mAttributeValue)
        call.enqueue(object : Callback<FavoriteResponse> {
            override fun onFailure(call: Call<FavoriteResponse>?, t: Throwable?) {
                println("Failure API REsponse")
                mServiceApiListener.onApiFailure(t?.message.toString(), true)
            }

            override fun onResponse(call: Call<FavoriteResponse>?,
                                    response: Response<FavoriteResponse>?) {
                println("Success API REsponse")
                var mFavoriteResponse: FavoriteResponse? = response?.body()
                if (response?.code() == 200) {
                    mServiceApiListener.onFavoriteApiSuccess("favorite", cellRow, p1)
                } else {
                    mServiceApiListener.onApiFailure(mFavoriteResponse?.meta?.message.toString(), true)
                }
            }
        })
    }

    fun cancelREquest(mActivity: Activity, mCancelRequest: String) {
        if (mCancelRequest.equals("userService")) {
            call?.cancel()
        } else {
            mWishListCall?.cancel()
        }
    }
}