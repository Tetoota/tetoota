package com.tetoota.service_product
import android.app.Activity
import com.tetoota.fragment.dashboard.ServicesDataResponse

/**
 * Created by charchit.kasliwal on 24-07-2017.
 */
class ServicePresenter : ServiceContract.Presenter, ServiceContract.serviceApiListener{

    var mServiceContract: ServiceContract.View
    private val mHomeInteractor : ServiceInteractor by lazy {
        ServiceInteractor(this)
    }

    constructor(mServiceContract: ServiceContract.View) {
        this.mServiceContract = mServiceContract
    }

    override fun getWishListData(mActivity: Activity) {
   // mHomeInteractor.getWishListdata(mActivity)
    }

    override fun cancelRequest(mActivity: Activity,wishListType : String) {
        mHomeInteractor.cancelREquest(mActivity,wishListType)
    }

    override fun onWishListApiFailure(message: String, isServerError: Boolean) {
        super.onWishListApiFailure(message, isServerError)
       mServiceContract.onWishListApiFailure(message,isServerError)
    }

    /**
     * the mActivity
     * the user post type "Services"
     */
    override fun getServicesData(mActivity: Activity, postType: String,pagination : Int?,userId:String) {
        mHomeInteractor.getServiceList(mActivity,postType,pagination,userId!!)
    }
    override fun getUserServiceList(mActivity: Activity,mUserId : String, postType: String,pagination : Int?) {
        mHomeInteractor.getUserServiceList(mActivity,mUserId, postType, pagination)
    }
    override fun getAllServicesData(mActivity: Activity, postType: String, pagination: Int?, myLatitude: Double, myLongitude: Double) {
        if(postType == "Products"){
            mHomeInteractor.getAllProductList(mActivity,postType,pagination, myLatitude, myLongitude)
        }else if(postType == "Wishlist"){
            mHomeInteractor.getAllWishList(mActivity,postType,pagination, myLatitude, myLongitude) }
        else {
            mHomeInteractor.getAllServiceList(mActivity,postType,pagination, myLatitude, myLongitude)
        }

    }

    override fun onWishListApiSuccess(mDataList: List<Any?>?, message: String) {
        mServiceContract.onWishListApiSuccessResult(mDataList,message)
    }

    override fun onFavoriteApiSuccess(message: String, cellRow: ServicesDataResponse, p1: Int) {
        mServiceContract.favoriteApiResult(message,cellRow,p1)
    }

    override fun onApiFailure(message: String, isServerError: Boolean) {
        mServiceContract.onApiFailureResult(message, isServerError)
    }

    override fun markFavorite(mActivity: Activity,cellRow : ServicesDataResponse,mAttributeValue : String,p1 : Int) {
        mHomeInteractor.markFavorites(mActivity,cellRow,mAttributeValue,p1)
    }
}