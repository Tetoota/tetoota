package com.tetoota.message

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.fragment.inbox.fragment.InboxFoodFragment
import com.tetoota.fragment.inbox.fragment.InboxFragment
import com.tetoota.service_product.ServiceProductFragment
import com.tetoota.utility.Constant
import kotlinx.android.synthetic.main.chat_tab_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

class ServicesTabLayout : BaseActivity() {
    var tabType = ""
    var bundle: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.service_tab_layout)
        bundle = intent.extras
        initView()
    }

    private fun initView() {
        iv_home.visibility = View.VISIBLE
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        var fragmentInbox = ServiceProductFragment()
        var fragmentInboxFood = ServiceProductFragment()
        fragmentInbox.arguments = bundle
        fragmentInboxFood.arguments = bundle
        var adapter = ServicesTabAdapter(this.supportFragmentManager)
        adapter.addFragment(fragmentInbox, Constant.SERVICES);
        adapter.addFragment(fragmentInboxFood, Constant.FOOD);
        viewPagerFood.adapter = adapter
        tabLayout.setupWithViewPager(viewPagerFood)
        val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(0)
        val p = tab.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(0, 0, 1, 0)
        tab.requestLayout()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this)
        finish()
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ServicesTabLayout::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

}