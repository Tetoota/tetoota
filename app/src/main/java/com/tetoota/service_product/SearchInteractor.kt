package com.tetoota.service_product

import android.app.Activity
import com.tetoota.TetootaApplication
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jitendra.nandiya on 16-11-2017.
 */
class SearchInteractor {
    var mSubCategoriesApiListener: SearchContract.SearchApiListener
    val API_SUCCESS_VALUE = 101
    val API_FAILURE_VALUE = 202

    constructor(mSubCategoriesApiListener: SearchContract.SearchApiListener) {
        this.mSubCategoriesApiListener = mSubCategoriesApiListener
    }

    fun getAllCategoriesData(mSearchString: String, mUserId: String, mPageNumber: Int, mActivity: Activity, postType: String): Unit {
        var call: Call<UserServiceResponse> = TetootaApplication.getHeader()
                .getSearchDataList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mSearchString, mPageNumber, mUserId, postType)
        call.enqueue(object : Callback<UserServiceResponse> {
            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                println("Review API RESPONSE Failure ${t?.message.toString()}")
                mSubCategoriesApiListener.onCategoriesApiFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<UserServiceResponse>?, response: Response<UserServiceResponse>?) {
                var mCategoriesListData: UserServiceResponse? = response?.body()
                println("UserVisibilityResponse Code ${response?.code()}")
                if (response?.code() == 200) {
                    if (mCategoriesListData?.data!!.size > 0) {
                        mSubCategoriesApiListener.onCategoriesApiSuccess(mCategoriesListData.data, "success")
                    } else {
                        mSubCategoriesApiListener.onCategoriesApiFailure(response.body()?.meta?.message.toString())
                    }
                } else {
                    mSubCategoriesApiListener.onCategoriesApiFailure(response?.body()?.meta?.message.toString())
                }
            }
        })
    }
}