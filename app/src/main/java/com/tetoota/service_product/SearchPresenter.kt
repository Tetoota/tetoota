package com.tetoota.service_product

import android.app.Activity
import com.tetoota.fragment.dashboard.ServicesDataResponse

/**
 * Created by jitendra.nandiya on 16-11-2017.
 */
class SearchPresenter : SearchContract.Presenter, SearchContract.SearchApiListener {

    override fun onCategoriesApiSuccess(mDataList: List<ServicesDataResponse?>?, message: String) {
        mCategoriesView.onSubCategoriesSuccessResult(mDataList as List<ServicesDataResponse>, message)
    }

    var mCategoriesView: SearchContract.View
    private val mCategoriesInteractor: SearchInteractor by lazy {
        SearchInteractor(this)
    }

    constructor(mCategoriesView: SearchContract.View) {
        this.mCategoriesView = mCategoriesView
    }

    override fun getSubCategoriesData(mSearchString: String, user_id : String, mPageNumber: Int, mActivity: Activity,postType:String) {
        mCategoriesInteractor.getAllCategoriesData(mSearchString, user_id, mPageNumber, mActivity,postType)
    }

    override fun onCategoriesApiFailure(message: String) {
        mCategoriesView.onSubCatergoriesFailureResult(message)
    }
}