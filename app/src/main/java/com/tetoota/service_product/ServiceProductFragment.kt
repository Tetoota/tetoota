package com.tetoota.service_product

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_add_post_request.*
import kotlinx.android.synthetic.main.activity_service_product.*
import kotlinx.android.synthetic.main.activity_service_product.tab_layout
import kotlinx.android.synthetic.main.activity_service_product.view_pager_container
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

class ServiceProductFragment : BaseFragment(), ProfileCompletionDialog.IDialogListener {
    private val ADDPOST_REQUEST: Int = 14
    private var mFragmentList: MutableList<Fragment> = ArrayList()
    private var mFragmentTitleList: MutableList<String> = ArrayList()
    var currentTabSelected: Int? = 0
    var bundle: Bundle? = null
    var tabType = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("onCreate","ServiceProductFragment")

        bundle = arguments
        if (bundle != null) {
            if (bundle!!.containsKey("Tab"))
                tabType = bundle!!.getString("Tab").toString()
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_service_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        fb_addpost.onClick {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            if (personData.complete_percentage!!.toInt() < 50) {
                ProfileCompletionDialog(context, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                        this, getString(R.string.err_msg_mobile_number_limit)).show()
            } else {
                if (tabType.equals("serviceTab", true)) {
                    val intent = AddPostRequestActivity.newMainIntent(context!!)
                    if (intent != null) {
                        if (currentTabSelected == 0) {
                            intent.putExtra("Tab", "serviceTab")
                        } else {
                            intent.putExtra("Tab", "productTab")
                        }
//                        intent.putExtra("TabSelected", currentTabSelected)
                    }
                    ActivityStack.getInstance(context!!)
//                    startActivity(intent)
                    startActivityForResult(intent, ADDPOST_REQUEST)
                } else {
                    val intent = AddPostRequestActivity.newMainIntent(context!!)
                    if (intent != null) {
                        intent.putExtra("Tab", "wishListTab")
                    }
                    ActivityStack.getInstance(context!!)
//                    startActivity(intent)
                    startActivityForResult(intent, ADDPOST_REQUEST)
                }
            }
        }
//        iv_home.setOnClickListener {
//            val intent = Intent(context!!, MainActivity::class.java)
//            startActivity(intent)
////            context.finish()
//        }
    }

    private fun initViews(): Unit {

        /**************kamal************/
//        iv_home.visibility = View.VISIBLE
//        iv_close.visibility = View.VISIBLE
//        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
//        iv_close.onClick { onBackPressed() }
        setupViewPager(view_pager_container)
        view_pager_container.disableScroll(false)
        tab_layout.setupWithViewPager(view_pager_container)
//        if (tabType.equals("serviceTab", true)) {
//            toolbar_title.text = Utils.getText(context, StringConstant.my_services)
//            view_pager_container.currentItem = 0
//        } else {
//            toolbar_title.text = Utils.getText(context, StringConstant.wishlist)
//            tab_layout.visibility = View.GONE
//            view_pager_container.currentItem = 2
//        }

        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                currentTabSelected = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
    }


//    override fun onBackPressed() {
//        super.onBackPressed()
//        ActivityStack.removeActivity(context!!)
//        finish()
//    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ServiceProductFragment::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(fragmentManager!!)
        if (tabType.equals("serviceTab", true)) {
            adapter.addFragment(ServiceFragment.newInstance("haveUserId", "yes", false), Utils.getText(context, StringConstant.services))
            adapter.addFragment(ProductFragment.newInstance("haveUserId", "yes", false), Utils.getText(context, StringConstant.product))
        } else {
            adapter.addFragment(WishlistFragment.newInstance("haveUserId", "yes", false), Utils.getText(context, StringConstant.wishlist))
        }
        viewPager.adapter = adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
//        private var mFragmentList: MutableList<Fragment> = ArrayList()
//        private var mFragmentTitleList: MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(context!!)) {
                val intent = ProfileDetailActivity.newMainIntent(context!!)
                ActivityStack.getInstance(context!!)
                startActivity(intent)
            } else {
                // TODO CHECK NET
                showSnackBar(Utils.getText(context!!, StringConstant.str_check_internet),view)
            }
        } else {

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADDPOST_REQUEST && data?.getStringExtra("ADDPOST_REQUEST").equals("SERVICE") && mFragmentList.size >= 0) {
            var serviceFragment: MyServiceFragment = mFragmentList[0] as MyServiceFragment
//            MyServiceFragment.refreshData()
        } else if (requestCode == ADDPOST_REQUEST && data?.getStringExtra("ADDPOST_REQUEST").equals("WISHLIST") && mFragmentList.size >= 0) {
            var wishlistFragment: WishlistFragment = mFragmentList[0] as WishlistFragment
            wishlistFragment.refreshData()
        } else if (requestCode == ADDPOST_REQUEST && data?.getStringExtra("ADDPOST_REQUEST").equals("PRODUCT") && mFragmentList.size >= 0) {
            var productFragment: ProductFragment = mFragmentList[1] as ProductFragment
            productFragment.refreshData()
        }
    }
}
