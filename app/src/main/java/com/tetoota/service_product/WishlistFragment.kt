package com.tetoota.service_product

import android.Manifest
import android.app.Activity
import android.app.ActivityOptions
import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.annotation.VisibleForTesting
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import android.widget.PopupMenu
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.google.gson.Gson
import com.google.maps.android.SphericalUtil
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.customviews.PaginationScrollListener
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.customviews.ReportConversationDialog
import com.tetoota.customviews.StartConversationDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.filter.FilterActivity
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.dashboard.HomeContract
import com.tetoota.fragment.dashboard.HomePresenter
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.dashboard.WishRecyclerAdapter
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.floating_add_filter_layout.*
import kotlinx.android.synthetic.main.fragment_wishlist.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [WishlistFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [WishlistFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WishlistFragment : BaseFragment(), ServiceContract.View, SearchContract.View, WishRecyclerAdapter.IAdapterClick,
        WishRecyclerAdapter.PaginationAdapterCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ProfileCompletionDialog.IDialogListener, StartConversationDialog.IDialogListener, ReportConversationDialog.IDialogListener, HomeContract.View {
    private var tetootaApplication: TetootaApplication? = null
    var link: String = "http://tetoota.com/service/?"
    lateinit var shortLink: Uri

    override fun onInvalidTextAPiFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = ProfileDetailActivity.newMainIntent(this.activity!!)
                ActivityStack.getInstance(this.activity!!)
                startActivity(intent)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    private val mSearchPresenter: SearchPresenter by lazy {
        SearchPresenter(this@WishlistFragment)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@WishlistFragment)
    }
    var exchangePostType: String? = ""
    private var intent: Intent? = null
    private val FILTER_REQUEST: Int = 12


    private val EDITSERVICEPRODUCT_REQUEST: Int = 13
    private val ADDPOST_REQUEST: Int = 14
    public var isRefresh: Boolean? = false
    public var isSearch: Boolean? = false
    public var mQuery: String? = null
    private var isLoad = false
    var isListLastPage = false
    private var isFirstLoading = false
    private var TOTAL_PAGES = 1
    private val RECORD_PER_PAGE = 10
    var currentPage = 1
    var linearLayoutManager: LinearLayoutManager? = null
    var menu: Menu? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var currentLatitude: Double = 0.toDouble()
    private var currentLongitude: Double = 0.toDouble()
    internal var oldLattitude = 0.0
    internal var oldLongitude = 0.0

    private lateinit var mWishRecyclerAdapter: WishRecyclerAdapter
    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@WishlistFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        validateAppCode()
        tetootaApplication = activity!!.applicationContext as TetootaApplication

        TetootaApplication.setTextValuesHashMap(Utils.getLangByCode(activity,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", activity)))

        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient()
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wishlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        service_rl_viewpager.visibility == View.GONE
        setupRecyclerView()

        /*  var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
          toolTitle.visibility = View.VISIBLE
          toolTitle.text = "Tetoota"*/

        if (isAddFilterShow)
            ll_addHome.visibility = View.VISIBLE
        else {
            ll_addHome.visibility = View.GONE
        }
        ll_addHome.visibility = View.GONE

        if (mGoogleApiClient != null) {
            locationEnable(this.activity!!, mLocationRequest!!, mGoogleApiClient!!)
        }
        setMultiLanguageText()
        resetFilterData()
        callWishListApi(false)
        view_list.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isListLastPage
            }

            override fun loadMoreItems() {
                isLoad = true
                currentPage += 1
                loadItemsLayout_recyclerView.visibility = View.VISIBLE
                callWishListApi(true)
            }

            override fun getTotalPageCount(): Int {
                return TOTAL_PAGES
            }

            override fun isLoading(): Boolean {
                return isLoad
            }
        })
        ll_addpost.onClick {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            val intent = AddPostRequestActivity.newMainIntent(this.activity!!)
            intent!!.putExtra("Tab", "serviceTab")
            ActivityStack.getInstance(this.activity!!)
//                startActivity(intent)
            startActivityForResult(intent, ADDPOST_REQUEST)
            /* if (personData.complete_percentage!!.toInt() < 50) {
                 ProfileCompletionDialog(activity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                         this@WishlistFragment, getString(R.string.err_msg_mobile_number_limit)).show()
             } else {
                 val intent = AddPostRequestActivity.newMainIntent(this.activity!!)
                 intent!!.putExtra("Tab", "serviceTab")
                 ActivityStack.getInstance(this.activity!!)
 //                startActivity(intent)
                 startActivityForResult(intent, ADDPOST_REQUEST)
             }*/
        }
        ll_filter.onClick {
            val intent = FilterActivity.newMainIntent(this.activity!!)
            ActivityStack.getInstance(this.activity!!)
//            startActivity(intent)
            startActivityForResult(intent, FILTER_REQUEST)
        }


    }

    private fun setupRecyclerView() {
        view_list.setHasFixedSize(true)
        view_list.itemAnimator = DefaultItemAnimator()
        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view_list.layoutManager = linearLayoutManager
        mWishRecyclerAdapter = WishRecyclerAdapter(activity!!, iAdapterClickListener = this,
                iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        view_list.adapter = mWishRecyclerAdapter
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    private fun fetchWishListData(pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
                //  showProgressDialog("Please Wait...")
            }
            mServicePresenter.getServicesData(this.activity!!, "Wishlist", pagination, Utils.loadPrefrence(Constant.USER_ID, "", context).toString())
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    private fun fetchAllServiceData(pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
                disableWindowTouch(this.activity!!)
            }
            Log.e("homeloc1", "" + tetootaApplication!!.myLatitude)
            Log.e("homeloc2", " " + tetootaApplication!!.myLongitude)

            mServicePresenter.getAllServicesData(this.activity!!, "Wishlist", pagination, tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    private fun fetchSearchListData(mSearchString: String, pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
            }
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mSearchPresenter.getSubCategoriesData(mSearchString, personData.user_id!!, pagination, this.activity!!, "Wishlist")
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setIcon(R.drawable.abc_ic_search_api_material)
        val searchView = MenuItemCompat.getActionView(menu?.findItem(R.id.action_search)) as SearchView
        val searchManager = context?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        menu?.clear();
        searchView.setOnSearchClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                Log.i("SearchView", "Search CLick-WishlistFragment")
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
//                Log.e("SearchView", "onQueryTextChange-ServiceFragment");
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.e("SearchView", "onQueryTextSubmit-WishlistFragment");
                isSearch = true
                isRefresh = false
                clearList()
                currentPage = 1
                isListLastPage = false
                mQuery = query
                callWishListApi(false)
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setIcon(R.drawable.abc_ic_search_api_material)
        val searchView = MenuItemCompat.getActionView(menu?.findItem(R.id.action_search)) as SearchView
        val searchManager = context!!.getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnSearchClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                /*     var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
                     toolTitle.visibility = View.GONE*/
                Log.i("SearchView", "Search CLick-WishlistFragment")
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
//                Log.e("SearchView", "onQueryTextChange-ServiceFragment");
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.e("SearchView", "onQueryTextSubmit-WishlistFragment");
                isSearch = true
                isRefresh = false
                clearList()
                currentPage = 1
                isListLastPage = false
                mQuery = query
                callWishListApi(false)
                return false
            }
        })
        super.onPrepareOptionsMenu(menu)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mServicePresenter.cancelRequest(this!!.activity!!, "services")
        if (activity != null) {
            enableWindowTouch(activity!!)
        }
    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .2f).toInt()
        return getTopMarginH
    }

    override fun onDetach() {
        super.onDetach()
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        private val ARG_PARAM3 = "param3"
        private var value = ""
        private var isAddFilterShow = true
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment WishlistFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String, param3: Boolean): WishlistFragment {
            val fragment = WishlistFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            args.putBoolean(ARG_PARAM3, param3)
            value = param1
            isAddFilterShow = param3
            fragment.arguments = args
            Log.i(javaClass.name, "======================" + isAddFilterShow)
            return fragment
        }
    }

    private fun mod(x: Int, y: Int): Int {
        if (x % y == 0) {
            return x / y
        } else {
            return (x / y) + 1
        }
    }

    override fun onWishListApiSuccessResult(mWishList: List<Any?>?, message: String) {
        if (this!!.activity!! != null) {
            if (progress_view != null) {
                progress_view.visibility = View.GONE
            }
            enableWindowTouch(this.activity!!)
            hideErrorView()
            loadItemsLayout_recyclerView.visibility = View.GONE
            if (activity != null && WishlistFragment() != null) {
//            mWishRecyclerAdapter.setElements(mWishList as MutableList<ServicesDataResponse?>, false)
                if (mWishList != null) {
                    if (mWishList.size > 0) {
                        rl_nrf.visibility = View.GONE
                        mWishRecyclerAdapter.addAll(mWishList as List<ServicesDataResponse?>)
                        mWishRecyclerAdapter.notifyDataSetChanged()
                    } else {
                        rl_nrf.visibility = View.VISIBLE
                    }
                } else {
                    rl_nrf.visibility = View.VISIBLE
                }
                Log.i(javaClass.name, "=====================mWishList  " + mWishList!!.size)
                /*if(isFirstLoading){
                isFirstLoading = false
                var mList : List<ServicesDataResponse> = mWishList as List<ServicesDataResponse>
                var value = mod(mList[0].total_record!!,RECORD_PER_PAGE)
                println("value ${value.toString()}")
                TOTAL_PAGES = value
                if (currentPage <= TOTAL_PAGES) mWishRecyclerAdapter.addLoadingFooter(TOTAL_PAGES)
                else isLastPage = true
            }else{
                mWishRecyclerAdapter.removeLoadingFooter()
                isLoad = false
                mWishRecyclerAdapter.addAll(mWishList as List<ServicesDataResponse>)
                if (currentPage !== TOTAL_PAGES)
                    mWishRecyclerAdapter.addLoadingFooter(TOTAL_PAGES)
                else
                    isLastPage = true
            }*/
                /*  mWishRecyclerAdapter.setElements(mWishList as MutableList<ServicesDataResponse?>,false)
              mWishRecyclerAdapter.notifyDataSetChanged()*/
            }
        }
        isLoad = false
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        if (view != null) {
            isLoad = false
            progress_view.visibility = View.GONE
            enableWindowTouch(this.activity!!)
            loadItemsLayout_recyclerView.visibility = View.GONE
            if (activity != null && WishlistFragment() != null) {
                if (isServerError) {
                    isListLastPage = true
                }
                if (TOTAL_PAGES != 1) {
                    showErrorView(message)
                }
            }
        }
    }

    override fun onSubCategoriesSuccessResult(mCategoriesList: List<ServicesDataResponse>, message: String) {
        progress_view.visibility = View.GONE
        enableWindowTouch(this.activity!!)
        hideErrorView()
        loadItemsLayout_recyclerView.visibility = View.GONE
        if (activity != null && WishlistFragment() != null) {
            if (mCategoriesList.size > 0) {
                rl_nrf.visibility = View.GONE
                mWishRecyclerAdapter.addAll(mCategoriesList as List<ServicesDataResponse?>)
                mWishRecyclerAdapter.notifyDataSetChanged()
            } else {
                rl_nrf.visibility = View.VISIBLE
            }
        }
        isLoad = false
    }

    override fun onSubCatergoriesFailureResult(message: String) {
        if (view != null) {
            isLoad = false
            progress_view.visibility = View.GONE
            rl_nrf.visibility = View.VISIBLE
            enableWindowTouch(this.activity!!)
            loadItemsLayout_recyclerView.visibility = View.GONE
            if (activity != null && WishlistFragment() != null) {
//                if(isServerError) {
                isListLastPage = true
//                }
                if (TOTAL_PAGES != 1) {
                    showErrorView(message)
                }
            }
        }
    }

    override fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        val mProductData = cellRow as ServicesDataResponse
        if (mString == "favorite") {
//            if (Utils.haveNetworkConnection(activity)) {
//                if (Utils.haveNetworkConnection(activity)) {

            /* if (Utils.haveNetworkConnection(activity)) {
                 mServicePresenter.markFavorite(activity, mProductData, mAttributeValue, p1)
             } else {
                 activity.toast(resources.getString(R.string.str_check_internet))
             }*/

//                } else {
//                    activity.toast("Please check Internet connetivity")
//                }
//            } else {
//                activity.toast("Please check Internet connetivity")
//            }
            showPopupMenu(mView, mProductData, mAttributeValue, p1)
        } else if (mString == "cellClick") {
            activity!!.toast("Under Development")
        } else if (mString == "shareClick") {
        } else if (mString.equals("Cell Item click")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = Intent(context, ProductDetailActivity::class.java)
                intent.putExtra("mProductData", mProductData)
                intent.putExtra("productType", "wishlist")
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    val transitionActivityOptions: ActivityOptions =
                            ActivityOptions.makeSceneTransitionAnimation(activity,
                                    mView, "iv_image")
//                    startActivity(intent, transitionActivityOptions.toBundle())
                    startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST, transitionActivityOptions.toBundle())
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                } else {
//                    startActivity(intent)
                    startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST)
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                }
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        }
    }

    /**
     * Method To Create Popup Menu
     * for share, Favorite, Report
     */
    private fun showPopupMenu(view: View, mProductData: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val popup = PopupMenu(activity, view)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        if (mAttributeValue == "0") {
            favItem?.title = Utils.getText(context, StringConstant.home_unfavourite)
        } else {
            favItem?.title = Utils.getText(context, StringConstant.favorite)
        }
        menu?.findItem(R.id.menu_share)!!.title = Utils.getText(context, StringConstant.share_text)
        menu?.findItem(R.id.menu_report_this)!!.title = Utils.getText(context, StringConstant.report_this)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
//                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
//                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//                    val postString: String = personData.first_name +
//                            " is offering " + '"' + mProductData.title!! + '"' + " on \n http://tetoota.com"
//                    var url: String = "";
//                    if (mProductData.image != null && mProductData.image_thumb != null) {
//                        url = Utils.getUrl(this.activity!!, mProductData.image, mProductData.image_thumb, false)
//                    } else {
//                        url = Utils.getUrl(this.activity!!, mProductData.image!!)
//                    }
//                    // shareProduct(mProductData.title!!,url)
//                    shareService(postString, url)
                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    val postString: String = personData.first_name + " is offering " + '"' + mProductData?.title!! + '"' + " on http://tetoota.com"
                    var url: String = "";
                    if (mProductData!!.image != null && mProductData!!.image_thumb != null) {
                        url = Utils.getUrl(activity, mProductData!!.image!!, mProductData!!.image_thumb!!, false)
                    } else {
                        url = Utils.getUrl(activity, mProductData!!.image!!)
                    }
//                    shareService(postString, url)
                    var gson = Gson()
                    var jsonString = gson.toJson(mProductData)
                    Log.e("jsonString", jsonString + "");
                    val newDeepLink = buildDeepLink(link + jsonString, postString, Constant.ANDROID_MIN_VERSION)
                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this.activity!!)) {
                        mServicePresenter.markFavorite(this.activity!!, mProductData, mAttributeValue, p1)
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    ReportConversationDialog(context, Constant.DIALOG_LOGIN_FAILURE_ALERT, mProductData.id.toString(),
                            this@WishlistFragment, getString(R.string.str_report_issue)).show()
                }
            }
            true
        }
        popup.show()
    }

    /**
     * Method to Share Product
     * TODO Deep linking
     */


    private fun validateAppCode() {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)
        if (uriPrefix.contains("YOUR_APP")) {
            AlertDialog.Builder(activity)
                    .setTitle("Invalid Configuration")
                    .setMessage("Please set your Dynamic Links domain in app/build.gradle")
                    .setPositiveButton(android.R.string.ok, null)
                    .create().show()
        }
    }

    /**
     * Build a Firebase Dynamic Link.
     * https://firebase.google.com/docs/dynamic-links/android/create#create-a-dynamic-link-from-parameters
     *
     * @param deepLink the deep link your app will open. This link must be a valid URL and use the
     * HTTP or HTTPS scheme.
     * @param minVersion the `versionCode` of the minimum version of your app that can open
     * the deep link. If the installed app is an older version, the user is taken
     * to the Play store to upgrade the app. Pass 0 if you do not
     * require a minimum version.
     * @return a [Uri] representing a properly formed deep link.
     */
    @VisibleForTesting
    fun buildDeepLink(jsonData: String, postString: String, minVersion: Int): Uri {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)

        // Set dynamic link parameters:
        //  * URI prefix (required)
        //  * Android Parameters (required)
        //  * Deep link
        // [START build_dynamic_link]
        val builder = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix(uriPrefix)
                .setAndroidParameters(DynamicLink.AndroidParameters.Builder()
                        .setMinimumVersion(minVersion)
                        .build())
                .setIosParameters(
                        DynamicLink.IosParameters.Builder("com.tetoota")
                                .setAppStoreId("1402450812")
                                .setMinimumVersion("1.2")
                                .build())
                .setLink(Uri.parse(jsonData))

        // Build the dynamic link
        val link = builder
//                .buildDynamicLink()
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                .addOnSuccessListener { result ->
                    // Short link created
                    shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    Log.e("flowchartLink", "" + shortLink)
                    shareService(activity, postString, shortLink.toString())
                }.addOnFailureListener {
                    Log.e("addOnFailureListener", "FAIL")
                }
        shareService(activity, postString, link.toString())

        // [END build_dynamic_link]
        // Return the dynamic link as a URI
        return Uri.parse("")
    }

    private fun shareDeepLink(deepLink: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link")
        intent.putExtra(Intent.EXTRA_TEXT, deepLink)

        startActivity(intent)
    }


    fun shareService(context: Context?, postString: String, deepLink: String) {
        mProgressDialog?.dismiss()
        try {
//            var ref = tv_referralCode.text.toString().trim()
//            var newUrl = inviteFriend_text.replace("@", ref)
//            val text = newUrl + "\n" + deepLink
//            Log.e(InviteFriendsFragment.TAG, ref + "\n\n" + "newUrl" + inviteFriend_text.replace("@", ref) + "\n\n" + text)
//            val urlToShare = StringConstant.app_link
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(context.getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            //  Log.e("imageUri - ","" + imageUri)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, postString + "\n" + deepLink)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            //  shareIntent.type = "image/*"
            // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
//            context?.startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

/*
    fun shareProduct(mTitle: String, mImageUrl: String) {
        val text = mTitle
        val pictureUri = Uri.parse(mImageUrl)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share images..."))
    }
*/*/

    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
        Utils.hideSoftKeyboard(this.activity!!)
        if (message == "report") {
            showProgressDialog(Utils.getText(context, StringConstant.please_wait))
            mHomePresenter.report(this.activity!!, mServiceId, mMesgDesc)
        }
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            activity!!.toast(message)
        } else {
            activity!!.toast(message)
        }
    }

    override fun retryPageLoad() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (cellRow.is_favourite == 0) {
            cellRow.is_favourite = 1
            activity?.toast("Marked Favorite")
        } else {
            cellRow.is_favourite = 0
            activity?.toast("Marked Unfavorite")
        }
        mWishRecyclerAdapter.notifyItemChanged(p1)
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest.create()
        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLocationChanged(location: Location?) {
        currentLatitude = location!!.latitude
        currentLongitude = location.longitude
        val d = SphericalUtil.computeDistanceBetween(
                LatLng(oldLattitude, oldLongitude), LatLng(location.latitude, location.longitude))
        if (d >= 500.00 || oldLattitude == 0.0) {
            oldLattitude = currentLatitude
            oldLongitude = currentLongitude
            Utils.savePreferences(Constant.USER_LOC_LAT, "" + currentLatitude, this.context!!)
            Utils.savePreferences(Constant.USER_LOC_LONG, "" + currentLongitude, this.context!!)
//            getAllDashboardList()
        }
    }

    fun locationEnable(context: Activity, mLocationRequest: LocationRequest, mGoogleApiClient: GoogleApiClient) {
        // Setting API to check the GPS on device is enabled or not
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)

        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            //                final LocationSettingsStates = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                    try {
                        //  getAllDashboardList()
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(
                                context,
                                100)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can
            // initialize location requests here.
            // Location settings are not satisfied. However, we have no way
            // to fix the settings so we won't show the dialog.
        }
    }

    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient?.isConnected!!) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                        this)
                mGoogleApiClient?.disconnect()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient?.isConnected!!) {
                stopLocationUpdates()
            }
        }
        if (activity != null) {
            enableWindowTouch(activity!!)
        }
    }

    private fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this)
    }

    /**
     * Initialize the google api client
     */
    @Synchronized
    private fun buildGoogleApiClient() {
        mLocationRequest = LocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this!!.activity!!)
                .addConnectionCallbacks(this@WishlistFragment)
                .addOnConnectionFailedListener(this@WishlistFragment)
                .addApi(LocationServices.API)
                .build()

    }

    override fun onResume() {
        super.onResume()
        /*if(this!!.isRefresh!!){
            currentPage = 1
            isListLastPage = false
            clearList()
            loadItemsLayout_recyclerView.visibility = View.VISIBLE
            callWishListApi(false)
        }*/
        if (mGoogleApiClient != null) {
            if (!mGoogleApiClient?.isConnected!!) {
                mGoogleApiClient!!.connect()
            }
        }
    }

    fun clearList() {
        mWishRecyclerAdapter.removeAll()
        mWishRecyclerAdapter.notifyDataSetChanged()
    }

    public fun callWishListApi(isLoadMore: Boolean) {
        Log.i(javaClass.name, "=========callWishListApi" + value)
        if (isSearch!!) {
            fetchSearchListData(mQuery!!, currentPage, isLoadMore)
        } else {
            if (value == "haveUserId") {
                fetchWishListData(currentPage, isLoadMore)
            } else {
                fetchAllServiceData(currentPage, isLoadMore)
            }
        }
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            view_list.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    public fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            view_list.visibility = View.GONE
            err_title.visibility = View.GONE
        }
    }

    private fun setMultiLanguageText() {
        tv_add.text = Utils.getText(context, StringConstant.str_addpost)
        filter.text = Utils.getText(context, StringConstant.str_filter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FILTER_REQUEST) {
                isRefresh = true
                refreshData()
                loadItemsLayout_recyclerView.visibility = View.VISIBLE
            } else if (requestCode == EDITSERVICEPRODUCT_REQUEST) {
                refreshData()
            }
        } else {
            resetFilterData()
        }
    }

    private fun resetFilterData() {
        Utils.savePreferences(Constant.CATEGORY_ID, "", this.context!!)
        Utils.savePreferences(Constant.SUBCATEGORIES_ID, "", this.context!!)
        Utils.savePreferences(Constant.TOPRATED, "no", this.context!!)
        Utils.savePreferences(Constant.CITY, "", this.context!!)
    }

    public fun refreshData() {
        try {
            currentPage = 1
            isListLastPage = false
            clearList()
            callWishListApi(false)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
