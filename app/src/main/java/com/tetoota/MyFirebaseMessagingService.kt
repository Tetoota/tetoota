package com.tetoota

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import android.text.TextUtils
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
//import androidx.work.OneTimeWorkRequest
//import androidx.work.WorkManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.login.LoginDataResponse
//import com.google.firebase.quickstart.fcm.R
import com.tetoota.main.MainActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.inbox.InboxFragment

class MyFirebaseMessagingService : FirebaseMessagingService(), ProfileDetailContract.View {
    var type: String = ""
    var title: String = ""
    var postId: Int = 0
    var proposalFrom: Int = 0
    var proposalTo: Int = 0
    var chatType: String = ""
    override fun onProfileSendEmailApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileSendEmailApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        val profileData: ProfileDataResponse = message?.get(0)!!
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MyFirebaseMessagingService)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        personData.tetoota_points = profileData.tetoota_points
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@MyFirebaseMessagingService)
    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
    }

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@MyFirebaseMessagingService)
    }
    private val TAG = MyFirebaseMessagingServiceDepricated::class.java.simpleName
    private var notificationUtils: NotificationUtils? = null
    private var tetootaApplication: TetootaApplication? = null
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e(TAG, "From: ${remoteMessage?.from}")

//        // Check if message contains a data payload.
//        remoteMessage?.data?.isNotEmpty()?.let {
//            Log.d(TAG, "Message data payload: " + remoteMessage.data)
//
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use WorkManager.
//                scheduleJob()
//            } else {
//                // Handle message within 10 seconds
//                handleNow()
//            }
//        }
//
//        // Check if message contains a notification payload.
//        remoteMessage?.notification?.let {
//            Log.d(TAG, "Message Notification Body: ${it.body}")
//        }
        Log.e(TAG, "From: " + remoteMessage!!.from)
//        if (remoteMessage == null)
//            return

        // Check if message contains a pushNotificationDataResponse payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.e(TAG, "PushNotificationDataResponse Payload: " + remoteMessage.data.toString())
            try {
//                val json = JSONObject(remoteMessage.data)
                title = remoteMessage.data.get("title").toString()
                var body: String = remoteMessage.data.get("body").toString()
                if (remoteMessage.data.containsKey("type"))
                    type = remoteMessage.data.get("type").toString()
                if (remoteMessage.data.containsKey("post_id"))
                    postId = remoteMessage.data.get("post_id")!!.toInt()
                if (remoteMessage.data.containsKey("proposal_from"))
                    proposalFrom = remoteMessage.data.get("proposal_from")!!.toInt()
                if (remoteMessage.data.containsKey("proposal_to"))
                    proposalTo = remoteMessage.data.get("proposal_to")!!.toInt()
                if (remoteMessage.data.containsKey("chat_type"))
                    chatType = remoteMessage.data.get("chat_type")!!.toString()
                if (!TextUtils.isEmpty(type) || !TextUtils.isEmpty(chatType)) {
                    Log.e("===============updateUi", "" + chatType)
                    updateUi()
                }
                sendNotification(title, body)

                Log.e(TAG, "$title $body")
//                showNotification(title,body)
//                val resultIntent = Intent(applicationContext, MainActivity::class.java)
////            val resultIntent = Intent(applicationContext, InboxFragment::class.java)
//                resultIntent.putExtra("notification", true)//                resultIntent.putExtra("message", body)
//
//                showNotificationMessage(applicationContext, title, body, resultIntent)
//                handleDataMessage(json)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }
        }
        // Check if message contains a notification payload.
        else if (remoteMessage.notification != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.notification!!.body!!)
        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private fun scheduleJob() {
        // [START dispatch_job]
//        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
//        WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    private fun updateUi() {
        val intent = Intent("updateChat")
        intent.putExtra(Constant.PROPOSAL_FROM, proposalFrom)
        intent.putExtra(Constant.PROPOSAL_TO, proposalTo)
        intent.putExtra(Constant.POST_ID, postId)
        intent.putExtra(Constant.CHAT_TYPE, chatType)
        this.sendBroadcast(intent)
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        val refreshedToken = token
        val refreshedId = FirebaseInstanceId.getInstance().id
        println("Refreshed Token $refreshedToken")
        println("Refreshed refreshedId $refreshedId")
        Utils.setFcmToken(refreshedToken, this)
        val intent = Intent("tokenReceiver")
        val broadcastManager = LocalBroadcastManager.getInstance(this)
        intent.action = "fcmToken"
        intent.putExtra("token", refreshedToken)
        Log.e("FCMTOKEN", refreshedToken.toString());
        broadcastManager.sendBroadcast(intent)

    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(title: String, messageBody: String) {
        var intent: Intent? = null
        Log.i(javaClass.name, "=====================sendNotification  $chatType   $type")
        intent = Intent(this, MainActivity::class.java)
        if (!TextUtils.isEmpty(type) || !TextUtils.isEmpty(chatType))
            intent.putExtra("notification", true)
        if (!TextUtils.isEmpty(chatType)) {
            intent.putExtra(Constant.PROPOSAL_FROM, proposalFrom)
            intent.putExtra(Constant.PROPOSAL_TO, proposalTo)
            intent.putExtra(Constant.POST_ID, postId)
            intent.putExtra(Constant.CHAT_TYPE, chatType)
        }
        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val bigTextStyle = NotificationCompat.BigTextStyle()

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "tetoota",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())

    }

    companion object {

        private const val TAG = "MyFirebaseMsgService"
    }
}