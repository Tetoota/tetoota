package com.tetoota.main
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.layout_push_notification.*
/**
 * Created by charchit.kasliwal on 23-10-2017.
 */
class PushNotificationSetttings : BaseActivity(), View.OnClickListener {
    private var isAllPushNotification: Boolean = true
    private var isChatNotification : Boolean = true
    private var isProposalNotification : Boolean = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_push_notification)
        initViews()
    }

    override fun onClick(p0: View?) {
        when(p0){
            iv_toggle -> {
               if(isAllPushNotification){
                    isAllPushNotification = false
                   iv_toggle.setImageResource(R.drawable.tv_toggle_off)
               }else{
                   isAllPushNotification = true
                   iv_toggle.setImageResource(R.drawable.tv_toggle_on)
               }
            }
            chat_toggle -> {
                if(isChatNotification){
                    isChatNotification = false
                    chat_toggle.setImageResource(R.drawable.tv_toggle_off)
                }else{
                    isChatNotification = true
                    chat_toggle.setImageResource(R.drawable.tv_toggle_on)
                }
            }
            proposal_toggle -> {
                if(isProposalNotification){
                    isProposalNotification = false
                    proposal_toggle.setImageResource(R.drawable.tv_toggle_off)
                }else{
                    isProposalNotification = true
                    proposal_toggle.setImageResource(R.drawable.tv_toggle_on)
                }
            }
        }
    }

    private fun initViews() {
        iv_toggle.setOnClickListener(this)
        chat_toggle.setOnClickListener(this)
        proposal_toggle.setOnClickListener(this)
        iv_toggle.setImageResource(R.drawable.tv_toggle_on)
        proposal_toggle.setImageResource(R.drawable.tv_toggle_on)
        chat_toggle.setImageResource(R.drawable.tv_toggle_on)
        setMultiLanguageText()
    }

    private fun setMultiLanguageText() {
        tv_chatpush.text = Utils.getText(this,StringConstant.tv_chatpush)
        tv_proposal_push.text = Utils.getText(this,StringConstant.tv_proposal_push)
        tv_allpush.text = Utils.getText(this,StringConstant.tv_allpush)
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, PushNotificationSetttings::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@PushNotificationSetttings)
        finish()
    }
}