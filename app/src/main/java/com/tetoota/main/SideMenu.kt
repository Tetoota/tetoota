package com.tetoota.main

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by charchit.kasliwal on 02-06-2017.
 */
class SideMenu : Parcelable {
     var name: String? = null
     var isSelected: Boolean = true
     var drawable: Int = 0

    constructor(name: String?, isSelected: Boolean, drawable: Int) {
        this.name = name
        this.isSelected = isSelected
        this.drawable = drawable
    }
    constructor(source: Parcel){
        this.name = source.readString()
        this.isSelected = source.readByte().toInt() != 0
        this.drawable = source.readInt()
    }
    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(this.name)
        dest.writeByte(if (this.isSelected) 1.toByte() else 0.toByte())
        dest.writeInt(this.drawable)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<SideMenu> = object : Parcelable.Creator<SideMenu> {
            override fun createFromParcel(source: Parcel): SideMenu = SideMenu(source)
            override fun newArray(size: Int): Array<SideMenu?> = arrayOfNulls(size)
        }
    }
}