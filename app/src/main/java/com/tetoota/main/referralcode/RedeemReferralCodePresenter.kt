package com.tetoota.main.referralcode

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class RedeemReferralCodePresenter : RedeemReferralCodeContract.Presenter, RedeemReferralCodeContract.RedeemReferralCodeApiResult {

    var mFavoriteView: RedeemReferralCodeContract.View

    val mFavoriteInteractor: RedeemReferralCodeInteractor by lazy {
        RedeemReferralCodeInteractor(this)
    }

    constructor(mFavoriteView: RedeemReferralCodeContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun applyRedeemReferralCodeCode(mActivity: Activity, mReferralCode: String) {
        mFavoriteInteractor.appReferralCode(mActivity, mReferralCode)
    }

    override fun onRedeemReferralCodeApiSuccess(mString: String, mReferralCode: String) {
        mFavoriteView.onRedeemReferralCodeSuccess(mString, mReferralCode)
    }

    override fun onRedeemReferralCodeApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onRedeemReferralCodeFailureResponse(mString, isServerError)
    }
}