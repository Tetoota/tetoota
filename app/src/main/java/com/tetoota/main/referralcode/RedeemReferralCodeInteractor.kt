package com.tetoota.main.referralcode

import android.app.Activity
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class RedeemReferralCodeInteractor {
    var call: Call<RedeemReferralCodeResponse>? = null
    var mInviteFriendsListener: RedeemReferralCodeContract.RedeemReferralCodeApiResult

    constructor(mFavoriteListener: RedeemReferralCodeContract.RedeemReferralCodeApiResult) {
        this.mInviteFriendsListener = mFavoriteListener
    }

    fun appReferralCode(mActivity: Activity, mReferralCode: String) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        call = TetootaApplication.getHeader()
                .applyReferralCode(
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        personData.user_id!!,
                        mReferralCode)
        call!!.enqueue(object : Callback<RedeemReferralCodeResponse> {
            override fun onResponse(call: Call<RedeemReferralCodeResponse>?,
                                    response: Response<RedeemReferralCodeResponse>?) {
                var mRedeemCouponsData: RedeemReferralCodeResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.meta?.status!!) {
                        if (mRedeemCouponsData != null) {
                            mInviteFriendsListener.onRedeemReferralCodeApiSuccess("success", mRedeemCouponsData.meta!!.message as String)
                        }
                    } else {
                        mInviteFriendsListener.onRedeemReferralCodeApiFailure(response?.body()?.meta?.message.toString(), false)
                    }
                } else {
                    if (response?.body() == null) {
                        try {
                            val jObjError = JSONObject(response?.errorBody()?.string())
                            val achualdata: JSONObject = jObjError.getJSONObject("meta")
                            val gson = Gson()
                            var mError: Meta = Meta()
                            val error = gson.fromJson(achualdata.toString(), Meta::class.java)
                            mInviteFriendsListener.onRedeemReferralCodeApiFailure(error.message!!, true)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        /* var mRedeemCouponsData : ResponseBody? = response?.errorBody()
                         val gson = GsonBuilder().create()
                         var mError = Meta()
                         try {
                           var  mError: String? = mRedeemCouponsData.
                             Log.d("nik",mError.toString());
                         } catch (e: IOException) {
                             // handle failure to read error
                             e.printStackTrace();
                         }*/
                    }
                }
            }

            override fun onFailure(call: Call<RedeemReferralCodeResponse>?, t: Throwable?) {
                mInviteFriendsListener.onRedeemReferralCodeApiFailure(t?.message.toString(), true)
            }
        })
    }
}