package com.tetoota.main.referralcode

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class RedeemReferralCodeContract {
    interface View {
        fun onRedeemReferralCodeSuccess(mString: String, mFavoriteList: String): Unit
        fun onRedeemReferralCodeFailureResponse(mString: String, isServerError: Boolean): Unit
    }

    interface Presenter {
        fun applyRedeemReferralCodeCode(mActivity: Activity, mReferralCode : String)
    }

    interface RedeemReferralCodeApiResult {
        fun onRedeemReferralCodeApiSuccess(mString: String, mReferralCode: String): Unit
        fun onRedeemReferralCodeApiFailure(mString: String, isServerError: Boolean): Unit
    }
}