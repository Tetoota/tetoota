package com.tetoota.main

//import com.crashlytics.android.Crashlytics

//import io.fabric.sdk.android.Fabric
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.NotificationManager
import android.app.ProgressDialog
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.cropImage.GPSTracker
import com.tetoota.cropImage.Utility
import com.tetoota.customviews.CustomSignOutDialog
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.fragment.DashboardFragment
import com.tetoota.fragment.corporateLogin.CorporateLoginFragment
import com.tetoota.fragment.couponcode.Meta
import com.tetoota.fragment.dashboard.HomeContract
import com.tetoota.fragment.dashboard.HomePresenter
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.favorites.FavoritesFragment
import com.tetoota.fragment.help.HelpFragment
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.inbox.fragment.InboxFragment
import com.tetoota.fragment.profile.*
import com.tetoota.fragment.scanandpay.ScanQrCode
import com.tetoota.fragment.settings.SettingsFragment
import com.tetoota.listener.IFragmentOpenCloseListener
import com.tetoota.login.LoginDataResponse
import com.tetoota.logout.LogoutDataResponse
import com.tetoota.main.referralcode.RedeemReferralCodeContract
import com.tetoota.main.referralcode.RedeemReferralCodePresenter
import com.tetoota.message.ChatTabLayout
import com.tetoota.pointssummary.PointsSummaryActivity
import com.tetoota.potluck.PotluckFoodFragment
import com.tetoota.selectlanguage.SelectLanguageActivity
import com.tetoota.service_product.WishlistFragment
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import io.intercom.android.sdk.UnreadConversationCountListener
import io.intercom.android.sdk.UserAttributes
import io.intercom.android.sdk.identity.Registration
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.home_toolbar_layout.*
import kotlinx.android.synthetic.main.home_toolbar_layout.view.*
import kotlinx.android.synthetic.main.nav_header_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.URLDecoder


class MainActivity : BaseActivity(), HomeContract.View, CustomSignOutDialog.IDialogListener,
        ProfileCompletionDialog.IDialogListener, ProfileDetailActivity.profilePicUpdate,
        IFragmentOpenCloseListener, SettingsFragment.OnFragmentInteraction, ProfileDetailContract.View, RedeemReferralCodeContract.View {
    override fun onProfileSendEmailApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@MainActivity)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@MainActivity)
    }

    var gps: GPSTracker? = null
    var mContext: Context? = null
    var isProfileUpdate: Boolean = false
    private val ADDPOST_REQUEST: Int = 14
    private val TAG_HOME = "dashboard"
    private val TAG_PROFILE = "profile"
    private val TAG_INBOX = "inbox"
    private val TAG_INVITE_FRIENDS = "invite_friends"
    private val TAG_EARN_TETOOTA_POINTS = "earn_tetoota_points"
    private val TAG_FAVORITES = "favorites"
    private val TAG_QRCODE = "str_scan_pay "
    private val TAG_REDEEM_COUPONS = "redeem_coupons"
    private val TAG_wishlist = "wishlist"
    private val TAG_SETTINGS = "settings"
    private val TAG_FOOD = "food"
    private val TAG_HELP = "help"
    private val TAG_CORPORATE_LOGIN = "corporate_login"
    private val TAG_LOGOUT = "logout"
    var CURRENT_TAG = TAG_HOME
    private var isBackButtonEnabled: Boolean = false
    private var inboxFrag: ChatTabLayout? = null
    var navItemIndex = 0
    var i: Int = 0
    val mHandler = Handler()
    // toolbar titles respected to selected nav menu item
    private var activityTitles: Array<String>? = null
    private lateinit var mMenuArrayList: ArrayList<SideMenu>
    lateinit var sliderCustomAdapter: SliderCustomAdapter
    private lateinit var mRegistrationBroadcastReceiver: BroadcastReceiver
    var backFragmentOn = ""
    var dashboardFragment = DashboardFragment()
    private var tetootaApplication: TetootaApplication? = null
    //  private lateinit var mReferrerClient: InstallReferrerClient
    //    val textViewHandler = Handler()
    //    private lateinit var mFirebaseAnalytics: FirebaseAnalytics
    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        setContentView(R.layout.activity_main)


        mContext = this
        tetootaApplication = applicationContext as TetootaApplication
        latLong()

        TetootaApplication.setTextValuesHashMap(Utils.getLangByCode(this,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", this)))


        //TOdo : jitu changes
        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)

        getCountData()
        getAllInvalidText()
        latLong()
        initViews()

//        Fabric.with(this, Crashlytics());

        //  val mixpanel = MixpanelAPI.getInstance(this, Constant.MIXPANEL_TOKEN)

//        // Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
//        sendGoogleAnalytics()
        setUpNavigationMenu()

        tetootaApplication!!.userMacAddress = Utility.getMacAddr(true)
        // Log.e("userMacAddress ", "" + tetootaApplication!!.userMacAddress)
//        getKeyHash()
        setUpNavigationMenu()

        tetootaApplication!!.userMacAddress = Utility.getMacAddr(true)

        /* toolbar.setNavigationOnClickListener(View.OnClickListener {
             println("Item Clicked")
         })*/

/*
        if (intent.getBooleanExtra("success",true)) {
            Log.e("iffff","")
            menuSelection(7)
            navItemIndex = 7
            CURRENT_TAG = TAG_SETTINGS
            loadFragment()
            val fragment = SettingsFragment()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frame, fragment)
            transaction.commit()
            return
        }
*/
        Log.i(javaClass.name, "==========================" + intent?.getBooleanExtra("notification", false)!!)
        if (intent?.getBooleanExtra("notification", false)!!) {
            Log.i("onNewIntent", "Notification onCreate")
            menuSelection(2)
            navItemIndex = 2
            CURRENT_TAG = TAG_INBOX
            loadFragment()
            if (intent?.getStringExtra("message") == "Message Alert") {
                Log.i("onNewIntent", "Notification")
            }
        } else if (savedInstanceState == null) {
            navItemIndex = 0
            CURRENT_TAG = TAG_HOME
            loadFragment()
        }
        lst_menu_items.onItemClickListener = navBarListListner

        btn_points.onClick {
            if (Utils.haveNetworkConnection(this)) {
                val intent = PointsSummaryActivity.newMainIntent(this)
                ActivityStack.getInstance(this)
                startActivity(intent)
            } else {
                showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
            }
        }

        mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, intent: Intent) {
                println("PushNotificationDataResponse Received")
                if (intent.getStringExtra("messageAlert") == "Message Alert") {
                    println("Message Push Received")
                    if (inboxFrag != null) {
                        var inbox = InboxFragment()
                        inbox!!.getChatHistory()
                    }
                } else {
                    if (sliderCustomAdapter != null) {
                        sliderCustomAdapter = SliderCustomAdapter(this@MainActivity, genericSideMenu() as ArrayList<SideMenu>)
                        lst_menu_items.adapter = sliderCustomAdapter
                    }
                }
            }
        }


/*        mReferrerClient = InstallReferrerClient.newBuilder(this).build()
        mReferrerClient.startConnection(object : InstallReferrerStateListener {

            override fun onInstallReferrerSetupFinished(responseCode: Int) {
                when (responseCode) {
                    InstallReferrerClient.InstallReferrerResponse.OK -> {
                        // Connection established
                        Log.e("startConnection ","OK " )
                    }
                    InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED -> {
                        // API not available on the current Play Store app
                        Log.e("startConnection ","FEATURE_NOT_SUPPORTED " )

                    }
                    InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE -> {
                        // Connection could not be established
                        Log.e("startConnection ","SERVICE_UNAVAILABLE " )

                    }
                }
            }

            override fun onInstallReferrerServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Log.e("startConnection ","onInstallReferrerServiceDisconnected " )

            }
        })
        val response: ReferrerDetails
        response = mReferrerClient.installReferrer
        response.installReferrer
        response.referrerClickTimestampSeconds
        response.installBeginTimestampSeconds

        Log.e("response.installRef ","***** " + response.installReferrer )*/

        // [START get_deep_link]
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(intent)
                .addOnSuccessListener(this) { pendingDynamicLinkData ->
                    // Get deep link from result (may be null if no link is found)
                    var deepLink: Uri? = null
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.link
                    }

                    // Handle the deep link. For example, open the linked
                    // content, or apply promotional credit to the user's
                    // account.
                    // ...

                    // [START_EXCLUDE]
                    // Display deep link in the UI
                    if (deepLink != null) {
//                        Snackbar.make(findViewById(android.R.id.content),
//                                "Found deep link!" + deepLink.toString(), Snackbar.LENGTH_LONG).show()
                        Log.e("deepLink!=null", deepLink.toString())

                        if (deepLink.toString().contains("referral")) {
                            val code = deepLink.toString().substringAfterLast('/')
                            getRedeemCouponsCodeApiCall(code.toString())
                        } else if (deepLink.toString().contains("service")) {
                            val code = deepLink.toString().substringAfterLast('?')
                            val json = URLDecoder.decode(code, "UTF-8")
                            Log.e("substringAfterLast", json.toString())
                            Log.e("jsonLink", json.toString())

                            val data: ServicesDataResponse = Gson().fromJson(json.toString(), ServicesDataResponse::class.java)
                            Log.e("data", data.toString())
                            val intent = Intent(this, ProductDetailActivity::class.java)
                            intent.putExtra("mProductData", data)
                            intent.putExtra("productType", "services")
                            startActivity(intent)
                        }
                    } else {
                        Log.d("TAG", "getDynamicLink: no link found")
                    }
                    // [END_EXCLUDE]
                }
                .addOnFailureListener(this) { e -> Log.w("TAG", "getDynamicLink:onFailure", e) }
        // [END get_deep_link]

    }

    private fun initViews() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        toolbar.navigationIcon = Utils.getDrawable(this@MainActivity, R.drawable.iv_sidemenu)
        mProgressDialog = ProgressDialog(this@MainActivity, R.style.newDialog)
        ll_headerlayout.setOnClickListener(View.OnClickListener {
            ProfileCompletionDialog(this@MainActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                    this@MainActivity, getString(R.string.err_msg_mobile_number_limit)).show()
        })
        try {
            val pInfo = packageManager.getPackageInfo(packageName, 0)
            val version = pInfo.versionName
            versionCode.text = "Version " + version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        sliderCustomAdapter = SliderCustomAdapter(this, genericSideMenu() as ArrayList<SideMenu>)
        lst_menu_items.adapter = sliderCustomAdapter
        activityTitles = resources.getStringArray(R.array.header_titles)



        toolbar.bell_icon.setOnClickListener(View.OnClickListener
        {
            menuSelection(2)
            navItemIndex = 2
            CURRENT_TAG = TAG_INBOX
            loadFragment()
        }
        )
    }

    override fun onRedeemReferralCodeSuccess(mString: String, mResponseMessage: String) {
//        progress_view.visibility = View.GONE
        Log.e("BACCC", "https://stackoverflow.com/questions/29442216/how-to-disable-or-enable-viewpager-swiping-in-android")
        openDailog("Success", mResponseMessage)
        callUserDetailAPI()
    }

    override fun onRedeemReferralCodeFailureResponse(mString: String, isServerError: Boolean) {
//        progress_view.visibility = View.GONE
        Log.e("BACCC", "FAIL")
        openDailog("Failure", mString)
    }

    val mRedeemReferralCodePresenter: RedeemReferralCodePresenter by lazy {
        RedeemReferralCodePresenter(this)
    }

    private fun openDailog(header: String, mResponseMsg: String) {
        val alertDialog = AlertDialog.Builder(this).create() //Read Update
//        alertDialog.setTitle(header)
        alertDialog.setMessage(mResponseMsg)
        alertDialog.setButton("Ok", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                alertDialog.dismiss()
            }
        })
        alertDialog.show()  //<-- See This!
    }

    fun getRedeemCouponsCodeApiCall(code: String) {
        Utils.hideSoftKeyboard(this)
        if (Utils.haveNetworkConnection(this)) {
//            progress_view.visibility = View.VISIBLE
            Log.e("co_code", code.toString())
            mRedeemReferralCodePresenter.applyRedeemReferralCodeCode(this, code)
        } else {
            this.toast(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    private fun setDataOnUI() {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)

        println("Points Deduction ${personData.tetoota_points}")
        if (java.lang.Long.valueOf(personData.tetoota_points.toString()) < 0) {
            tv_tetoota_point.text = "0"
        } else {
            tv_tetoota_point.text = personData.tetoota_points
        }
        if (personData.first_name != "") {
            // Log.e("first_nameeeeeeeeee ",""+ personData.first_name)
            // Log.e("last_nameeeeeeeeee ",""+ personData.last_name)

            tv_name.text = personData.first_name + " " + personData.last_name
        } else {
            tv_name.text = "Welcome User"
        }
        if (personData.designation != "") {
            tv_designation.text = personData.designation
        }
        if (personData.complete_percentage != null || personData.complete_percentage != "") {
            personData.complete_percentage?.toFloat()!! / 100
            holoCircularProgressBar.progress = personData.complete_percentage.toFloat() / 100
        } else {
            holoCircularProgressBar.progress = 0f
        }

        try {
            val proposalCount: Int = personData.proposal_count!!.toInt()
            Utils.saveInt(Constant.USER_PROPOSAL_COUNT.toString(), proposalCount)

            /********************notification count******************/

            if (proposalCount != 0) {
                toolbar.notification_count.visibility = View.VISIBLE
                toolbar.notification_count.text = proposalCount.toString()
                // toolbar.rl_message.visibility = View.VISIBLE
                // toolbar.message_count.text = proposalCount.toString()

            } else {
                //  toolbar.rl_message.visibility = View.GONE
                toolbar.notification_count.visibility = View.GONE
            }

            if (sliderCustomAdapter != null) {
                sliderCustomAdapter.notifyDataSetChanged()
            }
        } catch (e: NumberFormatException) {
        } catch (e: KotlinNullPointerException) {
        } catch (e: Exception) {
        }

        if (personData.profile_image!!.isEmpty()) {
            iv_user.setImageResource(R.drawable.user_placeholder);
        } else {

            Picasso.get().load(personData.profile_image).into(iv_user)
        }
/*
        Glide.with(this@MainActivity)
                .load(Utils.getUrl(this@MainActivity, personData.profile_image!!))
                .placeholder(R.drawable.user_placeholder)
                .error(R.drawable.user_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .centerCrop()
                .dontAnimate()
                .into(iv_user)
*/

        try {
            val registration = Registration.create().withUserId(personData.user_id.toString())
            Intercom.client().registerIdentifiedUser(registration)

            val userAttributes = UserAttributes.Builder()
                    .withName(personData.first_name + " " + personData.last_name)
                    .withPhone(personData.user_mobile_number.toString())
                    .withUserId(personData.user_id)
                    .build()
            Intercom.client().updateUser(userAttributes)
        } finally {

        }

    }


    override fun onResume() {
        super.onResume()

        setDataOnUI()
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                IntentFilter(Constant.PUSH_NOTIFICATION))
        getCountData()

        var key = intent.getStringExtra("KEY");
        if (key != null && key.equals("HELP")) {
//            Log.e("asdadas","asdasd")
//            HelpFragment.newInstance("", "")
            navItemIndex = 7

        }
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver)
        super.onPause()
    }

    /*******************************start onBackPress code***************************/

    var doubleBackToExitPressedOnce = false

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawers()
            return
        }
        val fragmentCount = supportFragmentManager.backStackEntryCount
        if (fragmentCount == 0) {

            Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)

            if (navItemIndex != 0) {
                menuSelection(0)
                navItemIndex = 0
                CURRENT_TAG = TAG_HOME
                loadFragment()
                return
            }
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
            } else {
                this.doubleBackToExitPressedOnce = true

                /* val toast = Toast.makeText(this, " Please click BACK again to exit ", Toast.LENGTH_SHORT)
                   val view = toast.view
                   view.background.setColorFilter(resources.getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_IN)
                   val text = view.findViewById<TextView>(android.R.id.message)
                   text.setTextColor(resources.getColor(android.R.color.white))
                   toast.show()*/

                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
                Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
            }

        } else {
            if (backFragmentOn.equals(activityTitles?.get(1))) {
                isBackButtonEnabled = false
                toolbar.navigationIcon = resources.getDrawable(R.drawable.iv_sidemenu)
                toolbar_title.text = headerTitle(1)
                supportFragmentManager.popBackStack()
                return
            } else if (backFragmentOn.equals(activityTitles?.get(4))) {
                isBackButtonEnabled = false
                toolbar.navigationIcon = resources.getDrawable(R.drawable.iv_sidemenu)
                toolbar_title.text = headerTitle(4)
                supportFragmentManager.popBackStack()
                return
            }
            // return

        }
        backFragmentOn = ""
        // super.onBackPressed();

    }

    /* override fun onBackPressed() {
         if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
             drawer_layout.closeDrawers()
             return
         }
         val fragmentCount = supportFragmentManager.backStackEntryCount
         if (fragmentCount == 0) {

             if (navItemIndex != 0) {
                 menuSelection(0)
                 navItemIndex = 0
                 CURRENT_TAG = TAG_HOME
                 loadFragment()
                 return
             }


           /*  if (doubleBackToExitPressedOnce) {
                 super.onBackPressed()
             } else {
                 this.doubleBackToExitPressedOnce = true
                 Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
                 Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 1500)
             }*/
         } else {
             if (backFragmentOn.equals(activityTitles?.get(1))) {
                 isBackButtonEnabled = false
                 toolbar.navigationIcon = resources.getDrawable(R.drawable.iv_sidemenu)
                 toolbar_title.text = headerTitle(1)
                 supportFragmentManager.popBackStack()
                 return
             } else if (backFragmentOn.equals(activityTitles?.get(4))) {
                 isBackButtonEnabled = false
                 toolbar.navigationIcon = resources.getDrawable(R.drawable.iv_sidemenu)
                 toolbar_title.text = headerTitle(4)
                 supportFragmentManager.popBackStack()
                 return
             }
             // return;

         }
         backFragmentOn = ""
         super.onBackPressed();

     }*/


    /* override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawers()
            return
        }
        var fragmentCount: Int = supportFragmentManager.backStackEntryCount
        if (fragmentCount > 0) {
            if (backFragmentOn.equals(activityTitles?.get(1))) {
                isBackButtonEnabled = false
                toolbar.navigationIcon = resources.getDrawable(R.drawable.iv_sidemenu)
                toolbar_title.text = headerTitle(1)
                supportFragmentManager.popBackStack()
                return
            } else if (backFragmentOn.equals(activityTitles?.get(4))) {
                isBackButtonEnabled = false
                toolbar.navigationIcon = resources.getDrawable(R.drawable.iv_sidemenu)
                toolbar_title.text = headerTitle(4)
                supportFragmentManager.popBackStack()
                return
            }
        } else {

            if (navItemIndex != 0) {
                menuSelection(0)
                navItemIndex = 0
                CURRENT_TAG = TAG_HOME
                loadFragment()
                return
            }
        }
        backFragmentOn = ""
        super.onBackPressed()
    }*/


    /**
     * Method
     */
    private fun setUpNavigationMenu(): Unit {
        val actionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            override fun onDrawerClosed(drawerView: View) {
                // Code here will be triggered once the mDrawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView)
                // val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                //inputMethodManager.hideSoftInputFromWindow(currentFocus.windowToken, 0)
            }

            override fun onDrawerOpened(drawerView: View) {
                try {
                    Utils.hideSoftKeyboard(this@MainActivity)
//                    if (isProfileUpdate) { //jitu
                    isProfileUpdate = false
//                    setDataOnUI()
                    callUserDetailAPI()
//                    } //jitu
                } catch (xc: Exception) {
                    xc.printStackTrace()
                }
                // Code here will be triggered once the mDrawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView)
            }
        }
        drawer_layout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.isDrawerIndicatorEnabled = false
        actionBarDrawerToggle.setToolbarNavigationClickListener {
            Utils.hideSoftKeyboard(this@MainActivity)
            if (isBackButtonEnabled) {
                onBackPressed()
            } else {
                println("Drawer Toggler")
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }
        // Calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState()
    }

    /**
     * Menu Selection
     */
    fun menuSelection(mPosition: Int): Unit {
        for (i in mMenuArrayList.indices) {
            mMenuArrayList[i].isSelected = i == mPosition
        }
        sliderCustomAdapter.notifyDataSetChanged()
//        lst_menu_items.adapter = sliderCustomAdapter
    }

    /**
     * On Create Options Menu
     */
    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }*/

    /**
     *  On Options Menu Item
     */
    /* override fun onOptionsItemSelected(item: MenuItem?): Boolean {
         when (item?.itemId) {
             android.R.id.home -> {

             }
         // android.R.id.
         }
         return super.onOptionsItemSelected(item)
     }*/

    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        fragmentTransaction.func()
        fragmentTransaction.commitAllowingStateLoss()
    }

    private fun loadFragment() {
        // set toolbar title
        setToolbarTitle()
        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (supportFragmentManager.findFragmentByTag(CURRENT_TAG) != null) {
            drawer_layout.closeDrawers()
            // show or hide the fab button
            // Log.e("loadFragment ", "rr ")


            return
        }
        val mPendingRunnable = Runnable {
            // update the main content by replacing fragments
            Handler().postDelayed(Runnable {
                var fragment = navigateOnFragment()
                Log.e("8888888==", "" + intent?.getBooleanExtra("notification", false))
                if (intent?.getBooleanExtra("notification", false)!! && CURRENT_TAG == TAG_INBOX) {
                    var bundle = Bundle()
                    bundle.putInt(Constant.PROPOSAL_FROM, intent.getIntExtra(Constant.PROPOSAL_FROM, 0))
                    bundle.putInt(Constant.PROPOSAL_TO, intent.getIntExtra(Constant.PROPOSAL_TO, 0))
                    if (intent.hasExtra(Constant.POST_ID))
                        bundle.putInt(Constant.POST_ID, intent.getIntExtra(Constant.POST_ID, 0))
                    bundle.putString(Constant.CHAT_TYPE, intent.getStringExtra(Constant.CHAT_TYPE))
//                bundle.putBundle(Constant.CHAT_TYPE, intent.extras)
                    fragment.arguments = bundle
                }
                supportFragmentManager.inTransaction { replace(R.id.frame, fragment) }
                intent.putExtra("notification", false)

            }, 100)
        }
        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable)
        }
        // show or hide the fab button
        //Closing drawer on item click
        drawer_layout.closeDrawers()
        // refresh toolbar menu
        invalidateOptionsMenu()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.i(javaClass.name, "==========================" + intent?.getBooleanExtra("notification", false)!!)
        if (intent?.getBooleanExtra("notification", false)!!) {
            Log.i("onNewIntent", "Notification")
            menuSelection(2)
            navItemIndex = 2
            CURRENT_TAG = TAG_INBOX
            loadFragment()
            if (intent.getStringExtra("message") == "Message Alert") {
                Log.i("onNewIntent", "Notification")
            }
        }
    }

    private fun setToolbarTitle() {
        if (navItemIndex == 0) {
            toolbar_title.text = "Tetoota"
        } else {
            toolbar_title.text = headerTitle(navItemIndex)
        }
    }

    /**
     * @return it will
     */


    private fun navigateOnFragment(): Fragment {
        when (navItemIndex) {
            0 -> {
                dashboardFragment = DashboardFragment.newInstance("", "")
                toolbar.rl_bell.visibility = View.VISIBLE
                latLong()
                getCountData()
                inboxFrag = null
                return dashboardFragment
            }
            // Home fragment
            1 ->
                // Profile Fragment

                return ProfileFragment.newInstance("", "", this@MainActivity)
            2 -> {
                inboxFrag = ChatTabLayout()
                toolbar.rl_bell.visibility = View.GONE
                // toolbar.rl_message.visibility = View.GONE
                Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
                return inboxFrag as ChatTabLayout
            }
            3 ->
                // WishlistFragment
                return (WishlistFragment.newInstance("","yes", false))
            4 ->
                // Favorites Fragment
                return FavoritesFragment.newInstance("key", "mainActivity", this@MainActivity)
            5 -> {

                return ScanQrCode()
            }
            6 -> {
                return PotluckFoodFragment()
            }
            7 -> {

                var settingFrag: Fragment = SettingsFragment().newInstance()
                return settingFrag
            }
            // Settings Fragment
            8 -> {
                val companyId = Utils.loadPrefrence(Constant.COMPANY_ID, "0", mContext)
                if (companyId.equals("0")) {
                    return CorporateLoginFragment().newInstance()
                } else {
                    return HelpFragment.newInstance("", "")
                }

            }
            9 ->
                return HelpFragment.newInstance("", "")

            else -> return DashboardFragment()
        }
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    /**
     * Navigation bar listener and switch fragment according to its position
     */
    private val navBarListListner = AdapterView.OnItemClickListener { parent, view, position, id ->
        menuSelection(position)
        when (position) {
            0 -> {
                navItemIndex = 0
                //  Log.e("Main Activity count", "" + tetootaApplication!!.count)

                if (tetootaApplication!!.count != 0) {
                    toolbar.notification_count.visibility = View.VISIBLE
                    toolbar.notification_count.text = tetootaApplication!!.count.toString()

                } else {
                    toolbar.notification_count.visibility = View.GONE
                }

                CURRENT_TAG = TAG_HOME
                loadFragment()
            }
            1 -> {
                navItemIndex = 1
                CURRENT_TAG = TAG_PROFILE
                loadFragment()
            }
            2 -> {
                navItemIndex = 2
                CURRENT_TAG = TAG_INBOX
                loadFragment()
            }
//            3 -> {
////                showSnackBar("Under Development")
//                navItemIndex = 3
//                CURRENT_TAG = TAG_INVITE_FRIENDS
//                loadFragment()
//            }
            /* 3 -> {
 //                showSnackBar("Under Development")
                 navItemIndex = 3
                 CURRENT_TAG = TAG_EARN_TETOOTA_POINTS
                 loadFragment()
             }*/
            3 -> {
                navItemIndex = 3
                CURRENT_TAG = TAG_wishlist
                loadFragment()
            }
//------------------------------------------------------------------//
            4 -> {
                // showSnackBar("Under Development")
                navItemIndex = 4
                CURRENT_TAG = TAG_FAVORITES
                loadFragment()
            }
            5 -> {
                // showSnackBar("Under Development")
                navItemIndex = 5
                CURRENT_TAG = TAG_QRCODE
                loadFragment()
            }

//------------------------------------------------------------------//
            6 -> {
                navItemIndex = 6
                CURRENT_TAG = TAG_FOOD
                loadFragment()
            }
            7 -> {
                navItemIndex = 7
                CURRENT_TAG = TAG_SETTINGS
                loadFragment()
            }
            8 -> {
                navItemIndex = 8

                val companyId = Utils.loadPrefrence(Constant.COMPANY_ID, "0", mContext)
                if (companyId.equals("0")) {
                    CURRENT_TAG = TAG_CORPORATE_LOGIN
                } else {
                    CURRENT_TAG = TAG_HELP
                }
                loadFragment()
            }
            9 -> {
                navItemIndex = 9
                val companyId = Utils.loadPrefrence(Constant.COMPANY_ID, "0", mContext)
                if (companyId.equals("0")) {
                    CURRENT_TAG = TAG_HELP
                } else {
                    CustomSignOutDialog(this@MainActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this@MainActivity, getString(R.string.err_msg_mobile_number_limit)).show()
                }


                loadFragment()
            }
            10 -> {
                CustomSignOutDialog(this@MainActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                        this@MainActivity, getString(R.string.err_msg_mobile_number_limit)).show()
            }
            else -> {
                navItemIndex = 10
                loadFragment()
            }
        }
    }

    override fun OnProfileUpdate(userModal: ProfileDataResponse) {
        isProfileUpdate = true
        // setDataOnUI()
    }

    override fun onStart() {
        super.onStart()
        /*  Intercom.client().handlePushMessage()
          Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE)*/
    }


    /**
     * on profile PushNotificationDataResponse
     */
    override fun onProfileData(param: String, message: String) {
        if (message == "Yes") {
            if (Utils.haveNetworkConnection(this)) {
                val intent = ProfileDetailActivity.newMainIntent(this)
                ActivityStack.getInstance(this)
                startActivity(intent)
            } else {
                // TODO CHECK NET
                showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    override fun onFragmentOpenClose(fromFragment: String, toFragment: String, tag: String) {
        if (fromFragment.equals("profile") && toFragment.equals("favorites")) {
            backFragmentOn = activityTitles?.get(1)!!
            toolbar_title.text = headerTitle(5)
            isBackButtonEnabled = true
            toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_white_24dp)
        } else if (fromFragment == "profile" && toFragment == "invite_friends") {
            backFragmentOn = activityTitles?.get(1)!!
            toolbar_title.text = headerTitle(3)
            isBackButtonEnabled = true
            toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_white_24dp)
        } else if (fromFragment == "earnTetootaPoints" && toFragment == "invite_friends") {
            backFragmentOn = activityTitles?.get(4)!!
            toolbar_title.text = headerTitle(3)
            isBackButtonEnabled = true
            toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_white_24dp)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Log.e("onActivityResult =  "," requestCode "+ requestCode + " resultCode " + resultCode + " data " + data )
            if (requestCode == ADDPOST_REQUEST && data?.getStringExtra("ADDPOST_REQUEST").equals("SERVICE")) {
                dashboardFragment.adapter!!.fragmentService!!.refreshData()
            } else if (requestCode == ADDPOST_REQUEST && data?.getStringExtra("ADDPOST_REQUEST").equals("PRODUCT")) {
                dashboardFragment.adapter!!.fragmentProduct!!.refreshData()
            } else if (requestCode == ADDPOST_REQUEST && data?.getStringExtra("ADDPOST_REQUEST").equals("WISHLIST")) {
                dashboardFragment.adapter!!.fragmentWishlist!!.refreshData()
            }
        }
    }

    override fun onYes(param: String, message: String) {
        if (Utils.haveNetworkConnection(this@MainActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            userLogoutApi()
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    /**
     * @param mActivity the m activity
     */
    fun userLogoutApi() {
        var call: Call<LogoutDataResponse> = TetootaApplication.getHeader()
                .userLogout(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", this@MainActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", this@MainActivity),
                        Utils.loadPrefrence(Constant.USER_ID, "en", this@MainActivity))
        call.enqueue(object : Callback<LogoutDataResponse> {
            override fun onFailure(call: Call<LogoutDataResponse>?, t: Throwable?) {
                println("On Failure")
                hideProgressDialog()
                showSnackBar(t?.message.toString())
            }

            override fun onResponse(call: Call<LogoutDataResponse>?, response: Response<LogoutDataResponse>?) {
//                var logoutApiResponse: LogoutDataResponse? = response?.body()
                hideProgressDialog()
                if (response?.code() == 200) {
                    var deviceId: String? = null
                    val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.cancelAll()
                    //  AccountKit.logOut()
                    if (Utils.getPrefFcmToken(this@MainActivity) != null && Utils.getPrefFcmToken(this@MainActivity).length > 0) {
                        deviceId = Utils.getPrefFcmToken(this@MainActivity)
                    } else {
                        deviceId = "Not Found"
                    }
                    Utils.clearAllPrefrences(this@MainActivity)
                    Utils.setFcmToken(deviceId, this@MainActivity)
                    //Todo: jitu changes
                    Intercom.client().handlePushMessage()
                    Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
                    //
                    ActivityStack.cleareAll()
                    startActivity<SelectLanguageActivity>()
//                    Glide.get(this@MainActivity).clearDiskCache()
                    Glide.get(this@MainActivity).clearMemory()
                    finish()
                } else if (response?.code() == 401) {
                    if (response.body() == null) {
                        try {
                            val jObjError = JSONObject(response.errorBody()?.string())
                            val achualdata: JSONObject = jObjError.getJSONObject("meta")
                            val gson = Gson()
                            var mError: Meta = Meta()
                            val error = gson.fromJson(achualdata.toString(), Meta::class.java)
                            showSnackBar(error.message!!)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    Utils.clearAllPrefrences(this@MainActivity)
                    //Todo: jitu changes
                    Intercom.client().handlePushMessage()
                    Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
                    //
                    ActivityStack.cleareAll()
                    startActivity<SelectLanguageActivity>()
//                    Glide.get(this@MainActivity).clearDiskCache()
                    Glide.get(this@MainActivity).clearMemory()
                    finish()
                }
            }
        })
    }

    private fun genericSideMenu(): ArrayList<SideMenu>? {
        mMenuArrayList = arrayListOf()
        //   Log.e("ffffffffff",""+ Utils.getText(this, StringConstant.home))
        /*  if (Utils.getText(this, StringConstant.home) == "") {
              mMenuArrayList.add(SideMenu("Home", true, R.drawable.iv_search))
          } else {
              mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.home), true, R.drawable.iv_search))
          }*/
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.home), true, R.drawable.home))
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.profile_service), false, R.drawable.profile))
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.inbox), false, R.drawable.inbox))
        // mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.invite_friends), false, R.drawable.iv_search))
        //mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.earn_points), false, R.drawable.iv_search))
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.wishlist), false, R.drawable.letter))
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.favorite), false, R.drawable.favorites))
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.str_scan_pay), false, R.drawable.qr))
        //mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.redeem_coupons), false, R.drawable.iv_search))
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.str_food), false, R.drawable.food))
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.settings), false, R.drawable.set))
        val companyId = Utils.loadPrefrence(Constant.COMPANY_ID, "0", mContext)
        if (companyId.equals("0")) {
            mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.corporateLogin), false, R.drawable.login))
        }
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.help), false, R.drawable.information))
        mMenuArrayList.add(SideMenu(Utils.getText(this, StringConstant.sign_out), false, R.drawable.logout))
        return mMenuArrayList
    }

    private fun headerTitle(position: Int): String? {
        var header: String = ""
        val companyId = Utils.loadPrefrence(Constant.COMPANY_ID, "0", mContext)
        if (position == 0) {
            header = Utils.getText(this, StringConstant.home)
        } else if (position == 1) {
            header = Utils.getText(this, StringConstant.profile_service)
        } else if (position == 2) {
            header = Utils.getText(this, StringConstant.inbox)
        } else if (position == 3) {
            header = Utils.getText(this, StringConstant.wishlist)
        }
//        else if (position == 3) {
//            header = Utils.getText(this, StringConstant.invite_friends)
//        }
        /* else if (position == 4) {
             header = Utils.getText(this, StringConstant.tetoota_points)
         }*/
        else if (position == 4) {
            header = Utils.getText(this, StringConstant.favorite)

        } else if (position == 5) {
            header = Utils.getText(this, StringConstant.str_scan_pay)

        }
        /*else if (position == 5) {
            header = Utils.getText(this, StringConstant.redeem_coupons)

        }*/
        else if (position == 6) {
            header = Utils.getText(this, StringConstant.str_food)
        } else if (position == 7) {
            header = Utils.getText(this, StringConstant.settings)
        } else if (position == 8) {
            if (companyId.equals("0")) {
                header = Utils.getText(this, StringConstant.corporateLogin)
            } else {
                header = Utils.getText(this, StringConstant.help)
            }
        } else if (position == 9) {
            header = Utils.getText(this, StringConstant.help)
        }
        return header
    }

    // Container Activity must implement this interface
    interface onMessageReceiveListener {
        fun onMessageReceived(position: Int)
    }

    override fun messageFromParentFragmentToActivity(myString: String) {
        if (sliderCustomAdapter != null) {
            sliderCustomAdapter = SliderCustomAdapter(this@MainActivity, genericSideMenu() as ArrayList<SideMenu>)
            lst_menu_items.adapter = sliderCustomAdapter
            menuSelection(9)
            setToolbarTitle()
        }
    }

    private fun callUserDetailAPI() {
        if (Utils.haveNetworkConnection(this@MainActivity)) {
//          showProgressDialog(Utils.getText(this,StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MainActivity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileData(this@MainActivity, personData)
        }
    }

    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        val profileData: ProfileDataResponse = message?.get(0)!!
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MainActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        personData.tetoota_points = profileData.tetoota_points
        personData.proposal_count = profileData.proposal_count
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@MainActivity)
        hideProgressDialog()
        setDataOnUI()

    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
        hideProgressDialog()
    }

    /*private fun sendGoogleAnalytics(){
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "1")
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "MainActivity")
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
    }*/


/*
     private fun getKeyHash() {
         val info: PackageInfo
         try {
             info = packageManager.getPackageInfo("com.tetoota", PackageManager.GET_SIGNATURES)
             for (signature in info.signatures) {
                 val md: MessageDigest
                 md = MessageDigest.getInstance("SHA")
                 md.update(signature.toByteArray())
                 val something = String(Base64.encode(md.digest(), 0))
                 //String something = new String(Base64.encodeBytes(md.digest()));
                 Log.e("hash key*********** ", something)
             }
         } catch (e1: PackageManager.NameNotFoundException) {
             Log.e("name not found", e1.toString())
         } catch (e: NoSuchAlgorithmException) {
             Log.e("no such an algorithm", e.toString())
         } catch (e: Exception) {
             Log.e("exception", e.toString())
         }
     }
*/


    private val unreadConversationCountListener = UnreadConversationCountListener { unreadCount ->

    }

    private fun setBadgeVisibility(unreadCount: Int, unreadCountView: TextView) {
        val visibility = if (unreadCount == 0) View.INVISIBLE else View.VISIBLE
        unreadCountView.visibility = visibility
    }

    /************************invalid text************************/
    fun getAllInvalidText(): Unit {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        if (Utils.haveNetworkConnection(this)) {

            mHomePresenter.getInvalidText(this)
        } else {
            this.toast(Utils.getText(this, StringConstant.str_check_internet))

        }
    }

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {
        //   Log.e("ddddddd","invalid " + mDataList)
        if (mDataList != null) {
            tetootaApplication!!.mDataList = mDataList
        }
    }

    override fun onInvalidTextAPiFailure(message: String?) {
        Log.e("message ", "" + message)
    }


    /****************get count************************/

    private fun getCountData() {
        if (Utils.haveNetworkConnection(this)) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mHomePresenter.getIncompleteProposalData(this, personData.user_id.toString(), tetootaApplication!!.userMacAddress)
        } else {
            this.toast(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        //  Log.e("mProposalMesgData ", "" + mProposalMesgData.get(0).user_id)
        // Log.e("message ", "" + message)
        if (mProposalMesgData.get(0).user_id != null) {
            if (mProposalMesgData.size != 0) {
                toolbar.notification_count.visibility = View.VISIBLE
                toolbar.notification_count.text = mProposalMesgData.size.toString()
                tetootaApplication?.count = mProposalMesgData.size

                if (mProposalMesgData.size > 0) {
                    if (tetootaApplication!!.isFirstTime == "isFirstTime") {
                        bell_icon.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                        tetootaApplication!!.isFirstTime = "isSecondTime"
                    }
                }
            } else {
                toolbar.notification_count.visibility = View.GONE
            }

        }
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        if (message == "No Records Found") {
            toolbar.notification_count.visibility = View.GONE
        }

    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        Log.i("message", "" + message)
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
        Log.i("mDataList", "" + mDataList)
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        Log.i("message", "" + message)
    }

    /******************************lat long*************************/

    fun latLong() {
        gps = GPSTracker(this@MainActivity)

        // check if GPS enabled
        if (gps!!.canGetLocation()) {

            val latitude = gps!!.getLatitude()
            val longitude = gps!!.getLongitude()

            // Log.e("gps!!.getLatitude() ",""+ gps!!.getLatitude())
            // Log.e("gps!!.getLongitude() ",""+ gps!!.getLongitude())
            tetootaApplication!!.myLatitude = latitude
            tetootaApplication!!.myLongitude = longitude
            // Log.e("myLatitude",""+ tetootaApplication!!.myLatitude)
            // Log.e("myLongitude",""+ tetootaApplication!!.myLongitude)

        } else {
            gps!!.showSettingsAlert()
        }
    }
}
