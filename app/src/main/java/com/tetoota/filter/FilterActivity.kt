package com.tetoota.filter
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.android.bizdak.categoery.adapter.FilterAdapter
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.categories.CategoriesContract
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.categories.CategoriesPresenter
import com.tetoota.login.LoginDataResponse
import com.tetoota.subcategories.SubFilterActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_filter.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class FilterActivity : BaseActivity(),
        CategoriesContract.View,
        View.OnClickListener,
        FilterAdapter.FilterListener {
    override fun onMobileSuccess(message: Int, mesgDesc: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //    lateinit var listener: FilterAdapter.FilterListener
    var register: MenuItem? = null
    lateinit var filterAdapter: FilterAdapter
    var SUBCATEGORY_REQUEST: Int = 11

    override fun onFilterSelected(position: Int, categoryName: CategoriesDataResponse) {
//        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
//        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//        if(!mSelectedcategorylist.contains(categoryName)){
//            mSelectedcategorylist.add(categoryName)
//        }
//        personData.categories?.clear()
//        personData.categories = mSelectedcategorylist as ArrayList<CategoriesDataResponse?>
//        val json1 = Gson().toJson(personData)
//        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@FilterActivity)

        val intent = SubFilterActivity.newMainIntent(this@FilterActivity)
        intent?.putExtra("categoryObject", categoryName)
        ActivityStack.getInstance(this@FilterActivity)
        startActivityForResult(intent, SUBCATEGORY_REQUEST)
    }

    //    lateinit var mSelectedCategoriesAdapter: FilteredAdapter
    lateinit var personData: LoginDataResponse
    var isTopRated: Boolean = true;
    var mCategoryList: ArrayList<CategoriesDataResponse> = ArrayList()
    var mRemoveCategoryList: ArrayList<CategoriesDataResponse> = ArrayList()

    private val mCategoriesPresenter: CategoriesPresenter by lazy {
        CategoriesPresenter(this@FilterActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setSupportActionBar(toolbar)
        setMultiLanguageText()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initViews()

        mCategoriesPresenter.getCategoriesData(this@FilterActivity)
    }

    private fun initViews() {
//        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
//        personData = Gson().fromJson(json, LoginDataResponse::class.java)
//        if (personData.categories != null) {
//            mRemoveCategoryList = personData.categories as ArrayList<CategoriesDataResponse>
//            mSelectedCategoriesAdapter = FilteredAdapter(personData.categories as java.util.ArrayList<CategoriesDataResponse>, iAdapterClickListener = this)
//            rl_selected_category.layoutManager = LinearLayoutManager(this@FilterActivity)
//            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
//            rl_selected_category.layoutManager = layoutManager
//            rl_selected_category.setHasFixedSize(false)
//            rl_selected_category.adapter = mSelectedCategoriesAdapter
//        }
//        rl_category_layout.setOnClickListener(this@FilterActivity)
        btn_apply.setOnClickListener(this@FilterActivity)
        iv_topRated.setOnClickListener(this)
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, FilterActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

//    override fun cellSelectedItemClick(mString: Int, cellRow: Any) {
//        mSelectedCategoriesAdapter.setItem(mString)
//    }

    /**
     * On Click
     */
    override fun onClick(p0: View?) {
        when (p0) {
//            rl_category_layout -> {
//                if (mCategoryList.size > 0) {
//                    val intent = FilterCategoryActivity.newMainIntent(this@FilterActivity)
//                    intent?.putParcelableArrayListExtra("categorylist", mCategoryList)
//                    intent?.putParcelableArrayListExtra("mSelectedcategorylist", personData.categories)
//                    ActivityStack.getInstance(this@FilterActivity)
//                    startActivity(intent)
//                }
//            }

            btn_apply -> {
                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//                var json1 = null

//                if (mRemoveCategoryList.size > 0) {
//                    personData.categories?.clear()
//                    personData.categories = mRemoveCategoryList as ArrayList<CategoriesDataResponse?>
//                    val json1 = Gson().toJson(personData)
////                    Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@FilterActivity)
//                    val stringBuilde = StringBuilder("")
//                    var isFirst : Boolean? = true
//                    for (categoriesDataResponse in personData.categories!!) {
//                        if (categoriesDataResponse != null) {
//                            if(!isFirst!!){
//                                stringBuilde.append(",")
//                            }
//                            isFirst = false
//                            stringBuilde!!.append(categoriesDataResponse.category_id)
//                        }
//                    }
//                    Utils.savePreferences(Constant.CATEGORY_ID, stringBuilde.toString(), this@FilterActivity)
//                }
                if (isTopRated) {
                    Utils.savePreferences(Constant.TOPRATED, "yes", this@FilterActivity)
                } else {
                    Utils.savePreferences(Constant.TOPRATED, "no", this@FilterActivity)
                }
                Utils.savePreferences(Constant.CITY, edt_select_cty.text.toString().trim(), this@FilterActivity)
                hideKeyboard()
                setResult(Activity.RESULT_OK, null)
                finish()
            }

            iv_topRated -> {
                if (isTopRated) {
                    isTopRated = false
                    iv_topRated.setBackgroundResource(R.drawable.iv_un_box_large);
                } else {
                    isTopRated = true
                    iv_topRated.setBackgroundResource(R.drawable.iv_chk_box_large);
                }
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//                var json1 = null

//                if (mRemoveCategoryList.size > 0) {
//                    personData.categories?.clear()
//                    personData.categories = mRemoveCategoryList as ArrayList<CategoriesDataResponse?>
//                    val json1 = Gson().toJson(personData)
////                    Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@FilterActivity)
//                    val stringBuilde = StringBuilder("")
//                    var isFirst : Boolean? = true
//                    for (categoriesDataResponse in personData.categories!!) {
//                        if (categoriesDataResponse != null) {
//                            if(!isFirst!!){
//                                stringBuilde.append(",")
//                            }
//                            isFirst = false
//                            stringBuilde!!.append(categoriesDataResponse.category_id)
//                        }
//                    }
//                    Utils.savePreferences(Constant.CATEGORY_ID, stringBuilde.toString(), this@FilterActivity)
//                }
            if (isTopRated) {
                Utils.savePreferences(Constant.TOPRATED, "yes", this@FilterActivity)
            } else {
                Utils.savePreferences(Constant.TOPRATED, "no", this@FilterActivity)
            }
            Utils.savePreferences(Constant.CITY, edt_select_cty.text.toString().trim(), this@FilterActivity)
            hideKeyboard()
            setResult(Activity.RESULT_OK, null)
            finish()        }
    }

    override fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String) {
        this.mCategoryList = mCategoriesList as ArrayList<CategoriesDataResponse>
        if (mCategoryList.size > 0) {
            filterAdapter = FilterAdapter(this, mCategoryList, this)
            val mLayoutManager = LinearLayoutManager(this)
            recycler_view.layoutManager = mLayoutManager
            recycler_view.itemAnimator = DefaultItemAnimator()
            recycler_view.adapter = filterAdapter
        }
    }

    override fun onCatergoriesFailureResult(message: String) {
    }

    override fun onLoginResult(message: Int, mesgDesc: String) {
    }

    fun hideKeyboard() {
        val imm = applicationContext.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(edt_select_cty.getWindowToken(), 0);
    }

    override fun onBackPressed() {
        super.onBackPressed()
        hideKeyboard()
        ActivityStack.removeActivity(this@FilterActivity)
        Utils.savePreferences(Constant.SUBCATEGORIES_ID, "", this@FilterActivity)
        Utils.savePreferences(Constant.CATEGORY_ID, "", this@FilterActivity)
        finish()
    }

    override fun onResume() {
        super.onResume()
//        initViews()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.isVisible = false
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.clear()
        menuInflater.inflate(R.menu.mesg_screen_menu, menu)
        register = menu?.findItem(R.id.action_view_proposal)?.setIcon(R.drawable.nav_arrow_white)
        register?.isVisible = false
        return super.onPrepareOptionsMenu(menu)
    }

    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
        tv_city.text = Utils.getText(this, StringConstant.str_city)
        edt_select_cty.hint = Utils.getText(this, StringConstant.select_your_city)
        tv_top_rated.text = Utils.getText(this, StringConstant.top_rated)

     //   Log.e("dddddddddd","vvvvvvvvvvvv"+ Utils.getText(this, StringConstant.str_filter))


        toolbar_title.text = Utils.getText(this, StringConstant.str_filter)
    }

    override fun onDeepLinkingResult() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
