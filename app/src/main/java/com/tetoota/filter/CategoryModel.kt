package com.android.bizdak.categoery.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by vaibhav.malviya on 04-07-2017.
 */
data class CategoryModel(var masterCategoryID: String,
                         var categoryName: String,
                         var categoryName_fr: String,
                         var masterCategoryIcon: String,
                         var masterCategoryIconThumb: String,
                         var isMasterCategorySelected: Boolean,
                         var lastUpdatedAt: String,
                         var childCategories: MutableList<SubCategoryModel>) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<CategoryModel> = object : Parcelable.Creator<CategoryModel> {
            override fun createFromParcel(source: Parcel): CategoryModel = CategoryModel(source)
            override fun newArray(size: Int): Array<CategoryModel?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
            source.readString().toString(),
    source.readString().toString(),
    source.readString().toString(),
    source.readString().toString(),
    source.readString().toString(),
    1 == source.readInt(),
    source.readString().toString(),
            source.createTypedArrayList(SubCategoryModel.CREATOR)!!
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(masterCategoryID)
        dest.writeString(categoryName)
        dest.writeString(categoryName_fr)
        dest.writeString(masterCategoryIcon)
        dest.writeString(masterCategoryIconThumb)
        dest.writeInt((if (isMasterCategorySelected) 1 else 0))
        dest.writeString(lastUpdatedAt)
        dest.writeTypedList(childCategories)
    }
}