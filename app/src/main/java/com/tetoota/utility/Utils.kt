package com.tetoota.utility

import android.app.Activity
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.graphics.BitmapFactory
import android.graphics.Point
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.telephony.TelephonyManager
import android.text.Html
import android.text.Spanned
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.facebook.FacebookSdk.getApplicationContext

import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.maps.android.SphericalUtil
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.database.DBAdapter
import com.tetoota.login.LoginDataResponse
import okhttp3.RequestBody
import java.net.InetAddress
import java.net.NetworkInterface
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by charchit.kasliwal on 01-06-2017.
 */
class Utils {

    companion object Utils {
        private val REQUEST_CHECK_SETTINGS = 100
        var screenWidth: Int = 0
        var screenHeight: Int = 0
        val PREF_FCM_ID = "FCM_ID"


        fun locationEnable(context: Activity, mLocationRequest: LocationRequest, mGoogleApiClient: GoogleApiClient) {
            // Setting API to check the GPS on device is enabled or not
            val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest)
            builder.setAlwaysShow(true)

            val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
            result.setResultCallback { result ->
                val status = result.status
                //                final LocationSettingsStates = result.getLocationSettingsStates();
                when (status.statusCode) {
                    LocationSettingsStatusCodes.SUCCESS -> {
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    context,
                                    REQUEST_CHECK_SETTINGS)
                        } catch (e: IntentSender.SendIntentException) {
                            // Ignore the error.
                        }
                    }
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    context,
                                    REQUEST_CHECK_SETTINGS)
                        } catch (e: IntentSender.SendIntentException) {
                            // Ignore the error.
                        }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }// All location settings are satisfied. The client can
                // initialize location requests here.
                // Location settings are not satisfied. However, we have no way
                // to fix the settings so we won't show the dialog.
            }
        }

        /**

         * @param c
         * *
         * @return
         */
        fun getScreenWidth(c: Context): Int {
            if (screenWidth == 0) {
                val wm = c.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                val display = wm.defaultDisplay
                val size = Point()
                display.getSize(size)
                screenWidth = size.x
            }
//            screenWidth = screenWidth-200;
            return screenWidth
        }

        /**
         * @param c
         * *
         * @return
         */
        fun getScreenHeight(c: Context): Int {
            if (screenHeight == 0) {
                val wm = c.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                val display = wm.defaultDisplay
                val size = Point()
                display.getSize(size)
                screenHeight = size.y
            }
            return screenHeight
        }

        fun createPartFromString(descriptionString: String): RequestBody {
            return RequestBody.create(
                    okhttp3.MultipartBody.FORM, descriptionString)
        }

        /**
         * Method to get resource color
         * @param context
         * *
         * @param id
         * *
         * @return
         */
        fun getColor(context: Context, id: Int): Int {
            val version = Build.VERSION.SDK_INT
            if (version >= 23) {
                return ContextCompat.getColor(context, id)
            } else {
                return context.resources.getColor(id)
            }
        }


        /**
         * Method To Check Internet connection
         */
        fun haveNetworkConnection(mContext: Context): Boolean {
            val networkTypes = intArrayOf(ConnectivityManager.TYPE_MOBILE, ConnectivityManager.TYPE_WIFI)
            try {
                val connectivityManager = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                for (networkType in networkTypes) {
                    val activeNetworkInfo = connectivityManager.activeNetworkInfo
                    if (activeNetworkInfo != null && activeNetworkInfo.type === networkType)
                        return true
                }
            } catch (e: Exception) {
                return false
            }

            return false
        }

        /**
         * @param activity the calling activity
         */
        fun hideSoftKeyboard(activity: Activity) {
            try {
                val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        /**
         * Save the value in preference

         * @param key
         * *
         * @param value
         * *
         * @param activity
         */
        fun savePreferences(key: String, value: String,
                            activity: Context) {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val editor = sharedPreferences.edit()
            editor.putString(key, value)
            editor.commit()
        }

        fun savePreferencesBoolean(key: String, value: Boolean,
                                   activity: Context) {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val editor = sharedPreferences.edit()
            editor.putBoolean(key, value)
            editor.commit()
        }

        /**
         * get the value from preference

         * @param key
         * *
         * @param defaultStr
         * *
         * @param activity
         * *
         * @return
         */
        fun loadPrefrence(key: String, defaultStr: String,
                          activity: Context?): String {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val lang = sharedPreferences.getString(key, defaultStr)
            return lang!!
        }


        fun loadPrefrenceBoolean(key: String, defaultStr: Boolean,
                                 activity: Context?): Boolean {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val lang = sharedPreferences.getBoolean(key, defaultStr)

            return lang
        }

        /**
         * get the value from preference

         * @param key
         * *
         * @param defaultStr
         * *
         * @param activity
         * *
         * @return
         */
        fun loadPreference(key: String, defaultStr: String,
                           service: Service): String {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(service)
            val lang = sharedPreferences.getString(key, defaultStr)
            return lang!!
        }

        fun saveInt(key: String, value: Int) {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
            val editor = sharedPreferences.edit()
            editor.putInt(key, value)
            editor.commit()
        }

        fun loadInt(key: Int): Int {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
            val savedValue = sharedPreferences.getInt(key.toString(), 0)
            return savedValue
        }

        /**
         * @param activity
         */

        fun clearAllPrefrences(activity: Context) {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val editor = sharedPreferences.edit()
            editor.clear()
            editor.commit()
        }

        fun checkDistance(latLng1: LatLng, latLng2: LatLng): Double {
            val b = getDistance(latLng1, latLng2) / 1000
            return b
        }

        fun getDistance(fromLocation: LatLng, toLocation: LatLng): Double {
            return SphericalUtil.computeDistanceBetween(
                    fromLocation, toLocation)
        }

        fun setFcmToken(token: String?, activity: Context) {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val editor = sharedPreferences.edit()
            editor.putString(PREF_FCM_ID, token)
            editor.commit()
        }

        fun fromHtml(html: String): Spanned {
            val result: Spanned
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
            } else {
                result = Html.fromHtml(html)
            }
            return result
        }

        /**
         * @param activity
         * *
         * @return
         */
        fun getPrefFcmToken(activity: Context): String {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val lang = sharedPreferences.getString(PREF_FCM_ID, "")
            return lang!!
        }

        fun getDrawable(mActivity: Activity, drawable: Int): Drawable? {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                return mActivity.resources.getDrawable(drawable, mActivity.theme)
            } else {
                return mActivity.resources.getDrawable(drawable)
            }
        }

        fun getTimeStamp(mActivity: Context, date: String): Long {
            var localTime: Date? = null
            try {
                localTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(date)
                println("TimeStamp is ${localTime!!.time}")
            } catch (e: java.text.ParseException) {
                e.printStackTrace()
            }
            return localTime!!.time

        }


        fun getLangByCode(ctx: Context?, lngCode: String): HashMap<String, String> {
            var dbAdapter: DBAdapter = DBAdapter(ctx!!)
            dbAdapter.open()
            var languageMap: HashMap<String, String> = dbAdapter.getLanguageByCode(lngCode)
            dbAdapter.close()
            return languageMap
        }

        fun getText(context: Context?, textKey: String): String {
//            var languageMap = getLangByCode(context,loadPrefrence(Constant.USER_SELECTED_LANG, "en", context))
//            var returnText: String = languageMap[textKey].toString()
//            if (null != returnText && !returnText.equals("null")) {
//                return returnText
//            }
//            return ""

            var languageMap = TetootaApplication.getTextValuesHashMap()
            var returnText: String = languageMap!![textKey].toString()
            Log.e("TAGG", returnText)
            if (null != returnText && !returnText.equals("null")) {
                return returnText
            }
            return ""
        }

        fun getTextValue(context: Context?, textKey: String): String {
            var languageMap = TetootaApplication.getTextValuesHashMap()
            var returnText: String = languageMap!![textKey].toString()
            if (null != returnText && !returnText.equals("null")) {
                return returnText
            }
            return ""
        }


        fun getUrl(context: Context?, url: String, extraName: String): String {
            var mTelephonyManager: TelephonyManager = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            var networkType: Int = mTelephonyManager.getNetworkType()

            when (networkType) {
                TelephonyManager.NETWORK_TYPE_GPRS,
                TelephonyManager.NETWORK_TYPE_EDGE,
                TelephonyManager.NETWORK_TYPE_CDMA,
                TelephonyManager.NETWORK_TYPE_1xRTT,
                TelephonyManager.NETWORK_TYPE_IDEN -> return url + extraName // 2G

                TelephonyManager.NETWORK_TYPE_UMTS,
                TelephonyManager.NETWORK_TYPE_EVDO_0,
                TelephonyManager.NETWORK_TYPE_EVDO_A,
                TelephonyManager.NETWORK_TYPE_HSDPA,
                TelephonyManager.NETWORK_TYPE_HSUPA,
                TelephonyManager.NETWORK_TYPE_HSPA,
                TelephonyManager.NETWORK_TYPE_EVDO_B,
                TelephonyManager.NETWORK_TYPE_EHRPD,
                TelephonyManager.NETWORK_TYPE_HSPAP -> return url + extraName // 3G

                TelephonyManager.NETWORK_TYPE_LTE -> return url // 4G
                else -> {
                    return url // WiFi
                }
            }
        }

        fun getUrl(context: Context?, url: String, extraName: String, isExtra: Boolean): String {
            var mTelephonyManager: TelephonyManager = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            var networkType: Int = mTelephonyManager.getNetworkType()

            when (networkType) {
                TelephonyManager.NETWORK_TYPE_GPRS,
                TelephonyManager.NETWORK_TYPE_EDGE,
                TelephonyManager.NETWORK_TYPE_CDMA,
                TelephonyManager.NETWORK_TYPE_1xRTT,
                TelephonyManager.NETWORK_TYPE_IDEN -> return extraName // 2G

                TelephonyManager.NETWORK_TYPE_UMTS,
                TelephonyManager.NETWORK_TYPE_EVDO_0,
                TelephonyManager.NETWORK_TYPE_EVDO_A,
                TelephonyManager.NETWORK_TYPE_HSDPA,
                TelephonyManager.NETWORK_TYPE_HSUPA,
                TelephonyManager.NETWORK_TYPE_HSPA,
                TelephonyManager.NETWORK_TYPE_EVDO_B,
                TelephonyManager.NETWORK_TYPE_EHRPD,
                TelephonyManager.NETWORK_TYPE_HSPAP -> return extraName // 3G

                TelephonyManager.NETWORK_TYPE_LTE -> return url // 4G
                else -> {
                    return url // WiFi
                }
            }
        }

        // Temprary
        fun getUrl(context: Context?, url: String): String {
            var mTelephonyManager: TelephonyManager = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            var networkType: Int = mTelephonyManager.getNetworkType()

            when (networkType) {
                TelephonyManager.NETWORK_TYPE_GPRS,
                TelephonyManager.NETWORK_TYPE_EDGE,
                TelephonyManager.NETWORK_TYPE_CDMA,
                TelephonyManager.NETWORK_TYPE_1xRTT,
                TelephonyManager.NETWORK_TYPE_IDEN -> return url // 2G

                TelephonyManager.NETWORK_TYPE_UMTS,
                TelephonyManager.NETWORK_TYPE_EVDO_0,
                TelephonyManager.NETWORK_TYPE_EVDO_A,
                TelephonyManager.NETWORK_TYPE_HSDPA,
                TelephonyManager.NETWORK_TYPE_HSUPA,
                TelephonyManager.NETWORK_TYPE_HSPA,
                TelephonyManager.NETWORK_TYPE_EVDO_B,
                TelephonyManager.NETWORK_TYPE_EHRPD,
                TelephonyManager.NETWORK_TYPE_HSPAP -> return url // 3G

                TelephonyManager.NETWORK_TYPE_LTE -> return url // 4G
                else -> {
                    return url // WiFi
                }
            }
        }

        fun getNetworkType(context: Context?): String {
            var mTelephonyManager: TelephonyManager = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            var networkType: Int = mTelephonyManager.getNetworkType()

            when (networkType) {
                TelephonyManager.NETWORK_TYPE_GPRS,
                TelephonyManager.NETWORK_TYPE_EDGE,
                TelephonyManager.NETWORK_TYPE_CDMA,
                TelephonyManager.NETWORK_TYPE_1xRTT,
                TelephonyManager.NETWORK_TYPE_IDEN -> return "2" // 2G

                TelephonyManager.NETWORK_TYPE_UMTS,
                TelephonyManager.NETWORK_TYPE_EVDO_0,
                TelephonyManager.NETWORK_TYPE_EVDO_A,
                TelephonyManager.NETWORK_TYPE_HSDPA,
                TelephonyManager.NETWORK_TYPE_HSUPA,
                TelephonyManager.NETWORK_TYPE_HSPA,
                TelephonyManager.NETWORK_TYPE_EVDO_B,
                TelephonyManager.NETWORK_TYPE_EHRPD,
                TelephonyManager.NETWORK_TYPE_HSPAP -> return "3" // 3G

                TelephonyManager.NETWORK_TYPE_LTE -> return "4" // 4G
                else -> {
                    return "WiFi" // WiFi
                }
            }
        }

        /**
         * Method to get screen dimension i.e. screen height and width
         *
         * @param context
         * the context
         * @return String width and height of screen with "-" separator
         */
        fun getScreenDiamentiones(context: Context): String {
            var screenWidth = 0
            var screenHeight = 0
            try {
                val displayMetrics = DisplayMetrics()
                // the results will be higher than using the activity context object
                // or
                // the getWindowManager() shortcut
                val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                wm.defaultDisplay.getMetrics(displayMetrics)
                screenWidth = displayMetrics.widthPixels
                screenHeight = displayMetrics.heightPixels
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return screenWidth.toString() + "-" + screenHeight
        }


        /*
* Copyright 2012 Google Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

        private val SECOND_MILLIS = 1000
        private val MINUTE_MILLIS = 60 * SECOND_MILLIS
        private val HOUR_MILLIS = 60 * MINUTE_MILLIS
        private val DAY_MILLIS = 24 * HOUR_MILLIS
        fun getTimeAgo(time: Long, ctx: Context): String? {
            var time = time
            if (time < 1000000000000L) {
                // if timestamp given in seconds, convert to millis
                time *= 1000
            }

            val now = System.currentTimeMillis()
            if (time > now || time <= 0) {
                return null
            }

            // TODO: localize
            val diff = now - time
            if (diff < MINUTE_MILLIS) {
                return "just now"
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago"
            } else if (diff < 50 * MINUTE_MILLIS) {
                return "${diff / MINUTE_MILLIS} minutes ago"
            } else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago"
            } else if (diff < 24 * HOUR_MILLIS) {
                return "${diff / HOUR_MILLIS} hours ago"
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday"
            } else {
                return "${diff / DAY_MILLIS} days ago"
            }
        }


        fun getIPAddress(useIPv4: Boolean): String {
            try {
                var interfaces: List<NetworkInterface> = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (networkInterface in interfaces) {
                    var addrs: List<InetAddress> = Collections.list(networkInterface.getInetAddresses());
                    for (inetAddress in addrs) {
                        if (!inetAddress.isLoopbackAddress()) {
                            var sAddr: String = inetAddress.getHostAddress();
                            //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                            var isIPv4 = sAddr.indexOf(':') < 0;

                            if (useIPv4) {
                                if (isIPv4)
                                    return sAddr;
                            } else {
                                if (!isIPv4) {
                                    var delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                    return if (delim < 0) sAddr.toUpperCase() else sAddr.substring(0, delim).toUpperCase();
                                }
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
            } // for now eat exceptions
            return "";
        }


        fun shareService(context: Context?) {
            // Log.e("mImageUrl - ","" + mImageUrl)

            try {
                val urlToShare = StringConstant.app_link
                val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                val postString: String = personData.first_name + " is sharing talent on " + context?.resources?.getString(R.string.app_name) + " free skill exchange app." + " Download now\n" + urlToShare

                var imageUri: Uri? = null
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(context.getResources(), R.drawable.share_social),
                            null, null));
                } catch (e: NullPointerException) {
                }
                //  Log.e("imageUri - ","" + imageUri)

                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                shareIntent.putExtra(Intent.EXTRA_TEXT, postString)
                shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
                //  shareIntent.type = "image/*"
                // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                //  startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
                context?.startActivity(Intent.createChooser(shareIntent, "Share images..."))

            } catch (ex: android.content.ActivityNotFoundException) {
            }
        }
        fun setVisible(token: String?, activity: Context) {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val editor = sharedPreferences.edit()
            editor.putString(Constant.IS_VISIBLED, token)
            editor.commit()
        }
        fun getVisible( activity: Context):String {
            val sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity)
            val editor = sharedPreferences.edit()
            sharedPreferences.getString(Constant.IS_VISIBLED,"")
            return  sharedPreferences.getString(Constant.IS_VISIBLED,"")!!
        }
    }

}
