package com.tetoota.utility

/**
 * Created by charchit.kasliwal on 31-05-2017.
 */
class Constant {
    companion object constant {

        val DIALOG_PERMISSIONS = 1000
        val DIALOG_LOGIN_FAILURE_ALERT = 1001
        val DIALOG_EDIT_SERVICE = 2001
        val DIALOG_DELETE_SERVICE = 3001
        val API_SUCCESS_VALUE = 101
        val API_FAILURE_VALUE = 202
        val KEY_NAME: String = "keyName"
        val IS_USER_LOGGED_IN: String = "isUserLoggedIn"
        val USER_SELECTED_LANG: String = "user_language"
        val CONSTANT_ADMIN: String = "tetootaAdmin"
        val CONSTACT_PWD: String = "654321"
        val DEVICE_TYPE: String = "ANDROID"
        val USER_AUTH_TOKEN: String = "auth_token"
        val LOGGED_IN_USER_DATA: String = "logged_in_user_data"
        //        val LOGGED_IN_USER_DATA_FILTER: String = "logged_in_user_data_filter"
        val ALL_PUSH_NOTIFICATION: String = "all_push"
        val PROPOSAL_PUSH_NOTIFICATION: String = "proposal_push"
        val CHAT_PUSH_NOTIFICATION: String = "chat_push"
        val IS_VISIBLE: String = "is_visible"
        val IS_VISIBLED: String = "is_visibleD"
        val IS_SERVICE: String = "is_service"
        val USER_ID: String = "user_id"
        val MOBILE_NUMBER: String = "mobile_number"
        val USER_LOC_LAT: String = "lat"
        val USER_LOC_LONG: String = "long"
        val USER_PROPOSAL_COUNT: Int = 0
        val PUSH_NOTIFICATION = "pushNotification"
        // id to handle the notification in the notification tray
        val NOTIFICATION_ID = 100
        val NOTIFICATION_ID_BIG_IMAGE = 101
        val CATEGORY_ID: String = "categoryId"
        val TOPRATED: String = "topRated"
        val CITY: String = "city"
        val SUBCATEGORIES_ID: String = "subcategories"
        val CALLREFERRALCODE: String = "callreferralcode"
        val REFERRALCODE: String = "referralcode"
        val VERSION: String = "version"
        val API_KEY_GOOGLE_TRASLATE = "AIzaSyBXnLhmlze2wfEOA1ipfqDQHAVAJA1wnYQ"
        val COMPANY_ID: String = "companyId"
        val USER_STATUS: String = "user_status"
        // MixPanel Token
       // val MIXPANEL_TOKEN: String = "f5772f4b0721e22d0e55346714f609f0"
        val MIXPANEL_TOKEN: String = "3e25e4e520ee322ccdb5195e06283058"
      //  val MIXPANEL_API_SECRET: String = "7b9dedaf8d9d00bc9728eedaefb388ee"
        // InterCom API Key
//        val INTERCOM_APIKEY: String = "android_sdk-b1a8b170332fc1d32e76541eb0aceeaf34551f9b"
        val INTERCOM_APIKEY: String = "android_sdk-9e4b6463080f8430b462248be11df3f69424d2a2"
        // InterCom API ID
//        val INTERCOM_APIID: String = "l8y8bm5n"
        val INTERCOM_APIID: String = "ccdyon7j"

//        val INTERCOM_APIKEY: String = "ios_sdk-6d2857e5921ac7e4aa880c09984e0a87b57b9552"
//        val INTERCOM_APIID: String = "ccdyon7j"

        val CONTACTS_UPLOADED: String = "CONTACTS_UPLOADED"


        val COMPLETE: String = "Complete"
        val DECLINE: String = "Decline"
        val CANCEL: String = "Cancel"
        val ACCEPT: String = "Accept"
        val PENDING: String = "Pending"
        val OPTIONAL_INFORMATION_EMAIL: String = "optional_information_email"
        val OPTIONAL_INFORMATION_ADHAR: String = "optional_information_adhar"
        val OPTIONAL_INFORMATION_CATEGORY: String = "optional_information_category"

        val Instagram: String = "Instagram"
        val Twitter: String = "Twitter"
        val Facebook: String = "Facebook"
        val ANDROID_MIN_VERSION: Int = 0
        val Email_Verify: String = "Email_Verify"
        val Aadhar_Verify: String = "Aadhar_Verify"



        val START_TRADING: String = "Start Trading"
        val ACCEPTED: String = "Accepted"
        val ON_GOING: String = "On Going"
        val COMPLETED: String = "Completed"
        val PROPOSAL_PENDING: String = "Proposal Pending"
        val DECLINE_STATUS: String = "Decline"
        val PROPOSAL_SENT: String = "Proposal Sent"

        val FOOD_RUNNING: String = "Food Running"
        val FOOD_STOP: String = "Food Stop"



        val SERVICES: String = "Services"
        val FOOD: String = "Food"
        var CHAT_TYPE = "chat_type"
        var POST_ID = "post_id"
        var PROPOSAL_FROM = "proposal_from"
        var PROPOSAL_TO = "proposal_to"
        var WAITING = "Waiting"
        var WISHLIST = "Wishlist"


    }
}