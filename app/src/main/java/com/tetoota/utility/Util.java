package com.tetoota.utility;

import android.app.Activity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by charchit.kasliwal on 12-07-2017.
 */

public class Util {
    /**
     * max number of images to be selected
     */
    public static final String SELECTOR_MAX_IMAGE_NUMBER = "selector_max_image_number";
    public static int mMaxImageNumber = 5;
    // ImageRecyclerViewAdapter.OnClick will set it to true
    // Activity.OnImageInteraction will show the alert, and set it to false
    public static boolean bReachMaxNumber = false;
    public static boolean SUPPORTS_ECLAIR = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ECLAIR;
    public static String ACTIVE_LOCATION_UPDATE_PROVIDER_DISABLED = "com.radioactiveyak.places.active_location_update_provider_disabled";
    public static void toggleImageSelected(String filename) {
        if (SELECTED_IMAGES.contains(filename)) {
            SELECTED_IMAGES.remove(filename);
        } else {
            SELECTED_IMAGES.add(filename);
        }
    }

    // results
    public static final String SELECTOR_RESULTS = "selector_results";
    public static final String SELECTOR_INITIAL_SELECTED_LIST = "selector_initial_selected_list";
    public static final ArrayList<String> SELECTED_IMAGES = new ArrayList<String>();

    public static boolean isImageSelected(String filename) {
        return SELECTED_IMAGES.contains(filename);
    }

    public static MultipartBody.Part[] createMultiPartData(List<String> mImageList ,Activity mActivity) {
      MultipartBody.Part[]  mQueueMultipleImageParts = new MultipartBody.Part[mImageList.size()];
        for (int index = 0; index < mImageList.size(); index++) {
//            Log.d(TAG, "requestUploadSurvey: Queue Multiple Image " + index + "  " + mResults.get(index));
            File file = new File(mImageList.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image"), file);
            mQueueMultipleImageParts[index] = MultipartBody.Part.createFormData("post_image_attached[]", file.getName(), surveyBody);
        }
        return mQueueMultipleImageParts;
    }

    public static File createTemporalFileFrom(Activity mActivity,InputStream inputStream) throws IOException {
        File targetFile = null;

        if (inputStream != null) {
            int read;
            byte[] buffer = new byte[8 * 1024];

            targetFile = createTemporalFile(mActivity);
            OutputStream outputStream = new FileOutputStream(targetFile);

            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            outputStream.flush();

            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return targetFile;
    }

    public static File createTemporalFile(Activity mActivity) {
        return new File(mActivity.getExternalCacheDir(), "${String.valueOf(System.currentTimeMillis())}.jpg"); // context needed
    }

    public static MultipartBody.Part[] AddMultiPartData(List<String> mImageList ,Activity mActivity) {
        MultipartBody.Part[]  mQueueMultipleImageParts = new MultipartBody.Part[mImageList.size()];
        for (int index = 0; index < mImageList.size(); index++) {
//            Log.d(TAG, "requestUploadSurvey: Queue Multiple Image " + index + "  " + mResults.get(index));
            File file = new File(mImageList.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image"), file);
            mQueueMultipleImageParts[index] = MultipartBody.Part.createFormData("post_image_attached[]", file.getName(), surveyBody);
        }
        return mQueueMultipleImageParts;
    }
}
