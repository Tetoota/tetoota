package com.tetoota.addrequest

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.fragment.BaseFragment
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_add__service.*
import kotlinx.android.synthetic.main.hint_layout.*

class HintFragment : BottomSheetDialogFragment() {
    private var tetootaApplication: TetootaApplication? = null


    companion object {
        var type = ""
        var screenType = ""
        fun newInstance(type: String): HintFragment {
            val fragment = HintFragment()
            this.type = type
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        tetootaApplication = activity!!.applicationContext as TetootaApplication
        return inflater?.inflate(R.layout.hint_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

        ivClose.setOnClickListener(View.OnClickListener { dialog.dismiss() })

    }

    private fun initView() {
        val mArgs = arguments
        type = mArgs!!.getString("key").toString()
        if (mArgs!!.getString("type").toString() != null)
            screenType = mArgs!!.getString("type").toString()
        if (!TextUtils.isEmpty(screenType) && screenType.equals(Constant.WISHLIST, true)) {
            tvHeading.text = Utils.getText(context, StringConstant.str_dodont)
            if (type.equals("title", true)) {
                tvPoint1.text = Utils.getText(context, StringConstant.str_wishlist_do_donts_title1)
                tvPoint2.text = Utils.getText(context, StringConstant.str_wishlist_do_donts_title2)
                tvSubPoint1.text = Utils.getText(context, StringConstant.str_wishlist_do_donts_1)
                tvSubPoint2.text = Utils.getText(context, StringConstant.str_wishlist_do_donts_2)
                tvSSubPoint1.visibility = View.GONE
                tvSSubPoint2.visibility = View.GONE
                tvSSubPoint3.visibility = View.GONE
                tvSSubPoint4.visibility = View.GONE
                tvPoint3.visibility = View.GONE
            } else if (type.equals("desc", true)) {
                tvHeading.text = Utils.getText(context, StringConstant.str_understand_how)
                tvPoint1.text = Utils.getText(context, StringConstant.str_understand_how_description1)
                tvPoint2.text = Utils.getText(context, StringConstant.str_understand_how_description2)
                tvSubPoint1.visibility = View.GONE
                tvSubPoint2.visibility = View.GONE
                tvSSubPoint1.text = Utils.getText(context, StringConstant.str_understand_how_description2_1)
                tvSSubPoint2.text = Utils.getText(context, StringConstant.str_understand_how_description2_2)
                tvSSubPoint3.visibility = View.GONE
                tvSSubPoint4.visibility = View.GONE
                tvPoint3.visibility = View.GONE
            } else if (type.equals("point", true)) {
                tvPoint3.visibility = View.VISIBLE
                tvHeading.text = Utils.getText(context, StringConstant.str_wishlist_pricing)
                tvPoint1.text = Utils.getText(context, StringConstant.str_wishlist_pricing1)
                tvPoint2.text = Utils.getText(context, StringConstant.str_wishlist_pricing2)
                tvPoint3.text = Utils.getText(context, StringConstant.str_wishlist_pricing3)
                tvSubPoint1.visibility = View.GONE
                tvSubPoint2.visibility = View.GONE
                tvSSubPoint1.visibility = View.GONE
                tvSSubPoint2.visibility = View.GONE
                tvSSubPoint3.visibility = View.GONE
                tvSSubPoint4.visibility = View.GONE
            }
        } else {
            tvPoint3.visibility = View.GONE
            if (type.equals("title", true)) {
                tvHeading.text = Utils.getText(context, StringConstant.str_dodont)
                tvPoint1.text = Utils.getText(context, StringConstant.str_question_title1)
                tvPoint2.text = Utils.getText(context, StringConstant.str_question_title2)
                tvSubPoint1.text = Utils.getText(context, StringConstant.str_question_title_1)
                tvSubPoint2.text = Utils.getText(context, StringConstant.str_question_title_2)
                tvSSubPoint1.visibility = View.GONE
                tvSSubPoint2.visibility = View.GONE
                tvSSubPoint3.visibility = View.GONE
                tvSSubPoint4.visibility = View.GONE
            } else if (type.equals("desc", true)) {
                tvHeading.text = Utils.getText(context, StringConstant.str_understand_how)
                tvPoint1.text = Utils.getText(context, StringConstant.str_question_description1)
                tvPoint2.text = Utils.getText(context, StringConstant.str_question_description2)
                tvSubPoint1.visibility = View.GONE
                tvSubPoint2.visibility = View.GONE
                tvSSubPoint1.text = Utils.getText(context, StringConstant.str_question_description_1)
                tvSSubPoint2.text = Utils.getText(context, StringConstant.str_question_description_2)
                tvSSubPoint3.visibility = View.GONE
                tvSSubPoint4.visibility = View.GONE
            } else if (type.equals("point", true)) {
                tvHeading.text = Utils.getText(context, StringConstant.str_what_this)
                tvPoint1.text = Utils.getText(context, StringConstant.str_question_points1)
                tvPoint2.text = Utils.getText(context, StringConstant.str_question_points2)
                tvSubPoint1.visibility = View.GONE
                tvSubPoint2.visibility = View.GONE
                tvSSubPoint1.text = Utils.getText(context, StringConstant.str_question_point_1)
                tvSSubPoint2.text = Utils.getText(context, StringConstant.str_question_point_2)
                tvSSubPoint3.text = Utils.getText(context, StringConstant.str_question_point_3)
                tvSSubPoint4.text = Utils.getText(context, StringConstant.str_question_point_4)
            }
        }

    }

}