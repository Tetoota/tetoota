package com.tetoota.addrequest

import android.app.Activity
import com.tetoota.TetootaApplication
import com.tetoota.fragment.dashboard.FavoriteResponse
import com.tetoota.fragment.home.model.RecentactivityResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Util
import com.tetoota.utility.Utils
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 11-07-2017.
 */
class AddRequestInteractor {
    val TEXT = "text"
    val IMAGE = "image"
    val AUDIO = "audio"
    val VIDEO = "video"
    var mAddRequestApiListener: AddRequestContract.addRequestApiListener

    constructor(mAddRequestApiListener: AddRequestContract.addRequestApiListener) {
        this.mAddRequestApiListener = mAddRequestApiListener
    }

    /**
     * Method to get PushNotificationDataResponse from Server
     */
    fun getTredingDataFromServer(mActivity: Activity) {
        val call: Call<TradingResponse> = TetootaApplication.getHeader()
                .tredingPrefrencesApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity))
        call.enqueue(object : Callback<TradingResponse> {
            override fun onResponse(call: Call<TradingResponse>?,
                                    response: Response<TradingResponse>?) {
                println("Success")
                var mDashboardSliderData: TradingResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        mAddRequestApiListener.onSuccess(mDashboardSliderData?.data as ArrayList<Any>, "sliderList")
                    } else {
                        mAddRequestApiListener.onDeletePostFailure(response.body()?.meta?.message.toString())
                    }
                } else {
                    mAddRequestApiListener.onDeletePostFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<TradingResponse>?, t: Throwable?) {
                if (call?.isCanceled!!) {
                    println("Request CAll Cancelled")
                } else {
                    println("Request CAll Another Issue")
                    mAddRequestApiListener.onDeletePostFailure(t?.message.toString())
                }
            }
        })

    }

    fun addServiceToServer(mActivity: Activity, map: HashMap<String, RequestBody>, mImageList: List<String>) {
        var call: Call<AddServiceDataResponse> = TetootaApplication.getHeader()
                .createAddPost(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), map, Util.createMultiPartData(mImageList, mActivity))
        // Log.e("TAG", "GGGGGGGGGGGGGGGG" + map)
        call.enqueue(object : Callback<AddServiceDataResponse> {
            override fun onResponse(call: Call<AddServiceDataResponse>,
                                    response: Response<AddServiceDataResponse>?) {
                AddServiceFragment.titleVaild
//                val json = Gson().toJson(response!!.body())
//                val emp = JSONObject(json).getJSONObject("meta")
////                val empname = emp.getString("title")
                if (response!!.body()?.meta?.code.equals("400")) {
                    mAddRequestApiListener.onCategoryListener(response!!.body()?.meta?.message.toString(), "failure")
                } else
                    mAddRequestApiListener.onCategoryListener(response!!.body()?.meta?.message.toString(), "success")

            }

            override fun onFailure(call: Call<AddServiceDataResponse>?, t: Throwable?) {
                mAddRequestApiListener.onCategoryListener(t?.message.toString(), "failure")
            }
        })
    }

    fun editServiceToServer(mActivity: Activity, map: HashMap<String, RequestBody>, mImageList: List<String>) {
        var call: Call<AddServiceDataResponse> = TetootaApplication.getHeader()
                .postEdit(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), map, Util.createMultiPartData(mImageList, mActivity))
        call.enqueue(object : Callback<AddServiceDataResponse> {
            override fun onResponse(call: Call<AddServiceDataResponse>,
                                    response: Response<AddServiceDataResponse>?) {
                if (response?.code() == 200) {
                    mAddRequestApiListener.editDataListener(response?.message().toString())
                } else {
                    mAddRequestApiListener.onCategoryListener("fail'", "failure")
                }
            }

            override fun onFailure(call: Call<AddServiceDataResponse>?, t: Throwable?) {
                mAddRequestApiListener.onCategoryListener(t?.message.toString(), "failure")
            }
        })
    }

    fun deleteServiceFromServer(mActivity: Activity, user_id: String, post_id: String) {
        val call: Call<FavoriteResponse> = TetootaApplication.getHeader()
                .deletePostApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), user_id, post_id)
        call.enqueue(object : Callback<FavoriteResponse> {
            override fun onResponse(call: Call<FavoriteResponse>?,
                                    response: Response<FavoriteResponse>?) {
                println("Success")
                var mDashboardSliderData: FavoriteResponse? = response?.body()
                if (response?.code() == 200) {
                    mAddRequestApiListener.onDeletePostSuccess(response.body()?.meta?.message.toString())  //("success")
                } else {
                    mAddRequestApiListener.onDeletePostFailure(response?.body()?.meta?.message.toString())  //("fail")
                }
            }

            override fun onFailure(call: Call<FavoriteResponse>?, t: Throwable?) {
                println("Request CAll Another Issue")
                mAddRequestApiListener.onDeletePostFailure("fail")
            }
        })
    }
}