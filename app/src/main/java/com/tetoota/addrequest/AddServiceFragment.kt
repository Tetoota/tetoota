package com.tetoota.addrequest

import android.app.Activity
import android.app.AlertDialog
import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.model.LatLng
import com.squareup.picasso.Picasso
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.categories.CategoriesContract
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.categories.CategoriesPresenter
import com.tetoota.customviews.ImageFilePath
import com.tetoota.fragment.BaseFragment
import com.tetoota.subcategories.DataItem
import com.tetoota.subcategories.SubCategoriesContract
import com.tetoota.subcategories.SubCategoriesPresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_add__service.*
import kotlinx.android.synthetic.main.fragment_add__service.view.*
import okhttp3.RequestBody
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AddServiceFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AddServiceFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddServiceFragment : BaseFragment(), AddRequestContract.View, BottomSheetDialog.IBottomSheetListener,
        CategoriesContract.View, SubCategoriesContract.View, View.OnClickListener {
    override fun onMobileSuccess(message: Int, mesgDesc: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    var mResults: ArrayList<String> = arrayListOf()
    var PLACE_AUTOCOMPLETE_REQUEST_CODE = 1
    var CAMERA_RESULT = 201
    var GALLARY_RESULT = 200
    private var isServicePreferred: Boolean = false
    private var isTrendingServiceCall: Boolean = false
    private var isCategoryServiceCall: Boolean = false
    private var isCategoryServiceFilled: Boolean = false
    private var isSubCategoryServiceCall: Boolean = false
    lateinit var rootview: View
    var dest1: String? = null
    var location: LatLng? = null
    var mCategoryId: String? = null
    var mSubCategoryId: String? = null
    //  var mTradingId: Int? = null
    var mTradingPreId: Int? = 1
    var myBitmap: Bitmap? = null
    var picUri: Uri? = null
    private var tetootaApplication: TetootaApplication? = null

    private val mAddServicePresenter: AddRequestPresenter by lazy {
        AddRequestPresenter(this@AddServiceFragment)
    }

    private val mCategoriesPresenter: CategoriesPresenter by lazy {
        CategoriesPresenter(this@AddServiceFragment)
    }
    private val mSubCAtegoryPresenter: SubCategoriesPresenter by lazy {
        SubCategoriesPresenter(this@AddServiceFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment

        tetootaApplication = activity!!.applicationContext as TetootaApplication

        rootview = inflater.inflate(R.layout.fragment_add__service, container, false)!!


        rootview.edt_title.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val length = rootview.edt_title.length()
                val convert = length.toString()
                txt_count30.setText(convert)

                /*********check invalid text**********/
                /*   var arr1= listOf<String>("mm","vadi","call","kk")
                   var text = 0
                   for (i in arr1.indices)
                   {
                       text += s.indexOf(""+ arr1[i])
                       if (text > 0)
                       {
                           Log.e("text "," if* " + text)
                           Log.e("arr1[i] "," if** " + arr1[i])
                           rootview.edt_title.setError("Invalid text")
                       }
                       Log.e("text "," * " + text)
                       Log.e("arr1[i] "," ** " + arr1[i])
                   }*/

                /*   var text  = 0
                   var arr= listOf<String>("sex","vadi","call me","fuck")
                   var i = 0
                   for (i in 0..arr.size -1) {
                       Log.e("iiii ","" + i)

                       text += s.indexOf(""+ arr[i])
                       Log.e("arr[i] ","" + arr[i])
                       Log.e("text ","" + text)
                       if (text > 0)
                       {
                           rootview.edt_title.setError("Invalid text")

                       }

                   }*/
                /***************************************/

            }

            override fun afterTextChanged(s: Editable) {}
        })
        rootview.edt_service.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val length = rootview.edt_service.length()
                val convert = length.toString()
                txt_count.setText(convert)
            }

            override fun afterTextChanged(s: Editable) {}
        })
        return rootview

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
    }

/*
   fun charFilter(){
       var sTitle = rootview.edt_title.text.toString().trim()
       if (rootview.edt_title.text.toString().trim() == "call me")
       {
           rootview.edt_title.error = "Please enter valid character."
           rootview.edt_title?.isFocusable = true
           return
       } else if (rootview.edt_service.text.toString().trim() == "call me")
       {
           rootview.edt_service.error = "Please enter valid character."
           rootview.edt_service?.isFocusable = true
           return
       }
    }
*/

    private fun initViews() {
        /*     val layoutManager = GridLayoutManager(getApplicationContext(), 5)
             rv_photos.visibility = View.VISIBLE
             rv_photos.layoutManager = layoutManager
             rv_photos.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?*/
        tv_tetoota_point_txt.setOnClickListener(this)
        tv_tetoota_desc_txt.setOnClickListener(this)
        tv_tetoota_title_txt.setOnClickListener(this)

        btn_submit_req.setOnClickListener(this)
        iv_toggle_service.setOnClickListener(this)
        rl_location.setOnClickListener(this)
        edt_location.setOnClickListener(this)
        //iv_addphotos.setOnClickListener(this)
        iv_servicephoto.setOnClickListener(this)
        rl_trading_prefrence.setOnClickListener(this)
        rl_category.setOnClickListener(this)
        rl_subCategory.setOnClickListener(this)
        edt_category.setOnClickListener(this)
        edt_subCategory.setOnClickListener(this)
        // rl_setquota.setOnClickListener(this)
        point_layout.setOnClickListener(this)
        tv_tetoota_point_txt.text = Utils.getText(context, StringConstant.str_what_this)
        tv_tetoota_desc_txt.text = Utils.getText(context, StringConstant.str_understand_how)
        tv_tetoota_title_txt.text = Utils.getText(context, StringConstant.str_dodont)
        setMultiLanguageText()

        //  mTradingId = 3


    }

    private fun setMultiLanguageText() {
        rl_set_tetoota_pt.visibility = View.VISIBLE
        tv_title.text = Utils.getText(context, StringConstant.title)
        tv_service.text = Utils.getText(context, StringConstant.add_service_describe_service)
        tv_category.text = Utils.getText(context, StringConstant.category)
        tv_subCategory.text = Utils.getText(context, StringConstant.add_service_subcategory_text)
        //  tv_set_quota.text = Utils.getText(context, StringConstant.add_service_quota_text)
        //  tv_qualification.text = Utils.getText(context, StringConstant.service_qualification_placeholder)
        //  tv_availbility.text = Utils.getText(context, StringConstant.str_availability)
        tv_location.text = Utils.getText(context, StringConstant.str_locations)
        edt_category.hint = Utils.getText(context, StringConstant.service_category_placeholder)
        edt_subCategory.hint = Utils.getText(context, StringConstant.service_subcategory_placeholder)
        edt_title.hint = Utils.getText(context, StringConstant.service_title_placeholder)
        edt_service.hint = Utils.getText(context, StringConstant.service_description_placeholder)
        // edt_qualification.hint = Utils.getText(context, StringConstant.write_certificate_degree)
        btn_submit_req.text = Utils.getText(context, StringConstant.btn_submit_req)
        tv_switch_btn.text = Utils.getText(context, StringConstant.service_virtually_placeholder)
        tv_service_preferred.text = Utils.getText(context, StringConstant.tv_service_preferred)
        tv_photo.text = Utils.getText(context, StringConstant.upload_picture_auto)
        tv_photos.text = Utils.getText(context, StringConstant.tv_photos)
        edt_location.hint = Utils.getText(context, StringConstant.service_location_placeholder)
        edt_trading_pref.hint = Utils.getText(context, StringConstant.add_service_fine_by_both)
        tv_trading_pref.text = Utils.getText(context, StringConstant.tv_trading_pref)
        //   edt_quota.hint = Utils.getText(context, StringConstant.service_quota_placeholder)
        edt_tetoota_pt.hint = Utils.getText(context, StringConstant.service_tetoota_point_placeholder)
        tv_tetoota_point.text = Utils.getText(context, StringConstant.tv_tetoota_point)
//        edt_trading_pref.text = Utils.getText(context,StringConstant.add_service_fine_by_both)
        per_text.text = Utils.getText(context, StringConstant.add_service_per_point)
        tv_tetoota_title_txt.setOnClickListener(this)
        tv_tetoota_desc_txt.setOnClickListener(this)
        tv_tetoota_point_txt.setOnClickListener(this)

        edt_tetoota_pt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (!s.toString().isEmpty()) {
                    var number: Int = Integer.parseInt(s.toString());
                    if (number > 0 && number < 70) {
//good
                        tv_average_condition.setText("Good Price");
                        tv_average_condition.setTextColor(ContextCompat.getColor(activity!!, R.color.color_green_color))
                    } else if (number >= 70 && number < 100) {
//average
                        tv_average_condition.setText("Average");
                        tv_average_condition.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimary))

                    } else if (number >= 100 && number < 200) {
                        //expensive
                        tv_average_condition.setText("Expensive");
                        tv_average_condition.setTextColor(ContextCompat.getColor(activity!!, R.color.colorExpensive))

                    } else if (number >= 200) {
                        //very expensive
                        tv_average_condition.setText("Very Expensive");
                        tv_average_condition.setTextColor(ContextCompat.getColor(activity!!, R.color.colorVeryExpensive))

                    } else if (number == 0) {
                        edt_tetoota_pt.setText("")
                        Toast.makeText(context, "Please enter valid value.", Toast.LENGTH_LONG).show()
                    }
                } else {
                    tv_average_condition.setText(null);
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        //  edt_availability.hint = Utils.getText(context, StringConstant.service_availability_placeholder)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar_category.visibility = View.GONE
        progressBar_trading.visibility = View.GONE
        iv_toggle_service.setImageResource(R.drawable.tv_toggle_off)
    }

    private fun validate(): Boolean {
        Log.e("===========", "" + tetootaPt)
        /* if(tetootaPt.get(0).equals("0"))
         {

         }*/
        if (rootview.edt_title.text.toString().trim().isEmpty()) {
            // activity!!.toast(Utils.getText(context, StringConstant.myprofile_write_yourself_min30))
            rootview.edt_title.setFocusable(true);
            rootview.edt_title.setFocusableInTouchMode(true);
            rootview.edt_title.requestFocus();
            rootview.edt_title.setError(Utils.getText(context, "add_service_title_alert"))
            return false
        } else if (rootview.edt_category.text.toString().trim().isEmpty()) {
            activity!!.toast(Utils.getText(context, StringConstant.add_service_category_alert))
            return false
        } else if (rootview.edt_subCategory.text.toString().trim().isEmpty()) {
            activity!!.toast(Utils.getText(context, StringConstant.add_service_subcategory_alert))
            return false
        }
//        else if (rootview.edt_quota.text.toString().trim().isEmpty()) {
//            activity!!.toast(Utils.getText(context, StringConstant.add_service_quota_alert))
//            return false
//        } else if (rootview.edt_availability.text.toString().trim().isEmpty()) {
//            activity!!.toast(Utils.getText(context, StringConstant.add_service_availability_alert))
//            return false
//        }
//        else if (rootview.edt_location.text.toString().trim().isEmpty()) {
//            activity!!.toast(Utils.getText(context, StringConstant.add_service_location_alert))
//            return false
//        }
        else if (rootview.edt_service.text.toString().trim().isEmpty()) {
            rootview.edt_service.setFocusable(true);
            rootview.edt_service.setFocusableInTouchMode(true);
            rootview.edt_service.requestFocus();
            rootview.edt_service.setError(Utils.getText(context, "add_service_describe_service"))
            return false
        }
//        else if (rootview.edt_trading_pref.text.toString().trim().isEmpty()) {
//            activity!!.toast(Utils.getText(context, StringConstant.add_service_trading_alert))
//            return false
//        }
        else if ((rootview.edt_tetoota_pt.text.toString().trim().isEmpty() || tetootaPt.get(0).equals('0'))) {
            //Log.e("fffffffffffffff","" + rootview.edt_tetoota_pt.text.toString())
            //activity!!.toast(Utils.getText(context, StringConstant.add_service_point_alert))
            rootview.edt_tetoota_pt.setFocusable(true);
            rootview.edt_tetoota_pt.setText("")
            rootview.edt_tetoota_pt.setFocusableInTouchMode(true);
            rootview.edt_tetoota_pt.requestFocus();
            rootview.edt_tetoota_pt.error = Utils.getText(context, Utils.getText(context, StringConstant.add_service_point_alert))
            return false
        }
//        else if (TextUtils.isEmpty(realPath)) {
//            Toast.makeText(context, getString(R.string.image_validation), Toast.LENGTH_LONG).show()
//            return false
//        }

        /* else if (mTradingPreId != null && mTradingPreId != 3 && !edt_tetoota_pt.text.toString().trim().isEmpty() && points_value.text.isNullOrBlank()) {
             activity!!.toast(Utils.getText(context, StringConstant.include_proposal_tradetime_alert))
             return false
         }*/
//        else if (tv_photo.visibility == View.VISIBLE) {
//            activity!!.toast(Utils.getText(context, StringConstant.add_service_image_alert))
//            return false
//        }
        else if (points_value.text.isNullOrBlank()) {
            activity!!.toast("Please select point value")
            return false
        }
        return true
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    companion object {
        val titleVaild: String = ""
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment Add_Service_Fragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): AddServiceFragment {
            val fragment = AddServiceFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onApiSuccess(mTredingList: ArrayList<Any>, message: String) {
        progressBar_trading.visibility = View.GONE
        isTrendingServiceCall = false
        BottomSheetDialog.newInstance1(mTredingList as ArrayList<TradingDataResponse>,
                this@AddServiceFragment, "trading")
        BottomSheetDialog().show(activity!!.supportFragmentManager, "dialog")
    }

    override fun onDeletePostApiSuccess(message: String) {
        progressBar_trading.visibility = View.GONE
        isTrendingServiceCall = false
    }

    override fun onApiFailure(message: String) {
        progressBar_trading.visibility = View.GONE
        isTrendingServiceCall = false
    }

    override fun onStop() {
        super.onStop()
    }

    private fun callSubCategory() {
        var myNumOrNull: Int? = mCategoryId?.toIntOrNull()
        mSubCAtegoryPresenter.getSubCategoriesData(myNumOrNull!!, this.activity!!)
    }

    override fun dataClick(mTredingDataResponse: Any, type: String) {
        //  Log.e("fffffffffffffff","" + type)
        if (type == "category") {
            val mTradingData = mTredingDataResponse as CategoriesDataResponse
            edt_category.text = mTradingData.category_name
            mCategoryId = mTradingData.category_id.toString()
            isCategoryServiceFilled = true
//            callSubCategory()
        } else if (type == "subcategory") {
            val mTradingData = mTredingDataResponse as DataItem
            edt_subCategory.text = mTradingData.sub_category_name
            mSubCategoryId = mTradingData.sub_category_id.toString()
        }
        /*  else if (type == "quota") {
            val mTradingData = mTredingDataResponse as String
            Thread.sleep(200)
            edt_quota.text = mTradingData
        }*/
        else if (type == "points") {
            val mTradingData = mTredingDataResponse as String
            Thread.sleep(200)
            points_value.text = mTradingData
        } else {
            val mTradingData = mTredingDataResponse as TradingDataResponse
            // Log.e("bbbbbbbbbbbb",""+ mTradingData)
            mTradingPreId = mTradingData.id
            Log.e("bbbiiiiiiiiiiiiii", "" + mTradingPreId)
            edt_trading_pref.text = mTradingData.description
            /*  if (mTradingId != 2 && mTradingId != null) {
                  rl_set_tetoota_pt.visibility = View.VISIBLE
              } else {
                  rl_set_tetoota_pt.visibility = View.GONE
              }*/
        }
    }

    override fun onSubCategoriesSuccessResult(mSubCategoriesList: List<DataItem>, message: String) {
        isSubCategoryServiceCall = false
        progressBar_subCategory.visibility = View.GONE
        BottomSheetDialog.newSubCategoryInstance(mSubCategoriesList as ArrayList<DataItem>, this@AddServiceFragment,
                "subcategory")
        BottomSheetDialog().show(activity!!.supportFragmentManager, "dialog")
    }

    override fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String) {
        isCategoryServiceCall = false
//        tv_photo.visibility = View.VISIBLE
        progressBar_category.visibility = View.GONE
        BottomSheetDialog.newInstance(mCategoriesList as ArrayList<CategoriesDataResponse>, this@AddServiceFragment,
                "categories")
        BottomSheetDialog().show(activity!!.supportFragmentManager, "dialog")
    }

    override fun onCatergoriesFailureResult(message: String) {
        isCategoryServiceCall = false
        progressBar_category.visibility = View.GONE
        if (activity != null) {
            activity!!.toast(message)
        }
    }

    override fun onSubCatergoriesFailureResult(message: String) {
        isSubCategoryServiceCall = false
        progressBar_subCategory.visibility = View.GONE
        if (activity != null) {
            activity!!.toast(message)
        }
    }

    var realPath = ""
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                var place: Place = PlaceAutocomplete.getPlace(activity, data)
                edt_location.setText(place.address)
                location = place.latLng
                //  Log.e("location!!.latitude ","" + location!!.latitude)
                //  Log.e("location!!.longitude ","" + location!!.longitude)
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                var status: Status = PlaceAutocomplete.getStatus(activity, data)
                // TODO: Handle the error
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == CAMERA_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                tv_photo.visibility = View.INVISIBLE
                var out: File = File(context!!.getFilesDir(), "newImage.jpg")
                if (!out.exists()) {
                    activity?.toast("Error while capturing image")
                    return
                }
//                    val mBitmap : Bitmap = BitmapFactory.decodeFile(out.getAbsolutePath())
//                    iv_servicephoto.setImageBitmap(mBitmap)
                realPath = out.getAbsolutePath()
                mResults.add(0, realPath!!)

                if (realPath.isEmpty()) {
                    iv_servicephoto.setImageResource(R.drawable.queuelist_place_holder);
                } else {

                    Picasso.get().load("file://" + realPath).into(iv_servicephoto)
                }

                /*  Glide.with(activity)
                          .load("file://" + realPath)
  //                            .centerCrop().fitCenter()
                          .centerCrop()
                          .placeholder(R.drawable.iv_add_post_bg)
                          .error(R.drawable.iv_add_post_bg)
                          .diskCacheStrategy(DiskCacheStrategy.NONE)
                          .skipMemoryCache(true)
                          .into(iv_servicephoto)*/
            }
        } else if (requestCode == GALLARY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                if (getPickImageResultUri(data) != null) {
                    tv_photo.visibility = View.INVISIBLE
                    realPath = ImageFilePath.getPath(activity, data?.data)
                    mResults.add(0, realPath!!)

                    if (realPath.isEmpty()) {
                        iv_servicephoto.setImageResource(R.drawable.queuelist_place_holder);
                    } else {

                        Picasso.get().load("file://" + realPath).into(iv_servicephoto)
                    }


                    /* Glide.with(activity)
                             .load("file://" + realPath)
 //                            .centerCrop().fitCenter()
                             .centerCrop()
                             .placeholder(R.drawable.iv_add_post_bg)
                             .error(R.drawable.iv_add_post_bg)
                             .diskCacheStrategy(DiskCacheStrategy.NONE)
                             .skipMemoryCache(true)
                             .into(iv_servicephoto)*/
                } else {
                    val bitmap: Bitmap
                    tv_photo.visibility = View.INVISIBLE
                    if (null == data?.extras?.get("pushNotificationDataResponse")) {
                        return
                    }
                    bitmap = data.extras?.get("pushNotificationDataResponse") as Bitmap
                    myBitmap = bitmap
                    var bytes: ByteArrayOutputStream = ByteArrayOutputStream()
                    myBitmap!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
                    var destination: File = File(Environment.getExternalStorageDirectory(),
                            "${System.currentTimeMillis()}.jpg")
                    var fo: FileOutputStream
                    try {
                        destination.createNewFile()
                        fo = FileOutputStream(destination)
                        fo.write(bytes.toByteArray())
                        fo.close()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (iv_servicephoto != null) {
                        Picasso.get().load("file://" + destination).placeholder(R.drawable.iv_add_post_bg).into(iv_servicephoto)

/*
                        Glide.with(activity)
                                .load("file://" + destination)
//                                .centerCrop().fitCenter()
                                .centerCrop()
                                .placeholder(R.drawable.iv_add_post_bg)
                                .error(R.drawable.iv_add_post_bg)
                                .into(iv_servicephoto)
*/
                    }
                    mResults.add(0, destination.toString())
                }
            }
        }

    }

    override fun onLoginResult(message: Int, mesgDesc: String) {
    }

    override fun onCategoryApiSuccess(message: String, responseType: String) {
        hideProgressDialog()
        if (responseType == "success") {
            clearAllFields()
            if (dest1 != null) {
                var file = File(dest1)
                if (file.exists()) {
                    file.delete()
                }
            }
            activity!!.toast(Utils.getText(context, StringConstant.post_added_successfully))
            val resultIntent = Intent()
            resultIntent.putExtra("ADDPOST_REQUEST", "SERVICE")
            activity!!.setResult(Activity.RESULT_OK, resultIntent)
            activity!!.finish()
        } else {
            scrollView.pageScroll(View.FOCUS_UP);
            activity!!.toast(message)
        }
    }

    fun clearAllFields() {
        edt_title.setText("")
        edt_category.text = ""
        edt_subCategory.text = ""
        edt_service.setText("")
        // edt_quota.setText("")
        edt_tetoota_pt.setText("")
        //  edt_qualification.setText("")
        // edt_availability.setText("")
        edt_location.setText("")
        mResults.clear()
        tv_photo.visibility = View.VISIBLE
        edt_trading_pref.text = ""
        Glide.with(this@AddServiceFragment)
                .load(R.drawable.iv_add_post_bg)
//                .centerCrop().fitCenter()
                .centerCrop()
                .placeholder(R.drawable.iv_add_post_bg)
                .error(R.drawable.iv_add_post_bg)
                .into(iv_servicephoto)
    }

    override fun onClick(v: View?) {
        when (v) {
            rl_location -> {
                var intent: Intent =
                        PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .build(activity)
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
            }
            edt_location -> {
                var intent: Intent =
                        PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .build(activity)
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
            }

            iv_servicephoto -> {
                selectImage()
            }
            iv_toggle_service -> {
                if (!isServicePreferred) {
                    isServicePreferred = true
                    iv_toggle_service.setImageResource(R.drawable.tv_toggle_on)
                } else {
                    isServicePreferred = false
                    iv_toggle_service.setImageResource(R.drawable.tv_toggle_off)
                }
            }

            rl_trading_prefrence -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    if (!isTrendingServiceCall) {
                        isTrendingServiceCall = true
                        progressBar_trading.visibility = View.VISIBLE
                        mAddServicePresenter.getTredingPrefrenceData(this.activity!!)
                    }
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }

            /*   iv_addphotos -> {
               }*/
            rl_category -> {
                if (com.tetoota.utility.Utils.haveNetworkConnection(this.activity!!)) {
                    if (!isCategoryServiceCall) {
                        if (!edt_subCategory.equals("")) {
                            edt_subCategory.text = ""
                        }
                        isCategoryServiceCall = true
                        progressBar_category.visibility = View.VISIBLE
                        mCategoriesPresenter.getCategoriesData(this.activity!!)
                    }
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
            edt_category -> {
                if (com.tetoota.utility.Utils.haveNetworkConnection(this.activity!!)) {
                    if (!isCategoryServiceCall) {
                        if (!edt_subCategory.equals("")) {
                            edt_subCategory.text = ""
                        }
                        isCategoryServiceCall = true
                        progressBar_category.visibility = View.VISIBLE
                        mCategoriesPresenter.getCategoriesData(this.activity!!)
                    }
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
/*
            rl_setquota -> {
                var quota : ArrayList<String> = ArrayList<String>()
//                quota.add("Hour")
//                quota.add("Day")
//                quota.add("Week")
//                quota.add("Month")
//                quota.add("Session")
//                quota.add("Package")
                quota.add("1")
                quota.add("2")
                quota.add("3")
                quota.add("4")
                BottomSheetDialog.newInstance(quota as ArrayList<String>
                        , this@AddServiceFragment,
                        "quota", "")
                BottomSheetDialog().show(activity!!.supportFragmentManager, "dialog")
            }
*/
            point_layout -> {
                var quota: ArrayList<String> = ArrayList<String>()
                quota.add("Visit")
                quota.add("Session")
                quota.add("Hour")
                quota.add("Package")
                quota.add("Unit")
                quota.add("Day")
                BottomSheetDialog.newInstance(quota as ArrayList<String>
                        , this@AddServiceFragment,
                        "points", "")
                BottomSheetDialog().show(activity!!.supportFragmentManager, "dialog")
            }
            rl_subCategory -> {
                if (isCategoryServiceFilled) {
                    if (com.tetoota.utility.Utils.haveNetworkConnection(this.activity!!)) {
                        if (!isSubCategoryServiceCall) {
                            isSubCategoryServiceCall = true
                            progressBar_subCategory.visibility = View.VISIBLE

                            callSubCategory()
                        }
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                }
            }
            edt_subCategory -> {
                if (isCategoryServiceFilled) {
                    if (com.tetoota.utility.Utils.haveNetworkConnection(this.activity!!)) {
                        if (!isSubCategoryServiceCall) {
                            isSubCategoryServiceCall = true
                            progressBar_subCategory.visibility = View.VISIBLE

                            callSubCategory()
                        }
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                }
            }
            btn_submit_req -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    callSubmitApi("" + location?.latitude, "" + location?.longitude)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
            tv_tetoota_title_txt -> {
//                HintFragment.newInstance("title")
                val bottomSheet = HintFragment()
                val args = Bundle()
                args.putString("key", "title")
                bottomSheet.setArguments(args);
                bottomSheet.show(fragmentManager, "title")
                //                var pickpowerfrag = HintFragment()
//                fragmentManager?.beginTransaction()
//                        ?.add(R.id.frame, pickpowerfrag)
//                        ?.addToBackStack(null)
//                        ?.commit()
            }
            tv_tetoota_desc_txt -> {
//                newInstance("desc")
//                HintFragment.newInstance("desc")
                val bottomSheet = HintFragment()
                val args = Bundle()
                args.putString("key", "desc")
                bottomSheet.setArguments(args);
                bottomSheet.show(fragmentManager, "desc")

            }
            tv_tetoota_point_txt -> {
//                newInstance("point")
//                HintFragment.newInstance("point")
                val bottomSheet = HintFragment()
                val args = Bundle()
                args.putString("key", "point")
                bottomSheet.setArguments(args);
                bottomSheet.show(fragmentManager, "point")

            }
        }
    }

    var tetootaPt = ""
    private fun callSubmitApi(lat: String, long: String) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            // charFilter()
            tetootaPt = edt_tetoota_pt.text.toString().trim()
            if (!validate()) {
                hideProgressDialog()
                println("Device")
                //TODO VALIDATIONS
            } else {
                showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                val title = edt_title.text.toString().trim()
                val category = mCategoryId
                val subcategory = mSubCategoryId
                val service = edt_service.text.toString().trim()
                var quota = points_value.text.toString().trim()//edt_quota.text.toString().trim()
                //  val qualifications = edt_qualification.text.toString().trim()
                // val availibility = edt_availability.text.toString().trim()

                var tredingPrefrence: Int? = null
                if (mTradingPreId == null) {
                    tredingPrefrence = 1
                } else {
                    tredingPrefrence = mTradingPreId
                }

                tetootaPt = edt_tetoota_pt.text.toString().trim()
                val locations = edt_location.text.toString().trim()
//                quota = Utils.getText(activity, StringConstant.add_service_per_point) + " " + quota
                quota = "PER " + quota
                val map = hashMapOf<String, RequestBody>()
                val requestBodyTitle = Utils.createPartFromString(title)
                val requestBodyCategory = Utils.createPartFromString(category!!)
                val requestBodySubCategory = Utils.createPartFromString(subcategory!!)
//                val requestBodyLocation = Utils.createPartFromString(locations)
                val requestBodyLocation = Utils.createPartFromString("")
                val requestBodyService = Utils.createPartFromString(service)
                val requestBodyQuota = Utils.createPartFromString(quota)
                //   val requestBodyQuali = Utils.createPartFromString(qualifications)
                //  val requestBodyAvailbility = Utils.createPartFromString(availibility)
                val requestBodyTredingPref = Utils.createPartFromString(tredingPrefrence.toString())

                if (tetootaPt == "") {
                    tetootaPt = "0"
                } else {
                    tetootaPt = tetootaPt
                }


                val tetootaPT = Utils.createPartFromString(tetootaPt)
                var virtualService: String = ""
                if (isServicePreferred.equals(false)) {
                    virtualService = "no"
                } else {
                    virtualService = "yes"
                }
                val requestBodytetoota = Utils.createPartFromString(tetootaPt)


                val requestLat = Utils.createPartFromString(tetootaApplication!!.myLatitude.toString())
                val requestLng = Utils.createPartFromString(tetootaApplication!!.myLongitude.toString())
                val requestVirtualService = Utils.createPartFromString(virtualService)
                val requestBodyservice = Utils.createPartFromString("Services")
                val requestUserId = Utils.createPartFromString(Utils.loadPrefrence(Constant.USER_ID, "", activity))
                map.put("post_type", requestBodyservice)
                map.put("post_address", requestBodyLocation)
                map.put("user_id", requestUserId)
                map.put("title", requestBodyTitle)
                map.put("categories", requestBodyCategory)
                map.put("sub_categories", requestBodySubCategory)
                map.put("description", requestBodyService)
                map.put("set_quote", requestBodyQuota)
                //  map.put("qualifications", requestBodyQuali)
                //  map.put("availability", requestBodyAvailbility)
//                map.put("trading_preference", requestBodyTredingPref)
                map.put("set_tetoota_points", requestBodytetoota)

                map.put("latitude", requestLat)
                map.put("longitude", requestLng)
//                map.put("virtual_service", requestVirtualService)
                map.put("allowed_services", requestBodyTredingPref)
                map.put("user_status", Utils.createPartFromString("Public"))

                mAddServicePresenter.addService(this.activity!!, map, mResults)
            }
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            hideProgressDialog()
        }
    }

    fun selectImage() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library", "Cancel")
        var builder: AlertDialog.Builder = AlertDialog.Builder(this@AddServiceFragment.context)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            if (items[item].equals("Take Photo")) {
                cameraIntent()
            } else if (items[item].equals("Choose from Library")) {
                galleryIntent()
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun cameraIntent() {
//        val outputFileUri = getCaptureImageOutputUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
        /* if (outputFileUri != null)
             intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)*/
        activity!!.startActivityForResult(intent, CAMERA_RESULT)
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private fun getCaptureImageOutputUri(): Uri {
        var outputFileUri: Uri? = null
        val getImage = activity!!.externalCacheDir
        if (getImage != null) {
            outputFileUri = Uri.fromFile(File(getImage.path, "profile.png"))
        }
        return outputFileUri!!
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        activity!!.startActivityForResult(Intent.createChooser(intent, "Select File"), GALLARY_RESULT)
    }

    /**
     * Get the URI of the selected image from [.getPickImageChooserIntent].

     * Will return the correct URI for camera and gallery image.

     * @param data the returned pushNotificationDataResponse of the activity result
     */
    fun getPickImageResultUri(data: Intent?): Uri? {
        var isCamera = true
        if (data != null) {
            val action = data.action
            isCamera = action != null && action == MediaStore.ACTION_IMAGE_CAPTURE
            if (isCamera) {
                return getCaptureImageOutputUri()
            } else {
                return data.data
            }
        }
        return null
    }

    override fun onDeepLinkingResult() {

    }

    fun newInstance(type: String): HintFragment {
        val fragment = HintFragment()
        var bundle = Bundle()
        bundle.putString("type", type)
        fragment.arguments = bundle

        return fragment


    }

}