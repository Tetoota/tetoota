package com.tetoota.addrequest

import com.tetoota.network.errorModel.Meta

data class AddServiceDataResponse(
        val data: List<Any>? = null,
        val meta: Meta? = null

)