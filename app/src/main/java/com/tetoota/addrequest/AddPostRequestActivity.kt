package com.tetoota.addrequest

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.MenuItem
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_add_post_request.*
import kotlinx.android.synthetic.main.toolbar_layout.*

import org.jetbrains.anko.onClick

class AddPostRequestActivity : BaseActivity() {
    private val mAddServiceFragment: AddServiceFragment by lazy {
        AddServiceFragment()
    }
    private val mAddServiceProduct: AddProductFragment by lazy {
        AddProductFragment()
    }
    private val mAddWishlistProduct: AddWishListFragment by lazy {
        AddWishListFragment()
    }

    // var iSendDataToFragment : ISendDataToFragmentListener;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        setContentView(R.layout.activity_add_post_request)
        initViews()
    }
    override fun onStart() {
        super.onStart()
    }
    private fun initViews() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent.getStringExtra("Tab") == "wishListTab") {
            toolbar_title.text = Utils.getText(this,StringConstant.str_add_wishlist)
        }else   toolbar_title.text = Utils.getText(this,StringConstant.add_service_add_request)

        iv_close.onClick { onBackPressed() }
/*
        launch(CommonPool) {
            delay(50)
            runOnUiThread(Runnable {
                Log.e("initViews() ","kamal")

               // setupViewPager(view_pager_container)
            })
        }
*/
        Handler().postDelayed(Runnable
        {
            setupViewPager(view_pager_container)

        }, 50)

        tab_layout.setupWithViewPager(view_pager_container)
        view_pager_container.disableScroll(false)
    }
    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }
    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@AddPostRequestActivity)
        finish()
    }
    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, AddPostRequestActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }
    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(mAddServiceFragment, "${Utils.getText(this,StringConstant.services)}")
        adapter.addFragment(mAddServiceProduct, "${Utils.getText(this,StringConstant.product)}")
        adapter.addFragment(mAddWishlistProduct, "${Utils.getText(this,StringConstant.wishlist)}")

        viewPager.adapter = adapter
        view_pager_container.disableScroll(true)
        if (intent.getStringExtra("Tab") == "serviceTab") {
            view_pager_container.currentItem = 0
        } else if (intent.getStringExtra("Tab") == "productTab") {
            view_pager_container.currentItem = 1
        } else if (intent.getStringExtra("Tab") == "wishListTab") {
            view_pager_container.currentItem = 2
        }
    }
    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private var mFragmentList: MutableList<Fragment> = ArrayList()
        private var mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }
        override fun getCount(): Int {
            return mFragmentList.size
        }
        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }
        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((requestCode == mAddServiceFragment.GALLARY_RESULT || (requestCode == mAddServiceFragment.CAMERA_RESULT)) && resultCode == RESULT_OK) {
            mAddServiceFragment.onActivityResult(requestCode, resultCode, data)
        } else if ((requestCode == mAddServiceProduct.GALLARY_RESULT) || (requestCode == mAddServiceProduct.CAMERA_RESULT) && resultCode == RESULT_OK) {
            mAddServiceProduct.onActivityResult(requestCode, resultCode, data)
        } else if ((requestCode == mAddWishlistProduct.GALLARY_RESULT) || (requestCode == mAddWishlistProduct.CAMERA_RESULT) && resultCode == RESULT_OK) {
            mAddWishlistProduct.onActivityResult(requestCode, resultCode, data)
        }
    }
}
