package com.tetoota.addrequest

import android.app.Activity
import com.tetoota.fragment.home.model.DataItem
import okhttp3.RequestBody
import java.time.Duration

/**
 * Created by charchit.kasliwal on 11-07-2017.
 */
class AddRequestContract {

    interface View {
        fun onCategoryApiSuccess(message: String, responseType : String)
        fun onApiSuccess(mCategoriesList: ArrayList<Any>, message: String)
        fun onApiFailure(message: String)
        fun onDeletePostApiSuccess(message: String)
        fun onEditDataListener(message: String){}
    }

    internal interface Presenter {
        fun getTredingPrefrenceData(mActivity : Activity)
        fun getCategoryData(mActivity : Activity)
        fun addService(mActivity: Activity, map: HashMap<String, RequestBody>, mImageList: List<String>)
        fun editService(mActivity: Activity, map: HashMap<String, RequestBody>, mImageList: List<String>){}
        fun deleteService(mActivity: Activity,userId : String, postId : String)

    }

    interface addRequestApiListener {
        fun onCategoryListener(message: String,  responseType : String)
        fun onSuccess(mDataList: ArrayList<Any>, message: String)
        fun onDeletePostFailure(message: String)
        fun onDeletePostSuccess(message: String)
        fun editDataListener(message: String){}

    }
}