package com.tetoota.addrequest

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.categories.CategoriesContract
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.categories.CategoriesPresenter
import com.tetoota.customviews.ImageFilePath
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.subcategories.DataItem
import com.tetoota.subcategories.SubCategoriesContract
import com.tetoota.subcategories.SubCategoriesPresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_add__service.*
import kotlinx.android.synthetic.main.fragment_add_product.view.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import okhttp3.RequestBody
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

/**
 * Created by charchit.kasliwal on 31-10-2017.
 */
class EditProductActivity : BaseActivity(), AddRequestContract.View, CategoriesContract.View, SubCategoriesContract.View, View.OnClickListener,
        BottomSheetDialog.IBottomSheetListener {
    override fun onMobileSuccess(message: Int, mesgDesc: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    var mResults: ArrayList<String> = arrayListOf()
    var PLACE_AUTOCOMPLETE_REQUEST_CODE = 1
    private var isServicePreferred: Boolean = false
    private var isTrendingServiceCall: Boolean = false
    private var isCategoryServiceCall: Boolean = false
    private var isCategoryServiceFilled: Boolean = false
    private var isSubCategoryServiceCall: Boolean = false
    var dest1: String? = null
    //    var location: LatLng? = null
    var lat: Double? = null
    var lng: Double? = null
    var mCategoryId: String? = null
    var mSubCategoryId: String? = null
    var mTradingId: Int? = null
    var mTradingPreId: Int? = null
    var myBitmap: Bitmap? = null
    var picUri: Uri? = null
    private var postId: String? = null
    private val mAddServicePresenter: AddRequestPresenter by lazy {
        AddRequestPresenter(this@EditProductActivity)
    }
    private val mCategoriesPresenter: CategoriesPresenter by lazy {
        CategoriesPresenter(this@EditProductActivity)
    }
    private val mSubCAtegoryPresenter: SubCategoriesPresenter by lazy {
        SubCategoriesPresenter(this@EditProductActivity)
    }
    private var tetootaApplication: TetootaApplication? = null


    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_add_product)
        tetootaApplication = this!!.applicationContext as TetootaApplication

        edt_title.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val length = edt_title.length()
                val convert = length.toString()
                txt_count30.setText(convert)
                if (edt_title.length() >= 30) {
                    ll_text_count30.visibility = View.GONE
                } else {
                    ll_text_count30.visibility = View.VISIBLE
                }


            }

            override fun afterTextChanged(s: Editable) {}
        })

        edt_service.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val length = edt_service.length()
                val convert = length.toString()
                txt_count.setText(convert)
                if (edt_service.length() >= 50) {
                    ll_text_count.visibility = View.GONE
                } else {
                    ll_text_count.visibility = View.VISIBLE
                }


            }

            override fun afterTextChanged(s: Editable) {}
        })

        initViews()
    }

    private fun initToolbar() {
        app_bar.visibility = View.VISIBLE
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar_title.text = Utils.getText(this, StringConstant.add_service_edit_product)
        iv_close.onClick { onBackPressed() }
    }


    private fun initViews() {
        initToolbar()
        /*     val layoutManager = GridLayoutManager(AccountKitController.getApplicationContext(), 5)
             rv_photos.visibility = View.VISIBLE
             rv_photos.layoutManager = layoutManager
             rv_photos.itemAnimator = DefaultItemAnimator()
             rv_photos.itemAnimator = DefaultItemAnimator()*/
        btn_submit_req.setOnClickListener(this)
        iv_toggle_service.setOnClickListener(this)
        rl_loc.setOnClickListener(this)
        edt_location.setOnClickListener(this)
        //  quota_layout.setOnClickListener(this)
        point_layout.setOnClickListener(this)
        rl_trading_prefrence.setOnClickListener(this)
        // iv_addphotos.setOnClickListener(this)
        iv_servicephoto.setOnClickListener(this)
        rl_category.setOnClickListener(this)
        rl_subCategory.setOnClickListener(this)
        edt_category.setOnClickListener(this)
        edt_subCategory.setOnClickListener(this)
        progressBar_category.visibility = View.GONE
        progressBar_trading.visibility = View.GONE
        iv_toggle_service.setImageResource(R.drawable.tv_toggle_off)
        setMultiLanguageText()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@EditProductActivity)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if (id == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, EditProductActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    private fun getDataFromPreviousActivity() {
        if (intent.getParcelableExtra<ServicesDataResponse>("mServiceData") != null) {
            val mServiceData = intent.getParcelableExtra<ServicesDataResponse>("mServiceData")
            edt_title.setText(mServiceData.title)
            edt_category.text = mServiceData.category_name
            edt_subCategory.text = mServiceData.sub_category_name
            edt_service.setText(mServiceData.content.toString())
            // edt_quota.setText(mServiceData.allowed_services)
            // edt_qualification.setText(mServiceData.qualification)
            //  edt_availability.setText(mServiceData.availability)
            postId = mServiceData.id.toString()
            lat = mServiceData.Lat
            lng = mServiceData.Long
//            val trading_prefrences: Array<String> = resources.getStringArray(R.array.trading_prefrences)
            if (mServiceData?.trading_preference.toString() == "1") {
                edt_trading_pref.text = Utils.getText(this, StringConstant.add_service_fine_by_both)
            } else if (mServiceData?.trading_preference.toString() == "2") {
                edt_trading_pref.text = Utils.getText(this, StringConstant.add_service_paid_by_point)
            } else if (mServiceData?.trading_preference.toString() == "3") {
                edt_trading_pref.text = Utils.getText(this, StringConstant.add_service_exchange_service)
            }
            if (mServiceData.virtual_service == "Yes") {
                iv_toggle_service.setImageResource(R.drawable.tv_toggle_on)
            } else {
                iv_toggle_service.setImageResource(R.drawable.tv_toggle_off)
            }

            edt_tetoota_pt.setText(mServiceData.tetoota_points)
            edt_location.setText(mServiceData.post_address)

            var url: String = "";
            if (mServiceData!!.image != null && mServiceData.image_thumb != null) {
                url = Utils.getUrl(this, mServiceData.image!!, mServiceData.image_thumb, false)
            } else {
                url = Utils.getUrl(this, mServiceData.image!!)
            }

            Glide.with(this@EditProductActivity)
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.queuelist_place_holder)
                    .error(R.drawable.queuelist_place_holder)
                    .into(iv_servicephoto)
        }
    }

    private fun setMultiLanguageText() {
        tv_title.text = Utils.getText(this, StringConstant.title)
        tv_service.text = Utils.getText(this, StringConstant.add_service_describe_product)
        tv_category.text = Utils.getText(this, StringConstant.category)
        tv_subCategory.text = Utils.getText(this, StringConstant.add_service_subcategory_text)
        // tv_set_quota.text = Utils.getText(this,StringConstant.str_quota)
        //  tv_qualification.text = Utils.getText(this,StringConstant.str_qualitification)
        // tv_availbility.text = Utils.getText(this,StringConstant.str_availability)
        tv_location.text = Utils.getText(this, StringConstant.str_locations)
        edt_category.hint = Utils.getText(this, StringConstant.str_choose_the_most)
        edt_subCategory.hint = Utils.getText(this, StringConstant.str_choose_the_most_subcategory)
        edt_title.hint = Utils.getText(this, StringConstant.str_put_a_very_specific_heading)
        edt_service.hint = Utils.getText(this, StringConstant.write_service_detail)
        //  edt_qualification.hint = Utils.getText(this,StringConstant.write_certificate_degree)
        // edt_availability.hint = Utils.getText(this,StringConstant.tell_when_you_provide_service)
        btn_submit_req.text = Utils.getText(this, StringConstant.btn_submit_req)
        tv_switch_btn.text = Utils.getText(this, StringConstant.switch_btn_perform_virtual)
        tv_service_preferred.text = Utils.getText(this, StringConstant.tv_service_preferred)
        tv_photo.text = Utils.getText(this, StringConstant.upload_picture_auto)
        tv_photos.text = Utils.getText(this, StringConstant.tv_photos)
        edt_location.hint = Utils.getText(this, StringConstant.edt_location)
        edt_trading_pref.hint = Utils.getText(this, StringConstant.edt_trading_pref)
        tv_trading_pref.text = Utils.getText(this, StringConstant.tv_trading_pref)
        // edt_quota.hint = Utils.getText(this,StringConstant.how_many_times_service)
        edt_tetoota_pt.hint = Utils.getText(this, StringConstant.edt_tetoota_pt)
        tv_tetoota_point.text = Utils.getText(this, StringConstant.tv_tetoota_point)
        getDataFromPreviousActivity()
    }

    private fun validate(): Boolean {
        if (edt_title.text.toString().trim().isEmpty() || edt_title.text.toString().length < 10) {
            toast(Utils.getText(this, StringConstant.myprofile_write_yourself_min30))
            return false
        }
//        else if (edt_category.text.toString().trim().isEmpty()) {
//            toast(Utils.getText(this,StringConstant.add_service_category_alert))
//            return false
//        } else if (edt_subCategory.text.toString().trim().isEmpty()) {
//            toast(Utils.getText(this,StringConstant.add_service_subcategory_alert))
//            return false
//        }
        else if (edt_service.text.toString().trim().isEmpty() || edt_service.text.toString().length <= 50) {
            toast(Utils.getText(this, StringConstant.myprofile_write_description_min50))
            return false
        }
        /*    else if (edt_quota.text.toString().trim().isEmpty()) {
                toast(Utils.getText(this,StringConstant.add_service_quota_alert))
                return false
            }*/
        /*  else if (edt_availability.text.toString().trim().isEmpty()) {
              toast(Utils.getText(this,StringConstant.add_service_availability_alert))
              return false
          } */
//        else if (edt_trading_pref.text.toString().trim().isEmpty()) {
//            toast(Utils.getText(this, StringConstant.add_service_trading_alert))
//            return false
//        }
//        else if (edt_location.text.toString().trim().isEmpty()) {
//            toast(Utils.getText(this, StringConstant.add_service_location_alert))
//            return false
//        }
        else if (mTradingPreId != 3 && (edt_tetoota_pt.text.toString().trim().isEmpty() || edt_tetoota_pt.text.toString().trim() == "0")) {
            toast(Utils.getText(this, StringConstant.add_service_point_alert))
            return false
        } else if (mTradingPreId != 3 && edt_tetoota_pt.text.toString().trim() != "") {
            val probInt = Integer.parseInt(edt_tetoota_pt.text.toString())
            if (probInt > 130) {
                toast(Utils.getText(this, StringConstant.add_max_service_point_alert))
                return false
            }

            return true
        }
/*
        if (mTradingId != 2 && mTradingId != null) {
            if (edt_tetoota_pt.text.toString().trim().isEmpty() || edt_tetoota_pt.text.toString().trim() == "0") {
                toast(Utils.getText(this,StringConstant.add_service_point_alert))
                return false
            }
        }
*/
        return true
    }

    override fun onApiSuccess(mTredingList: ArrayList<Any>, message: String) {
        progressBar_trading.visibility = View.GONE
        isTrendingServiceCall = false
        BottomSheetDialog.newInstance1(mTredingList as ArrayList<TradingDataResponse>,
                this@EditProductActivity, "trading")
        BottomSheetDialog().show(this@EditProductActivity.supportFragmentManager, "dialog")
    }

    override fun onDeletePostApiSuccess(message: String) {
        progressBar_trading.visibility = View.GONE
        isTrendingServiceCall = false
    }

    override fun onApiFailure(message: String) {
        progressBar_trading.visibility = View.GONE
        isTrendingServiceCall = false
    }

    override fun onStop() {
        super.onStop()
    }

    private fun callSubCategory() {
        var myNumOrNull: Int? = mCategoryId?.toIntOrNull()
        mSubCAtegoryPresenter.getSubCategoriesData(myNumOrNull!!, this)
    }

    override fun dataClick(mTredingDataResponse: Any, type: String) {
        if (type == "category") {
            val mTradingData = mTredingDataResponse as CategoriesDataResponse
            edt_category.text = mTradingData.category_name
            mCategoryId = mTradingData.category_id.toString()
            isCategoryServiceFilled = true
//            callSubCategory()
        } else if (type == "subcategory") {
            val mTradingData = mTredingDataResponse as DataItem
            edt_subCategory.text = mTradingData.sub_category_name
            mSubCategoryId = mTradingData.sub_category_id.toString()
        } else if (type == "points") {
            val mTradingData = mTredingDataResponse as String
            Thread.sleep(200)
            points_value.text = mTradingData
        } else {
            val mTradingData = mTredingDataResponse as TradingDataResponse
            Log.e("bbbbbbbbbbbb", "" + mTradingData)
            mTradingPreId = mTradingData.id
            Log.e("bbbiiiiiiiiiiiiii", "" + mTradingPreId)
            edt_trading_pref.text = mTradingData.description
        }
    }

    override fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String) {
        isCategoryServiceCall = false
//        tv_photo.visibility = View.VISIBLE
        progressBar_category.visibility = View.GONE
        BottomSheetDialog.newInstance(mCategoriesList as ArrayList<CategoriesDataResponse>, this,
                "categories")
        BottomSheetDialog().show(this.supportFragmentManager, "dialog")
    }

    override fun onCatergoriesFailureResult(message: String) {
        isCategoryServiceCall = false
        progressBar_category.visibility = View.GONE
        if (this != null) {
            this.toast(message)
        }
    }

    override fun onLoginResult(message: Int, mesgDesc: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDeepLinkingResult() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSubCategoriesSuccessResult(mSubCategoriesList: List<DataItem>, message: String) {
        isSubCategoryServiceCall = false
        progressBar_subCategory.visibility = View.GONE
        BottomSheetDialog.newSubCategoryInstance(mSubCategoriesList as ArrayList<DataItem>, this,
                "subcategory")
        BottomSheetDialog().show(this!!.supportFragmentManager, "dialog")
    }

    override fun onSubCatergoriesFailureResult(message: String) {
        isSubCategoryServiceCall = false
        progressBar_subCategory.visibility = View.GONE
        if (this != null) {
            this.toast(message)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                var place: Place = PlaceAutocomplete.getPlace(this@EditProductActivity, data)
                edt_location.setText(place.address)
                lat = place.latLng.latitude
                lng = place.latLng.longitude
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                var status: Status = PlaceAutocomplete.getStatus(this@EditProductActivity, data)
                // TODO: Handle the error
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == 200) {
            val bitmap: Bitmap
            if (resultCode == Activity.RESULT_OK) {
                if (getPickImageResultUri(data) != null) {
                    tv_photo.visibility = View.INVISIBLE
                    val realPath = ImageFilePath.getPath(this@EditProductActivity, data?.data)
                    mResults.add(0, realPath!!)
                    Glide.with(this@EditProductActivity)
                            .load("file://" + realPath)
//                            .centerCrop().fitCenter()
                            .centerCrop()
                            .placeholder(R.drawable.iv_add_post_bg)
                            .error(R.drawable.iv_add_post_bg)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(iv_servicephoto)
                } else {
                    tv_photo.visibility = View.INVISIBLE
                    bitmap = data?.extras?.get("pushNotificationDataResponse") as Bitmap
                    myBitmap = bitmap
                    var bytes: ByteArrayOutputStream = ByteArrayOutputStream()
                    myBitmap!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
                    var destination: File = File(Environment.getExternalStorageDirectory(),
                            "${System.currentTimeMillis()}.jpg")
                    var fo: FileOutputStream
                    try {
                        destination.createNewFile()
                        fo = FileOutputStream(destination)
                        fo.write(bytes.toByteArray())
                        fo.close()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (iv_servicephoto != null) {
                        Glide.with(this@EditProductActivity)
                                .load("file://" + destination)
//                                .centerCrop().fitCenter()
                                .centerCrop()
                                .placeholder(R.drawable.iv_add_post_bg)
                                .error(R.drawable.iv_add_post_bg)
                                .into(iv_servicephoto)
                    }
                    mResults.add(0, destination.toString())

                }
            }
        }

    }

    override fun onCategoryApiSuccess(message: String, responseType: String) {
        hideProgressDialog()
        if (responseType == "success") {

        } else {
            toast(message)
        }
    }

    override fun onEditDataListener(message: String) {
        super.onEditDataListener(message)
        hideProgressDialog()
        clearAllFields()
        if (dest1 != null) {
            var file = File(dest1)
            if (file.exists()) {
                file.delete()
            }
        }
//        val intent = Intent(this@EditProductActivity, ServiceProductActivity::class.java)
//        intent?.putExtra("Tab", "serviceTab")
//        startActivity(intent)
//        ActivityStack.cleareAll()
//        finish()
        toast(Utils.getText(this, StringConstant.post_edited_successfully))
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun clearAllFields() {
        edt_title.setText("")
        edt_category.text = ""
        edt_service.setText("")
        // edt_quota.setText("")
        edt_tetoota_pt.setText("")
        //  edt_qualification.setText("")
        //  edt_availability.setText("")
        edt_location.setText("")
        mResults.clear()
        tv_photo.visibility = View.VISIBLE
        edt_trading_pref.text = ""
        Glide.with(this@EditProductActivity)
                .load(R.drawable.iv_add_post_bg)
//                .centerCrop().fitCenter()
                .centerCrop()
                .placeholder(R.drawable.iv_add_post_bg)
                .error(R.drawable.iv_add_post_bg)
                .into(iv_servicephoto)
    }

    override fun onClick(v: View?) {
        when (v) {
            rl_loc -> {
                var intent: Intent =
                        PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .build(this@EditProductActivity)
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
            }
            edt_location -> {
                var intent: Intent =
                        PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .build(this@EditProductActivity)
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
            }
            iv_servicephoto -> {
                selectImage()
            }
            iv_toggle_service -> {
                if (!isServicePreferred) {
                    isServicePreferred = true
                    iv_toggle_service.setImageResource(R.drawable.tv_toggle_on)
                } else {
                    isServicePreferred = false
                    iv_toggle_service.setImageResource(R.drawable.tv_toggle_off)
                }
            }

            rl_trading_prefrence -> {
                if (com.tetoota.utility.Utils.haveNetworkConnection(this@EditProductActivity)) {
                    if (!isTrendingServiceCall) {
                        isTrendingServiceCall = true
                        progressBar_trading.visibility = View.VISIBLE
                        mAddServicePresenter.getTredingPrefrenceData(this@EditProductActivity)
                    }
                } else {
                    toast(Utils.getText(this@EditProductActivity, StringConstant.str_check_internet))
                }

                //showSnackBar("You can not change service prefrence")
            }
            rl_category -> {
                if (com.tetoota.utility.Utils.haveNetworkConnection(this@EditProductActivity)) {
                    if (!isCategoryServiceCall) {
                        if (!edt_subCategory.equals("")) {
                            edt_subCategory.text = ""
                        }
                        isCategoryServiceCall = true
                        progressBar_category.visibility = View.VISIBLE
                        mCategoriesPresenter.getCategoriesData(this@EditProductActivity)
                    }
                } else {
                    toast(Utils.getText(this@EditProductActivity, StringConstant.str_check_internet))
                }

                // showSnackBar("You can not change service category")
            }
            edt_category -> {
                if (com.tetoota.utility.Utils.haveNetworkConnection(this@EditProductActivity)) {
                    if (!isCategoryServiceCall) {
                        if (!edt_subCategory.equals("")) {
                            edt_subCategory.text = ""
                        }
                        isCategoryServiceCall = true
                        progressBar_category.visibility = View.VISIBLE
                        mCategoriesPresenter.getCategoriesData(this@EditProductActivity)
                    }
                } else {
                    this.toast(Utils.getText(this, StringConstant.str_check_internet))
                }
            }

            rl_subCategory -> {
                if (isCategoryServiceFilled) {
                    if (com.tetoota.utility.Utils.haveNetworkConnection(this@EditProductActivity)) {
                        if (!isSubCategoryServiceCall) {
                            isSubCategoryServiceCall = true
                            progressBar_subCategory.visibility = View.VISIBLE

                            callSubCategory()
                        }
                    } else {
                        toast(Utils.getText(this@EditProductActivity, StringConstant.str_check_internet))
                    }
                }
            }
            edt_subCategory -> {
                if (isCategoryServiceFilled) {
                    if (com.tetoota.utility.Utils.haveNetworkConnection(this@EditProductActivity)) {
                        if (!isSubCategoryServiceCall) {
                            isSubCategoryServiceCall = true
                            progressBar_subCategory.visibility = View.VISIBLE

                            callSubCategory()
                        }
                    } else {
                        toast(Utils.getText(this@EditProductActivity, StringConstant.str_check_internet))
                    }
                }
            }

            btn_submit_req -> {
                if (Utils.haveNetworkConnection(this@EditProductActivity)) {
                    callSubmitApi(this.lat!!, this.lng!!)
                }
            }

/*
            quota_layout -> {
                var quota : ArrayList<String> = ArrayList<String>()
                quota.add("1")
                quota.add("2")
                quota.add("3")
                quota.add("4")

                BottomSheetDialog.newInstance(quota as ArrayList<String>
                        , this,
                        "quota", "")
                BottomSheetDialog().show(this!!.supportFragmentManager, "dialog")
            }
*/
            point_layout -> {
                var quota: ArrayList<String> = ArrayList<String>()
                quota.add("Visit")
                quota.add("Session")
                quota.add("Hours")
                quota.add("Pacakge")
                quota.add("Unit")
                quota.add("Day")
                BottomSheetDialog.newInstance(quota as ArrayList<String>
                        , this,
                        "points", "")
                BottomSheetDialog().show(this.supportFragmentManager, "dialog")
            }
        }
    }

    private fun callSubmitApi(lat: Double, long: Double) {
        if (Utils.haveNetworkConnection(this@EditProductActivity)) {
            if (!validate()) {
                hideProgressDialog()
                println("Device")
                //TODO VALIDATIONS
            } else {
                showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                val title = edt_title.text.toString().trim()
                val category: String?
                if (mCategoryId == null) {
                    category = ""
                } else {
                    category = mCategoryId
                }
                val subcategory: String?
                if (mCategoryId == null) {
                    subcategory = ""
                } else {
                    subcategory = mSubCategoryId
                }
                val service = edt_service.text.toString().trim()
                var quota = points_value.text.toString().trim()
                // val qualifications = edt_qualification.text.toString().trim()
                // val availibility = edt_availability.text.toString().trim()
                //  val tredingPrefrence = edt_quota.text.toString().trim()
                var tetootaPt = edt_tetoota_pt.text.toString().trim()
                val locations = edt_location.text.toString().trim()
                val postId = postId
                quota = Utils.getText(this, StringConstant.add_service_per_point) + " " + quota
                val map = hashMapOf<String, RequestBody>()
                val id = Utils.createPartFromString(postId!!)
                val requestBodyTitle = Utils.createPartFromString(title)
                // val requestBodyCategory = Utils.createPartFromString(category!!)
                val requestBodyLocation = Utils.createPartFromString(locations)
                val requestBodyService = Utils.createPartFromString(service)
                val requestBodyQuota = Utils.createPartFromString(quota)
                // val requestBodyQuali = Utils.createPartFromString(qualifications)
                //  val requestBodyAvailbility = Utils.createPartFromString(availibility)
                // val requestBodyTredingPref = Utils.createPartFromString(tredingPrefrence.toString())
                if (tetootaPt == "") {
                    tetootaPt = "0"
                } else {
                    tetootaPt = tetootaPt
                }
                var virtualService: String = ""
                if (isServicePreferred.equals(false)) {
                    virtualService = "no"
                } else {
                    virtualService = "yes"
                }

                val requestBodytetoota = Utils.createPartFromString(tetootaPt)
                val requestLat = Utils.createPartFromString(tetootaApplication!!.myLatitude.toString())
                val requestLng = Utils.createPartFromString(tetootaApplication!!.myLongitude.toString())
                val postType = Utils.createPartFromString("Services")
                val requestVirtualService = Utils.createPartFromString(virtualService)
                val requestUserId = Utils.createPartFromString(Utils.loadPrefrence(Constant.USER_ID, "", this@EditProductActivity))
                map.put("post_address", requestBodyLocation)
                map.put("user_id", requestUserId)
                map.put("post_id", id)
                map.put("title", requestBodyTitle)
                // map.put("categories", requestBodyCategory)
                map.put("description", requestBodyService)
                map.put("set_quote", requestBodyQuota)
                //  map.put("qualifications", requestBodyQuali)
                //   map.put("availability", requestBodyAvailbility)
                // map.put("trading_preference", requestBodyTredingPref)
                map.put("set_tetoota_points", requestBodytetoota)
                map.put("latitude", requestLat)
                map.put("longitude", requestLng)
                map.put("virtual_service", requestVirtualService)
                //  map.put("allowed_services", requestBodyTredingPref)
                mAddServicePresenter.editService(this@EditProductActivity, map, mResults)
            }
        } else {
            hideProgressDialog()
        }
    }

    fun selectImage() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library", "Cancel")
        var builder: AlertDialog.Builder = AlertDialog.Builder(this@EditProductActivity)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            if (items[item].equals("Take Photo")) {
                cameraIntent()
            } else if (items[item].equals("Choose from Library")) {
                galleryIntent()
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun cameraIntent() {
        val outputFileUri = getCaptureImageOutputUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        /* if (outputFileUri != null)
             intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)*/
        startActivityForResult(intent, 200)
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private fun getCaptureImageOutputUri(): Uri {
        var outputFileUri: Uri? = null
        val getImage = externalCacheDir
        if (getImage != null) {
            outputFileUri = Uri.fromFile(File(getImage.path, "profile.png"))
        }
        return outputFileUri!!
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select File"), 200)
    }

    /**
     * Get the URI of the selected image from [.getPickImageChooserIntent].

     * Will return the correct URI for camera and gallery image.

     * @param data the returned pushNotificationDataResponse of the activity result
     */
    private fun getPickImageResultUri(data: Intent?): Uri? {
        var isCamera = true
        if (data != null) {
            val action = data.action
            isCamera = action != null && action == MediaStore.ACTION_IMAGE_CAPTURE
        }
        if (isCamera) {
            return getCaptureImageOutputUri()
        } else {
            return data?.data
        }
    }


}// Required empty public constructor
