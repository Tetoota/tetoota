package com.tetoota.network.errorModel

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class ErrorResponse(var data: List<Any>, var meta: Meta)