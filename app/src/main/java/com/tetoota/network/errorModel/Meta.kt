package com.tetoota.network.errorModel

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
open class Meta(var code: String, var status: String, var message: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString().toString(),
            source.readString().toString(),
            source.readString().toString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(code)
        writeString(status)
        writeString(message)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Meta> = object : Parcelable.Creator<Meta> {
            override fun createFromParcel(source: Parcel): Meta = Meta(source)
            override fun newArray(size: Int): Array<Meta?> = arrayOfNulls(size)
        }
    }
}