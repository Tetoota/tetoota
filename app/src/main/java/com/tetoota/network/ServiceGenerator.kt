package com.tetoota.network

import android.text.TextUtils
import com.google.gson.GsonBuilder
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class ServiceGenerator {
    companion object service {
        private var builder: Retrofit.Builder? = null
        private val httpClient = OkHttpClient.Builder()
        private var retrofit: Retrofit? = null
        private val DIRECTORY_SEPERATOR = "/"
        private val APP_VERSION = "v1"//test
        private val API_DIRECTORY = "api"// App Version directory on server
        //  private val BASE_URL = "http://tetoota.hiteshi.com" // App Version directory on server
        // private val BASE_URL = "http://tetoota.com" // App Version directory on server
        // private val BASE_URL = "http://182.70.251.155/tetoota"

//         private val BASE_URL = "http://app1.tetoota.com/" //Live

        private val BASE_URL = "http://tetoota.com" //test


        // private val BASE_URL = "http://tetoota.com"
        //        http://maps.googleapis.com/maps/api/geocode/json?address=ADDRESS&sensor=false
        private val googlePlacesBaseUrl = "http://maps.googleapis.com/maps/api/geocode/"
//        private val googlePlacesBaseUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=Rajwada%20Indore%20M.%20P.&sensor=false/"
        //Ref Url

        private var API_URL = BASE_URL + DIRECTORY_SEPERATOR + API_DIRECTORY + DIRECTORY_SEPERATOR +
                APP_VERSION + DIRECTORY_SEPERATOR
        private var API_GOOGLETRASLATE_URL = "https://translation.googleapis.com/language/translate/"

        fun changeApiBaseUrl(isAppBaseUrl: Boolean) {
            if (isAppBaseUrl) {
                API_URL = BASE_URL + DIRECTORY_SEPERATOR + API_DIRECTORY + DIRECTORY_SEPERATOR +
                        APP_VERSION + DIRECTORY_SEPERATOR
            } else {
//                API_URL = googlePlacesBaseUrl
                API_URL = API_GOOGLETRASLATE_URL
            }
        }

        fun <S> createService(serviceClass: Class<S>, username: String?, password: String?): S {
            if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
                val authToken = Credentials.basic(username!!, password!!)
                return createService(serviceClass, authToken)
            }
            return createService(serviceClass, null, null)
        }

        fun <S> createServiceForGoogleTranslate(serviceClass: Class<S>): S {
            val logging = HttpLoggingInterceptor()
            // set your desired log level BODY
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
            val gson = GsonBuilder()
                    .setLenient()
                    .create()
            builder = Retrofit.Builder()
                    .baseUrl(API_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))

            retrofit = builder!!.build()

            return retrofit!!.create(serviceClass)
        }

        fun <S> createService(serviceClass: Class<S>, authToken: String): S {
            val logging = HttpLoggingInterceptor()
            // set your desired log level
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
            val gson = GsonBuilder()
                    .setLenient()
                    .create()
            builder = Retrofit.Builder()
                    .baseUrl(API_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create(gson))

            if (!TextUtils.isEmpty(authToken)) {
                val interceptor = AuthenticationInterceptor(authToken)
                if (!httpClient.interceptors().contains(interceptor)) {
                    httpClient.addInterceptor(interceptor)
                    builder!!.client(httpClient.build())
                    retrofit = builder!!.build()
                } else {
                    retrofit = builder!!.build()
                }
            }
            return retrofit!!.create(serviceClass)
        }
    }
}