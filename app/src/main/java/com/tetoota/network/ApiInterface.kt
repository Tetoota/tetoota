package com.tetoota.listener

import com.google.gson.JsonObject
import com.tetoota.addrequest.AddServiceDataResponse
import com.tetoota.addrequest.TradingResponse
import com.tetoota.appintro.WalkThroughResponse
import com.tetoota.categories.CategoriesResponse
import com.tetoota.categories.DeepLinkingResponse
import com.tetoota.details.ViewProductResponse
import com.tetoota.fragment.corporateLogin.data.CompanyDataResponse
import com.tetoota.fragment.corporateLogin.data.SubmitCorporateLoginResponse
import com.tetoota.fragment.couponcode.SharePointsResponse
import com.tetoota.fragment.couponcode.coupon_code.CouponCodeDataResponse
import com.tetoota.fragment.couponcode.coupon_code.PaymentResponse
import com.tetoota.fragment.dashboard.DashSliderResponse
import com.tetoota.fragment.dashboard.FavoriteResponse
import com.tetoota.fragment.dashboard.HomeResponse
import com.tetoota.fragment.help.data.ShowWebViewResponse
import com.tetoota.fragment.home.data.ContactUploadRequest
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.RecentactivityResponse
import com.tetoota.fragment.inbox.pojo.ChatHistoryResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatResponse
import com.tetoota.fragment.inbox.pojo.ProposalApiResponse
import com.tetoota.fragment.inbox.pojo.SendChatMesg
import com.tetoota.fragment.inbox.pojo.delete_chat_data_responce.DeleteChatDataResponse
import com.tetoota.fragment.invite_friends.InviteFriendsResponse
import com.tetoota.fragment.profile.ProfileResponse
import com.tetoota.fragment.settings.PushSettingsResponse
import com.tetoota.fragment.settings.UserVisibilityResponse
import com.tetoota.fragment.settings.data.UserStatusData
import com.tetoota.google_translation.GoogleDetectLanguage.DetectLanguageResponse
import com.tetoota.google_translation.GoogleLanguagesResponce.GoogleLanguagesResponse
import com.tetoota.google_translation.GoogleTranslateResponse
import com.tetoota.login.LoginResponse
import com.tetoota.logout.LogoutApiResponse
import com.tetoota.logout.LogoutDataResponse
import com.tetoota.main.referralcode.RedeemReferralCodeResponse
import com.tetoota.marketplace.MatketPlaceResponse
import com.tetoota.marketplace.data.RedeemData
import com.tetoota.message.ReminderResponse
import com.tetoota.pointssummary.PointsSummaryResponse
import com.tetoota.pointssummary.data.UserHoldPointsResponse
import com.tetoota.potluck.PotluckFoodResponse
import com.tetoota.proposal.CheckProposalsDataResponce.CheckProposalsDataResponse
import com.tetoota.proposal.IncludeProposalsResponse
import com.tetoota.proposal.ProposalActionResponse
import com.tetoota.proposal.ProposalByIdResp
import com.tetoota.proposal.ReviewApiResponse
import com.tetoota.reviews.ViewReviewsResponse
import com.tetoota.selectlanguage.SelectLanguageResponse
import com.tetoota.service_product.UserServiceResponse
import com.tetoota.subcategories.SubCategoriesResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by charchit.kasliwal on 28-06-2017.
 */
interface ApiInterface {
    // Language List API
    @GET("AppLanguages/languageList")
    fun getLanguageList(@Header("Username") username: String,
                        @Header("Password") password: String): Call<SelectLanguageResponse>

    /* Google Translate API*/
    @GET("v2")
    fun getGoogleTranslateText(@Query("key") key: String,
                               @Query("q") q: String,
                               @Query("source") source: String,
                               @Query("target") target: String,
                               @Query("format") format: String): Call<GoogleTranslateResponse>

    /* Get Google Supported Languages */
    @GET("v2/languages")
    fun getGoogleLanguages(@Query("key") key: String,
                           @Query("target") target: String): Call<GoogleLanguagesResponse>

    /* Get Google Supported Languages */
//    @FormUrlEncoded
    @POST("v2/detect")
    fun getGoogleDetectLanguage(@Query("key") key: String,
                                @Query("q") q: String): Call<DetectLanguageResponse>

    @FormUrlEncoded
    @POST("Language/getLanguageData")
    fun getSelectedLanguageData(@Header("Username") username: String,
                                @Header("Password") password: String,
                                @Header("Language") Language: String,
                                @Field("lastSync") lastSync: String): Call<JsonObject>

    // CategoriesList API
    @GET("Categories/categoriesList")
    fun getCategoriesList(@Header("Username") username: String
                          , @Header("Password") password: String,
                          @Header("Language") Language: String): Call<CategoriesResponse>

    // SubCategoriesList API
    @FormUrlEncoded
    @POST("SubCategories/subcategoriesList")
    fun getSubCategoriesList(@Header("Username") username: String,
                             @Header("Password") password: String,
                             @Header("Language") Language: String,
                             @Header("Auth-Token") deviceType: String,
                             @Field("category_id") category_id: Int): Call<SubCategoriesResponse>

    @FormUrlEncoded
    @POST("ProductServiceSearch/searchedlist")
    fun getSearchDataList(@Header("Username") username: String,
                          @Header("Password") password: String,
                          @Header("Language") Language: String,
                          @Header("Auth-Token") deviceType: String,
                          @Field("search_string") search_string: String,
                          @Field("page_number") page_number: Int,
                          @Field("user_id") user_id: String,
                          @Field("post_type") postType: String): Call<UserServiceResponse>

    // Slides List API
    @GET("AppStartSlides/slidesList")
    fun getSlidesList(@Header("Username") username: String, @Header("Password") password: String,
                      @Header("Language") language: String, @Header("Devicetype") Devicetype: String):
            Call<WalkThroughResponse>


    // Registration API
    @FormUrlEncoded
    @POST("Authentication/register")
    fun userRegistration(@Header("Username") username: String, @Header("Password") password: String,
                         @Header("Language") language: String, @Field("device_type") deviceType: String,
                         @Field("device_token") deviceToken: String,
                         @Field("device_id") deviceId: String, @Field("categories_ids") categoriesIds: String,
                         @Field("phone_number") phoneNumber: String,
                         @Field("referral_string") referral_string: String): Call<LoginResponse>


    // mobile Registration API
    @FormUrlEncoded
    @POST("Authentication/verify_mobile")
    fun userEnterMobile(@Header("Username") username: String, @Header("Password") password: String,
                        @Header("Language") language: String, @Field("device_type") deviceType: String,
                        @Field("device_token") deviceToken: String,
                        @Field("device_id") deviceId: String,
                        @Field("phone_number") phoneNumber: String): Call<ResponseBody>


    /*  // Reminder postreminder API
      @FormUrlEncoded
      @POST("Reminder/postreminder")
      fun postreminder(@Header("Username") username: String, @Header("Password") password: String,
                          @Header("Language") language: String, @Field("device_type") deviceType: String,
                          @Field("device_token") deviceToken: String,
                          @Field("device_id") deviceId: String,
                          @Field("user_id") userId: String,
                          @Field("proposal_id") proposalId: String): Call<ResponseBody>*/


    // mobile Registration API
    @FormUrlEncoded
    @POST("Authentication/verify_otp")
    fun userRegistrationMobile(@Header("Username") username: String, @Header("Password") password: String,
                               @Header("Language") language: String, @Field("device_type") deviceType: String,
                               @Field("device_token") deviceToken: String,
                               @Field("device_id") deviceId: String, @Field("categories_ids") categoriesIds: String,
                               @Field("phone_number") phoneNumber: String,
                               @Field("otp") otp: String,
                               @Field("referral_string") referral_string: String): Call<LoginResponse>


    // Dashboard(Slider/ API)
    @GET("HomeSlider/dashboardSlider")
    fun dashBoardSlider(@Header("Username") username: String, @Header("Password") password: String,
                        @Header("Language") language: String,
                        @Header("Auth-Token") deviceType: String): Call<DashSliderResponse>

    /*
        @GET("Home/dashboardList")
        fun dashBoardHomeTabApi(@Header("Username") username: String, @Header("Password") password: String,
                                @Header("Language") language: String,
                                @Header("Auth-Token") deviceType: String,
                                @Header("user_id") user_id: String): Call<HomeResponse>
    */
    @FormUrlEncoded
    @POST("Home/dashboardList")
    fun dashBoardHomeTabApi(@Header("Username") username: String, @Header("Password") password: String,
                            @Header("Language") language: String,
                            @Header("Auth-Token") deviceType: String,
                            @Field("lat") lat: String,
                            @Field("lng") lng: String,
                            @Field("user_id") user_id: String): Call<HomeResponse>

    @FormUrlEncoded
    @POST("Home/trendingNowList")
    fun getTrendingNowList(@Header("Username") username: String, @Header("Password") password: String,
                           @Header("Language") language: String,
                           @Header("Auth-Token") deviceType: String,
                           @Field("user_id") user_id: String): Call<UserServiceResponse>

    @FormUrlEncoded
    @POST("ReportProductService/insert")
    fun reportServiceApi(@Header("Username") username: String, @Header("Password") password: String,
                         @Header("Language") language: String,
                         @Header("Auth-Token") deviceType: String,
                         @Field("user_id") user_id: String, @Field("post_id") post_id: String,
                         @Field("report_description") report_description: String): Call<FavoriteResponse>

    //enq_g@camsonline.com
    //inverstermf@idfc.com
    @FormUrlEncoded
    @POST("AllPostDetails/show")
    fun dashboardAllApi(@Header("Username") username: String, @Header("Password") password: String,
                        @Header("Language") language: String,
                        @Header("Auth-Token") deviceType: String,
                        @Field("post_type") postType: String,
                        @Field("user_id") userId: String,
                        @Field("page_number") page_number: Int,
                        @Field("category_ids") categoryIds: String,
                        @Field("sub_category_ids") subCategoryIds: String,
                        @Field("top_rated") topRated: String,
                        @Field("city") city: String,
                        @Field("lat") lat: String,
                        @Field("lng") lng: String): Call<UserServiceResponse>

    @FormUrlEncoded
    @POST("PostAttribute/getAttribute")
    fun favoriteApi(@Header("Username") username: String, @Header("Password") password: String,
                    @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                    @Field("post_id") postId: String, @Field("user_id") userId: String,
                    @Field("attribute_key") attributeKey: String,
                    @Field("attribute_value") attributeValue: String): Call<FavoriteResponse>

    @GET("TradingPreferences/preferencesList")
    fun tredingPrefrencesApi(@Header("Username") username: String, @Header("Password") password: String,
                             @Header("Language") language: String,
                             @Header("Auth-Token") deviceType: String): Call<TradingResponse>

    /**
     * Create Queue Endpoints
     */
    @Multipart
    @POST("PostAdd/insert")
    fun createAddPost(
            @Header("Username") username: String, @Header("Password") password: String,
            @Header("Language") language: String,
            @Header("Auth-Token") deviceType: String,
            @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>,
            @Part multiImagesParts: Array<MultipartBody.Part>
    ): Call<AddServiceDataResponse>

    /**
     * Create Queue Endpoints
     */
    @Multipart
    @POST("EditPost/update")
    fun postEdit(
            @Header("Username") username: String, @Header("Password") password: String,
            @Header("Language") language: String,
            @Header("Auth-Token") deviceType: String,
            @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>,
            @Part multiImagesParts: Array<MultipartBody.Part>
    ): Call<AddServiceDataResponse>

    @FormUrlEncoded
    @POST("EditPost/delete")
    fun deletePostApi(@Header("Username") username: String, @Header("Password") password: String,
                      @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                      @Field("user_id") user_id: String, @Field("post_id") post_id: String): Call<FavoriteResponse>

    @FormUrlEncoded
    @POST("AllPostDetails/sendservicedata")
    fun sendservicedata(@Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                        @Field("user_id") user_id: String, @Field("post_id") post_id: String): Call<ViewProductResponse>

    @FormUrlEncoded
    @POST("ProfileDetails/show")
    fun profileDetailApi(@Header("Username") username: String, @Header("Password") password: String,
                         @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                         @Field("user_id") user_id: String): Call<ProfileResponse>


    @FormUrlEncoded
    @POST("ProfileDetails/showUserDetail")
    fun showUserDetail(@Header("Username") username: String, @Header("Password") password: String,
                       @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                       @Field("user_id") user_id: String): Call<ProfileResponse>

    @Multipart
    @POST("ProfileDetails/edit")
    fun profileUpdateApi(@Header("Username") username: String, @Header("Password") password: String,
                         @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                         @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>,
                         @Part file: MultipartBody.Part): Call<ProfileResponse>

    @Multipart
    @POST("ProfileDetails/edit")
    fun profileUpdateApi(@Header("Username") username: String, @Header("Password") password: String,
                         @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                         @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>): Call<ProfileResponse>

    @FormUrlEncoded
    @POST("PostAttribute/showAttribute")
    fun getFavoriteList(@Header("Username") username: String, @Header("Password") password: String,
                        @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                        @Field("user_id") user_id: String, @Field("page_number") page_number: Int): Call<UserServiceResponse>

    @FormUrlEncoded
    @POST("Referral/sendReferralCode")
    fun getReferralCode(@Header("Username") username: String, @Header("Password") password: String,
                        @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                        @Field("user_id") user_id: String): Call<InviteFriendsResponse>

    //user_id, post_type
    @FormUrlEncoded
    @POST("Referral/redeemReferralCode")
    fun applyReferralCode(
            @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
            @Field("referral_to") referral_to: String,
            @Field("referral_code") referral_code: String): Call<RedeemReferralCodeResponse>

    @FormUrlEncoded
    @POST("Referral/sharepoints")
    fun sharepoints(
            @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
            @Field("user_id") user_id: String,
            @Field("post_type") post_type: String): Call<SharePointsResponse>

    @FormUrlEncoded
    @POST("CouponCode/redeem")
    fun getCouponsCode(
            @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
            @Field("user_id") user_id: String,
            @Field("coupon_code") coupon_code: String): Call<CouponCodeDataResponse>

    /*@FormUrlEncoded
    @POST("NearBy/show")
    fun getNearByList(@Header("Username") username: String, @Header("Password") password: String,
                      @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                      @Field("lat") letitude: Double, @Field("lng") longitude: Double): Call<NearByDataResponse>*/
    @FormUrlEncoded
    @POST("NearBy/show")
    fun getNearByList(@Header("Username") username: String, @Header("Password") password: String,
                      @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                      @Field("user_id") user_id: String,
                      @Field("lat") letitude: Double, @Field("lng") longitude: Double,
                      @Field("page_number") page_number: Int): Call<UserServiceResponse>

    @FormUrlEncoded
    @POST("MyPostDetails/show")
    fun getMultipleListType(@Header("Username") username: String, @Header("Password") password: String,
                            @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                            @Field("user_id") user_id: String, @Field("page_number") page_number: Int,
                            @Field("post_type") post_type: String): Call<UserServiceResponse>


    @FormUrlEncoded
    @POST("MyPostDetails/show")
    fun getMyFoodList(@Header("Username") username: String, @Header("Password") password: String,
                            @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                            @Field("user_id") user_id: String, @Field("page_number") page_number: Int,
                            @Field("post_type") post_type: String): Call<PotluckFoodResponse>


    @FormUrlEncoded
    @POST("ReviewShow/postReview")
    fun getReviewsList(@Header("Username") username: String, @Header("Password") password: String,
                       @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                       @Field("user_id") user_id: String): Call<ViewReviewsResponse>

    @FormUrlEncoded
    @POST("ReviewShow/reviewPostId")
    fun reviewByPostId(@Header("Username") username: String, @Header("Password") password: String,
                       @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                       @Field("post_id") post_id: String): Call<ViewReviewsResponse>


    @FormUrlEncoded
    @POST("ProposalProcess/view")
    fun getProposalMessageList(@Header("Username") username: String, @Header("Password") password: String,
                               @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                               @Field("user_id") user_id: String): Call<ProposalApiResponse>


    @FormUrlEncoded
    @POST("ProposalProcess/incompleteProposal")
    fun getIncompleteProposal(@Header("Username") username: String, @Header("Password") password: String,
                              @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                              @Field("user_id") user_id: String,
                              @Field("user_mac_address") user_mac_address: String): Call<ProposalApiResponse>

    /**
     * Jitendra
     */
    @FormUrlEncoded
    @POST("ProposalAdd/insert")
    fun getProposalsData(@Header("Username") username: String, @Header("Password") password: String,
                         @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                         @Field("post_id") post_id: String,
                         @Field("proposal_to") proposal_to: String,
                         @Field("proposal_from") proposal_from: String,
                         @Field("tetoota_points") tetoota_points: String,
                         @Field("exchange_post_id") exchange_post_id: String,
                         @Field("exchange_post_type") exchange_post_type: String,
                         @Field("proposal_time") proposal_time: String,
                         @Field("chat_type") type: String): Call<IncludeProposalsResponse>

    @FormUrlEncoded
    @POST("ProposalProcess/viewuserproposaldetails")
    fun checkProposalsData(@Header("Username") username: String, @Header("Password") password: String,
                           @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                           @Field("user_id") user_id: String): Call<CheckProposalsDataResponse>

    @FormUrlEncoded
    @POST("ProposalProcess/actionOnProposal")
    fun actionOnProposal(@Header("Username") username: String, @Header("Password") password: String,
                         @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                         @Field("proposal_id") proposal_id: String,
                         @Field("proposal_status") proposal_status: String,
                         @Field("user_id") userId: String,
                         @Field("chat_type") chatType: String): Call<ProposalActionResponse>

    /*  receiver_user_id
      msg_recieved_user_id
      user_id
      proposal_id*/
    @FormUrlEncoded
    @POST("Reminder/postreminder")
    fun postreminder(@Header("Username") username: String, @Header("Password") password: String,
                     @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                     @Field("user_id") user_id: String,
                     @Field("proposal_id") proposal_id: String): Call<ResponseBody>


    @FormUrlEncoded
    @POST("Reminder/postreminder")
    fun postremindernew(@Header("Username") username: String, @Header("Password") password: String,
                        @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                        @Field("device_token") deviceToken: String,
                        @Field("device_id") deviceId: String,
                        @Field("user_id") user_id: String,
                        @Field("proposal_id") proposal_id: String): Call<ResponseBody>


    @FormUrlEncoded
    @POST("Reminder/viewreminder")
    fun viewreminder(@Header("Username") username: String, @Header("Password") password: String,
                     @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                     @Field("user_id") user_id: String,
                     @Field("proposal_id") user_mac_address: String): Call<ResponseBody>


    @FormUrlEncoded
    @POST("ProposalProcess/proposalTtradingAction")
    fun proposalCompleteDeal(@Header("Username") username: String, @Header("Password") password: String,
                             @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                             @Field("proposal_id") proposal_id: String, @Field("trading_status") trading_status: String,
                             @Field("referral_user_status") referral_user_status: String, @Field("user_id") user_id
                             : String): Call<ProposalActionResponse>

    @FormUrlEncoded
    @POST("ProposalProcess/proposalReceiverTradingAction")
    fun proposalExchangeCompleteDeal(@Header("Username") username: String, @Header("Password") password: String,
                                     @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                                     @Field("proposal_id") proposal_id: String,
                                     @Field("receiver_trading_status") receiver_trading_status: String,
                                     @Field("receiver_id") receiver_id: String): Call<ProposalActionResponse>

    @FormUrlEncoded
    @POST("ProposalProcess/singleProposal")
    fun proposalById(@Header("Username") username: String, @Header("Password") password: String,
                     @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                     @Field("proposal_id") proposal_id: String): Call<ProposalByIdResp>

    @FormUrlEncoded
    @POST("BadgesAdd/insert")
    fun addReviews(@Header("Username") username: String, @Header("Password") password: String,
                   @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
                   @Field("user_id") user_id: String,
                   @Field("post_id") post_id: String,
                   @Field("response_time") response_time: String,
                   @Field("quality_services") quality_services: String,
                   @Field("friendliness") friendliness: String,
                   @Field("review_message") review_message: String): Call<ReviewApiResponse>


    @FormUrlEncoded
    @POST("Logout/user")
    fun userLogout(@Header("Username") username: String, @Header("Password") password: String,
                   @Header("Language") language: String,
                   @Header("Auth-Token") deviceType: String,
                   @Field("user_id") user_id: String): Call<LogoutDataResponse>

    /* DELETE CHAT MESSAGE API */
    @FormUrlEncoded
    @POST("ChatMessages/delete")
    fun deleteChatMesg(@Header("Username") username: String, @Header("Password") password: String,
                       @Header("Language") language: String,
                       @Header("Auth-Token") deviceType: String,
                       @Field("conver_id") conver_id: Int,
                       @Field("user_id") user_id: String): Call<DeleteChatDataResponse>

    @FormUrlEncoded
    @POST("ChatMessages/send")
    fun sendChatMesg(@Header("Username") username: String, @Header("Password") password: String,
                     @Header("Language") language: String,
                     @Header("Auth-Token") deviceType: String,
                     @Field("msg_send_user_id") msg_send_user_id: String,
                     @Field("msg_recieved_user_id") msg_recieved_user_id: String,
                     @Field("post_id") post_id: String,
                     @Field("chat_message") chat_message: String,
                     @Field("proposal_id") proposal_id: String,
                     @Field("chat_type") type: String): Call<SendChatMesg>

    @FormUrlEncoded
    @POST("LatestActivity/shows")
    fun getChatHistory(@Header("Username") username: String, @Header("Password") password: String,
                       @Header("Language") language: String,
                       @Header("Auth-Token") deviceType: String,
                       @Field("user_id") user_id: String,
                       @Field("chat_type") type: String): Call<ChatHistoryResponse>

    /* CHAT MESSAGE API */
    @FormUrlEncoded
    @POST("ChatMessages/show")
    fun showChatMesg(@Header("Username") username: String, @Header("Password") password: String,
                     @Header("Language") language: String,
                     @Header("Auth-Token") deviceType: String,
                     @Field("msg_send_user_id") msg_send_user_id: String,
                     @Field("msg_recieved_user_id") msg_recieved_user_id: String,
                     @Field("post_id") post_id: String): Call<LogoutApiResponse>

//    @FormUrlEncoded
//    @POST("ChatMessages/show")
//    fun getOneToOneChatRecord(@Header("Username") username: String, @Header("Password") password: String,
//                              @Header("Language") language: String,
//                              @Header("Auth-Token") deviceType: String,
//                              @Field("msg_send_user_id") msg_send_user_id: String,
//                              @Field("msg_recieved_user_id") msg_recieved_user_id: String,
//                              @Field("user_id") user_id: String,
//                              @Field("post_id") post_id: String): Call<OneToOneChatResponse>

    @FormUrlEncoded
    @POST("Conversation/show")
    fun getOneToOneChatRecord(@Header("Username") username: String, @Header("Password") password: String,
                              @Header("Language") language: String,
                              @Header("Auth-Token") deviceType: String,
                              @Field("msg_send_user_id") msg_send_user_id: String,
                              @Field("msg_recieved_user_id") msg_recieved_user_id: String,
                              @Field("user_id") user_id: String,
                              @Field("chat_type") type: String,
                              @Field("by") by: String): Call<OneToOneChatResponse>

    @FormUrlEncoded
    @POST("TetootaPointsHistory/details")
    fun pointsHistory(@Header("Username") username: String, @Header("Password") password: String,
                      @Header("Language") language: String,
                      @Header("Auth-Token") deviceType: String,
                      @Field("history_type") history_type: String,
                      @Field("user_id") user_id: String): Call<PointsSummaryResponse>

    @FormUrlEncoded
    @POST("NotificationSettings/set")
    fun setNotificationSetting(@Header("Username") username: String, @Header("Password") password: String,
                               @Header("Language") language: String,
                               @Header("Auth-Token") deviceType: String,
                               @Field("user_id") user_id: String,
                               @Field("all_notificaiton") all_notificaiton: String,
                               @Field("proposal_notificaiton") proposal_notificaiton: String,
                               @Field("chat_notification") chat_notification: String): Call<PushSettingsResponse>

    @FormUrlEncoded
    @POST("VisibleSettings/set")
    fun setUserVisibiltySettings(@Header("Username") username: String, @Header("Password") password: String,
                                 @Header("Language") language: String,
                                 @Header("Auth-Token") deviceType: String,
                                 @Field("user_id") user_id: String,
                                 @Field("is_visible") is_visible: String): Call<UserVisibilityResponse>


    @FormUrlEncoded
    @POST("usercontacts/postUserContacts")
    fun sendContactList(@Header("Username") username: String, @Header("Password") password: String,
                        @Header("Language") language: String,
                        @Header("Auth-Token") deviceType: String,
                        @Field("Contact") Contact: JsonObject): Call<UserVisibilityResponse>

    @FormUrlEncoded
    @POST("user/getUserholdpoints")
    fun getUserHoldPoints(@Header("Username") username: String, @Header("Password") password: String,
                          @Header("Language") language: String,
                          @Header("Auth-Token") deviceType: String,
                          @Field("user_id") user_id: String): Call<UserHoldPointsResponse>

    @FormUrlEncoded
    @POST("AppPageDetails/pagedetails")
    fun getPageDetails(@Header("Language") language: String,
                       @Field("page_title") page_title: String): Call<ShowWebViewResponse>

    @FormUrlEncoded
    @POST("MatchIp/matchip")
    fun doDeepLinking(@Header("Language") language: String,
                      @Field("user_id") user_id: String,
                      @Field("user_ip") user_ip: String): Call<DeepLinkingResponse>

    @POST("UserContacts/postUserContacts")
    fun postUserContacts(@Header("Username") username: String, @Header("Password") password: String,
                         @Header("Language") language: String,
                         @Header("Auth-Token") deviceType: String,
                         @Body contactUploadRequest: ContactUploadRequest): Call<ContactUploadResponse>


    @POST("RecentActivity/recentactivity")
    fun getRecentactivity(@Header("Username") username: String, @Header("Password") password: String,
                          @Header("Language") language: String,
                          @Header("Auth-Token") deviceType: String): Call<RecentactivityResponse>

    @POST("RecentActivity/invalidText")
    fun getInvalidText(@Header("Username") username: String, @Header("Password") password: String,
                       @Header("Language") language: String,
                       @Header("Auth-Token") deviceType: String): Call<RecentactivityResponse>

    @FormUrlEncoded
    @POST("authentication/checkUser")
    fun checkUser(@Header("Username") username: String, @Header("Password") password: String,
                  @Header("Language") language: String, @Field("device_type") deviceType: String,
                  @Field("device_token") deviceToken: String,
                  @Field("device_id") deviceId: String,
                  @Field("phone_number") phoneNumber: String): Call<LoginResponse>

    @FormUrlEncoded
    @POST("ProfileDetails/optionalInformation")
    fun updateOptionalInformation(@Header("Username") username: String,
                                  @Header("Language") language: String,
                                  @Header("Auth-Token") deviceType: String, @Field("user_id") user_id: String
                                  , @Field("email") email: String, @Field("aadhar_number") aadhar_number: String,
                                  @Field("categories_ids") categories_ids: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("Company/getCompanyData")
    fun getCompany(@Header("Language") language: String,
                   @Header("Auth-Token") deviceType: String,
                   @Field("lastSync") lastSync: String): Call<CompanyDataResponse>

    @FormUrlEncoded
    @POST("User/CheckUser")
    fun submitCorporate(@Header("Language") language: String,
                        @Header("Auth-Token") deviceType: String,
                        @Field("user_id") user_id: String,
                        @Field("company_id") company_id: String,
                        @Field("company_code") company_code: String): Call<SubmitCorporateLoginResponse>

    @FormUrlEncoded
    @POST("User/updateuserstatus")
    fun setStatusPublicPrivate(@Header("Language") language: String,
                               @Header("Auth-Token") deviceType: String,
                               @Field("user_id") user_id: String,
                               @Field("status") status: String): Call<UserStatusData>

//----------------------Akash-Gupta-Email/Aadhar verify API-----------------------------------------//

    // Email/Aadhar verify API
    @FormUrlEncoded
    @POST("ProfileDetails/sendEmail")
    fun verifyEmailAadhar(@Header("Language") Language: String,
                          @Header("device_token") deviceToken: String,
                          @Header("Auth-Token") deviceType: String,
                          @Field("email") email: String,
                          @Field("user_id") user_id: String,
                          @Field("type") type: String,
                          @Field("aadhar_verify") aadhar_verify: String): Call<ResponseBody>

    //---------------------Akash-Gupta-Market-Place verify API-----------------------------------------//
    @FormUrlEncoded
    @POST("Offers/show")
    fun UserReadOnlyDetailApi(@Header("Username") username: String,
                              @Header("Password") password: String,
                              @Header("Language") language: String,
                              @Header("Auth-Token") deviceType: String,
                              @Field("page_number") page_number: Int?,
                              @Field("user_id") user_id: String): Call<MatketPlaceResponse>

    //----------------------Akash-Gupta-Market-Place-Submit-Form verify API----------------------------//
    @FormUrlEncoded
    @POST("Offers/redeem")
    fun OffersRedeemApi(@Header("Language") Language: String,
                        @Header("device_token") deviceToken: String,
                        @Header("Auth-Token") deviceType: String,
                        @Field("user_id") user_id: String,
                        @Field("post_id") post_id: String): Call<RedeemData>

    //------------------Akash-Gupa-Food-Potluck--------------------------------------------------------//
    @FormUrlEncoded
    @POST("AllPostDetails/show")
    fun potlwekFoodAllApi(@Header("Username") username: String,
                          @Header("Password") password: String,
                          @Header("Language") language: String,
                          @Header("Auth-Token") deviceType: String,
                          @Field("page_number") page_number: Int?,
                          @Field("post_type") Food: String,
                          @Field("user_id") user_id: String,
                          @Field("lat") lat: Double,
                          @Field("lng") longi: Double): Call<PotluckFoodResponse>

    //------------------Akash-Gupa-Add-Food-Potluck-----------------------------------------------------//
    @Multipart
    @POST("PostAdd/insert")
    fun addFoodPotluckApi(
            @Header("Username") username: String,
            @Header("Password") password: String,
            @Header("Language") language: String,
            @Header("Auth-Token") deviceType: String,
            @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>,
            @Part multiImagesParts: Array<MultipartBody.Part>): Call<PotluckFoodResponse>

    //------------------Akash-Gupa-Add-Food-Potluck-----------------------------------------------------//
    @Multipart
    @POST("PostAdd/insert")
    fun addFoodPotluckApi(
            @Header("Username") username: String,
            @Header("Password") password: String,
            @Header("Language") language: String,
            @Header("Auth-Token") deviceType: String,
            @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>): Call<PotluckFoodResponse>

    //---------------------Ravinder-QRCode-Scan--------------------------------------------------------//
    @FormUrlEncoded
    @POST("Offers/payment")
    fun payment(@Header("Language") language: String,
                @Header("Auth-Token") deviceType: String,
                @Field("user_id") user_id: String,
                @Field("receiver_id") status: String,
                @Field("points") points: String): Call<PaymentResponse>
//----------------------Akash-Gupta-Market-Place verify API-----------------------------------------//
    /* @FormUrlEncoded
     @POST("AllPostDetails/show")
     fun MarketPlaceAllApi(@Header("Username") username: String, @Header("Password") password: String,
                           @Header("Language") language: String,
                           @Header("Auth-Token") deviceType: String,
                           @Field("post_type") postType: String,
                           @Field("user_id") userId: String,
                           @Field("page_number") page_number: Int,
                           @Field("category_ids") categoryIds: String,
                           @Field("sub_category_ids") subCategoryIds: String,
                           @Field("top_rated") topRated: String,
                           @Field("city") city: String,
                           @Field("lat") lat: String,
                           @Field("lng") lng: String): Call<UserServiceResponse>*/
//----------------------Nikita-Market-Conversation/stage-API---------------------------------------//

    @FormUrlEncoded
    @POST("Conversation/stage")
    fun GetProposalAllStage(
            @Header("Language") language: String,
            @Header("Auth-Token") deviceType: String,
            @Field("proposal_from") proposalFrom: String,
            @Field("proposal_to") proposalTo: String,
            @Field("post_id") postId: String,
            @Field("proposal_id") proposalId: String,
            @Field("chat_type") type: String,
            @Field("user_id") userId: String,
            @Field("by") by: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("Conversation/foodstage")
    fun GetProposalFoodAllStage(
            @Header("Language") language: String,
            @Header("Auth-Token") deviceType: String,
            @Field("proposal_from") proposalFrom: String,
            @Field("proposal_to") proposalTo: String,
            @Field("post_id") postId: String,
            @Field("proposal_id") proposalId: String,
            @Field("chat_type") type: String,
            @Field("user_id") userId: String): Call<ResponseBody>

    //----------------------Nikita-Market-Conversation/stage-API---------------------------------------//
    @FormUrlEncoded
    @POST("Conversation/yes")
    fun ProposalYes(
            @Header("Username") username: String, @Header("Password") password: String,
            @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
            @Field("post_id") post_id: String,
            @Field("proposal_to") proposal_to: String,
            @Field("proposal_from") proposal_from: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("ProposalProcess/FoodReceiverComplete")
    fun foodRequestComplete(
            @Header("Username") username: String, @Header("Password") password: String,
            @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
            @Field("proposal_id") proposal_id: String,
            @Field("food_status") food_Status: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("ProposalProcess/foodunlist")
    fun foodListUnlist(
            @Header("Username") username: String, @Header("Password") password: String,
            @Header("Language") language: String, @Header("Auth-Token") deviceType: String,
            @Field("post_id") proposal_id: String
    ): Call<ResponseBody>
//-------------------------------------------------------End---------------------------------------//
}