package com.tetoota.checkUser;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.converter.gson.GsonConverterFactory;


public class Constant {

    public static final String BASE_URL = "http://tetoota.com/api/v1/";


    public static final String SHAREDPREFUSER = "USERINFO";
    public static OkHttpClient getinfo() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
       // logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(3, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(3, TimeUnit.MINUTES).addInterceptor(logging)
                .build();
        return okHttpClient;
    }




}
