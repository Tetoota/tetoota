package com.tetoota.userdetail

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.customviews.ReportConversationDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.fragment.dashboard.HomeContract
import com.tetoota.fragment.dashboard.HomePresenter
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.nearby.NearByRecyclerAdapter
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.service_product.FullImageActivity
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_user_detail.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

class UserDetailActivity : BaseActivity(), HomeContract.View, ProfileDetailContract.View, ServiceContract.View, NearByRecyclerAdapter.IAdapterClick,
        NearByRecyclerAdapter.PaginationAdapterCallback, ReportConversationDialog.IDialogListener,
        ProfileCompletionDialog.IDialogListener {
    override fun onProfileSendEmailApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInvalidTextAPiFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
        Utils.hideSoftKeyboard(this)
        if (message == "report") {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mHomePresenter.report(this, mServiceId, mMesgDesc)
        }
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            this.toast(message)
        } else {
            this.toast(message)
        }
    }

    override fun retryPageLoad() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this)) {
                val intent = ProfileDetailActivity.newMainIntent(this)
                ActivityStack.getInstance(this)
                startActivity(intent)
            } else {
                this.toast(Utils.getText(this, StringConstant.str_check_internet))
            }
        } else {

        }

    }

    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@UserDetailActivity)
    }
    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@UserDetailActivity)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@UserDetailActivity)
    }
    var linearLayoutManager: LinearLayoutManager? = null
    private lateinit var mNearByRecyclerAdapter: NearByRecyclerAdapter
    private val EDITSERVICEPRODUCT_REQUEST: Int = 13
    var menu: Menu? = null
    private var context: Context? = null
    private var praposalFrom: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)

        context = applicationContext
        initToolbar()

        if (intent != null) {
            praposalFrom = intent.getStringExtra("cvMain")

            try {
                getUserDetail()
            } catch (e: Exception) {
            }
        }

    }

    fun getUserDetail() {
        if (Utils.haveNetworkConnection(this)) {
            progress_view.visibility = View.VISIBLE
            mProfileDetailPresenter.getUserDetailShow(this, praposalFrom)
        } else {
            this.toast(Utils.getText(context, StringConstant.str_check_internet))
            progress_view.visibility = View.GONE
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        toolbar_title.text = "User Detail"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun setUiData(message: List<ProfileDataResponse?>?) {

        if (message!![0]?.first_name != "") {
            tv_name.text = "${message[0]?.first_name} ${message[0]?.last_name}"
        } else {
            tv_name.visibility = View.GONE
        }
        if (message[0]?.profile_image!!.isEmpty()) {
            iv_profile.setImageResource(R.drawable.user_placeholder);
        } else {

            Picasso.get().load(message[0]?.profile_image).into(iv_profile)
        }
        if (message[0]?.review_avg != "") {
            tv_review_avg.text = message[0]?.review_avg?.toFloat().toString()
            user_reviews.rating = message[0]?.review_avg!!.toFloat()

        } else {
            tv_review_avg.visibility = View.GONE
        }

        if (message[0]?.profile_image!!.isEmpty())
        {
            // context.toast("Profile image is not available")

        }else {
            iv_profile.onClick {
                val intent = FullImageActivity.newMainIntent(this)
                intent!!.putExtra("profile_image", message[0]?.profile_image!!)
                ActivityStack.getInstance(this)
                this.startActivity(intent)

            }
        }


        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        view_list.setHasFixedSize(true)
        view_list.itemAnimator = DefaultItemAnimator()
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        view_list.layoutManager = linearLayoutManager
        mNearByRecyclerAdapter = NearByRecyclerAdapter(this, iAdapterClickListener = this,
                iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        view_list.adapter = mNearByRecyclerAdapter
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, UserDetailActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this) * .2f).toInt()
        return getTopMarginH
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@UserDetailActivity)
        finish()
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        progress_view.visibility = View.GONE
        if (message != null) {
            // Log.e("message ", "***** " + message)
            setUiData(message)

            if (Utils.haveNetworkConnection(this@UserDetailActivity)) {
                var pagination: Int = 1
                mServicePresenter.getUserServiceList(this@UserDetailActivity, praposalFrom, "ServicesProducts", pagination)

            } else {
                toast(Utils.getText(this, StringConstant.str_check_internet))
            }


        }
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
        Log.i("message ", "***** " + message)
    }

    override fun onWishListApiSuccessResult(mCategoriesList: List<Any?>?, message: String) {
        if (mCategoriesList != null) {
            mNearByRecyclerAdapter.addAll(mCategoriesList as List<ServicesDataResponse?>)
            mNearByRecyclerAdapter.notifyDataSetChanged()
           // Log.e("message ", "***** " + mCategoriesList)

        }
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        Log.i("message ", "***** " + message)

    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (cellRow.is_favourite == 0) {
            cellRow.is_favourite = 1
            this.toast("Marked Favorite")
        } else {
            cellRow.is_favourite = 0
            this.toast("Marked Unfavorite")
        }
        mNearByRecyclerAdapter.notifyItemChanged(p1)
    }

    override fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        val mProductData = cellRow as ServicesDataResponse
        if (mString == "favorite") {
            showPopupMenu(mView, mProductData, mAttributeValue, p1)
        } else if (mString == "cellClick") {
            this.toast("Under Development")
        } else if (mString == "shareClick") {
        } else if (mString.equals("Cell Item click")) {
            if (Utils.haveNetworkConnection(this)) {
                val intent = Intent(this, ProductDetailActivity::class.java)
                intent.putExtra("mProductData", mProductData)
                intent.putExtra("productType", "services")
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    val transitionActivityOptions: ActivityOptions =
                            ActivityOptions.makeSceneTransitionAnimation(this,
                                    mView, "iv_image")
                    startActivity(intent, transitionActivityOptions.toBundle())
                    // startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST, transitionActivityOptions.toBundle())
                    this.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                } else {
                    startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST)
                    //startActivity(intent)
                    this.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                }
            } else {
                this.toast(Utils.getText(this, StringConstant.str_check_internet))
            }
        }
    }

    private fun showPopupMenu(view: View, mProductData: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val popup = PopupMenu(this, view)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        if (mAttributeValue == "0") {
            favItem?.title = Utils.getText(this, StringConstant.home_unfavourite)
        } else {
            favItem?.title = Utils.getText(this, StringConstant.favorite)
        }
        menu?.findItem(R.id.menu_share)!!.title = Utils.getText(this, StringConstant.share_text)
        menu?.findItem(R.id.menu_report_this)!!.title = Utils.getText(this, StringConstant.report_this)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
                    var url: String = "";
                    if (mProductData.image != null && mProductData.image_thumb != null) {
                        url = Utils.getUrl(this, mProductData.image, mProductData.image_thumb, false)
                    } else {
                        url = Utils.getUrl(this, mProductData.image!!)
                    }
                    // shareProduct(mProductData.title!!, url)
                    shareService(mProductData.title!!, url)
                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this)) {
                        mServicePresenter.markFavorite(this, mProductData, mAttributeValue, p1)
                    } else {
                        this.toast(Utils.getText(this, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    ReportConversationDialog(this, Constant.DIALOG_LOGIN_FAILURE_ALERT, mProductData.id.toString(),
                            this@UserDetailActivity, getString(R.string.str_report_issue)).show()
                }
            }
            true
        }
        popup.show()
    }

    fun shareService(mTitle: String, mImageUrl: String) {
        try {
            val text = mTitle
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(getResources(), R.drawable.static_image),
                        null, null));
            } catch (e: NullPointerException) {
            }
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            shareIntent.type = "image/*"
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            //  startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
            startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: android.content.ActivityNotFoundException) {
        }
    }

}
