package com.tetoota.proposal;

import com.tetoota.network.errorModel.Meta;

import java.util.List;

public class ReviewApiResponse{
	private List<Object> data;
	private Meta meta;

	public void setData(List<Object> data){
		this.data = data;
	}

	public List<Object> getData(){
		return data;
	}

	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}

	@Override
 	public String toString(){
		return 
			"ReviewApiResponse{" + 
			"pushNotificationDataResponse = '" + data + '\'' +
			",meta = '" + meta + '\'' + 
			"}";
		}
}