package com.tetoota.proposal

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.customviews.PaginationScrollListener
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.exchange_service_product_activity.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
/**
 * Created by jitendra.nadndiya on 09-08-2017.
 */
class ExchnageServiceProductListActivity : BaseActivity(), ServiceContract.View, ExchangeServiceProductAdapter.IAdapterClick,
        ExchangeServiceProductAdapter.PaginationAdapterCallback {
    override fun retryPageLoad() {}
    private val ADDPOST_REQUEST: Int = 14
    private var isLoad = false
    private var isListLastPage = false
    private var isFirstLoading = false
    private var TOTAL_PAGES = 1
    private val RECORD_PER_PAGE = 10
    private val PAGE_START = 1
    private var currentPage = PAGE_START
    private var exchangeType: String? = null
    var linearLayoutManager: LinearLayoutManager? = null
    private lateinit var mExchangeServiceProductAdapter: ExchangeServiceProductAdapter

    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@ExchnageServiceProductListActivity)
    }

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exchange_service_product_activity)
        exchangeType = intent.getStringExtra("itemName")
        initToolbar()
        setupRecyclerView()
        view_list.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isListLastPage
            }

            override fun loadMoreItems() {
                isLoad = true
                currentPage += 1
                loadItemsLayout_recyclerView.visibility = View.VISIBLE
                fetchWishListData(currentPage, true)
            }

            override fun getTotalPageCount(): Int {
                return TOTAL_PAGES
            }

            override fun isLoading(): Boolean {
                return isLoad
            }
        })
        fetchWishListData(currentPage, false)
    }

    private fun initToolbar(): Unit {
        iv_close.visibility = View.VISIBLE
        if (exchangeType.equals("Services")) {
           // toolbar_title.text = "Exchange Service/Product"
            toolbar_title.text = Utils.getText(this, StringConstant.str_service_product)
        } else {
            toolbar_title.text = "Exchange Product"
        }
        iv_close.onClick {
            onBackPressed()
        }
    }

    private fun setupRecyclerView() {
        view_list.setHasFixedSize(true)
        view_list.itemAnimator = DefaultItemAnimator()
        linearLayoutManager = LinearLayoutManager(this@ExchnageServiceProductListActivity, LinearLayoutManager.VERTICAL, false)
        view_list.layoutManager = linearLayoutManager
        mExchangeServiceProductAdapter = ExchangeServiceProductAdapter(iAdapterClickListener = this)
        view_list.adapter = mExchangeServiceProductAdapter
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ExchnageServiceProductListActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this@ExchnageServiceProductListActivity) * .3f).toInt()
        return getTopMarginH
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
        ActivityStack.removeActivity(this@ExchnageServiceProductListActivity)
//        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun fetchWishListData(pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this@ExchnageServiceProductListActivity)) {
            if (!isLoadMore) {
                showProgressDialog(Utils.getText(this,StringConstant.please_wait))
            }
            if (exchangeType.equals("Services")) {
                mServicePresenter.getServicesData(this@ExchnageServiceProductListActivity, "ServicesProducts", pagination,Utils.loadPrefrence(Constant.USER_ID, "", this@ExchnageServiceProductListActivity).toString())
            } else {
                mServicePresenter.getServicesData(this@ExchnageServiceProductListActivity, "Products", pagination,Utils.loadPrefrence(Constant.USER_ID, "", this@ExchnageServiceProductListActivity).toString())
            }
        } else {
            toast(Utils.getText(this,StringConstant.str_check_internet))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mServicePresenter.cancelRequest(this@ExchnageServiceProductListActivity, "services")
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (cellRow.is_favourite == 0) {
            cellRow.is_favourite = 1
            toast("Mark Favorite")
        } else {
            cellRow.is_favourite = 0
            toast("Mark Unfavorite")
        }
        mExchangeServiceProductAdapter.notifyItemChanged(p1)
    }

    override fun onWishListApiSuccessResult(mWishList: List<Any?>?, message: String) {

        if (this@ExchnageServiceProductListActivity != null) {
            hideProgressDialog()
            hideErrorView()
            loadItemsLayout_recyclerView.visibility = View.GONE
            mExchangeServiceProductAdapter.setElements(mWishList as MutableList<ServicesDataResponse?>, false)
            //mExchangeServiceProductAdapter.addAll(mWishList as MutableList<ServicesDataResponse>)
            isLoad = false
        }
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        Log.e("isServerError ","" + isServerError)
        Log.e("message ","" + message)
        hideProgressDialog()
        loadItemsLayout_recyclerView.visibility = View.GONE
        isLoad = false
        if (this != null && this@ExchnageServiceProductListActivity != null) {
            if (!isServerError) {
                isListLastPage = true
                if(mExchangeServiceProductAdapter.itemCount == 0){
                    // Todo : if list is blank open below line.
//                    showSnackBar("No Record Found")
                 /*   val intent = AddPostRequestActivity.newMainIntent(this!!)
                    intent!!.putExtra("Tab", "serviceTab")
                    ActivityStack.getInstance(this!!)
                    startActivity(intent)
                    finish()*/
                    //startActivityForResult(intent, ADDPOST_REQUEST)
                }
            }
            if (TOTAL_PAGES != 1) {
                showErrorView(message)
            }
        }
    }

    override fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        val mProductData = cellRow as ServicesDataResponse
        val resultIntent = Intent()
        resultIntent.putExtra("item", mProductData.title)
        resultIntent.putExtra("post_type", mProductData.post_type)
        resultIntent.putExtra("exchangeId", ""+mProductData.id )
        setResult(Activity.RESULT_OK, resultIntent)
        ActivityStack.removeActivity(this@ExchnageServiceProductListActivity)
        finish()
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            nv_view.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
     fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            nv_view.visibility = View.GONE
            err_title.visibility = View.GONE
        }
    }
}