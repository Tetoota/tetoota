package com.tetoota.proposal

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.tetoota.proposal.CheckProposalsDataResponce.CheckProposalsDataResponse
import com.tetoota.TetootaApplication
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import okhttp3.ResponseBody
import org.json.JSONObject
import org.json.JSONTokener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jitendra.nandiya on 08-08-2017.
 */
class ProposalsInteractor {
    var call: Call<IncludeProposalsResponse>? = null
    var callProposalData: Call<CheckProposalsDataResponse>? = null
    var proposalActionCall: Call<ProposalActionResponse>? = null
    var mProposalsApiListener: ProposalsContract.ProposalsApiListener

    constructor(mProposalsApiListener: ProposalsContract.ProposalsApiListener) {
        this.mProposalsApiListener = mProposalsApiListener
    }

    fun proposalsApiCalling(mActivity: Activity, postId: String, proposalTo: String, proposalFrom: String, tetootaPoints: String,
                            exchangePostId: String, exchangePostType: String, proposalTime: String, type: String): Unit {
        call = TetootaApplication.getHeader()
                .getProposalsData(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        postId, proposalTo, proposalFrom, tetootaPoints, exchangePostId, exchangePostType, proposalTime, type)
        call!!.enqueue(object : Callback<IncludeProposalsResponse> {
            override fun onResponse(call: Call<IncludeProposalsResponse>?, response: Response<IncludeProposalsResponse>?) {
                println("Success")
                var mIncludeProposalData: IncludeProposalsResponse? = response?.body()
                if (response?.code() == 200) {
                    mProposalsApiListener.onProposalsAPiSuccess(mIncludeProposalData?.meta?.message)
                } else {
                    mProposalsApiListener.onProposalsApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<IncludeProposalsResponse>?, t: Throwable?) {
                mProposalsApiListener.onProposalsApiFailure(t?.message.toString())
            }
        })
    }

    fun checkProposalApiCall(mActivity: Activity, userId: String): Unit {
        callProposalData = TetootaApplication.getHeader().checkProposalsData(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), userId)

        callProposalData!!.enqueue(object : Callback<CheckProposalsDataResponse> {
            override fun onResponse(call: Call<CheckProposalsDataResponse>?, response: Response<CheckProposalsDataResponse>?) {
                var mIncludeProposalData: CheckProposalsDataResponse? = response?.body()
                if (response?.code() == 200) {

                    mProposalsApiListener.onCheckProposalsDataApiSuccess(mIncludeProposalData!!)
                } else {
                    mProposalsApiListener.onCheckProposalsDataApiFailure(mIncludeProposalData?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<CheckProposalsDataResponse>?, t: Throwable?) {
                mProposalsApiListener.onCheckProposalsDataApiFailure(t?.message.toString())
            }
        })
    }


    fun proposalActionApiCall(mActivity: Activity, mProposalMesgData: ProposalMessageData?, acceptanceType: String, pos: Int, userId: String, chatType: String) {
        proposalActionCall = TetootaApplication.getHeader()
                .actionOnProposal(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mProposalMesgData?.proposal_id!!, acceptanceType, userId, chatType)
        proposalActionCall!!.enqueue(object : Callback<ProposalActionResponse> {
            override fun onResponse(call: Call<ProposalActionResponse>?, response: Response<ProposalActionResponse>?) {
                var mIncludeProposalData: ProposalActionResponse? = response?.body()
                if (response?.code() == 200) {
                    mProposalsApiListener.onProposalActionSuccess(mIncludeProposalData?.meta?.message,
                            mProposalMesgData, acceptanceType, pos)
                } else {
                    mProposalsApiListener.onProposalsApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ProposalActionResponse>?, t: Throwable?) {
                mProposalsApiListener.onProposalsApiFailure(t?.message.toString())
            }
        })
    }

    /**********************************cancel**************************************/

    fun proposalActionCancelApiCall(mActivity: Activity, proposal_id: String, acceptanceType: String, pos: Int, userId: String, chatType: String) {
        proposalActionCall = TetootaApplication.getHeader()
                .actionOnProposal(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), proposal_id, acceptanceType, userId, chatType)
        proposalActionCall!!.enqueue(object : Callback<ProposalActionResponse> {
            override fun onResponse(call: Call<ProposalActionResponse>?, response: Response<ProposalActionResponse>?) {
                var mIncludeProposalData: ProposalActionResponse? = response?.body()
                if (response?.code() == 200) {
                    mProposalsApiListener.onProposalCancelSuccess(mIncludeProposalData?.meta?.message,
                            proposal_id, acceptanceType, pos)
                } else {
                    mProposalsApiListener.onProposalCancelFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ProposalActionResponse>?, t: Throwable?) {
                mProposalsApiListener.onProposalCancelFailure(t?.message.toString())
            }
        })
    }


    fun proposalDealApiCall(mActivity: Activity, mProposalId: String?, acceptanceType: String, referral_user_status: String, user_id: String) {
        Utils.loadPrefrence(Constant.USER_ID, "en", mActivity)
        proposalActionCall = TetootaApplication.getHeader()
                .proposalCompleteDeal(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mProposalId!!, acceptanceType, referral_user_status, user_id)
        proposalActionCall!!.enqueue(object : Callback<ProposalActionResponse> {
            override fun onResponse(call: Call<ProposalActionResponse>?, response: Response<ProposalActionResponse>?) {
                var mIncludeProposalData: ProposalActionResponse? = response?.body()
                if (response?.code() == 200) {
                    mProposalsApiListener.onProposalsAPiSuccess(mIncludeProposalData?.meta?.message)
                } else {
                    mProposalsApiListener.onProposalsApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ProposalActionResponse>?, t: Throwable?) {
                mProposalsApiListener.onProposalsApiFailure(t?.message.toString())
            }
        })
    }

    fun proposalExhangeDealApiCall(mActivity: Activity, mProposalId: String?, acceptanceType: String, receiver_id: String) {
        proposalActionCall = TetootaApplication.getHeader()
                .proposalExchangeCompleteDeal(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mProposalId!!, acceptanceType,
                        Utils.loadPrefrence(Constant.USER_ID, "", mActivity!!))
        proposalActionCall!!.enqueue(object : Callback<ProposalActionResponse> {
            override fun onResponse(call: Call<ProposalActionResponse>?, response: Response<ProposalActionResponse>?) {
                var mIncludeProposalData: ProposalActionResponse? = response?.body()
                if (response?.code() == 200) {
                    mProposalsApiListener.onProposalsAPiSuccess(mIncludeProposalData?.meta?.message)
                } else {
                    mProposalsApiListener.onProposalsApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ProposalActionResponse>?, t: Throwable?) {
                mProposalsApiListener.onProposalsApiFailure(t?.message.toString())
            }
        })
    }

    /**
     * Method to Get PushNotificationDataResponse from Proposal Id
     */
    fun getProposalById(proposalId: String, mActivity: Activity) {
        var call: Call<ProposalByIdResp>? = null
        call = TetootaApplication.getHeader()
                .proposalById(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        proposalId)
        call!!.enqueue(object : Callback<ProposalByIdResp> {
            override fun onResponse(call: Call<ProposalByIdResp>?, response: Response<ProposalByIdResp>?) {
                println("Success")
                var mIncludeProposalData: ProposalByIdResp? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        mProposalsApiListener.onProposalByIdSuccess(mIncludeProposalData?.meta?.message,
                                mIncludeProposalData?.data?.get(0))
                    } else {
                        mProposalsApiListener.onProposalsApiFailure(response.body()?.meta?.message.toString())
                    }
                } else {
                    mProposalsApiListener.onProposalsApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ProposalByIdResp>?, t: Throwable?) {
                mProposalsApiListener.onProposalsApiFailure(t?.message.toString())
            }
        })
    }

    fun addReviewApi(mActivity: Activity, mUserId: String,
                     mPostId: String, response_time: String, quality_services: String,
                     friendliness: String, review_message: String) {
        var proposalActionCall: Call<ReviewApiResponse>? = null
        proposalActionCall = TetootaApplication.getHeader()
                .addReviews(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId, mPostId, response_time, quality_services,
                        friendliness, review_message)
        proposalActionCall.enqueue(object : Callback<ReviewApiResponse> {
            override fun onResponse(call: Call<ReviewApiResponse>?, response: Response<ReviewApiResponse>?) {
                if (response?.code() == 200) {
                    mProposalsApiListener.onAddReviewApISuccess(response.message().toString())
                } else {
                    mProposalsApiListener.onAddReviewApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ReviewApiResponse>?, t: Throwable?) {
                mProposalsApiListener.onProposalsApiFailure(t?.message.toString())
            }
        })
    }

    fun foodCompleteApi(mActivity: Activity, proposalId: String,
                        foodStatus: String) {
        var statusCode = 0
        var message = ""
        var proposalActionCall: Call<ResponseBody>? = null
        proposalActionCall = TetootaApplication.getHeader()
                .foodRequestComplete(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), proposalId, foodStatus)
        proposalActionCall.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Log.i(javaClass.name, "==============" + response?.code())
                if (response.code() == 200) {
                    var jobj = JSONObject(response.body()!!.string())
                    var metaJobj = jobj.getJSONObject("meta")
                    if (metaJobj.has("code"))
                        statusCode = metaJobj.getInt("code")
                    if (metaJobj.has("message"))
                        message = metaJobj.getString("message")
                    if (statusCode == 200) {
                        mProposalsApiListener.onFoodCompleteSuccess(message)
                    } else {
                        mProposalsApiListener.onFoodCompleteFailure(message)
                    }
                } else {
                    mProposalsApiListener.onFoodCompleteFailure("failure")
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                mProposalsApiListener.onFoodCompleteFailure(t?.message.toString())
            }
        })
    }

    fun foodListUnList(mActivity: Activity, postId: String) {
        var statusCode = 0
        var message = ""
        var proposalActionCall: Call<ResponseBody>? = null
        proposalActionCall = TetootaApplication.getHeader()
                .foodListUnlist(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), postId)
        proposalActionCall.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.code() == 200) {
                    var jobj = JSONObject(response.body()!!.string())
                    var metaJobj = jobj.getJSONObject("meta")
                    if (metaJobj.has("code"))
                        statusCode = metaJobj.getInt("code")
                    if (metaJobj.has("message"))
                        message = metaJobj.getString("message")
                    Log.i(javaClass.name, "==============" + message)
                    if (statusCode == 200) {
                        mProposalsApiListener.onFoodListUnListSuccess(message)
                    } else {
                        mProposalsApiListener.onFoodListUnListFailure(message)
                    }
                } else {
                    mProposalsApiListener.onFoodListUnListFailure("failure")
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                mProposalsApiListener.onFoodListUnListFailure(t?.message.toString())
            }
        })
    }
}
