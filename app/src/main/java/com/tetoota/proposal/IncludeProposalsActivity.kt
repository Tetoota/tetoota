package com.tetoota.proposal

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.proposal.CheckProposalsDataResponce.CheckProposalsDataResponse
import com.tetoota.proposal.CheckProposalsDataResponce.DataItem
import com.tetoota.R
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.addrequest.BottomSheetDialog
import com.tetoota.addrequest.TradingDataResponse
import com.tetoota.customviews.CustomServiceAddAlert
import com.tetoota.details.CustomDropDownAdapter
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.include_proposals_activity.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

class IncludeProposalsActivity : BaseActivity(), ServiceContract.View, CustomServiceAddAlert.IDialogListener,
        BottomSheetDialog.IBottomSheetListener, ProposalsContract.View, View.OnClickListener, IDailogListener {
    override fun onFoodListUnListSuccessResult(message: String?) {

    }

    override fun onFoodCompleteSuccessResult(message: String?) {

    }

    private var mServiceData: ServicesDataResponse? = null
    private var customDailog: CustomDailogAddFavorite? = null
    private var exchangePostId: String = ""
    private var exchangePostType: String = ""
    private var proposalTime: String = "1 week"
    private var isAbalToSendProposal: Boolean = false
    private var serviceCount: Int = 0
    private var listType = ""

    private var list = ArrayList<String>()
    private var mList: List<ServicesDataResponse>? = null


    private val mProposalPresenter: ProposalsPresenter by lazy {
        ProposalsPresenter(this@IncludeProposalsActivity)
    }
    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@IncludeProposalsActivity)
    }

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.include_proposals_activity)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        initToolbar()
        fetchWishListData()
        getUserProposalsData()
        getDataFromPreviousScreen()
        clickListener()


        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (position > 0) {

                    exchangePostId = mList!![position - 1].id.toString()
                    exchangePostType = mList!![position - 1].post_type.toString()
                    tv_exchangeServiceProduct.text = mList!![position - 1].title
                    var str = mList!![position - 1].set_quote
                    val newStr = str!!.replaceFirst("PER", "")
                    tv_per.text = "1 " + newStr
                    // Toast.makeText(this@IncludeProposalsActivity,  "Selected item =  " + list[position], Toast.LENGTH_SHORT).show()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }


    }

    override fun onWishListApiSuccessResult(myServiceList: List<Any?>?, message: String) {
        Log.e("SIZE_LIST", "SIZE_LIST")
        hideProgressDialog()
        mList = myServiceList as List<ServicesDataResponse>

        if (myServiceList != null) {
            Log.e("sizeeeeeeeee ", "" + myServiceList.size)
            serviceCount = myServiceList!!.size


            list.add("Exchange Service/Product")
            for (model in mList!!) {
                list.add(model.title!!)
            }
            spinnerservice(list)
        } else {
            serviceCount = 0
        }

    }


    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
        // proposalTime = Utrl_exchangeServiceProductils.getText(this, StringConstant.str_proposalTime)
        toolbar_title.text = Utils.getText(this, StringConstant.start_converse_include)
        tv_sendProposal.text = Utils.getText(this, StringConstant.include_proposal_send)
        //include_proposal_message.text = Utils.getText(this, StringConstant.include_proposal_message)
        // tv_time_with_in.text = Utils.getText(this, StringConstant.include_proposal_within)
        tv_time_with_in.text = "Within"
        //  tv_i_want.text = Utils.getText(this, StringConstant.str_tv_i_want)
        if (listType.equals("wishlist"))
            tv_will_offer.text = Utils.getText(this, StringConstant.str_wishlist_you_will_give)
        else
            tv_will_offer.text = Utils.getText(this, StringConstant.str_tv_include_proposal_will_offer)
        tvOr.text = Utils.getText(this, StringConstant.or)
        tv_points_points.text = Utils.getText(this, StringConstant.str_tv_points)
        tv_exchange_service_product.text = Utils.getText(this, StringConstant.str_exchange_service_product)
    }

    /**
     * Method to initailize all click Listener
     */
    private fun clickListener() {
        tv_sendProposal.setOnClickListener(this)
        tv_time.setOnClickListener(this)
        rl_exchangeServiceProduct.setOnClickListener(this)
        iv_points.setOnClickListener(this)
        iv_exchangeServiceProduct.setOnClickListener(this)
        setMultiLanguageText()
    }

    private fun getDataFromPreviousScreen() {
        mServiceData = intent.getParcelableExtra<ServicesDataResponse>("mServiceDataResponse")
        listType = intent.getStringExtra("list_type")
//        var spinnerAdapter = CustomDropDownAdapter(this, mServiceData as MutableList<ServicesDataResponse?>)
//        var spinner: Spinner = findViewById(R.id.mSpinner) as Spinner
//        spinner.adapter = spinnerAdapter

        if (mServiceData!!.profile_image!!.isEmpty()) {
            iv_user.setImageResource(R.drawable.user_placeholder);
        } else {
            Picasso.get().load(mServiceData!!.profile_image).placeholder(R.drawable.lohgo).into(iv_user)
        }
/*
        if (mServiceData!!.profile_image != "" && mServiceData!!.profile_image != null) {


            Glide.with(this@IncludeProposalsActivity)
                    .load(Utils.getUrl(this@IncludeProposalsActivity, mServiceData!!.profile_image!!))
                    .placeholder(R.drawable.lohgo)
                    .error(R.drawable.lohgo)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .dontAnimate()
                    .into(iv_user)
        } else Glide.with(this@IncludeProposalsActivity)
                .load("")
                .placeholder(R.drawable.user_placeholder)
                .error(R.drawable.user_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .dontAnimate()
                .into(iv_user)
*/
        var str = mServiceData!!.set_quote
        val newStr = str!!.replaceFirst("PER", "")
        Log.e("TAg", "fffffffff " + mServiceData!!.set_quote)
        // tv_i_want.text = "I want 1 " +newStr+" of "
        if (listType.equals("wishlist"))
            tv_i_want.text = Utils.getText(this, StringConstant.str_wishlist_i_can_give) + " " + newStr + " " + Utils.getText(this, StringConstant.str_wishlist_i_can_give_of)
        else
            tv_i_want.text = Utils.getText(this, StringConstant.str_tv_i_want) + " " + newStr + " " + Utils.getText(this, StringConstant.str_of)



        tv_productTitle.text = mServiceData!!.title
        // tv_time.text = "1 " + Utils.getText(this, StringConstant.include_proposal_week)
//        tv_time.text = "1 " + "week"
        tv_from_username.text = Utils.getText(this, StringConstant.includeproposal_by) + "(${mServiceData!!.user_first_name.toString()})"
        tv_exchangeServiceProduct.text = Utils.getText(this, StringConstant.include_proposal_exchange_title)
//        if (mServiceData!!.trading_preference == "1") {
//            ll_points.visibility = View.VISIBLE
//            iv_points.visibility = View.INVISIBLE
//            rl_exchangeServiceProduct.visibility = View.GONE
//        } else if (mServiceData!!.trading_preference == "2") {
//            ll_points.visibility = View.GONE
//            rl_exchangeServiceProduct.visibility = View.VISIBLE
//
//            iv_exchangeServiceProduct.tag = "selected"
//            iv_exchangeServiceProduct.visibility = View.INVISIBLE
//        } else if (mServiceData!!.trading_preference == "3") {
//            ll_points.visibility = View.VISIBLE
//            iv_points.visibility = View.VISIBLE
//            rl_exchangeServiceProduct.visibility = View.VISIBLE
//            iv_exchangeServiceProduct.visibility = View.VISIBLE
//        }

        if (mServiceData!!.trading_preference == "1") {
            ll_points.visibility = View.VISIBLE
            iv_points.visibility = View.VISIBLE
            rl_exchangeServiceProduct.visibility = View.VISIBLE
            iv_exchangeServiceProduct.visibility = View.VISIBLE
        } else if (mServiceData!!.trading_preference == "2") {
            ll_points.visibility = View.VISIBLE
            iv_points.visibility = View.INVISIBLE
            rl_exchangeServiceProduct.visibility = View.GONE
        } else if (mServiceData!!.trading_preference == "3") {
            ll_points.visibility = View.GONE
            rl_exchangeServiceProduct.visibility = View.VISIBLE
            iv_exchangeServiceProduct.tag = "selected"
            iv_exchangeServiceProduct.visibility = View.INVISIBLE
        }
        if (mServiceData!!.trading_preference != "3") {
            if (!mServiceData!!.tetoota_points?.isEmpty()!!) {
                ll_points.visibility = View.VISIBLE
                tv_points.text = mServiceData!!.tetoota_points.toString()
            } else {
                ll_points.visibility = View.GONE
            }
        }
        tv_unit.text = mServiceData!!.set_quote.toString()
    }

    /***
     * Open Custom Add Fevorite Dialog.

     * @param dailogId
     * *
     * @param dailogMsg
     * *
     * @param yesButtonText
     * *
     * @param noButtonText
     */
    private fun openCustomTimeDialog(dailogId: String, dailogMsg: String) {
        customDailog = CustomDailogAddFavorite(dailogId, this@IncludeProposalsActivity, this@IncludeProposalsActivity, dailogMsg,
                createItems())
        customDailog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        customDailog?.show()
        customDailog?.setCanceledOnTouchOutside(true)
    }

    private fun openCustomExchangeServiceProductDialog(dailogId: String, dailogMsg: String) {
        customDailog = CustomDailogAddFavorite(dailogId, this@IncludeProposalsActivity, this@IncludeProposalsActivity, dailogMsg,
                createServiceProductItems())
        println("kkkkkkkkkkkkkkk" + createServiceProductItems())
        customDailog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        customDailog?.show()
        customDailog?.setCanceledOnTouchOutside(true)
    }

    fun createItems(): ArrayList<String> {
        val items = ArrayList<String>()
        /*   items.add("1 " + Utils.getText(this, StringConstant.include_proposal_week))
           items.add("2 " + Utils.getText(this, StringConstant.include_proposal_weeks))
           items.add("3 " + Utils.getText(this, StringConstant.include_proposal_weeks))
           items.add("4 " + Utils.getText(this, StringConstant.include_proposal_weeks))*/
        items.add("1 " + "week")
        items.add("2 " + "weeks")
        items.add("3 " + "weeks")
        items.add("4 " + "weeks")

        return items
    }

    fun spinnerservice(list: List<String>?) {
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, list!!)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        //Setting the ArrayAdapter data on the Spinner
        spinner.adapter = aa
    }


    fun createServiceProductItems(): java.util.ArrayList<String>? {
        val items = java.util.ArrayList<String>()
        items.add("Services")
        items.add("Products")
        return items
    }

    private fun initToolbar(): Unit {
        iv_close.visibility = View.VISIBLE
        iv_close.onClick {
            onBackPressed()
        }
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, IncludeProposalsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@IncludeProposalsActivity)
        finish()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun getProposalsData() {
        if (Utils.haveNetworkConnection(this@IncludeProposalsActivity)) {

            if (isAbalToSendProposal) {
                Log.e("TAG", "ssssssssssssssss ifff = " + isAbalToSendProposal)

                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                if (iv_exchangeServiceProduct.tag.equals("selected") && exchangePostId.equals("")) {
                    showSnackBar(Utils.getText(this, StringConstant.send_proposal_service_exchange_alert))
                } else {
                    if (iv_exchangeServiceProduct.tag.equals("selected")) {
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))

                        /*************************proposalTime hindi english check ***************************************/
                        val proposalTimeLocal: String = tv_time_with_in.text.toString() + " " + proposalTime

                        /*   if (proposalTimeLocal == "week")
                           {

                           }*/

                        /****************************************************************/


                        mProposalPresenter.getProposalsData(this@IncludeProposalsActivity,
                                mServiceData?.id.toString(), mServiceData?.user_id.toString(), personData.user_id!!,
                                "0", exchangePostId, exchangePostType, "", Constant.SERVICES)
                    } else if (iv_points.tag.equals("selected")) {
                        println("Points Deduction ${personData.tetoota_points}")
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))

                        val proposalTimeLocal: String = tv_time_with_in.text.toString() + " " + proposalTime
                        mProposalPresenter.getProposalsData(this@IncludeProposalsActivity,
                                mServiceData?.id.toString(), mServiceData?.user_id.toString(), personData.user_id!!,
                                mServiceData?.tetoota_points.toString(), exchangePostId, "", proposalTimeLocal, Constant.SERVICES)
                    }
                }
            } else {
                Log.e("TAG", "ssssssssssssssss elsee = " + isAbalToSendProposal)

                showSnackBar(Utils.getText(this, StringConstant.proposal_pending_alert))
            }
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    private fun getUserProposalsData() {
        if (Utils.haveNetworkConnection(this@IncludeProposalsActivity)) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mProposalPresenter.checkProposal(this@IncludeProposalsActivity, personData.user_id!!)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    fun gettetootaPts(): Int {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@IncludeProposalsActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        println("Points Deduction ${personData.tetoota_points}")
        return Integer.parseInt(personData.tetoota_points.toString())
    }

    override fun onProposalsFailureResult(message: String) {
        hideProgressDialog()
        showSnackBar(message)
    }

    override fun onProposalsApiSuccessResult(message: String?) {
        hideProgressDialog()
//        showSnackBar(message!!)
        showSnackBarWithCallback(message!!).addCallback(object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                setResult(Activity.RESULT_OK)
                finish()
            }
        })
    }

    override fun onUserProposalsDataFailure(message: String?) {
        super.onUserProposalsDataFailure(message)
        hideProgressDialog()
        showSnackBar(message!!)
    }

    override fun onUserProposalsDataSuccess(checkProposalsDataResponse: CheckProposalsDataResponse?) {
        super.onUserProposalsDataSuccess(checkProposalsDataResponse)
        hideProgressDialog()
        var dataList: List<DataItem?> = checkProposalsDataResponse!!.data!!

        /*if (dataList.size!! > 0) {
            for (checkProposalListData in dataList) {
                if (checkProposalListData!!.postId!!.equals(mServiceData?.id)) {
                    if (checkProposalListData!!.tradingStatus.equals("Complete", true) ||
                            checkProposalListData!!.proposalStatus.equals("Decline", true) ||
                            checkProposalListData!!.tradingStatus.equals("Cancel", true)||
                            checkProposalListData!!.receiver_trading_status.equals("Cancel", true)||
                            checkProposalListData!!.receiver_trading_status.equals("Complete", true)) {
                        isAbalToSendProposal = true
                        break
                    } else {
                        isAbalToSendProposal = false
                        break
                    }
                } else {
                    isAbalToSendProposal = true
                }
            }
        } else {
            isAbalToSendProposal = true
        }*/

        if (dataList.size > 0) {
            for (checkProposalListData in dataList) {
                Log.e("TAG", "ssssssssssssssss size() = " + checkProposalListData.toString())

                if (checkProposalListData!!.postId!!.equals(mServiceData?.id)) {
                    if (mServiceData!!.trading_preference == "1" || mServiceData!!.trading_preference == "") {

                        if (checkProposalListData.tradingStatus.equals("Complete", true) ||
                                checkProposalListData.proposalStatus.equals("Decline", true) ||
                                checkProposalListData.tradingStatus.equals("Cancel", true)) {
                            isAbalToSendProposal = true
                            Log.e("TAG", "ssssssssssssssss size() = IF" + checkProposalListData.toString())
                            break
                        } else if (checkProposalListData.tradingStatus.equals("Pending", true) ||
                                checkProposalListData.proposalStatus.equals("Decline", true) ||
                                checkProposalListData.tradingStatus.equals("Pending", true)) {
                            isAbalToSendProposal = true
                            Log.e("TAG", "ssssssssssssssss size() = IF" + checkProposalListData.toString())
                            break
                        } else {
                            isAbalToSendProposal = false
                            break
                        }
                    }
                    /*   else if(mServiceData!!.trading_preference == "2"){
                           if (checkProposalListData!!.tradingStatus.equals("Complete", true) ||
                                   checkProposalListData!!.proposalStatus.equals("Decline", true) ||
                                   checkProposalListData!!.tradingStatus.equals("Cancel", true)) {
                               isAbalToSendProposal = true
                               break
                           } else {
                               isAbalToSendProposal = false
                               break
                           }
                       }*/
                    else {
                        if (checkProposalListData.proposalStatus.equals("Decline", true) ||
                                checkProposalListData.receiver_trading_status.equals("Cancel", true) ||
                                checkProposalListData.receiver_trading_status.equals("Complete", true)) {
                            isAbalToSendProposal = true

                            break
                        } else if (checkProposalListData.proposalStatus.equals("Decline", true) ||
                                checkProposalListData.receiver_trading_status.equals("Pending", true) ||
                                checkProposalListData.receiver_trading_status.equals("Pending", true)) {
                            isAbalToSendProposal = true

                            break
                        } else {
                            isAbalToSendProposal = false

                            break
                        }
                    }
                } else {
                    isAbalToSendProposal = true
                }
            }
        } else {
            isAbalToSendProposal = true
        }
    }

    /*
        private fun validate(): Boolean {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@IncludeProposalsActivity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            val requiredTetootaPoints: Int? = mServiceData!!.tetoota_points.toString().trim().toIntOrNull()
            if (ll_points.visibility == View.VISIBLE && requiredTetootaPoints!! > 0
                    && requiredTetootaPoints >= personData.tetoota_points!!.toInt()) {
                Log.e("vvvvvvvvvvvvvvvvvvvvvvvvv","")
                toast(Utils.getText(this, StringConstant.send_proposal_insufficient_point_alert))
                return false
            }
            return true
        }
    */
    private fun validate(): Boolean {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@IncludeProposalsActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        val requiredTetootaPoints: Int? = mServiceData!!.tetoota_points.toString().trim().toIntOrNull()

        if (ll_points.visibility == View.VISIBLE && requiredTetootaPoints!! > 0
                && requiredTetootaPoints > personData.tetoota_points!!.toInt() && iv_points.tag.equals("selected")) {
            //  Log.e("vvvvvvvvvv", "" + personData.tetoota_points!!)
            // Log.e("requiredTetootaPoints", "" + requiredTetootaPoints)
            toast(Utils.getText(this, StringConstant.send_proposal_insufficient_point_alert))
            return false
        }
        return true
    }


    override fun onClick(v: View?) {
        when (v) {
            tv_sendProposal -> {
                if (serviceCount > 0) {
                    if (!validate()) {
//                    hideProgressDialog()
//                    println("Device")

                    } else {

                        getProposalsData()
                    }
                } else {
                    CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this@IncludeProposalsActivity, getString(R.string.err_msg_mobile_number_limit)).show()
                }

            }
            tv_time -> {
                openCustomTimeDialog("1", Utils.getText(this, StringConstant.include_proposal_tradetime_alert))
            }
            rl_exchangeServiceProduct -> {
                if (iv_exchangeServiceProduct.tag.equals("selected")) {
                    //  openCustomExchangeServiceProductDialog("2", "Please select the item from list")
                    if (serviceCount > 0) {
                        spinner.performClick()
                        // openServiceProductListActivity("Services")

                    } else {
                        CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                this@IncludeProposalsActivity, getString(R.string.err_msg_mobile_number_limit)).show()

                    }

                }
            }
            iv_points -> {
                if (iv_points.tag.equals("unselected")) {
                    iv_points.tag = "selected"
                    iv_exchangeServiceProduct.tag = "unselected"
                    iv_points.setImageResource(R.drawable.ic_adjust_black_24dp)
                    iv_exchangeServiceProduct.setImageResource(R.drawable.ic_panorama_fish_eye_black_24dp)
                    //include_proposal_message.visibility = View.INVISIBLE
                }
//                else {
//                    //include_proposal_message.visibility = View.VISIBLE
//                    iv_points.tag = "unselected"
//                    iv_exchangeServiceProduct.tag = "selected"
//                    iv_points.setImageDrawable(resources.getDrawable(R.drawable.radio))
//                    iv_exchangeServiceProduct.setImageDrawable(resources.getDrawable(R.drawable.dot))
//                }
            }

            iv_exchangeServiceProduct -> {
                if (iv_exchangeServiceProduct.tag.equals("unselected")) {
                    //include_proposal_message.visibility = View.VISIBLE
                    iv_exchangeServiceProduct.tag = "selected"
                    iv_points.tag = "unselected"
                    iv_exchangeServiceProduct.setImageResource(R.drawable.ic_adjust_black_24dp)
                    iv_points.setImageResource(R.drawable.ic_panorama_fish_eye_black_24dp)
                    if (iv_exchangeServiceProduct.tag.equals("selected")) {
                        if (serviceCount > 0) {
                            // openServiceProductListActivity("Services")
                            spinner.performClick()
                        } else {
                            CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                    this@IncludeProposalsActivity, getString(R.string.err_msg_mobile_number_limit)).show()
                        }
                        //  openCustomExchangeServiceProductDialog("2", "Please select the item from list")

                    }

                }
//                else {
//                    //include_proposal_message.visibility = View.INVISIBLE
//                    iv_exchangeServiceProduct.tag = "unselected"
//                    iv_points.tag = "selected"
//                    iv_exchangeServiceProduct.setImageDrawable(resources.getDrawable(R.drawable.radio))
//                    iv_points.setImageDrawable(resources.getDrawable(R.drawable.dot))
//                }
            }
        }
    }

    override fun dataClick(mTredingDataResponse: Any, type: String) {
        if (type == "category") {
            val mTradingData = mTredingDataResponse as String
            tv_time.text = mTradingData
        } else {
            val mTradingData = mTredingDataResponse as TradingDataResponse
        }
    }

    override fun onItemClick(id: String, itemName: String) {
        if (id.equals("1")) {
            proposalTime = itemName
            // Log.e("ccccccccccc","" + proposalTime)
            tv_time.text = proposalTime
            customDailog?.dismiss()
            customDailog = null
        } else if (id.equals("2")) {
            exchangePostType = itemName
            openServiceProductListActivity(exchangePostType)
            customDailog?.dismiss()
            customDailog = null
        }
    }

    fun openServiceProductListActivity(itemName: String) {
        val intent = ExchnageServiceProductListActivity.newMainIntent(this@IncludeProposalsActivity)
        if (intent != null) {
            intent.putExtra("itemName", itemName)
        }
        ActivityStack.getInstance(this@IncludeProposalsActivity)
        startActivityForResult(intent, 10)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            10 -> {
                if (resultCode == Activity.RESULT_OK) {
                    val returnValue = data?.getStringExtra("item")
                    exchangePostType = data!!.getStringExtra("post_type")
                    exchangePostId = data.getStringExtra("exchangeId")!!
                    tv_exchangeServiceProduct.text = returnValue
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    showSnackBar("Canceled By User")
                }
            }
        }
    }

    /*******************************user have no service then so add new service********/

    private fun fetchWishListData() {
        if (Utils.haveNetworkConnection(this@IncludeProposalsActivity)) {
            mServicePresenter.getServicesData(this@IncludeProposalsActivity, "ServicesProducts", 1,Utils.loadPrefrence(Constant.USER_ID, "", this@IncludeProposalsActivity).toString())
        } else {
            toast(Utils.getText(this, StringConstant.str_check_internet))
        }
    }


    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        hideProgressDialog()
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onYes(param: String, message: String) {
        val intent = AddPostRequestActivity.newMainIntent(this)
        intent!!.putExtra("Tab", "serviceTab")
        ActivityStack.getInstance(this!!)
        startActivity(intent)
        finish()
    }
}
