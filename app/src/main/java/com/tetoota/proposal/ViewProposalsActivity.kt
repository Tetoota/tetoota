package com.tetoota.proposal

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableString
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.proposal.ProposalByIdData
import com.tetoota.proposal.ProposalsContract
import com.tetoota.proposal.ProposalsPresenter
import com.tetoota.R
import com.tetoota.customviews.ReviewProposalDialog
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.toolbar_layout.*
import kotlinx.android.synthetic.main.view_proposals_activity.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import javax.security.auth.login.LoginException
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.style.ForegroundColorSpan
import android.graphics.Paint.UNDERLINE_TEXT_FLAG
import com.tetoota.fragment.help.HelpFragment
import com.tetoota.help.HelpActivity
import kotlinx.android.synthetic.main.activity_messaging.*
import kotlinx.android.synthetic.main.view_proposals_activity.btn_cancelDeal
import kotlinx.android.synthetic.main.view_proposals_activity.btn_completeDeal
import kotlinx.android.synthetic.main.view_proposals_activity.iv_tetoota_logo
import kotlinx.android.synthetic.main.view_proposals_activity.iv_user
import kotlinx.android.synthetic.main.view_proposals_activity.ll_points
import kotlinx.android.synthetic.main.view_proposals_activity.ll_serviceTaken
import kotlinx.android.synthetic.main.view_proposals_activity.tv
import kotlinx.android.synthetic.main.view_proposals_activity.tv_from_username
import kotlinx.android.synthetic.main.view_proposals_activity.tv_points
import kotlinx.android.synthetic.main.view_proposals_activity.tv_pointsValue
import kotlinx.android.synthetic.main.view_proposals_activity.tv_productTitle
import kotlinx.android.synthetic.main.view_proposals_activity.tv_proposal_mesg
import kotlinx.android.synthetic.main.view_proposals_activity.tv_serviceTaken
import kotlinx.android.synthetic.main.view_proposals_activity.tv_timePeriodValue
import kotlinx.android.synthetic.main.view_proposals_activity.tv_timep
import kotlinx.android.synthetic.main.view_proposals_activity.tv_unit
import kotlinx.android.synthetic.main.view_proposals_activity_new.*


/**
 * Created by jitendra.nandiya on 09-08-2017.
 * Last Edit By Charchit Kasliwal 16/08/2017
 */
class ViewProposalsActivity : BaseActivity(),
        View.OnClickListener, ProposalsContract.View, ProfileDetailContract.View, ReviewProposalDialog.IRatingDialogListener {
    override fun onFoodListUnListSuccessResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFoodCompleteSuccessResult(message: String?) {

    }

    override fun onProfileSendEmailApiSuccessResult(message: String) {
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
    }

    var link: String = "http://tetoota.com/service/?"
    lateinit var shortLink: Uri

    override fun onOptionalInformationApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        val profileData: ProfileDataResponse = message?.get(0)!!
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ViewProposalsActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        personData.tetoota_points = profileData.tetoota_points
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@ViewProposalsActivity)
//        clearStack()
        openSharingDialog()
    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
//        clearStack()
        openSharingDialog()
    }

    private lateinit var mProposal: ProposalByIdData
    private var acceptanceType: String = ""
    var mProposalId: String? = null
    private var mResponseTime: String = ""
    private var mQualityService: String = ""
    private var mFriendLines: String = ""
    private var mReviewMesg: String = ""
    private lateinit var personData: LoginDataResponse
    private var exchangePostType: String = ""
    private val mProposalPresenter: ProposalsPresenter by lazy {
        ProposalsPresenter(this@ViewProposalsActivity)
    }

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@ViewProposalsActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        super.onCreate(savedInstanceState)
        //Old layout is view_proposals_activity
        setContentView(R.layout.view_proposals_activity_new)

        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)

        //  var  binding : ViewProposalsActivityBinding = DataBindingUtil.setContentView(this, R.layout.view_proposals_activity)
        val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ViewProposalsActivity)
        personData = Gson().fromJson(json, LoginDataResponse::class.java)
//        initToolbar()
        getDataFromPreviousScreen()
        btn_completeDeall.setOnClickListener(this)
//        btn_cancelDeal.setOnClickListener(this)
        setMultiLanguageText()
        setSpannable()
    }

    //Add new method to show text as hyperlink
    private fun setSpannable() {
        val wordtoSpan = SpannableString("In case you do not receive points or service mentioned, you can contact us")

        wordtoSpan.setSpan(ForegroundColorSpan(Color.BLUE), 0, wordtoSpan.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        tv_descc.setText(wordtoSpan);
        tv_descc.paintFlags = tv_descc.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG
        tv_descc.setOnClickListener(this)
    }

    private fun getDataFromPreviousScreen() {
        mProposalId = intent.getStringExtra("proposalId")
        if (Utils.haveNetworkConnection(this@ViewProposalsActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mProposalPresenter.getProposalById(mProposalId.toString(), this@ViewProposalsActivity)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
        //  tv.text = Utils.getText(this, StringConstant.proposal_summary)
        // tv_title.text = Utils.getText(this, StringConstant.proposal_summary)
//        btn_cancelDeal.text = Utils.getText(this, StringConstant.cancel_deal)
//        btn_completeDeal.text = Utils.getText(this, StringConstant.complete_deal)
        tv_descc.text = Utils.getText(this, StringConstant.proposal_description)
        tv_proposal_mesg.text = Utils.getText(this, StringConstant.proposal_message)
        tv_timep.text = Utils.getText(this, StringConstant.time_period)
        tv_serviceTaken.text = Utils.getText(this, StringConstant.service_taken)
        tv_points.text = Utils.getText(this, StringConstant.total_points)
    }

    private fun initToolbar(): Unit {
        iv_close.visibility = View.VISIBLE
        toolbar_title.text = Utils.getText(this, StringConstant.str_view_proposals)
        iv_close.onClick {
            onBackPressed()
        }
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ViewProposalsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@ViewProposalsActivity)
        finish()
    }

    fun gettetootaPts(): Int {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ViewProposalsActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        println("Points Deduction ${personData.tetoota_points}")
        return Integer.parseInt(personData.tetoota_points.toString())
    }

    override fun onClick(v: View?) {
        when (v) {
            tv_descc -> {
                HelpFragment.newInstance("", "")
            }
            btn_completeDeall -> {
                if (mProposal != null && mProposal?.exchange_post_type == "") {
                    acceptanceType = "Complete"
                    ReviewProposalDialog(this@ViewProposalsActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this@ViewProposalsActivity, "accept").show()
                } else {
                    acceptanceType = "Complete"
                    ReviewProposalDialog(this@ViewProposalsActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this@ViewProposalsActivity, "accept").show()
                }
            }
            btn_cancelDeall -> {
                if (mProposal.exchange_post_type == "") {
                    if (gettetootaPts() >= mProposal?.tetoota_points!!) {
                        acceptanceType = "Cancel"
                        ReviewProposalDialog(this@ViewProposalsActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                this@ViewProposalsActivity, "cancel").show()
                    } else {
                        showSnackBar("You have not sufficient point to accept this proposal")
                    }
                } else {
                    acceptanceType = "Cancel"
//                    ReviewProposalDialog(this@ViewProposalsActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
//                            this@ViewProposalsActivity, "cancel").show()
                    mProposalPresenter.proposalActionCancel(this, mProposal.proposal_id.toString(), "Decline", 0, Utils.loadPrefrence(Constant.USER_ID, "", this@ViewProposalsActivity).toString(), Constant.SERVICES)
                    startActivity<MainActivity>()
                    finish()
                }
            }
        }
    }

    override fun onProposalsApiSuccessResult(message: String?) {
        hideProgressDialog()
//        if (isProposalExchange == false) {
//            mProposalPresenter.proposalCompleteAction(this@ViewProposalsActivity, mProposalId.toString(), acceptanceType, "text")
//        } else {

        // Log.e("personData.user_id",""+ personData.user_id)
        // Log.e("mProposal.proposal_to",""+ mProposal.proposal_to)
        // Log.e("personData.proposal_from",""+ mProposal.proposal_from)


        if (mProposal.exchange_post_type != "" && personData.user_id == mProposal.proposal_to.toString()) {
            // Log.e("ifffffffffff",""+ personData.user_id)
            mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_to.toString(), mProposal.exchange_post_id.toString()
                    , mResponseTime, mQualityService, mFriendLines, mReviewMesg)

        } else if (personData.user_id == mProposal.proposal_from.toString()) {
            // Log.e(" elseeee ifffffffffff",""+ personData.user_id)
            mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_from.toString(), mProposal.post_id.toString()
                    , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
        } else {
            //  Log.e("elseeeeeeeee",""+ personData.user_id)
            mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_to.toString(), mProposal.post_id.toString()
                    , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
        }

        /*********************************/
        /*  if (personData.user_id == mProposal.proposal_from.toString()) {
               Log.e("ifffffffffff",""+ personData.user_id)
               mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_to.toString(), mProposal.post_id.toString()
                       , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
           } else {
               Log.e("elseeeeeeeeee",""+ personData.user_id)
               mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_from.toString(), mProposal.post_id.toString()
                       , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
           }*/

        /********************************************/
/*
            if (personData.user_id == mProposal.proposal_from.toString()) {
                mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_to.toString(), mProposal.post_id.toString()
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            } else {
                mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_from.toString(), mProposal.post_id.toString()
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            }
*/
//        }
    }

    override fun onProposalsFailureResult(message: String) {
        hideProgressDialog()
        showSnackBar(message)
//        onBackPressed()
    }

    override fun onProposalsByIdSuccessResult(message: String?, proposalByIdData: ProposalByIdData?) {
        super.onProposalsByIdSuccessResult(message, proposalByIdData)
        hideProgressDialog()
        if (proposalByIdData != null) {


            if (proposalByIdData.exchange_post_type != null) {
                if (proposalByIdData.exchange_post_type.equals("Services") || proposalByIdData.exchange_post_type.equals("Products")) {
                    tv.text = Utils.getText(this, StringConstant.str_exchange_service_product) + " " + Utils.getText(this, StringConstant.proposal_by) + proposalByIdData?.firstName
                    ll_serviceTaken.visibility = View.VISIBLE
                    ll_points.visibility = View.GONE
                } else {
                    tv.text = Utils.getText(this, StringConstant.str_tv_points) + " " + Utils.getText(this, StringConstant.proposal_by) + proposalByIdData?.firstName
                    ll_serviceTaken.visibility = View.GONE
                    ll_points.visibility = View.VISIBLE
                }
            }


            if (proposalByIdData.proposal_from != personData.user_id!!.toInt()) {

                Log.e("TAG", " iffffff " + proposalByIdData.proposal_from + " = " + personData.user_id)
                var str = proposalByIdData.sender_set_quota
                val newStr = str!!.replaceFirst("PER", "")
                tvyouwilll.text = Utils.getText(this, StringConstant.str_you_will_give) + " " + newStr + " " + Utils.getText(this, StringConstant.str_of)
                var str1 = proposalByIdData.set_quote
                val newStr1 = str1!!.replaceFirst("PER", "")
                tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_get) + " " + newStr1 + " " + Utils.getText(this, StringConstant.str_of)

                tv_from_username.text = Utils.getText(this, StringConstant.str_by) + " " + personData.first_name + " " + personData.last_name
                tv_to_usernamee.text = Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.firstName + " " + proposalByIdData.lastName
            } else {
                Log.e("TAG", " elseeee  " + proposalByIdData.proposal_from + " = " + personData.user_id)
                var str = proposalByIdData.sender_set_quota
                val newStr = str!!.replaceFirst("PER", "")
                tvyouwilll.text = Utils.getText(this, StringConstant.str_you_will_get) + " " + newStr + " " + Utils.getText(this, StringConstant.str_of)

                if (proposalByIdData.exchange_post_type!!.isEmpty()) {

                    tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_give_points)
                    tv_from_username.text = Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.receiver_name
                    tv_to_usernamee.text = Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.firstName + " " + proposalByIdData.lastName

                } else {
                    var str1 = proposalByIdData.set_quote
                    val newStr1 = str1!!.replaceFirst("PER", "")
                    tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_give) + " " + newStr1 + " " + Utils.getText(this, StringConstant.str_of)
                    tv_from_username.text = Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.receiver_name
                    tv_to_usernamee.text = Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.firstName + " " + proposalByIdData.lastName
                }


            }

            /*  tv_from_username.text = "by "+ personData.first_name+" "+personData.last_name
              tv_to_usernamee.text = "by "+ proposalByIdData.firstName+" "+proposalByIdData.lastName*/


            mProposal = proposalByIdData
            tv_productTitle.text = proposalByIdData.title


            // tv_unit.text = Utils.getText(this, StringConstant.proposal_tetoota_points)
            tv_unit.text = Utils.getText(this, StringConstant.str_tetoota_points)
            exchangePostType = proposalByIdData.exchange_post_type!!

            if (proposalByIdData.exchange_post_type.isEmpty()) {

                ll_serviceTaken.visibility = View.GONE
                ll_points.visibility = View.VISIBLE
                tv_pointsValue.text = proposalByIdData.set_tetoota_points.toString()
                tv_serviceTakenValuee.text = proposalByIdData.title.toString()
                iv_tetoota_logo.setImageResource(R.drawable.lohgo);
                iv_tetoota_logo.visibility = View.VISIBLE
                iv_user3.visibility = View.GONE
            } else {
                iv_user3.visibility = View.VISIBLE
                iv_tetoota_logo.visibility = View.GONE
                ll_points.visibility = View.GONE
                ll_serviceTaken.visibility = View.VISIBLE
                tv_serviceTakenValuee.text = proposalByIdData.exchange_post_title.toString()
                if (proposalByIdData.sender_profile_image!!.isEmpty()) {
                    iv_user3.setImageResource(R.drawable.user_placeholder);
                } else {

                    Picasso.get().load(proposalByIdData.sender_profile_image).placeholder(R.drawable.user_placeholder).into(iv_user3)
                }
            }
            tv_timePeriodValue.text = proposalByIdData.proposal_time.toString()


            //  tv_from_username.text = Utils.getText(this, StringConstant.proposal_by) + "(${proposalByIdData?.firstName} ${proposalByIdData?.lastName})"


            if (proposalByIdData.profile_image!!.isEmpty()) {
                iv_user.setImageResource(R.drawable.user_placeholder);
            } else {

                Picasso.get().load(proposalByIdData.profile_image).placeholder(R.drawable.user_placeholder).into(iv_user)
            }


        }
    }


    /**
     * the on Submit pressed listener
     */
    override fun onSubmitPressed(param: String, message: String, mResponseTime: String, mQualityService: String, mFriendLines: String, mReviewMesg: String) {
        if (Utils.haveNetworkConnection(this@ViewProposalsActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            this.mResponseTime = mResponseTime
            this.mQualityService = mQualityService
            this.mFriendLines = mFriendLines
            this.mReviewMesg = mReviewMesg


            val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)

            Log.e("personData", personData.toString())
            if (exchangePostType.isEmpty() || personData.user_id == mProposal.proposal_from.toString()) {
                Log.e("TAG", "hhhhhh if =  " + acceptanceType)
                mProposalPresenter.proposalCompleteAction(this@ViewProposalsActivity, mProposalId.toString(), acceptanceType, "", "personData.referral_string!!", personData.user_id!!)
            } else {
                Log.e("TAG", "hhhhhh else =  " + acceptanceType)
                mProposalPresenter.proposalCompleteAction(this@ViewProposalsActivity, mProposalId.toString(), acceptanceType, "text", "personData.referral_string!!", personData.user_id!!)
            }

            /* if (personData.user_id == mProposal.proposal_from.toString()) {
                 mProposalPresenter.proposalCompleteAction(this@ViewProposalsActivity, mProposalId.toString(), acceptanceType, "")//amit sir api calling
             }else {
                 mProposalPresenter.proposalCompleteAction(this@ViewProposalsActivity, mProposalId.toString(), acceptanceType, "text")
             }*/

/*
            if (mProposal.exchange_post_type != "" && personData.user_id == mProposal.proposal_to.toString()) {
                mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_to.toString(), mProposal.exchange_post_id.toString()
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)

            } else {
                mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_to.toString(), mProposal.post_id.toString()
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            }
*/


        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onCancelPressed() {
    }

    override fun onProposalActionSuccessREsult(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {
        super.onProposalActionSuccessREsult(message, mProposalMesg, acceptanceType, pos)
        if (acceptanceType == "complete") {
            showSnackBar("Deal Completed")
            if (personData.user_id == mProposal.proposal_from.toString()) {
                mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_to.toString(), mProposal.post_id.toString()
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            } else {
                mProposalPresenter.addReviewApi(this@ViewProposalsActivity, mProposal.proposal_from.toString(), mProposal.post_id.toString()
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            }
        } else {
            hideProgressDialog()
            showSnackBar("Deal Cancelled by User")
        }
    }

    override fun onAddReviewApISuccessResult(message: String?) {
        super.onAddReviewApISuccessResult(message)
        hideProgressDialog()
        cv_main.visibility = View.GONE
        if (message != null) {
            showSnackBar(message)
        }
        callUserDetailAPI()
    }

    override fun onAddReviewApiFailureResult(message: String?) {
        super.onAddReviewApiFailureResult(message)
        hideProgressDialog()
        if (message != null) {
            showSnackBar(message)
        }
    }

    private fun callUserDetailAPI() {
        cv_main.visibility = View.GONE
        if (Utils.haveNetworkConnection(this@ViewProposalsActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ViewProposalsActivity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileData(this@ViewProposalsActivity, personData)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
//            clearStack()
            openSharingDialog()
        }
    }

    /*fun openSharingDialog() {
        val items = arrayOf<CharSequence>("Would you like to share this trading?","Cancel")
        var builder: AlertDialog.Builder = AlertDialog.Builder(this@ViewProposalsActivity)
        builder.setTitle("Share Trading")
        builder.setItems(items) { dialog, item ->
            if (items[item].equals("Would you like to share this trading?")) {
                shareProduct(mProposal?.title!!, mProposal?.post_image!!)
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss()
                clearStack()
            }
        }
        builder.show()
    }*/
    private fun openSharingDialog() {
        val alertDialog = AlertDialog.Builder(this) //Read Update
        alertDialog.setTitle(Utils.getText(this, StringConstant.send_proposal_share_trading))
        alertDialog.setMessage(Utils.getText(this, StringConstant.send_proposal_share_trading_alert))
        alertDialog.setPositiveButton(Utils.getText(this, StringConstant.tetoota_ok), object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ViewProposalsActivity)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                val postString = personData.first_name + " has exchanged " + mProposal.title!! + " on http://tetoota.com"
                shareProduct(postString, mProposal.post_image!!)
            }
        })
        alertDialog.setNegativeButton(Utils.getText(this, StringConstant.cancel), object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                onBackPressed()
            }
        })
        alertDialog.show()  //<-- See This!
    }

    /**
     * Method to Share Product
     * TODO Deep linking
     */
    fun shareProduct(mTitle: String, mImageUrl: String) {
        try {
            val text = mTitle
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                        BitmapFactory.decodeResource(getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            //val pictureUri = Uri.parse(mImageUrl)
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            //   shareIntent.type = "image/*"
            //   shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
        } catch (ex: android.content.ActivityNotFoundException) {
        }
    }

    fun clearStack() {
        hideProgressDialog()
        val intent = MainActivity.newMainIntent(this@ViewProposalsActivity)
        ActivityStack.getInstance(this@ViewProposalsActivity)
        ActivityStack.cleareAll()
        startActivity(intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            clearStack()
        }
    }
}
