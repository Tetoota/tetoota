package com.tetoota.proposal;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.tetoota.R;

import java.util.ArrayList;

/**
 * Created by jitendra.nandiya on 09-08-2017.
 */

public class CustomDailogAddFavorite extends Dialog implements View.OnClickListener {
    private Activity activity;
    private Context mContext;
    private IDailogListener dailogListener;
    private String id;
    private String dialogMsg ;
    private TextView tv_title, tv_item1, tv_item2, tv_item3, tv_item4;
    private ArrayList<String> itemArray;

    public CustomDailogAddFavorite(final String id, final Context mContext, IDailogListener dailog, String dialogMsg,
                                   ArrayList<String> itemArray) {
        super(mContext);
        this.mContext = mContext;
        this.id = id;
        this.dialogMsg = dialogMsg ;
        this.dailogListener = dailog;
        this.itemArray = itemArray;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);

      //  Log.e("wwwwwwwwwwwwwwwwwww","rereeere" );
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(dialogMsg);
        tv_item1 = (TextView) findViewById(R.id.tv_item1);
        tv_item2 = (TextView) findViewById(R.id.tv_item2);
        tv_item3 = (TextView) findViewById(R.id.tv_item3);
        tv_item4 = (TextView) findViewById(R.id.tv_item4);

        tv_item1.setOnClickListener(this);
        tv_item2.setOnClickListener(this);
        tv_item3.setOnClickListener(this);
        tv_item4.setOnClickListener(this);

        if (itemArray.size() == 1) {
            tv_item1.setVisibility(View.VISIBLE);
            tv_item1.setText(itemArray.get(0));
            tv_item2.setVisibility(View.GONE);
            tv_item3.setVisibility(View.GONE);
            tv_item4.setVisibility(View.GONE);
        } else if (itemArray.size() == 2) {
            tv_item1.setVisibility(View.VISIBLE);
            tv_item1.setText(itemArray.get(0));
            tv_item2.setVisibility(View.VISIBLE);
            tv_item2.setText(itemArray.get(1));
            tv_item3.setVisibility(View.GONE);
            tv_item4.setVisibility(View.GONE);
        } else if (itemArray.size() == 3) {
            tv_item1.setVisibility(View.VISIBLE);
            tv_item1.setText(itemArray.get(0));
            tv_item2.setVisibility(View.VISIBLE);
            tv_item2.setText(itemArray.get(1));
            tv_item3.setVisibility(View.VISIBLE);
            tv_item3.setText(itemArray.get(2));
            tv_item4.setVisibility(View.GONE);
        } else if (itemArray.size() == 4) {
            tv_item1.setVisibility(View.VISIBLE);
            tv_item1.setText(itemArray.get(0));
            tv_item2.setVisibility(View.VISIBLE);
            tv_item2.setText(itemArray.get(1));
            tv_item3.setVisibility(View.VISIBLE);
            tv_item3.setText(itemArray.get(2));
            tv_item4.setVisibility(View.VISIBLE);
            tv_item4.setText(itemArray.get(3));
        } else {
            tv_item1.setVisibility(View.GONE);
            tv_item2.setVisibility(View.GONE);
            tv_item3.setVisibility(View.GONE);
            tv_item4.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_item1:
                dailogListener.onItemClick(id, itemArray.get(0));
                break;
            case R.id.tv_item2:
                dailogListener.onItemClick(id, itemArray.get(1));
                break;
            case R.id.tv_item3:
                dailogListener.onItemClick(id, itemArray.get(2));
                break;
            case R.id.tv_item4:
                dailogListener.onItemClick(id, itemArray.get(3));
                break;
            default:
                break;
        }
        dismiss();
    }
}
