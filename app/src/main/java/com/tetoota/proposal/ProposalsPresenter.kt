package com.tetoota.proposal

import android.app.Activity
import android.util.Log
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.proposal.CheckProposalsDataResponce.CheckProposalsDataResponse
import com.tetoota.utility.Constant

class ProposalsPresenter : ProposalsContract.Presenter, ProposalsContract.ProposalsApiListener {


    override fun checkProposal(mActivity: Activity, userId: String) {
        mProposalsInteractor.checkProposalApiCall(mActivity, userId)
    }


    var mProposalsContract: ProposalsContract.View

    private val mProposalsInteractor: ProposalsInteractor by lazy {
        com.tetoota.proposal.ProposalsInteractor(this)
    }

    constructor(mHomeContract: ProposalsContract.View) {
        this.mProposalsContract = mHomeContract
    }

    override fun getProposalsData(mActivity: Activity, postId: String, proposalTo: String, proposalFrom: String, tetootaPoints: String,
                                  exchangePostId: String, exchangePostType: String, proposalTime: String, type: String) {
        mProposalsInteractor.proposalsApiCalling(mActivity, postId, proposalTo, proposalFrom, tetootaPoints, exchangePostId, exchangePostType, proposalTime, type)
    }

    override fun proposalAction(mActivity: Activity, mProposalMesgData: ProposalMessageData,
                                acceptanceType: String, pos: Int, userId: String, chatType: String) {
        super.proposalAction(mActivity, mProposalMesgData, acceptanceType, pos, userId, chatType)
        mProposalsInteractor.proposalActionApiCall(mActivity, mProposalMesgData, acceptanceType, pos, userId, chatType)
    }

    override fun proposalActionCancel(mActivity: Activity, proposal_id: String,
                                      acceptanceType: String, pos: Int, userId: String, chatType: String) {
        // super.proposalActionCancel(mActivity, proposal_id , acceptanceType, pos)
        mProposalsInteractor.proposalActionCancelApiCall(mActivity, proposal_id, acceptanceType, pos, userId, chatType)
    }


    override fun proposalCompleteAction(mActivity: Activity, mProposalId: String,
                                        acceptanceType: String, receiver_id: String, referral_user_status: String, user_id: String) {
        super.proposalCompleteAction(mActivity, mProposalId, acceptanceType, receiver_id, referral_user_status, user_id)
        if (!receiver_id.isEmpty()) {
            if (acceptanceType == Constant.COMPLETE) {
                mProposalsInteractor.proposalExhangeDealApiCall(mActivity, mProposalId, Constant.COMPLETE, receiver_id)
            } else {
                mProposalsInteractor.proposalExhangeDealApiCall(mActivity, mProposalId, Constant.CANCEL, receiver_id)
            }
        } else {
            if (acceptanceType == Constant.COMPLETE) {
                mProposalsInteractor.proposalDealApiCall(mActivity, mProposalId, Constant.COMPLETE, referral_user_status, user_id)
            } else {
                mProposalsInteractor.proposalDealApiCall(mActivity, mProposalId, Constant.CANCEL, referral_user_status, user_id)
            }
        }
    }

    override fun addReviewApi(mActivity: Activity, mUserId: String,
                              mPostId: String, response_time: String, quality_services: String,
                              friendliness: String, review_message: String) {
        super.addReviewApi(mActivity, mUserId, mPostId, response_time, quality_services, friendliness,
                review_message)
        mProposalsInteractor.addReviewApi(mActivity, mUserId, mPostId, response_time, quality_services, friendliness,
                review_message)
    }

    override fun getProposalById(proposalId: String, mActivity: Activity) {
        super.getProposalById(proposalId, mActivity)
        mProposalsInteractor.getProposalById(proposalId, mActivity)
    }

    override fun onProposalsAPiSuccess(message: String?) {
        mProposalsContract.onProposalsApiSuccessResult(message)
    }

    override fun onCheckProposalsDataApiFailure(message: String?) {
        super.onCheckProposalsDataApiFailure(message)
        mProposalsContract.onUserProposalsDataFailure(message)
    }

    override fun onCheckProposalsDataApiSuccess(checkProposalsDataResponse: CheckProposalsDataResponse?) {
        super.onCheckProposalsDataApiSuccess(checkProposalsDataResponse)
        mProposalsContract.onUserProposalsDataSuccess(checkProposalsDataResponse)
    }

    override fun onProposalActionSuccess(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {
        mProposalsContract.onProposalActionSuccessREsult(message, mProposalMesg, acceptanceType, pos)
    }

    override fun onProposalsApiFailure(message: String) {
        mProposalsContract.onProposalsFailureResult(message)
    }

    override fun onProposalCancelSuccess(message: String?, proposal_id: String, acceptanceType: String, pos: Int) {
        mProposalsContract.onProposalActionCancelSuccessREsult(message, proposal_id, acceptanceType, pos)
    }

    override fun onProposalCancelFailure(message: String?) {
        mProposalsContract.onProposalsCancelFailureResult(message!!)
    }


    override fun onProposalByIdSuccess(message: String?, proposalByIdData: ProposalByIdData?) {
        super.onProposalByIdSuccess(message, proposalByIdData)
        mProposalsContract.onProposalsByIdSuccessResult(message, proposalByIdData)
    }

    override fun onAddReviewApISuccess(message: String?) {
        super.onAddReviewApISuccess(message)
        mProposalsContract.onAddReviewApISuccessResult(message)
    }

    override fun onAddReviewApiFailure(message: String?) {
        super.onAddReviewApiFailure(message)
        if (message != null) {
            mProposalsContract.onProposalsFailureResult(message)
        }
    }

    override fun onFoodCompleteSuccess(message: String?) {
        super.onFoodCompleteSuccess(message)
        mProposalsContract.onFoodCompleteSuccessResult(message!!)
    }

    override fun onFoodCompleteFailure(message: String?) {
        super.onFoodCompleteFailure(message)
        Log.i(javaClass.name, "==============onFoodCompleteFailure  " + message)
        mProposalsContract.onFoodCompleteFailureResult(message!!)
    }

    override fun foodComplete(mActivity: Activity, proposalId: String, foodStatus: String) {
        mProposalsInteractor.foodCompleteApi(mActivity, proposalId, foodStatus)
    }

    override fun foodListUnList(mActivity: Activity, postId: String) {
        mProposalsInteractor.foodListUnList(mActivity, postId)
    }

    override fun onFoodListUnListFailure(message: String?) {
        super.onFoodListUnListFailure(message)
        mProposalsContract.onFoodListUnListFailureResult(message!!)
    }

    override fun onFoodListUnListSuccess(message: String?) {
        super.onFoodListUnListSuccess(message)
        mProposalsContract.onFoodListUnListSuccessResult(message!!)
    }
}
