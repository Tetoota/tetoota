package com.tetoota.proposal

import android.app.Activity
import android.util.Log
import com.tetoota.proposal.CheckProposalsDataResponce.CheckProposalsDataResponse
import com.tetoota.fragment.inbox.ProposalMessageData

/**
 * Created by jitendra.nandiya on 08-08-2017.
 */
class ProposalsContract {
    interface View {
        fun onFoodCompleteSuccessResult(message: String?)
        fun onFoodCompleteFailureResult(message: String) {}

        fun onFoodListUnListSuccessResult(message: String?)
        fun onFoodListUnListFailureResult(message: String) {}
        fun onProposalsApiSuccessResult(message: String?)
        //        fun onProposalsExchangeApiSuccessResult(message: String?)
        fun onProposalsByIdSuccessResult(message: String?, proposalByIdData: ProposalByIdData?) {}

        fun onProposalActionSuccessREsult(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {}
        fun onProposalActionCancelSuccessREsult(message: String?, propoalId: String, acceptanceType: String, pos: Int) {}

        fun onProposalsFailureResult(message: String) {}
        fun onProposalsCancelFailureResult(message: String) {}
        //        fun onProposalsExchangeFailureResult(message: String) {}
        fun onAddReviewApISuccessResult(message: String?) {}

        fun onAddReviewApiFailureResult(message: String?) {}
        fun onUserProposalsDataSuccess(checkProposalsDataResponse: CheckProposalsDataResponse?) {}
        fun onUserProposalsDataFailure(message: String?) {}
    }

    internal interface Presenter {
        fun getProposalsData(mActivity: Activity, postId: String, proposalTo: String, proposalFrom: String, tetootaPoints: String,
                             exchangePostId: String, exchangePostType: String, proposalTime: String, type: String)

        fun proposalAction(mActivity: Activity, propoalId: ProposalMessageData, acceptanceType: String, pos: Int, userId: String, chatType: String) {}

        fun proposalActionCancel(mActivity: Activity, propoalId: String, acceptanceType: String, pos: Int, userId: String, chatType: String) {}

        //        fun proposalCompleteAction(mActivity : Activity,propoalId : String, acceptanceType : String,pos : Int){}
        fun proposalCompleteAction(mActivity: Activity, propoalId: String, acceptanceType: String, receiver_id: String, referral_user_status: String, user_id: String) {}

        //        fun proposalExchangeCompleteAction(mActivity : Activity,propoalId : String, acceptanceType : String,receiver_id : String)
        fun getProposalById(proposalId: String, mActivity: Activity) {}

        fun addReviewApi(mActivity: Activity, mUserId: String,
                         mPostId: String, response_time: String, quality_services: String,
                         friendliness: String, review_message: String) {
        }

        fun checkProposal(mActivity: Activity, userId: String)
        fun foodComplete(mActivity: Activity, proposalId: String, foodStatus: String)
        fun foodListUnList(mActivity: Activity, postId: String)
    }

    interface ProposalsApiListener {
        fun onProposalsAPiSuccess(message: String?)
        fun onProposalByIdSuccess(message: String?, proposalByIdData: ProposalByIdData?) {}
        fun onProposalActionSuccess(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {}
        fun onProposalCancelSuccess(message: String?, proposal_id: String, acceptanceType: String, pos: Int) {}
        fun onProposalCancelFailure(message: String?) {}
        fun onProposalsApiFailure(message: String)
        fun onAddReviewApISuccess(message: String?) {}
        fun onAddReviewApiFailure(message: String?) {}
        fun onCheckProposalsDataApiSuccess(checkProposalsDataResponseData: CheckProposalsDataResponse?) {}
        fun onCheckProposalsDataApiFailure(message: String?) {}
        //        fun onProposalExchangeCompleteApiSuccess(message: String?)
//        fun onProposalsExchangeApiFailure(message: String)
        fun onFoodCompleteSuccess(message: String?) {}

        fun onFoodCompleteFailure(message: String?) {
        }

        fun onFoodListUnListSuccess(message: String?) {}

        fun onFoodListUnListFailure(message: String?) {
        }
    }
}
