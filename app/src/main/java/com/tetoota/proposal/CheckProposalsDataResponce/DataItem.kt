package com.tetoota.proposal.CheckProposalsDataResponce

import com.google.gson.annotations.SerializedName

data class DataItem(

        @field:SerializedName("proposal_status")
        val proposalStatus: String? = null,

        @field:SerializedName("proposal_id")
        val proposalId: Int? = null,

        @field:SerializedName("latitude")
        val latitude: Double? = null,

        @field:SerializedName("friendliness")
        val friendliness: Any? = null,

        @field:SerializedName("review_message")
        val reviewMessage: Any? = null,

        @field:SerializedName("description")
        val description: String? = null,

        @field:SerializedName("availability")
        val availability: String? = null,

        @field:SerializedName("trading_status")
        val tradingStatus: String? = null,

        @field:SerializedName("title")
        val title: String? = null,

        @field:SerializedName("proposal_to_user_profile_img")
        val proposalToUserProfileImg: String? = null,

        @field:SerializedName("proposal_from_user_profile_img")
        val proposalFromUserProfileImg: String? = null,

        @field:SerializedName("average_badges")
        val averageBadges: Int? = null,

        @field:SerializedName("proposal_to_user_lastName")
        val proposalToUserLastName: String? = null,

        @field:SerializedName("quality_services")
        val qualityServices: Any? = null,

        @field:SerializedName("exchange_post_record")
        val exchangePostRecord: List<Any?>? = null,

        @field:SerializedName("post_type")
        val postType: String? = null,

        @field:SerializedName("proposal_duration")
        val proposalDuration: String? = null,

        @field:SerializedName("longitude")
        val longitude: Double? = null,

        @field:SerializedName("post_image")
        val postImage: String? = null,

        @field:SerializedName("prop_tetoota_points")
        val propTetootaPoints: Int? = null,

        @field:SerializedName("proposal_from_user_lastName")
        val proposalFromUserLastName: String? = null,

        @field:SerializedName("proposal_to_user_firstName")
        val proposalToUserFirstName: String? = null,

        @field:SerializedName("proposal_sender_id")
        val proposalSenderId: Int? = null,

        @field:SerializedName("trading_preference")
        val tradingPreference: Any? = null,

        @field:SerializedName("creation_date")
        val creationDate: String? = null,

        @field:SerializedName("hours_left")
        val hoursLeft: Int? = null,

        @field:SerializedName("set_quote")
        val setQuote: String? = null,

        @field:SerializedName("proposal_create_datetime")
        val proposalCreateDatetime: String? = null,

        @field:SerializedName("qualification")
        val qualification: String? = null,

        @field:SerializedName("post_id")
        val postId: Int? = null,

        @field:SerializedName("proposal_from_user_firstName")
        val proposalFromUserFirstName: String? = null,

        @field:SerializedName("user_id")
        val userId: Int? = null,

        @field:SerializedName("share_service_platform")
        val shareServicePlatform: String? = null,

        @field:SerializedName("response_time")
        val responseTime: Any? = null,

        @field:SerializedName("set_tetoota_points")
        val setTetootaPoints: Int? = null,

        @field:SerializedName("categories_ids")
        val categoriesIds: Int? = null,

        @field:SerializedName("virtual_service")
        val virtualService: String? = null,

        @field:SerializedName("proposal_time")
        val proposalTime: String? = null,

        @field:SerializedName("receiver_trading_status")
        val receiver_trading_status: String? = null
)