package com.tetoota.marketplace.data

import com.google.gson.annotations.SerializedName

class MetaData {
    @SerializedName("code")
    public var mCode: Int? = null
    @SerializedName("message")
    public var mMessage: String? = null
    @SerializedName("status")
    public var mStatus: Boolean? = null
}