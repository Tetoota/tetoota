package com.tetoota.marketplace.data

import com.google.gson.annotations.SerializedName
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.network.errorModel.Meta

data class RedeemData(
        val data: String = "",
        val meta: Meta? = null) {


//    @SerializedName("data")
//    public var mData = ""
//    @SerializedName("meta")
//    public var mMeta: MetaData? = null
}