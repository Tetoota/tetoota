package com.tetoota.marketplace

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.marketplace.data.RedeemData
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MarketPlaceDetailInteractor {

    //----------------------------------Define-Public-Variable-----------------------------------------//
    var mReadOnlyApiListener: MarketPlaceContract.ReadProfileApiListener

    //----------------------------------Define-constructor----------------------------------//
    constructor(mReadOnlyApiListener: MarketPlaceContract.ReadProfileApiListener) {
        this.mReadOnlyApiListener = mReadOnlyApiListener
    }

    //----------------------------------Define-ReadOnlyDataApiCall----------------------------------//
    fun ReadOnlyDataApiCall(mActivity: Activity, pagination: Int?, personData: LoginDataResponse) {
        //Log.e("HelloAkash",""+ personData)
        var call: Call<MatketPlaceResponse> = TetootaApplication.getHeader()
                .UserReadOnlyDetailApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        personData.auth_token!!,
                        pagination,
                        personData.user_id!!)
        call.enqueue(object : Callback<MatketPlaceResponse> {
            override fun onResponse(call: Call<MatketPlaceResponse>?,
                                    response: Response<MatketPlaceResponse>?) {
                //println("Success")
                var mProfileData: MatketPlaceResponse? = response?.body()
                //Log.e("mReadOnlyDataaaaaa",""+ mProfileData)
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        Utils.savePreferences(Constant.IS_VISIBLE,
                                mProfileData?.data?.get(0)?.is_service.toString(), mActivity)
                        mReadOnlyApiListener.onReadOnlyApISuccess(mProfileData?.data, "userProfileData")
                    } else {
                        mReadOnlyApiListener.onReadOnlyApIiFailure(response.body()?.meta?.message.toString(), "profileFailure")
                    }
                } else {
                    mReadOnlyApiListener.onReadOnlyApIiFailure(response?.body()?.meta?.message.toString(), "profileFailure")
                }
            }

            override fun onFailure(call: Call<MatketPlaceResponse>?, t: Throwable?) {
                mReadOnlyApiListener.onReadOnlyApIiFailure(t?.message.toString(), "profileFailure")
            }
        })
    }

    //--------------------------------------------Define-RedeemDetail----------------------------------//
    fun RedeemDetail(mActivity: Activity, mUserId: String, post_id: String) {
        // Log.e("Akash",""+ mUserId)
        var call: Call<RedeemData> = TetootaApplication.getHeader()
                .OffersRedeemApi(Constant.CONSTANT_ADMIN,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId, post_id)
        call.enqueue(object : Callback<RedeemData> {
            override fun onResponse(call: Call<RedeemData>?,
                                    response: Response<RedeemData>?) {
                var meta = response?.body()?.meta
                var message = ""
                var s = response?.errorBody()?.string()
                if (response?.code() == 200) {
                    Log.i(javaClass.name, "=================" + meta?.message)
                    mReadOnlyApiListener.OffersRedeemDetailApiSuccessResult(meta?.message!!, response!!.body()!!.data)
                } else {
                    var jobj = JSONObject(s)
                    var metaJobj = jobj.getJSONObject("meta")
                    if (metaJobj.has("message"))
                        message = metaJobj.getString("message")
                    mReadOnlyApiListener.OffersRedeemDetailApiFailureResult(message)
                }
            }

            override fun onFailure(call: Call<RedeemData>?, t: Throwable?) {
                mReadOnlyApiListener.OffersRedeemDetailApiFailureResult("failure")
            }
        })
    }
//-----------------------------------------------------------End----------------------------------//

}