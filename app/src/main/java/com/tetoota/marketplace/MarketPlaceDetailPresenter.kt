package com.tetoota.marketplace

import android.app.Activity

import com.tetoota.login.LoginDataResponse

class MarketPlaceDetailPresenter : MarketPlaceContract.ReadProfileApiListener {
//---------------------Define-OffersRedeemDetailApiSuccessResult----------------------------------//
    override fun OffersRedeemDetailApiSuccessResult(message: String, code : String) {
        mReadDataContract.onOffersRedeemDetailApiSuccessResult(message,code)
    }
    override fun OffersRedeemDetailApiFailureResult(message: String) {
        mReadDataContract.onOffersRedeemDetailApiFailureResult(message)
    }
//------------------------------------------Define-Redeemfunction----------------------------------//
    fun Redeemfunction(mActivity: Activity, mUserId: String, post_id:String ) {
        mDataProfileInteractor.RedeemDetail(mActivity, mUserId,post_id)
    }
//------------------------------------Define-onReadOnlyApISuccess----------------------------------//
    override fun onReadOnlyApISuccess(profileData: List<MarketPlaceDataResponse?>?, message: String) {
        mReadDataContract.onReadDataApISuccess(profileData, message)
    }
//------------------------------------------Define-onReadOnlyApIiFailure--------------------------//
    override fun onReadOnlyApIiFailure(message: String, apiCallMethod: String) {
        mReadDataContract.onReadDataApiFailureResult(message, apiCallMethod)
    }
//-------------------------------------------------------------------------------------------------//
    var mReadDataContract: MarketPlaceContract.View
    private val mDataProfileInteractor: MarketPlaceDetailInteractor by lazy {
        com.tetoota.marketplace.MarketPlaceDetailInteractor(this)
    }
//------------------------------------------constructor--------------------------------------------//
    constructor(mProfileContract: MarketPlaceContract.View) {
        this.mReadDataContract = mProfileContract
    }
//------------------------------------getDataReadOnly----------------------------------------------//
     fun getDataReadOnly(mActivity: Activity,pagination: Int?, personData: LoginDataResponse) {
        if (mActivity != null) {
            mDataProfileInteractor.ReadOnlyDataApiCall(mActivity,pagination,personData)
        }
    }
//---------------------------------------End-------------------------------------------------------//
}

