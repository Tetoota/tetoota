package com.tetoota.marketplace

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.AbsListView
import android.widget.ImageView
import com.google.gson.Gson
import com.tetoota.R
import com.tetoota.cropImage.RotateBitmap.TAG
import com.tetoota.customviews.PaginationScrollListener
import com.tetoota.fragment.BaseFragment
import com.tetoota.listener.IFragmentOpenCloseListener
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_market_place.*
import kotlinx.android.synthetic.main.activity_market_place.loadItemsLayout_recyclerView
import kotlinx.android.synthetic.main.activity_market_place.progress_view
import kotlinx.android.synthetic.main.error_layout.*
import org.jetbrains.anko.toast

/**
 * Created by jitendra.nandiya on 14-08-2017.
 */
class MarketPlaceFragment : BaseFragment(),
        View.OnClickListener,
        MarketPlaceContract.View,
        MarketPlaceAdapter.IAdapterClick,
        ViewPager.OnPageChangeListener,
        MarketPlaceAdapter.PaginationAdapterCallback {
    //-------------------------------Define-Public-Variable----------------------------------------------//
    private var dotsCount: Int = 0
    var dots = arrayOfNulls<ImageView>(dotsCount)
    private var isLoad = false
    public var currentPage = 1
    public var isListLastPage = false
    private var TOTAL_PAGES = 100
    private var adapter: MarketPlaceAdapter? = null
    var visibleItemCount: Int = 0
    var pastVisiblesItems: Int = 0
    var totalItemCount: Int = 0

    private lateinit var linearLayoutManager: LinearLayoutManager
    //-------------------------------Define-retryPageLoad-method------------------------------------------//
    override fun retryPageLoad() {
    }

    //----------------------------------------------------------------------------------------------------//
    override fun onPageScrollStateChanged(p0: Int) {
    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
    }

    //-------------------------------Define-onPageSelected-method------------------------------------------//
    override fun onPageSelected(p0: Int) {
        for (i in 0 until dotsCount) {
            try {
                dots[i]?.setImageDrawable(context?.let { ContextCompat.getDrawable(it, R.drawable.select_walkthorugh_item) })
                dots[p0]?.setImageDrawable(context?.let { ContextCompat.getDrawable(it, R.drawable.select_walkthorugh_item) })

                if (p0 + 1 == dotsCount) {
                } else {
                    // btn_next.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    //------------------Define-onOffersRedeemDetailApiSuccessResult-method-----------------------------//
    override fun onOffersRedeemDetailApiSuccessResult(message: String, code: String) {
    }

    override fun onOffersRedeemDetailApiFailureResult(message: String) {
    }

    //-----------------------------------------Define-cellItemClick-method-----------------------------//
    override fun cellItemClick(mViewClickType: String, data: MarketPlaceDataResponse) {
        val intent = Intent(context, MarketDetailShowActivity::class.java)
        intent.putExtra("data", data)
        //Log.i(javaClass.name, "=============MarketPlaceFragment"+data.post_id)
        startActivityForResult(intent, 1010)
    }

    //----------------------------------Define-onReadDataApISuccess-method-----------------------------//
    override fun onReadDataApISuccess(marketList: List<MarketPlaceDataResponse?>?, mesg: String) {
        hideProgressDialog()
//        TOTAL_PAGES += 1
        progress_view.visibility = View.GONE
        rl_nrf.visibility = View.GONE
        hideErrorView()
        loadItemsLayout_recyclerView.visibility = View.GONE
        if (activity != null && MarketPlaceFragment() != null) {
            if (marketList != null && marketList.size > 0) {
                rl_nrf.visibility = View.GONE
                adapter?.addAll(marketList as List<MarketPlaceDataResponse?>)
                adapter?.notifyDataSetChanged()

                Log.e("TAG","adapterAB "+adapter?.itemCount.toString())
            } else {
                rl_nrf.visibility = View.VISIBLE
            }
        } else {
            rl_nrf.visibility = View.VISIBLE
        }
//                if (activity != null && MarketPlaceFragment() != null) {
//                    if (message != null) {
//                        if (message.size > 0) {
//                            //Log.e("TAG", "SIZE() = " + message.size)
//                            var sdfsd: MarketPlaceDataResponse = message.get(0) as MarketPlaceDataResponse
//                        } else {
//                        }
//                    }
//                    adapter?.addAll(message as List<MarketPlaceDataResponse?>)
//                    adapter?.notifyDataSetChanged()
//                }
//                isLoad = false
//            } else {
//                loadItemsLayout_recyclerView.visibility = View.GONE
//                isListLastPage = true
//                progress_view.visibility = View.GONE
//                rl_nrf.visibility = View.VISIBLE
//                hideErrorView()
//            }
        Log.i(javaClass.name, "===========onScrolled========" + marketList!!.size)

        isLoad = marketList!!.size >= 10
    }

    //-----------------------------------------Define-hideErrorView-method-----------------------------//
    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            marketplace_list.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    //-----------------------------Define-onReadDataApiFailureResult-method-----------------------------//
    override fun onReadDataApiFailureResult(message: String, apiCallMethod: String) {
    }

    override fun onClick(v: View?) {
    }

    //----------------------------------------------Define-onCreate-method-----------------------------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    //------------------------------------------Define-onCreateView-method-----------------------------//
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.activity_market_place, container, false)
        return view
    }

    //-------------------------------------Define-onActivityCreated-method-----------------------------//
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    //-------------------------------------Define-onActivityCreated-method-----------------------------//
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getLastLocation(true)
        setupRecyclerView()
//        OnItemClickRecyclerView()
        marketplace_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                visibleItemCount = linearLayoutManager?.getChildCount()!!
                totalItemCount = linearLayoutManager?.getItemCount()!!
                pastVisiblesItems = linearLayoutManager?.findFirstVisibleItemPosition()!!
                var lastVisiblesItems = linearLayoutManager?.findLastVisibleItemPosition()!!
                if (lastVisiblesItems == totalItemCount - 1 && visibleItemCount + pastVisiblesItems >= totalItemCount && isLoad) {
                    currentPage += 1
                    Log.e(javaClass.name, "currentPage" + currentPage)
                    loadItemsLayout_recyclerView.visibility = View.VISIBLE
                    getLastLocation(true)
                    isLoad = false
                }
            }
        })
//        marketplace_list.addOnScrollListener(object : RecyclerView.OnScrollListener) {
//            override fun isLastPage(): Boolean {
//                return isListLastPage
//            }
//
//            override fun loadMoreItems() {
//                Log.i(javaClass.name,"========loadMoreItems==========="+isLoad)
//                isLoad = true
//                currentPage += 1
//                //Log.e(TAG, "currentPage" + currentPage)
//                loadItemsLayout_recyclerView.visibility = View.VISIBLE
//                getLastLocation(true)
//            }
//
//            override fun getTotalPageCount(): Int {
//                return TOTAL_PAGES
//            }
//
//            override fun isLoading(): Boolean {
//                return isLoad
//            }
//        })
    }

    //-------------------------------------Define-setupRecyclerView-method-----------------------------//
    private fun setupRecyclerView() {
        linearLayoutManager = LinearLayoutManager(context!!)
        marketplace_list.setHasFixedSize(true)
        marketplace_list.layoutManager = linearLayoutManager
        adapter = MarketPlaceAdapter(context!!, iAdapterClickListener = this, iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        marketplace_list.setLayoutManager(linearLayoutManager);
        marketplace_list.adapter = adapter
        //print("======================" + mDataList)
    }

    //-------------------------------------Define-fetchServiceData-method-----------------------------//
    private fun fetchServiceData(pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (isLoadMore) {
//                progress_view.visibility = View.VISIBLE
            }
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mReadDataDetailPresenter.getDataReadOnly(activity!!, pagination, personData)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
        }
    }

    //---------------------------------------Define-getLastLocation-method-----------------------------//
    @SuppressLint("MissingPermission")
    fun getLastLocation(isLoadMore: Boolean) {
        fetchServiceData(currentPage, isLoadMore)
    }

    //-------------------------------Define-OnItemClickRecyclerView-method-----------------------------//
    private fun OnItemClickRecyclerView() {
        linearLayoutManager = LinearLayoutManager(context!!)
        marketplace_list.setHasFixedSize(true)
        marketplace_list.layoutManager = linearLayoutManager
        adapter = MarketPlaceAdapter(context!!, iAdapterClickListener = this
                , iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        marketplace_list.setLayoutManager(linearLayoutManager);
        marketplace_list.adapter = adapter
    }

    //----------------------------------------------Define-onAttach-method-----------------------------//
    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    //------------------------------------Define-heightCalculation-method-----------------------------//
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .2f).toInt()
        return getTopMarginH
    }

    //-------------------------------Define-OnItemClickRecyclerView-method-----------------------------//
    override fun onDetach() {
        super.onDetach()
    }

    //------------------------------------------------Define-object-method-----------------------------//
    companion object {
        var iFragmentOpenCloseListener: IFragmentOpenCloseListener? = null
        //-----------------------------------------Define-newMainIntent-method-----------------------------//
        fun newMainIntent(context: Context, iFragmentOpenCloseListenerLocal: IFragmentOpenCloseListener): MarketPlaceFragment {
            //            val intent = Intent(context, EarnPointsFragment::class.java).apply {
            //                iFragmentOpenCloseListener = iFragmentOpenCloseListenerLocal
            //                flags = Intent.FLAG_ACTIVITY_NEW_TASK
            //                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            //            }
            //            return intent
            iFragmentOpenCloseListener = iFragmentOpenCloseListenerLocal
            val fragment = MarketPlaceFragment()
            return fragment
        }
    }

    //----------------------------------------------Define-onResume-method-----------------------------//
    override fun onResume() {
        super.onResume()
    }

    //-------------------------------Define-OnItemClickRecyclerView-method-----------------------------//
    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out)
        fragmentTransaction.func()
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commitAllowingStateLoss()
    }

    //-------------------------------Define-onCreateOptionsMenu-method---------------------------------//
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //-------------------------------Define-onPrepareOptionsMenu-method--------------------------------//
    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setVisible(false);
        super.onPrepareOptionsMenu(menu)
    }

    //-----------------------------------Define-onFragmentOpenClose-method-----------------------------//
    fun onFragmentOpenClose(fromFragment: String, toFragment: String, tag: String) {
        iFragmentOpenCloseListener!!.onFragmentOpenClose("earnTetootaPoints", "invite_friends", "")
    }

    //-------------------------------Define-OnItemClickRecyclerView-method-------------------------//
    private fun Intent.putExtra(s: String, mProductData: MarketPlaceDataResponse) {

    }

    //-------------------------------Define-OnItemClickRecyclerView-method-----------------------------//
    private val mReadDataDetailPresenter: MarketPlaceDetailPresenter by lazy {
        com.tetoota.marketplace.MarketPlaceDetailPresenter(this@MarketPlaceFragment)
    }
//-----------------------------------------------------------------End-----------------------------//

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i(javaClass.name, "==========================" + requestCode)
        currentPage = 1
        getLastLocation(true)
    }
}


