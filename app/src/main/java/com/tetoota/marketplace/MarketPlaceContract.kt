package com.tetoota.marketplace

import android.app.Activity

class MarketPlaceContract {
    interface View {
        fun onReadDataApISuccess(message: List<MarketPlaceDataResponse?>?, mesg: String)
        fun onReadDataApiFailureResult(message: String, apiCallMethod: String)
        fun onOffersRedeemDetailApiSuccessResult(message: String, code: String)
        fun onOffersRedeemDetailApiFailureResult(message: String)
    }

    internal interface Presenter {
    }

    interface ReadProfileApiListener {
        fun onReadOnlyApISuccess(profileData: List<MarketPlaceDataResponse?>?, message: String)
        fun onReadOnlyApIiFailure(message: String, apiCallMethod: String)
        fun OffersRedeemDetailApiSuccessResult(message: String, code: String)
        fun OffersRedeemDetailApiFailureResult(message: String)

    }


}