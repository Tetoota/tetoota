package com.tetoota.marketplace

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.tetoota.R
import kotlinx.android.synthetic.main.activity_marketplacelistviewitem.view.*
import java.util.ArrayList
import com.tetoota.TetootaApplication
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

class MarketPlaceAdapter(
        val mContext: Context,
        var iAdapterClickListener: IAdapterClick,
        var iPaginationAdapterCallback: PaginationAdapterCallback,
        var screenHeight: Int,
        var mTrendingList: MutableList<Any?> = ArrayList<Any?>()) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    //------------------------------onBindViewHolder-------------------------------------------------------//
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, p1: Int) {
        if (viewHolder is ViewHolder) {
            val mServiceList = mTrendingList as List<MarketPlaceDataResponse>
            viewHolder.bindServiceData(viewHolder as ViewHolder?, p1, mServiceList[p1], screenHeight, iAdapterClickListener, mContext)
        } else if (viewHolder is ViewHolder) {
            viewHolder.bindTrendingData(mTrendingList[p1] as MarketPlaceDataResponse)
        }
    }

    //------------------------------onCreateViewHolder--------------------------------------------------//
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MarketPlaceAdapter.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.activity_marketplacelistviewitem, viewGroup, false)
        return ViewHolder(view)
    }

    //--------------------------------------addAll------------------------------------------------------//
    fun addAll(moveResults: List<MarketPlaceDataResponse?>) {
        for (result in moveResults) {
            add(result)
        }
    }

    //----------------------------------getItemCount--------------------------------------------------//
    override fun getItemCount(): Int {
        return mTrendingList.size
    }

    //-----------------------------------------------add----------------------------------------------//
    private fun add(result: MarketPlaceDataResponse?) {
        mTrendingList.add(result)
        notifyItemInserted(mTrendingList.size - 1)
    }

    //-----------------------------------------ViewHolder----------------------------------------------//
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mImageView = view.iv_readingimage!!
        val tvCouponsLeft = view.tvCouponsLeft!!
        val mtitle = view.tvtitle!!
        val mtv_description = view.tvUsername!!
        val mtv_tetoota_pointsn = view.tvpoint!!
        val llMainMarket = view.llMainMarket!!
        private var tetootaApplication: TetootaApplication? = null
        //---------------------------bindServiceData-------------------------------------------------------//
        fun bindServiceData(viewHolder: ViewHolder?, p1: Int, mServiceData: MarketPlaceDataResponse,
                            height: Int, iAdapterClickListener: MarketPlaceAdapter.IAdapterClick, context: Context) {
            tetootaApplication = context.applicationContext as TetootaApplication
            mtitle.text = mServiceData.title
            mtv_description.text = mServiceData.description
            mtv_tetoota_pointsn.text = mServiceData.tetoota_points.toString()
            if (mServiceData.photo!!.isEmpty()) {
                mImageView.setImageResource(R.drawable.queuelist_place_holder);
            } else {
                Picasso.get().load(mServiceData.photo).into(mImageView)
            }
            if (mServiceData.photo!!.isEmpty()) {
                mImageView.setImageResource(R.drawable.user_placeholder);
            } else {
                Picasso.get().load(mServiceData.photo).into(mImageView)
            }
            llMainMarket.onClick {
                iAdapterClickListener.cellItemClick("service", mServiceData)
            }
            if (mServiceData.photo!!.isEmpty()) {
                context.toast("Profile image is not available")
            }
            if (mServiceData.couponsLeft == 0) {
                tvCouponsLeft.visibility = View.VISIBLE
                tvCouponsLeft.setText(Utils.getText(context, StringConstant.str_redeem_code))
                llMainMarket.setCardBackgroundColor(context.resources.getColor(R.color.color_grey_color))

            } else {
                llMainMarket.setCardBackgroundColor(context.resources.getColor(R.color.color_white))

                tvCouponsLeft.visibility = View.GONE
            }

        }

        //------------------------------bindTrendingData---------------------------------------------------//
        fun bindTrendingData(mTrendingDataResponse: MarketPlaceDataResponse) {
            mtitle.text = mTrendingDataResponse.title
            mtv_description.text = mTrendingDataResponse.description
            mtv_tetoota_pointsn.text = mTrendingDataResponse.tetoota_points.toString()
            if (mTrendingDataResponse.photo!!.isEmpty()) {
                mImageView.setImageResource(R.drawable.queuelist_place_holder)
            } else {
                Picasso.get().load(mTrendingDataResponse.photo).into(mImageView)
            }
            if (mTrendingDataResponse.photo!!.isEmpty()) {
            } else {
            }
        }
    }

    //------------------------------IAdapterClick------------------------------------------------------//
    interface IAdapterClick {
        fun cellItemClick(mViewClickType: String, poition: MarketPlaceDataResponse): Unit
    }

    //------------------------------PaginationAdapterCallback------------------------------------------//
    interface PaginationAdapterCallback {
        fun retryPageLoad()
    }

    //------------------------------getTotalRecord----------------------------------------------------//
    fun getTotalRecord(): Int? {
        if (mTrendingList.size > 0) {
            return (mTrendingList as List<MarketPlaceDataResponse>)[0].count
        } else {
            return -1
        }
    }
//----------------------------------------------End------------------------------------------------//
}