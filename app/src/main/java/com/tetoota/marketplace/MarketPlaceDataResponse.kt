package com.tetoota.marketplace

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.tetoota.fragment.profile.SyncWithPhone
import com.tetoota.potluck.PotluckFoodDataResponse

class MarketPlaceDataResponse : Parcelable {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = ""
    @SerializedName("description")
    @Expose
    var description: String? = ""
    @SerializedName("user_id")
    @Expose
    var user_id: String? = ""
    @SerializedName("tetoota_points")
    @Expose
    var tetoota_points: Int? = null
    @SerializedName("photo")
    @Expose
    var photo: String? = ""
    @SerializedName("status")
    @Expose
    var status: String? = ""
    @SerializedName("members")
    @Expose
    var members: Int? = null
    @SerializedName("date")
    @Expose
    var date: String? = ""
    @SerializedName("is_service")
    @Expose
    var is_service: String? = ""
    @SerializedName("content")
    @Expose
    var content: String? = ""
    @SerializedName("post_type")
    @Expose
    var post_type: String? = ""
    @SerializedName("location")
    @Expose
    var location: String? = ""
    @SerializedName("post_id")
    @Expose
    var post_id: String? = ""
    @SerializedName("service_provider")
    @Expose
    var service_provider: String? = ""
    @SerializedName("count")
    @Expose
    var count: Int? = null
    @SerializedName("coupons_left")
    @Expose
    var couponsLeft: Int? = null
    @SerializedName("redeem_code")
    @Expose
    var redeem_code: String? = ""
//    @SerializedName("coupons_left")
//    @Expose
//    var coupons_left: String? = ""
//
    protected constructor(`in`: Parcel) {
        this.id = `in`.readValue(Int::class.java.classLoader) as Int
        this.title = `in`.readValue(String::class.java.classLoader) as String
        this.description = `in`.readValue(String::class.java.classLoader) as String
        this.user_id = `in`.readValue(String::class.java.classLoader) as String
        this.tetoota_points = `in`.readValue(Int::class.java.classLoader) as Int
        this.photo = `in`.readValue(String::class.java.classLoader) as String
        this.status = `in`.readValue(String::class.java.classLoader) as String
        this.members = `in`.readValue(Int::class.java.classLoader) as Int
        this.date = `in`.readValue(String::class.java.classLoader) as String
        this.is_service = `in`.readValue(String::class.java.classLoader) as String
        this.content = `in`.readValue(String::class.java.classLoader) as String
        this.post_type = `in`.readValue(String::class.java.classLoader) as String
        this.location = `in`.readValue(String::class.java.classLoader) as String
        this.post_id = `in`.readValue(String::class.java.classLoader) as String
        this.service_provider = `in`.readValue(String::class.java.classLoader) as String
        this.count = `in`.readValue(Int::class.java.classLoader) as Int
        this.couponsLeft = `in`.readValue(Int::class.java.classLoader) as Int
        this.redeem_code = `in`.readValue(String::class.java.classLoader) as String
    }

    constructor() {}

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeValue(id)
        dest.writeValue(title)
        dest.writeValue(description)
        dest.writeValue(user_id)
        dest.writeValue(tetoota_points)
        dest.writeValue(photo)
        dest.writeValue(status)
        dest.writeValue(members)
        dest.writeValue(date)
        dest.writeValue(is_service)
        dest.writeValue(content)
        dest.writeValue(post_type)
        dest.writeValue(location)
        dest.writeValue(post_id)
        dest.writeValue(service_provider)
        dest.writeValue(count)
        dest.writeValue(couponsLeft)
        dest.writeValue(redeem_code)
    }

    override fun describeContents(): Int {
        return 0
    }


    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MarketPlaceDataResponse> = object : Parcelable.Creator<MarketPlaceDataResponse> {
            override fun createFromParcel(source: Parcel): MarketPlaceDataResponse = MarketPlaceDataResponse(source)
            override fun newArray(size: Int): Array<MarketPlaceDataResponse?> = arrayOfNulls(size)
        }
    }


}
