package com.tetoota.marketplace

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.squareup.picasso.Picasso
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_marketplace_details.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import android.widget.Toast
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.customviews.CustomServiceAddAlert
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import kotlinx.android.synthetic.main.activity_marketplace_dialog.*
import kotlinx.android.synthetic.main.activity_select_categories.*
import kotlinx.android.synthetic.main.error_layout.*
import org.jetbrains.anko.enabled
import org.jetbrains.anko.onClick

class MarketDetailShowActivity : BaseActivity(),
        MarketPlaceContract.View, CustomServiceAddAlert.IDialogListener, ProfileCompletionDialog.IDialogListener, ServiceContract.View {
    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this@MarketDetailShowActivity)) {
                val intent = ProfileDetailActivity.newMainIntent(this@MarketDetailShowActivity)
                ActivityStack.getInstance(this@MarketDetailShowActivity)
                startActivity(intent)
            } else {
                // TODO CHECK NET
                showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    //------------------------------------Define-Public-Variable---------------------------------------//
    lateinit var personData: LoginDataResponse
    lateinit var mMarketDetailShowDataResponse: MarketPlaceDataResponse
    var mPostID: String? = null
    var dialog: Dialog? = null

    private var serviceCount: Int = 0
    private var mList: List<ServicesDataResponse>? = null
    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@MarketDetailShowActivity)
    }

    //-------------------------------------------------------------------------------------------------//
    override fun onApiFailureResult(message: String, isServerError: Boolean) {
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
    }

    override fun onReadDataApISuccess(message: List<MarketPlaceDataResponse?>?, mesg: String) {
    }

    override fun onReadDataApiFailureResult(message: String, apiCallMethod: String) {
    }

    override fun onYes(param: String, message: String) {
        val intent = AddPostRequestActivity.newMainIntent(this)
        intent!!.putExtra("Tab", "serviceTab")
        ActivityStack.getInstance(this!!)
        startActivity(intent)
        finish()
    }

    override fun onWishListApiSuccessResult(myServiceList: List<Any?>?, message: String) {
        Log.e("myServiceList", myServiceList?.size.toString())
        mList = myServiceList as List<ServicesDataResponse>

        if (myServiceList != null) {
            Log.e("sizeeeeeeeee ", "" + myServiceList.size)
            serviceCount = myServiceList!!.size

        } else {
            serviceCount = 0
        }
    }

    override fun onOffersRedeemDetailApiSuccessResult(message: String, code: String) {
        hideProgressDialog()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        btn_conversation.visibility = View.GONE
        llDiscountCode.visibility = View.VISIBLE
        tvRedeemCode.text = code
    }

    override fun onOffersRedeemDetailApiFailureResult(message: String) {
        hideProgressDialog()
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    //---------------------------------onCreate--------------------------------------------------------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_marketplace_details)

    }

    //----------------------------------onResume--------------------------------------------------------//
    override fun onResume() {
        super.onResume()
        initViews()
        getDataFromPreviousActivity()
    }

    //----------------------------------getDataFromPreviousActivity------------------------------------//
    @SuppressLint("LongLogTag")
    private fun getDataFromPreviousActivity() {
        mMarketDetailShowDataResponse = getIntent().getParcelableExtra("data")
        mPostID = mMarketDetailShowDataResponse.id.toString()
        TextView_tvtitle.text = mMarketDetailShowDataResponse.title
        tv_product_category.text = mMarketDetailShowDataResponse.description
        TextView_Location.text = mMarketDetailShowDataResponse.location
        TextView_ServiceProvider.text = mMarketDetailShowDataResponse.service_provider
        TextView_ImgTetootaPoint.text = mMarketDetailShowDataResponse.tetoota_points.toString()
        TextView_Title.text = Utils.getText(this, StringConstant.title)
        TextView_description.text = Utils.getText(this, StringConstant.str_market_description)
        TextView_serviceProvider.text = Utils.getText(this, StringConstant.tv_service_provider)
        TextView_TetootaPoint.text = Utils.getText(this, StringConstant.str_market_points)
        TextView_locationtxt.text = Utils.getText(this, StringConstant.add_service_location_text)
        btn_conversation.text = Utils.getText(this, StringConstant.str_market_button)
        if (mMarketDetailShowDataResponse.couponsLeft == 0) {
            btn_conversation.setBackgroundColor(resources.getColor(R.color.color_login_text_color))
            btn_conversation.enabled=false
            btn_conversation.setText(Utils.getText(this, StringConstant.str_redeem_code))

        } else {
            btn_conversation.setBackgroundColor(resources.getColor(R.color.color_start_conversation_btn))
            btn_conversation.enabled=true
        }

        if (TextUtils.isEmpty(mMarketDetailShowDataResponse.redeem_code)) {
            btn_conversation.visibility = View.VISIBLE
            llDiscountCode.visibility = View.GONE
            btn_conversation.setOnClickListener {
                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MarketDetailShowActivity)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    if (personData.first_name == null && personData.first_name.equals("") || personData.last_name == null || personData.last_name.equals("")
                            && personData.profile_image == null || personData.profile_image.equals("")) {
                        ProfileCompletionDialog(this@MarketDetailShowActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                this@MarketDetailShowActivity, getString(R.string.err_msg_mobile_number_limit)).show()
                    } else if (serviceCount <= 0) {
                        CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                this, getString(R.string.err_msg_mobile_number_limit)).show()
                    } else {
                        showDialog(this@MarketDetailShowActivity)
                        if (dialog != null)
                            dialog!!.show()
                    }
            }
        } else {
            btn_conversation.visibility = View.GONE
            llDiscountCode.visibility = View.VISIBLE
            tvRedeemCode.text = mMarketDetailShowDataResponse.redeem_code
        }
        if (mMarketDetailShowDataResponse.photo != null && !mMarketDetailShowDataResponse.photo!!.isEmpty()) {
            Picasso.get().load(mMarketDetailShowDataResponse.photo!!).placeholder(R.drawable.place_holder).error(R.drawable.place_holder).into(pager_introduction)
        } else {
            pager_introduction.setImageResource(R.drawable.place_holder);
        }
        mServicePresenter.getServicesData(this, "ServicesProducts", 1,"")
    }

    //----------------------------------------------------initViews------------------------------------//
    private fun initViews() {
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        toolbar_title.text = Utils.getText(this, StringConstant.str_marketplace)
    }

    //----------------------------------------------------End------------------------------------//
    private fun showDialog(mContext: Context) {
        dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.window!!.decorView.setBackgroundColor(resources.getColor(R.color.color_tra))
        dialog!!.setContentView(R.layout.activity_marketplace_dialog)
        setMultiLanguageText(mContext, dialog!!)
        dialog!!.btn_yes.setOnClickListener(View.OnClickListener {
            dialog!!.dismiss()
            if (Utils.haveNetworkConnection(mContext!!)) {
                mredeemDataResponse.Redeemfunction((mContext as Activity?)!!, Utils.loadPrefrence(Constant.USER_ID, "", mContext), mPostID.toString())
            } else {
                showErrorView("${Utils.getText(mContext!!, StringConstant.str_check_internet)}")
            }
        })
//------------------------------btn_no.setOnClickListener------------------------------------------//
        dialog!!.btn_no.setOnClickListener(View.OnClickListener {
            dialog!!.dismiss()
        })
    }

    private fun setMultiLanguageText(mContext: Context, dialog: Dialog) {
        dialog.tv_name.text = Utils.getText(mContext, StringConstant.str_market_confirm)
        dialog.btn_no.text = Utils.getText(mContext, StringConstant.cancel)
        dialog.btn_yes.text = Utils.getText(mContext, StringConstant.tetoota_ok)
        dialog.tv_address.text = /*Utils.getText(context,StringConstant.share_referral)*/"YMBK87W"
    }

    private fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            rl_maincontent_dialog.visibility = View.GONE
            checkbox_layout.visibility = View.GONE
        }
    }

    private val mredeemDataResponse: MarketPlaceDetailPresenter by lazy {
        com.tetoota.marketplace.MarketPlaceDetailPresenter(this)
    }

}

