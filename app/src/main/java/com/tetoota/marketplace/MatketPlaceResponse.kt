package com.tetoota.marketplace

import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.network.errorModel.Meta

data class MatketPlaceResponse (
    val data: List<MarketPlaceDataResponse?>? = null,
    val meta: Meta? = null
)