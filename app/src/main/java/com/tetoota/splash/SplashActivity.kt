package com.tetoota.splash

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.customviews.fonts.AllPermissisonsDialog
import com.tetoota.listener.IDialogListener
import com.tetoota.main.MainActivity
import com.tetoota.selectlanguage.LanguageDataResponse
import com.tetoota.selectlanguage.SelectLanguageActivity
import com.tetoota.selectlanguage.SelectLanguageContract
import com.tetoota.selectlanguage.SelectLanguagePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_splash.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast


/**
 * Created by charchit.kasliwal on 30-05-2017.
 */
class SplashActivity : AppCompatActivity(), SplashContract.View, IDialogListener, SelectLanguageContract.View,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private lateinit var mFirebaseAnalytics: FirebaseAnalytics
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private val SPLASH_TIME_OUT = 2000
    var mSplashPresenter = SplashPresenter(this)
    var mContext: Context? = null
    private var tetootaApplication: TetootaApplication? = null
    private val mSelectLanguagePresenter: SelectLanguagePresenter by lazy {
        SelectLanguagePresenter(this@SplashActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      /*  requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)*/
        setContentView(R.layout.activity_splash)

        mContext = this
        tetootaApplication = applicationContext as TetootaApplication

        tetootaApplication!!.isFirstTime = "isFirstTime"

        TetootaApplication.setTextValuesHashMap(Utils.getLangByCode(this,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", this)))
        buildGoogleApiClient()

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        sendGoogleAnalytics()
        //requires a registration e.g. in the onCreate method

    }

    /**
     * Initialize the google api client
     */
    @Synchronized
    private fun buildGoogleApiClient() {
        mLocationRequest = LocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this@SplashActivity)
                .addConnectionCallbacks(this@SplashActivity)
                .addOnConnectionFailedListener(this@SplashActivity)
                .addApi(LocationServices.API)
                .build()

    }

    private fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this)
    }


/*
    public override fun onStart() {
        super.onStart()
        // Branch init

        Branch.getInstance().initSession(object : Branch.BranchReferralInitListener {
            override fun onInitFinished(referringParams: JSONObject, error: BranchError?) {
                if (error == null) {
                    Log.e("BRANCH SDK IF ", referringParams.toString())
                } else {
                    Log.e("BRANCH SDK ELSE ", error.message)
                    Log.e("referringParams else ", referringParams.toString())
                }
            }
        }, this.intent.data, this)
    }
*/

/*
    override fun onNewIntent(intent: Intent) {
        this.intent = intent
    }
*/


    override fun onResume() {
        super.onResume()
        mGoogleApiClient?.connect()
        Handler().postDelayed(Runnable {
            if (permissions())
                locationEnable(this@SplashActivity, mLocationRequest!!, mGoogleApiClient!!)
            //  mSplashPresenter.checkAppSession(this@SplashActivity)
        }, SPLASH_TIME_OUT.toLong())
    }


    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient?.isConnected!!) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                    this)
            mGoogleApiClient?.disconnect()
        }
    }


    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mGoogleApiClient?.isConnected!!) {
            stopLocationUpdates()
        }
    }

    override fun onMainActivitySuccess() {
        if (Utils.haveNetworkConnection(this@SplashActivity)) {
            progress_view.visibility = View.VISIBLE
            mSelectLanguagePresenter.getSelectedLanguageData(this@SplashActivity)
        } else {
            this.toast(Utils.getText(mContext, StringConstant.str_check_internet))
            progress_view.visibility = View.INVISIBLE
            startActivity<MainActivity>()
            finish()
        }
    }

    override fun onLoginActivitySuccess() {
        startActivity<SelectLanguageActivity>()
        finish()
    }

    /**
     * Method to Check Runtime Permissions on Splash
     * Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
     */


    fun permissions(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        } else if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            println("You Dont have permissions")
            return true
        } else {
            println("All Permissions Are Required To Move Forward")
            if (!isFinishing) {
                AllPermissisonsDialog(this, this, Constant.DIALOG_PERMISSIONS, this).show()
            }
        }
        return false
    }


/*
    fun permissions(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        } else if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
            return true
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)) {
            println("You Dont have permissions")
            return true
        } else {
            println("All Permissions Are Required To Move Forward")
            if (!isFinishing) {
                AllPermissisonsDialog(this, this, Constant.DIALOG_PERMISSIONS, this).show()
            }
        }
        return false
    }
*/

    /**
     * I Dialog Listener call Back
     */
    override fun onYesPress(param: String, message: String) {
        if (message.equals("success", ignoreCase = true)) {
            locationEnable(this@SplashActivity, mLocationRequest!!, mGoogleApiClient!!)
        } else {
            mSplashPresenter.checkAppSession(this@SplashActivity)
            //finish()
        }
    }

    override fun onLocationChanged(p0: Location?) {

    }

    //@SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest.create()

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            100 -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        mSplashPresenter.checkAppSession(this@SplashActivity)
                        // All required changes were successfully made
                    }
                    Activity.RESULT_CANCELED -> {
                        mSplashPresenter.checkAppSession(this@SplashActivity)
                    }
                }
            }
        }

    }

    override fun onSelectLanguageSuccessResult(mCategoriesList: List<LanguageDataResponse>?, message: String) {}

    override fun onSelectedLanguageSuccessResult(message: String) {
        super.onSelectedLanguageSuccessResult(message)
        progress_view.visibility = View.INVISIBLE
        startActivity<MainActivity>()
        finish()
    }

    override fun onSelectLanguageFailureResult(message: String) {
        progress_view.visibility = View.INVISIBLE
    }


    fun locationEnable(context: Activity, mLocationRequest: LocationRequest, mGoogleApiClient: GoogleApiClient) {
        // Setting API to check the GPS on device is enabled or not
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)

        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            //                final LocationSettingsStates = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        mSplashPresenter.checkAppSession(this@SplashActivity)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(
                                context,
                                100)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can
            // initialize location requests here.
            // Location settings are not satisfied. However, we have no way
            // to fix the settings so we won't show the dialog.
        }
    }

    private fun sendGoogleAnalytics() {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "1")
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "SplashScreen")
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image")
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
    }
}


