package com.tetoota.splash
import android.app.Activity
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
/**
 * Created by charchit.kasliwal on 01-06-2017.
 */
class SplashInteractor {
    var mSplashListener : SplashContract.onSplashListener

    constructor(mSplashListener: SplashContract.onSplashListener) {
        this.mSplashListener = mSplashListener
    }

    fun appSession(mActivity: Activity) {
        if(Utils.loadPrefrence(Constant.IS_USER_LOGGED_IN,"false",mActivity) != null&&Utils.loadPrefrence(Constant.IS_USER_LOGGED_IN,"false",mActivity) == "true"){
            mSplashListener.onMainActivitySuccess("success")
        } else{
            mSplashListener.onLoginActivitySuccess("success")
        }
    }
}