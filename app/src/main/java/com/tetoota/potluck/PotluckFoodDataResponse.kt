package com.tetoota.potluck

import android.os.Parcel
import android.os.Parcelable

import android.os.Parcelable.Creator
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PotluckFoodDataResponse : Parcelable {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("content")
    @Expose
    var content: String? = null
    @SerializedName("Lat")
    @Expose
    var Lat: Any? = null
    @SerializedName("Long")
    @Expose
    var Long: Any? = null
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("days")
    @Expose
    var days: String? = null
    @SerializedName("user_id")
    @Expose
    var userId: Int? = null
    @SerializedName("user_first_name")
    @Expose
    var user_first_name: String? = null
    @SerializedName("user_last_name")
    @Expose
    var user_last_name: String? = null
    @SerializedName("profile_image")
    @Expose
    var profileImage: String? = null
    @SerializedName("expiry_days")
    @Expose
    var expiry_days: String? = null
    @SerializedName("total_record")
    @Expose
    var total_record: Int? = null
    @SerializedName("creation_date")
    @Expose
    var creation_date: String? = null
    @SerializedName("post_type")
    @Expose
    var Food: String? = null
    @SerializedName("reviews")
    @Expose
    var reviews: String? = null

    @SerializedName("review_avg")
    @Expose
    var review_avg: String? = null
    @SerializedName("total_usercount")
    @Expose
    var total_usercount: String? = null
    @SerializedName("userReview")
    @Expose
    var userReview: String? = ""

    protected constructor(`in`: Parcel) {
        this.id = `in`.readValue(Int::class.java.classLoader) as Int
        this.title = `in`.readValue(String::class.java.classLoader) as String
        this.content = `in`.readValue(String::class.java.classLoader) as String
        this.Lat = `in`.readValue(String::class.java.classLoader) as Any
        this.Long = `in`.readValue(String::class.java.classLoader) as Any
        this.image = `in`.readValue(String::class.java.classLoader) as String
        this.days = `in`.readValue(String::class.java.classLoader) as String
        this.userId = `in`.readValue(Int::class.java.classLoader) as Int
        this.user_first_name  = `in`.readValue(String::class.java.classLoader) as String
        this.user_last_name  = `in`.readValue(String::class.java.classLoader) as String
        this.profileImage = `in`.readValue(String::class.java.classLoader) as String
        this.expiry_days = `in`.readValue(String::class.java.classLoader) as String
        this.total_record = `in`.readValue(Int::class.java.classLoader) as Int
        this.creation_date = `in`.readValue(String::class.java.classLoader) as String
        this.Food = `in`.readValue(String::class.java.classLoader) as String
        this.reviews = `in`.readValue(String::class.java.classLoader) as String
        this.review_avg = `in`.readValue(String::class.java.classLoader) as String
        this.total_usercount = `in`.readValue(String::class.java.classLoader) as String
        this.userReview = `in`.readValue(String::class.java.classLoader) as String
    }

    constructor() {}

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeValue(id)
        dest.writeValue(title)
        dest.writeValue(content)
        dest.writeValue(Lat)
        dest.writeValue(Long)
        dest.writeValue(image)
        dest.writeValue(days)
        dest.writeValue(userId)
        dest.writeValue(user_first_name)
        dest.writeValue(user_last_name)
        dest.writeValue(profileImage)
        dest.writeValue(expiry_days)
        dest.writeValue(total_record)
        dest.writeValue(creation_date)
        dest.writeValue(Food)
        dest.writeValue(reviews)
        dest.writeValue(review_avg)
        dest.writeValue(total_usercount)
        dest.writeValue(userReview)
    }
    override fun describeContents(): Int {
        return 0
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<PotluckFoodDataResponse> = object : Parcelable.Creator<PotluckFoodDataResponse> {
            override fun createFromParcel(source: Parcel): PotluckFoodDataResponse = PotluckFoodDataResponse(source)
            override fun newArray(size: Int): Array<PotluckFoodDataResponse?> = arrayOfNulls(size)
        }
    }

}