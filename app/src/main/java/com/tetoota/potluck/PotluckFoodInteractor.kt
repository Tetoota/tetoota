package com.tetoota.potluck

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Util
import com.tetoota.utility.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class PotluckFoodInteractor {
    //-------------------------------Define-Public-Variable--------------------------------------------//
    var mPotluckFoodApiListener: PotluckFoodContract.PotlwekFoodApiListener

    //-------------------------------Define-constructor------------------------------------------------//
    constructor(mPotluckFoodApiListener: PotluckFoodPresenter) {
        this.mPotluckFoodApiListener = mPotluckFoodApiListener
    }

    //-------------------------------Define-potlwekFoodApiCall-----------------------------------------//
    fun potlwekFoodApiCall(mActivity: Activity, pagination: Int?, PostType: String, personData: LoginDataResponse, lat: Double, longi: Double) {
        Log.e("PotlwekFood", "" + personData)
        var call: Call<PotluckFoodResponse> = TetootaApplication.getHeader()
                .potlwekFoodAllApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        personData.auth_token!!,
                        pagination,
                        PostType,
                        personData.user_id!!, lat, longi)
        call.enqueue(object : Callback<PotluckFoodResponse> {
            override fun onResponse(call: Call<PotluckFoodResponse>?,
                                    response: Response<PotluckFoodResponse>?) {
                println("PotlwekFoodSuccess")
                var mProfileData: PotluckFoodResponse? = response?.body()
                Log.e("PotlwekFoodData", "" + mProfileData)
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        mPotluckFoodApiListener.onPotlwekFoodApISuccess(mProfileData?.data, "userProfileData")
                    } else {
                        mPotluckFoodApiListener.onPotlwekFoodApIiFailure(response.body()?.meta?.message.toString(), "profileFailure")
                    }
                } else {
                    mPotluckFoodApiListener.onPotlwekFoodApIiFailure(response?.body()?.meta?.message.toString(), "profileFailure")
                }
            }

            override fun onFailure(call: Call<PotluckFoodResponse>?, t: Throwable?) {
                mPotluckFoodApiListener.onPotlwekFoodApIiFailure(t?.message.toString(), "profileFailure")
            }
        })
    }

    //-------------------------------Define-prepareFilePart--------------------------------------------//
    fun prepareFilePart(mActivity: Activity, partName: String, fileUri: String): MultipartBody.Part {
        //File creating from selected URL
        val file = File(fileUri)
        // create RequestBody instance from file
        val requestFile = okhttp3.RequestBody.create(MediaType.parse("image*//**//*"), file)
        val body = MultipartBody.Part.createFormData("post_image_attached[]", file.name, requestFile)
        //Log.e("*****body*****", "" + body)
        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
    }

    //-------------------------------Define-AddPotluckFoodAPI--------------------------------------------//
    fun AddPotluckFoodAPI(mActivity: Activity, data: HashMap<String, RequestBody>, mImagePath: ArrayList<String>) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//        if (mImagePath == null || mImagePath.size <= 0) {
//            var call: Call<PotluckFoodResponse> = TetootaApplication.getHeader().addFoodPotluckApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
//                    Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
//                    personData.auth_token!!, data)
//            call!!.enqueue(object : Callback<PotluckFoodResponse> {
//                override fun onResponse(call: Call<PotluckFoodResponse>?,
//                                        response: Response<PotluckFoodResponse>?) {
//                    var mProfileData: PotluckFoodResponse? = response?.body()
//                    if (response?.code() == 200) {
//                        if (response.body()?.data?.size!! > 0) {
//                            val json = Gson().toJson(response.body()?.data!![0])
//                            Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json, mActivity)
//                            mPotluckFoodApiListener.onPotlwekFoodApIiFailure(response.body()?.meta?.message.toString(), "profileUpdate")
//                        } else {
//                            mPotluckFoodApiListener.onPotlwekFoodApIiFailure(response.body()?.meta?.message.toString(), "profileFailure")
//                        }
//                    } else {
//                        mPotluckFoodApiListener.onPotlwekFoodApIiFailure(response?.body()?.meta?.message.toString(), "profileFailure")
//                    }
//                }
//
//                override fun onFailure(call: Call<PotluckFoodResponse>?, t: Throwable?) {
//                    mPotluckFoodApiListener.onPotlwekFoodApIiFailure(t?.message.toString(), "profileFailure")
//                }
//            })
//        } else {
        var call: Call<PotluckFoodResponse> = TetootaApplication.getHeader()
                .addFoodPotluckApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        personData.auth_token!!, data, Util.createMultiPartData(mImagePath, mActivity))
        call.enqueue(object : Callback<PotluckFoodResponse> {
            override fun onResponse(call: Call<PotluckFoodResponse>?,
                                    response: Response<PotluckFoodResponse>?) {
                var mProfileData: PotluckFoodResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.meta?.code == 200) {

                        //   Log.e("mActivity",""+ mActivity)
                        mPotluckFoodApiListener.onAddPotluckSuccess(response.body()?.meta?.message.toString())
                    } else {
                        mPotluckFoodApiListener.onAddPotluckFailure(response.body()?.meta?.message.toString())
                    }
                } else {
                    mPotluckFoodApiListener.onAddPotluckFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<PotluckFoodResponse>?, t: Throwable?) {
                mPotluckFoodApiListener.onAddPotluckFailure(t?.message.toString())
            }
        }
        )
//        }
    }
//-------------------------------------------------End--------------------------------------------//
}