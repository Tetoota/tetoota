package com.tetoota.potluck

import com.tetoota.details.Meta

data class PotluckFoodResponse (
        val data: List<PotluckFoodDataResponse?>? = null,
        val meta: Meta? = null
)