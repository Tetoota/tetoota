package com.tetoota.potluck

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.cropImage.RotateBitmap.TAG
import com.tetoota.customviews.PaginationScrollListener
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.potluck.my_potluck_food.MyFoodContract
import com.tetoota.potluck.my_potluck_food.MyFoodPresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_google_translate.progress_view
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.fragment_potlwek_food.*
import kotlinx.android.synthetic.main.fragment_potlwek_food.loadItemsLayout_recyclerView
import kotlinx.android.synthetic.main.fragment_potlwek_food.rl_nrf
import kotlinx.android.synthetic.main.fragment_wishlist.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.io.IOException

class MyPotluckFoodFragment : BaseFragment(),
        View.OnClickListener,
        MyFoodContract.View,
        ViewPager.OnPageChangeListener,
        PotluckFoodAdapter.IAdapterClick,
        PotluckFoodAdapter.PaginationAdapterCallback {
    override fun onWishListApiSuccessResult(mCategoriesList: List<Any?>?, message: String) {
        hideProgressDialog()
        //Log.i("listtttt", "" + (message?.get(0)?.total_record))
        if (view != null) {
            if (adapter?.itemCount != adapter?.getTotalRecord()) {
                progress_view.visibility = View.GONE
                rl_nrf.visibility = View.GONE
                hideErrorView()
                loadItemsLayout_recyclerView.visibility = View.GONE
                if (activity != null && MyPotluckFoodFragment() != null) {
                    if (mCategoriesList != null) {
                        if (mCategoriesList.size > 0) {
                            //Log.e("TAG", "SIZE() = " + mCategoriesList.size)
                            var sdfsd: PotluckFoodDataResponse = mCategoriesList.get(0) as PotluckFoodDataResponse
                        } else {
                        }
                    }
                    adapter?.addAll(mCategoriesList as List<PotluckFoodDataResponse?>)
                }
                isLoad = false
            } else {
                loadItemsLayout_recyclerView.visibility = View.GONE
                isListLastPage = true
                progress_view.visibility = View.GONE
                rl_nrf.visibility = View.VISIBLE
                hideErrorView()
            }
        }
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        try {
            isListLastPage = true
            progress_view.visibility = View.GONE
            loadItemsLayout_recyclerView.visibility = View.GONE
            hideErrorView()
        } catch (e: IOException) {
            e.printStackTrace();
        }
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //-------------------------------------Define-Public-Variable---------------------------------------//
    private var adapter: PotluckFoodAdapter? = null
    private val FILTER_REQUEST: Int = 12
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoad = false
    public var currentPage = 1
    public var isListLastPage = false
    private var TOTAL_PAGES = 100
    var exchangePostType: String? = ""
    private var tetootaApplication: TetootaApplication? = null
    //-------------------------------------retryPageLoad-Method-----------------------------------------//
    override fun retryPageLoad() {
    }

    //-------------------------------------cellItemClick-Method-----------------------------------------//
    override fun cellItemClick(mViewClickType: String, data: PotluckFoodDataResponse) {
        val intent = Intent(context, PotluckFoodActivity::class.java)
        intent.putExtra("data", data)
        startActivity(intent)
    }

    //------------------------------------------onClick-Method-----------------------------------------//
    override fun onClick(v: View?) {
    }

    //------------------------------------------onPotlwekFoodApISuccess-Method------------------------//
//    override fun onPotlwekFoodApISuccess(message: List<PotluckFoodDataResponse?>?, mesg: String) {
//        hideProgressDialog()
//        //Log.i("listtttt", "" + (message?.get(0)?.total_record))
//        if (view != null) {
//            if (adapter?.itemCount != adapter?.getTotalRecord()) {
//                progress_view.visibility = View.GONE
//                rl_nrf.visibility = View.GONE
//                hideErrorView()
//                loadItemsLayout_recyclerView.visibility = View.GONE
//                if (activity != null && MyPotluckFoodFragment() != null) {
//                    if (message != null) {
//                        if (message.size > 0) {
//                            //Log.e("TAG", "SIZE() = " + message.size)
//                            var sdfsd: PotluckFoodDataResponse = message.get(0) as PotluckFoodDataResponse
//                        } else {
//                        }
//                    }
//                    adapter?.addAll(message as List<PotluckFoodDataResponse?>)
//                }
//                isLoad = false
//            } else {
//                loadItemsLayout_recyclerView.visibility = View.GONE
//                isListLastPage = true
//                progress_view.visibility = View.GONE
//                rl_nrf.visibility = View.VISIBLE
//                hideErrorView()
//            }
//        }
//    }

    //------------------------------------------hideErrorView-Method----------------------------------//
    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            view_list.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    //------------------------------------------onPotlwekFoodApiSuccessResult-Method-------------------//
//    override fun onPotlwekFoodApiSuccessResult(message: String, apiCallMethod: String) {
//        try {
//            isListLastPage = true
//            progress_view.visibility = View.GONE
//            loadItemsLayout_recyclerView.visibility = View.GONE
//            hideErrorView()
//        } catch (e: IOException) {
//            e.printStackTrace();
//        }
//    }

    //------------------------------------onPotlwekFoodDetailApiSuccessResult-Method-------------------//
//    override fun onPotlwekFoodDetailApiSuccessResult(message: String, status: Boolean) {
//    }
//
//    override fun onPotlwekFoodDetailApiFailureResult(message: String) {
//    }

    override fun onPageScrollStateChanged(p0: Int) {
    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
    }

    override fun onPageSelected(p0: Int) {
    }

    //------------------------------------onCreate-Method---------------------------------------------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tetootaApplication = activity!!.applicationContext as TetootaApplication
        setHasOptionsMenu(true)
    }

    //------------------------------------onCreateView-Method------------------------------------------//
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_potlwek_food, container, false)
    }

    //------------------------------------onAttach-Method----------------------------------------------//
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    //------------------------------------onDetach-Method----------------------------------------------//
    override fun onDetach() {
        super.onDetach()
    }

    //------------------------------------OnFragmentInteractionListener-Method-------------------------//
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    //------------------------------------setupRecyclerView-Method------------------------------------//
    private fun setupRecyclerView() {
        linearLayoutManager = LinearLayoutManager(context!!)
        PotlwekFood_recycler_view.setHasFixedSize(true)
        PotlwekFood_recycler_view.layoutManager = linearLayoutManager
        adapter = PotluckFoodAdapter(context!!, iAdapterClickListener = this, iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        PotlwekFood_recycler_view.setLayoutManager(linearLayoutManager);
        PotlwekFood_recycler_view.adapter = adapter
    }

    //--------------------------------------heightCalculation-Method------------------------------------//
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .2f).toInt()
        return getTopMarginH
    }

    //--------------------------------------FragmentManager-Method------------------------------------//
    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out)
        fragmentTransaction.func()
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commitAllowingStateLoss()
    }

    //--------------------------------------FragmentManager-Method------------------------------------//
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        getData(true, "Food")
        PotlwekFood_recycler_view.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isListLastPage
            }

            override fun loadMoreItems() {
                isLoad = true
                currentPage++
                //Log.e(TAG, "currentPage" + currentPage)
                loadItemsLayout_recyclerView.visibility = View.VISIBLE
                getData(true, "Food")
            }

            override fun getTotalPageCount(): Int {
                return TOTAL_PAGES
            }

            override fun isLoading(): Boolean {
                return isLoad
            }
        })
        PotlwekFoo_fab.onClick {
            val intent = AddPotluckActivity.newMainIntent(this.activity!!)
            ActivityStack.getInstance(this.activity!!)
            startActivityForResult(intent, FILTER_REQUEST)
        }
    }

    //---------------------------mPotluckFoodDetailPresenter-Method------------------------------------//
    private val mPotluckFoodDetailPresenter: MyFoodPresenter by lazy {
        MyFoodPresenter(this@MyPotluckFoodFragment)
    }

    //-------------------------------------fetchServiceData-Method------------------------------------//
    private fun fetchServiceData(pagination: Int, PostType: String, isLoadMore: Boolean, lat: Double, longi: Double) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                showProgressDialog("Please Wait...")
            }
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mPotluckFoodDetailPresenter.getServicesData(activity!!, PostType, pagination)
        } else {
            activity!!.toast(Utils.getText(context, "No Record Found"))
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            loadItemsLayout_recyclerView.visibility = View.GONE
            hideProgressDialog()
        }
    }

    //----------------------------------------------getData-Method------------------------------------//
    @SuppressLint("MissingPermission")
    fun getData(isLoadMore: Boolean, PostType: String) {
        fetchServiceData(currentPage, PostType, isLoadMore, tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude)
    }

    //----------------------------------onCreateOptionsMenu-Method------------------------------------//
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //----------------------------------onPrepareOptionsMenu-Method------------------------------------//
    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        activity!!.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.setVisible(false);
        super.onPrepareOptionsMenu(menu)
    }

    //------------------------------------------newInstance-Method------------------------------------//
    fun newInstance(): MyPotluckFoodFragment {
        val fragment = MyPotluckFoodFragment()
        return fragment
    }

    //--------------------------------------------------End-Method------------------------------------//
    companion object {
        fun newInstance(param1: String, param2: String): MyPotluckFoodFragment {
            val fragment = MyPotluckFoodFragment()
            return fragment
        }
    }
}