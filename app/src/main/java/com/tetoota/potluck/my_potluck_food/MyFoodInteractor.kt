package com.tetoota.potluck.my_potluck_food

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.dashboard.FavoriteResponse
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.potluck.PotluckFoodResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 24-07-2017.
 */
class MyFoodInteractor {
    val sliderList: String? = "sliderList"
    var call: Call<PotluckFoodResponse>? = null
    var mServiceApiListener: MyFoodContract.serviceApiListener

    constructor(mServiceApiListener: MyFoodContract.serviceApiListener) {
        this.mServiceApiListener = mServiceApiListener
    }

    @Synchronized
    fun getMyFoodList(mActivity: Activity, postType: String, pagination: Int?) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        call = TetootaApplication.getHeader()
                .getMyFoodList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        personData.user_id!!, pagination!!, postType)
//        Log.e("HI ", personData.toString())
        call!!.enqueue(object : Callback<PotluckFoodResponse> {
            override fun onResponse(call: Call<PotluckFoodResponse>?,
                                    response: Response<PotluckFoodResponse>?) {
                var mDashboardSliderData: PotluckFoodResponse? = response?.body()
                Log.e("getMyFoodList  ", "" + response?.body().toString())
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        if (mDashboardSliderData != null) {
                            Log.e("getMyFoodList  ", "if if")
                            mServiceApiListener.onWishListApiSuccess(mDashboardSliderData.data, "wishlist")
                        }
                    } else {
                        Log.e("getMyFoodList  ", "if else")
                        mServiceApiListener.onApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                } else {
                    Log.e("getMyFoodList" +
                            "  ", "else")
                    mServiceApiListener.onApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<PotluckFoodResponse>?, t: Throwable?) {
                Log.e("onFailure  ", t?.message.toString())

                mServiceApiListener.onApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun cancelREquest(mActivity: Activity, mCancelRequest: String) {
        if (mCancelRequest.equals("userService")) {
            call?.cancel()
        }
    }
}