package com.tetoota.potluck.my_potluck_food

import android.app.Activity
import com.tetoota.fragment.dashboard.ServicesDataResponse

/**
 * Created by charchit.kasliwal on 24-07-2017.
 */
class MyFoodPresenter : MyFoodContract.Presenter, MyFoodContract.serviceApiListener {
    override fun onWishListApiSuccess(mDataList: List<Any?>?, message: String) {

    }

    override fun onApiFailure(message: String, isServerError: Boolean) {
    }

    override fun onFavoriteApiSuccess(message: String, cellRow: ServicesDataResponse, p1: Int) {
    }

    var mMyFoodContract: MyFoodContract.View
    private val mHomeInteractor: MyFoodInteractor by lazy {
        MyFoodInteractor(this)
    }

    constructor(mMyFoodContract: MyFoodContract.View) {
        this.mMyFoodContract = mMyFoodContract
    }

    override fun getWishListData(mActivity: Activity) {
        // mHomeInteractor.getWishListdata(mActivity)
    }

    override fun cancelRequest(mActivity: Activity, wishListType: String) {
        mHomeInteractor.cancelREquest(mActivity, wishListType)
    }

    override fun onWishListApiFailure(message: String, isServerError: Boolean) {
        super.onWishListApiFailure(message, isServerError)
        mMyFoodContract.onWishListApiFailure(message, isServerError)
    }

    /**
     * the mActivity
     * the user post type "Services"
     */
    override fun getServicesData(mActivity: Activity, postType: String, pagination: Int?) {
        mHomeInteractor.getMyFoodList(mActivity, postType, pagination)
    }

}