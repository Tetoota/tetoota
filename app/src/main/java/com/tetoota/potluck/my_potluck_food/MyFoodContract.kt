package com.tetoota.potluck.my_potluck_food

import android.app.Activity
import com.tetoota.fragment.dashboard.ServicesDataResponse

/**
 * Created by charchit.kasliwal on 24-07-2017.
 */
class MyFoodContract {
    interface View {
        fun onWishListApiSuccessResult(mCategoriesList: List<Any?>?, message: String)
        fun onWishListApiFailure(message: String, isServerError: Boolean){}
        fun onApiFailureResult(message: String, isServerError: Boolean)
        fun favoriteApiResult(message: String, cellRow : ServicesDataResponse, p1 : Int)
    }

    internal interface Presenter {
        fun getWishListData(mActivity : Activity)
        fun getServicesData(mActivity : Activity,postType : String,pagination : Int?)
        fun cancelRequest(mActivity: Activity,wishListType : String)
    }

    interface serviceApiListener {
        fun onWishListApiSuccess(mDataList: List<Any?>?, message: String)
        fun onApiFailure(message: String, isServerError: Boolean)
        fun onWishListApiFailure(message: String, isServerError: Boolean){}
        fun onFavoriteApiSuccess(message: String,cellRow : ServicesDataResponse,p1 : Int)
    }
}