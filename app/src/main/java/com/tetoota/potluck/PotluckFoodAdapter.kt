package com.tetoota.potluck

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.model.LatLng
import com.squareup.picasso.Picasso
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_potlwekfood_item.view.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

class PotluckFoodAdapter(
        val mContext: Context,
        var iAdapterClickListener: IAdapterClick,
        var iPaginationAdapterCallback: PaginationAdapterCallback,
        var screenHeight: Int,
        var mPotlwekFoodList: MutableList<Any?> = ArrayList<Any?>()) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

//---------------------------Define-onCreateViewHolder-Method---------------------------------------------//
    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.activity_potlwekfood_item, viewGroup, false)
        return ViewHolder(view)
    }
//--------------------------------Define-getItemCount-Method---------------------------------------------//
    override fun getItemCount(): Int {
        return mPotlwekFoodList.size
    }
//--------------------------------------Define-addAll-Method---------------------------------------------//
    fun addAll(moveResults: List<PotluckFoodDataResponse?>) {
        for (result in moveResults) {
            add(result)
        }
    }
//----------------------------------------Define-add-Method---------------------------------------------//
    private fun add(result: PotluckFoodDataResponse?) {
        mPotlwekFoodList.add(result)
        notifyItemInserted(mPotlwekFoodList.size - 1)
    }
//----------------------------------------Define-onBindViewHolder-Method-------------------------------//
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, p1: Int) {
        if (viewHolder is ViewHolder) {
            val mServiceList = mPotlwekFoodList as List<PotluckFoodDataResponse>
            viewHolder.bindServiceData(viewHolder as ViewHolder?, p1, mServiceList[p1], screenHeight, iAdapterClickListener, mContext)
        } else if (viewHolder is ViewHolder) {
            viewHolder.bindData(mPotlwekFoodList[p1] as PotluckFoodDataResponse)
        }
    }
//----------------------------------------Define-ViewHolder-Method---------------------------------//
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mImageView = view.ivPotLock!!
        val mtitle = view.tvTitle!!
        val tvUsername = view.tvUsername!!
        val mtv_description = view.description!!
        val tvLocation = view.tvLocation!!
        //val mtv_date = view.date!!
        val llMainMarket = view.llMainMarket!!
        private var tetootaApplication: TetootaApplication? = null
//----------------------------------------Define-bindServiceData-Method-----------------------------//
        fun bindServiceData(viewHolder: ViewHolder?, p1: Int, mServiceData: PotluckFoodDataResponse,
                            height: Int, iAdapterClickListener: PotluckFoodAdapter.IAdapterClick, context: Context) {
            tetootaApplication = context.applicationContext as TetootaApplication
         /*   mtitle.text = mServiceData.title*/
            tvLocation.text = String.format("%.2f", Utils.checkDistance(LatLng(tetootaApplication!!.myLatitude.toDouble(), tetootaApplication!!.myLongitude.toDouble()),
                    LatLng(mServiceData.Lat as Double, mServiceData.Long as Double))) + "Km"
            tvUsername.text = mServiceData.user_first_name + " " + mServiceData.user_last_name

            if (mServiceData.title!!.isEmpty()) {
                mtitle.text = "No Data Available"
            } else {
                mtitle.text = mServiceData.title
            }
            if (mServiceData.user_first_name!!.isEmpty()) {
                tvUsername.text = "No Data Available"
            } else {
                tvUsername.text = mServiceData.user_first_name + " " + mServiceData.user_last_name
            }
            tvUsername.setSelected(true)
            mtv_description.text = mServiceData.content

            if (mServiceData.image!!.isEmpty()) {
                mImageView.setImageResource(R.drawable.user_placeholder);
            } else {
                Picasso.get().load(mServiceData.image).resize(170,170).error(R.drawable.place_holder).placeholder(R.drawable.place_holder).into(mImageView)
            }
            llMainMarket.onClick {
                iAdapterClickListener.cellItemClick("service", mServiceData)
            }
            if (mServiceData.image?.isEmpty()!!) {
                context.toast("Profile image is not available")
            }
        }
//----------------------------------------Define-bindData-Method-----------------------------------//
        fun bindData(mPotlewekDataResponse: PotluckFoodDataResponse) {
           // mtitle.text = mPotlewekDataResponse.title
            if (mPotlewekDataResponse.user_first_name!!.isEmpty()) {
                tvUsername.text = "No Data"
            } else {
                tvUsername.text = mPotlewekDataResponse.user_first_name + " " + mPotlewekDataResponse.user_last_name
            }
            if (mPotlewekDataResponse.title!!.isEmpty()) {
                mtitle.text = "No Data"
            } else {
                mtitle.text = mPotlewekDataResponse.title
            }
            if (mPotlewekDataResponse.content!!.isEmpty()) {
                mtv_description.text = "No Data"
            } else {
                mtv_description.text = mPotlewekDataResponse.content
            }
            //mtv_description.text = mPotlewekDataResponse.content
            if (mPotlewekDataResponse.image!!.isEmpty()) {
                mImageView.setImageResource(R.drawable.queuelist_place_holder)
            } else {
                Picasso.get().load(mPotlewekDataResponse.image).into(mImageView)
            }
        }
    }
//------------------------------------Define-IAdapterClick-Method-----------------------------------//
    interface IAdapterClick {
        fun cellItemClick(mViewClickType: String, data: PotluckFoodDataResponse): Unit
    }
//-----------------------Define-PaginationAdapterCallback-Method-----------------------------------//
    interface PaginationAdapterCallback {
        fun retryPageLoad()
    }
//---------------------------------Define-getTotalRecord-Method-----------------------------------//
    fun getTotalRecord(): Int? {
        if (mPotlwekFoodList.size > 0) {
            return (mPotlwekFoodList as List<PotluckFoodDataResponse>)[0].total_record
        } else {
            return -1
        }
    }
//---------------------------------Define-End-Method-----------------------------------//
}