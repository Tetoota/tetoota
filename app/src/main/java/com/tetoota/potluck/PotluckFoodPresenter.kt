package com.tetoota.potluck

import android.app.Activity
import com.tetoota.login.LoginDataResponse
import okhttp3.RequestBody

class PotluckFoodPresenter : PotluckFoodContract.PotlwekFoodApiListener,
        PotluckFoodContract.Presenter {

    override fun onAddPotluckSuccess(mString: String) {
        mPotluckFoodContract.onAddPotluckSuccess(mString)
    }

    override fun onAddPotluckFailure(mString: String) {
        mPotluckFoodContract.onAddPotluckFailureResponse(mString)
    }

    //-------------------------------Define-onPotlwekFoodApIi-----------------------------------------//
    override fun onPotlwekFoodApIiSuccess(message: String, apiCallMethod: String) {
    }

    override fun onPotlwekFoodApISuccess(profileData: List<PotluckFoodDataResponse?>?, message: String) {
        mPotluckFoodContract.onPotlwekFoodApISuccess(profileData, message)
    }

    override fun onPotlwekFoodApIiFailure(message: String, apiCallMethod: String) {
        mPotluckFoodContract.onPotlwekFoodApiSuccessResult(message, apiCallMethod)
    }

    override fun PotlwekFoodDetailApiSuccessResult(message: String, status: Boolean) {
    }

    override fun PotlwekFoodDetailApiFailureResult(message: String) {
    }

    var mPotluckFoodContract: PotluckFoodContract.View
    private val mPotluckFoodInteractor: PotluckFoodInteractor by lazy {
        com.tetoota.potluck.PotluckFoodInteractor(this)
    }

    //---------------------------------------------Define-constructor----------------------------------//
    constructor(mPotluckFoodContract: PotluckFoodContract.View) {
        this.mPotluckFoodContract = mPotluckFoodContract
    }

    //----------------------------------Define-getDataPotlwekFoodOnly----------------------------------//
    fun getDataPotlwekFoodOnly(mActivity: Activity, pagination: Int?, PostType: String, personData: LoginDataResponse, lat: Double, longi: Double) {
        if (mActivity != null) {
            mPotluckFoodInteractor.potlwekFoodApiCall(mActivity, pagination, PostType, personData, lat, longi)
        }
    }

    //----------------------------------Define-PotluckDataAPI----------------------------------//
    override fun PotluckDataAPI(mActivity: Activity, data: HashMap<String, RequestBody>, mImagePath: ArrayList<String>) {
        if (mActivity != null) {
            mPotluckFoodInteractor.AddPotluckFoodAPI(mActivity, data, mImagePath)
        }
    }
//-----------------------------------------------------------End----------------------------------//
}