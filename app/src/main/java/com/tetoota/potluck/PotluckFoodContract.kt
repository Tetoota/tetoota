package com.tetoota.potluck

import android.app.Activity
import okhttp3.RequestBody

class PotluckFoodContract {
    interface View {
        fun onPotlwekFoodApISuccess(message: List<PotluckFoodDataResponse?>?, mesg: String)
        fun onPotlwekFoodApiSuccessResult(message: String, apiCallMethod: String)
        fun onPotlwekFoodDetailApiSuccessResult(message: String, status : Boolean)
        fun onPotlwekFoodDetailApiFailureResult(message: String)

        fun onAddPotluckSuccess(mString: String)
        fun onAddPotluckFailureResponse(mString: String)
    }
    internal interface Presenter {
        fun PotluckDataAPI(mActivity: Activity, data: HashMap<String, RequestBody>, mImagePath: ArrayList<String>)
    }
    interface PotlwekFoodApiListener {
        fun onPotlwekFoodApISuccess(profileData: List<PotluckFoodDataResponse?>?, message: String)
        fun onPotlwekFoodApIiSuccess(message: String, apiCallMethod: String)
       /* fun onPotlwekFoodApIiSuccess(message: String, apiCallMethod: String)*/
        fun onPotlwekFoodApIiFailure(message: String, apiCallMethod: String)
        fun PotlwekFoodDetailApiSuccessResult(message: String, status : Boolean)
        fun PotlwekFoodDetailApiFailureResult(message: String)

        fun onAddPotluckSuccess(mString: String)
        fun onAddPotluckFailure(mString: String)

    }
}