package com.tetoota.potluck

import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.addrequest.MyFileContentProvider
import com.tetoota.customviews.ImageFilePath
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Util
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_add_potluck.*
import kotlinx.android.synthetic.main.activity_add_potluck.btn_submit_req
import kotlinx.android.synthetic.main.fragment_add__service.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import okhttp3.RequestBody
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.io.*

class AddPotluckActivity : BaseActivity(),
        View.OnClickListener,
        OnMapReadyCallback,
        PotluckFoodContract.View {
    //-----------------------Define-Public-Variable----------------------------------------------//
    private var menu: Menu? = null
    var CAMERA_RESULT = 224
    var GALLARY_RESULT = 124
    var PLACE_PICKER_REQUEST = 1;
    var mlatitude: String? = "";
    var mlongitude: String? = "";
    var selectNumberDay: Int = 0
    var delayDaywise: Int = 0
    var userSelectDaysDelay: Any? = "No Delay"
    var userSelectNumberDay: Any? = "1 Day"
    var mcategories: String? = "36"
    var msub_categories: String? = "186"
    lateinit var personData: LoginDataResponse
    lateinit var mPotluckFoodDataResponse: PotluckFoodDataResponse
    private var mContext: Context? = null
    var mResults: ArrayList<String> = arrayListOf()
    //-----------------------onPotlwekFoodApiSuccessResult----------------------------------------------//
    override fun onAddPotluckSuccess(mString: String) {
        // Log.e("onAddPotluckSuccess","onAddPotluckSuccess")
        hideProgressDialog()
        toast(mString)
        onBackPressed()
    }

    //---------------------------------------------------------------------------------------------------//
    override fun onAddPotluckFailureResponse(mString: String) {
        // Log.e("onAddPotluckFailureResponse","onAddPotluckFailureResponse")
        hideProgressDialog()
        toast(mString)
    }

    //---------------------------------------------------------------------------------------------------//
    override fun onPotlwekFoodApISuccess(message: List<PotluckFoodDataResponse?>?, mesg: String) {
        hideProgressDialog()
        //Toast.makeText(applicationContext,"Successfully",Toast.LENGTH_SHORT).show()
    }

    //---------------------------------------------------------------------------------------------------//
    override fun onPotlwekFoodApiSuccessResult(message: String, apiCallMethod: String) {
        hideProgressDialog()
        //Toast.makeText(applicationContext,"Successfully",Toast.LENGTH_SHORT).show()
        if (apiCallMethod == "success") {
            this!!.toast(Utils.getText(mContext!!, StringConstant.post_edited_successfully))
            onBackPressed()
        } else {
            this!!.toast(message)
        }
    }

    //---------------------------------------------------------------------------------------------------//
    override fun onPotlwekFoodDetailApiSuccessResult(message: String, status: Boolean) {
        hideProgressDialog()
        //Toast.makeText(applicationContext,"Failed",Toast.LENGTH_SHORT).show()
    }

    //---------------------------------------------------------------------------------------------------//
    override fun onPotlwekFoodDetailApiFailureResult(message: String) {
        hideProgressDialog()
    }

    //-----------------------Define-mAddPotluckPresenter----------------------------------------------//
    private val mAddPotluckPresenter: PotluckFoodPresenter by lazy {
        PotluckFoodPresenter(this@AddPotluckActivity)
    }

    //-----------------------Define-validate-Method---------------------------------------------------//
    private fun validate(): Boolean {
        val Title = EditText_Title.text.toString().trim()
        val Description = EditText_Description.text.toString().trim()
        /* val ChangeListingDays = tvChangeListingDays.text.toString().trim()
         val ChangeListingDayswise = tvChangeDaysDelay.text.toString().trim()
         val ChangeListingLocation = tvGetLocation.text.toString().trim()*/
        if (Title == null || Title.equals("")) {
            // toast(Utils.getText(this, StringConstant.myprofile_firstname_alert))
            EditText_Title.setFocusable(true);
            EditText_Title.setFocusableInTouchMode(true);
            EditText_Title.requestFocus();
            EditText_Title.setError(Utils.getText(this, StringConstant.str_potluck_title_validation))
            return false
        } else if (Description == null || Description.equals("")) {
            // toast(Utils.getText(this, StringConstant.myprofile_lastname_alert))
            EditText_Description.setFocusable(true);
            EditText_Description.setFocusableInTouchMode(true);
            EditText_Description.requestFocus();
            EditText_Description.setError(Utils.getText(this, StringConstant.str_potluck_description_validation))
            return false
        } else if (selectNumberDay == 0) {
            // toast(Utils.getText(this, StringConstant.myprofile_write_yourself_min50))
            tvChangeListingDays.setFocusable(true);
            tvChangeListingDays.setFocusableInTouchMode(true);
            tvChangeListingDays.requestFocus();
            tvChangeListingDays.setError(Utils.getText(this, StringConstant.str_potluck_days_validation))
            return false
        }
//        else if (delayDaywise == -1) {
//            // toast(Utils.getText(this, StringConstant.myprofile_write_yourself_min50))
//            tvChangeDaysDelay.setFocusable(true);
//            tvChangeDaysDelay.setFocusableInTouchMode(true);
//            tvChangeDaysDelay.requestFocus();
//            tvChangeDaysDelay.setError(Utils.getText(this, StringConstant.str_potluck_days_validation))
//            return false
//        }
        else if (mlatitude == "" || mlongitude == "") {
            // toast(Utils.getText(this, StringConstant.myprofile_write_yourself_min50))
            tvPickupLocation.setFocusable(true);
            tvGetLocation.setFocusableInTouchMode(true);
            tvGetLocation.requestFocus();
            tvGetLocation.setError(Utils.getText(this, StringConstant.str_potluck_location_validation))
            return false
        } else if (mResults == null && mResults.size <= 0) {
            toast(Utils.getText(this, StringConstant.str_potluck_image_validation))
            return false
        }
        return true
    }

    //-----------------------Define-ChangeDateWise-Method---------------------------------------------//
    private fun ChangeDateWise() {
        showDateWisePopupMenu()
    }

    //-----------------------Define-OpenChangeDate-Method---------------------------------------------//
    private fun OpenChangeDate() {
        showPopupMenu()
    }

    //-----------------------Define-ChangeLocation-Method---------------------------------------------//
    private fun ChangeLocation() {
        //Log.i(javaClass.name,"======================AddPotluckActivity")
        val builder = PlacePicker.IntentBuilder()
        startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
    }

    //-----------------------Define-showPopupMenu-Method---------------------------------------------//
    private fun showPopupMenu() {
        val popup = PopupMenu(this, tvChangeListingDays)
        var DateChangeValue = 1
        while (DateChangeValue < 29) {
            println(DateChangeValue)
            //popup.getMenu().add(Menu.NONE, DateChangeValue, 1, "");
            if (DateChangeValue == 1)
                popup.getMenu().add(1, DateChangeValue, DateChangeValue, DateChangeValue.toString() + " " + Utils.getText(mContext, StringConstant.str_poluck_list_day));
            else
                popup.getMenu().add(1, DateChangeValue, DateChangeValue, DateChangeValue.toString() + " " + Utils.getText(mContext, StringConstant.str_poluck_list_days));
            DateChangeValue++
        }
        this.menu = popup.menu
        popup.show()
        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                selectNumberDay = item.getItemId()
                //Log.e("ValueSelected", selectNumberDay.toString())
                userSelectNumberDay = selectNumberDay.toString() + " Day"
                tvListingDay.text = Utils.getText(mContext, StringConstant.str_potluck_list) + " " + selectNumberDay + " " + Utils.getText(mContext, StringConstant.str_poluck_list_day)
//                if (selectNumberDay == 1) {
//                    userSelectNumberDay = selectNumberDay.toString() + " Day"
//                    tvListingDay.text = Utils.getText(mContext, StringConstant.str_potluck_list) + " " + selectNumberDay + " " + Utils.getText(mContext, StringConstant.str_poluck_list_day)
//                } else {
//                    userSelectNumberDay = selectNumberDay.toString() + " Days"
//                    tvListingDay.text = Utils.getText(mContext, StringConstant.str_potluck_list) + " " + selectNumberDay + " " + Utils.getText(mContext, StringConstant.str_poluck_list_days)
//                }
                return false
            }
        })
    }

    //-----------------------Define-showDateWisePopupMenu-Method---------------------------------------------//
    private fun showDateWisePopupMenu() {
        val popup = PopupMenu(this, tvChangeDaysDelay)
        var DateChangeValue = 0
        while (DateChangeValue < 30) {
            println(DateChangeValue)
            if (DateChangeValue == 0)
                popup.getMenu().add(1, DateChangeValue, DateChangeValue, Utils.getText(mContext, StringConstant.str_potluck_nodelay));
            else if (DateChangeValue == 1)
                popup.getMenu().add(1, DateChangeValue, DateChangeValue, DateChangeValue.toString() + " " + Utils.getText(mContext, StringConstant.str_poluck_list_day));
            else
                popup.getMenu().add(1, DateChangeValue, DateChangeValue, DateChangeValue.toString() + " " + Utils.getText(mContext, StringConstant.str_poluck_list_day));
            DateChangeValue++
        }
        this.menu = popup.menu
        popup.show()
        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                delayDaywise = item.getItemId()
                //Log.e("ValueSelectedDay", delayDaywise.toString() + "day")
                if (delayDaywise == 0) {
                    userSelectDaysDelay = "No Delay"
                    tvDaysDelay.text = Utils.getText(mContext, StringConstant.str_potluck_immediately)
                } else if (delayDaywise == 1) {
                    userSelectDaysDelay = delayDaywise.toString() + " Day"
                    tvDaysDelay.text = Utils.getText(mContext, StringConstant.str_potluck_list) + " " + delayDaywise + " " + Utils.getText(mContext, StringConstant.str_potluck_immediately)
                } else {
                    userSelectDaysDelay = delayDaywise.toString() + " Days"
                    tvDaysDelay.text = Utils.getText(mContext, StringConstant.str_potluck_list) + " " + delayDaywise + " " + Utils.getText(mContext, StringConstant.str_potluck_immediately)
                }
                return false
            }
        })
    }

    //-----------------------------Define-onCreate-Method---------------------------------------------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_potluck)
        tvChangeListingDays.setOnClickListener(this)
        tvChangeDaysDelay.setOnClickListener(this)
        tvGetLocation.setOnClickListener(this)
        setToolbar()
    }

    //-----------------------------Define-companion object-Method--------------------------------------//
    companion object {
        fun newMainIntent(Context: Context): Intent? {
            val intent = Intent(Context, AddPotluckActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    //-----------------------------Define-onResume-Method---------------------------------------------//
    override fun onResume() {
        super.onResume()
        initViews()
    }

    //-----------------------------Define-initViews-Method---------------------------------------------//
    private fun initViews() {
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        toolbar_title.text = Utils.getText(this, StringConstant.str_food)
        TV_Title.text = Utils.getText(this, StringConstant.tv_title)
        TV_Description.text = Utils.getText(this, StringConstant.str_tv_description)
        btn_submit_req.text = Utils.getText(this, StringConstant.str_potluck_submit)
        ivClickImage.text = Utils.getText(this, StringConstant.str_potluck_photo)
        EditText_Title.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val length =EditText_Title.length()
                val convert = length.toString()
                txt_food_count30.text = convert
            }

            override fun afterTextChanged(s: Editable) {}
        })

        EditText_Description.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val length =EditText_Description.length()
                val convert = length.toString()
                txt_counts30.text = convert
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    //-----------------------------Define-onMapReady-Method---------------------------------------------//
    override fun onMapReady(googleMap: GoogleMap?) {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json))
            if (!success) {
            }
        } catch (e: Resources.NotFoundException) {
        } finally {
            // Position the map's camera near Sydney, Australia.
            if (googleMap != null) {
                googleMap.addMarker(MarkerOptions()
                        .position(LatLng(mPotluckFoodDataResponse!!.Lat!! as Double, mPotluckFoodDataResponse.Long!! as Double))
                        .title(mPotluckFoodDataResponse.title))
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(mPotluckFoodDataResponse!!.Lat!! as Double, mPotluckFoodDataResponse.Long!! as Double)))
                // Zoom out to zoom level 10, animating with a duration of 2 seconds.
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10F), 2000, null)
            }
        }
    }

    //-----------------------------Define-setToolbar-Method---------------------------------------------//
    private fun setToolbar() {
        ivClickImage.setOnClickListener(this)
        ivFoodImage.setOnClickListener(this)
        btn_submit_req.setOnClickListener(this)
    }

    //-----------------------------Define-takePicture-Method---------------------------------------------//
    fun selectImage() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library", "Cancel")
        var builder: AlertDialog.Builder = AlertDialog.Builder(this@AddPotluckActivity)
        builder.setTitle("Add Photo!")
        builder.setItems(items) { dialog, item ->
            if (items[item].equals("Take Photo")) {
                cameraIntent()
            } else if (items[item].equals("Choose from Library")) {
                galleryIntent()
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss()
            }
        }
        builder.show()
    }

    //---------------------------------------------------------------------------------------------------//
    private fun cameraIntent() {
        val outputFileUri = getCaptureImageOutputUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
        /* if (outputFileUri != null)
             intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)*/
        startActivityForResult(intent, CAMERA_RESULT)
    }

    //---------------------------------------------------------------------------------------------------//
    fun getImagePathFromInputStreamUri(uri: Uri): String {
        var inputStream: InputStream? = null
        var filePath: String? = null

        if (uri.authority != null) {
            try {
                inputStream = contentResolver.openInputStream(uri) // context needed
                val photoFile = Util.createTemporalFileFrom(this@AddPotluckActivity, inputStream)
                filePath = photoFile.getPath()
            } catch (e: FileNotFoundException) {
                // log
            } catch (e: IOException) {
                // log
            } finally {
                try {
                    inputStream!!.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

        return filePath!!
    }
//---------------------------------------------------------------------------------------------------//
    /**
     * Get URI to image received from capture by camera.
     */
    private fun getCaptureImageOutputUri(): Uri {
        var outputFileUri: Uri? = null
        val getImage = externalCacheDir
        if (getImage != null) {
            outputFileUri = Uri.fromFile(File(getImage.path, "profile.png"))
        }
        return outputFileUri!!
    }

    //---------------------------------------------------------------------------------------------------//
    private fun galleryIntent() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT//
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALLARY_RESULT)
    }

    //---------------------------------------------------------------------------------------------------//
    fun getImageContentUri(imageFile: File): Uri {
        val filePath = imageFile.absolutePath
        val cursor = contentResolver?.query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                arrayOf(MediaStore.Images.Media._ID),
                MediaStore.Images.Media.DATA + "=? ",
                arrayOf(filePath), null)
        if (cursor != null && cursor.moveToFirst()) {
            var id: Int = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID))
            val baseUri = Uri.parse("content://media/external/images/media")
            return Uri.withAppendedPath(baseUri, "" + id)
        } else {
            if (imageFile.exists()) {
                var values: ContentValues = ContentValues()
                values.put(MediaStore.Images.Media.DATA, filePath)
                return contentResolver!!.insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!
            } else {
                return null!!
            }
        }
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        var bytes: ByteArrayOutputStream = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        var path: String = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    /**
     * Get the URI of the selected image from [.getPickImageChooserIntent].

     * Will return the correct URI for camera and gallery image.

     * @param data the returned pushNotificationDataResponse of the activity result
     */
    fun getPickImageResultUri(data: Intent?): Uri? {
        var isCamera = true
        if (data != null) {
            val action = data.action
            isCamera = action != null && action == MediaStore.ACTION_IMAGE_CAPTURE
        }
        if (isCamera) {
            return getCaptureImageOutputUri()
        } else {
            return data?.data
        }
    }

    //-----------------------------Define-onClick-Method---------------------------------------------//
    override fun onClick(v: View?) {
        when (v) {
            tvChangeListingDays -> {
                OpenChangeDate()
            }
            tvChangeDaysDelay -> {
                ChangeDateWise()
            }
            tvGetLocation -> {
                ChangeLocation()
            }
            ivClickImage -> {
//                takePicture()
                selectImage()

            }
            ivFoodImage -> {
//                takePicture()
                selectImage()

            }
//-----------------------------Define-btn_submit_req-Method-----------------------------------------//
            btn_submit_req -> {
                if (!validate()) {
                    hideProgressDialog()
                    println("Device")
                } else {
                    if (Utils.haveNetworkConnection(this@AddPotluckActivity)) {
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@AddPotluckActivity)
                        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                        val map = hashMapOf<String, RequestBody>()

                        val requestBodyUserId = Utils.createPartFromString(personData.user_id!!)
                        val requestBodytitle = Utils.createPartFromString(EditText_Title.text.toString().trim())
                        val requestBodydescription = Utils.createPartFromString(EditText_Description.text.toString().trim() + "")
                        val requestBodylatitude = Utils.createPartFromString(mlatitude.toString().trim())
                        val requestBodylongitude = Utils.createPartFromString(mlongitude.toString().trim())
                        val requestBodyChangeDaysDelay = Utils.createPartFromString(userSelectNumberDay.toString().trim())
                        val requestBodyChangeDayswise = Utils.createPartFromString(userSelectDaysDelay.toString().trim())

                        val requestBodycategory = Utils.createPartFromString(mcategories.toString().trim())
                        val requestBodysubcategory = Utils.createPartFromString(msub_categories.toString().trim())
                        val requestBodyFood = Utils.createPartFromString("Food")

                        map.put("user_id", requestBodyUserId)
                        map.put("title", requestBodytitle)
                        map.put("description", requestBodydescription)
                        map.put("latitude", requestBodylatitude)
                        map.put("longitude", requestBodylongitude)
                        map.put("expiry_days", requestBodyChangeDaysDelay)
                        map.put("days", requestBodyChangeDayswise)
                        map.put("post_type", requestBodyFood)
                        map.put("categories", requestBodycategory)
                        map.put("sub_categories", requestBodysubcategory)

                        mAddPotluckPresenter.PotluckDataAPI(this@AddPotluckActivity, map, mResults)
                        //Toast.makeText(applicationContext,"ImageUpload Failed",Toast.LENGTH_SHORT).show()
                    } else {
                        //Toast.makeText(applicationContext,"Failed",Toast.LENGTH_SHORT).show()
                        hideProgressDialog()
                        showSnackBar(Utils.getText(mContext!!, StringConstant.str_check_internet))
                    }
                }
            }
        }
    }

    //-----------------------------Define-onActivityResult-Method---------------------------------------//
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Log.e("requestCode ","" + requestCode)
        var bitmap: Bitmap
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PLACE_PICKER_REQUEST -> {
                    if (requestCode === PLACE_PICKER_REQUEST) {
                        if (resultCode === RESULT_OK) {
                            val place = PlacePicker.getPlace(data, this)
                            mlatitude = String.format("%.7f", place.latLng.latitude)
                            mlongitude = String.format("%.7f", place.latLng.longitude)
                            //Log.e("mlatitude", mlatitude)
                            //Log.e("mlongitude", mlongitude)
                            //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show()
                        }
                    }
                }

                CAMERA_RESULT -> {
                    if (resultCode == Activity.RESULT_OK) {
                        var out: File = File(getFilesDir(), "newImage.jpg")
                        if (!out.exists()) {
                            toast("Error while capturing image")
                            return;
                        }
                        val realPath = out.getAbsolutePath()
                        mResults.add(0, realPath!!)

                        if (realPath.isEmpty()) {
                            ivFoodImage.setImageResource(R.drawable.queuelist_place_holder);
                        } else {

                            Picasso.get().load("file://" + realPath).into(ivFoodImage)
                        }

/*
                Glide.with(activity)
                        .load("file://" + realPath)
//                            .centerCrop().fitCenter()
                        .centerCrop()
                        .placeholder(R.drawable.iv_add_post_bg)
                        .error(R.drawable.iv_add_post_bg)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(ivFoodImage)
*/
                    }
                }
                GALLARY_RESULT -> {
                    if (resultCode == Activity.RESULT_OK) {
                        if (getPickImageResultUri(data) != null) {
                            val realPath = ImageFilePath.getPath(this@AddPotluckActivity, data?.data)
                            mResults.add(0, realPath!!)
                            if (realPath.isEmpty()) {
                                ivFoodImage.setImageResource(R.drawable.queuelist_place_holder);
                            } else {

                                Picasso.get().load("file://" + realPath).into(ivFoodImage)
                            }

/*
                    Glide.with(activity)
                            .load("file://" + realPath)
//                            .centerCrop().fitCenter()
                            .centerCrop()
                            .placeholder(R.drawable.iv_add_post_bg)
                            .error(R.drawable.iv_add_post_bg)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(ivFoodImage)
*/

                        } else {
                            val bitmap: Bitmap
                            tv_photo.visibility = View.INVISIBLE
                            if (null == data?.extras?.get("pushNotificationDataResponse")) {
                                return
                            }
                            bitmap = data?.extras?.get("pushNotificationDataResponse") as Bitmap
//                            myBitmap = bitmap
                            var bytes: ByteArrayOutputStream = ByteArrayOutputStream()
//                            myBitmap!!.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
                            var destination: File = File(Environment.getExternalStorageDirectory(),
                                    "${System.currentTimeMillis()}.jpg")
                            var fo: FileOutputStream
                            try {
                                destination.createNewFile()
                                fo = FileOutputStream(destination)
                                fo.write(bytes.toByteArray())
                                fo.close()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            if (ivFoodImage != null) {
                                Picasso.get().load("file://" + destination).placeholder(R.drawable.iv_add_post_bg).into(ivFoodImage)

/*
                        Glide.with(activity)
                                .load("file://" + destination)
//                                .centerCrop().fitCenter()
                                .centerCrop()
                                .placeholder(R.drawable.iv_add_post_bg)
                                .error(R.drawable.iv_add_post_bg)
                                .into(ivFoodImage)
*/
                            }
                            mResults.add(0, destination.toString())
                        }
                    }
                }

            }
        }
    }

    //-----------------------------Define-getPath-Method----------------------------------------------//
    fun getPath(uri: Uri): String? {
        try {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            //Log.e("OK 1", "" + projection)
            val cursor = managedQuery(uri, projection, null, null, null)
            //Log.e("OK 2", "" + cursor!!)
            if (cursor == null) {
                return null
            }
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            //Log.e("OK 3", "" + column_index)
            cursor!!.moveToFirst()
            //Log.e("OK 4", "" + cursor!!.getString(column_index))
            return cursor!!.getString(column_index)
        } catch (e: Exception) {
            Toast.makeText(this, "Image is too big in resolution please try again", Toast.LENGTH_LONG).show()
            return null
        }
    }
//-----------------------------End----------------------------------------------------------------//
}
