package com.tetoota.potluck

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.customviews.CustomServiceAddAlert
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.message.MessagingFoodActivity
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_potlwekactivity_food.*
import kotlinx.android.synthetic.main.activity_potlwekactivity_food.iv_user
import kotlinx.android.synthetic.main.activity_potlwekactivity_food.tv_avg_review
import kotlinx.android.synthetic.main.activity_potlwekactivity_food.tv_quality1
import kotlinx.android.synthetic.main.activity_potlwekactivity_food.tv_title
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

class PotluckFoodActivity : BaseActivity(),
        PotluckFoodContract.View,
        OnMapReadyCallback,
        View.OnClickListener, ServiceContract.View, CustomServiceAddAlert.IDialogListener, ProfileCompletionDialog.IDialogListener {
    override fun onApiFailureResult(message: String, isServerError: Boolean) {
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
    }

    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@PotluckFoodActivity)
    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this@PotluckFoodActivity)) {
                val intent = ProfileDetailActivity.newMainIntent(this@PotluckFoodActivity)
                ActivityStack.getInstance(this@PotluckFoodActivity)
                startActivity(intent)
            } else {
                // TODO CHECK NET
                showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    override fun onYes(param: String, message: String) {
        val intent = AddPostRequestActivity.newMainIntent(this)
        intent!!.putExtra("Tab", "serviceTab")
        ActivityStack.getInstance(this!!)
        startActivity(intent)
        finish()
    }

    override fun onWishListApiSuccessResult(myServiceList: List<Any?>?, message: String) {
        Log.e("myServiceList", myServiceList?.size.toString())
        mList = myServiceList as List<ServicesDataResponse>

        if (myServiceList != null) {
            Log.e("sizeeeeeeeee ", "" + myServiceList.size)
            serviceCount = myServiceList!!.size

        } else {
            serviceCount = 0
        }
    }

    override fun onAddPotluckSuccess(mString: String) {
    }

    override fun onAddPotluckFailureResponse(mString: String) {
    }

    //----------------------------------onPotluckFoodApISuccess-----------------------------------------//
    override fun onPotlwekFoodApISuccess(message: List<PotluckFoodDataResponse?>?, mesg: String) {
    }

    override fun onPotlwekFoodApiSuccessResult(message: String, apiCallMethod: String) {
    }

    override fun onPotlwekFoodDetailApiSuccessResult(message: String, status: Boolean) {
        hideProgressDialog()
    }

    override fun onPotlwekFoodDetailApiFailureResult(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        hideProgressDialog()
    }

    private var serviceCount: Int = 0
    private var mList: List<ServicesDataResponse>? = null
    //----------------------------------Define-Public-Variable----------------------------------------//
    lateinit var personData: LoginDataResponse
    lateinit var mPotluckFoodDataResponse: PotluckFoodDataResponse
    //----------------------------------Define-onCreate-Method----------------------------------------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_potlwekactivity_food)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        btn_requestlisting.setOnClickListener(this)
    }

    //----------------------------------Define-onResume-Method----------------------------------------//
    override fun onResume() {
        super.onResume()
        initViews()
        getDataFromPreviousActivity()
    }

    //----------------------------------Define-initViews-Method----------------------------------------//
    private fun initViews() {
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        toolbar_title.text = Utils.getText(this, StringConstant.str_food)
        Availilty.text = Utils.getText(this, StringConstant.str_potluck_availibility)
        TextView_description.text = Utils.getText(this, StringConstant.str_potluck_description)
        btn_requestlisting.text = Utils.getText(this, StringConstant.str_potluck_requestlisting)
        tv_quality1.text = Utils.getText(this, StringConstant.str_quality)
        tv_avg_review.text = Utils.getText(this, StringConstant.str_friendlines)
        tv_avg_counts.text = Utils.getText(this, StringConstant.str_tv_avg)
        tv_response_times.text = Utils.getText(this, StringConstant.str_responsetime)
        tv_friendlines.text = Utils.getText(this, StringConstant.str_friendlines)
    }

    //----------------------------------Define-getDataFromPreviousActivity-Method---------------------//
    private fun getDataFromPreviousActivity() {
        mPotluckFoodDataResponse = getIntent().getParcelableExtra("data");
        if (mPotluckFoodDataResponse.image != null && !mPotluckFoodDataResponse.image!!.isEmpty()) {
            Picasso.get().load(mPotluckFoodDataResponse.image!!).placeholder(R.drawable.place_holder).error(R.drawable.place_holder).into(image_potlwek)
        } else {
            image_potlwek.setImageResource(R.drawable.place_holder)
        }
        if (mPotluckFoodDataResponse.profileImage != null && !mPotluckFoodDataResponse.profileImage!!.isEmpty()) {
            Picasso.get().load(mPotluckFoodDataResponse.profileImage!!).placeholder(R.drawable.place_holder).error(R.drawable.place_holder).into(iv_user)
        } else {
            iv_user.setImageResource(R.drawable.place_holder);
        }
        tv_title.text = mPotluckFoodDataResponse.user_first_name + " " + mPotluckFoodDataResponse.user_last_name
        Userdate.text = mPotluckFoodDataResponse.creation_date
        TextView_Date.text = mPotluckFoodDataResponse.expiry_days
        description.text = mPotluckFoodDataResponse.content
        tv_avg_review.text = mPotluckFoodDataResponse.reviews

        var rev = Utils.getText(this, StringConstant.home_rating_review)
        //  tv_avg_review.text = mPotluckFoodDataResponse?.reviews!!.toString().substring(0, 4)
        tv_avg_counts.text = "( " + mPotluckFoodDataResponse?.reviews + " " + rev + " )"

        if (mPotluckFoodDataResponse.user_first_name!!.isEmpty()) {
            tv_title.text = "No Data Available"
        } else {
            tv_title.text = mPotluckFoodDataResponse.user_first_name + " " + mPotluckFoodDataResponse.user_last_name
        }
        if (mPotluckFoodDataResponse.creation_date!!.isEmpty()) {
            Userdate.text = "No Data Available"
        } else {
            Userdate.text = mPotluckFoodDataResponse.creation_date
        }
        if (mPotluckFoodDataResponse.expiry_days!!.isEmpty()) {
            TextView_Date.text = "No Data Available"
        } else {
            TextView_Date.text = mPotluckFoodDataResponse.expiry_days
        }
        if (mPotluckFoodDataResponse.content!!.isEmpty()) {
            description.text = "No Data Available"
        } else {
            description.text = mPotluckFoodDataResponse.content
        }
        if (mPotluckFoodDataResponse.reviews!!.isEmpty()) {
            tv_avg_review.text = "No Data Available"
        } else {
//            String.format("%.2f", mPotluckFoodDataResponse.reviews!!.toDouble())
            tv_avg_review.text = String.format("%.2f", mPotluckFoodDataResponse.review_avg!!.toDouble()) + "(" + mPotluckFoodDataResponse.total_usercount + " Review)"
        }
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)

        mServicePresenter.getServicesData(this, "ServicesProducts", 1, personData.user_id.toString())

    }

    //---------------------------------------------------Define-onMapReady-Method---------------------//
    override fun onMapReady(googleMap: GoogleMap?) {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json))
            if (!success) {
            }
        } catch (e: Resources.NotFoundException) {
        } finally {
            // Position the map's camera near Sydney, Australia.
            if (googleMap != null) {
                googleMap.addMarker(MarkerOptions()
                        .position(LatLng(mPotluckFoodDataResponse!!.Lat!! as Double, mPotluckFoodDataResponse.Long!! as Double))
                        .title(mPotluckFoodDataResponse.title))
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(mPotluckFoodDataResponse!!.Lat!! as Double, mPotluckFoodDataResponse.Long!! as Double)))
                // Zoom out to zoom level 10, animating with a duration of 2 seconds.
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10F), 2000, null)
            }
        }
    }

    //-----------------------------------------onClick------------------------------------------------//
    override fun onClick(p0: View?) {
        when (p0) {
            btn_requestlisting -> {
                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@PotluckFoodActivity)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                if (personData.first_name == null && personData.first_name.equals("") || personData.last_name == null || personData.last_name.equals("")
                        && personData.profile_image == null || personData.profile_image.equals("")) {
                    ProfileCompletionDialog(this@PotluckFoodActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this@PotluckFoodActivity, getString(R.string.err_msg_mobile_number_limit)).show()
                } else if (serviceCount <= 0) {
                    CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this, getString(R.string.err_msg_mobile_number_limit)).show()
                    return
                } else {
                    val intent = Intent(this, MessagingFoodActivity::class.java)
                    intent.putExtra("mPotluckFoodDataResponse", mPotluckFoodDataResponse)
                    intent.putExtra("productType", "food")
                    startActivity(intent)
                }
            }
        }
    }
//---------------------------------------------------End------------------------------------------//
}
