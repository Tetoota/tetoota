package com.tetoota.customviews
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.gson.Gson
import com.tetoota.R
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.profile_complete_dialog_layout.*

/**
 * Created by charchit.kasliwal on 07-06-2017.
 */
class ProfileCompletionDialog : Dialog {
    private val iDialogListener: IDialogListener
    constructor(context: Context?, dialogID : Int, iDialogListener: IDialogListener, message : String) : super(context!!, R.style.newDialog) {
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        window!!.attributes.windowAnimations = R.style.DialogAnimation
        setContentView(R.layout.profile_complete_dialog_layout)
        this.iDialogListener = iDialogListener
    }

    private fun setMultiLanguageText() {
        btn_yes.text = Utils.getText(context,StringConstant.update_now)
        btn_no.text = Utils.getText(context,StringConstant.cancel)
        tv_complete_profile.text = Utils.getText(context,StringConstant.str_complete_profile)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setDataOnUI()
        setMultiLanguageText()
        btn_yes.setOnClickListener(View.OnClickListener {
            dismiss()
            iDialogListener.onProfileData("","Yes")
        })

        btn_no.setOnClickListener(View.OnClickListener {
            dismiss()
            iDialogListener.onProfileData("","No")
        })
    }

    private fun setDataOnUI() {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
        val personData  = Gson().fromJson(json, LoginDataResponse::class.java)
        if(personData.first_name != "") tv_name.text = personData.first_name
        else tv_name.text = "Welcome user"
        if(personData.designation != "") tv_designation.text = personData.designation
        else tv_designation.visibility = View.GONE
        tv_profile_per.text = "${personData.complete_percentage}% Completed"
        if(personData.address != "") tv_address.text = personData.address
        else tv_address.visibility = View.GONE
        if(personData.complete_percentage!!.toInt() >= 100) btn_layout.visibility = View.GONE
        else btn_layout.visibility = View.VISIBLE


        var profileRemain = (100.minus(personData.complete_percentage.toInt()))
        profile_complete.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, java.lang.Float.parseFloat(personData.complete_percentage.toString()))
        profile_complete1.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, java.lang.Float.parseFloat(profileRemain.toString()))
        profile_complete.setBackgroundResource(R.color.color_green_color)
        profile_complete1.setBackgroundResource(R.color.color_grey_color)
        profile_complete.setTextColor(Utils.getColor(context, R.color.color_green_color))
        profile_complete1.setTextColor(Utils.getColor(context, R.color.color_grey_color))
       Glide.with(context)
                .load(Utils.getUrl(context, personData.profile_image!!) )
                .placeholder(R.drawable.lohgo)
                .error(R.drawable.lohgo)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .centerCrop()
                .dontAnimate()
                .into(iv_user)
    }

    interface IDialogListener {
        /**
         * @param param the param value
         * *
         * @param message the message on yess press
         */
        fun onProfileData(param: String, message: String)
    }
}