package com.tetoota.customviews

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.main.MainActivity
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.review_dialog_layout.*
import org.jetbrains.anko.startActivity

/**
 * Created on 15//06/2019.
 */
class ThankYouDialog : Dialog {

    constructor(context: Context?, dialogID: Int, message: String) : super(context!!, R.style.newDialog) {
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        window!!.attributes.windowAnimations = R.style.DialogAnimation
        setContentView(R.layout.thank_you_dialog_layout)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setMultiLanguageText()

        btn_yes.setOnClickListener(View.OnClickListener {
            dismiss()
            ActivityStack.cleareAll()
            context.startActivity<MainActivity>()
        })
        iv_close.setOnClickListener(View.OnClickListener {
            dismiss()
        })
    }

    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
//        tv_qualityService.text = Utils.getText(context, StringConstant.str_quality)
//        tv_responseTime.text = Utils.getText(context, StringConstant.str_responsetime)
//        tv_friendliness.text = Utils.getText(context, StringConstant.str_friendlines)
        btn_yes.text = Utils.getText(context, StringConstant.tetoota_ok)
        tv_name.text = Utils.getText(context, StringConstant.str_payement_success)
//        tv_rate_trade.text = Utils.getText(context, StringConstant.review_message)
//        tv_name.text = Utils.getText(context, StringConstant.review_title)
    }


}