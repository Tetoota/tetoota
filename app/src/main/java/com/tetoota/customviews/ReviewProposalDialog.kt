package com.tetoota.customviews
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.tetoota.R
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.review_dialog_layout.*
import org.jetbrains.anko.toast

/**
 * Created by charchit.kasliwal on 17-08-2017.
 */
class ReviewProposalDialog : Dialog {
    private val iDialogListener: IRatingDialogListener

    constructor(context: Context?, dialogID : Int, iDialogListener: IRatingDialogListener, message : String) : super(context!!, R.style.newDialog) {
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        window!!.attributes.windowAnimations = R.style.DialogAnimation
        setContentView(R.layout.review_dialog_layout)
        this.iDialogListener = iDialogListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setMultiLanguageText()
        rb_response_time.rating = 0.0f

        btn_yes.setOnClickListener(View.OnClickListener {
            if(validation()) {
                dismiss()
                iDialogListener.onSubmitPressed("", "", rb_response_time.rating.toString(),
                        rb_quality_service.rating.toString(),
                        rb_friendlines.rating.toString(), et_message.text.toString().trim())
            }
        })
        iv_close.setOnClickListener(View.OnClickListener {
            dismiss()
        })
    }

    fun validation(): Boolean{
        if(et_message.text.toString().isNullOrEmpty()) {
            context.toast("Please enter review message")
            return false
        }
        return true
    }

    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
        tv_qualityService.text = Utils.getText(context,StringConstant.str_quality)
        tv_responseTime.text = Utils.getText(context,StringConstant.str_responsetime)
        tv_friendliness.text = Utils.getText(context,StringConstant.str_friendlines)
        btn_yes.text = Utils.getText(context,StringConstant.str_submit)
        tv_rate_trade.text = Utils.getText(context,StringConstant.review_message)
        tv_name.text = Utils.getText(context,StringConstant.review_title)
    }

    /**
     * Created by charchit.kasliwal on 31-05-2017.
     */
    interface IRatingDialogListener {
        /**
         * @param param the param value
         * *
         * @param message the message on yess press
         */
        fun onSubmitPressed(param: String, message: String, mResponseTime : String, mQualityService: String, mFriendLines : String,
                            mReviewMesg : String)

        fun onCancelPressed()
    }
}