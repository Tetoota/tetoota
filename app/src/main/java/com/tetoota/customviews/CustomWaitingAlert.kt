package com.tetoota.customviews

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.tetoota.R
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.custom_sign_out_layout.*
import org.jetbrains.anko.onClick

class CustomWaitingAlert : Dialog {
    private val iDialogListener: IDialogListener
    private var dialogId: Int = 0
    private var message: String = ""

    constructor(context: Context?, dialogID: Int, iDialogListener: IDialogListener, message: String) : super(context!!, R.style.newDialog) {
        this.dialogId = dialogID
        this.message = message
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        window!!.attributes.windowAnimations = R.style.DialogAnimation
        setContentView(R.layout.custom_waiting_layout)
        this.iDialogListener = iDialogListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setMultiLanguageText()

        btn_yes.onClick {
            dismiss()
        }
    }

    interface IDialogListener {
        /**
         * @param param the param value
         * *
         * @param message the message on yess press
         */
        fun onYes(param: String, message: String)

        fun dialogAlert(dialogID: Int, message: String) {}

    }

    private fun setMultiLanguageText() {
        tv_subtitle.text = Utils.getText(context, StringConstant.st_msg_noproduct)
        btn_yes.text = Utils.getText(context, StringConstant.signout_yes)
    }
}