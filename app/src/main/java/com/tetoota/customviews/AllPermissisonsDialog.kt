package com.tetoota.customviews.fonts

import android.app.Dialog
import android.content.Context
import android.view.WindowManager
import com.tetoota.R
import com.tetoota.listener.IDialogListener
import kotlinx.android.synthetic.main.permissions_dialog.*
import org.jetbrains.anko.onClick

/**
 * Created by charchit.kasliwal on 01-06-2017.
 */
open class AllPermissisonsDialog : Dialog{
    val mContext : Context
    var iDialogListener : IDialogListener
    /**
     * Secondary Constructor
     */
    constructor(context: Context?, mContext: Context, dialogId : Int, iDialogListener: IDialogListener) : super(context!!) {
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        setContentView(R.layout.permissions_dialog)
        setCancelable(false)
        this.mContext = mContext
        this.iDialogListener = iDialogListener
        btn_ok.onClick {
            dismiss()
            println("Button ok Click")
            iDialogListener.onYesPress("key", "success")
        }
        btn_cancel.onClick {
            dismiss()
            println("Button Cancel Click")
            iDialogListener.onYesPress("key", "fail")
        }
    }

}