package com.tetoota.customviews

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import com.tetoota.R
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.custom_sign_out_layout.*
import org.jetbrains.anko.onClick


/**
 * Created by charchit.kasliwal on 07-06-2017.
 */
class CustomSignOutDialog : Dialog {
    private val iDialogListener: IDialogListener
    private var dialogId : Int = 0
    private var message : String = ""
    constructor(context: Context?, dialogID : Int, iDialogListener: IDialogListener, message : String) : super(context!!, R.style.newDialog) {
        this.dialogId = dialogID
        this.message = message
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        window!!.attributes.windowAnimations = R.style.DialogAnimation
        setContentView(R.layout.custom_sign_out_layout)
        this.iDialogListener = iDialogListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setMultiLanguageText()
        btn_no.onClick {
            dismiss()
            println("Btn ok")
        }

        btn_yes.onClick {
            dismiss()
            println("Btn ok")
            if(dialogId == Constant.DIALOG_DELETE_SERVICE || dialogId == Constant.DIALOG_EDIT_SERVICE){
              iDialogListener.dialogAlert(dialogId,message)
            }else{
                iDialogListener.onYes("Ok",message)
            }
        }
    }

    interface IDialogListener {
        /**
         * @param param the param value
         * *
         * @param message the message on yess press
         */
        fun onYes(param: String, message: String)
        fun dialogAlert(dialogID: Int, message: String){}

    }

    private fun setMultiLanguageText() {
        if(dialogId == Constant.DIALOG_EDIT_SERVICE){
            tv_subtitle.text = Utils.getText(context,StringConstant.edit_post_alert)
        }else if(dialogId == Constant.DIALOG_DELETE_SERVICE){
            tv_subtitle.text = Utils.getText(context,StringConstant.delete_post_alert)
        }else{
            tv_subtitle.text = Utils.getText(context,StringConstant.sign_out_alert)
        }
        btn_yes.text = Utils.getText(context,StringConstant.signout_yes)
        btn_no.text = Utils.getText(context,StringConstant.signout_no)
    }
}