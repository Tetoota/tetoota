package com.tetoota.customviews

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.tetoota.R
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.start_conversation_dialog_layout.*
import org.jetbrains.anko.toast

class StartConversationDialog : Dialog {
    private val iDialogListener: IDialogListener
    private var mServiceId: String? = ""
    private var mContext: Context? = null
    private var mMessage: String? = null
    private var mIntent: Intent? = null
    private var m: String? = ""
    private var exchangePostType: String? = ""


    constructor(context: Context?, dialogID: Int, mServiceId: String, iDialogListener: IDialogListener,
                message: String, intent: Intent, exchangePostType: String) : super(context!!, R.style.newDialog) {
        this.mServiceId = mServiceId
        this.mContext = context
        this.mMessage = message
        this.mIntent = intent
        this.exchangePostType = exchangePostType

        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        window!!.attributes.windowAnimations = R.style.DialogAnimation
        setContentView(R.layout.start_conversation_dialog_layout)
        this.iDialogListener = iDialogListener


        m = mIntent!!.getStringExtra("productType")
        //   Log.e("mmmmmmmmmmmmmmmm", "" + m)

        if (this.mMessage == "report") {
            tv_name.text = Utils.getText(context, StringConstant.str_report_service)
            et_message.hint = Utils.getText(context, StringConstant.str_report_service_detail)
            tv_complete_profile.text = Utils.getText(context, StringConstant.report_this_message)
            btn_includeProposal.text = Utils.getText(context, StringConstant.str_submit)
            btn_sendMessage.text = Utils.getText(context, StringConstant.report_back)
        } else {
            setMultiLanguageText()
        }
    }

    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
        tv_name.text = Utils.getText(context, StringConstant.start_converse_title)
        et_message.hint = Utils.getText(context, StringConstant.start_converse_placeholder)
        tv_complete_profile.text = Utils.getText(context, StringConstant.start_converse_message)
        btn_sendMessage.text = Utils.getText(context, StringConstant.start_converse_send)
        btn_includeProposal.text = Utils.getText(context, StringConstant.start_converse_include)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btn_sendMessage.setOnClickListener(View.OnClickListener {
            if (this.mMessage == "report") {
                dismiss()
                iDialogListener.onYesPress("", "", "", "")
            } else {
                if (et_message.text.toString().trim().isNotEmpty()) {
                    var s = et_message.text.toString().trim()
                  if (s.length > 1)
                  {
                      dismiss()
                      iDialogListener.onYesPress(et_message.text.toString().trim(), "sendMessage", mServiceId!!, et_message.text.toString().trim())
                  }else{
                      mContext?.toast("Please enter mimimum 2 character")
                  }

                } else {
                    mContext?.toast("Please include message")
                }
            }
        })

        if (m == "wishlist" || exchangePostType == "Wishlist") {
            btn_includeProposal.visibility = View.VISIBLE
            tv_complete_profile.visibility = View.VISIBLE

            btn_includeProposal.setOnClickListener(View.OnClickListener {
//                    dismiss()
//                    iDialogListener.onYesPress("", "report", mServiceId!!, et_message.text.toString().trim())
//
///*
//                if (et_message.text.toString().trim().isNotEmpty()) {
//                    dismiss()
//                    iDialogListener.onYesPress("", "report", mServiceId!!, et_message.text.toString().trim())
//                } else {
//                    mContext?.toast(Utils.getText(context,StringConstant.please_specify_your_reason))
//                }
//*/
//                } else {
                    dismiss()
                    iDialogListener.onYesPress("", "includeProposal", mServiceId!!, et_message.text.toString().trim())

/*
                if (et_message.text.toString().trim().isNotEmpty()) {
                    dismiss()
                    iDialogListener.onYesPress("", "includeProposal", mServiceId!!, et_message.text.toString().trim())
                } else {
                    mContext?.toast(Utils.getText(context,StringConstant.please_specify_your_reason))
                }
*/
//                }
            })
//            btn_sendMessage.setTextColor(Color.WHITE)
          //  btn_sendMessage.setBackgroundColor(Color.parseColor("#F6B498"))
            //btn_sendMessage.setBackgroundResource(R.drawable.btn_left_and_right_circular_clr_gray);
        } else {
            tv_complete_profile.visibility = View.VISIBLE
            btn_includeProposal.visibility = View.VISIBLE
            btn_includeProposal.setOnClickListener(View.OnClickListener {
                if (this.mMessage == "report") {
                    dismiss()
                    iDialogListener.onYesPress("", "report", mServiceId!!, et_message.text.toString().trim())

/*
                if (et_message.text.toString().trim().isNotEmpty()) {
                    dismiss()
                    iDialogListener.onYesPress("", "report", mServiceId!!, et_message.text.toString().trim())
                } else {
                    mContext?.toast(Utils.getText(context,StringConstant.please_specify_your_reason))
                }
*/
                } else {
                    dismiss()
                    iDialogListener.onYesPress("", "includeProposal", mServiceId!!, et_message.text.toString().trim())

/*
                if (et_message.text.toString().trim().isNotEmpty()) {
                    dismiss()
                    iDialogListener.onYesPress("", "includeProposal", mServiceId!!, et_message.text.toString().trim())
                } else {
                    mContext?.toast(Utils.getText(context,StringConstant.please_specify_your_reason))
                }
*/
                }
            })

        }

        rl_close.setOnClickListener(View.OnClickListener {
            dismiss()
            iDialogListener.onYesPress("", "", "", "")
        })

    }

    interface IDialogListener {
        /**
         * @param param the param value
         * *
         * @param message the message on yess press
         */
        fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String)
    }
}