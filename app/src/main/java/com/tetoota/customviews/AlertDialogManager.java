package com.tetoota.customviews;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.tetoota.R;
import com.tetoota.socialmedias.facebook.AlertDialogCallBack;

/**
 * Created by jitendra.nandiya on 22-11-2017.
 */

public class AlertDialogManager {
    private AlertDialogCallBack alertDialogCallBack ;
    private String dialogId ;
    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     *               - pass null if you don't want icon
     * */
    public void showAlertDialog(final Context context, String title, String message,
                                Boolean status, final boolean isFinishing, final AlertDialogCallBack alertDialogCallBack, String id) {
        this.alertDialogCallBack = alertDialogCallBack ;
        this.dialogId = id ;
        final Activity activity = (Activity) context;
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        if(status != null)
            // Setting alert dialog icon
            alertDialog.setIcon((status) ? R.drawable.twitter_icon : R.drawable.iv_tetoota_icon);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(isFinishing){
                    activity.setResult(activity.RESULT_CANCELED);
                    activity.finish();
                    alertDialogCallBack.onSuccess(dialogId, null);
                }
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
