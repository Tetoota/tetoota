package com.tetoota.login

import android.os.Parcel
import android.os.Parcelable
import com.tetoota.categories.CategoriesDataResponse

data class LoginDataResponse(
		val user_id: String? = null,
		val user_mobile_number: String? = null,
		private val deviceToken: String? = null,
		val auth_token: String? = null,
		val address: String? = null,
		val first_name: String? = null,
		val last_name: String? = null,
		val email: String? = null,
		val profile_image: String? = null,
		val designation: String? = null,
		val review_avg: String? = null,
		val response_time: String? = null,
		val quality_service: String? = null,
		private val device_type: String? = null,
		private val device_id: String? = null,
		var tetoota_points: String? = null,
		val all_notification: String? = null,
		val proposal_notification: String? = null,
		val chat_notification: String? = null,
		val is_visible: String? = null,
		var categories: ArrayList<CategoriesDataResponse?>? = null,
		val complete_percentage: String? = null,
		private val device_token: String? = null,
		var is_service: String? = null,
		val referral_string: String? = null,
		val email_verify: String? = null,
		val aadhar_verify: String? = null,
		var proposal_count : String? = null) : Parcelable {

	constructor(source: Parcel) : this(
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.createTypedArrayList(CategoriesDataResponse.CREATOR),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString(),
			source.readString()
	)

	override fun describeContents() = 0

	override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
		writeString(user_id)
		writeString(user_mobile_number)
		writeString(deviceToken)
		writeString(auth_token)
		writeString(address)
		writeString(first_name)
		writeString(last_name)
		writeString(email)
		writeString(profile_image)
		writeString(designation)
		writeString(review_avg)
		writeString(response_time)
		writeString(quality_service)
		writeString(device_type)
		writeString(device_id)
		writeString(tetoota_points)
		writeString(all_notification)
		writeString(proposal_notification)
		writeString(chat_notification)
		writeString(is_visible)
		writeTypedList(categories)
		writeString(complete_percentage)
		writeString(device_token)
		writeString(referral_string)
		writeString(proposal_count)
		writeString(email_verify)
		writeString(aadhar_verify)
		writeString(is_service)
	}

	companion object {
		@JvmField
		val CREATOR: Parcelable.Creator<LoginDataResponse> = object : Parcelable.Creator<LoginDataResponse> {
			override fun createFromParcel(source: Parcel): LoginDataResponse = LoginDataResponse(source)
			override fun newArray(size: Int): Array<LoginDataResponse?> = arrayOfNulls(size)
		}
	}
}