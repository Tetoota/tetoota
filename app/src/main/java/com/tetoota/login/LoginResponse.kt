package com.tetoota.login

import com.tetoota.network.errorModel.Meta

data class LoginResponse(
	val data: LoginDataResponse? = null,
	val meta: Meta? = null
)
