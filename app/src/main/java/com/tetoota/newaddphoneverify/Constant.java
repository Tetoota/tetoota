package com.tetoota.newaddphoneverify;

import com.tetoota.listener.ApiInterface;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Constant {

   // public static final String BASE_URL = "http://app.tetoota.com/tetoota";
    public static final String BASE_URL = "http://tetoota.com/api/v1/";

    public static ApiInterface retrofit = null;


    public static OkHttpClient getinfo() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.NONE);


        return new OkHttpClient.Builder().addInterceptor(logging).connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS).build();
    }

    public static final ApiInterface retrofitService = new Retrofit.Builder()
            .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(getinfo()).build().create(ApiInterface.class);


}
