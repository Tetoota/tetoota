package com.tetoota.newaddphoneverify

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.categories.CategoriesContract
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.categories.CategoriesPresenter
import com.tetoota.cropImage.Utility
import com.tetoota.fragment.help.ShowWebViewActivity
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_select_categories.*
import kotlinx.android.synthetic.main.error_layout.*

class EnterMobileNumber : BaseActivity(), View.OnClickListener, CategoriesContract.View {

    override fun onMobileSuccess(message: Int, mesgDesc: String) {
        hideProgressDialog()
        Log.e("ddddddddddddd ", "my Messageeeeeeeeeeeeeeeee" + mesgDesc)
        moveToNextScreen()
    }

    private var preferenceHelper: PreferenceHelper? = null

    var number: String? = null
    var context: Context? = null

    private val mCategoriesPresenter: CategoriesPresenter by lazy {
        CategoriesPresenter(this@EnterMobileNumber)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_mobile_number)

        context = this;

        initViews()
        btn_next.setOnClickListener(this@EnterMobileNumber)
        error_btn_retry.setOnClickListener(this@EnterMobileNumber)

    }

    private fun initViews() {

        preferenceHelper = PreferenceHelper(this)
        setMultiLanguageText()
    }

    private fun setMultiLanguageText() {

        btn_next.text = Utils.getText(this, StringConstant.str_next)

    }

    override fun onClick(v: View?) {
        when (v) {
            error_btn_retry -> {
                if (Utils.haveNetworkConnection(this@EnterMobileNumber)) {
                    showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
                    mCategoriesPresenter.getCategoriesData(this@EnterMobileNumber)
                } else {
                    showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
                }
            }
            btn_next -> {
                if (Utils.haveNetworkConnection(this@EnterMobileNumber)) {
                    val edtmobile = findViewById<EditText>(R.id.edittext_mobile) as EditText
                    var mobile = edtmobile.text
                    if (!mobile.isEmpty())
                    {
                        if (mobile.length>9)
                        {
                            showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
                            mCategoriesPresenter.enterMobile(this@EnterMobileNumber, edtmobile.text.toString())

                        }else{
                            Utility.showAlert(context, "Please enter 10 digits Mobile number!")
                        }

                    }else{
                        Utility.showAlert(context, "Please enter OTP!")
                    }


                } else {
                    showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
                }
            }
            tv_terms_text -> {
                val intent = ShowWebViewActivity.newMainIntent(this@EnterMobileNumber)
                ActivityStack.getInstance(this@EnterMobileNumber)
                //intent?.putExtra("webUrl", webPageUrl)
                intent?.putExtra("pageHeading", "Terms and Conditions")
                startActivity(intent)
            }
        }
    }

    /**
     * On Resume
     */
    override fun onResume() {
        super.onResume()
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, EnterMobileNumber::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String) {
        hideProgressDialog()
        hideErrorView()
    }

    override fun onCatergoriesFailureResult(message: String) {
        hideProgressDialog()
        showErrorView(message)
        showSnackBar(message)
    }


    override fun onLoginResult(message: Int, mesgDesc: String) {

    }

    override fun onDeepLinkingResult() {
        moveToNextScreen()
    }

    fun moveToNextScreen() {
        var mobile_number = number.toString()
        val intent = EnterOtp.newMainIntent(this)
        ActivityStack.getInstance(this@EnterMobileNumber)
        overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
        ActivityStack.cleareAll()
        startActivity(intent)
        finish()
        println("Success")
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            rl_maincontent.visibility = View.VISIBLE
            checkbox_layout.visibility = View.VISIBLE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    private fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            rl_maincontent.visibility = View.GONE
            checkbox_layout.visibility = View.GONE
        }
    }
}
