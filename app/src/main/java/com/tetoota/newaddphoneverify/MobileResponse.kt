package com.tetoota.newaddphoneverify

import com.tetoota.login.LoginDataResponse
import com.tetoota.network.errorModel.Meta

data class MobileResponse(
        val data: MobileDataResponse? = null,
        val meta: Meta? = null
)