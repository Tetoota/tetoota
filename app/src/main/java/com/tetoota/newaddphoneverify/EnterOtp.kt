package com.tetoota.newaddphoneverify

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.categories.CategoriesContract
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.categories.CategoriesPresenter
import com.tetoota.cropImage.Utility
import com.tetoota.fragment.help.ShowWebViewActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_select_categories.*
import kotlinx.android.synthetic.main.error_layout.*
import org.json.JSONObject

class EnterOtp : BaseActivity(), View.OnClickListener, CategoriesContract.View {


    override fun onMobileSuccess(message: Int, mesgDesc: String) {

        // moveToNextScreen()
    }

    private var preferenceHelper: PreferenceHelper? = null

    var number: String? = null
    var context: Context? = null

    private val mCategoriesPresenter: CategoriesPresenter by lazy {
        CategoriesPresenter(this@EnterOtp)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_otp_kotlin)

        context = this;

        initViews()
        btn_next.setOnClickListener(this@EnterOtp)
        error_btn_retry.setOnClickListener(this@EnterOtp)
        tv_terms_text.setOnClickListener(this@EnterOtp)


    }

    private fun initViews() {

        preferenceHelper = PreferenceHelper(this)
        setMultiLanguageText()
    }

    private fun setMultiLanguageText() {

        btn_next.text = Utils.getText(this, StringConstant.str_next)
        val tempString = Utils.getText(this, StringConstant.help_term_condition)
        val spanString = SpannableString(tempString)
        spanString.setSpan(UnderlineSpan(), 0, spanString.length, 0)
        spanString.setSpan(StyleSpan(Typeface.BOLD), 0, spanString.length, 0)
        spanString.setSpan(StyleSpan(Typeface.ITALIC), 0, spanString.length, 0)
        tv_terms_text.text = spanString
    }

    override fun onClick(v: View?) {
        when (v) {
            error_btn_retry -> {
                if (Utils.haveNetworkConnection(this@EnterOtp)) {
                    showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
                    mCategoriesPresenter.getCategoriesData(this@EnterOtp)
                } else {
                    showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
                }
            }
            btn_next -> {
                if (Utils.haveNetworkConnection(this@EnterOtp)) {
                    val edtotp = findViewById<EditText>(R.id.edittext_otp) as EditText
                    val referral_string = Utils.loadPrefrence(Constant.KEY_NAME, "", this)
                    // Log.e("referral_string ","**** " + referral_string)
                    //  mCategoriesPresenter.checkUserLoginFromAPI(this@SelectCategoriesActivity, number.toString(), resultnew, referral_string)
                    var otp = edtotp.text
                    if (!otp.isEmpty()) {
                        if (otp.length > 5) {
                            var mobile = Utils.loadPrefrence(Constant.MOBILE_NUMBER, "en", this@EnterOtp)
                            mCategoriesPresenter.mobileRegisterwithverify(this@EnterOtp, mobile, edtotp.text.toString(), "", referral_string)

                        } else {
                            Utility.showAlert(context, "Please enter 6 digits OTP!")
                        }

                    } else {
                        Utility.showAlert(context, "Please enter OTP!")
                    }


                } else {
                    showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
                }
            }
            tv_terms_text -> {
                val intent = ShowWebViewActivity.newMainIntent(this@EnterOtp)
                ActivityStack.getInstance(this@EnterOtp)
                //intent?.putExtra("webUrl", webPageUrl)
                intent?.putExtra("pageHeading", "Terms and Conditions")
                startActivity(intent)
            }
        }
    }

    /**
     * On Resume
     */
    override fun onResume() {
        super.onResume()
        if (Utils.haveNetworkConnection(this@EnterOtp)) {
            showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
            mCategoriesPresenter.getCategoriesData(this@EnterOtp)
        } else {
            showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
        }
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, EnterOtp::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String) {
        hideProgressDialog()
        hideErrorView()
    }

    override fun onCatergoriesFailureResult(message: String) {
        hideProgressDialog()
        showErrorView(message)
        showSnackBar(message)
    }


    override fun onLoginResult(message: Int, mesgDesc: String) {
        hideProgressDialog()
        if (message == Constant.API_SUCCESS_VALUE) {
            if (Utils.haveNetworkConnection(this)) {
                Log.e("TAGYOUR", mesgDesc)
                try {
                    showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    if (personData.user_id != null) {
                        mCategoriesPresenter.doDeepLinking(this@EnterOtp, Utils.loadPrefrence(Constant.USER_AUTH_TOKEN
                                , "", this@EnterOtp), personData.user_id!!
                                , Utils.getIPAddress(true))
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    hideProgressDialog()
                    Toast.makeText(this, "Phone number and otp does not match", Toast.LENGTH_SHORT).show()
                }

            } else {
                moveToNextScreen()
            }
        } else if (message == Constant.API_FAILURE_VALUE) {
            println("Failure")
        } else {
            println("Unkknown")
        }
    }

    override fun onDeepLinkingResult() {

        //  Log.e("fffffffffffffffffffffffffffff","")
        hideProgressDialog()
        moveToNextScreen()
    }

    fun moveToNextScreen() {
        val intent = MainActivity.newMainIntent(this)
        ActivityStack.getInstance(this@EnterOtp)
        overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
        ActivityStack.cleareAll()
        startActivity(intent)
        finish()
        println("Success")
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            rl_maincontent.visibility = View.VISIBLE
            checkbox_layout.visibility = View.VISIBLE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    private fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            rl_maincontent.visibility = View.GONE
            checkbox_layout.visibility = View.GONE
        }
    }
}
