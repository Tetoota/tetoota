package com.tetoota.newaddphoneverify;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class PreferenceHelper {
    public static String PREF_NAME = "TETOOTA";
    private SharedPreferences app_prefs;

    private static String PHONE_NUMBER = "phone_number";
    private Context context;




    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        this.context = context;
    }


    public void putPhoneNumber(String phonenumber) {
        Editor edit = app_prefs.edit();
        edit.putString(PHONE_NUMBER, phonenumber);
        edit.commit();
    }

    public String getphoneNumber() {
        return app_prefs.getString(PHONE_NUMBER, null);

    }


}


