package com.tetoota.newaddphoneverify

import android.os.Parcel
import android.os.Parcelable
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.login.LoginDataResponse


data class MobileDataResponse(
        val phone_number: String? = null) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString()


    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(phone_number)


    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MobileDataResponse> = object : Parcelable.Creator<MobileDataResponse> {
            override fun createFromParcel(source: Parcel): MobileDataResponse = MobileDataResponse(source)
            override fun newArray(size: Int): Array<MobileDataResponse?> = arrayOfNulls(size)
        }
    }
}