package com.tetoota.pointssummary


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson

import com.tetoota.R
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_points_spent.*
import org.jetbrains.anko.toast

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class PointsHoldFragment : Fragment(), PointsSummaryContract.View, PointsSummaryAdapter.INotifyActivityListener {
    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    private lateinit var mPointsSummaryAdapter : PointsSummaryAdapter
    var linearLayoutManager: LinearLayoutManager? = null
    val mPointsSummaryPresenter : PointsSummaryPresenter by lazy {
        PointsSummaryPresenter(this@PointsHoldFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater!!.inflate(R.layout.fragment_points_hold, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_pts_spent.setHasFixedSize(true)
        rv_pts_spent.itemAnimator = DefaultItemAnimator()

        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_pts_spent.layoutManager = linearLayoutManager
        mPointsSummaryAdapter = PointsSummaryAdapter(this.activity!!,iAdapterClickListener = this)
        rv_pts_spent.adapter = mPointsSummaryAdapter

        if(Utils.haveNetworkConnection(this.activity!!)){
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
            val personData  = Gson().fromJson(json, LoginDataResponse::class.java)
            mPointsSummaryPresenter.pointsSpentList(this.activity!!,"hold", personData.user_id!!)
        }else{
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
        }
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment PointsSpentFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): PointsHoldFragment {
            val fragment = PointsHoldFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onPointSpendSuccess(mString: String, mFavoriteList: ArrayList<PointSummaryDataResponse?>) {

        if (mFavoriteList.size != 0)
        {
            rl_nrf.visibility = View.GONE
        }else{
            rl_nrf.visibility = View.VISIBLE

        }

        mPointsSummaryAdapter.addAll(mFavoriteList as List<PointSummaryDataResponse?>)
        mPointsSummaryAdapter.notifyDataSetChanged()
    }

    override fun onPointSpentFailure(mString: String, isServerError: Boolean) {
    }

    override fun notifyActivity(pos: Int?, mServiceDataResp: ServicesDataResponse, mFav: Int, btnClick: String, mView: View) {
    }

    override fun notifyForListItemCount(count: Int?) {
    }
}// Required empty public constructor
