package com.tetoota.pointssummary.di

import android.app.Activity
import com.tetoota.pointssummary.data.Data
import com.tetoota.pointssummary.data.UserHoldPointsInteractor

/**
 * Created by abhinav.maurya on 01-02-2018.
 */
class MainPointSummaryPresenter : MainPointSummaryContract.Presenter, MainPointSummaryContract.MainPointsSummaryResult{

    var mPointSummaryView : MainPointSummaryContract.View

    val mPointsSpendInteract : UserHoldPointsInteractor by lazy {
        UserHoldPointsInteractor(this)
    }

    constructor(mPointSummaryView: MainPointSummaryContract.View) {
        this.mPointSummaryView = mPointSummaryView
    }


    override fun holdPointData(mActivity: Activity, user_id: String) {
        mPointsSpendInteract.getUserHoldPoints(mActivity, user_id)
    }

    override fun onUserHoldPointsAPiSuccess(mString: String, holdPointResponse: Data) {
        mPointSummaryView.onUserHoldPointsAPiSuccess(mString, holdPointResponse);
    }

    override fun onUserHoldPointsAPiFailure(mString: String, isServerError: Boolean) {
        mPointSummaryView.onUserHoldPointsAPiFailure(mString, isServerError)
    }


}