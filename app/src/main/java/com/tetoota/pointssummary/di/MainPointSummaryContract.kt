package com.tetoota.pointssummary.di

import android.app.Activity
import com.tetoota.pointssummary.data.Data

/**
 * Created by abhinav.maurya on 01-02-2018.
 */
class MainPointSummaryContract {

    interface View{
        fun onUserHoldPointsAPiSuccess(mString: String, holdPointResponse: Data)
        fun onUserHoldPointsAPiFailure(mString: String,isServerError: Boolean)
    }

    interface Presenter{
        fun holdPointData(mActivity : Activity, user_id : String)
    }

    interface MainPointsSummaryResult{
        fun onUserHoldPointsAPiSuccess(mString: String, holdPointResponse: Data) : Unit
        fun onUserHoldPointsAPiFailure(mString : String, isServerError: Boolean) : Unit
    }

}