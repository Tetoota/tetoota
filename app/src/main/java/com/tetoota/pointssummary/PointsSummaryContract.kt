package com.tetoota.pointssummary

import android.app.Activity

/**
 * Created by charchit.kasliwal on 20-06-2017.
 */
class PointsSummaryContract {
    interface View{
        fun onPointSpendSuccess(mString: String, mFavoriteList: ArrayList<PointSummaryDataResponse?>)
        fun onPointSpentFailure(mString: String,isServerError: Boolean)
    }

    interface Presenter{
        fun pointsSpentList(mActivity : Activity,history_type: String?, user_id : String)
    }

    interface PointsSummaryResult{
        fun onPointSpendSuccess(mString: String, mFavoriteList: ArrayList<PointSummaryDataResponse?>) : Unit
        fun onPointSpentFailure(mString : String, isServerError: Boolean) : Unit
    }
}