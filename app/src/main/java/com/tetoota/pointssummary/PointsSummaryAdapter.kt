package com.tetoota.pointssummary

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.point_summary_row.view.*

/**
 * Created by charchit.kasliwal on 26-10-2017.
 */
class PointsSummaryAdapter(val mContext: Context,
                           private var mPtsSummaryList: ArrayList<PointSummaryDataResponse> = ArrayList<PointSummaryDataResponse>(),
                           val iAdapterClickListener: INotifyActivityListener) : RecyclerView.Adapter<PointsSummaryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.point_summary_row, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        viewHolder.bindPointsSummaryData(viewHolder, p1,
                mPtsSummaryList[p1], iAdapterClickListener,mContext)
    }

    fun getLastPosition() = if (mPtsSummaryList.lastIndex == -1) 0 else mPtsSummaryList.lastIndex

    private fun add(result: Any?) {
        mPtsSummaryList.add(result as PointSummaryDataResponse)
        notifyItemInserted(mPtsSummaryList.size - 1)
    }

    fun addAll(moveResults: List<PointSummaryDataResponse?>) {
        for (result in moveResults) {
            add(result)
        }
    }

    override fun getItemCount(): Int {
        return mPtsSummaryList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val btn_total_tps_pt = view.btn_total_tps_pt!!
        private val tv_name = view.tv_name!!
        private val tv_description = view.tv_description!!
        private val tv_timeleft = view.tv_timeleft!!
        private val tv_service_provider = view.tv_service_provider!!
        private val tv_for = view.tv_for!!
        fun bindPointsSummaryData(viewHolder: ViewHolder?, p1: Int, s: PointSummaryDataResponse,
                                  iAdapterClickListener: INotifyActivityListener,context: Context) {


            tv_for.text = Utils.getText(context,StringConstant.str_for)

            viewHolder?.btn_total_tps_pt?.text = s.tetoota_points.toString()
            viewHolder?.tv_name?.text = s.title.toString()
            viewHolder?.tv_description?.text = s.fullname.toString()
            viewHolder?.tv_timeleft?.text = s.point_transaction_date.toString()
            if (s.history_type == "")
            {
                tv_service_provider.text = Utils.getText(context,StringConstant.tv_service_provider)+" :"
                viewHolder?.tv_description?.text = s.fullname.toString()
            }else{
                tv_service_provider.text = Utils.getText(context,StringConstant.str_from)
                viewHolder?.tv_description?.text = s.fullname.toString()
            }
        }
    }

    interface INotifyActivityListener {
        fun notifyActivity(pos: Int?, mServiceDataResp: ServicesDataResponse, mFav: Int, btnClick: String, mView: View): Unit
        fun notifyForListItemCount(count: Int?)
    }
}