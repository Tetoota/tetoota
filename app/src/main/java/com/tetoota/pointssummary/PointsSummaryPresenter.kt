package com.tetoota.pointssummary

import android.app.Activity


/**
 * Created by charchit.kasliwal on 20-06-2017.
 */
class PointsSummaryPresenter : PointsSummaryContract.Presenter, PointsSummaryContract.PointsSummaryResult{


    var mPointSummaryView : PointsSummaryContract.View

    val mPointsSpendInteract : PointsSummaryInteractor by lazy {
        PointsSummaryInteractor(this)
    }

    constructor(mPointSummaryView: PointsSummaryContract.View) {
        this.mPointSummaryView = mPointSummaryView
    }


    override fun pointsSpentList(mActivity: Activity, history_type: String?, user_id : String) {
        mPointsSpendInteract.pointsSpentList(mActivity,history_type, user_id)
    }

    override fun onPointSpendSuccess(mString: String, mFavoriteList: ArrayList<PointSummaryDataResponse?>) {
        mPointSummaryView.onPointSpendSuccess(mString,mFavoriteList)
    }


    override fun onPointSpentFailure(mString: String, isServerError: Boolean) {
        mPointSummaryView.onPointSpentFailure(mString,isServerError)
    }


}