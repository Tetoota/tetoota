package com.tetoota.pointssummary
import com.tetoota.network.errorModel.Meta
data class PointsSummaryResponse(
		val data: ArrayList<PointSummaryDataResponse?>? = null,
		val meta: Meta? = null
)
