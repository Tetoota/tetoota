package com.tetoota.logout
import com.tetoota.network.errorModel.Meta

/**
 * Created by charchit.kasliwal on 11-09-2017.
 */
class LogoutApiResponse(code: String, status: String, message: String) : Meta(code, status, message) {
}