package com.tetoota.logout

import com.google.gson.annotations.SerializedName

data class LogoutDataResponse(
	@field:SerializedName("data")
	val data: List<Any?>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)