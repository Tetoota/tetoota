package com.tetoota.google_translation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.google_translation.GoogleLanguagesResponce.LanguagesItem
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_google_translate.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

/**
 * Created by jitendra.nandiya on 03-01-2018.
 */
class GoogleTranslateActivity : BaseActivity(), GoogleTranslateContract.View, View.OnClickListener {
    var languagesList: ArrayList<String>? = null
    var mGoogleLanguagesList: List<LanguagesItem?>? = null
    var selectedPosition: Int = 0
    var apiCallCount: Int = 0
    var text: String = ""
    var sourceLangaugeCode = "en"

    override fun onGoogleLanguagesSuccess(mString: String, mLanguagesList: List<LanguagesItem?>) {
        try {
            languagesList = ArrayList<String>()
            mGoogleLanguagesList = mLanguagesList
            for (languagesItem in mLanguagesList) {
                languagesList!!.add(languagesItem?.name!!)
            }
            spinner.setAdapter(ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_dropdown_item,
                    languagesList!!))
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            apiCallCount--
            if(apiCallCount==0)
            progress_view.visibility = View.GONE
        }

        spinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(arg0: AdapterView<*>,
                                        arg1: View, position: Int, arg3: Long) {
                selectedPosition = position
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
                // TODO Auto-generated method stub
            }
        })
    }

    private val mGoogleTranslatePresenter: GoogleTranslatePresenter by lazy {
        GoogleTranslatePresenter(this@GoogleTranslateActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_translate)
        initToolbar()
        btnTranslate.setOnClickListener(this)
        textView.text = Utils.getText(this,StringConstant.please_select_language)
        btnTranslate.text = Utils.getText(this,StringConstant.google_translate_btn_title)
        tv_translatedText.text = Utils.getText(this,StringConstant.translated_text_placeholder)
        text = intent.getStringExtra("translateText")
        tv_text.text = text
        getGoogleLanguages()
        getGoogleDetectApiCall(text)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnTranslate -> {
                if (mGoogleLanguagesList!!.size > 0) {
                    var target: String = mGoogleLanguagesList!!.get(selectedPosition)?.language.toString()
                    getGoogleTranslateApiCall(text, target)
                }
            }
        }
    }

    fun getGoogleLanguages() {
        if (Utils.haveNetworkConnection(this)) {
            apiCallCount++
            progress_view.visibility = View.VISIBLE
            mGoogleTranslatePresenter.getGoogleLanguages(Constant.API_KEY_GOOGLE_TRASLATE, "en")
        } else {
            toast(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    fun getGoogleTranslateApiCall(q : String, target: String) {
        if (Utils.haveNetworkConnection(this)) {
            apiCallCount++
            progress_view.visibility = View.VISIBLE
            mGoogleTranslatePresenter.getGoogleTranslateText(Constant.API_KEY_GOOGLE_TRASLATE,
                    q,
                    sourceLangaugeCode, target, "text")
        } else {
            toast(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    fun getGoogleDetectApiCall(q : String){
        if (Utils.haveNetworkConnection(this)) {
            apiCallCount++
            progress_view.visibility = View.VISIBLE
            mGoogleTranslatePresenter.getGoogleDetectLanguage(Constant.API_KEY_GOOGLE_TRASLATE, q)
        } else {
            toast(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, GoogleTranslateActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onGoogleTranslateSuccess(mString: String, mResult: String) {
        apiCallCount--
        if(apiCallCount==0)
            progress_view.visibility = View.GONE
        tv_translatedText.text = mResult
    }

    override fun onGoogleTranslateFailureResponse(mString: String, isServerError: Boolean) {
        apiCallCount--
        if(apiCallCount==0)
            progress_view.visibility = View.GONE
        toast(mString +" : "+isServerError)
    }

    override fun onGoogleDetectLanguageSuccess(mString: String, mLanguageCode: String) {
        apiCallCount--
        if(apiCallCount==0) {
            progress_view.visibility = View.GONE
            sourceLangaugeCode = mLanguageCode
        }
    }

    override fun onGoogleDetectLanguageFailureResponse(mString: String, isServerError: Boolean) {
        apiCallCount--
        if(apiCallCount==0)
            progress_view.visibility = View.GONE
        toast(mString +" : "+isServerError)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar_title.text = Utils.getText(this,StringConstant.googleTranslateTitle)
        iv_close.onClick { onBackPressed() }
    }

    /*override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflater = menuInflater
        inflater.inflate(R.menu.product_detail_menu, menu)
        return true
    }*/

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home ->{
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * On Back Pressed Method Call
     */
    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@GoogleTranslateActivity)
        finish()
    }
}