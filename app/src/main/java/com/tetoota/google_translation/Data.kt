package com.tetoota.google_translation

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("translations")
	val translations: List<TranslationsItem?>? = null
)