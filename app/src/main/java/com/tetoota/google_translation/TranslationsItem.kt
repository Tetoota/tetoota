package com.tetoota.google_translation

import com.google.gson.annotations.SerializedName

data class TranslationsItem(

	@field:SerializedName("translatedText")
	val translatedText: String? = null
)