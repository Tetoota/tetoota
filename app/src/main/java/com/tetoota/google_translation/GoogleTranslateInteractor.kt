package com.tetoota.google_translation

import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.couponcode.Meta
import com.tetoota.google_translation.GoogleDetectLanguage.DetectLanguageResponse
import com.tetoota.google_translation.GoogleDetectLanguage.DetectionsItemItem
import com.tetoota.google_translation.GoogleLanguagesResponce.GoogleLanguagesResponse
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

/**
 * Created by jitendra.nandiya on 03-01-2018.
 */
class GoogleTranslateInteractor {
    var call: Call<GoogleTranslateResponse>? = null
    var callLanguages: Call<GoogleLanguagesResponse>? = null
    var callDetectLanguage: Call<DetectLanguageResponse>? = null
    var mInviteFriendsListener: GoogleTranslateContract.GoogleTranslateApiResult

    constructor(mFavoriteListener: GoogleTranslateContract.GoogleTranslateApiResult) {
        this.mInviteFriendsListener = mFavoriteListener
    }

    fun getGoogleTranslate(key: String, q: String, source: String, target: String, format: String) {
        call = TetootaApplication.getHeaderGoogleTranslate()
                .getGoogleTranslateText(key, q, source, target, format)
        call!!.enqueue(object : Callback<GoogleTranslateResponse> {
            override fun onResponse(call: Call<GoogleTranslateResponse>?,
                                    response: Response<GoogleTranslateResponse>?) {
                var mGoogleTranslateData: GoogleTranslateResponse? = response?.body()
                try {
                    if (response?.code() == 200) {
                        if (null !== response.body()?.data?.translations?.get(0) && !(response.body()?.data!!.translations?.get(0)?.equals(""))!!) {
                            if (mGoogleTranslateData != null) {
                                mInviteFriendsListener.onGoogleTranslateApiSuccess("success", (mGoogleTranslateData.data?.translations?.get(0) as TranslationsItem).translatedText.toString())
                            }
                        } else {
                            mInviteFriendsListener.onGoogleTranslateApiFailure("Error", false)
                        }
                    } else {
                        var errorMessage: String = ""
                        try {
                            if (response?.body() == null) {
                                val jObjError = JSONObject(response?.errorBody()?.string())
                                val achualdata: JSONObject = jObjError.getJSONObject("error")
                                val gson = Gson()
                                var mError: Meta = Meta()
                                val error = gson.fromJson(achualdata.toString(), Meta::class.java)
                                errorMessage = error.message!!
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } finally {
                            mInviteFriendsListener.onGoogleTranslateApiFailure(errorMessage, true)
                        }

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<GoogleTranslateResponse>?, t: Throwable?) {
                mInviteFriendsListener.onGoogleTranslateApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun getGoogleLanguages(key: String, target: String) {
        callLanguages = TetootaApplication.getHeaderGoogleTranslate()
                .getGoogleLanguages(key, target)
        callLanguages!!.enqueue(object : Callback<GoogleLanguagesResponse> {

            override fun onResponse(call: Call<GoogleLanguagesResponse>?,
                                    response: Response<GoogleLanguagesResponse>?) {
                var mGoogleLanguages: GoogleLanguagesResponse? = response?.body()
                try {
                    if (response?.code() == 200) {
                        if (null !== response.body()?.data?.languages && response.body()?.data?.languages!!.size > 0) {
                            if (mGoogleLanguages != null) {
                                mInviteFriendsListener.onGoogleLanguagesApiSuccess("success", mGoogleLanguages.data?.languages!!)
                            }
                        } else {
                            mInviteFriendsListener.onGoogleTranslateApiFailure("Error", false)
                        }
                    } else {
                        var errorMessage: String = ""
                        try {
                            if (response?.body() == null) {
                                val jObjError = JSONObject(response?.errorBody()?.string())
                                val achualdata: JSONObject = jObjError.getJSONObject("error")
                                val gson = Gson()
                                var mError: Meta = Meta()
                                val error = gson.fromJson(achualdata.toString(), Meta::class.java)
                                errorMessage = error.message!!
//                                mInviteFriendsListener.onGoogleTranslateApiFailure(errorMessage, true)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } finally {
                            mInviteFriendsListener.onGoogleTranslateApiFailure(errorMessage, true)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<GoogleLanguagesResponse>?, t: Throwable?) {
                mInviteFriendsListener.onGoogleTranslateApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun getGoogleDetectLanguage(key: String, q: String) {
        callDetectLanguage = TetootaApplication.getHeaderGoogleTranslate()
                .getGoogleDetectLanguage(key, q)
        callDetectLanguage!!.enqueue(object : Callback<DetectLanguageResponse> {

            override fun onResponse(call: Call<DetectLanguageResponse>?,
                                    response: Response<DetectLanguageResponse>?) {
                try {
                    var mGoogleLanguages: DetectLanguageResponse? = response?.body()
                    var languageList: List<List<DetectionsItemItem?>?> = mGoogleLanguages?.data?.detections!!
                    if (response?.code() == 200) {
                        if (null != languageList.size > 0) {
                            if (mGoogleLanguages != null) {
                                mInviteFriendsListener.onGoogleDetectLanguageApiSuccess("success", getDetectLanguageCode(languageList))
                            }
                        } else {
                            mInviteFriendsListener.onGoogleDetectLanguageApiFailure("Error", false)
                        }
                    } else {
                        var errorMessage: String = ""
                        try {
                            if (response?.body() == null) {
                                val jObjError = JSONObject(response?.errorBody()?.string())
                                val achualdata: JSONObject = jObjError.getJSONObject("error")
                                val gson = Gson()
                                var mError: Meta = Meta()
                                val error = gson.fromJson(achualdata.toString(), Meta::class.java)
                                errorMessage = error.message!!
//                                mInviteFriendsListener.onGoogleTranslateApiFailure(errorMessage, true)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } finally {
                            mInviteFriendsListener.onGoogleDetectLanguageApiFailure(errorMessage, true)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<DetectLanguageResponse>?, t: Throwable?) {
                mInviteFriendsListener.onGoogleDetectLanguageApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun getDetectLanguageCode(languageList: List<List<DetectionsItemItem?>?>): String {
        var confidence: Double = 0.0
        var languageCode: String = ""
        for (detectionsItemItem in languageList) {
            if (detectionsItemItem?.get(0)?.confidence!! > confidence) {
                confidence = detectionsItemItem.get(0)?.confidence!!
                languageCode = detectionsItemItem.get(0)?.language!!
            }
        }
        return languageCode
    }
}