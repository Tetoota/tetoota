package com.tetoota.google_translation.GoogleDetectLanguage

import com.google.gson.annotations.SerializedName

data class DetectionsItemItem(

	@field:SerializedName("confidence")
	val confidence: Double? = null,

	@field:SerializedName("isReliable")
	val isReliable: Boolean? = null,

	@field:SerializedName("language")
	val language: String? = null
)