package com.tetoota.google_translation.GoogleDetectLanguage

import com.google.gson.annotations.SerializedName

data class DetectLanguageResponse(

	@field:SerializedName("data")
	val data: Data? = null
)