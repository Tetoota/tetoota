package com.tetoota.google_translation.GoogleDetectLanguage

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("detections")
	val detections: List<List<DetectionsItemItem?>?>? = null
)