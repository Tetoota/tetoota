package com.tetoota.google_translation.GoogleLanguagesResponce

import com.google.gson.annotations.SerializedName

data class Data(
	@field:SerializedName("languages")
	val languages: List<LanguagesItem?>? = null
)