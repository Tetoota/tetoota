package com.tetoota.details

import android.app.Activity
import android.app.AlertDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.content.res.Resources.NotFoundException
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.annotation.VisibleForTesting
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.Toast
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.share.Sharer
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import com.facebook.share.widget.ShareDialog
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.addrequest.*
import com.tetoota.appintro.ProductDetailSliderAdapter
import com.tetoota.customviews.CustomServiceAddAlert
import com.tetoota.customviews.CustomSignOutDialog
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.customviews.StartConversationDialog
import com.tetoota.fragment.dashboard.HomeContract
import com.tetoota.fragment.dashboard.HomePresenter
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.InboxContract
import com.tetoota.fragment.inbox.InboxPresenter
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.google_translation.GoogleTranslateActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.message.GetStageResponse
import com.tetoota.message.MessagingActivity
import com.tetoota.proposal.IncludeProposalsActivity
import com.tetoota.proposal.ProposalByIdData
import com.tetoota.reviews.ViewReviewsActivity
import com.tetoota.service_product.FullImageActivity
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.Constant.constant.ANDROID_MIN_VERSION
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.toast
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL

class ProductDetailActivity : BaseActivity(), OnMapReadyCallback, ViewPager.OnPageChangeListener,
        ProfileCompletionDialog.IDialogListener, CustomServiceAddAlert.IDialogListener,
        View.OnClickListener, StartConversationDialog.IDialogListener, AddRequestContract.View,
        HomeContract.View, InboxContract.View, CustomSignOutDialog.IDialogListener, ServiceContract.View, ViewProductContract.View {
    override fun onGetStageSuccess(message: String, chatStatus: String, data: ProposalByIdData) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onGetStageFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //    var isServiceAdded: Boolean = false
    private var serviceCount: Int = 0
    private var mList: List<ServicesDataResponse>? = null


    override fun onViewProductSuccess(mString: String, mFavoriteList: String) {
    }

    override fun onViewProductFailureResponse(mString: String, isServerError: Boolean) {
    }

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {
    }

    override fun onInvalidTextAPiFailure(message: String?) {
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
    }

    override fun ongetTrendingFailure(message: String) {
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
    }

    override fun ongetIncompleteProposalFailure(message: String) {
    }

    // TODO: Rename and change types of parameters

    var s1: String? = ""
    var listType: String? = ""
    var exchangePostType: String? = ""
    private val EDITSERVICEPRODUCT: Int = 13
    private val INCLUDE_PROPOSAL_ACTIVITY: Int = 10
    var loginManager: LoginManager? = null
    lateinit var callbackManager: CallbackManager;
    var shareDialog: ShareDialog? = null
    private var menu: Menu? = null
    private var dotsCount: Int = 0
    lateinit var personData: LoginDataResponse
    private var dots = arrayOfNulls<ImageView>(dotsCount)
    var mServiceData: ServicesDataResponse? = null
    var editMenuItem: MenuItem? = null
    var deleteMenuItem: MenuItem? = null
    private var tetootaApplication: TetootaApplication? = null
    private var context: Context? = null
    lateinit var shortLink: Uri
    var link: String = "http://tetoota.com/service/?"
    var productType = ""
    private val mAdapter: ProductDetailSliderAdapter by lazy {
        ProductDetailSliderAdapter(this@ProductDetailActivity)
    }
    private val mInboxPresenter: InboxPresenter by lazy {
        InboxPresenter(this@ProductDetailActivity)
    }
    private val mViewProductPresenter: ViewProductPresenter by lazy {
        ViewProductPresenter(this@ProductDetailActivity)
    }

    private val mAddServicePresenter: AddRequestPresenter by lazy {
        AddRequestPresenter(this@ProductDetailActivity)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@ProductDetailActivity)
    }
    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@ProductDetailActivity)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflater = menuInflater
        inflater.inflate(R.menu.product_detail_menu, menu)
        inflater.inflate(R.menu.product_detail_menu, menu)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)
        tetootaApplication = this.getApplicationContext() as TetootaApplication
        context = applicationContext
        //  initToolbar()

        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)

        activityUIIntializer()
        // Initialize facebook SDK.
        FacebookSdk.sdkInitialize(getApplicationContext())
        validateAppCode()

        // Create a callbackManager to handle the login responses.
        callbackManager = CallbackManager.Factory.create()

        shareDialog = ShareDialog(this)
        // this part is optional
        shareDialog!!.registerCallback(callbackManager, callback)
        GoogleMapOptions().liteMode(true)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        iv_favorite_selected.setOnClickListener(this)
        btn_conversation.setOnClickListener(this)
        iv_share.setOnClickListener(this)
        tv_navigation.setOnClickListener(this)
        btn_view_reviews.setOnClickListener(this)
        tv_avg_review.setOnClickListener(this)
        tv_avg_count.setOnClickListener(this)
        tv_translatedText.setOnClickListener(this)
        tv_translatedTextDesc.setOnClickListener(this)
        tv_contentTranslatedText.setOnClickListener(this)
        tv_qualifications_degreeTranslate.setOnClickListener(this)
        tv_location_detail_degreeTranslate.setOnClickListener(this)
        iv_user.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        getDataFromPreviousActivity()
    }

    private fun getDataFromPreviousActivity() {
        //  pager_introduction.adapter = mAdapter
        //  pager_introduction.currentItem = 0
        listType = intent.getStringExtra("productType")
        if (intent.getStringExtra("productType") == "services") {
            mServiceData = intent.getParcelableExtra("mProductData")
        } else if (intent.getStringExtra("productType") == "product") {
            mServiceData = intent.getParcelableExtra("mProductData")
            //  Log.e("mServiceData ","" + mServiceData)
        } else if (intent.getStringExtra("productType") == "wishlist") {
            productType = getString(R.string.wish_list)
            mServiceData = intent.getParcelableExtra("mProductData")
            rl_badeges.visibility = View.GONE
        }


        mViewProductPresenter.getViewProductCode(this@ProductDetailActivity
                , "", mServiceData?.id.toString())

//          Log.e("fff", "" + mServiceData)

        for (i in (mServiceData?.all_images!!.indices - 1).reversed()) {
            (mServiceData?.all_images!! as ArrayList).removeAt(i)
        }

        //  Log.e("fffffffffffffffffff", "" + mServiceData?.is_favourite)
        if (mServiceData?.is_favourite == 1) {
            iv_favorite_selected.setImageResource(R.drawable.iv_wish)
        } else {
            iv_favorite_selected.setImageResource(R.drawable.iv_wish_un)
        }

        tv_content.text = mServiceData?.content
        tv_product_category.text = mServiceData?.content
        tv_point.text = Utils.getText(this, StringConstant.str_tetoota_points) + " " + mServiceData?.set_quote
        tv_service_providervalue.text = mServiceData?.user_first_name + " " + mServiceData?.user_last_name
        //  pager_introduction.addOnPageChangeListener(this@ProductDetailActivity)
        tv_productname.text = mServiceData?.title
        tv_qualifications_degree.text = mServiceData?.describe_yourself
        tv_service_virtually.text = Utils.getText(this, StringConstant.product_detail_quota) + ": " + mServiceData?.allowed_services + " | " + mServiceData?.tetoota_points + " " + Utils.getText(this, StringConstant.product_details_point) + " " + mServiceData?.set_quote

        if (mServiceData?.whatsapp == "True") {
            iv_call1.setBackgroundDrawable(resources.getDrawable(R.drawable.call_selected))
        } else {
            iv_call1.setBackgroundDrawable(resources.getDrawable(R.drawable.call_unselected))
        }
        if (mServiceData?.facebook == "True") {
            iv_fb1.setBackgroundDrawable(resources.getDrawable(R.drawable.facebook_selected))
        } else {
            iv_fb1.setBackgroundDrawable(resources.getDrawable(R.drawable.facebook_unselected))
        }
        if (mServiceData?.gmail == "True") {
            iv_email1.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_selected))
        } else {
            iv_email1.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_unselected))
        }
        if (mServiceData?.linkedin == "True") {
            iv_linkedin1.setBackgroundDrawable(resources.getDrawable(R.drawable.linkedin_selected))
        } else {
            iv_linkedin1.setBackgroundDrawable(resources.getDrawable(R.drawable.linkedin_unselected))
        }
        if (mServiceData?.instagram == "True") {
            iv_insta1.setBackgroundDrawable(resources.getDrawable(R.drawable.instagram_selected))
        } else {
            iv_insta1.setBackgroundDrawable(resources.getDrawable(R.drawable.instagram_unselected))
        }

        var rev = Utils.getText(this, StringConstant.home_rating_review)

        tv_avg_review.text = mServiceData?.reviews!!.toString()
        if (tv_avg_review.text.length >= 4) {
            tv_avg_review.text = tv_avg_review.text.substring(0, 3)
        }
        Log.e("e", "===================tv_avg_review" + tv_avg_review.text)
        tv_avg_count.text = "( " + mServiceData?.total_usercount + " " + rev + " )"

        if (mServiceData?.review_avg != null) {
            user_reviews1.rating = mServiceData?.review_avg!!.toFloat()
            //  Log.e("++++++++",""+ mServiceData?.quality_service)
            // Log.e("+++++",""+ mServiceData?.friendliness)

            quality_reviews1.rating = mServiceData?.quality_service!!.toFloat()
            friendlines_reviews1.rating = mServiceData?.friendliness!!.toFloat()
        }


        if (listType == "wishlist") {
            btn_points.visibility = View.GONE
            if (mServiceData?.virtual_service.toString() == "Yes") {
                tv_service1.text = Utils.getText(this, StringConstant.PD_I_perform_wishlist_virtually)
            } else {
                tv_service1.text = Utils.getText(this, StringConstant.wishlist_detail_no_virtual)

            }

        } else {
            btn_points.visibility = View.VISIBLE
            if (mServiceData?.virtual_service.toString() == "Yes") {
                tv_service1.text = Utils.getText(this, StringConstant.PD_I_perform_service_virtually)
            } else {
                tv_service1.text = Utils.getText(this, StringConstant.product_detail_no_virtual)

            }
            mServicePresenter.getServicesData(this, "ServicesProducts", 1, Utils.loadPrefrence(Constant.USER_ID, "", this@ProductDetailActivity).toString())

        }


        tv_trading_prefrencevalue.text = mServiceData?.virtual_service
        if (mServiceData?.trading_preference.toString() == "1") {
            tv_trading_prefrence.text = Utils.getText(this, StringConstant.add_service_fine_by_both)
            //  btn_points.visibility = View.VISIBLE
        } else if (mServiceData?.trading_preference.toString() == "2") {
            tv_trading_prefrence.text = Utils.getText(this, StringConstant.add_service_paid_by_point)
            //  btn_points.visibility = View.VISIBLE
        } else if (mServiceData?.trading_preference.toString() == "3") {
            tv_trading_prefrence.text = Utils.getText(this, StringConstant.add_service_exchange_service)
            // btn_points.visibility = View.VISIBLE
        }

        tv_total_pts.text = mServiceData?.tetoota_points.toString()

        tv_location_detail.text = "${mServiceData?.post_address}"

        if (mServiceData!!.profile_image!!.isEmpty()) {
            iv_user.setImageResource(R.drawable.user_placeholder);
        } else {
            Picasso.get().load(mServiceData!!.profile_image).into(iv_user)
        }

        if (mServiceData!!.image!!.isEmpty()) {
            pager_introduction.setImageResource(R.drawable.queuelist_place_holder);
        } else {
            Picasso.get().load(mServiceData!!.image).into(pager_introduction)
        }


        /*    if (!(mServiceData!!.all_images!!.isEmpty() || mServiceData!!.all_images == null)) {
                dotsCount = mServiceData!!.all_images!!.size
                setUiPageViewController()
                if (Utils.getNetworkType(this).equals("4") || Utils.getNetworkType(this).equals("wifi", true)) {
                    mAdapter.setElements(mServiceData?.all_images as ArrayList<String>)
                } else {
                    if (null != mServiceData?.all_images_thumb) {
                        mAdapter.setElements(mServiceData?.all_images_thumb as ArrayList<String>)
                    }
                }
                mAdapter.notifyDataSetChanged()
            }
            else {
                dotsCount = 1
                setUiPageViewController()
                val mDashboardSlider = ArrayList<String>()

                if (mServiceData!!.image != null && mServiceData!!.image_thumb != null) {
                    mDashboardSlider.add(0, Utils.getUrl(this, mServiceData!!.image!!, mServiceData!!.image_thumb!!, false))
                } else {
                    mDashboardSlider.add(0, Utils.getUrl(this, mServiceData!!.image!!))
                }
                mAdapter.setElements(mDashboardSlider)
                mAdapter.notifyDataSetChanged()
            }*/
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
        personData = Gson().fromJson(json, LoginDataResponse::class.java)
        if (mServiceData?.user_id.toString() == personData.user_id) {
            btn_conversation.visibility = View.GONE
        }
        initToolbar()
        setMultiLanguageText()
    }

    private fun activityUIIntializer() {
        iv_insta1.setOnClickListener(this)
        iv_linkedin1.setOnClickListener(this)
        iv_email1.setOnClickListener(this)
    }


    private fun initToolbar() {
        setSupportActionBar(toolbar)
        if (intent.getStringExtra("productType") == "services") {
            if (mServiceData!!.post_type.equals("Services", true) || mServiceData!!.post_type.equals("Trending", true)) {
                toolbar_title.text = Utils.getText(this, StringConstant.service_details_title)

            } else if (mServiceData!!.post_type == "Products") {
                toolbar_title.text = Utils.getText(this, StringConstant.product_details_title)

            } else {
                toolbar_title.text = Utils.getText(this, StringConstant.wishlist_details_title)
            }
        } else if (intent.getStringExtra("productType") == "product") {
            toolbar_title.text = Utils.getText(this, StringConstant.product_details_title)
        } else if (intent.getStringExtra("productType") == "services") {
            toolbar_title.text = Utils.getText(this, StringConstant.service_details_title)
        } else {
            toolbar_title.text = Utils.getText(this, StringConstant.wishlist_details_title)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


/*
    private fun initToolbar() {
        setSupportActionBar(toolbar)

        if (intent.getStringExtra("productType") == "services") {
            toolbar_title.text = Utils.getText(this, StringConstant.service_details_title)

        } else if (intent.getStringExtra("productType") == "product") {
            toolbar_title.text = Utils.getText(this, StringConstant.product_details_title)
        } else {
            toolbar_title.text = Utils.getText(this, StringConstant.wishlist_details_title)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
*/


    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            R.id.action_delete_service -> {
                CustomSignOutDialog(this@ProductDetailActivity, Constant.DIALOG_DELETE_SERVICE,
                        this@ProductDetailActivity, intent.getStringExtra("productType")).show()
            }
            R.id.action_edit_service -> {
/*
                CustomSignOutDialog(this@ProductDetailActivity, Constant.DIALOG_EDIT_SERVICE,
                        this@ProductDetailActivity, intent.getStringExtra("productType")).show()
*/
                if (intent.getStringExtra("productType") == "services" || intent.getStringExtra("productType") == "wishlist") {
                    val intent = EditServiceActivity.newMainIntent(this@ProductDetailActivity)
                    intent?.putExtra("mServiceData", mServiceData)
                    intent?.putExtra("productType", this.intent.getStringExtra("productType"))
                    ActivityStack.getInstance(this@ProductDetailActivity)
//                startActivity(intent)
                    startActivityForResult(intent, EDITSERVICEPRODUCT)
                } else {
                    val intent = EditProductActivity.newMainIntent(this@ProductDetailActivity)
                    intent?.putExtra("mServiceData", mServiceData)
                    ActivityStack.getInstance(this@ProductDetailActivity)
//                startActivity(intent)
                    startActivityForResult(intent, EDITSERVICEPRODUCT)
                }

            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

    /**
     * On Back Pressed Method Call
     */
    override fun onBackPressed() {
        super.onBackPressed()

        if (s1 == "changed") {
            Log.e("Changed ", "")
            if (intent.getStringExtra("productType") == "services") {
                val resultIntent = Intent()
                resultIntent.putExtra("ADDPOST_REQUEST", "SERVICE")
                this.setResult(Activity.RESULT_OK, resultIntent)
                finish()
            } else if (intent.getStringExtra("productType") == "product") {
                val resultIntent = Intent()
                resultIntent.putExtra("ADDPOST_REQUEST", "PRODUCT")
                this.setResult(Activity.RESULT_OK, resultIntent)
                finish()
            } else if (intent.getStringExtra("productType") == "wishlist") {
                val resultIntent = Intent()
                resultIntent.putExtra("ADDPOST_REQUEST", "WISHLIST")
                this.setResult(Activity.RESULT_OK, resultIntent)
                finish()
            } else {
                val resultIntent = Intent()
                resultIntent.putExtra("ADDPOST_REQUEST", "NEARBY")
                this.setResult(Activity.RESULT_OK, resultIntent)
                finish()
            }
        } else {
            // Log.e("unChanged ", "lllllll")
            ActivityStack.removeActivity(this@ProductDetailActivity)
            //setResult(Activity.RESULT_OK)
            finish()
        }


    }

    override fun onMapReady(googleMap: GoogleMap?) {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json))

            if (!success) {
            }
        } catch (e: NotFoundException) {

        } finally {
            // Position the map's camera near Sydney, Australia.
            if (googleMap != null) {
                var mServiceData: ServicesDataResponse? = null
                if (intent.getStringExtra("productType").equals("services")) {
                    mServiceData = intent.getParcelableExtra<ServicesDataResponse>("mProductData")
                } else if (intent.getStringExtra("productType").equals("product")) {
                    mServiceData = intent.getParcelableExtra<ServicesDataResponse>("mProductData")
                } else {
                    mServiceData = intent.getParcelableExtra<ServicesDataResponse>("mProductData")
                }
                googleMap.addMarker(MarkerOptions()
                        .position(LatLng(mServiceData!!.Lat!!, mServiceData.Long!!))
                        .title(mServiceData.title))
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(mServiceData.Lat!!, mServiceData.Long!!)))
                // Zoom out to zoom level 10, animating with a duration of 2 seconds.
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10F), 2000, null)
            }
        }
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ProductDetailActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

/*    private fun setUiPageViewController() {
        if (dotsCount != 1) {
            // dotsCount = mAdapter.count
            dots = arrayOfNulls(dotsCount)
            for (i in 0..dotsCount - 1) {
                dots[i] = ImageView(this@ProductDetailActivity)
                dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
                val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(4, 0, 4, 0)
                viewPagerCountDots.addView(dots[i], params)
            } // Commit Testing
            dots[0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
        }
    }*/

    override fun onPageScrollStateChanged(p0: Int) {
    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        editMenuItem = menu?.findItem(R.id.action_edit_service)
        editMenuItem!!.title = Utils.getText(this, StringConstant.service_edit_title)
//            register?.isVisible = false
        deleteMenuItem = menu?.findItem(R.id.action_delete_service)
        deleteMenuItem!!.title = Utils.getText(this, StringConstant.service_delete_title)
//            deleteMenuItem?.isVisible = false
        if (mServiceData?.user_id.toString() == personData.user_id.toString()) {
            editMenuItem?.isVisible = true
            deleteMenuItem?.isVisible = true
        } else {
            editMenuItem?.isVisible = false
            deleteMenuItem?.isVisible = false
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onPageSelected(p0: Int) {
        for (i in 0..dotsCount - 1) {
            try {
                dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
                dots[p0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
                if (p0 + 1 == dotsCount) {
                    // btn_next.visibility = View.VISIBLE
                } else {
                    // btn_next.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_view_reviews -> {
                if (Utils.haveNetworkConnection(this@ProductDetailActivity)) {
                    tetootaApplication!!.isCheck = "ServiceReview"
                    val intent = ViewReviewsActivity.newMainIntent(this@ProductDetailActivity)
                    intent?.putExtra("post_id", mServiceData?.id.toString())
                    ActivityStack.getInstance(this@ProductDetailActivity)
                    startActivity(intent)
                } else {
                }
            }
            tv_avg_review -> {
                if (Utils.haveNetworkConnection(this@ProductDetailActivity)) {
                    tetootaApplication!!.isCheck = "ServiceReview"
                    val intent = ViewReviewsActivity.newMainIntent(this@ProductDetailActivity)
                    intent?.putExtra("post_id", mServiceData?.id.toString())
                    ActivityStack.getInstance(this@ProductDetailActivity)
                    startActivity(intent)
                } else {
                }
            }
            tv_avg_count -> {
                if (Utils.haveNetworkConnection(this@ProductDetailActivity)) {
                    tetootaApplication!!.isCheck = "ServiceReview"
                    val intent = ViewReviewsActivity.newMainIntent(this@ProductDetailActivity)
                    intent?.putExtra("post_id", mServiceData?.id.toString())
                    ActivityStack.getInstance(this@ProductDetailActivity)
                    startActivity(intent)
                } else {
                }
            }

            btn_conversation -> {
/*
                StartConversationDialog(this@ProductDetailActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT, "",
                        this@ProductDetailActivity, getString(R.string.err_msg_mobile_number_limit), intent.putExtra("productType", listType), exchangePostType!!).show()
*/
                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                /* if (personData.complete_percentage!!.toInt() < 50) {
                     ProfileCompletionDialog(this@ProductDetailActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                             this@ProductDetailActivity, getString(R.string.err_msg_mobile_number_limit)).show()

                 } else {
                     StartConversationDialog(this@ProductDetailActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT, "",
                             this@ProductDetailActivity, getString(R.string.err_msg_mobile_number_limit), intent.putExtra("productType", listType), exchangePostType!!).show()
                 }*/
//                var is_service: String = Utils.loadPrefrence(Constant.IS_SERVICE, "", this)
                Log.e("isServiceAdded2", serviceCount.toString())

                if (personData.first_name == null && personData.first_name.equals("") || personData.last_name == null || personData.last_name.equals("")
                        && personData.profile_image == null || personData.profile_image.equals("")) {
                    ProfileCompletionDialog(this@ProductDetailActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this@ProductDetailActivity, getString(R.string.err_msg_mobile_number_limit)).show()

                } else if (serviceCount <= 0) {
                    CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this, getString(R.string.err_msg_mobile_number_limit)).show()
                    return
                } else {
                    val intent = Intent(context, MessagingActivity::class.java)
                    intent.putExtra("mProductData", mServiceData)
                    intent.putExtra("productType", "services")
                    if (listType.equals("wishlist", true))
                        intent.putExtra("type", Constant.WISHLIST)

                    startActivity(intent)
//                    StartConversationDialog(this@ProductDetailActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT, "",
//                            this@ProductDetailActivity, getString(R.string.err_msg_mobile_number_limit), intent.putExtra("productType", listType), exchangePostType!!).show()
                }


            }

            iv_favorite_selected -> {
                if (mServiceData?.is_favourite == 1) {
                    //mServiceData?.is_favourite = 0
                    if (Utils.haveNetworkConnection(this@ProductDetailActivity)) {
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                        mHomePresenter.markFavorite(this, mServiceData!!, "0", 0)
                        //iv_favorite_selected.setImageResource(R.drawable.iv_wish_un)

                    } else {
                        showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                    }
                    iv_favorite_selected.setImageResource(R.drawable.iv_wish_un)
                } else {
                    if (Utils.haveNetworkConnection(this@ProductDetailActivity)) {
                        // mServiceData?.is_favourite = 1
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                        mHomePresenter.markFavorite(this, mServiceData!!, "1", 0)
                        iv_favorite_selected.setImageResource(R.drawable.iv_wish)
                    } else {
                        showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                    }
                }
            }

            iv_share -> {
//                showSnackBar("Under Development")
                sendEmail()
            }
            tv_navigation -> {
//                val gmmIntentUri = Uri.parse("google.navigation:q=Taronga+Zoo,+Sydney+Australia")
                var address = mServiceData?.post_address
                val strArray = address?.toCharArray()
                val sb = StringBuffer()
                var i = 0
                while (i < strArray!!.size) {
                    if (strArray[i] != ' ' && strArray[i] != '\t') {
                        sb.append(strArray[i])
                    } else {
                        sb.append("+")
                    }
                    i++
                }
                val gmmIntentUri = Uri.parse("google.navigation:q=" + sb.toString())
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.`package` = "com.google.android.apps.maps"
                startActivity(mapIntent)
            }

            tv_translatedText -> {
                googleTranslate(tv_productname.text as String)
            }
            tv_translatedTextDesc -> {
                googleTranslate(tv_product_category.text as String)
            }
            tv_contentTranslatedText -> {
                googleTranslate(tv_content.text as String)
            }
            tv_qualifications_degreeTranslate -> {
                googleTranslate(tv_qualifications_degree.text as String)
            }
            tv_location_detail_degreeTranslate -> {
                googleTranslate(tv_location_detail.text as String)
            }

            iv_user -> {
                if (mServiceData!!.profile_image!!.isEmpty()) {
                    // this.toast("Profile image is not available")

                } else {
                    val intent = FullImageActivity.newMainIntent(this)
                    intent!!.putExtra("profile_image", mServiceData!!.profile_image)
                    ActivityStack.getInstance(this)
                    startActivity(intent)
                }

            }
        }
    }

    override fun onProfileData(param: String, message: String) {
        if (message == "Yes") {
            if (Utils.haveNetworkConnection(this.context!!)) {
                val intent = ProfileDetailActivity.newMainIntent(this.context!!)
                ActivityStack.getInstance(this.context!!)
                startActivity(intent)
            } else {
//                showSnackBar(resources.getString(R.string.str_check_internet))
            }
        } else {

        }
    }


    private fun googleTranslate(text: String) {
        if (Utils.haveNetworkConnection(this)) {
            val intent = Intent(this, GoogleTranslateActivity::class.java)
            intent.putExtra("translateText", text)
            startActivity(intent)
        }
    }

    private fun sendEmail() {
        /*val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
//      intent.putExtra(Intent.EXTRA_TEXT, this@ProductDetailActivity.resources.getString(R.string.inviteFriend_text))
        startActivity(Intent.createChooser(intent, ""))*/

//        showPopupMenu()
        showPopupMenu()
    }

    /**
     * Method To Create Popup Menu
     * for share, Favorite, Report
     *//*
    private fun showPopupMenu() {
        val popup = PopupMenu(this, iv_share)
        popup.menuInflater.inflate(R.menu.whatsapp_facebook_menu, popup.menu)
        this.menu = popup.menu
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_whatapp -> {
//                    openWhatsApp()
//                    shareProductOnWhatApp()
                    sendOnWhatsapp()
                }
                R.id.menu_facebook -> {
                    var permissionNeeds: java.util.ArrayList<String> = arrayListOf()
                    permissionNeeds.add("publish_actions")
//                    permissionNeeds.add("email,public_profile")
                    //this loginManager helps you eliminate adding a LoginButton to your UI
                    loginManager = LoginManager.getInstance();
                    loginManager!!.logInWithPublishPermissions(this, permissionNeeds);
                    loginManager!!.registerCallback(callbackManager, loginCallback);
                }
            }
            true
        }
        popup.show()
    }*/

    /**
     * Method To Create Popup Menu
     * for share, Favorite, Report
     */
    private fun showPopupMenu() {
        var mAttributeValue: String = ""
        val popup = PopupMenu(this, iv_share)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        val shareItem = menu?.findItem(R.id.menu_share)
        val reportThisItem = menu?.findItem(R.id.menu_report_this)

        if (mServiceData?.is_favourite == 0) {
            mAttributeValue = "1"
            favItem?.title = Utils.getText(this, StringConstant.favorite)
        } else {
            mAttributeValue = "0"
            favItem?.title = Utils.getText(this, StringConstant.home_unfavourite)
        }

        shareItem?.title = Utils.getText(this, StringConstant.share_text)
        reportThisItem?.title = Utils.getText(this, StringConstant.report_this)

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ProductDetailActivity)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    val postString: String = personData.first_name + " is offering " + '"' + mServiceData?.title!! + '"' + " on http://tetoota.com"
                    var url: String = "";
                    if (mServiceData!!.image != null && mServiceData!!.image_thumb != null) {
                        url = Utils.getUrl(this, mServiceData!!.image!!, mServiceData!!.image_thumb!!, false)
                    } else {
                        url = Utils.getUrl(this, mServiceData!!.image!!)
                    }
//                    shareService(postString, url)
                    var gson = Gson()
                    var jsonString = gson.toJson(mServiceData)
                    Log.e("jsonString", jsonString + "");
                    val newDeepLink = buildDeepLink(link + jsonString, postString, ANDROID_MIN_VERSION)

                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this)) {
                        mServicePresenter.markFavorite(this, this.mServiceData!!, mAttributeValue, 0)
                    } else {
                        toast(Utils.getText(this, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    StartConversationDialog(this, Constant.DIALOG_LOGIN_FAILURE_ALERT, mServiceData?.id.toString(),
                            this@ProductDetailActivity, getString(R.string.str_report_issue), intent.putExtra("productType", ""), exchangePostType!!).show()
                }
            }
            true
        }
        popup.show()
    }

    /**
     * Method to Share Product
     * TODO Deep linking
     */


//    fun shareService(mTitle: String, mImageUrl: String) {
//        try {
//            val text = mTitle
//            var imageUri: Uri? = null
//            try {
//                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(getResources(), R.drawable.share_social),
//                        null, null));
//            } catch (e: NullPointerException) {
//            }
//            val shareIntent = Intent()
//            shareIntent.action = Intent.ACTION_SEND
//            shareIntent.setType("text/plain");
////            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
//            //     shareIntent.type = "image/*"
//            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
//            // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//            //  startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
//            startActivity(Intent.createChooser(shareIntent, "Share images..."))
//
//        } catch (ex: android.content.ActivityNotFoundException) {
//        }
//    }


    fun shareService(context: Context?, postString: String, deepLink: String) {
        mProgressDialog?.dismiss()
        try {
//            var ref = tv_referralCode.text.toString().trim()
//            var newUrl = inviteFriend_text.replace("@", ref)
//            val text = newUrl + "\n" + deepLink
//            Log.e(InviteFriendsFragment.TAG, ref + "\n\n" + "newUrl" + inviteFriend_text.replace("@", ref) + "\n\n" + text)
//            val urlToShare = StringConstant.app_link
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(context.getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            //  Log.e("imageUri - ","" + imageUri)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, postString + "\n" + deepLink)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            //  shareIntent.type = "image/*"
            // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
//            context?.startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
/*
    fun shareProduct(mTitle: String, mImageUrl: String) {
        val text = mTitle
        val pictureUri = Uri.parse(mImageUrl)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share images..."))
    }
*/*/

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            toast(message)
        } else {
            toast(message)
        }
    }

    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
//        Utils.hideSoftKeyboard(this@ProductDetailActivity)
        if (message == "includeProposal") {
            openIncludeProposalsFragment()
        } else if (message == "sendMessage") {
            if (param.isNotEmpty()) {
                if (Utils.haveNetworkConnection(this@ProductDetailActivity)) {
                    mInboxPresenter.sendMesg(this@ProductDetailActivity,
                            personData.user_id, mServiceData?.user_id.toString(), mServiceData?.id.toString(), param, "", Constant.SERVICES)
                } else {
                    showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                }
            } else {
                showSnackBar(resources.getString(R.string.str_chat_not_empty))
            }
        } else if (message == "report") {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mHomePresenter.report(this, mServiceId, mMesgDesc)
        }
    }

    private fun openIncludeProposalsFragment() {
        val intent = IncludeProposalsActivity.newMainIntent(this@ProductDetailActivity)
        if (intent != null) {
            intent.putExtra("mServiceDataResponse", mServiceData)
            intent.putExtra("list_type", listType)
        }
        ActivityStack.getInstance(this@ProductDetailActivity)
//        startActivity(intent)
        startActivityForResult(intent, INCLUDE_PROPOSAL_ACTIVITY)
    }

    private fun setMultiLanguageText() {
        exchangePostType = mServiceData!!.post_type
        if (listType == "services") {
            if (mServiceData!!.post_type == "Services") {
                tv_service_provider.text = Utils.getText(this, StringConstant.tv_service_provider)
                tv_location.text = Utils.getText(this, StringConstant.str_locations)
                rl_servicevirtually.visibility = View.VISIBLE
                btn_points.visibility = View.VISIBLE

            } else if (mServiceData!!.post_type == "Products") {
                tv_service_provider.text = Utils.getText(this, StringConstant.tv_product_provider)
                tv_location.text = Utils.getText(this, StringConstant.str_product_locations)
                rl_servicevirtually.visibility = View.GONE
                btn_points.visibility = View.VISIBLE

            } else {
                tv_service_provider.text = Utils.getText(this, StringConstant.tv_user_provider)
                tv_location.text = Utils.getText(this, StringConstant.str_wish_locations)
                rl_servicevirtually.visibility = View.VISIBLE
                btn_points.visibility = View.GONE
                if (mServiceData?.virtual_service.toString() == "Yes") {
                    tv_service1.text = Utils.getText(this, StringConstant.PD_I_perform_wishlist_virtually)
                } else {
                    tv_service1.text = Utils.getText(this, StringConstant.wishlist_detail_no_virtual)

                }

            }

        } else if (listType == "product") {
            tv_service_provider.text = Utils.getText(this, StringConstant.tv_product_provider)
            tv_location.text = Utils.getText(this, StringConstant.str_product_locations)
            rl_servicevirtually.visibility = View.GONE
            btn_points.visibility = View.VISIBLE


        } else if (listType == "wishlist") {
            tv_service_provider.text = Utils.getText(this, StringConstant.tv_user_provider)
            tv_location.text = Utils.getText(this, StringConstant.str_wish_locations)
            rl_servicevirtually.visibility = View.VISIBLE
            btn_points.visibility = View.VISIBLE

        }

        if (Utils.getText(this, StringConstant.str_tv_translate) == "Translate") {
            tv_translatedText.visibility = View.GONE
            tv_translatedTextDesc.visibility = View.GONE
        } else {
            tv_translatedText.visibility = View.GONE
            tv_translatedTextDesc.visibility = View.GONE
            //  tv_translatedText.visibility = View.VISIBLE
            //  tv_translatedTextDesc.visibility = View.VISIBLE
        }
        tv_avg.text = Utils.getText(this, StringConstant.str_tv_avg)
        tv_response_time.text = Utils.getText(this, StringConstant.str_responsetime)
        tv_qualifications.text = Utils.getText(this, StringConstant.qualifications_degree)
        tv_trending_divider.text = Utils.getText(this, StringConstant.availability_trading_preferences)
        // tv_location.text = Utils.getText(this, StringConstant.str_locations)
        tv_navigation.text = Utils.getText(this, StringConstant.product_detail_navigate)
        btn_view_reviews.text = Utils.getText(this, StringConstant.view_review)
        btn_conversation.text = Utils.getText(this, StringConstant.start_converse_title)
        tv_badge1.text = Utils.getText(this, StringConstant.str_badege)
        tv_verification1.text = Utils.getText(this, StringConstant.str_verification)
        tv_sync_phone1.text = Utils.getText(this, StringConstant.str_sync_wid_phone)
        tv_quality1.text = Utils.getText(this, StringConstant.str_quality)
        tv_friendlines1.text = Utils.getText(this, StringConstant.str_friendlines)
        tv_title.text = Utils.getText(this, StringConstant.tv_title)
        tv_description.text = Utils.getText(this, StringConstant.str_tv_description)
        if (productType.equals(getString(R.string.wish_list), true))
            tv_price.text = Utils.getText(this, StringConstant.willing_to_offer)
        else
            tv_price.text = Utils.getText(this, StringConstant.tv_price)
        tv_translatedText.text = Utils.getText(this, StringConstant.str_tv_translate)
        tv_translatedTextDesc.text = Utils.getText(this, StringConstant.str_tv_translate)
    }

    override fun onDashboardApiSuccessResult(mCategoriesList: ArrayList<Any>, message: String) {
    }

    override fun onDashboardApiSuccessResult(mCategoriesList: ArrayList<Any>, arrayList: ArrayList<Any>, message: String) {
    }

    override fun onDashboardApiFailureResult(message: String) {
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        hideProgressDialog()
        if (cellRow.is_favourite == 0) {
            cellRow.is_favourite = 1
            s1 = "changed"
            this?.toast("Mark Favorite")

        } else {
            cellRow.is_favourite = 0
            s1 = "changed"
            this.toast("Mark Unfavorite")
        }
    }


    override fun ongetAllProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
    }

    override fun ongetAllProposalFailure(message: String) {
    }

    override fun onChatMesgSuccess(message: String) {
        super.onChatMesgSuccess(message)
        if (!isFinishing) {
            showSnackBar(getString(R.string.str_mesg_send_success))
        }
    }

    override fun onChatMesgFailure(message: String) {
        super.onChatMesgFailure(message)
        println("message Failure $message")
    }

    override fun onYes(param: String, message: String) {
        val intent = AddPostRequestActivity.newMainIntent(this)
        intent!!.putExtra("Tab", "serviceTab")
        ActivityStack.getInstance(this!!)
        startActivity(intent)
        finish()
    }

    override fun dialogAlert(dialogID: Int, message: String) {
//        super.dialogAlert(dialogID, message)
        if (dialogID == Constant.DIALOG_DELETE_SERVICE) {
            mAddServicePresenter.deleteService(this@ProductDetailActivity
                    , personData.user_id.toString(), mServiceData?.id.toString())
        }

/*
        if (dialogID == Constant.DIALOG_EDIT_SERVICE) {

            mAddServicePresenter.deleteService(this@ProductDetailActivity, personData.user_id.toString(), mServiceData?.id.toString())

           if (message == "service") {
               if (intent.getStringExtra("productType") == "services" || intent.getStringExtra("productType") == "wishlist") {
                val intent = EditServiceActivity.newMainIntent(this@ProductDetailActivity)
                intent?.putExtra("mServiceData", mServiceData)
                intent?.putExtra("productType", this.intent.getStringExtra("productType"))
                ActivityStack.getInstance(this@ProductDetailActivity)
//                startActivity(intent)
                startActivityForResult(intent, EDITSERVICEPRODUCT)
            } else {
                val intent = EditProductActivity.newMainIntent(this@ProductDetailActivity)
                intent?.putExtra("mServiceData", mServiceData)
                ActivityStack.getInstance(this@ProductDetailActivity)
//                startActivity(intent)
                startActivityForResult(intent, EDITSERVICEPRODUCT)
            }
        } else {
            mAddServicePresenter.deleteService(this@ProductDetailActivity
                    , personData.user_id.toString(), mServiceData?.id.toString())
        }
        }
*/
    }

    override fun onCategoryApiSuccess(message: String, responseType: String) {
    }

    override fun onApiSuccess(mCategoriesList: ArrayList<Any>, message: String) {
    }

    override fun onDeletePostApiSuccess(message: String) {
        toast(message)
//        val intent = Intent(this@ProductDetailActivity, ServiceProductActivity::class.java)
//        intent?.putExtra("Tab", "serviceTab")
//        startActivity(intent)
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onApiFailure(message: String) {
        /*if (message == "success") {
            val intent = Intent(this@ProductDetailActivity, ServiceProductActivity::class.java)
            intent?.putExtra("Tab", "serviceTab")
            startActivity(intent)
        }*/
        toast(message)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EDITSERVICEPRODUCT) {
                setResult(Activity.RESULT_OK)
                finish()
            } else if (requestCode == INCLUDE_PROPOSAL_ACTIVITY) {

            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private val callback = object : FacebookCallback<Sharer.Result> {
        override fun onSuccess(result: Sharer.Result) {
            Log.v("FACEBOOK CALLBACE", "Successfully posted")
            toast("Posted Successfully")
        }

        override fun onCancel() {
            Log.v("FACEBOOK CALLBACE", "Sharing cancelled")
            toast("Sharing cancelled")
        }

        override fun onError(error: FacebookException) {
            Log.v("FACEBOOK CALLBACE", error.message)
            toast(error.message!!)
        }
    }

    private val loginCallback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            Log.v("FACEBOOK LOGIN_CALLBACE", "Successfully Login")
            toast("Login Successfully")
//            setImageShare()
            val job = SendfeedbackJob()
            job.execute("", "")
//            setImageTextShare()
        }

        override fun onCancel() {
            Log.v("FACEBOOK LOGIN_CALLBACE", "Login cancelled")
            toast("Login cancelled")
        }

        override fun onError(error: FacebookException) {
            Log.v("FACEBOOK LOGIN_CALLBACE", error.message)
            toast(error.message!!)
        }
    }

    private fun setImageShare() {
        val image = BitmapFactory.decodeResource(resources, R.drawable.iv_tetoota_icon)
//        val url = URL(mServiceData?.image!!)
//        val image = BitmapFactory.decodeStream(url.openConnection().getInputStream())
        val photo = SharePhoto.Builder()
                .setBitmap(image)
//                .setCaption("#Tutorialwing")
                .build()
        val content = SharePhotoContent.Builder()
                .addPhoto(photo)
                .build()
//        fb_share_button.shareContent = content
        shareDialog?.show(content, ShareDialog.Mode.AUTOMATIC)
//        ShareApi.share(content, null);
//        fb_share_button.setShareContent(content)
    }

    private fun setImageTextShare() {
//        val shareLinkContent = ShareLinkContent.Builder()
//        shareLinkContent.setContentTitle("Tetooota")
//                .setContentDescription("Hello Friends....")
////                .setImageUrl(Uri.parse())
//                .build()
//        if (!ShareDialog.canShow(ShareLinkContent::class.java)) {
//            return
//        }
//        shareDialog?.show(shareLinkContent,ShareDialog.Mode.AUTOMATIC);

        /*val linkContent = ShareLinkContent.Builder()
                .setContentTitle("Android Facebook Integration and Login Tutorial")
                .setImageUrl(Uri.parse("https://www.studytutorial.in/wp-content/uploads/2017/02/FacebookLoginButton-min-300x136.png"))
                .setContentDescription("This tutorial explains how to integrate Facebook and Login through Android Application")
                .setContentUrl(Uri.parse("https://www.studytutorial.in/android-facebook-integration-and-login-tutorial"))
                .build();
        shareDialog?.show(linkContent);*/
        var url: String = "";
        if (mServiceData!!.image != null && mServiceData!!.image_thumb != null) {
            url = Utils.getUrl(this, mServiceData!!.image!!, mServiceData!!.image_thumb!!, false)
        } else {
            url = Utils.getUrl(this, mServiceData!!.image!!)
        }
        val linkContent = ShareLinkContent.Builder()
                .setContentTitle(mServiceData?.title!!)
                .setImageUrl(Uri.parse(url))
                .setContentDescription("This Tetoota Product...")
                .setContentUrl(Uri.parse(url))
                .build();
        shareDialog?.show(linkContent);
    }

    public fun getLocalBitmapUri(): Uri {
        val image = BitmapFactory.decodeResource(resources, R.drawable.iv_tetoota_icon)
        var bmpUri: Uri? = null
        try {
            var file: File = File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png")
            file.getParentFile().mkdirs()
            var out: FileOutputStream = FileOutputStream(file)
            image?.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.close()
            bmpUri = Uri.fromFile(file)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri!!
    }

    private inner class SendfeedbackJob : AsyncTask<String, Void, String>() {
        lateinit var image: Bitmap
        lateinit var content: SharePhotoContent
        override fun onPreExecute() {
            super.onPreExecute()
            showProgressDialog("please wait...")
        }

        override fun doInBackground(params: Array<String>): String {
            var urll: String = "";
            if (mServiceData!!.image != null && mServiceData!!.image_thumb != null) {
                urll = Utils.getUrl(this@ProductDetailActivity, mServiceData!!.image!!, mServiceData!!.image_thumb!!, false)
            } else {
                urll = Utils.getUrl(this@ProductDetailActivity, mServiceData!!.image!!)
            }
            val url = URL(urll)
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream())
            val photo = SharePhoto.Builder()
                    .setBitmap(image)
                    .build()
            content = SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build()
            return "some message"
        }

        override fun onPostExecute(message: String) {
            hideProgressDialog()
            shareDialog?.show(content, ShareDialog.Mode.AUTOMATIC)
        }
    }

    private fun sendOnWhatsapp() {
        val text = mServiceData?.title!!
//        val uri = Uri.parse(mServiceData?.image!!)
        val shareIntent = Intent(android.content.Intent.ACTION_SEND)
//        shareIntent.type = "image/*"
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
//        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        val pm: PackageManager = this@ProductDetailActivity.getPackageManager()
        var activityList = pm.queryIntentActivities(shareIntent, 0)
        for (app: ResolveInfo in activityList) {
            if ((app.activityInfo.name).contains("com.whatsapp")) {
                var activity = app.activityInfo
                var name = ComponentName(activity.applicationInfo.packageName, activity.name)
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
                shareIntent.setComponent(name)
                startActivity(shareIntent)
                return
            } else {
//                Toast.makeText(this@ProductDetailActivity, "WhatsApp not Installed", Toast.LENGTH_SHORT).show()
            }
        }
        Toast.makeText(this@ProductDetailActivity, "WhatsApp not Installed", Toast.LENGTH_SHORT).show()
    }

    fun shareProductOnWhatApp() {
        val pm: PackageManager = this@ProductDetailActivity.getPackageManager()
        try {
            val text = mServiceData?.title!!
            var url: String = "";
            if (mServiceData!!.image != null && mServiceData!!.image_thumb != null) {
                url = Utils.getUrl(this, mServiceData!!.image!!, mServiceData!!.image_thumb!!, false)
            } else {
                url = Utils.getUrl(this, mServiceData!!.image!!)
            }
            val pictureUri = Uri.parse(url)
            val whatsappIntent = Intent(android.content.Intent.ACTION_SEND)
            whatsappIntent.type = "image/*"
            whatsappIntent.`package` = "com.whatsapp"
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, text)
            whatsappIntent.putExtra(Intent.EXTRA_STREAM, pictureUri) //add image path
            startActivity(Intent.createChooser(whatsappIntent, "Share image with"))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show()
        }
    }

    fun openWhatsApp() {
        val pm: PackageManager = this@ProductDetailActivity.getPackageManager()
        try {
            val waIntent = Intent(Intent.ACTION_SEND)
            waIntent.type = "image/*"
            val text = mServiceData?.title!!
            var url: String = "";
            if (mServiceData!!.image != null && mServiceData!!.image_thumb != null) {
                url = Utils.getUrl(this, mServiceData!!.image!!, mServiceData!!.image_thumb!!, false)
            } else {
                url = Utils.getUrl(this, mServiceData!!.image!!)
            }
            val pictureUri = Uri.parse(url)
            val info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)
            //Check if package exists or not. If not then code
            //in catch block will be called
            waIntent.`package` = "com.whatsapp"
            waIntent.putExtra(Intent.EXTRA_TEXT, text)
            waIntent.putExtra(Intent.EXTRA_STREAM, pictureUri) //add image path
            startActivity(Intent.createChooser(waIntent, "Share with"))
        } catch (e: PackageManager.NameNotFoundException) {
            Toast.makeText(this@ProductDetailActivity, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                    .show()
        }
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onWishListApiSuccessResult(myServiceList: List<Any?>?, message: String) {
        Log.e("myServiceList", myServiceList?.size.toString())
        mList = myServiceList as List<ServicesDataResponse>

        if (myServiceList != null) {
            Log.e("sizeeeeeeeee ", "" + myServiceList.size)
            serviceCount = myServiceList!!.size

        } else {
            serviceCount = 0
        }
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
    }

    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this) * .2f).toInt()
        return getTopMarginH
    }

    /**
     * Build a Firebase Dynamic Link.
     * https://firebase.google.com/docs/dynamic-links/android/create#create-a-dynamic-link-from-parameters
     *
     * @param deepLink the deep link your app will open. This link must be a valid URL and use the
     * HTTP or HTTPS scheme.
     * @param minVersion the `versionCode` of the minimum version of your app that can open
     * the deep link. If the installed app is an older version, the user is taken
     * to the Play store to upgrade the app. Pass 0 if you do not
     * require a minimum version.
     * @return a [Uri] representing a properly formed deep link.
     */
    @VisibleForTesting
    fun buildDeepLink(jsonData: String, postString: String, minVersion: Int): Uri {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)

        // Set dynamic link parameters:
        //  * URI prefix (required)
        //  * Android Parameters (required)
        //  * Deep link
        // [START build_dynamic_link]
        val builder = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix(uriPrefix)
                .setAndroidParameters(DynamicLink.AndroidParameters.Builder()
                        .setMinimumVersion(minVersion)
                        .build())
                .setIosParameters(
                        DynamicLink.IosParameters.Builder("com.tetoota")
                                .setAppStoreId("1402450812")
                                .setMinimumVersion("1.2")
                                .build())
                .setLink(Uri.parse(jsonData))

        // Build the dynamic link
        val link = builder
//                .buildDynamicLink()
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                .addOnSuccessListener { result ->
                    // Short link created
                    shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    Log.e("flowchartLink", "" + shortLink)
                    shareService(this, postString, shortLink.toString())
                }.addOnFailureListener {
                    Log.e("addOnFailureListener", "FAIL")
                }
        shareService(this, postString, link.toString())

        // [END build_dynamic_link]
        // Return the dynamic link as a URI
        return Uri.parse("")
    }

    private fun shareDeepLink(deepLink: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link")
        intent.putExtra(Intent.EXTRA_TEXT, deepLink)

        startActivity(intent)
    }

    private fun validateAppCode() {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)
        if (uriPrefix.contains("YOUR_APP")) {
            AlertDialog.Builder(this)
                    .setTitle("Invalid Configuration")
                    .setMessage("Please set your Dynamic Links domain in app/build.gradle")
                    .setPositiveButton(android.R.string.ok, null)
                    .create().show()
        }
    }
}
