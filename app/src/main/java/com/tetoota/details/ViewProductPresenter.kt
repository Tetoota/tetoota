package com.tetoota.details

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class ViewProductPresenter : ViewProductContract.Presenter, ViewProductContract.ViewProductApiResult {
    var mFavoriteView: ViewProductContract.View

    val mFavoriteInteractor: ViewProductInteractor by lazy {
        ViewProductInteractor(this)
    }

    constructor(mFavoriteView: ViewProductContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun getViewProductCode(mActivity: Activity, user_id: String, post_id: String) {
        mFavoriteInteractor.getSendServiceData(mActivity, user_id, post_id)
    }

    override fun onViewProductApiSuccess(mString: String, mReferralCode: String) {
        mFavoriteView.onViewProductSuccess(mString, mReferralCode)
    }

    override fun onViewProductApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onViewProductFailureResponse(mString, isServerError)
    }
}