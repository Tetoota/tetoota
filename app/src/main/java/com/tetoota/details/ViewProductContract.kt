package com.tetoota.details

import android.app.Activity

class ViewProductContract {
    interface View {
        fun onViewProductSuccess(mString: String, mFavoriteList: String): Unit
        fun onViewProductFailureResponse(mString: String, isServerError: Boolean): Unit
    }

    interface Presenter {
        fun getViewProductCode(mActivity: Activity, user_id: String, post_id: String)
    }

    interface ViewProductApiResult {
        fun onViewProductApiSuccess(mString: String, mReferralCode: String): Unit
        fun onViewProductApiFailure(mString: String, isServerError: Boolean): Unit
    }
}