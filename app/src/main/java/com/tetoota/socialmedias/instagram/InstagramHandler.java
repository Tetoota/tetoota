package com.tetoota.socialmedias.instagram;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import com.tetoota.socialmedias.AlertDialogManager;
import com.tetoota.socialmedias.SocialMediaConstant;
import com.tetoota.socialmedias.SocialMediaProfilePojo;
import com.tetoota.socialmedias.Utility;
import java.util.HashMap;

/**
 * Instagram credentials are created by
 * UserId: jitendra.nandiya@hiteshi.com
 * Password: instagram_30390
 */
public class InstagramHandler {

    private static InstagramHandler instance = null;
    private InstagramApp mApp = null;
    private Activity mContext = null;
    private InstagramCallback mInstagramCallback = null;

    public static InstagramHandler getInstance(){
        if(null == instance){
            instance = new InstagramHandler();
        }
        return instance;
    }

    public void loginToInstagram(Activity context) {
        mContext = context;
        mInstagramCallback = (InstagramCallback) context;
        mApp = new InstagramApp(context, SocialMediaConstant.instagram_client_iD,
                SocialMediaConstant.instagram_client_secret, SocialMediaConstant.instagram_callback_url);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {
                mApp.fetchUserName(handler);
            }

            @Override
            public void onFail(String error) {
                new AlertDialogManager().showAlertDialog(mContext, "Login error!", error, false, false);
            }
        });

        mApp.authorize();

    }

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {

                HashMap<String, String> userInfoHashmap = mApp.getUserInfo();

                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_IS_LOGIN_KEY, true, mContext);
                SocialMediaProfilePojo socialMediaProfilePojo = new SocialMediaProfilePojo();
                socialMediaProfilePojo.setSocialMediaType(SocialMediaConstant.INSTRAGRAM);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_TYPE_KEY,
                        SocialMediaConstant.INSTRAGRAM, mContext);

                String id = userInfoHashmap.get(InstagramApp.TAG_ID);
                socialMediaProfilePojo.setProfileId(id);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_USER_ID_KEY, id, mContext);

                String userName = userInfoHashmap.get(InstagramApp.TAG_USERNAME);
                socialMediaProfilePojo.setUserName(userName);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_USER_NAME_KEY, userName, mContext);

                String email = "";
                socialMediaProfilePojo.setEmailAddress(email);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_EMAIL_ADDRESS_KEY, email, mContext);

                String profileImage = userInfoHashmap.get(InstagramApp.TAG_PROFILE_PICTURE);
                socialMediaProfilePojo.setProfileImage(profileImage);
                Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_PROFILE_IMAGE_KEY, profileImage, mContext);

                mInstagramCallback.returnLoginDetails(socialMediaProfilePojo);
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                new AlertDialogManager().showAlertDialog(mContext, "Error",
                        "Connection Error", false, false);
            }
            return false;
        }
    });

    public void logoutInstagram(){
        try {
            mApp.resetAccessToken();
        }catch (Exception e){
        }
    }


}
