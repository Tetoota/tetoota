package com.tetoota.socialmedias.linkedin;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.tetoota.R;
import com.tetoota.socialmedias.AlertDialogManager;
import com.tetoota.socialmedias.SocialMediaConstant;
import com.tetoota.socialmedias.SocialMediaProfilePojo;
import com.tetoota.socialmedias.Utility;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Linked Credentials are created by
 * Userid: qa.account@hiteshi.com
 * Password: ht123456
 */
public class LinkedinActivity extends Activity {

    private String shareText = "";
    private String value = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_linkedin);

        value = getIntent().getStringExtra("extra");
        shareText = getIntent().getStringExtra("sharetext");
        if (value.equalsIgnoreCase("login")) {
            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.authenticating));
            login_linkedin();
        } else if (value.equalsIgnoreCase("logout")) {
            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.logout_every_media));
            LISessionManager.getInstance(getApplicationContext()).clearSession();
            setResult(RESULT_OK);
            finish();
        } else if (value.equalsIgnoreCase("share")) {
            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.share_input));
            shareOnLinedIn(shareText);
        } else if (value.equalsIgnoreCase("loginandshare")) {
            ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.authenticating));
            login_linkedin();
        }
//        getKeyHash();
    }

    private void getKeyHash() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.tetoota", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    /*Once User's can authenticated,
      It make an HTTP GET request to LinkedIn's REST API using the currently authenticated user's credentials.
      If successful, A LinkedIn ApiResponse object containing all of the relevant aspects of the server's response will be returned.
     */
    public void getUserData() {
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(LinkedinActivity.this, SocialMediaConstant.LINKEDIN_PERMISSIONS_URL, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse result) {
                try {
                    setUserProfile(result.getResponseDataAsJson());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onApiError(LIApiError error) {
                new AlertDialogManager().showAlertDialog(LinkedinActivity.this, "Error",
                        "Connection Error", false, true);
            }
        });
    }

    // Authenticate with linkedin and intialize Session.
    public void login_linkedin() {
        LISessionManager.getInstance(getApplicationContext()).init(this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                if(value.equalsIgnoreCase("loginandshare")){
                    ((TextView) findViewById(R.id.messagetext)).setText(getString(R.string.share_input));
                    shareOnLinedIn(shareText);
                } else {
                    getUserData();
                }
            }

            @Override
            public void onAuthError(LIAuthError error) {
                new AlertDialogManager().showAlertDialog(LinkedinActivity.this, "Error",
                        error.toString(), false, true);
            }
        }, true);
    }

    private Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS, Scope.W_SHARE);
    }

    /*
       Set User Profile Information in Navigation Bar.
     */
    public void setUserProfile(JSONObject response) {
        try {
            Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_IS_LOGIN_KEY, true, LinkedinActivity.this);
            SocialMediaProfilePojo socialMediaProfilePojo = new SocialMediaProfilePojo();
            socialMediaProfilePojo.setSocialMediaType(SocialMediaConstant.LINKEDIN);
            Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_TYPE_KEY,
                    SocialMediaConstant.LINKEDIN, LinkedinActivity.this);

            String id = response.optString("id");
            socialMediaProfilePojo.setProfileId(id);
            Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_USER_ID_KEY, id, LinkedinActivity.this);

            String userName = response.optString("formattedName");
            socialMediaProfilePojo.setUserName(userName);
            Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_USER_NAME_KEY, userName, LinkedinActivity.this);

            String email = response.optString("emailAddress");
            socialMediaProfilePojo.setEmailAddress(email);
            Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_EMAIL_ADDRESS_KEY, email, LinkedinActivity.this);

            String profileImage = response.optString("pictureUrl");
            socialMediaProfilePojo.setProfileImage(profileImage);
            Utility.savePreferences(SocialMediaConstant.SOCIAL_MEDIA_PROFILE_IMAGE_KEY, profileImage, LinkedinActivity.this);

            Intent intent = new Intent();
            intent.putExtra("result", socialMediaProfilePojo);
            setResult(RESULT_OK, intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void shareOnLinedIn(String comment) {
        String payload = "{" +
                "\"comment\":\"" + comment + "\"," +
                "\"visibility\":{" +
                "    \"code\":\"anyone\"}" +
                "}";

//        String shareJsonText = "{ \n" +
//                "   \"comment\":\"" + "TalkingBookz" + "\"," +
//                "   \"visibility\":{ " +
//                "      \"code\":\"anyone\"" +
//                "   }," +
//                "   \"content\":{ " +
//                "      \"title\":\"+audiobookinfo.title+\"," +
//                "      \"description\":\"+audiobookinfo.description+\"," +
//                "      \"submitted-url\":\"+audiobookinfo.sample_url+\"," +
//                "      \"submitted-image-url\":\"+audiobookinfo.cover_url+\"" +
//                "   }" +
//                "}";

        APIHelper apiHelper = APIHelper.getInstance(LinkedinActivity.this);
        apiHelper.postRequest(LinkedinActivity.this, SocialMediaConstant.LINKEDIN_SHARE_URL, payload, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
        //getUserData();
        super.onActivityResult(requestCode, resultCode, data);
    }
}
