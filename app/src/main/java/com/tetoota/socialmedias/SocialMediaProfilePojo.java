package com.tetoota.socialmedias;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abhinav.maurya on 29-06-2017.
 */

public class SocialMediaProfilePojo implements Parcelable {

    private String socialMediaType = null;
    private String profileId = null;
    private String userName = null;
    private String emailAddress = null;
    private String profileImage = null;

    public String getSocialMediaType() {
        return socialMediaType;
    }

    public void setSocialMediaType(String socialMediaType) {
        this.socialMediaType = socialMediaType;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }


    public SocialMediaProfilePojo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.socialMediaType);
        dest.writeString(this.profileId);
        dest.writeString(this.userName);
        dest.writeString(this.emailAddress);
        dest.writeString(this.profileImage);
    }

    protected SocialMediaProfilePojo(Parcel in) {
        this.socialMediaType = in.readString();
        this.profileId = in.readString();
        this.userName = in.readString();
        this.emailAddress = in.readString();
        this.profileImage = in.readString();
    }

    public static final Creator<SocialMediaProfilePojo> CREATOR = new Creator<SocialMediaProfilePojo>() {
        @Override
        public SocialMediaProfilePojo createFromParcel(Parcel source) {
            return new SocialMediaProfilePojo(source);
        }

        @Override
        public SocialMediaProfilePojo[] newArray(int size) {
            return new SocialMediaProfilePojo[size];
        }
    };
}
