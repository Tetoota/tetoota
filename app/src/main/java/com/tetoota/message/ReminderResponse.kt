package com.tetoota.message



data class ReminderResponse(
	val data: String? = null,
	val meta: Meta? = null
)
