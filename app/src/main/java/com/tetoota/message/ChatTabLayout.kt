package com.tetoota.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.inbox.fragment.InboxFoodFragment
import com.tetoota.fragment.inbox.fragment.InboxFragment
import com.tetoota.utility.Constant
import kotlinx.android.synthetic.main.chat_tab_layout.*

class ChatTabLayout : BaseFragment() {
    var tabType = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.chat_tab_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        var fragmentInbox = InboxFragment()
        var fragmentInboxFood = InboxFoodFragment()
        var adapter = ServicesTabAdapter(fragmentManager!!);
        adapter.addFragment(fragmentInbox, Constant.SERVICES);
        adapter.addFragment(fragmentInboxFood, Constant.FOOD);
        viewPagerFood.adapter = adapter
        tabLayout.setupWithViewPager(viewPagerFood)
        if (arguments != null) {
            var bundle = arguments
            if (bundle!!.containsKey(Constant.CHAT_TYPE))
                tabType = bundle!!.getString(Constant.CHAT_TYPE).toString()
            var adapter = ServicesTabAdapter(fragmentManager!!)
            if (tabType.equals(Constant.SERVICES, true)) {
                viewPagerFood.currentItem = 0
                fragmentInbox.arguments = bundle
                adapter.notifyDataSetChanged()
            } else if (tabType.equals(Constant.FOOD, true)) {
                viewPagerFood.currentItem = 1
                fragmentInboxFood.arguments = bundle
                adapter.notifyDataSetChanged()
            } else {
                tabType = Constant.SERVICES
                fragmentInbox.arguments = bundle
                viewPagerFood.currentItem = 0
                adapter.notifyDataSetChanged()
            }
        }
        val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(0)
        val p = tab.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(0, 0, 1, 0)
        tab.requestLayout()
    }
}