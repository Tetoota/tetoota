package com.tetoota.message

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class ReminderContract {
    interface View {
        fun onReminderSuccess(mReminderResponse: Meta): Unit
        fun onReminderFailureResponse(mString: String, isServerError: Boolean): Unit
    }

    interface Presenter {
        fun postReminder(mActivity: Activity, proposal_id: String?, userId: String)
        fun cancelRequest(mActivity: Activity, wishListType: String)
    }

    interface ReferralCodeApiResult {
        fun onReminderCodeApiSuccess(mReminderResponse: Meta): Unit
        fun onReminderCodeApiFailure(mString: String, isServerError: Boolean): Unit
    }
}