package com.tetoota.message

import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.categories.CategoriesContract
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.categories.CategoriesPresenter
import com.tetoota.customviews.CustomServiceAddAlert
import com.tetoota.customviews.ReviewProposalDialog
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.inbox.InboxContract
import com.tetoota.fragment.inbox.InboxPresenter
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.inbox.adapter.ChatRoomAdapter
import com.tetoota.fragment.inbox.fragment.InboxFragment
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatDataResponse
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.message.viewreminder.ViewReminderContract
import com.tetoota.message.viewreminder.ViewReminderPresenter
import com.tetoota.potluck.PotluckFoodDataResponse
import com.tetoota.proposal.CheckProposalsDataResponce.CheckProposalsDataResponse
import com.tetoota.proposal.CheckProposalsDataResponce.DataItem
import com.tetoota.proposal.ProposalByIdData
import com.tetoota.proposal.ProposalsContract
import com.tetoota.proposal.ProposalsPresenter
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.activity_messaging.*
import kotlinx.android.synthetic.main.activity_messaging.btn_cancelDDeal

import kotlinx.android.synthetic.main.activity_messaging.btn_yes
import kotlinx.android.synthetic.main.activity_messaging.et_messagee
import kotlinx.android.synthetic.main.activity_messaging.rb_friendlinese
import kotlinx.android.synthetic.main.activity_messaging.rb_quality_servicee
import kotlinx.android.synthetic.main.activity_messaging.rb_response_timee
import kotlinx.android.synthetic.main.activity_messaging.rl_maincontent
import kotlinx.android.synthetic.main.activity_messaging.tv_friendliness
import kotlinx.android.synthetic.main.activity_messaging.tv_name
import kotlinx.android.synthetic.main.activity_messaging.tv_qualityService
import kotlinx.android.synthetic.main.activity_messaging.tv_rate_trade
import kotlinx.android.synthetic.main.activity_messaging.tv_responseTime
import kotlinx.android.synthetic.main.chat_food_layout.*
import kotlinx.android.synthetic.main.chatting_layout.*
import kotlinx.android.synthetic.main.food_accept_layout.*
import kotlinx.android.synthetic.main.food_review_layout.*
import kotlinx.android.synthetic.main.include_proposal_new.*
import kotlinx.android.synthetic.main.include_proposal_new.iv_exchangeServiceProduct
import kotlinx.android.synthetic.main.include_proposal_new.iv_points
import kotlinx.android.synthetic.main.include_proposal_new.rl_exchangeServiceProduct
import kotlinx.android.synthetic.main.include_proposal_new.scrollView
import kotlinx.android.synthetic.main.include_proposal_new.spinner
import kotlinx.android.synthetic.main.include_proposal_new.tv_exchangeServiceProduct
import kotlinx.android.synthetic.main.include_proposal_new.tv_i_want
import kotlinx.android.synthetic.main.include_proposals_activity.*
import kotlinx.android.synthetic.main.review_dialog_layout.*
import kotlinx.android.synthetic.main.send_proposal_first_step.*
import kotlinx.android.synthetic.main.send_proposal_first_step.btnProposalYes
import kotlinx.android.synthetic.main.send_proposal_first_step.tvShowMsg
import kotlinx.android.synthetic.main.toolbar_layout.*
import kotlinx.android.synthetic.main.view_proposals_activity.*
import kotlinx.android.synthetic.main.view_proposals_activity_new.*
import kotlinx.android.synthetic.main.view_proposals_activity_new.btn_cancelDeall
import kotlinx.android.synthetic.main.view_proposals_activity_new.btn_completeDeall
import kotlinx.android.synthetic.main.view_proposals_activity_new.ivUser
import kotlinx.android.synthetic.main.view_proposals_activity_new.iv_user3
import kotlinx.android.synthetic.main.view_proposals_activity_new.llPoints
import kotlinx.android.synthetic.main.view_proposals_activity_new.ll_serviceTaken
import kotlinx.android.synthetic.main.view_proposals_activity_new.tv
import kotlinx.android.synthetic.main.view_proposals_activity_new.tvPointsValue
import kotlinx.android.synthetic.main.view_proposals_activity_new.tvProductTitle
import kotlinx.android.synthetic.main.view_proposals_activity_new.tv_descc
import kotlinx.android.synthetic.main.view_proposals_activity_new.tv_from_username
import kotlinx.android.synthetic.main.view_proposals_activity_new.tv_serviceTakenValuee
import kotlinx.android.synthetic.main.view_proposals_activity_new.tv_to_usernamee
import kotlinx.android.synthetic.main.view_proposals_activity_new.tvyouwillgivee
import kotlinx.android.synthetic.main.view_proposals_activity_new.tvyouwilll
import org.jetbrains.anko.enabled
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast


class MessagingActivity : BaseActivity(),
        ViewReminderContract.View,
        View.OnClickListener,
        ReminderContract.View,
        ReminderContract.ReferralCodeApiResult,
        InboxContract.View, ProposalsContract.View, ProfileDetailContract.View,
        CategoriesContract.View, ReviewProposalDialog.IRatingDialogListener, CustomServiceAddAlert.IDialogListener, ServiceContract.View {
    override fun onFoodListUnListSuccessResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFoodCompleteSuccessResult(message: String?) {

    }

    override fun onViewReminderSuccess(meta: com.tetoota.message.viewreminder.Meta) {
        Log.e("onViewReminderSuccess", "onViewReminderSuccess")
        if (meta != null) {
            btn_reminder.setBackgroundResource(R.drawable.rounded_complete_deal)
            btn_reminder.enabled = true
        }
    }

    private var wishListType = ""
    private var tetootaApplication: TetootaApplication? = null
    lateinit var mPotluckFoodDataResponse: PotluckFoodDataResponse
    private var exchangePostId: String = ""
    private var exchangePostType: String = ""
    private var mResponseTime: String = ""
    private var mQualityService: String = ""
    private var mFriendLines: String = ""
    private var mReviewMesg: String = ""
    private var acceptanceType: String = ""
    private lateinit var mProposal: ProposalByIdData
    private var mChatHistoryData: ChatHistoryDataResponse? = null
    private var mProposalMesgData: ProposalMessageData? = null
    private var chatArrayList: ArrayList<OneToOneChatDataResponse>? = null
    private lateinit var personData: LoginDataResponse
    private var mChatAdapter: ChatRoomAdapter? = null
    private var proposal_status: String = ""
    private var proposalExchangeID = ""
    private var proposalFromId = ""
    private var proposalToId = ""
    private var proposalTime = ""
    private var proposalExchangeType = ""
    private var proposalPostId = ""
    private var proposalId = ""
    var register: MenuItem? = null
    var action_report: MenuItem? = null
    private var cardUpdateReceiver: CardUpdateReceiver? = null
    private var isPreViousScreen = false
    var mServiceData: ServicesDataResponse? = null
    private var mList: List<ServicesDataResponse>? = null
    private var list = ArrayList<String>()
    private var serviceCount: Int = 0
    private var isAbalToSendProposal: Boolean = false
    private var serviceCreatorId: String = ""
    private var serviceReciverId: String = ""
    private var postId: String = ""
    private var senderId: String = ""
    private var postTitle = ""
    private var postImage = ""
    private var screenFrom = ""

    private lateinit var mRegistrationBroadcastReceiver: BroadcastReceiver
    val mViewReminderPresenter: ViewReminderPresenter by lazy {
        ViewReminderPresenter(this@MessagingActivity)
    }

    override fun onViewReminderFailureResponse(mString: String, isServerError: Boolean) {
        btn_reminder.setBackgroundResource(R.drawable.rounded_complete_deal_unselected)
        btn_reminder.enabled = false
        btn_reminder.setText(mString)
        var textStr: String = mString.substring(0, mString.lastIndexOf(" "))
        var time: String = mString.substring(mString.lastIndexOf(" ") + 1).trim()
        Log.e("mTimeReminder", time
                .toString())
        val timeArr = time.split(":").toTypedArray()

        var timeMilles: Long = (((timeArr[0].toInt() * 60) + timeArr[1].toInt()) * 60 + timeArr[2].toInt()).toLong() * 1000
        Log.e("timeMilles", timeMilles.toString())
        object : CountDownTimer(timeMilles.toLong(), 1000) {

            override fun onTick(timeMilles: Long) {
                var millisUntilFinished = timeMilles
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60
                val hoursInMilli = minutesInMilli * 60

                val elapsedHours = millisUntilFinished / hoursInMilli
                millisUntilFinished = millisUntilFinished % hoursInMilli

                val elapsedMinutes = millisUntilFinished / minutesInMilli
                millisUntilFinished = millisUntilFinished % minutesInMilli

                val elapsedSeconds = millisUntilFinished / secondsInMilli

                val yy = String.format("%02d:%02d:%02d", elapsedHours, elapsedMinutes, elapsedSeconds)
                btn_reminder.setText(textStr + " " + yy)
            }

            override fun onFinish() {
                btn_reminder.enabled = true
                btn_reminder.setBackgroundResource(R.drawable.rounded_complete_deal)
                btn_reminder.setText("Reminder")
            }
        }.start()
    }

    override fun onSubmitPressed(param: String, message: String, mResponseTime: String, mQualityService: String, mFriendLines: String, mReviewMesg: String) {
        if (Utils.haveNetworkConnection(this@MessagingActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            this.mResponseTime = mResponseTime
            this.mQualityService = mQualityService
            this.mFriendLines = mFriendLines
            this.mReviewMesg = mReviewMesg
            val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            if (proposalExchangeType.isEmpty() || personData.user_id == proposalFromId.toString()) {
                Log.e("TAG", "hhhhhh if =  " + acceptanceType)
                mProposalPresenter.proposalCompleteAction(this@MessagingActivity, proposalId.toString(), acceptanceType, "", "personData.referral_string!!", personData.user_id!!)
            } else {
                Log.e("TAG", "hhhhhh else =  " + acceptanceType)
                mProposalPresenter.proposalCompleteAction(this@MessagingActivity, proposalId.toString(), acceptanceType, "text", "personData.referral_string!!", personData.user_id!!)
            }
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onCancelPressed() {
    }

    override fun onReminderCodeApiSuccess(mReminderRenspose: Meta) {
        Log.e("TAG", "onReminderCodeApiSuccess")
    }

    override fun onReminderCodeApiFailure(mString: String, isServerError: Boolean) {
        Log.e("TAG", "onReminderCodeApiFailure")


    }

    override fun onReminderSuccess(mReminderRenspose: Meta) {
        Log.e("TAG", "onReminderSuccess")
        btn_reminder.setBackgroundResource(R.drawable.rounded_complete_deal_unselected)
        btn_reminder.enabled = false
        var mString: String = mReminderRenspose.message;
        btn_reminder.setText(mString)
        var textStr: String = mString.substring(0, mString.lastIndexOf(" "))
        var time: String = mString.substring(mString.lastIndexOf(" ") + 1).trim()
        Log.e("", time
                .toString())
        val timeArr = time.split(":").toTypedArray()

        var timeMilles: Long = (((timeArr[0].toInt() * 60) + timeArr[1].toInt()) * 60 + timeArr[2].toInt()).toLong() * 1000
        Log.e("timeMilles", timeMilles.toString())
        object : CountDownTimer(timeMilles.toLong(), 1000) {

            override fun onTick(timeMilles: Long) {
                var millisUntilFinished = timeMilles
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60
                val hoursInMilli = minutesInMilli * 60

                val elapsedHours = millisUntilFinished / hoursInMilli
                millisUntilFinished = millisUntilFinished % hoursInMilli

                val elapsedMinutes = millisUntilFinished / minutesInMilli
                millisUntilFinished = millisUntilFinished % minutesInMilli

                val elapsedSeconds = millisUntilFinished / secondsInMilli

                val yy = String.format("%02d:%02d:%02d", elapsedHours, elapsedMinutes, elapsedSeconds)
                btn_reminder.setText(textStr + " " + yy)
            }

            override fun onFinish() {
                btn_reminder.enabled = true
                btn_reminder.setBackgroundResource(R.drawable.rounded_complete_deal)
                btn_reminder.setText("Reminder")
            }
        }.start()

    }

    override fun onReminderFailureResponse(mString: String, isServerError: Boolean) {
        Log.e("TAG", "onReminderFailureResponse")

    }

    override fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String) {
    }

    override fun onCatergoriesFailureResult(message: String) {
    }

    override fun onLoginResult(message: Int, mesgDesc: String) {
    }

    override fun onMobileSuccess(message: Int, mesgDesc: String) {
    }

    override fun onDeepLinkingResult() {
    }

    //    private lateinit var mProposal: ProposalByIdData
    private val mInboxPresenter: InboxPresenter by lazy {
        InboxPresenter(this@MessagingActivity)
    }

    private val mProposalPresenter: ProposalsPresenter by lazy {
        ProposalsPresenter(this@MessagingActivity)
    }
    val mReminderPresenter: ReminderPresenter by lazy {
        ReminderPresenter(this@MessagingActivity)
    }
    private val mCategoriesPresenter: CategoriesPresenter by lazy {
        CategoriesPresenter(this@MessagingActivity)
    }

    override fun ongetAllProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
    }

    override fun ongetAllProposalFailure(message: String) {
    }

    override fun onChatMesgSuccess(message: String) {
        super.onChatMesgSuccess(message)
        hideProgressDialog()
        val oneToOneChat = OneToOneChatDataResponse()
        if (intent.getStringExtra("isProposalAccepted") != null) {
            oneToOneChat.msg_send_user_id = personData.user_id
            oneToOneChat.msg_recieved_user_id = mChatHistoryData?.msg_recieved_user_id.toString()
            oneToOneChat.post_id = mChatHistoryData?.post_id.toString()
            oneToOneChat.proposal_id = ""
            oneToOneChat.chatMesgDateTime = (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString())
            oneToOneChat.chatMessageId = ""
            oneToOneChat.chat_message = chatBoxText.text.toString()
            oneToOneChat.conver_id = ""

        } else {
            var reciverId = mProposalMesgData?.proposal_sender_id
            var postId = mProposalMesgData?.post_id
            oneToOneChat.msg_send_user_id = personData.user_id
            oneToOneChat.msg_recieved_user_id = reciverId
            oneToOneChat.post_id = postId
            oneToOneChat.proposal_id = ""
            oneToOneChat.chatMesgDateTime = (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString())
            oneToOneChat.chatMessageId = ""

            if (chatBoxText.text.isNotEmpty()) {
                oneToOneChat.chat_message = chatBoxText.text.toString()
            } else {
                oneToOneChat.chat_message = "Your Proposal has been accepted"
            }
            oneToOneChat.conver_id = ""
        }
        hideProgressDialog()
//        chatArrayList?.add(oneToOneChat)
//        if (mChatAdapter == null) {
//            mChatAdapter = ChatRoomAdapter(this@MessagingActivity, personData, chatArrayList!!)
//            chatList.adapter = mChatAdapter
//        } else
//            mChatAdapter!!.notifyDataSetChanged()
//        if (mChatAdapter!!.itemCount > 1) {
//            // scrolling to bottom of the recycler view
//            chatList.layoutManager?.smoothScrollToPosition(chatList, null, mChatAdapter!!.itemCount - 1)
//        }
        chatBoxText.setText("")
        fetchChat(personData.user_id.toString(), serviceCreatorId,
                personData.user_id.toString(), proposalId,screenFrom)
    }

    override fun onChatMesgFailure(message: String) {
        super.onChatMesgFailure(message)
        hideProgressDialog()
        showSnackBar(message)
    }

    private fun getProposalYes(proposalFrom: String, proposalTo: String, postId: String) {
        if (Utils.haveNetworkConnection(this@MessagingActivity)) {
            if (isAbalToSendProposal) {
                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                if (iv_exchangeServiceProduct.tag.equals("selected") && exchangePostId.equals("")) {
                    showSnackBar(Utils.getText(this, StringConstant.send_proposal_service_exchange_alert))
                } else {
                    if (iv_exchangeServiceProduct.tag.equals("selected")) {
                        /*************************proposalTime hindi english check ***************************************/

                        mInboxPresenter.getProposalYes(this@MessagingActivity,
                                mServiceData?.id.toString(), mServiceData?.user_id.toString(), personData.user_id!!
                        )
                    } else if (iv_points.tag.equals("selected")) {
                        println("Points Deduction ${personData.tetoota_points}")
                        val proposalTimeLocal: String = tv_time_with_in_new.text.toString() + " " + proposalTime
                        mInboxPresenter.getProposalYes(this@MessagingActivity,
                                mServiceData?.id.toString(), mServiceData?.user_id.toString(), personData.user_id!!)
                    }
                }
            } else {
                showSnackBar(Utils.getText(this, StringConstant.proposal_pending_alert))
            }
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    fun validation(): Boolean {
        if (chatBoxText.text.toString().trim().isEmpty()) {
            toast(Utils.getText(this, StringConstant.chat_message_alert))
            return false
        }
        return true
    }

    override fun onClick(p0: View?) {
        when (p0) {
            tv_sendProposal_new -> {
                var exChangeType = ""
                if (intent.getStringExtra("type") != null && intent.getStringExtra("type") == Constant.WISHLIST)
                    exChangeType = Constant.SERVICES
                else if (intent.getStringExtra("productType") == "food")
                    exChangeType = Constant.FOOD
                else if (intent.getStringExtra("productType") == "services")
                    exChangeType = Constant.SERVICES
//                Log.i(javaClass.name, "=============================exChangeType " + exChangeType)
                if (serviceCount > 0) {
                    if (!validate()) {
                    } else {
                        getProposalsData(exChangeType)
                    }
                } else {
                    CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                            this@MessagingActivity, getString(R.string.err_msg_mobile_number_limit)).show()
                }

            }
            iv_points -> {
                if (iv_points.tag.equals("unselected")) {
                    iv_points.tag = "selected"
                    iv_exchangeServiceProduct.tag = "unselected"
                    iv_points.setImageResource(R.drawable.ic_adjust_black_24dp)
                    iv_exchangeServiceProduct.setImageResource(R.drawable.ic_panorama_fish_eye_black_24dp)
                    //include_proposal_message.visibility = View.INVISIBLE
                }
            }
            rl_exchangeServiceProduct -> {
                if (iv_exchangeServiceProduct.tag.equals("selected")) {
                    //  openCustomExchangeServiceProductDialog("2", "Please select the item from list")
                    if (serviceCount > 0) {
                        spinner.performClick()
                    } else {
                        CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                this@MessagingActivity, getString(R.string.err_msg_mobile_number_limit)).show()
                    }

                }
            }
            iv_exchangeServiceProduct -> {
                if (iv_exchangeServiceProduct.tag.equals("unselected")) {
                    iv_exchangeServiceProduct.tag = "selected"
                    iv_points.tag = "unselected"
                    iv_exchangeServiceProduct.setImageResource(R.drawable.ic_adjust_black_24dp)
                    iv_points.setImageResource(R.drawable.ic_panorama_fish_eye_black_24dp)
                    if (iv_exchangeServiceProduct.tag.equals("selected")) {
                        if (serviceCount > 0) {
                            // openServiceProductListActivity("Services")
                            spinner.performClick()
                        } else {
                            CustomServiceAddAlert(this, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                    this@MessagingActivity, getString(R.string.err_msg_mobile_number_limit)).show()
                        }
                    }
                }
            }

            btnProposalYes -> {
                llSendProposalNew.visibility = View.GONE
                tv_sendProposal_new.setOnClickListener(this)
                getProposalYes("", "", "")
            }
            btn_yes -> {
                if (validationn()) {
                    mResponseTime = rb_response_timee.rating.toString()
                    mQualityService = rb_quality_servicee.rating.toString()
                    mFriendLines = rb_friendlinese.rating.toString()
                    mReviewMesg = et_messagee.text.toString().trim()
                    acceptanceType = "Complete"
                    if (Utils.haveNetworkConnection(this@MessagingActivity)) {
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                        this.mResponseTime = mResponseTime
                        this.mQualityService = mQualityService
                        this.mFriendLines = mFriendLines
                        this.mReviewMesg = mReviewMesg


                        val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                        val personData = Gson().fromJson(json, LoginDataResponse::class.java)

                        Log.e("personData", personData.user_id + "   " + proposalFromId)
                        if (proposalExchangeType.isEmpty() || personData.user_id == proposalFromId.toString()) {
                            Log.e("TAG", "hhhhhh if =  " + acceptanceType)
                            mProposalPresenter.proposalCompleteAction(this@MessagingActivity, proposalId.toString(), acceptanceType, "", "personData.referral_string!!", personData.user_id!!)
                        } else {
                            Log.e("TAG", "hhhhhh else =  " + acceptanceType)
                            mProposalPresenter.proposalCompleteAction(this@MessagingActivity, proposalId.toString(), acceptanceType, "text", "personData.referral_string!!", personData.user_id!!)
                        }
                    }
                }
            }
            send_bt -> {
                if (validation()) {
                    hideKeyboard(this)
                    if (com.tetoota.utility.Utils.haveNetworkConnection(this@MessagingActivity)) {
                        if (chatBoxText.text.toString().isNotEmpty()) {
                            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                            if (!isPreViousScreen) {
                                if (intent.getStringExtra("isProposalAccepted") != null) {
                                    if (personData.user_id.toString() == mChatHistoryData?.msg_send_user_id.toString()) {
                                        mInboxPresenter.sendMesg(this@MessagingActivity,
                                                personData.user_id, mChatHistoryData?.msg_recieved_user_id.toString(),
                                                mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!, Constant.SERVICES)
                                    } else if (personData.user_id.toString() == mChatHistoryData?.msg_recieved_user_id.toString()) {
                                        mInboxPresenter.sendMesg(this@MessagingActivity,
                                                personData.user_id, mChatHistoryData?.msg_send_user_id.toString(),
                                                mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!, Constant.SERVICES)
                                    }
                                } else {
                                    var reciverId = mServiceData?.user_id.toString()
                                    var postId = mServiceData?.id.toString()
                                    mInboxPresenter.sendMesg(this@MessagingActivity,
                                            personData.user_id, reciverId!!,
                                            postId!!, chatBoxText.text.toString().trim(), proposalId.toString(), Constant.SERVICES)
                                }
                            } else {
                                mInboxPresenter.sendMesg(this@MessagingActivity,
                                        personData.user_id, serviceCreatorId,
                                        postId, chatBoxText.text.toString().trim(), proposalId, Constant.SERVICES)
                            }
                        } else {
                            hideProgressDialog()
                        }
                    } else {
                        hideProgressDialog()
                        showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                    }
                }
            }

            btn_completeDeall -> {
                mProposalMesgData = ProposalMessageData()
                mProposalMesgData!!.proposal_id = proposalId
                if (!(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString()).equals(proposalFromId)) {
                    if (proposal_status.equals(Constant.PENDING, ignoreCase = true)) {
                        if (Utils.haveNetworkConnection(this@MessagingActivity)) {
                            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                            //proposal_id = mProposalMesgData!!.proposal_id
                            mProposalPresenter.proposalAction(this@MessagingActivity, mProposalMesgData!!, Constant.ACCEPT, 0, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), Constant.SERVICES)
                        } else {
                            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                        }
                    } else if (proposal_status.equals(Constant.ACCEPT, ignoreCase = true)) {
                        Toast.makeText(this, " " + proposal_status, Toast.LENGTH_LONG).show()
                        if (proposalExchangeType == "") {
                            acceptanceType = "Complete"
                            ReviewProposalDialog(this@MessagingActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                    this@MessagingActivity, "accept").show()
                        } else {
                            acceptanceType = "Complete"
                            ReviewProposalDialog(this@MessagingActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                    this@MessagingActivity, "accept").show()
                        }
                    }
                }
            }

            btn_cancelDeall -> {
                Log.i(javaClass.name, "=============" + proposalExchangeType)
                if (!TextUtils.isEmpty(wishListType) || proposalExchangeType.equals(Constant.WISHLIST, true)) {
                    if (Utils.haveNetworkConnection(this@MessagingActivity)) {
                        acceptanceType = "Cancel"
                        if (personData.user_id == proposalFromId) {
                            mProposalPresenter.proposalCompleteAction(this@MessagingActivity, proposalId, acceptanceType, "",
                                    "personData.referral_string!!", Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString())
                        }
                    } else {
                        showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                    }
                } else {
                    if ((Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString()).equals(proposalFromId))
                        acceptanceType = "Cancel"
                    else acceptanceType = "Decline"
                    mProposalPresenter.proposalActionCancel(this, proposalId, acceptanceType, 0, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), Constant.SERVICES)
                }
            }
            btn_cancelDDeal -> {
                acceptanceType = "Cancel"
                Log.i(javaClass.name, "======btn_cancelDDeal  " + acceptanceType + "  " + proposal_status)

                if (Utils.haveNetworkConnection(this@MessagingActivity)) {
                    if (personData.user_id == proposalFromId) {
                        mProposalPresenter.proposalCompleteAction(this@MessagingActivity, proposalId, acceptanceType, "",
                                "personData.referral_string!!", Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString())
                    } else {
                        mProposalPresenter.proposalCompleteAction(this@MessagingActivity, proposalId, acceptanceType, proposalToId,
                                "personData.referral_string!!", Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString())
                    }
                } else {
                    showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                }
            }
            btn_reminder -> {

//                if (personData.user_id.toString().equals(proposalFromId)) {
                mReminderPresenter.postReminder(this@MessagingActivity, proposalId, personData.user_id!!)
//
//                } else {
//                    mReminderPresenter.postReminder(this@MessagingActivity, proposalId, personData.user_id!!, proposalToId)
//                }
            }
        }
    }


    fun validationn(): Boolean {
        if (et_messagee.text.toString().isNullOrEmpty()) {
            this.toast("Please enter review message")
            return false
        }

        return true
    }

    private fun sendMessageMethod() {
        if (com.tetoota.utility.Utils.haveNetworkConnection(this@MessagingActivity)) {
            Log.i(javaClass.name, "==============isProposalAccepted  " + acceptanceType + "    " + intent.getStringExtra("isProposalAccepted"))
            if (intent.getStringExtra("isProposalAccepted") != null) {
                if (acceptanceType.equals("Cancel", true) || acceptanceType.equals("Decline", true)) {
                    Log.i(javaClass.name, "==============fgfr5y65 " + acceptanceType)
                    if (acceptanceType.equals("Cancel", true))
                        mInboxPresenter.sendMesg(this@MessagingActivity,
                                Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), serviceCreatorId,
                                postId, "Your Proposal has been canceled", proposalId!!, Constant.SERVICES)
                    else

                        mInboxPresenter.sendMesg(this@MessagingActivity,
                                personData.user_id, serviceCreatorId,
                                postId, "Your Proposal has been declined", proposalId!!, Constant.SERVICES)
                } else if ((Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString() == mChatHistoryData?.msg_send_user_id.toString()) && (!acceptanceType.equals("Cancel", true)
                                || !acceptanceType.equals("Decline", true))) {
                    fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity),
                            serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity), mChatHistoryData?.post_id!!,screenFrom)
//                    chatBoxText.setText("Your Proposal has been accepted")
//                    mInboxPresenter.sendMesg(this@MessagingActivity,
//                            Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), mChatHistoryData?.msg_recieved_user_id.toString(),
//                            mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!, Constant.SERVICES)
                }
            } else if (acceptanceType.equals("Cancel", true)
                    || acceptanceType.equals("Decline", true)) {
                if (acceptanceType.equals("Cancel", true))
                    chatBoxText.setText("Your Proposal has been canceled")
                else
                    chatBoxText.setText("Your Proposal has been declined")
                mInboxPresenter.sendMesg(this@MessagingActivity,
                        Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), serviceCreatorId,
                        postId, chatBoxText.text.toString(), proposalId!!, Constant.SERVICES)
            } else {
                var reciverId = mProposalMesgData?.proposal_sender_id
                var postId = mProposalMesgData?.post_id
                if (reciverId != "" && postId != "") {
                    fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity),
                            serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity), mChatHistoryData?.post_id!!,screenFrom)
//                    mInboxPresenter.sendMesg(this@MessagingActivity,
//                            Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), reciverId!!,
//                            postId!!, "Your Proposal has been accepted", mProposalMesgData?.proposal_id.toString(), Constant.SERVICES)
                }
            }
            //fetchChatHistory(personData, mChatHistoryData!!)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tetootaApplication = this.applicationContext as TetootaApplication
        setContentView(R.layout.activity_messaging)

        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
        send_bt.setOnClickListener(this)
        btn_completeDeall.setOnClickListener(this)
        btn_cancelDeall.setOnClickListener(this)
        btn_cancelDDeal.setOnClickListener(this)
        btn_yes.setOnClickListener(this)
        btnProposalYes.setOnClickListener(this)
        rl_exchangeServiceProduct.setOnClickListener(this)
        iv_exchangeServiceProduct.setOnClickListener(this)
        iv_points.setOnClickListener(this)
        llMainViewProposal.visibility = View.GONE
        llMainViewProposal.setOnClickListener(this)
        btn_reminder.setOnClickListener(this)
        println("Current time" + (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString()))
        setMultiLanguageText()
        getDataFromPReviousScreen()
        fetchWishListData()
        setSpannable()
        mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, intent: Intent) {
                if (intent.getStringExtra("messageAlert") == "Message Alert") {
                    println("Message Push Received")
                    getDataFromPReviousScreen()
                }
            }
        }
    }

    private fun getDataFromPReviousScreen() {
        val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingActivity)
        personData = Gson().fromJson(json, LoginDataResponse::class.java)
        chatArrayList = ArrayList<OneToOneChatDataResponse>()
        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        chatList.layoutManager = layoutManager
        /*   chatList.layoutManager = LinearLayoutManager(this@MessagingActivity)*/
        chatList.setHasFixedSize(true)
        if (intent.getStringExtra("type") != null)
            wishListType = intent.getStringExtra("type")
        if (intent.getStringExtra("isProposalAccepted") != null) {
            screenFrom = ""
            isPreViousScreen = true
            cv_main.visibility = View.GONE
            mChatHistoryData = intent.getParcelableExtra<ChatHistoryDataResponse>("inboxPojo")
            postTitle = mChatHistoryData!!.post_title!!.toString()
            postImage = mChatHistoryData!!.post_image!!.toString()
            postId = mChatHistoryData!!.post_id.toString()
            //fetchChatHistory(personData, mChatHistoryData!!)
            Log.i(javaClass.name, "=====adesad===========" + intent.getStringExtra("isProposalAccepted") + "   " + mChatHistoryData!!.proposal_to.toString() + "    " + mChatHistoryData!!.proposal_from.toString())
            if (personData.user_id == mChatHistoryData!!.msg_send_user_id.toString()) {
                serviceCreatorId = mChatHistoryData!!.msg_recieved_user_id.toString()
                senderId = mChatHistoryData!!.msg_send_user_id.toString()
                postId = mChatHistoryData!!.post_id.toString()
                initToolbar(mChatHistoryData!!.msg_reciever_name!!)
            } else {
                Log.i(javaClass.name, "=====adesad==sads=========  $postId")
                serviceCreatorId = mChatHistoryData!!.msg_send_user_id.toString()
                senderId == mChatHistoryData!!.msg_recieved_user_id.toString()
                postId = mChatHistoryData!!.post_id.toString()
                initToolbar(mChatHistoryData!!.msg_sender_name!!)
            }
        } else if (intent.getStringExtra("productType") == "services") {
            screenFrom = "Start"
            isPreViousScreen = true
            mServiceData = intent.getParcelableExtra("mProductData")
            initToolbar("${mServiceData!!.user_first_name.toString()} ${mServiceData!!.user_last_name.toString()}")
            serviceCreatorId = mServiceData!!.user_id.toString()
            postId = mServiceData!!.id.toString()
            println("Receiver$serviceCreatorId<=--->  $postId")
            Log.i(javaClass.name, "================dsfs  $serviceCreatorId")
            senderId = personData.user_id.toString()

        } else if (intent.getStringExtra("productType") == resources.getString(R.string.wish_list)) {
            screenFrom = "Start"
            isPreViousScreen = true
            mServiceData = intent.getParcelableExtra("mProductData")
            initToolbar("${mServiceData!!.user_first_name.toString()} ${mServiceData!!.user_last_name.toString()}")
            serviceCreatorId = mServiceData!!.user_id.toString()
            postId = mServiceData!!.id.toString()
            println("Receiver$serviceCreatorId<=--->  $postId")
            Log.i(javaClass.name, "================dsfw  $serviceCreatorId")
            senderId = personData.user_id.toString()

        } else {
            screenFrom = "Start"
            if (intent.getParcelableExtra<ProposalMessageData>("proposalMesgData") != null) {
                mProposalMesgData = intent.getParcelableExtra<ProposalMessageData>("proposalMesgData")
                cv_main.visibility = View.VISIBLE
                tv_from_username.text = "${mProposalMesgData!!.proposal_from_user_firstName.toString()} ${mProposalMesgData!!.proposal_from_user_lastName.toString()}"
                initToolbar("${mProposalMesgData!!.proposal_from_user_firstName.toString()} ${mProposalMesgData!!.proposal_from_user_lastName.toString()}")

                if (mProposalMesgData!!.exchange_post_id == "0") {
                    //Log.e("iffffff","" + mProposalMesg)
                    var iwant = Utils.getText(this, StringConstant.str_tv_wants)
                    var iwill = Utils.getText(this, StringConstant.str_tv_and_will_offer)
                    var sss = "<font color='#3a3a3a'> " + iwill + " </font> " + mProposalMesgData!!.exchange_post_title + " " + Utils.getText(this, StringConstant.str_tetoota_points)
                    var within = "<font color='#3a3a3a'> " + mProposalMesgData!!.proposal_time + "</font> "
                    tvProductTitle.text = (Html.fromHtml("<font color='#3a3a3a'>" + iwant + "</font> " + "\"" + mProposalMesgData!!.title + "\"" + "\"" + sss + "\"" + within))
                    // tv_proposal_desc.text = (mProposalMesgData!!.exchange_post_title + " Tetoota points")
                } else {
                    var iwant = Utils.getText(this, StringConstant.str_tv_wants)
                    var iwill = Utils.getText(this, StringConstant.str_tv_and_will_offer)
                    var sss = "<font color='#3a3a3a'> " + iwill + " </font> " + mProposalMesgData!!.exchange_post_title
                    var within = "<font color='#3a3a3a'> " + mProposalMesgData!!.proposal_time + "</font> "
                    tvProductTitle.text = (Html.fromHtml("<font color='#3a3a3a'>" + iwant + "</font> " + "\"" + mProposalMesgData!!.title + "\"" + "\"" + sss + "\"" + within))
                }

                if (mProposalMesgData!!.proposal_from_user_profile_img!!.isEmpty()) {
                    ivUser.setImageResource(R.drawable.user_placeholder);
                } else {
                    Picasso.get().load(mProposalMesgData!!.proposal_from_user_profile_img!!).into(ivUser)
                }


                serviceCreatorId = mProposalMesgData!!.proposal_sender_id.toString()
                postId = mProposalMesgData!!.post_id.toString()
                senderId = personData.user_id.toString()

            }
        }

        if (intent.getStringExtra("isProposalAccepted") != null) {

        } else if (intent.getParcelableExtra<ProposalMessageData>("proposalMesgData") != null) {

        }
        fetchChat(personData.user_id!!.toString(),
                serviceCreatorId, personData.user_id.toString(), postId,screenFrom)
    }

    /**
     * the m Proposal PushNotificationDataResponse
     */
    private fun initToolbar(mproPosalData: String): Unit {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar_title.text = mproPosalData
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, MessagingActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.mesg_screen_menu, menu)
        register = menu?.findItem(R.id.action_view_proposal)?.setIcon(R.drawable.complete_deal)
        register?.isVisible = false
        action_report = menu?.findItem(R.id.action_view_proposal)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.clear()
        menuInflater.inflate(R.menu.mesg_screen_menu, menu)
        register = menu?.findItem(R.id.action_view_proposal)?.setIcon(R.drawable.complete_deal)
        register?.isVisible = false
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_view_proposal) {

            return true
        } else if (id == android.R.id.home) {
            onBackPressed()
        } else if (id == R.id.action_report) {
            val intent = MainActivity.newMainIntent(this)
            intent!!.putExtra("KEY", "HELP")
            startActivity(intent)

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Utils.hideSoftKeyboard(this@MessagingActivity)
        ActivityStack.removeActivity(this@MessagingActivity)
        finish()
    }

    override fun onOneToOneSuccess(message: String, mChatList: ArrayList<OneToOneChatDataResponse>) {
        super.onOneToOneSuccess(message, mChatList)
        hideProgressDialog()
        if (mChatList.size > 0) {
            chatArrayList = mChatList
            mChatAdapter = ChatRoomAdapter(this@MessagingActivity, personData, chatArrayList!!)
            chatList.adapter = mChatAdapter
        }
        var size = mChatList!!.size
        var proposalFrom = mChatList[size - 1]!!.proposal_from.toString()
        var proposalTo = mChatList[size - 1]!!.proposal_to.toString()
        postId = mChatList[size - 1]!!.post_id.toString()
        proposalId = mChatList[size - 1]!!.proposal_id.toString()
        var exCh = ""
        if (!TextUtils.isEmpty(mChatList[size - 1]!!.exchange_post_type.toString()))
            exCh = mChatList[size - 1]!!.exchange_post_type.toString()
        else exCh = Constant.SERVICES
        if (TextUtils.isEmpty(proposalFrom) || proposalTo.equals("null", true)) {
            callGetAllStage(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), serviceCreatorId, postId, exCh)
        } else callGetAllStage(proposalFrom, proposalTo, postId, exCh)
    }

    override fun onOneToOneFailure(message: String) {
        super.onOneToOneFailure(message)
        //showSnackBar(message)
        callGetAllStage(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), serviceCreatorId, postId, Constant.SERVICES)
    }

    //     * Fetch Chat History
    private fun fetchChat(msg_send_user_id: String, msg_recieved_user_id: String, user_id: String,
                          post_id: String,by:String) {
        if (Utils.haveNetworkConnection(this@MessagingActivity)) {
            mInboxPresenter.getOneToOneChatData(this@MessagingActivity, msg_send_user_id,
                    msg_recieved_user_id, user_id, post_id, Constant.SERVICES,by)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onResume() {
        super.onResume()

        val intentFilter = IntentFilter("updateChat")
        cardUpdateReceiver = CardUpdateReceiver()
        this@MessagingActivity.registerReceiver(cardUpdateReceiver, intentFilter)

        TetootaApplication.activityResumed()
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                IntentFilter(Constant.PUSH_NOTIFICATION))
    }

    override fun onPause() {
        TetootaApplication.activityPaused()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver)
        super.onPause()
    }

    override fun onProposalsApiSuccessResult(message: String?) {
        hideProgressDialog()
        if (acceptanceType.equals(Constant.COMPLETE, ignoreCase = true)) {
            if (proposalExchangeType != "" && Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString() == proposalToId.toString()) {
                //Log.e("ifffffffffff", "" + personData.user_id)
                mProposalPresenter.addReviewApi(this@MessagingActivity, proposalToId.toString(), proposalExchangeID.toString()
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)

            } else if (Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString() == proposalFromId.toString()) {
                //Log.e(" elseeee ifffffffffff", "" + personData.user_id)
                mProposalPresenter.addReviewApi(this@MessagingActivity, proposalFromId, postId
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            } else {
                //Log.e("elseeeeeeeee", "" + proposalToId)
                mProposalPresenter.addReviewApi(this@MessagingActivity, proposalToId.toString(), postId
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            }
        } else {
            if (message != null) {
                cv_main.visibility = View.GONE
                showSnackBar(message)
            }
        }
        fetchChat(personData.user_id!!.toString(),
                serviceCreatorId, personData.user_id.toString(), postId,screenFrom)

    }

    override fun onProposalsByIdSuccessResult(message: String?, proposalByIdData: ProposalByIdData?) {
        super.onProposalsByIdSuccessResult(message, proposalByIdData)
        hideProgressDialog()

    }

    private fun setDataForPopup(proposalByIdData: ProposalByIdData) {
        //if condition is work for points
        if (proposalExchangeID == "0") {
            ll_serviceTaken.visibility = View.GONE
            llPoints.visibility = View.VISIBLE
            //Log.i(javaClass.name, "======dfdsf==========" + proposal_status)
            if (Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString() == proposalFromId) {
                tv.text = Utils.getText(this, StringConstant.str_tv_points) + " " + Utils.getText(this, StringConstant.proposal_by) + " " + proposalByIdData.sender_name!!.toString()
                tvPointsValue.text = proposalByIdData.tetoota_points!!.toString() + " tetoota points"
                tvProductTitle.setText(proposalByIdData.title!!.toString()).toString()
                var str = proposalByIdData.receiver_set_quota
                val newStr = str!!.replace("PER", "")
                tvyouwilll.text = Utils.getText(this, StringConstant.str_you_will_get) + " " + newStr + " " + Utils.getText(this, StringConstant.str_of)
//                tvyouwilll.text = Utils.getText(this, StringConstant.str_you_will_give) + " " + newStr + " " + Utils.getText(this, StringConstant.str_of)
                var str1 = proposalByIdData.set_quote
                val newStr1 = str1!!.replace("PER", "")
                tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_give).substring(0, (Utils.getText(this, StringConstant.str_you_will_give).length - 2))
                //Log.e("iffffff","" + mProposalMesg)
                var iwant = Utils.getText(this, StringConstant.str_tv_wants)
                var iwill = Utils.getText(this, StringConstant.str_tv_and_will_offer)
                var sss = "<font color='#3a3a3a'> " + iwill + " </font> " + proposalByIdData.exchange_post_title.toString() + " " + Utils.getText(this, StringConstant.str_tetoota_points)
                //tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_give_points)
                tv_to_usernamee.text = "${proposalByIdData.sender_name!!.toString().toString()} "
                tv_from_username.setText(Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.receiver_name!!.toString()).toString()

                Picasso.get().load(R.drawable.iv_tetoota_icon).placeholder(R.drawable.iv_tetoota_icon).into(iv_user3)
//            }
                if (proposalByIdData.sender_profile_image!!.toString()!!.isEmpty()) {
                    ivUser.setImageResource(R.drawable.user_placeholder);
                } else {
                    Picasso.get().load(proposalByIdData.sender_profile_image!!.toString()!!).into(ivUser)
                }
            } else {
                tv.text = Utils.getText(this, StringConstant.str_tv_points) + " " + Utils.getText(this, StringConstant.proposal_by) + " " + proposalByIdData.sender_name!!.toString()
                tvProductTitle.text = proposalByIdData.tetoota_points!!.toString() + " tetoota points"
                tvPointsValue.setText(proposalByIdData.title!!.toString()).toString()
                var str = proposalByIdData.receiver_set_quota
                val newStr = str!!.replace("PER", "")
                tvyouwilll.text = Utils.getText(this, StringConstant.str_you_will_get).substring(0, (Utils.getText(this, StringConstant.str_you_will_get).length - 2))
                var str1 = proposalByIdData.set_quote
                val newStr1 = str1!!.replace("PER", "")
                tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_give) + "" + newStr1 + " " + Utils.getText(this, StringConstant.str_of)
                //Log.e("iffffff","" + mProposalMesg)
                var iwant = Utils.getText(this, StringConstant.str_tv_wants)
                var iwill = Utils.getText(this, StringConstant.str_tv_and_will_offer)
                var sss = "<font color='#3a3a3a'> " + iwill + " </font> " + proposalByIdData.exchange_post_title.toString() + " " + Utils.getText(this, StringConstant.str_tetoota_points)
                //tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_give_points)
                tv_to_usernamee.text = "${proposalByIdData.sender_name!!.toString().toString()} "
                tv_from_username.setText(Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.receiver_name!!.toString()).toString()
//            if (proposalByIdData.profile_image!!.toString().isEmpty()) {
//                iv_user3.setImageResource(R.drawable.user_placeholder);
//            } else {

                Picasso.get().load(R.drawable.iv_tetoota_icon).placeholder(R.drawable.iv_tetoota_icon).into(ivUser)
//            }
                if (proposalByIdData.sender_profile_image!!.toString()!!.isEmpty()) {
                    iv_user3.setImageResource(R.drawable.user_placeholder);
                } else {
                    Picasso.get().load(proposalByIdData.sender_profile_image!!.toString()!!).into(iv_user3)
                }
            }
            // tv_proposal_desc.text = (mProposalMesgData!!.exchange_post_title + " Tetoota points")
        } else {
            ll_serviceTaken.visibility = View.VISIBLE
            llPoints.visibility = View.GONE
            tv.text = Utils.getText(this, StringConstant.str_exchange_service_product) + " " + Utils.getText(this, StringConstant.proposal_by) + " " + proposalByIdData.sender_name!!.toString()
            Log.i(javaClass.name, "======dfdsf==========ELSE" + proposalByIdData.title!!.toString() + "   " + proposalByIdData.set_quota)
            if (Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString() == proposalFromId) {
                tv_serviceTakenValuee.text = proposalByIdData.exchange_post_title.toString()
                var iwant = Utils.getText(this, StringConstant.str_tv_wants)
                var iwill = Utils.getText(this, StringConstant.str_tv_and_will_offer)
                var sss = "<font color='#3a3a3a'> " + iwill + " </font> " + proposalByIdData.exchange_post_title.toString()
                tvProductTitle.text = proposalByIdData.title!!.toString()
                var str = proposalByIdData.receiver_set_quota
                val newStr = str!!.replace("PER", "")
                tvyouwilll.text = Utils.getText(this, StringConstant.str_you_will_get) + "" + newStr + " " + Utils.getText(this, StringConstant.str_of)
                var str1 = proposalByIdData.sender_set_quota
                val newStr1 = str1!!.replace("PER", "")
                tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_give) + "" + newStr1 + " " + Utils.getText(this, StringConstant.str_of)
                tv_to_usernamee.text = "${proposalByIdData.sender_name!!.toString()} "
                tv_from_username.setText(Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.receiver_name!!.toString()).toString()
                if (proposalByIdData.receiver_profile_image!!.toString()!!.isEmpty()) {
                    ivUser.setImageResource(R.drawable.user_placeholder);
                } else {

                    Picasso.get().load(proposalByIdData.receiver_profile_image!!.toString()).placeholder(R.drawable.user_placeholder).into(ivUser)
                }
                if (proposalByIdData.sender_profile_image!!.toString()!!.isEmpty()) {
                    iv_user3.setImageResource(R.drawable.user_placeholder);
                } else {
                    Picasso.get().load(proposalByIdData.sender_profile_image!!.toString()!!).into(iv_user3)
                }
            } else {
                tvProductTitle.text = proposalByIdData.exchange_post_title.toString()
                tv_serviceTakenValuee.text = proposalByIdData.title!!.toString()
                var iwant = Utils.getText(this, StringConstant.str_tv_wants)
                var iwill = Utils.getText(this, StringConstant.str_tv_and_will_offer)
                var sss = "<font color='#3a3a3a'> " + iwill + " </font> " + proposalByIdData.exchange_post_title.toString()
                var str = proposalByIdData.sender_set_quota
                val newStr = str!!.replace("PER", "")
                tvyouwilll.text = Utils.getText(this, StringConstant.str_you_will_get) + "" + newStr + " " + Utils.getText(this, StringConstant.str_of)
                var str1 = proposalByIdData.receiver_set_quota
                val newStr1 = str1!!.replace("PER", "")
                tvyouwillgivee.text = Utils.getText(this, StringConstant.str_you_will_give) + "" + newStr1 + " " + Utils.getText(this, StringConstant.str_of)
                tv_from_username.text = "${proposalByIdData.sender_name!!.toString()} "
                tv_to_usernamee.setText(Utils.getText(this, StringConstant.str_by) + " " + proposalByIdData.receiver_name!!.toString()).toString()
                if (proposalByIdData.sender_profile_image!!.toString()!!.isEmpty()) {
                    ivUser.setImageResource(R.drawable.user_placeholder);
                } else {
                    Picasso.get().load(proposalByIdData.sender_profile_image!!.toString()).placeholder(R.drawable.user_placeholder).into(ivUser)
                }
                if (proposalByIdData.receiver_profile_image!!.toString()!!.isEmpty()) {
                    iv_user3.setImageResource(R.drawable.user_placeholder);
                } else {
                    Picasso.get().load(proposalByIdData.receiver_profile_image!!.toString()!!).into(iv_user3)
                }

                // tv_proposal_desc.text = mProposalMesgData!!.exchange_post_title
            }
        }


    }

    override fun onProposalActionSuccessREsult(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {
        super.onProposalActionSuccessREsult(message, mProposalMesg, acceptanceType, pos)
        hideProgressDialog()
        if (acceptanceType == Constant.ACCEPT) {
            cv_main.visibility = View.GONE
            llFoodMain.visibility = View.GONE
            showSnackBar(message.toString())
            /*isMenuVisiable = true
            invalidateOptionsMenu()*/
            fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity),
                    serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity), mChatHistoryData?.post_id!!,screenFrom)
        } else {
            llFoodMain.visibility = View.GONE
            InboxFragment.isAcceptedRejected = true
            showSnackBar(message.toString())
        }
    }

    override fun onProposalsFailureResult(message: String) {
        super.onProposalsFailureResult(message)
        hideProgressDialog()
        if (message != null) {
            showSnackBar(message)
        }
    }

    private fun setMultiLanguageText() {
        tv_time_with_in_new.text = "Within"
        tv_points_points_new.text = Utils.getText(this, StringConstant.str_tv_points)
        tvShowMsg.text = Utils.getText(this, StringConstant.str_proposal_ready)
        btnProposalYes.text = Utils.getText(this, StringConstant.signout_yes)
        btn_completeDeall.text = Utils.getText(this, StringConstant.accept)
        btn_cancelDDeal.text = Utils.getText(this, StringConstant.decline)
        chatBoxText.hint = Utils.getText(this, StringConstant.str_chatBoxText)
        tv_qualityService.text = Utils.getText(this, StringConstant.str_quality)
        tv_responseTime.text = Utils.getText(this, StringConstant.str_responsetime)
        tv_friendliness.text = Utils.getText(this, StringConstant.str_friendlines)
        btn_yes.text = Utils.getText(this, StringConstant.complete_deal)
        tv_rate_trade.text = Utils.getText(this, StringConstant.review_message)
        tv_name.text = Utils.getText(this, StringConstant.review_title)
        //For Food Request
        tvFoodFriendliness.text = Utils.getText(this, StringConstant.str_friendlines)
        tvFoodResponseTime.text = Utils.getText(this, StringConstant.str_responsetime)
        tvFoodQualityService.text = Utils.getText(this, StringConstant.str_quality)
        btnFoodAccept.text = Utils.getText(this, StringConstant.accept)
        btnFoodCancel.text = Utils.getText(this, StringConstant.cancel)
        btnFoodSubmit.text = Utils.getText(this, StringConstant.str_submit)
    }

    //Intialize Brodcast Reciver
    inner class CardUpdateReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                Log.e("===========testtttttttt", "  " + intent.getIntExtra(Constant.PROPOSAL_TO, 0) + "   " + intent.getIntExtra(Constant.PROPOSAL_FROM, 0))
                Log.i(javaClass.name, "======================CardUpdateReceiver  " + personData.user_id.toString() + "   " + serviceCreatorId)
                if (intent.getIntExtra(Constant.PROPOSAL_TO, 0) == serviceCreatorId.toInt() || intent.getIntExtra(Constant.PROPOSAL_FROM, 0) == serviceCreatorId.toInt())
                    fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(),
                            serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(),
                            proposalId,screenFrom)
            }
        }

    }

    private fun setSpannable() {
        val wordtoSpan = SpannableString("In case you do not receive points or service mentioned, you can contact us")

        wordtoSpan.setSpan(ForegroundColorSpan(Color.BLUE), 0, wordtoSpan.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        tv_descc.setText(wordtoSpan);
        tv_descc.paintFlags = tv_descc.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG
        tv_descc.setOnClickListener(this)
    }

    fun gettetootaPts(): Int {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        println("Points Deduction ${personData.tetoota_points}")
        return Integer.parseInt(personData.tetoota_points.toString())
    }


    override fun onAddReviewApISuccessResult(message: String?) {
        super.onAddReviewApISuccessResult(message)
        openSharingDialog()
        cv_main.visibility = View.GONE
        if (message != null) {
            showSnackBar(message)
        }
        callUserDetailAPI()
    }

    override fun onAddReviewApiFailureResult(message: String?) {
        super.onAddReviewApiFailureResult(message)
        hideProgressDialog()
        if (message != null) {
            showSnackBar(message)
        }
    }

    private fun callUserDetailAPI() {
        if (Utils.haveNetworkConnection(this@MessagingActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingActivity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileData(this@MessagingActivity, personData)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
//            clearStack()
//            openSharingDialog()
        }
    }

    private fun openSharingDialog() {
        try {
            val alertDialog = AlertDialog.Builder(this) //Read Update
            alertDialog.setTitle(Utils.getText(this, StringConstant.send_proposal_share_trading))
            alertDialog.setMessage(Utils.getText(this, StringConstant.send_proposal_share_trading_alert))
            alertDialog.setPositiveButton(Utils.getText(this, StringConstant.tetoota_ok), object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, which: Int) {
                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingActivity)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    val postString = "Tetoota" + " has exchanged " + "Free Skill Share" + " on http://tetoota.com"
                    shareProduct(postString, "http://tetoota.com/assets/images/sharing_image/sharing.png")
                }
            })
            alertDialog.setNegativeButton(Utils.getText(this, StringConstant.cancel), object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, which: Int) {
                    onBackPressed()
                }
            })
            alertDialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun shareProduct(mTitle: String, mImageUrl: String) {
        try {
            val text = mTitle
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                        BitmapFactory.decodeResource(getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
        } catch (ex: android.content.ActivityNotFoundException) {
        }
    }

    fun clearStack() {
        hideProgressDialog()
        val intent = MainActivity.newMainIntent(this@MessagingActivity)
        ActivityStack.getInstance(this@MessagingActivity)
        ActivityStack.cleareAll()
        startActivity(intent)
        finish()
    }


    override fun onOptionalInformationApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        val profileData: ProfileDataResponse = message?.get(0)!!
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        personData.tetoota_points = profileData.tetoota_points
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@MessagingActivity)
        hideProgressDialog()
//        clearStack()
       // openSharingDialog()
    }

    override fun onProfileSendEmailApiSuccessResult(message: String) {
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
//        clearStack()
       // openSharingDialog()
    }

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@MessagingActivity)
    }


    private fun callGetAllStage(proposalFrom: String, proposalTo: String, postId: String, exChangetype: String) {
        if (Utils.haveNetworkConnection(this@MessagingActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingActivity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mInboxPresenter.getStageData(this@MessagingActivity, proposalFrom!!, proposalTo, postId, "", exChangetype,
                    Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString(), screenFrom)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
//            clearStack()
           // openSharingDialog()
        }
    }

    override fun onGetStageSuccess(message: String, chatStatus: String, data: ProposalByIdData) {
        hideProgressDialog()
        if (data != null && !chatStatus.equals(Constant.START_TRADING, true)) {
            proposalId = data.proposal_id!!.toString()
            proposalFromId = data.proposal_from!!.toString()
            proposalToId = data.proposal_to!!.toString()
            proposal_status = data.proposal_status!!.toString()
            proposalExchangeType = data.exchange_post_type!!.toString()
            proposalExchangeID = data.exchange_post_id!!.toString()
        }
        btnBFoodCancel.visibility = View.GONE
        if (chatStatus.equals(Constant.START_TRADING, true)) {
            getHeightLayout()
            if (mServiceData != null) {
                llSendProposalNew.visibility = View.VISIBLE
                cv_main.visibility = View.VISIBLE
                llProposalView.visibility = View.VISIBLE
                rl_maincontent.visibility = View.GONE
                llMainViewProposal.visibility = View.GONE
                isAbalToSendProposal = true
                setDataFromPreviousScreenForSendProposal()
            } else cv_main.visibility = View.GONE
            btn_reminder.visibility = View.GONE
        } else if (chatStatus.equals(Constant.ACCEPTED, true)) {
            proposalExchangeType = data.exchange_post_type!!.toString()
            btn_cancelDDeal.text = Utils.getText(this, StringConstant.cancel_deal)
            if (data.exchange_post_id.toString() == "0") {
                if (!TextUtils.isEmpty(wishListType) || proposalExchangeType.equals(Constant.WISHLIST)) {
                    if (data.proposal_to!!.toString().equals(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString())) {
                        llMainViewProposal.visibility = View.GONE
                        cv_main.visibility = View.VISIBLE
                        llProposalView.visibility = View.GONE
                        rl_maincontent.visibility = View.VISIBLE
                    } else {
                        setDataForPopup(data)
                        llMainViewProposal.visibility = View.VISIBLE
                        cv_main.visibility = View.VISIBLE
                        llProposalView.visibility = View.GONE
                        rl_maincontent.visibility = View.GONE
                        btn_completeDeall.text = Utils.getText(this, StringConstant.str_accepted)
                        btn_cancelDeall.text = Utils.getText(this, StringConstant.cancel_deal)
                    }
                } else {
                    if (data.proposal_from!!.toString().equals(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString())) {
                        llMainViewProposal.visibility = View.GONE
                        cv_main.visibility = View.VISIBLE
                        llProposalView.visibility = View.GONE
                        rl_maincontent.visibility = View.VISIBLE
                    } else {
                        cv_main.visibility = View.GONE
                    }
                }
            } else {
                llMainViewProposal.visibility = View.GONE
                cv_main.visibility = View.VISIBLE
                llProposalView.visibility = View.GONE
                rl_maincontent.visibility = View.VISIBLE
            }
        } else if (chatStatus.equals(Constant.ON_GOING, true)) {
            if (proposalExchangeID.equals("0")) {
                Log.e("TAG", "ON_GOING")
                if (data.proposal_from.toString().equals(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString())) {
                    if (data.proposal_status.equals(Constant.ACCEPT) && data.trading_status.equals(Constant.PENDING)) {
                        //setDataAfterAccept(data)
                        llProposalView.visibility = View.GONE
                        cv_main.visibility = View.VISIBLE
                        rl_maincontent.visibility = View.VISIBLE
                        llMainViewProposal.visibility = View.GONE
                        btn_cancelDDeal.text = Utils.getText(this, StringConstant.cancel_deal)
                    } else {
                        btn_completeDeall.text = Utils.getText(this, StringConstant.accept)
                        btn_cancelDeall.text = Utils.getText(this, StringConstant.decline)
                        cv_main.visibility = View.GONE
                    }
                }
                btn_reminder.visibility = View.GONE
            } else {
                if (data.proposal_status.equals(Constant.ACCEPT, ignoreCase = true)
                        && data.receiver_trading_status.equals(Constant.PENDING, ignoreCase = true)
                        && data.trading_status.equals(Constant.COMPLETE, ignoreCase = true)) {
                    Log.e("TAGAA", "b e if" + data.proposal_status + " " + data.receiver_trading_status + " " + data.trading_status)
                    if (personData.user_id == data.proposal_from.toString() && data.proposal_from!! > 0) {
//                        btn_completeDeal.visibility = View.GONE
                        btn_reminder.visibility = View.VISIBLE
                        mViewReminderPresenter.viewReminder(this@MessagingActivity, proposalId!!, personData.user_id!!)
                        cv_main.visibility = View.GONE
                        Log.e("b e if if", data.proposal_status + " " + data.receiver_trading_status + " " + data.trading_status)
                    } else {
//                        btn_completeDeal.visibility = View.VISIBLE
                        btn_reminder.visibility = View.GONE
                        llProposalView.visibility = View.GONE
                        cv_main.visibility = View.VISIBLE
                        rl_maincontent.visibility = View.VISIBLE
                        llMainViewProposal.visibility = View.GONE
                        Log.e("b e if else", data.proposal_status + " " + data.receiver_trading_status + " " + data.trading_status)

                    }
                } else if (data.proposal_status.equals(Constant.ACCEPT, ignoreCase = true)
                        && data.receiver_trading_status.equals(Constant.COMPLETE, ignoreCase = true)
                        && data.trading_status.equals(Constant.PENDING, ignoreCase = true)) {
                    Log.e("c else if", data.proposal_status + " " + data.receiver_trading_status + " " + data.trading_status)
                    if (personData.user_id == data.proposal_from.toString() && data.proposal_from!! > 0) {
                        Log.e("c else if if", data.proposal_status + " " + data.receiver_trading_status + " " + data.trading_status)
//                        btn_completeDeal.visibility = View.VISIBLE
                        llProposalView.visibility = View.GONE
                        cv_main.visibility = View.VISIBLE
                        rl_maincontent.visibility = View.VISIBLE
                        llMainViewProposal.visibility = View.GONE
                        btn_reminder.visibility = View.GONE
                    } else {
                        cv_main.visibility = View.GONE
//                        btn_completeDeal.visibility = View.GONE
                        btn_reminder.visibility = View.VISIBLE
                        mViewReminderPresenter.viewReminder(this@MessagingActivity, proposalId!!, personData.user_id!!)
                    }
                } else {
                    Log.e("d else", data.receiver_trading_status + " " + data.trading_status)
                    if (personData.user_id == data.proposal_from.toString() && data.proposal_from!! > 0) {
                        Log.e("d else if", data.proposal_status + " " + data.receiver_trading_status + " " + data.trading_status)

                        if (data.proposal_status.equals(Constant.ACCEPT, ignoreCase = true)
                                && data.trading_status.equals(Constant.PENDING, ignoreCase = true)) {
                            Log.e("d else if if", data.proposal_status + " " + data.receiver_trading_status + " " + data.trading_status)

//                            btn_completeDeal.visibility = View.VISIBLE
                            btn_reminder.visibility = View.GONE
                        }
                    } else if (data.proposal_status.equals(Constant.ACCEPT, ignoreCase = true)
                            && data.receiver_trading_status.equals(Constant.PENDING, ignoreCase = true)) {
                        Log.e("d e else else if", data.proposal_status + " " + data.receiver_trading_status + " " + data.trading_status)

                        btn_reminder.visibility = View.GONE
                    }
                }
            }
        } else if (chatStatus.equals(Constant.PROPOSAL_PENDING, true)) {
            llFood.visibility = View.GONE
            if (data.proposal_from!!.toString().equals(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString())) {
                btn_completeDeall.text = Utils.getText(this, StringConstant.str_proposal_sent)
                btn_cancelDeall.text = Utils.getText(this, StringConstant.cancel)
            } else {
                btn_completeDeall.text = Utils.getText(this, StringConstant.accept)
                btn_cancelDeall.text = Utils.getText(this, StringConstant.decline)
            }
            setDataForPopup(data)
            llMainViewProposal.visibility = View.VISIBLE
            cv_main.visibility = View.VISIBLE
            llProposalView.visibility = View.GONE
            rl_maincontent.visibility = View.GONE
        } else if (chatStatus.equals(Constant.COMPLETED, true)) {

            cv_main.visibility = View.GONE
        } else if (chatStatus.equals(Constant.DECLINE, true) || chatStatus.equals(Constant.CANCEL, true)) {
            cv_main.visibility = View.GONE
        }
    }

    private var listType = ""
    private fun setDataFromPreviousScreenForSendProposal() {
        try {
            if (mServiceData!!.profile_image!!.isEmpty()) {
                ivUser.setImageResource(R.drawable.user_placeholder);
            } else {
                Picasso.get().load(mServiceData!!.profile_image).placeholder(R.drawable.lohgo).into(ivUser)
            }
            var str = mServiceData!!.set_quote
            val newStr = str!!.replaceFirst("PER", "")
            // tv_i_want.text = "I want 1 " +newStr+" of "
//            if (wishListType.equals(getString(R.string.wish_list))) {
//                tv_i_want.text = Utils.getText(this, StringConstant.str_tv_will_offer)
//                tv_will_offer_new.text = Utils.getText(this, StringConstant.str_you_will_offer)
//            } else
            tv_i_want.text = Utils.getText(this, StringConstant.str_tv_i_want) + " " + newStr + " " + Utils.getText(this, StringConstant.str_of)

            tvSentProductTitle.text = mServiceData!!.title
            tv_from_username.text = Utils.getText(this, StringConstant.includeproposal_by) + "(${mServiceData!!.user_first_name.toString()})"
            tv_exchangeServiceProduct.text = Utils.getText(this, StringConstant.include_proposal_exchange_title)
            if (mServiceData!!.profile_image!!.toString()!!.isEmpty()) {
                ivSentUser.setImageResource(R.drawable.user_placeholder);
            } else {

                Picasso.get().load(mServiceData!!.profile_image!!.toString()).placeholder(R.drawable.user_placeholder).into(ivSentUser)
            }
//            if (mServiceData!!.trading_preference == "1") {
//                ll_points_new.visibility = View.VISIBLE
//                iv_points.visibility = View.VISIBLE
//                rl_exchangeServiceProduct.visibility = View.VISIBLE
//                iv_exchangeServiceProduct.visibility = View.VISIBLE
//            } else if (mServiceData!!.trading_preference == "2") {
//                ll_points_new.visibility = View.VISIBLE
//                iv_points.visibility = View.INVISIBLE
//                rl_exchangeServiceProduct.visibility = View.GONE
//            } else if (mServiceData!!.trading_preference == "3") {
//                ll_points_new.visibility = View.GONE
//                rl_exchangeServiceProduct.visibility = View.VISIBLE
//                iv_exchangeServiceProduct.tag = "selected"
//                iv_exchangeServiceProduct.visibility = View.INVISIBLE
//            }
            if (!mServiceData!!.tetoota_points?.isEmpty()!!) {
                ll_points_new.visibility = View.VISIBLE
                tv_points_neww.text = mServiceData!!.tetoota_points.toString()
            } else {
                ll_points_new.visibility = View.GONE
            }
            tv_unitt.text = mServiceData!!.set_quote.toString()
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    if (position > 0) {

                        exchangePostId = mList!![position - 1].id.toString()
                        exchangePostType = mList!![position - 1].post_type.toString()
                        tv_exchangeServiceProduct.text = mList!![position - 1].title
                        var str = mList!![position - 1].set_quote
                        val newStr = str!!.replaceFirst("PER", "")
                        tv_per_new.text = "1 " + newStr
                        // Toast.makeText(this@IncludeProposalsActivity,  "Selected item =  " + list[position], Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

    }

    private fun getHeightLayout() {
        val vto = scrollView.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    scrollView.getViewTreeObserver().removeGlobalOnLayoutListener(this)
                } else {
                    scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this)
                }
                val width = scrollView.getMeasuredWidth()
                val height = scrollView.getMeasuredHeight()
                val params = llSendProposalNew.layoutParams
// Changes the height and width to the specified *pixels*
                params.height = height
                llSendProposalNew.setLayoutParams(params)
            }
        })
    }

    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@MessagingActivity)
    }

    private fun fetchWishListData() {
        if (Utils.haveNetworkConnection(this@MessagingActivity)) {
            Log.i(javaClass.name, "=[=============" + wishListType)
            if (TextUtils.isEmpty(wishListType))
                mServicePresenter.getServicesData(this@MessagingActivity, Constant.SERVICES, 1, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity).toString())
            else
                mServicePresenter.getServicesData(this@MessagingActivity, Constant.SERVICES, 1, serviceCreatorId)
        } else {
            toast(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onWishListApiSuccessResult(myServiceList: List<Any?>?, message: String) {
        Log.e("SIZE_LIST", "SIZE_LIST")
        hideProgressDialog()
        mList = myServiceList as List<ServicesDataResponse>

        if (myServiceList != null) {
            Log.e("sizeeeeeeeee ", "" + myServiceList.size)
            serviceCount = myServiceList!!.size


            list.add("Exchange Service/Product")
            for (model in mList!!) {
                list.add(model.title!!)
            }
            spinnerservice(list)
        } else {
            serviceCount = 0
        }

    }

    fun spinnerservice(list: List<String>?) {
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, list!!)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        //Setting the ArrayAdapter data on the Spinner
        spinner.adapter = aa
    }


    override fun onGetStageFailure(message: String) {
        hideProgressDialog()
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        hideProgressDialog()
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onYes(param: String, message: String) {
        val intent = AddPostRequestActivity.newMainIntent(this)
        intent!!.putExtra("Tab", "serviceTab")
        ActivityStack.getInstance(this!!)
        startActivity(intent)
        finish()
    }

    private fun validate(): Boolean {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        val requiredTetootaPoints: Int? = mServiceData!!.tetoota_points.toString().trim().toIntOrNull()

        if (ll_points_new.visibility == View.VISIBLE && requiredTetootaPoints!! > 0
                && requiredTetootaPoints > personData.tetoota_points!!.toInt() && iv_points.tag.equals("selected")) {
            //  Log.e("vvvvvvvvvv", "" + personData.tetoota_points!!)
            // Log.e("requiredTetootaPoints", "" + requiredTetootaPoints)
            toast(Utils.getText(this, StringConstant.send_proposal_insufficient_point_alert))
            return false
        }
        return true
    }

    private fun getProposalsData(exChangePostTypee: String) {
        if (Utils.haveNetworkConnection(this@MessagingActivity)) {

            if (isAbalToSendProposal) {
                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                if (iv_exchangeServiceProduct.tag.equals("selected") && exchangePostId.equals("")) {
                    showSnackBar(Utils.getText(this, StringConstant.send_proposal_service_exchange_alert))
                } else {
                    if (iv_exchangeServiceProduct.tag.equals("selected")) {
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))

                        /*************************proposalTime hindi english check ***************************************/

                        mProposalPresenter.getProposalsData(this@MessagingActivity,
                                mServiceData?.id.toString(), mServiceData?.user_id.toString(), personData.user_id!!,
                                "0", exchangePostId, exChangePostTypee, "", Constant.SERVICES)
                    } else if (iv_points.tag.equals("selected")) {
                        println("Points Deduction ${personData.tetoota_points}")
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))

                        val proposalTimeLocal: String = tv_time_with_in_new.text.toString() + " " + proposalTime
                        mProposalPresenter.getProposalsData(this@MessagingActivity,
                                mServiceData?.id.toString(), mServiceData?.user_id.toString(), personData.user_id!!,
                                mServiceData?.tetoota_points.toString(), exchangePostId, exChangePostTypee, proposalTimeLocal, Constant.SERVICES)
                    }
                }
            } else {
                showSnackBar(Utils.getText(this, StringConstant.proposal_pending_alert))
            }
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm!!.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onProposalActionCancelSuccessREsult(message: String?, propoalId: String, acceptanceType: String, pos: Int) {
        super.onProposalActionCancelSuccessREsult(message, propoalId, acceptanceType, pos)
        hideProgressDialog()
        fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity),
                serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingActivity), postId,screenFrom)
    }

    override fun onStop() {
        super.onStop()
        if (cardUpdateReceiver != null) {
            this@MessagingActivity!!.unregisterReceiver(cardUpdateReceiver)
        }
    }
}
