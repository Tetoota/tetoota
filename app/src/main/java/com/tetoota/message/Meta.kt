package com.tetoota.message

import com.google.gson.annotations.SerializedName

data class Meta(

        val code: Int,
        val status: Boolean,
        val message: String,
        val time: Int
)