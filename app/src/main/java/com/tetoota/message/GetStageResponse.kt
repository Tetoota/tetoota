package com.tetoota.message

import com.tetoota.fragment.dashboard.DashboardSliderDataResponse
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.proposal.ProposalByIdData

data class GetStageResponse(val data: ProposalByIdData,
                            val meta: Meta? = null) {
}