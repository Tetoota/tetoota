package com.tetoota.message

import android.app.Activity
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.message.viewreminder.Meta
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class ReminderInteractor {
    var call: Call<ResponseBody>? = null
    var mInviteFriendsListener: ReminderContract.ReferralCodeApiResult

    constructor(mFavoriteListener: ReminderContract.ReferralCodeApiResult) {
        this.mInviteFriendsListener = mFavoriteListener
    }

    fun postReminder(mActivity: Activity, proposal_id: String?, userId: String) {
        call = TetootaApplication.getHeader()
                .postreminder(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), userId, proposal_id!!)
        call!!.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>?,
                                    response: Response<ResponseBody>?) {
                var json: JSONObject = JSONObject(response?.body()?.string())
                var mDashboardSliderData = Gson().fromJson(json.getJSONObject("meta").toString(), com.tetoota.message.Meta::class.java)
//                var mDashboardSliderData: ResponseBody? = response?.body()
                if (response?.code() == 200) {
                    Log.e("response", mDashboardSliderData.toString())
                    if (mDashboardSliderData?.status!!) {
                        mInviteFriendsListener.onReminderCodeApiSuccess(mDashboardSliderData)
                    } else {
                        mInviteFriendsListener.onReminderCodeApiFailure(mDashboardSliderData.message.toString(), false)
                    }
                } else {
                    mInviteFriendsListener.onReminderCodeApiFailure(mDashboardSliderData.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                Log.e("onFailure", "onFailure")
                mInviteFriendsListener.onReminderCodeApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun cancelREquest(mActivity: Activity, mCancelRequest: String) {
//        if (mCancelRequest.equals("userService")) {
        call?.cancel()
//        } else {
//            mWishListCall?.cancel()
//        }
    }
}