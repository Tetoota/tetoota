package com.tetoota.message

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class ReminderPresenter : ReminderContract.Presenter, ReminderContract.ReferralCodeApiResult {
    var mFavoriteView: ReminderContract.View

    val mFavoriteInteractor: ReminderInteractor by lazy {
        ReminderInteractor(this)
    }

    constructor(mFavoriteView: ReminderContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun postReminder(mActivity: Activity, proposal_id: String?,  userId: String) {
        mFavoriteInteractor.postReminder(mActivity,proposal_id,userId)
    }

    override fun cancelRequest(mActivity: Activity, wishListType: String) {
        mFavoriteInteractor.cancelREquest(mActivity, wishListType)
    }


    override fun onReminderCodeApiSuccess(mReminderResponse: Meta) {
        mFavoriteView.onReminderSuccess(mReminderResponse)
    }

    override fun onReminderCodeApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onReminderFailureResponse(mString, isServerError)
    }
}