package com.tetoota.message.viewreminder

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class ViewReminderPresenter : ViewReminderContract.Presenter, ViewReminderContract.ViewReminderApiResult {
    var mFavoriteView: ViewReminderContract.View

    val mFavoriteInteractor: ViewReminderInteractor by lazy {
        ViewReminderInteractor(this)
    }

    constructor(mFavoriteView: ViewReminderContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun viewReminder(mActivity: Activity, proposal_id: String, userId: String) {
        mFavoriteInteractor.viewReminder(mActivity, proposal_id, userId)
    }

    override fun cancelRequest(mActivity: Activity, wishListType: String) {
        mFavoriteInteractor.cancelREquest(mActivity, wishListType)
    }


    override fun onViewReminderApiSuccess(meta: Meta) {
        mFavoriteView.onViewReminderSuccess(meta)
    }

    override fun onViewReminderApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onViewReminderFailureResponse(mString, isServerError)
    }
}