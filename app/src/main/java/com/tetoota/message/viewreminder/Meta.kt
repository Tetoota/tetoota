package com.tetoota.message.viewreminder

data class Meta(
        val code: Int? = null,
        val message: String? = null,
        val text: String? = null,
        val status: Boolean? = null,
        val time: Int? = null
)
