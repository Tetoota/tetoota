package com.tetoota.message.viewreminder

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.message.ReminderResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class ViewReminderInteractor {
    var call: Call<ResponseBody>? = null
    var mInviteFriendsListener: ViewReminderContract.ViewReminderApiResult

    constructor(mFavoriteListener: ViewReminderContract.ViewReminderApiResult) {
        this.mInviteFriendsListener = mFavoriteListener
    }

    fun viewReminder(mActivity: Activity, proposal_id: String, userId: String) {
        call = TetootaApplication.getHeader()
                .viewreminder(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), userId, proposal_id)
        call!!.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>?,
                                    response: Response<ResponseBody>?) {
                if (response?.code() == 200) {
                    var json: JSONObject = JSONObject(response?.body()?.string())
                    var mDashboardSliderData = Gson().fromJson(json.getJSONObject("meta").toString(), Meta::class.java)
                    Log.e("aka", "aka")
                    if (mDashboardSliderData.status!!) {
                        Log.e("1aka", "aka1")
                        mInviteFriendsListener.onViewReminderApiSuccess(mDashboardSliderData)
                    } else {
                        Log.e("aka2", "aka2")
                        mInviteFriendsListener.onViewReminderApiFailure(mDashboardSliderData.text.toString(), false)
                    }
                } else {
                    mInviteFriendsListener.onViewReminderApiFailure("Failure", true)
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                mInviteFriendsListener.onViewReminderApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun cancelREquest(mActivity: Activity, mCancelRequest: String) {
//        if (mCancelRequest.equals("userService")) {
        call?.cancel()
//        } else {
//            mWishListCall?.cancel()
//        }
    }
}