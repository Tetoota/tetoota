package com.tetoota.message.viewreminder

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class ViewReminderContract {
    interface View {
        fun onViewReminderSuccess(meta: Meta): Unit
        fun onViewReminderFailureResponse(mString: String, isServerError: Boolean): Unit
    }

    interface Presenter {
        fun viewReminder(mActivity: Activity, proposal_id: String, userId: String)
        fun cancelRequest(mActivity: Activity, wishListType: String)
    }

    interface ViewReminderApiResult {
        fun onViewReminderApiSuccess(meta: Meta): Unit
        fun onViewReminderApiFailure(mString: String, isServerError: Boolean): Unit
    }
}