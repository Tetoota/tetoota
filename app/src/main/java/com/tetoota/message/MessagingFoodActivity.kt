package com.tetoota.message

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.PopupMenu
import android.widget.Toast
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.customviews.CustomServiceAddAlert
import com.tetoota.customviews.CustomWaitingAlert
import com.tetoota.customviews.ReviewProposalDialog
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.inbox.InboxContract
import com.tetoota.fragment.inbox.InboxPresenter
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.inbox.adapter.ChatRoomAdapter
import com.tetoota.fragment.inbox.fragment.InboxFragment
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatDataResponse
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.potluck.PotluckFoodDataResponse
import com.tetoota.proposal.ProposalByIdData
import com.tetoota.proposal.ProposalsContract
import com.tetoota.proposal.ProposalsPresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.activity_messaging.*
import kotlinx.android.synthetic.main.activity_messaging.et_messagee
import kotlinx.android.synthetic.main.chat_food_layout.*
import kotlinx.android.synthetic.main.chatting_layout.*
import kotlinx.android.synthetic.main.food_accept_layout.*
import kotlinx.android.synthetic.main.food_menu_dialog.*
import kotlinx.android.synthetic.main.food_review_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import kotlinx.android.synthetic.main.view_proposals_activity_new.tv_descc
import org.jetbrains.anko.toast


class MessagingFoodActivity : BaseActivity(),
        View.OnClickListener,
        InboxContract.View, ProposalsContract.View, ProfileDetailContract.View, ReviewProposalDialog.IRatingDialogListener,
        CustomServiceAddAlert.IDialogListener, CustomWaitingAlert.IDialogListener {

    private var tetootaApplication: TetootaApplication? = null
    lateinit var mPotluckFoodDataResponse: PotluckFoodDataResponse
    private var exchangePostId: String = ""
    private var exchangePostType: String = ""
    private var mResponseTime: String = ""
    private var mQualityService: String = ""
    private var mFriendLines: String = ""
    private var mReviewMesg: String = ""
    private var acceptanceType: String = ""
    private lateinit var mProposal: ProposalByIdData
    private var mChatHistoryData: ChatHistoryDataResponse? = null
    private var mProposalMesgData: ProposalMessageData? = null
    private var chatArrayList: ArrayList<OneToOneChatDataResponse>? = null
    private lateinit var personData: LoginDataResponse
    private lateinit var mChatAdapter: ChatRoomAdapter
    private var foodStatus: String = ""
    private var proposal_status: String = ""
    private var receiver_trading_status: String = ""
    private var proposalFirstName = ""
    private var proposalLastName = ""
    private var proposalReciverName = ""
    private var proposalTitle = ""
    private var proposalExchangeTitle = ""
    private var proposalExchangeID = ""
    private var proposalFromId = ""
    private var proposalToId = ""
    private var foodStageStatus = ""
    private var proposalDesc = ""
    private var proposalImage = ""
    private var proposalSenderImage = ""
    private var proposalTime = ""
    private var proposalHoursLeft = ""
    private var proposalExchangeType = ""
    private var proposalPostId = ""
    private var proposalId = ""
    var register: MenuItem? = null
    var action_report: MenuItem? = null
    private var cardUpdateReceiver: CardUpdateReceiver? = null
    private var isPreViousScreen = false
    var mServiceData: ServicesDataResponse? = null
    private var mList: List<ServicesDataResponse>? = null
    private var list = ArrayList<String>()
    private var serviceCount: Int = 0
    private var isAbalToSendProposal: Boolean = false
    private var serviceCreatorId: String = ""
    private var serviceReciverId: String = ""
    private var postId: String = ""
    private var senderId: String = ""
    private lateinit var mRegistrationBroadcastReceiver: BroadcastReceiver
    var dialog: Dialog? = null

    override fun onSubmitPressed(param: String, message: String, mResponseTime: String, mQualityService: String, mFriendLines: String, mReviewMesg: String) {
        if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            this.mResponseTime = mResponseTime
            this.mQualityService = mQualityService
            this.mFriendLines = mFriendLines
            this.mReviewMesg = mReviewMesg


            val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            if (proposalExchangeType.isEmpty() || personData.user_id == proposalFromId.toString()) {
                mProposalPresenter.proposalCompleteAction(this@MessagingFoodActivity, proposalId.toString(), acceptanceType, "", "personData.referral_string!!", personData.user_id!!)
            } else {
                mProposalPresenter.proposalCompleteAction(this@MessagingFoodActivity, proposalId.toString(), acceptanceType, "text", "personData.referral_string!!", personData.user_id!!)
            }
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onCancelPressed() {
    }

    //    private lateinit var mProposal: ProposalByIdData
    private val mInboxPresenter: InboxPresenter by lazy {
        InboxPresenter(this@MessagingFoodActivity)
    }

    private val mProposalPresenter: ProposalsPresenter by lazy {
        ProposalsPresenter(this@MessagingFoodActivity)
    }

    override fun ongetAllProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
    }

    override fun ongetAllProposalFailure(message: String) {
    }

    override fun onChatMesgSuccess(message: String) {
        super.onChatMesgSuccess(message)
        hideProgressDialog()
        val oneToOneChat = OneToOneChatDataResponse()
        if (intent.getStringExtra("isProposalAccepted") != null) {
            oneToOneChat.msg_send_user_id = personData.user_id
            oneToOneChat.msg_recieved_user_id = mChatHistoryData?.msg_recieved_user_id.toString()
            oneToOneChat.post_id = mChatHistoryData?.post_id.toString()
            oneToOneChat.proposal_id = ""
            oneToOneChat.chatMesgDateTime = (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString())
            oneToOneChat.chatMessageId = ""
            oneToOneChat.chat_message = chatBoxText.text.toString()
            oneToOneChat.conver_id = ""

        } else {
            var reciverId = mProposalMesgData?.proposal_sender_id
            var postId = mProposalMesgData?.post_id
            oneToOneChat.msg_send_user_id = personData.user_id
            oneToOneChat.msg_recieved_user_id = reciverId
            oneToOneChat.post_id = postId
            oneToOneChat.proposal_id = ""
            oneToOneChat.chatMesgDateTime = (android.text.format.DateFormat.format("yyyy-MM-dd kk:mm:ss", java.util.Date()).toString())
            oneToOneChat.chatMessageId = ""

            if (chatBoxText.text.isNotEmpty()) {
                oneToOneChat.chat_message = chatBoxText.text.toString()
            } else {
                oneToOneChat.chat_message = "Your Request has been accepted"
            }
            oneToOneChat.conver_id = ""
        }
        hideProgressDialog()
        chatArrayList?.add(oneToOneChat)
        mChatAdapter.notifyDataSetChanged()
        if (mChatAdapter.itemCount > 1) {
            // scrolling to bottom of the recycler view
            chatList.layoutManager?.smoothScrollToPosition(chatList, null, mChatAdapter.itemCount - 1)
        }
        chatBoxText.setText("")
        fetchChat(personData.user_id.toString(), serviceCreatorId,
                personData.user_id.toString(), proposalId)
    }

    override fun onChatMesgFailure(message: String) {
        super.onChatMesgFailure(message)
        hideProgressDialog()
        showSnackBar(message)
    }

    fun validation(): Boolean {
        if (chatBoxText.text.toString().trim().isEmpty()) {
            toast(Utils.getText(this, StringConstant.chat_message_alert))
            return false
        }
        return true
    }

    override fun onClick(p0: View?) {
        when (p0) {
            send_bt -> {
                if (validation()) {
                    hideKeyboard(this)
                    if (com.tetoota.utility.Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
                        if (chatBoxText.text.toString().isNotEmpty()) {
                            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                            if (!isPreViousScreen) {
                                if (intent.getStringExtra("isProposalAccepted") != null) {
                                    if (personData.user_id.toString() == mChatHistoryData?.msg_send_user_id.toString()) {
                                        mInboxPresenter.sendMesg(this@MessagingFoodActivity,
                                                personData.user_id, mChatHistoryData?.msg_recieved_user_id.toString(),
                                                mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!, Constant.FOOD)
                                    } else if (personData.user_id.toString() == mChatHistoryData?.msg_recieved_user_id.toString()) {
                                        mInboxPresenter.sendMesg(this@MessagingFoodActivity,
                                                personData.user_id, mChatHistoryData?.msg_send_user_id.toString(),
                                                mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!, Constant.FOOD)
                                    }
                                } else {
                                    var reciverId = mServiceData?.user_id.toString()
                                    var postId = mServiceData?.id.toString()
                                    mInboxPresenter.sendMesg(this@MessagingFoodActivity,
                                            personData.user_id, reciverId!!,
                                            postId!!, chatBoxText.text.toString().trim(), proposalId.toString(), Constant.FOOD)
                                }
                            } else {
                                mInboxPresenter.sendMesg(this@MessagingFoodActivity,
                                        personData.user_id, serviceCreatorId,
                                        postId, chatBoxText.text.toString().trim(), proposalId, Constant.FOOD)
                            }
                        } else {
                            hideProgressDialog()
                        }
                    } else {
                        hideProgressDialog()
                        showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                    }
                }
            }

            btnFoodAccept -> {
                acceptanceType = "Accept"
                mProposalMesgData = ProposalMessageData()
                mProposalMesgData!!.proposal_id = proposalId
                if (foodStageStatus.equals(Constant.WAITING, true)) {

                    CustomWaitingAlert(this, 0,
                            this@MessagingFoodActivity, StringConstant.str_waiting_food).show()
                } else {
                    if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                        //proposal_id = mProposalMesgData!!.proposal_id
                        mProposalPresenter.proposalAction(this@MessagingFoodActivity, mProposalMesgData!!, Constant.ACCEPT, 0, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(), Constant.FOOD)
                    } else {
                        showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                    }
                }
                //sendFoodMessageMethod()
            }
            btnBFoodCancel -> {
                acceptanceType = "Cancel"
                mProposalMesgData = ProposalMessageData()
                mProposalMesgData!!.proposal_id = proposalId
                if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
                    showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                    //proposal_id = mProposalMesgData!!.proposal_id
                    mProposalPresenter.proposalAction(this@MessagingFoodActivity, mProposalMesgData!!, Constant.CANCEL, 0, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(), Constant.FOOD)
                } else {
                    showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                }
                //sendFoodMessageMethod()
            }
            btnFoodCancel -> {
                acceptanceType = "Decline"
                mProposalMesgData = ProposalMessageData()
                mProposalMesgData!!.proposal_id = proposalId
                if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
                    showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                    //proposal_id = mProposalMesgData!!.proposal_id
                    mProposalPresenter.proposalAction(this@MessagingFoodActivity, mProposalMesgData!!, Constant.DECLINE, 0, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(), Constant.FOOD)
                } else {
                    showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                }
                //sendFoodMessageMethod()
            }
            btnCFoodCancel -> {
                acceptanceType = "Cancel"
                mProposalMesgData = ProposalMessageData()
                mProposalMesgData!!.proposal_id = proposalId

                if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
                    if (personData.user_id == proposalFromId) {
                        mProposalPresenter.proposalCompleteAction(this@MessagingFoodActivity, proposalId, acceptanceType, "",
                                "personData.referral_string!!", Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString())
                    } else {
                        mProposalPresenter.proposalCompleteAction(this@MessagingFoodActivity, proposalId, acceptanceType, proposalToId,
                                "personData.referral_string!!", Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString())
                    }
                } else {
                    showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                }
                //sendFoodMessageMethod()
            }
            btnFoodSubmit -> {
                Log.e("personData", "$proposalExchangeType     $acceptanceType")
                if (validationn()) {
                    mResponseTime = rb_food_response_timee.rating.toString()
                    mQualityService = rb_food_quality_servicee.rating.toString()
                    mFriendLines = rb_food_friendlinese.rating.toString()
                    mReviewMesg = et_food_messagee.text.toString().trim()
                    acceptanceType = "Complete"
                    if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                        this.mResponseTime = mResponseTime
                        this.mQualityService = mQualityService
                        this.mFriendLines = mFriendLines
                        this.mReviewMesg = mReviewMesg

                        val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                        Log.e("personData", personData.user_id + "   " + proposalFromId)
                        Log.e("TAG", "hhhhhh if =  " + acceptanceType)
                        mProposalPresenter.proposalCompleteAction(this@MessagingFoodActivity, proposalId.toString(), acceptanceType, "", "personData.referral_string!!", personData.user_id!!)
                    }
                }
            }
            btnFood -> {
                showProgressDialog(Utils.getText(this, StringConstant.please_wait))
//                val proposalTimeLocal: String = tv_time_with_in_new.text.toString() + " " + proposalTime
                mProposalPresenter.getProposalsData(this@MessagingFoodActivity,
                        mPotluckFoodDataResponse?.id.toString(), mPotluckFoodDataResponse?.userId.toString(), personData.user_id!!,
                        "0", exchangePostId, Constant.FOOD, "", Constant.FOOD)
            }
            btnCompleteFood -> {
                showDialog(this@MessagingFoodActivity)
            }
        }
    }


    fun validationn(): Boolean {
        if (foodStatus.equals("food", true)) {
            if (et_food_messagee.text.toString().isNullOrEmpty()) {
                this.toast("Please enter review message")
                return false
            }
        } else {
            if (et_messagee.text.toString().isNullOrEmpty()) {
                this.toast("Please enter review message")
                return false
            }
        }

        return true
    }

    private fun sendFoodMessageMethod() {
        if (com.tetoota.utility.Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            if (acceptanceType.equals("Accept", true)) {
                if ((Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString() == mChatHistoryData?.msg_send_user_id.toString()) && (!acceptanceType.equals("Cancel", true) || !acceptanceType.equals("Decline", true))) {
                    chatBoxText.setText("Your request has been accepted")
                    mInboxPresenter.sendMesg(this@MessagingFoodActivity,
                            Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(), mChatHistoryData?.msg_recieved_user_id.toString(),
                            mChatHistoryData?.post_id!!, chatBoxText.text.toString().trim(), mChatHistoryData?.proposal_id!!, Constant.FOOD)
                } else if ((Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString() == mChatHistoryData?.msg_recieved_user_id.toString()) && (!acceptanceType.equals("Cancel", true) || !acceptanceType.equals("Decline", true))) {
                    chatBoxText.setText("Your request has been accepted")
                    mInboxPresenter.sendMesg(this@MessagingFoodActivity,
                            Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(), mChatHistoryData?.msg_send_user_id.toString(),
                            mChatHistoryData?.post_id!!, "Your Proposal has been accepted", mChatHistoryData!!.proposal_id!!, Constant.FOOD)
                }
            } else if (acceptanceType.equals("Cancel", true)) {
                chatBoxText.setText("Your request has been canceled")
                mInboxPresenter.sendMesg(this@MessagingFoodActivity,
                        Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(), serviceCreatorId,
                        postId, chatBoxText.text.toString(), proposalId!!, Constant.FOOD)
            }
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tetootaApplication = this.applicationContext as TetootaApplication
        setContentView(R.layout.activity_messaging)
        cv_main.visibility = View.GONE
        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
        send_bt.setOnClickListener(this)
        btnFoodAccept.setOnClickListener(this)
        btnFoodCancel.setOnClickListener(this)
        btnFoodSubmit.setOnClickListener(this)
        btnFood.setOnClickListener(this)
        btnCompleteFood.setOnClickListener(this)
        btnCFoodCancel.setOnClickListener(this)
        btnBFoodCancel.setOnClickListener(this)
        setMultiLanguageText()
        getDataFromPReviousScreen()
        setSpannable()
        mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, intent: Intent) {
                if (intent.getStringExtra("messageAlert") == "Message Alert") {
                    println("Message Push Received")
                    getDataFromPReviousScreen()
                }
            }
        }
    }

    private fun getDataFromPReviousScreen() {
        val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingFoodActivity)
        personData = Gson().fromJson(json, LoginDataResponse::class.java)
        chatArrayList = ArrayList<OneToOneChatDataResponse>()
        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        chatList.layoutManager = layoutManager
        chatList.setHasFixedSize(true)
        if (intent.getStringExtra("productType") == "food") {
            val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingFoodActivity)
            personData = Gson().fromJson(json, LoginDataResponse::class.java)
            chatArrayList = ArrayList<OneToOneChatDataResponse>()
            val layoutManager = LinearLayoutManager(this)
            layoutManager.stackFromEnd = true
            chatList.layoutManager = layoutManager
            chatList.setHasFixedSize(true)
            foodStatus = "food"
            isPreViousScreen = true
            mPotluckFoodDataResponse = intent.getParcelableExtra("mPotluckFoodDataResponse")
            initToolbar("${mPotluckFoodDataResponse!!.user_first_name.toString()} ${mPotluckFoodDataResponse!!.user_last_name.toString()}")
            serviceCreatorId = mPotluckFoodDataResponse!!.userId.toString()
            postId = mPotluckFoodDataResponse!!.id.toString()
            senderId = personData.user_id.toString()
            setFoodData()
        } else if (intent.getStringExtra("isProposalAccepted") != null) {
            isPreViousScreen = true
            cv_main.visibility = View.GONE
            mChatHistoryData = intent.getParcelableExtra<ChatHistoryDataResponse>("inboxPojo")
            postId = mChatHistoryData!!.post_id.toString()
            //fetchChatHistory(personData, mChatHistoryData!!)
            Log.i(javaClass.name, "=====adesad===========" + intent.getStringExtra("isProposalAccepted") + "   " + mChatHistoryData!!.proposal_to.toString() + "    " + mChatHistoryData!!.proposal_from.toString())

            if (personData.user_id == mChatHistoryData!!.msg_send_user_id.toString()) {
                serviceCreatorId = mChatHistoryData!!.msg_recieved_user_id.toString()
                senderId = mChatHistoryData!!.msg_send_user_id.toString()
                postId = mChatHistoryData!!.post_id.toString()
                initToolbar(mChatHistoryData!!.msg_reciever_name!!)
            } else {
                Log.i(javaClass.name, "=====adesad==sads=========" + postId)
                serviceCreatorId = mChatHistoryData!!.msg_send_user_id.toString()
                senderId == mChatHistoryData!!.msg_recieved_user_id.toString()
                postId = mChatHistoryData!!.post_id.toString()
                initToolbar(mChatHistoryData!!.msg_sender_name!!)
            }
        }
        fetchChat(personData.user_id!!.toString(),
                serviceCreatorId, personData.user_id.toString(), postId)
    }

    /**
     * the m Proposal PushNotificationDataResponse
     */
    private fun initToolbar(mproPosalData: String): Unit {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar_title.text = mproPosalData
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, MessagingFoodActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.mesg_screen_menu, menu)
        register = menu?.findItem(R.id.action_view_proposal)?.setIcon(R.drawable.complete_deal)
        register?.isVisible = false
        action_report = menu?.findItem(R.id.action_view_proposal)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.clear()
        menuInflater.inflate(R.menu.mesg_screen_menu, menu)
        register = menu?.findItem(R.id.action_view_proposal)?.setIcon(R.drawable.complete_deal)
        register?.isVisible = false
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_view_proposal) {

            return true
        } else if (id == android.R.id.home) {
            onBackPressed()
        } else if (id == R.id.action_report) {
            val intent = MainActivity.newMainIntent(this)
            intent!!.putExtra("KEY", "HELP")
            startActivity(intent)

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Utils.hideSoftKeyboard(this@MessagingFoodActivity)
        ActivityStack.removeActivity(this@MessagingFoodActivity)
        finish()
    }

    override fun onOneToOneSuccess(message: String, mChatList: ArrayList<OneToOneChatDataResponse>) {
        super.onOneToOneSuccess(message, mChatList)
        hideProgressDialog()
        if (mChatList.size > 0) {
            chatArrayList = mChatList
            mChatAdapter = ChatRoomAdapter(this@MessagingFoodActivity, personData, chatArrayList!!)
            chatList.adapter = mChatAdapter
        } else {
            chatArrayList = mChatList
            mChatAdapter = ChatRoomAdapter(this@MessagingFoodActivity, personData, chatArrayList!!)
            chatList.adapter = mChatAdapter
//            register?.isVisible = false
        }
        var size = mChatList!!.size
        var proposalFrom = mChatList[size - 1]!!.proposal_from.toString()
        var proposalTo = mChatList[size - 1]!!.proposal_to.toString()
        postId = mChatList[size - 1]!!.post_id.toString()
        proposalId = mChatList[size - 1]!!.proposal_id.toString()
        if (!TextUtils.isEmpty(mChatList[size - 1]!!.exchange_post_type.toString())
                && mChatList[size - 1]!!.exchange_post_type.toString().equals("Food", true))
            foodStatus = mChatList[size - 1]!!.exchange_post_type.toString()
        Log.i(javaClass.name, "=====adesad==sads=========" + mChatList[size - 1]!!.exchange_post_type + "    " + mChatList[size - 1]!!.proposal_from + "   " + mChatList[size - 1]!!.proposal_to)
        if (TextUtils.isEmpty(proposalFrom) || proposalTo.equals("null", true)) {
            callGetAllStage(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(), serviceCreatorId, postId)
        } else callGetAllStage(proposalFrom, proposalTo, postId)
    }

    override fun onOneToOneFailure(message: String) {
        super.onOneToOneFailure(message)
        //showSnackBar(message)
        callGetAllStage(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(), serviceCreatorId, postId)
    }

    //     * Fetch Chat History
    private fun fetchChat(msg_send_user_id: String, msg_recieved_user_id: String, user_id: String,
                          post_id: String) {
        if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            mInboxPresenter.getOneToOneChatData(this@MessagingFoodActivity, msg_send_user_id,
                    msg_recieved_user_id, user_id, post_id, Constant.FOOD,"")
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onResume() {
        super.onResume()

        val intentFilter = IntentFilter("updateChat")
        cardUpdateReceiver = CardUpdateReceiver()
        this@MessagingFoodActivity.registerReceiver(cardUpdateReceiver, intentFilter)

        TetootaApplication.activityResumed()
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                IntentFilter(Constant.PUSH_NOTIFICATION))
    }

    override fun onPause() {
        TetootaApplication.activityPaused()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver)
        super.onPause()
    }

    override fun onProposalsApiSuccessResult(message: String?) {
        hideProgressDialog()
        //Log.i(javaClass.name, "==============acceptanceType " + acceptanceType + "     " + proposal_status + "\n" + proposalFromId + "  " + proposalToId)
        if (acceptanceType.equals(Constant.COMPLETE, ignoreCase = true)) {
            if (proposalExchangeType != "" && Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString() == proposalToId.toString()) {
                //Log.e("ifffffffffff", "" + personData.user_id)
                mProposalPresenter.addReviewApi(this@MessagingFoodActivity, proposalToId.toString(), proposalExchangeID.toString()
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)

            } else if (Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString() == proposalFromId.toString()) {
                //Log.e(" elseeee ifffffffffff", "" + personData.user_id)
                mProposalPresenter.addReviewApi(this@MessagingFoodActivity, proposalFromId, postId
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            } else {
                //Log.e("elseeeeeeeee", "" + proposalToId)
                mProposalPresenter.addReviewApi(this@MessagingFoodActivity, proposalToId.toString(), postId
                        , mResponseTime, mQualityService, mFriendLines, mReviewMesg)
            }
        } else {
            if (message != null) {
                showSnackBar(message)
            }
        }
        fetchChat(personData.user_id!!.toString(),
                serviceCreatorId, personData.user_id.toString(), postId)

    }

    override fun onProposalsByIdSuccessResult(message: String?, proposalByIdData: ProposalByIdData?) {
        super.onProposalsByIdSuccessResult(message, proposalByIdData)
        hideProgressDialog()
    }

    override fun onProposalActionSuccessREsult(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {
        super.onProposalActionSuccessREsult(message, mProposalMesg, acceptanceType, pos)
        hideProgressDialog()
        if (acceptanceType == Constant.ACCEPT) {
            cv_main.visibility = View.GONE
            llFoodMain.visibility = View.GONE
            showSnackBar(message.toString())
            /*isMenuVisiable = true
            invalidateOptionsMenu()*/
            //sendFoodMessageMethod()
        } else {
            llFoodMain.visibility = View.GONE
            InboxFragment.isAcceptedRejected = true
            showSnackBar(message.toString())
        }
        fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity),
                serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity), postId)
    }

    override fun onProposalsFailureResult(message: String) {
        super.onProposalsFailureResult(message)
        hideProgressDialog()
        if (message != null) {
            showSnackBar(message)
        }
    }

    private fun setMultiLanguageText() {
        chatBoxText.hint = Utils.getText(this, StringConstant.str_chatBoxText)
        //For Food Request
        tvFoodFriendliness.text = Utils.getText(this, StringConstant.str_friendlines)
        tvFoodResponseTime.text = Utils.getText(this, StringConstant.str_responsetime)
        tvFoodQualityService.text = Utils.getText(this, StringConstant.str_quality)
        btnFoodAccept.text = Utils.getText(this, StringConstant.accept)
        btnFoodCancel.text = Utils.getText(this, StringConstant.cancel)
        btnFoodSubmit.text = Utils.getText(this, StringConstant.str_submit)
        btnCompleteFood.text = Utils.getText(this, StringConstant.complete_deal)
        btnCFoodCancel.text = Utils.getText(this, StringConstant.cancel)
    }

    //Intialize Brodcast Reciver
    inner class CardUpdateReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                Log.i(javaClass.name, "======================CardUpdateReceiver  " + personData.user_id.toString() + "   " + serviceCreatorId)
                if (intent.getIntExtra(Constant.PROPOSAL_FROM, 0) == serviceCreatorId.toInt())
                    fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(),
                            serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(),
                            proposalId)
            }
        }

    }

    private fun setSpannable() {
        val wordtoSpan = SpannableString("In case you do not receive points or service mentioned, you can contact us")
        wordtoSpan.setSpan(ForegroundColorSpan(Color.BLUE), 0, wordtoSpan.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        tv_descc.setText(wordtoSpan);
        tv_descc.paintFlags = tv_descc.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG
        tv_descc.setOnClickListener(this)
    }

    override fun onAddReviewApISuccessResult(message: String?) {
        super.onAddReviewApISuccessResult(message)
        cv_main.visibility = View.GONE
        if (message != null) {
            showSnackBar(message)
        }
        callUserDetailAPI()
    }

    override fun onAddReviewApiFailureResult(message: String?) {
        super.onAddReviewApiFailureResult(message)
        hideProgressDialog()
        if (message != null) {
            showSnackBar(message)
        }
    }

    private fun callUserDetailAPI() {
        if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingFoodActivity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileData(this@MessagingFoodActivity, personData)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
//            clearStack()
        }
    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        val profileData: ProfileDataResponse = message?.get(0)!!
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingFoodActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        personData.tetoota_points = profileData.tetoota_points
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this@MessagingFoodActivity)
        hideProgressDialog()
//        clearStack()
        // openSharingDialog()
    }

    override fun onProfileSendEmailApiSuccessResult(message: String) {
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
//        clearStack()
        //openSharingDialog()
    }

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@MessagingFoodActivity)
    }


    private fun callGetAllStage(proposalFrom: String, proposalTo: String, postId: String) {
        if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@MessagingFoodActivity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mInboxPresenter.getStageData(this@MessagingFoodActivity, proposalFrom!!, proposalTo, postId, "", Constant.FOOD, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString(),"")
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
//            clearStack()
            //penSharingDialog()
        }
    }

    override fun onGetStageSuccess(message: String, chatStatus: String, data: ProposalByIdData) {
        hideProgressDialog()
        if (data != null && !chatStatus.equals(Constant.START_TRADING, true)) {
            proposalId = data.proposal_id!!.toString()
            proposalFromId = data.proposal_from!!.toString()
            proposalToId = data.proposal_to!!.toString()
            proposal_status = data.proposal_status!!.toString()
            proposalExchangeType = data.exchange_post_type!!.toString()
            proposalExchangeID = data.exchange_post_id!!.toString()
            foodStageStatus = data.food_status!!.toString()
            setFoodStageData(data)
        }
        if (chatStatus.equals(Constant.START_TRADING, true)) {
            Log.i(javaClass.name, "==================" + intent.getStringExtra("productType"))
            btnFood.visibility = View.VISIBLE
            if (intent.getStringExtra("productType") == "food")
                llFood.visibility = View.VISIBLE
            else llFood.visibility = View.GONE
            llFoodMain.visibility = View.GONE
            rlFoodReview.visibility = View.GONE
            btnBFoodCancel.visibility = View.GONE
        } else if (chatStatus.equals(Constant.ACCEPTED, true)) {
            llFood.visibility = View.VISIBLE
            rlFoodReview.visibility = View.GONE
            llFoodMain.visibility = View.GONE
            proposalExchangeType = data.exchange_post_type!!.toString()
            if (data.proposal_from!!.toString().equals(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString())) {
                btnBFoodCancel.visibility = View.VISIBLE
                llFoodComplete.visibility = View.GONE
            } else {
                btnBFoodCancel.visibility = View.GONE
                llFoodComplete.visibility = View.VISIBLE
            }
            rlFoodReview.visibility = View.GONE
        } else if (chatStatus.equals(Constant.ON_GOING, true)) {
            llFood.visibility = View.GONE
            llFoodMain.visibility = View.GONE
            btnBFoodCancel.visibility = View.GONE
            if (data.proposal_from!!.toString().equals(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString())) {
                rlFoodReview.visibility = View.VISIBLE
            } else {
                rlFoodReview.visibility = View.GONE
            }
        } else if (chatStatus.equals(Constant.PROPOSAL_PENDING, true)) {
            llFoodComplete.visibility = View.GONE
            if (data.proposal_from!!.toString().equals(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString())) {
                llFoodMain.visibility = View.GONE
                btnBFoodCancel.visibility = View.VISIBLE
            } else {
                llFoodMain.visibility = View.VISIBLE
                btnBFoodCancel.visibility = View.GONE
            }
            llFood.visibility = View.VISIBLE
            btnFood.visibility = View.GONE
        } else if (chatStatus.equals(Constant.COMPLETED, true)) {
            btnBFoodCancel.visibility = View.GONE
            llFoodMain.visibility = View.GONE
        } else if (chatStatus.equals(Constant.DECLINE, true) || chatStatus.equals(Constant.CANCEL, true)) {
            btnBFoodCancel.visibility = View.GONE
            llFoodMain.visibility = View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setFoodData() {
        tvTitle.text = mPotluckFoodDataResponse!!.title
        tvUsername.text = mPotluckFoodDataResponse.user_first_name + " " + mPotluckFoodDataResponse.user_last_name
        tvLocation.text = String.format("%.2f", Utils.checkDistance(LatLng(tetootaApplication!!.myLatitude.toDouble(), tetootaApplication!!.myLongitude.toDouble()),
                LatLng(mPotluckFoodDataResponse.Lat as Double, mPotluckFoodDataResponse.Long as Double))) + "Km"
        if (mPotluckFoodDataResponse.image!!.isEmpty()) {
            ivPotLock.setImageResource(R.drawable.queuelist_place_holder)
        } else {
            Picasso.get().load(mPotluckFoodDataResponse.image).into(ivPotLock)
        }

        if (mPotluckFoodDataResponse.profileImage!!.isEmpty()) {
            ivFoodUserImage.setImageResource(R.drawable.queuelist_place_holder)
        } else {
            Picasso.get().load(mPotluckFoodDataResponse.profileImage).into(ivFoodUserImage)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setFoodStageData(data: ProposalByIdData) {
        // if (Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity).toString() == proposalFromId) {
        tvTitle.text = data!!.title
        tvUsername.text = data.receiver_name
        tvLocation.text = String.format("%.2f", Utils.checkDistance(LatLng(tetootaApplication!!.myLatitude.toDouble(), tetootaApplication!!.myLongitude.toDouble()),
                LatLng(data.latitude as Double, data.longitude as Double))) + "Km"
        if (data.receiver_profile_image!!.isEmpty()) {
            ivFoodUserImage.setImageResource(R.drawable.queuelist_place_holder)
        } else {
            Picasso.get().load(data.receiver_profile_image).into(ivFoodUserImage)
        }
//        } else {
//            tvTitle.text = data!!.title
//            tvUsername.text = data.sender_name
//            tvLocation.text = String.format("%.2f", Utils.checkDistance(LatLng(tetootaApplication!!.myLatitude.toDouble(), tetootaApplication!!.myLongitude.toDouble()),
//                    LatLng(data.latitude as Double, data.longitude as Double))) + "Km"
//        }
        if (data.post_image!!.isEmpty()) {
            ivPotLock.setImageResource(R.drawable.queuelist_place_holder)
        } else {
            Picasso.get().load(data.post_image).into(ivPotLock)
        }
    }

    override fun onGetStageFailure(message: String) {
        hideProgressDialog()
    }

    override fun onYes(param: String, message: String) {
        val intent = AddPostRequestActivity.newMainIntent(this)
        intent!!.putExtra("Tab", "serviceTab")
        ActivityStack.getInstance(this!!)
        startActivity(intent)
        finish()
    }


    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm!!.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onProposalActionCancelSuccessREsult(message: String?, propoalId: String, acceptanceType: String, pos: Int) {
        super.onProposalActionCancelSuccessREsult(message, propoalId, acceptanceType, pos)
        hideProgressDialog()
        if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity),
                    serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity), postId)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
        // sendFoodMessageMethod()
    }

    override fun onStop() {
        super.onStop()
        if (cardUpdateReceiver != null) {
            this@MessagingFoodActivity!!.unregisterReceiver(cardUpdateReceiver)
        }
    }

    override fun onFoodCompleteSuccessResult(message: String?) {
        hideProgressDialog()
        Toast.makeText(this@MessagingFoodActivity, message, Toast.LENGTH_LONG).show()
        if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity),
                    serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity), postId)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onFoodCompleteFailureResult(message: String) {
        super.onFoodCompleteFailureResult(message)
        hideProgressDialog()
        Toast.makeText(this@MessagingFoodActivity, message, Toast.LENGTH_LONG).show()
        if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            fetchChat(Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity),
                    serviceCreatorId, Utils.loadPrefrence(Constant.USER_ID, "", this@MessagingFoodActivity), postId)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    private fun showDialog(mContext: Context) {
        dialog = Dialog(this)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setCancelable(false)
        dialog!!.window!!.decorView.setBackgroundColor(resources.getColor(R.color.color_tra))
        var window = dialog!!.getWindow()
        window!!.setGravity(Gravity.BOTTOM);
        window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog!!.setContentView(R.layout.food_menu_dialog)
        dialog!!.show()
//------------------------------btn_no.setOnClickListener------------------------------------------//
        dialog!!.tvListing.setOnClickListener(View.OnClickListener {
            dialog!!.dismiss()
            if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
                showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                mProposalPresenter.foodComplete(this@MessagingFoodActivity,
                        proposalId, Constant.COMPLETE)
            } else showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        })
        dialog!!.tvFoodListCancel.setOnClickListener(View.OnClickListener {
            dialog!!.dismiss()
        })
        dialog!!.tvUnList.setOnClickListener(View.OnClickListener {
            dialog!!.dismiss()
            if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
                showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                mProposalPresenter.foodListUnList(this@MessagingFoodActivity, postId)
            } else {
                showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
            }
        })
    }

    override fun onFoodListUnListSuccessResult(message: String?) {
        hideProgressDialog()
//                val proposalTimeLocal: String = tv_time_with_in_new.text.toString() + " " + proposalTime
        if (Utils.haveNetworkConnection(this@MessagingFoodActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mProposalPresenter.foodComplete(this@MessagingFoodActivity,
                    proposalId, Constant.COMPLETE)
        } else showSnackBar(Utils.getText(this, StringConstant.str_check_internet))


    }

    override fun onFoodListUnListFailureResult(message: String) {
        super.onFoodListUnListFailureResult(message)
        hideProgressDialog()
        Toast.makeText(this@MessagingFoodActivity, message, Toast.LENGTH_LONG).show()
    }

    override fun dialogAlert(dialogID: Int, message: String) {

    }

}
