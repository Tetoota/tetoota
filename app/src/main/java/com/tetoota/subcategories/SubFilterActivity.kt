package com.tetoota.subcategories

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.android.bizdak.categoery.adapter.FilterAdapter
import com.android.bizdak.categoery.adapter.SubFilterAdapter
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.categories.CategoriesDataResponse
import com.tetoota.filter.FilterActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_sub_filter.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class SubFilterActivity : BaseActivity(), SubCategoriesContract.View, View.OnClickListener {
    var register: MenuItem? = null
    var mSelectedcategorylist: ArrayList<CategoriesDataResponse> = ArrayList()
    var subCategoriesArray: ArrayList<Int> = ArrayList()
    override fun onSubCategoriesSuccessResult(mCategoriesList: List<DataItem>, message: String) {
        progress.visibility = View.GONE
        this.filterList = mCategoriesList as ArrayList<DataItem>

        if (filterList.size > 0) {
            subCategoriesAdapter = SubFilterAdapter(this, filterList)
            val mLayoutManager = LinearLayoutManager(this)
            recycler_view.layoutManager = mLayoutManager
            recycler_view.itemAnimator = DefaultItemAnimator()
            recycler_view.adapter = subCategoriesAdapter
        }
    }

    override fun onSubCatergoriesFailureResult(message: String) {
        progress.visibility = View.GONE
        val simpleAlert = AlertDialog.Builder(this@SubFilterActivity).create()
        simpleAlert.setTitle("Alert")
        simpleAlert.setMessage("No Data Found")

        simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
            dialogInterface, i ->
//            Toast.makeText(applicationContext, "You clicked on OK", Toast.LENGTH_SHORT).show()
        })

        simpleAlert.show()
    }

    //    var filterList: MutableList<SubCategoryModel> = mutableListOf()
    var filterList: MutableList<DataItem> = mutableListOf()
    lateinit var listener: FilterAdapter.FilterListener
    lateinit var subCategoriesAdapter: SubFilterAdapter

    lateinit var categoryObject: CategoriesDataResponse

    private val mSubCategoriesPresenter: SubCategoriesPresenter by lazy {
        SubCategoriesPresenter(this@SubFilterActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_filter)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        progress.visibility = View.VISIBLE
        var bundal: Bundle = Bundle()
        bundal = intent.extras!!
        categoryObject = bundal.getParcelable("categoryObject")!!
        toolbar_title.text = categoryObject.category_name

        /*************kamal***************/
        toolbar_save.visibility = View.VISIBLE
        toolbar_save.text = "Reset"

        btn_apply.setOnClickListener(this@SubFilterActivity)
        toolbar_save.setOnClickListener(this@SubFilterActivity)

        mSubCategoriesPresenter.getSubCategoriesData(categoryObject.category_id, this@SubFilterActivity)
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, SubFilterActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

//    override fun onBackPressed() {
//        super.onBackPressed()
////        val intent = Intent()
////        intent.putParcelableArrayListExtra("FILTER", filterList as ArrayList<DataItem>)
////        setResult(Activity.RESULT_OK, intent)
///*        ActivityStack.removeActivity(this@SubFilterActivity)
//        var i = 0
//        var numIterations = filterList.size
//        var selectedCount: Int = 0
//        while (i < numIterations) {
//            if (filterList.get(i).isSelected) {
//                print("TRUE")
//                selectedCount += 1
//                subCategoriesArray.add(i)
//            } else {
//                print("FALSE")
//            }
//            i++
//        }
//        /////////////////////////
//// Set Category id
//        if (selectedCount > 0) {
//            val result = StringBuilder()
//            var string: String = Utils.loadPrefrence(Constant.CATEGORY_ID, "", this@SubFilterActivity)
//            result.append(string)
//            if (result.toString().equals("")) {
//                result.append("" + categoryObject.category_id)
//            } else {
//                result.append("," + categoryObject.category_id)
//            }
//            Utils.savePreferences(Constant.CATEGORY_ID, result.toString().trim(), this@SubFilterActivity)
//        }
//        ////////////////////
//        // Set SubCategories id
//        i = 0
//        numIterations = subCategoriesArray.size
//        val result = StringBuilder()
//        var string: String = Utils.loadPrefrence(Constant.SUBCATEGORIES_ID, "", this@SubFilterActivity)
//        result.append(string)
//        while (i < numIterations) {
//            if (i == 0 && result.toString().equals("")) {
//                result.append(filterList.get(subCategoriesArray.get(i)).sub_category_id)
//            } else {
//                result.append("," + filterList.get(subCategoriesArray.get(i)).sub_category_id)
//            }
//            i++
//        }
//        Utils.savePreferences(Constant.SUBCATEGORIES_ID, result.toString().trim(), this@SubFilterActivity)
//        finish()*/
//    }

    override fun onClick(p0: View?) {
        when (p0) {
            btn_apply -> {
//                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
//                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//                setResult(Activity.RESULT_OK, null)
//                finish()

                ActivityStack.removeActivity(this@SubFilterActivity)
                var i = 0
                var numIterations = filterList.size
                var selectedCount: Int = 0
                while (i < numIterations) {
                    if (filterList.get(i).isSelected) {
                        print("TRUE")
                        selectedCount += 1
                        subCategoriesArray.add(i)
                    } else {
                        print("FALSE")
                    }
                    i++
                }
                /////////////////////////
// Set Category id
                if (selectedCount > 0) {
                    val result = StringBuilder()
                    var string: String = Utils.loadPrefrence(Constant.CATEGORY_ID, "", this@SubFilterActivity)
                    result.append(string)
                    if (result.toString().equals("")) {
                        result.append("" + categoryObject.category_id)
                    } else {
                        result.append("," + categoryObject.category_id)
                    }
                    Utils.savePreferences(Constant.CATEGORY_ID, result.toString().trim(), this@SubFilterActivity)
                }
                ////////////////////
                // Set SubCategories id
                i = 0
                numIterations = subCategoriesArray.size
                val result = StringBuilder()
                var string: String = Utils.loadPrefrence(Constant.SUBCATEGORIES_ID, "", this@SubFilterActivity)
                result.append(string)
                while (i < numIterations) {
                    if (i == 0 && result.toString().equals("")) {
                        result.append(filterList.get(subCategoriesArray.get(i)).sub_category_id)
                    } else {
                        result.append("," + filterList.get(subCategoriesArray.get(i)).sub_category_id)
                    }
                    i++
                }
                Utils.savePreferences(Constant.SUBCATEGORIES_ID, result.toString().trim(), this@SubFilterActivity)
                setResult(Activity.RESULT_OK, null)
                finish()
            }
            toolbar_save -> {
                for (item in filterList.indices) {
                    filterList[item].isSelected=false
                }
                subCategoriesAdapter.notifyDataSetChanged()
                Log.e("dddddddddd","vvvvvvvvvvvv")
            }
        }
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.isVisible = false
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.clear()
        menuInflater.inflate(R.menu.mesg_screen_menu, menu)
        register = menu?.findItem(R.id.action_view_proposal)?.setIcon(R.drawable.nav_arrow_white)
        register?.isVisible = false
        return super.onPrepareOptionsMenu(menu)
    }
}
