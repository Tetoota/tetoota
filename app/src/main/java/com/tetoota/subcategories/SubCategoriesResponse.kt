package com.tetoota.subcategories

data class SubCategoriesResponse(
	val data: List<DataItem?>? = null,
	val meta: Meta? = null
)
