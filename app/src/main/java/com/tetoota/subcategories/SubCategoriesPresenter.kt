package com.tetoota.subcategories

import android.app.Activity

/**
 * Created by jitendra.nandiya on 13-11-2017.
 */
class SubCategoriesPresenter : SubCategoriesContract.Presenter,  SubCategoriesContract.CategoriesApiListener{
    override fun onCategoriesApiSuccess(mDataList: List<DataItem?>?, message: String) {
        mCategoriesView.onSubCategoriesSuccessResult(mDataList as List<DataItem>,message)
    }

    var mCategoriesView : SubCategoriesContract.View
    private val mCategoriesInteractor : SubCategoriesInteractor by lazy {
        SubCategoriesInteractor(this)
    }

    constructor(mCategoriesView: SubCategoriesContract.View){
        this.mCategoriesView = mCategoriesView
    }

    override fun getSubCategoriesData(mCategoryId : Int, mActivity : Activity) {
        mCategoriesInteractor.getAllCategoriesData(mCategoryId, mActivity)
    }

    override fun onCategoriesApiFailure(message: String) {
        mCategoriesView.onSubCatergoriesFailureResult(message)
    }
}