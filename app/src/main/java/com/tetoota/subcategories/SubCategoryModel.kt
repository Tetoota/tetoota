package com.android.bizdak.categoery.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by vaibhav.malviya on 05-07-2017.
 */
class SubCategoryModel(var masterSubCategoryID: String,
                       var subCategoryName: String,
                       var subCategoryName_fr: String,
                       var subCategoryIcon: String,
                       var subCategoryIconThumb: String,
                       var lastUpdatedAt: String,
                       var isMasterSubCategorySelected: Boolean) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<SubCategoryModel> = object : Parcelable.Creator<SubCategoryModel> {
            override fun createFromParcel(source: Parcel): SubCategoryModel = SubCategoryModel(source)
            override fun newArray(size: Int): Array<SubCategoryModel?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
            source.readString().toString(),
            source.readString().toString(),
            source.readString().toString(),
            source.readString().toString(),
            source.readString().toString(),
            source.readString().toString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(masterSubCategoryID)
        dest.writeString(subCategoryName)
        dest.writeString(subCategoryName_fr)
        dest.writeString(subCategoryIcon)
        dest.writeString(subCategoryIconThumb)
        dest.writeString(lastUpdatedAt)
        dest.writeInt((if (isMasterSubCategorySelected) 1 else 0))
    }
}