package com.tetoota.fragment.favorites

import android.app.Activity
import com.tetoota.fragment.dashboard.ServicesDataResponse

/**
 * Created by charchit.kasliwal on 12-06-2017.
 */
class FavoritePresenter : FavoriteContract.Presenter,FavoriteContract.FavoriteApiResult{
    var mFavoriteView : FavoriteContract.View

    val mFavoriteInteractor : FavoriteInteractor by lazy {
        FavoriteInteractor(this)
    }

    constructor(mFavoriteView: FavoriteContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun getUserFavoriteList(mActivity : Activity,pagination : Int?) {
        mFavoriteInteractor.getFavoriteList(mActivity, pagination)
    }

    override fun onFavoriteApiSuccess(mString: String, mFavoriteList : ArrayList<ServicesDataResponse>) {
        mFavoriteView.onFavoriteListSuccess(mString, mFavoriteList)
    }

    override fun onFavoriteApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onFavoriteFailureResponse(mString,isServerError)
    }
}