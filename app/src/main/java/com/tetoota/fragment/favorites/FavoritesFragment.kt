package com.tetoota.fragment.favorites

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.Toolbar
import android.view.*
import com.tetoota.R
import com.tetoota.customviews.PaginationScrollListener
import com.tetoota.details.ProductDetailActivity
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.listener.IFragmentOpenCloseListener
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServiceFragment
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.fragment_favorites.*
import kotlinx.android.synthetic.main.home_toolbar_layout.*
import kotlinx.android.synthetic.main.home_toolbar_layout.view.*
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FavoritesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FavoritesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FavoritesFragment : BaseFragment(), FavoriteContract.View,
        FavoriteAdapter.INotifyActivityListener, ServiceContract.View {
    // TODO: Rename and change types of parameters
    private var isLoad = false
    private var isListLastPage = false
    private var mParam1: String? = null
    private var mParam2: String? = null
    internal var getScreenHeight: Int = 0
    internal var getScreenWidth: Int = 0
    private lateinit var mFavoriteAdapter: FavoriteAdapter
    private var TOTAL_PAGES = 0
    private val RECORD_PER_PAGE = 10
    private var currentPage = 1
    var gridLayoutManager: GridLayoutManager? = null

    val mFavPresenter: FavoritePresenter by lazy {
        FavoritePresenter(this@FavoritesFragment)
    }

    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@FavoritesFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }
    /**
     *
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    private fun setActionBarToolbar() {
        if (this@FavoritesFragment.arguments!!.getString(ARG_PARAM2).equals("profileScreen")) {
            iFragmentOpenCloseListener!!.onFragmentOpenClose("profile", "favorites", "")
        }

        val toolbar = activity!!.findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.toolbar_title.text = Utils.getText(activity, StringConstant.favorite)
      // toolbar.setTitle(Utils.getText(activity, StringConstant.favorite))
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    /**
     * the bundle saved instance state
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gridLayoutManager = GridLayoutManager(activity, 2)
        rv_favoritelist.layoutManager = this.gridLayoutManager
        rv_favoritelist.hasFixedSize()
        setActionBarToolbar()
        rv_favoritelist.addOnScrollListener(object : PaginationScrollListener(gridLayoutManager) {
            override fun isLastPage(): Boolean {
                return isListLastPage
            }

            override fun loadMoreItems() {
                isLoad = true
                currentPage += 1
                loadItemsLayout_recyclerView.visibility = View.VISIBLE
                getFavoriteApiCall(currentPage, true)
            }

            override fun getTotalPageCount(): Int {
                return TOTAL_PAGES
            }

            override fun isLoading(): Boolean {
                return isLoad
            }
        })
        getFavoriteApiCall(currentPage, false)
        mFavoriteAdapter = FavoriteAdapter(this.activity!!, iAdapterClickListener = this,
                screenHeight = heightCalculation(.44f), screenWidth = widthCalculation(0.51f))
        rv_favoritelist.adapter = mFavoriteAdapter
        error_btn_retry.setOnClickListener(View.OnClickListener {

        })
    }

    fun getFavoriteApiCall(pagination: Int, isLoadMore: Boolean) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoadMore) {
                progress_view.visibility = View.VISIBLE
            }
            mFavPresenter.getUserFavoriteList(this.activity!!, pagination)
        } else {
            activity!!.toast(Utils.getText(context,StringConstant.str_check_internet))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onDetach() {
        super.onDetach()
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        //        iFragmentOpenCloseListener
//        var iFragmentOpenCloseListener = null
        var iFragmentOpenCloseListener: IFragmentOpenCloseListener? = null

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment FavoritesFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String, iFragmentOpenCloseListenerLocal: IFragmentOpenCloseListener): FavoritesFragment {
            iFragmentOpenCloseListener = iFragmentOpenCloseListenerLocal
            val fragment = FavoritesFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }

        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FavoritesFragment {
            val fragment = FavoritesFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    //--------------------------------
    override fun onFavoriteListSuccess(mString: String, mFavoriteList: ArrayList<ServicesDataResponse>) {
        if(view != null){
            TOTAL_PAGES += 1
            progress_view.visibility = View.GONE
            loadItemsLayout_recyclerView.visibility = View.GONE
            hideErrorView()
            if (null == mFavoriteAdapter) {
                mFavoriteAdapter = FavoriteAdapter(this.activity!!, mFavoriteList, this,
                        screenHeight = heightCalculation(.44f), screenWidth = widthCalculation(0.51f))
                rv_favoritelist.adapter = mFavoriteAdapter
            } else {
                mFavoriteAdapter.addAll(mFavoriteList as List<ServicesDataResponse?>)
                mFavoriteAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onFavoriteFailureResponse(mString: String, isServerError: Boolean) {
        if(view != null){
            progress_view.visibility = View.GONE
            loadItemsLayout_recyclerView.visibility = View.GONE
            isLoad = false
            if (activity != null && ServiceFragment() != null) {
                if (!isServerError) {
                    isListLastPage = true
                }
                if (TOTAL_PAGES == 0) {
//                activity.toast(mString)
                    showErrorView(mString)
                }
            }
        }
    }

    override fun notifyActivity(pos: Int?, mServiceDataResp: ServicesDataResponse,
                                mFav: Int, btnClick: String, mView: View) {
        if (btnClick == "btnFav") {
            mServicePresenter.markFavorite(this.activity!!, mServiceDataResp, mFav.toString(), pos!!)
        } else if (btnClick == "cellRowClick") {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = Intent(context, ProductDetailActivity::class.java)
                intent.putExtra("mProductData", mServiceDataResp)
                intent.putExtra("productType", "services")
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    val transitionActivityOptions: ActivityOptions =
                            ActivityOptions.makeSceneTransitionAnimation(activity,
                                    mView, "iv_image")
//                    startActivity(intent, transitionActivityOptions.toBundle())
                    startActivityForResult(intent,11, transitionActivityOptions.toBundle())
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                } else {
//                    startActivity(intent)
                    startActivityForResult(intent,11)
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                }
            } else {
                activity!!.toast(Utils.getText(context,StringConstant.str_check_internet))
            }
        }
    }

    override fun notifyForListItemCount(count: Int?) {
        if (count == 0) {
            showErrorView(this@FavoritesFragment.resources.getString(R.string.error_msg))
        }
    }

    override fun onWishListApiSuccessResult(mCategoriesList: List<Any?>?, message: String) {
        progress_view.visibility = View.GONE
        hideErrorView()
        loadItemsLayout_recyclerView.visibility = View.GONE
        print("onWishListApiSuccessResult")
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        progress_view.visibility = View.GONE
        loadItemsLayout_recyclerView.visibility = View.GONE
        isLoad = false
        if (activity != null && ServiceFragment() != null) {
            if (!isServerError) {
                isListLastPage = true
            }
            if (TOTAL_PAGES != 1) {
                showErrorView(message)
            }
        }
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        activity!!.toast(getString(R.string.str_item_removed))
        mFavoriteAdapter.setItem(p1)
    }

    /**
     * Method for height calculation
     */
    internal fun heightCalculation(value: Float): Int {
        getScreenHeight = Utils.getScreenHeight(this.activity!!)
        val getTopMarginH = (getScreenHeight * value).toInt()
        return getTopMarginH
    }

    /**
     * Method for height calculation
     */
    internal fun widthCalculation(value: Float): Int {
        getScreenWidth = Utils.getScreenWidth(this.activity!!)
        val getTopMarginH = (getScreenWidth * value).toInt()
        return getTopMarginH
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            rv_favoritelist.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            rv_favoritelist.visibility = View.GONE
            err_title.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        super.onPrepareOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==11){
            clearList()
            isLoad = false
            currentPage = 1
            getFavoriteApiCall(currentPage, false)
        }
    }

    fun clearList() {
        mFavoriteAdapter.removeAll()
        mFavoriteAdapter.notifyDataSetChanged()
    }
}// Required empty public constructor
