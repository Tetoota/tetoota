package com.tetoota.fragment.favorites

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.squareup.picasso.Picasso
import com.tetoota.R
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.favorite_list_row.view.*


/**
 * Created by charchit.kasliwal on 12-06-2017.
 */
class FavoriteAdapter(var mContext: Context,
                      private var mFavoriteList: ArrayList<ServicesDataResponse> = ArrayList<ServicesDataResponse>(),
                      val iAdapterClickListener: INotifyActivityListener, var screenHeight: Int,
                      var screenWidth: Int) : RecyclerView.Adapter<FavoriteAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.favorite_list_row, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        viewHolder.bindFavoriteListData(mContext, viewHolder, p1,
                mFavoriteList[p1], iAdapterClickListener, screenHeight, screenWidth)
    }

    fun getLastPosition() = if (mFavoriteList.lastIndex == -1) 0 else mFavoriteList.lastIndex

    fun setItem(pos: Int?) {
        if (pos != null) {
            mFavoriteList.removeAt(pos)
            notifyItemRemoved(pos)
            notifyItemRangeRemoved(pos, itemCount)
            iAdapterClickListener.notifyForListItemCount(mFavoriteList.size)
        }
    }

    fun addAll(moveResults: List<ServicesDataResponse?>) {
        for (result in moveResults) {
            add(result)
        }
    }

    private fun add(result: Any?) {
        mFavoriteList.add(result as ServicesDataResponse)
        notifyItemInserted(mFavoriteList.size - 1)
    }

    fun removeAll() {
        val size = mFavoriteList.size
        if (size > 0) {
            this.mFavoriteList.clear()
            notifyItemRangeRemoved(0, size)
        }
    }

    override fun getItemCount(): Int {
        return mFavoriteList.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mProductName = view.tv_productname!!
        val mUserProfilePic = view.iv_userprofilepic!!
        val mIvProduct = view.iv_product!!
        val mIvUsername = view.tv_username!!
        val iv_mark_fav = view.iv_mark_fav!!
        val tv_tetoota_point = view.tv_tetoota_point!!
        val cv_favorite = view.cv_favorite!!
        val rl_card_view_height = view.rl_card_view_height!!
        val iv_tetootaicon = view.iv_tetootaicon!!

        fun bindFavoriteListData(mContext: Context, viewHolder: ViewHolder?, p1: Int, s: ServicesDataResponse,
                                 iAdapterClickListener: INotifyActivityListener, screenHeight: Int,
                                 screenWidth: Int) {
            cv_favorite.layoutParams = LinearLayout.LayoutParams(screenWidth, screenHeight)
            viewHolder?.mProductName?.text = s.title
            viewHolder?.tv_tetoota_point?.text = "${s.tetoota_points.toString()} " + Utils.getText(mContext, StringConstant.str_tatoota_points)
            viewHolder?.iv_tetootaicon?.setColorFilter(ContextCompat.getColor(itemView.context, R.color.color_white), android.graphics.PorterDuff.Mode.MULTIPLY);
            viewHolder?.mIvUsername?.text = s.user_first_name + " " + s.user_last_name
            if (s.is_favourite == 1) {
                iv_mark_fav.setImageResource(R.drawable.ic_favorite)
            } else {
                iv_mark_fav.setImageResource(R.drawable.iv_unlike)
            }
            if (s.profile_image!!.isEmpty()) {
                // mUserProfilePic.loadUrl(s.profile_image)
                mUserProfilePic.setImageResource(R.drawable.user_placeholder);
            } else {
                Picasso.get().load(s.profile_image).into(mUserProfilePic)
            }

            iv_mark_fav.setOnClickListener(View.OnClickListener {
                iAdapterClickListener.notifyActivity(p1, s, 0, "btnFav", iv_mark_fav)
            })

            rl_card_view_height.setOnClickListener(View.OnClickListener {
                iAdapterClickListener.notifyActivity(p1, s, 0, "cellRowClick", rl_card_view_height)
            })
            //  mIvProduct.loadProductUrl(s.image, s.image_thumb)

            if (s.image!!.isEmpty()) {
                mIvProduct.setImageResource(R.drawable.fav_placeholder);
            } else {

                Picasso.get().load(s.image).into(mIvProduct)
            }


        }

/*
        fun ImageView.loadUrl(url: String) {
            if (url != "" && url != null) {
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, url))
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.user_placeholder)
                        .error(R.drawable.user_placeholder)
                        .into(mUserProfilePic)
            } else {
                Glide.with(itemView.context)
                        .load("")
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.user_placeholder)
                        .error(R.drawable.user_placeholder)
                        .into(mUserProfilePic)
            }
        }
*/

/*
        private fun ImageView.loadProductUrl(url: String?, minSizeImage: String?) {
            var newTest = minSizeImage
            if (newTest == null) {
                newTest = "100"
            }
            if (url != "" && url != null) {
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, url, newTest, false))
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.fav_placeholder)
                        .error(R.drawable.fav_placeholder)
                        .into(mIvProduct)
            } else {
                Glide.with(itemView.context)
                        .load("")
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.fav_placeholder)
                        .error(R.drawable.fav_placeholder)
                        .into(mIvProduct)
            }
        }
*/
    }

    interface INotifyActivityListener {
        fun notifyActivity(pos: Int?, mServiceDataResp: ServicesDataResponse, mFav: Int, btnClick: String, mView: View): Unit
        fun notifyForListItemCount(count: Int?)
    }
}


