package com.tetoota.fragment.help.data

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("page_id")
	val pageId: Int? = null,

	@field:SerializedName("page_title")
	val pageTitle: String? = null,

	@field:SerializedName("page_content")
	val pageContent: String? = null,

	@field:SerializedName("page_status")
	val pageStatus: String? = null
)