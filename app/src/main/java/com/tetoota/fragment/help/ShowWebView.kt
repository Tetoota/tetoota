package com.tetoota.fragment.help

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.fragment.help.data.DataItem
import com.tetoota.fragment.help.di.ShowWebViewContractor
import com.tetoota.fragment.help.di.ShowWebViewPresenter
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_show_web_view.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick


/**
 * Created by jitendra.nandiya on 27-11-2017.
 */
class ShowWebViewActivity : BaseActivity(), ShowWebViewContractor.View {

    val mShowWebViewPresenter: ShowWebViewPresenter by lazy {
        ShowWebViewPresenter(this@ShowWebViewActivity)
    }

    //    internal var progressDialog: ProgressDialog? = null
    private var webUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_web_view)
        initViews()


    }

    private fun initViews(): Unit {
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        //Get webview
//        webView = (WebView) findViewById(R.id.webView1);
        val bundle = intent.extras
        if (null != bundle && bundle.containsKey("pageHeading")) {
            //webUrl = bundle!!.getString("webUrl")
            val pageTitle = bundle.getString("pageHeading")


            if (pageTitle.equals("About Us", ignoreCase = true))
                toolbar_title.text = Utils.getText(this@ShowWebViewActivity, StringConstant.help_about_us)

             if (pageTitle.equals("Privacy Policy", ignoreCase = true))
                toolbar_title.text = Utils.getText(this@ShowWebViewActivity, StringConstant.help_privacy_policy)

             if (pageTitle.equals("Terms and Conditions", ignoreCase = true))
                toolbar_title.text = Utils.getText(this@ShowWebViewActivity, StringConstant.help_term_condition)

             if (pageTitle.equals("isFromHelp", ignoreCase = true))
                toolbar_title.text = Utils.getText(this@ShowWebViewActivity, StringConstant.help_privacy_policy)

             if (pageTitle.equals("How It Works", ignoreCase = true))
                toolbar_title.text = Utils.getText(this@ShowWebViewActivity, StringConstant.help_how_it_woks)

             if (pageTitle.equals("FAQs", ignoreCase = true))
                toolbar_title.text = Utils.getText(this@ShowWebViewActivity, StringConstant.help_faq)



//            else // default
//                toolbar_title.text = pageTitle


//            if (pageTitle.equals("About Us", ignoreCase = true))
//                toolbar_title.text = Utils.getText(this@ShowWebViewActivity, StringConstant.help_about_us)
//            else
//                toolbar_title.text = pageTitle


            mShowWebViewPresenter.PageDetailsData(this@ShowWebViewActivity, pageTitle!!)
        }
        //startWebView(webUrl);
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@ShowWebViewActivity)
        finish()
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ShowWebViewActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    private fun startWebView(url: String) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        webView?.setWebViewClient(object : WebViewClient() {
//            internal var progressDialog: ProgressDialog? = null

            //If you will not use this method url links are opeen in new brower not in webview
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            //Show loader on url load
            override fun onLoadResource(view: WebView, url: String) {
//                if (progressDialog == null) {
//                    // in standard case YourActivity.this
//                    progressDialog = ProgressDialog(this@ShowWebViewActivity)
//                    progressDialog!!.setMessage("Loading...")
//                    progressDialog!!.show()
//                    Log.i("progressDialog","progressDialog show")
//                }
            }

            override fun onPageFinished(view: WebView, url: String) {
//                try {
//                    if (progressDialog!!.isShowing()) {
//                        progressDialog!!.dismiss()
//                        Log.i("progressDialog","progressDialog dismiss")
//                        progressDialog = null
//                    }
//                } catch (exception: Exception) {
//                    exception.printStackTrace()
//                }
            }
        })

        // Javascript inabled on webview
        webView?.getSettings()?.javaScriptEnabled = true

        // Other webview options
        /*
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setBuiltInZoomControls(true);
        */

        /*
         String summary = "<html><body>You scored <b>192</b> points.</body></html>";
         webview.loadData(summary, "text/html", null);
         */

        //Load url in webview
        webView?.loadUrl(url)
    }

    private inner class MyWebChromeClient(internal var context: Context) : WebChromeClient()

    override fun onPageDetailsAPiSuccess(mString: String, pageDetailResponse: ArrayList<DataItem>) {

        try {
            startWebViewText(pageDetailResponse[0].pageContent!!)
        } catch (e: Exception) {
        }

    }

    private fun startWebViewText(url: String) {
        try {
            webView.loadData(url, "text/html; charset=utf-8", "utf-8");
        } catch (e: Exception) {
        }
    }

    override fun onPageDetailsAPiFailure(mString: String, isServerError: Boolean) {

    }
}
