package com.tetoota.fragment.help.di

import android.app.Activity
import com.tetoota.fragment.help.data.DataItem
import com.tetoota.fragment.help.data.ShowWebViewInteractor

/**
 * Created by abhinav.maurya on 01-02-2018.
 */
class ShowWebViewPresenter : ShowWebViewContractor.PageDetailsResult, ShowWebViewContractor.Presenter{

    var mShopWebViewView : ShowWebViewContractor.View

    val mPointsSpendInteract : ShowWebViewInteractor by lazy {
        ShowWebViewInteractor(this)
    }

    constructor(mPointSummaryView: ShowWebViewContractor.View) {
        this.mShopWebViewView = mPointSummaryView
    }

    override fun PageDetailsData(mActivity: Activity, page_title: String) {
        mPointsSpendInteract.getUserHoldPoints(mActivity, page_title)
    }

    override fun onPageDetailsAPiSuccess(mString: String, pageDetailResponse: ArrayList<DataItem>) {
        mShopWebViewView.onPageDetailsAPiSuccess(mString, pageDetailResponse)
    }

    override fun onPageDetailsAPiFailure(mString: String, isServerError: Boolean) {
        mShopWebViewView.onPageDetailsAPiFailure(mString, isServerError)
    }
}