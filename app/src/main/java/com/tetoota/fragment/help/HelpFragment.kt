package com.tetoota.fragment.help

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.appintro.WalkThroughActivity
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.fragment_help.*
import android.content.ActivityNotFoundException
import android.util.Log




/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HelpFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [HelpFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HelpFragment : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v) {
            /*ll_contact -> {
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return
                }
                val intent = Intent(ACTION_CALL)
                intent.setData(Uri.parse("tel:" + "9174740155"))
                context.startActivity(intent)
            }*/
            ll_email -> {
//                val i = Intent(Intent.ACTION_SEND)
//                i.putExtra(android.content.Intent.EXTRA_EMAIL, arrayOf<String>("jitendra.nandiya@hiteshi.com"))
//                i.putExtra(android.content.Intent.EXTRA_SUBJECT, "Feedback")
//                i.putExtra(android.content.Intent.EXTRA_TEXT, "Good Service")
//                startActivity(Intent.createChooser(i, "Send email"))
                openEmail()
            }
            /*  ll_chat_with_us -> {
                  Intercom.client().handlePushMessage()
                  Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE)
              }*/
            ll_aboutUs -> {
                openWebpage("About Us")
            }
            ll_privacyPolicy -> {
                openWebpage("Privacy Policy")
            }
            ll_TermsAgreement -> {
                openWebpage("Terms and Conditions")
            }
            ll_walkThrough -> {
//                context.startActivity<WalkThroughActivity>()
                val intent = Intent(context, WalkThroughActivity::class.java)
                intent.putExtra("isFromHelp", true)
                startActivity(intent)
            }
            ll_how_it_works -> {
                openWebpage("How It Works")
            }
            ll_faq -> {
                openWebpage("FAQs")
            }
            ll_video -> {
                watchYoutubeVideo(Utils.getText(context, StringConstant.video_link))
            }
            ll_reach_us -> {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:"+Utils.getText(context, StringConstant.toll_free_number))
                startActivity(intent)
            }
        }
    }

    fun watchYoutubeVideo(id: String) {
        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
        val webIntent = Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=$id"))
        try {
            startActivity(appIntent)
        } catch (ex: ActivityNotFoundException) {
            startActivity(webIntent)
        }

    }

    override fun onResume() {
        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE)
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
    }

    private fun openWebpage(pageHeading: String) {
//        val url = webPageUrl
//        val i = Intent(Intent.ACTION_VIEW)
//        i.data = Uri.parse(url)
//        startActivity(i)

        val intent = ShowWebViewActivity.newMainIntent(this.activity!!)
        ActivityStack.getInstance(this.activity!!)
        //intent?.putExtra("webUrl", webPageUrl)
        intent?.putExtra("pageHeading", pageHeading)
        startActivity(intent)
    }

    private fun openEmail() {
//        val emailIntent = Intent(Intent.ACTION_SEND)
//        emailIntent.type = "text/plain"
//        startActivity(emailIntent)

        val emailSelectorIntent = Intent(Intent.ACTION_SENDTO)
        emailSelectorIntent.data = Uri.parse("mailto:")

        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("tetoota.india@gmail.com"))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tetoota App")
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        emailIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        emailIntent.selector = emailSelectorIntent
        startActivity(emailIntent)
    }

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    private fun setMultiLanguageText() {
        tv_email.text = Utils.getText(context, StringConstant.email_us)
        tv_aboutUs.text = Utils.getText(context, StringConstant.help_about_us)
        tv_privacyPolicy.text = Utils.getText(context, StringConstant.help_privacy_policy)
        tv_TermsAgreement.text = Utils.getText(context, StringConstant.help_term_condition)
        tv_walkThrough.text = Utils.getText(context, StringConstant.help_walthrough)
        // tv_chat_with_us.text = Utils.getText(context,StringConstant.chat_with_us)
        tv_how_it_works.text = Utils.getText(context, StringConstant.help_how_it_woks)
        tv_faq.text = Utils.getText(context, StringConstant.help_faq)
        tv_video.text = Utils.getText(context, StringConstant.video)
        tv_reach_us.text = Utils.getText(context, StringConstant.reach_us)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        var view = inflater.inflate(R.layout.fragment_help, container, false)

        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.VISIBLE)

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMultiLanguageText()
//            ll_contact.setOnClickListener(this)
        ll_email.setOnClickListener(this)
        //   ll_chat_with_us.setOnClickListener(this)
        ll_TermsAgreement.setOnClickListener(this)
        ll_aboutUs.setOnClickListener(this)
        ll_privacyPolicy.setOnClickListener(this)
        ll_walkThrough.setOnClickListener(this)
        ll_how_it_works.setOnClickListener(this)
        ll_faq.setOnClickListener(this)
        ll_video.setOnClickListener(this)
        ll_reach_us.setOnClickListener(this)

    }

    override fun onDetach() {
        super.onDetach()
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment HelpFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): HelpFragment {
            val fragment = HelpFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        super.onPrepareOptionsMenu(menu)
    }
}// Required empty public constructor
