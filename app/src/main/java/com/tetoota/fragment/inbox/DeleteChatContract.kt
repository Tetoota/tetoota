package com.tetoota.fragment.inbox

import android.app.Activity

/**
 * Created by jitendra.nandiya on 11-12-2017.
 */
class DeleteChatContract {
    interface View{
        fun onDeleteChatSuccessResponce(mString: String, position: Int): Unit
        fun onDeleteChatFailureResponce(mString: String,isServerError: Boolean) : Unit
    }

    interface Presenter{
        fun getDeleteChatData(mActivity : Activity, conver_id : Int?, position : Int?)
    }

    interface DeleteChatApiResult{
        fun onDeleteChatApiSuccess(mString: String, position : Int): Unit
        fun onDeleteChatApiFailure(mString: String,isServerError: Boolean) : Unit
    }
}