package com.tetoota.fragment.inbox

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.userdetail.UserDetailActivity
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.message_view_row.view.*
import org.jetbrains.anko.onClick


/**
 * Created by charchit.kasliwal on 19-09-2017.
 */
class MessageListAdapter(private var mMessageList: ArrayList<ChatHistoryDataResponse>,
                         private val mMessageRowListener: MessageRowCallBack, private val personData: LoginDataResponse)
    : RecyclerView.Adapter<MessageListAdapter.MessageListHolder>() {

    override fun onBindViewHolder(holder: MessageListHolder, position: Int) {
        holder.bindMessageData(holder, position, mMessageList[position], mMessageRowListener, personData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageListHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.message_view_row, parent, false)
        return MessageListHolder(v)
    }

    override fun getItemCount(): Int {
        return mMessageList.size
    }

    interface MessageRowCallBack {
        fun iCellItemClick(mProposalMesgData: ChatHistoryDataResponse, mView: View, p1: Int, isCompleteDealButtonClick: Boolean, isCancelButtonClick: Boolean)
    }

    class MessageListHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tv_name = view.tv_name!!
        private val tv_description = view.tv_description!!
        private val iv_user_img = view.iv_user_img!!
        var rl_mainlayout = view.rl_mainlayout!!
        var view_background = view.view_background!!
        private val tv_time = view.tv_time!!
        private val tv_completeDeal = view.tv_completeDeal!!
        private val outer_frame_layout = view.outer_frame_layout!!
        var productorservicename = view.productorservicename
        var tv_cancel = view.tv_cancel
        var message_unread_count = view.message_unread_count
        //  var exchangeproductorservicename = view.exchangeproductorservicename

        fun bindMessageData(viewHolder: MessageListHolder?, p1: Int,
                            mProposalMesg: ChatHistoryDataResponse, mMessageRowListener: MessageRowCallBack, personData: LoginDataResponse) {

//            tv_cancel.text = Utils.getText(itemView.context, StringConstant.cancel)
            tv_cancel.visibility = View.GONE

            if (mProposalMesg.msg_unread_count != 0) {
                message_unread_count.visibility = View.VISIBLE
                message_unread_count.text = mProposalMesg.msg_unread_count.toString()

            } else {
                message_unread_count.visibility = View.GONE
            }

//            if (mProposalMesg.cancelProposal == 1) {
//                tv_cancel.visibility = View.VISIBLE
//                tv_description.setTextColor(Color.parseColor("#F2A182"));
//            } else {
//                tv_cancel.visibility = View.GONE
//                tv_description.setTextColor(Color.parseColor("#e46e6e6e"));
//            }
            if (personData.user_id == mProposalMesg.msg_send_user_id.toString()) {
                tv_name.text = mProposalMesg.msg_reciever_name

                if (mProposalMesg.msg_reciever_profile_img!!.isEmpty()) {
                    iv_user_img.setImageResource(R.drawable.user_placeholder);
                } else {

                    Picasso.get().load(mProposalMesg.msg_reciever_profile_img).into(iv_user_img)

                }

                /* Glide.with(itemView.context)
                         .load(Utils.getUrl(itemView.context, mProposalMesg.msg_reciever_profile_img!!, "_thumb"))
                         .centerCrop()
                         .placeholder(R.drawable.user_placeholder)
                         .into(iv_user_img)*/
            } else {
                tv_name.text = mProposalMesg.msg_sender_name

                if (mProposalMesg.msg_sender_profile_img!!.isEmpty()) {
                    iv_user_img.setImageResource(R.drawable.user_placeholder);
                } else {

                    Picasso.get().load(mProposalMesg.msg_sender_profile_img).into(iv_user_img)

                }

            }

            tv_description.text = mProposalMesg.chat_message
            if (mProposalMesg.last_conversation_time != null && !mProposalMesg.last_conversation_time.isEmpty()) {
                val mesgTimeStamp = Utils.getTimeStamp(itemView.context, mProposalMesg?.last_conversation_time)
                tv_time.text = Utils.getTimeAgo(mesgTimeStamp, itemView.context)
            }

            tv_completeDeal.text = Utils.getText(itemView.context, StringConstant.complete_deal)
            // Todo: Jitu New Changes
            //  Log.e("mainnnnnnnnnnn",""+ mProposalMesg.exchange_post_type)

            if (mProposalMesg.exchange_post_type!=null && mProposalMesg.exchange_post_type?.isEmpty()) {
                if (personData.user_id == mProposalMesg.proposal_from.toString()) {
                    if (!TextUtils.isEmpty(mProposalMesg.post_title))
                        productorservicename.text = mProposalMesg.post_title
                    else
                        productorservicename.visibility = View.GONE
//                    if (mProposalMesg.trading_status.equals("Pending", true) &&
//                            mProposalMesg.proposal_status.equals("Accept", true)) {
//                        tv_completeDeal.visibility = View.VISIBLE
//
//                    } else {
//                        tv_completeDeal.visibility = View.GONE
//                    }
                } else {
                    // productorservicename.text = (mProposalMesg.tetoota_points + " Tetoota points")
                    if (!mProposalMesg.tetoota_points_only.equals("0"))
                        productorservicename.text = mProposalMesg.tetoota_points_only + " Tetoota points"
                    else
                        productorservicename.visibility = View.GONE
//                    if (mProposalMesg.trading_status.equals("complete", true) &&
//                            mProposalMesg.proposal_status.equals("Accept", true)) {
//                        tv_completeDeal.visibility = View.GONE
//
//                    } else {
//                        tv_completeDeal.visibility = View.GONE
//                    }
                }
            } else {
                // Log.e("elseeeeeeeeeeee",""+ mProposalMesg.exchange_post_type)
                if (personData.user_id == mProposalMesg.proposal_from.toString()) {
                    if (!TextUtils.isEmpty(mProposalMesg.post_title))
                        productorservicename.text = mProposalMesg.post_title
                    else
                        productorservicename.visibility = View.GONE
//                    if (mProposalMesg.trading_status.equals("Pending", true) &&
//                            mProposalMesg.proposal_status.equals("Accept", true)) {
//                        tv_completeDeal.visibility = View.VISIBLE
//                    } else {
//                        tv_completeDeal.visibility = View.GONE
//
//                    }
                } else {
                    if (!TextUtils.isEmpty(mProposalMesg.post_title_reciever))
                        productorservicename.text = mProposalMesg.post_title_reciever
                    else
                        productorservicename.visibility = View.GONE
//                    if (mProposalMesg.receiver_trading_status.equals("Pending", true) &&
//                            mProposalMesg.proposal_status.equals("Accept", true)) {
//                        tv_completeDeal.visibility = View.VISIBLE
//                    } else {
//                        tv_completeDeal.visibility = View.GONE
//                    }
                }
            }

            /************************row color change***********************************/

/*
            if (personData.user_id == mProposalMesg.proposal_from.toString()) {
                if (mProposalMesg.trading_status.equals("Complete", true) &&
                        mProposalMesg.proposal_status.equals("Accept", true)
                        && mProposalMesg.exchange_post_type?.isEmpty()!!) {
                } else if (mProposalMesg.trading_status.equals("Complete", true) &&
                        mProposalMesg.proposal_status.equals("Accept", true)
                        && mProposalMesg.exchange_post_type != "" &&
                        mProposalMesg.receiver_trading_status.equals("Pending", true)) {
                    outer_frame_layout.setBackgroundColor(Color.parseColor("#F9D3C7"));
                    rl_mainlayout.setBackgroundColor(Color.parseColor("#F9D3C7"));
                }
            } else {
                if (mProposalMesg.trading_status.equals("Pending", true) &&
                        mProposalMesg.proposal_status.equals("Accept", true)
                        && mProposalMesg.exchange_post_type?.isEmpty()!!) {
                    outer_frame_layout.setBackgroundColor(Color.parseColor("#F9D3C7"));
                    rl_mainlayout.setBackgroundColor(Color.parseColor("#F9D3C7"));
                } else if (mProposalMesg.trading_status.equals("Pending", true) &&
                        mProposalMesg.proposal_status.equals("Accept", true)
                        && mProposalMesg.exchange_post_type != "" &&
                        mProposalMesg.receiver_trading_status.equals("Complete", true)) {
                    outer_frame_layout.setBackgroundColor(Color.parseColor("#F9D3C7"));
                    rl_mainlayout.setBackgroundColor(Color.parseColor("#F9D3C7"));
                }
            }
*/


//            if (personData.user_id == mProposalMesg.proposal_from.toString()) {
//                if (mProposalMesg.trading_status.equals("Complete", true) &&
//                        mProposalMesg.proposal_status.equals("Accept", true)
//                        && mProposalMesg.exchange_post_type != "" &&
//                        mProposalMesg.receiver_trading_status.equals("Pending", true)) {
//                    outer_frame_layout.setBackgroundColor(Color.parseColor("#F9D3C7"));
//                    rl_mainlayout.setBackgroundColor(Color.parseColor("#F9D3C7"));
//                }
//            } else {
//                if (mProposalMesg.trading_status.equals("Pending", true) &&
//                        mProposalMesg.proposal_status.equals("Accept", true)
//                        && mProposalMesg.exchange_post_type.isEmpty()) {
//                    outer_frame_layout.setBackgroundColor(Color.parseColor("#F9D3C7"));
//                    rl_mainlayout.setBackgroundColor(Color.parseColor("#F9D3C7"));
//                } else if (mProposalMesg.trading_status.equals("Pending", true) &&
//                        mProposalMesg.proposal_status.equals("Accept", true)
//                        && mProposalMesg.exchange_post_type != "" &&
//                        mProposalMesg.receiver_trading_status.equals("Complete", true)) {
//                    outer_frame_layout.setBackgroundColor(Color.parseColor("#F9D3C7"));
//                    rl_mainlayout.setBackgroundColor(Color.parseColor("#F9D3C7"));
//                }
//            }

            /**************************************************************************/


            rl_mainlayout.setOnClickListener(View.OnClickListener {
                mMessageRowListener.iCellItemClick(mProposalMesg, rl_mainlayout, p1, false, false)
            })
//            tv_cancel.setOnClickListener(View.OnClickListener {
//               // mMessageRowListener.iCellItemClick(mProposalMesg, tv_cancel, p1, false, true)
//            })

            //Log.e("eeeeeeee  ",""+ mProposalMesg.post_title)

            // productorservicename.text = mProposalMesg.post_title
        }
    }

    fun removeItem(position: Int) {
        mMessageList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, mMessageList.size)
    }

    fun restoreItem(item: ChatHistoryDataResponse, position: Int) {
        mMessageList.add(position, item)
        notifyItemInserted(position)
    }
}
