package com.tetoota.fragment.inbox.pojo

import android.os.Parcel
import android.os.Parcelable
import com.tetoota.network.errorModel.Meta

data class ChatHistoryResponse(
        val data: ArrayList<ChatHistoryDataResponse?>? = null,
        val meta: Meta? = null) : Parcelable {
    constructor(source: Parcel) : this(
            source.createTypedArrayList(ChatHistoryDataResponse.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeTypedList(data)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ChatHistoryResponse> = object : Parcelable.Creator<ChatHistoryResponse> {
            override fun createFromParcel(source: Parcel): ChatHistoryResponse = ChatHistoryResponse(source)
            override fun newArray(size: Int): Array<ChatHistoryResponse?> = arrayOfNulls(size)
        }
    }
}
