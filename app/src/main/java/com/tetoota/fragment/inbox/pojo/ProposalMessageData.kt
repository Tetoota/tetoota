package com.tetoota.fragment.inbox

import android.os.Parcel
import android.os.Parcelable

data class ProposalMessageData(
        val proposal_status: String? = null,
        val exchange_post_title: String? = null,
        var proposal_id: String? = null,
        val latitude: String? = null,
        val friendliness: String? = null,
        val review_message: String? = null,
        val description: String? = null,
        val availability: String? = null,
        val trading_status: String? = null,
        val title: String? = null,
        val hours_left: String? = null,
        val proposal_duration: String? = null,
        val proposal_to_user_profile_img: String? = null,
        val proposal_to_user_firstName: String? = null,
        val proposal_from_user_profile_img: String? = null,
        val average_badges: Int? = null,
        val proposal_to_user_lastName: String? = null,
        val quality_services: String? = null,
        val exchange_post_record: List<ExchangePostRecordItem?>? = null,
        val exchange_post_id: String? = null,
        val exchange_post_type: String? = null,
        val post_type: String? = null,
        val longitude: String? = null,
        val post_image: String? = null,
        val prop_tetoota_points: String? = null,
        val proposal_from_user_lastName: String? = null,
        val trading_preference: String? = null,
        val creation_date: String? = null,
        val set_quote: String? = null,
        val proposal_create_datetime: String? = null,
        val qualification: String? = null,
        val post_id: String? = null,
        val proposal_from_user_firstName: String? = null,
        val user_id: String? = null,
        val proposal_sender_id: String? = null,
        val share_service_platform: String? = null,
        val response_time: String? = null,
        val set_tetoota_points: String? = null,
        val categories_ids: String? = null,
        val virtual_service: String? = null,
        val proposal_time: String? = null,
        val proposal_from: String? = null



) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.createTypedArrayList(ExchangePostRecordItem.CREATOR),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()


    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(proposal_status)
        writeString(exchange_post_title)
        writeString(proposal_id)
        writeString(latitude)
        writeString(friendliness)
        writeString(review_message)
        writeString(description)
        writeString(availability)
        writeString(trading_status)
        writeString(title)
        writeString(hours_left)
        writeString(proposal_duration)
        writeString(proposal_to_user_profile_img)
        writeString(proposal_to_user_firstName)
        writeString(proposal_from_user_profile_img)
        writeValue(average_badges)
        writeString(proposal_to_user_lastName)
        writeString(quality_services)
        writeTypedList(exchange_post_record)
        writeString(exchange_post_id)
        writeString(exchange_post_type)
        writeString(post_type)
        writeString(longitude)
        writeString(post_image)
        writeString(prop_tetoota_points)
        writeString(proposal_from_user_lastName)
        writeString(trading_preference)
        writeString(creation_date)
        writeString(set_quote)
        writeString(proposal_create_datetime)
        writeString(qualification)
        writeString(post_id)
        writeString(proposal_from_user_firstName)
        writeString(user_id)
        writeString(proposal_sender_id)
        writeString(share_service_platform)
        writeString(response_time)
        writeString(set_tetoota_points)
        writeString(categories_ids)
        writeString(virtual_service)
        writeString(proposal_time)
        writeString(proposal_from)

    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ProposalMessageData> = object : Parcelable.Creator<ProposalMessageData> {
            override fun createFromParcel(source: Parcel): ProposalMessageData = ProposalMessageData(source)
            override fun newArray(size: Int): Array<ProposalMessageData?> = arrayOfNulls(size)
        }
    }

}