package com.tetoota.fragment.inbox.pojo

import android.os.Parcel
import android.os.Parcelable
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.network.errorModel.Meta

data class ProposalApiResponse(
        val data: List<ProposalMessageData?>? = null,
        val meta: Meta? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.createTypedArrayList(ProposalMessageData.CREATOR),
            source.readParcelable<Meta>(Meta::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeTypedList(data)
        writeParcelable(meta, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ProposalApiResponse> = object : Parcelable.Creator<ProposalApiResponse> {
            override fun createFromParcel(source: Parcel): ProposalApiResponse = ProposalApiResponse(source)
            override fun newArray(size: Int): Array<ProposalApiResponse?> = arrayOfNulls(size)
        }
    }
}