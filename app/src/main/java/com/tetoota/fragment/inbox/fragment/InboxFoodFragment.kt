package com.tetoota.fragment.inbox.fragment

//import sun.swing.SwingUtilities2.drawRect
import android.app.Activity
import android.app.AlertDialog
import android.app.NotificationManager
import android.content.*
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.*
import android.widget.Toast
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.proposal.ProposalsContract
import com.tetoota.proposal.ProposalsPresenter
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.proposal.ViewProposalsActivity
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.inbox.*
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.help.HelpActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.message.MessagingFoodActivity
import com.tetoota.proposal.ProposalByIdData
import com.tetoota.userdetail.UserDetailActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.fragment_inbox.*
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [InboxFoodFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [InboxFoodFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class InboxFoodFragment : BaseFragment(), InboxListAdapter.IAdapterClickListener, InboxContract.View,
        SwipeRefreshLayout.OnRefreshListener, ProposalsContract.View,
        View.OnClickListener, InboxListAdapter.IMessageRowClick, MainActivity.onMessageReceiveListener
        , RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, DeleteChatContract.View, InboxFoodListAdapter.IMessageRowClick {
    override fun onFoodListUnListSuccessResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFoodCompleteSuccessResult(message: String?) {

    }

    override fun onGetStageSuccess(message: String, chatStatus: String, data: ProposalByIdData) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onGetStageFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var proposal_id: String? = "";
    var proposalFrom = 0
    var proposalTo = 0
    override fun onDeleteChatSuccessResponce(mString: String, position: Int) {
        var deletedItem: ChatHistoryDataResponse
        deletedItem = mArrayList.get(position)
        val deletedIndex = position
        val name = deletedItem.msg_sender_name
        mInboxAdapter.mMessageListAdapter.removeItem(position)
        // showing snack bar with Undo option
        /*val snackbar = Snackbar
                .make(coordinator_layout, name + " removed from list!", Snackbar.LENGTH_LONG)
        snackbar.setAction("UNDO", View.OnClickListener {
            // undo is selected, restore the deleted item
            mInboxAdapter.mMessageListAdapter.restoreItem(deletedItem, deletedIndex)
        })
        snackbar.setActionTextColor(Color.YELLOW)
        snackbar.show()*/
    }

    override fun onDeleteChatFailureResponce(mString: String, isServerError: Boolean) {
        activity?.toast("Please try again...")
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int, position: Int) {
        if (viewHolder is MessageListAdapter.MessageListHolder) {
            var deletedItem: ChatHistoryDataResponse
            deletedItem = mArrayList.get(position)
            // Log.e("deletedItemmmmmmm", "" + deletedItem)
            /*val deletedIndex = viewHolder.getAdapterPosition()
            val name = deletedItem.msg_sender_name
            mInboxAdapter.mMessageListAdapter.removeItem(viewHolder.getAdapterPosition())

            // showing snack bar with Undo option
            val snackbar = Snackbar
                    .make(coordinator_layout, name + " removed from list!", Snackbar.LENGTH_LONG)
            snackbar.setAction("UNDO", View.OnClickListener {
                // undo is selected, restore the deleted item
                mInboxAdapter.mMessageListAdapter.restoreItem(deletedItem, deletedIndex)
            })
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()*/
            if (deletedItem.exchange_post_type!!.isEmpty()) {
                if (deletedItem.trading_status == "Pending" && deletedItem.proposal_status == Constant.ACCEPT) {
                    Toast.makeText(context, "The deal is yet to be completed", Toast.LENGTH_SHORT).show()
                    mInboxAdapter.mMessageListAdapter.notifyItemChanged(position)
                } else {
                    openDeleteChatDialog(this.activity!!, deletedItem.conver_id, position)
                }
            } else {
                if (deletedItem.trading_status == "Pending" && deletedItem.proposal_status == Constant.ACCEPT
                        && deletedItem.receiver_trading_status == "Pending") {
                    Toast.makeText(context, "The deal is yet to be completed", Toast.LENGTH_SHORT).show()
                    mInboxAdapter.mMessageListAdapter.notifyItemChanged(position)
                } else {
                    openDeleteChatDialog(this.activity!!, deletedItem.conver_id, position)
                }
            }
        }
    }

    private lateinit var mInboxAdapter: InboxFoodListAdapter
    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    lateinit var mMesgList: MainActivity.onMessageReceiveListener
    private lateinit var mArrayList: ArrayList<ChatHistoryDataResponse>
    private var mParam2: String? = null
    private lateinit var personData: LoginDataResponse
    private val p = Paint()
    private var cardUpdateReceiver: CardUpdateReceiver? = null
    private val mInboxPresenter: InboxPresenter by lazy {
        InboxPresenter(this@InboxFoodFragment)
    }
    private val mProposalPresenter: ProposalsPresenter by lazy {
        ProposalsPresenter(this@InboxFoodFragment)
    }
    private val mDeleteChatPresenter: DeleteChatPresenter by lazy {
        DeleteChatPresenter(this@InboxFoodFragment)
    }

    /**
     * the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
            var bundle = arguments
            if (bundle!!.containsKey(Constant.PROPOSAL_FROM))
                proposalFrom = bundle.getInt(Constant.PROPOSAL_FROM)
            if (bundle!!.containsKey(Constant.PROPOSAL_TO))
                proposalTo = bundle.getInt(Constant.PROPOSAL_TO)
            Log.i("88888   ", " " + bundle.getString(Constant.CHAT_TYPE))
        }
        setHasOptionsMenu(true)
        var languageMap = TetootaApplication.getTextValuesHashMap()
        languageMap!![Utils.getText(context, StringConstant.tetoota_ok)].toString()
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
        personData = Gson().fromJson(json, LoginDataResponse::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_inbox, container, false)
        return view
    }

    fun getChatHistory() {
        if (Utils.haveNetworkConnection(this.context!!)) {
            mInboxPresenter.getChatHistoryData(this.activity!!, personData.user_id!!, Constant.FOOD)
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            error_btn_retry -> {
                view_list_swipe_refresh.isRefreshing = true
                getDataFromServer()
            }
            fb_chat -> {
                val intent = HelpActivity.newMainIntent(this.activity!!)
                activity!!.startActivity(intent)
            }
        }
    }

    /**
     * the bundle saved instance state
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Utils.saveInt(Constant.USER_PROPOSAL_COUNT.toString(), 0)
        val notificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
        error_btn_retry.setOnClickListener(this@InboxFoodFragment)
        fb_chat.setOnClickListener(this@InboxFoodFragment)
    }

    override fun onRefresh() {
        view_list_swipe_refresh.isRefreshing = true
        getDataFromServer()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getChatHistory()
        rv_message_list.layoutManager = LinearLayoutManager(context)
        rv_message_list.setHasFixedSize(true)
        if (personData != null) {
            rl_nrf.visibility = View.GONE;
            mInboxAdapter = InboxFoodListAdapter(this.activity!!, personData, iAdapterClickListener = this, iMessageClick = this,
                    screenHeight = heightCalculation())
            mInboxAdapter.getFragmentObject(this)
            rv_message_list.adapter = mInboxAdapter
            view_list_swipe_refresh.setOnRefreshListener(this)
            view_list_swipe_refresh.setColorSchemeResources(
                    R.color.refresh_progress_1,
                    R.color.refresh_progress_2,
                    R.color.refresh_progress_3)
        } else {
            rl_nrf.visibility = View.GONE;
            mInboxAdapter = InboxFoodListAdapter(this.activity!!, personData, iAdapterClickListener = this, iMessageClick = this,
                    screenHeight = heightCalculation())
            mInboxAdapter.getFragmentObject(this)
            rv_message_list.adapter = mInboxAdapter
            view_list_swipe_refresh.setOnRefreshListener(this)
            view_list_swipe_refresh.setColorSchemeResources(
                    R.color.refresh_progress_1,
                    R.color.refresh_progress_2,
                    R.color.refresh_progress_3)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter("updateChat")
        cardUpdateReceiver = CardUpdateReceiver()
        activity!!.registerReceiver(cardUpdateReceiver, intentFilter)
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        view_list_swipe_refresh.post(Runnable {
            view_list_swipe_refresh.isRefreshing = true
            getDataFromServer()
        })
        if (InboxFragment.isAcceptedRejected) {
            if (InboxFragment.position != -1) {
                InboxFragment.isAcceptedRejected = false;
                mInboxAdapter.setItem(InboxFragment.position)
                InboxFragment.position = -1
            }
        }

    }
    /*override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
             activity.menuInflater.inflate(R.menu.dashboard_menu, menu)
             menu?.findItem(R.id.action_search)?.isVisible = true
             super.onCreateOptionsMenu(menu, inflater)
     }*/

    override fun onDetach() {
        super.onDetach()
    }

    private fun getDataFromServer() {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mInboxPresenter.getProposalData(this.activity!!, personData.user_id.toString())
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
        }
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        public var isAcceptedRejected = false
        public var position: Int = -1

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment InboxFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): InboxFoodFragment {
            val fragment = InboxFoodFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun ongetAllProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        if (view != null) {
            view_list_swipe_refresh.isRefreshing = false
            hideErrorView()
            mInboxPresenter.getChatHistoryData(this.activity!!, personData.user_id!!, Constant.FOOD)
            //  Log.e("7777777777777777777","" + mProposalMesgData)
            mInboxAdapter.setElements(mProposalMesgData)

        }
    }

    override fun ongetAllProposalFailure(message: String) {
        if (view != null) {
            //  Log.e("ongetAllProposalFailure ","" + message)
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mInboxPresenter.getChatHistoryData(this.activity!!, personData.user_id!!, Constant.FOOD)
            view_list_swipe_refresh.isRefreshing = false
            if (message == "No Records Found") {
                rl_nrf.visibility = View.VISIBLE
            } else {
                rl_nrf.visibility = View.GONE

            }
        }
    }

    override fun onChatHistorySuccess(mChatHistoryResponse: ArrayList<ChatHistoryDataResponse>) {
        super.onChatHistorySuccess(mChatHistoryResponse)
        try {
            //Log.e("888888888888888888", "" + mChatHistoryResponse)
            if (!mChatHistoryResponse.isEmpty()) {
                rl_nrf.visibility = View.GONE
                mArrayList = mChatHistoryResponse
                mInboxAdapter.setInboxHistroyElement(mChatHistoryResponse)
                Log.e("88888888888888FOOD   ", "" + mArrayList.size)
                if (proposalFrom != 0 && proposalTo != 0) {
                    loop@ for (i in mArrayList.indices) {
                        Log.e("888888888888==FOOD====", "   " + proposalFrom + "   " + proposalTo + "\n" + mArrayList[i].msg_recieved_user_id!!.equals(proposalFrom) + "   " + mArrayList[i].msg_send_user_id!!.equals(proposalTo))
                        if (mArrayList[i].msg_recieved_user_id!!.equals(proposalFrom) || mArrayList[i].msg_send_user_id!!.equals(proposalFrom)) {
                            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
                            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                            InboxFragment.isAcceptedRejected = false
                            InboxFragment.position = i
                            val intent = MessagingFoodActivity.newMainIntent(this.activity!!)
                            intent?.putExtra("inboxPojo", mArrayList[i])
                            intent?.putExtra("inboxSelectedProposal", mArrayList[i].msg_sender_name)
                            intent?.putExtra("isProposalAccepted", "true")
                            ActivityStack.getInstance(this.activity!!)
                            startActivity(intent)
                            break@loop
                        }

                    }
                    proposalTo = 0
                    proposalFrom = 0
                }
            } else {
                rl_nrf.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onChatHistoryFailure(message: String) {
        super.onChatHistoryFailure(message)
        //  Log.e("onChatHistoryFailure ","" + message)
        try {
            if (message == "null") {
                rl_nrf.visibility = View.VISIBLE
            } else {
                rl_nrf.visibility = View.GONE

            }
        } catch (e: Exception) {

        }

    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .25f).toInt()
        return getTopMarginH
    }

    fun gettetootaPts(): Int {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        println("Points Deduction ${personData.tetoota_points}")
        return Integer.parseInt(personData.tetoota_points.toString())
    }

    override fun cellItemClick(mViewClickType: String, mProposalMesgData: ProposalMessageData, mView: View, p1: Int) {
        if (mViewClickType == "cellRowClick") {
            val intent = MessagingFoodActivity.newMainIntent(this.activity!!)
            ActivityStack.getInstance(this.activity!!)
            startActivity(intent)
        } else if (mViewClickType == "btnNo") {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                mProposalPresenter.proposalAction(this.activity!!, mProposalMesgData, Constant.DECLINE, p1, personData.user_id!!, Constant.FOOD)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        } else if (mViewClickType == "reply") {
            InboxFragment.isAcceptedRejected = false
            InboxFragment.position = p1
            val intent = MessagingFoodActivity.newMainIntent(this.activity!!)
            intent?.putExtra("proposalMesgData", mProposalMesgData)
            ActivityStack.getInstance(this.activity!!)
            startActivity(intent)
        } else if (mViewClickType == "btnYes") {
            if (mProposalMesgData.exchange_post_type == "") {
                if (gettetootaPts() >= Integer.parseInt(mProposalMesgData.prop_tetoota_points.toString())) {
                    mProposalMesgData.exchange_post_record
                    if (Utils.haveNetworkConnection(this.activity!!)) {
                        showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                        proposal_id = mProposalMesgData.proposal_id
                        mProposalPresenter.proposalAction(this.activity!!, mProposalMesgData, Constant.ACCEPT, p1, personData.user_id!!, Constant.FOOD)
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                } else {
                    activity!!.toast("You have not sufficient points to accept this proposal")
                }
            } else {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                    proposal_id = mProposalMesgData.proposal_id
                    mProposalPresenter.proposalAction(this.activity!!, mProposalMesgData, Constant.ACCEPT, p1, personData.user_id!!, Constant.FOOD)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
        } else if (mViewClickType == "cvMain") {
            val intent = UserDetailActivity.newMainIntent(this.activity!!)
            intent!!.putExtra("cvMain", mProposalMesgData.proposal_from)
            ActivityStack.getInstance(this.activity!!)
            startActivity(intent)
        }
    }

    override fun onProposalsApiSuccessResult(message: String?) {
        hideProgressDialog()
        view_list_swipe_refresh.isRefreshing = false
    }

    override fun onProposalActionSuccessREsult(message: String?, mProposalMesg: ProposalMessageData, acceptanceType: String, pos: Int) {
        hideProgressDialog()
        //  Log.e("message ", "" + message + "mProposalMesg " + mProposalMesg + "acceptanceType " + acceptanceType)

        if (acceptanceType == Constant.ACCEPT) {
            activity!!.toast("Proposal accepted")
            if (Utils.haveNetworkConnection(activity!!)) {
                mInboxPresenter.sendMesg(activity!!,
                        personData.user_id, mProposalMesg.proposal_sender_id.toString(),
                        mProposalMesg.post_id.toString(), "Proposal has been accepted", mProposalMesg.proposal_id.toString(), Constant.FOOD)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
            mInboxAdapter.setItem(pos)
        } else {
            activity!!.toast("Proposal declined")
            mInboxAdapter.setItem(pos)
        }
    }

    override fun onProposalActionCancelSuccessREsult(message: String?, propoalId: String, acceptanceType: String, pos: Int) {
        super.onProposalActionCancelSuccessREsult(message, propoalId, acceptanceType, pos)
        hideProgressDialog()
        //  Log.e(" elsee message ", "" + message + "propoalId " + propoalId + "acceptanceType " + acceptanceType)
        activity!!.toast("Proposal Cancelled")
        mInboxAdapter.setItemCancel(pos)


    }

    override fun onProposalsFailureResult(message: String) {
        view_list_swipe_refresh.isRefreshing = false

    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            rv_message_list.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    fun showErrorView(throwable: String) {
        if (view != null) {
            if (error_layout.visibility === View.GONE) {
                error_layout.visibility = View.VISIBLE
                error_txt_cause.text = throwable
                rv_message_list.visibility = View.GONE
                err_title.visibility = View.GONE
            }
        }
    }

    override fun messageItemClick(mViewClickType: ChatHistoryDataResponse, mView: View, p1: Int, isCompleteDealButtonClick: Boolean, isCancelButtonClick: Boolean) {
        if (isCompleteDealButtonClick) {
            val intent = ViewProposalsActivity.newMainIntent(this.activity!!)
            intent?.putExtra("proposalId", mViewClickType.proposal_id)
            ActivityStack.getInstance(this.activity!!)
            startActivity(intent)
        } else if (isCancelButtonClick) {
            //  Log.e("isCancelButtonClick ","" + isCancelButtonClick)
            if (Utils.haveNetworkConnection(this.activity!!)) {
                showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                mProposalPresenter.proposalActionCancel(this.activity!!, mViewClickType.proposal_id.toString(), "Cancel", p1, personData.user_id!!, Constant.FOOD)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        } else {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            InboxFragment.isAcceptedRejected = false
            InboxFragment.position = p1
            val intent = MessagingFoodActivity.newMainIntent(this.activity!!)
            intent?.putExtra("inboxPojo", mViewClickType)
            intent?.putExtra("inboxSelectedProposal", mViewClickType.msg_sender_name)
            intent?.putExtra("isProposalAccepted", "true")
            ActivityStack.getInstance(this.activity!!)
            startActivity(intent)
        }
    }

    override fun onMessageReceived(position: Int) {

    }

    override fun onChatMesgSuccess(message: String) {
        super.onChatMesgSuccess(message)
        // getChatHistory()

    }

    override fun onChatMesgFailure(message: String) {
        super.onChatMesgFailure(message)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        super.onPrepareOptionsMenu(menu)
    }

    fun callSwipe() {
        val itemTouchHelperCallback = RecyclerItemTouchHelper(
                0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mInboxAdapter.recycleView)

        val itemTouchHelperCallback1 = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                Log.e("Swipe", "onMove")
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                // Row is swiped from recycler view
                // remove it from adapter
                Log.e("Swipe", "onSwiped")
            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                Log.e("Swipe", "onChildDraw")
            }
        }
        // attaching the touch helper to recycler view
        ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(mInboxAdapter.recycleView)
    }

    private fun openDeleteChatDialog(mActivity: Activity, conver_id: Int?, position: Int?) {
        val alertDialog = AlertDialog.Builder(context) //Read Update
        alertDialog.setTitle(Utils.getText(mActivity, StringConstant.chat_delete_title))
        alertDialog.setMessage(Utils.getText(mActivity, StringConstant.chat_delete_alert))
        alertDialog.setPositiveButton("Ok", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {


                mDeleteChatPresenter.getDeleteChatData(mActivity, conver_id, position)
            }
        })
        alertDialog.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                var deletedItem: ChatHistoryDataResponse
                deletedItem = mArrayList.get(position!!)
                val deletedIndex = position
                val name = deletedItem.msg_sender_name
                mInboxAdapter.mMessageListAdapter.removeItem(position)
                mInboxAdapter.mMessageListAdapter.restoreItem(deletedItem, deletedIndex)
            }
        })
        alertDialog.show()  //<-- See This!
    }

    inner class CardUpdateReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                var test = intent.action
                getChatHistory()
                Log.e("testtttttttttttttt", test)

            }

        }

    }


    override fun onStop() {
        super.onStop()
        if (cardUpdateReceiver != null) {
            activity!!.unregisterReceiver(cardUpdateReceiver)
        }
    }

}// Required empty public constructor