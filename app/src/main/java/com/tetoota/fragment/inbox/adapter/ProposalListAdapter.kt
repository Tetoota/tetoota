package com.tetoota.fragment.inbox
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.tetoota.R
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.proposal_view_row.view.*

/**
 * Created by charchit.kasliwal on 09-08-2017.
 */
class ProposalListAdapter(private var mProposalList: MutableList<ProposalMessageData>,
                          private var iProposalCellClick: ProposalListAdapter.IProposalCallBack)
    : RecyclerView.Adapter<ProposalListAdapter.ProposalViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProposalListAdapter.ProposalViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.proposal_view_row, parent, false)
        return ProposalViewHolder(v)

        // return null
    }

    override fun onBindViewHolder(viewHolder: ProposalListAdapter.ProposalViewHolder, p1: Int) {
        viewHolder.bindProposalData(viewHolder, p1, mProposalList[p1], iProposalCellClick)
    }

    override fun getItemCount(): Int {
        return mProposalList.size
    }

    interface IProposalCallBack {
        fun cellProposalClick(mViewClickType: String, cellRow: ProposalMessageData, mView: View, p1: Int): Unit
    }

    class ProposalViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val iv_user_pic = view.iv_user_pic!!
        private val tv_username = view.tv_username!!
        private val tv_time = view.tv_time!!
        private val cv_main = view.cv_main!!
        private val btn_yes = view.btn_yes!!
        private val btn_no = view.btn_no!!
     //   private val tv_with_in = view.tv_with_in!!
        //   private val tv_proposal_status = view.tv_proposal_status!!
        //   private val tv_proposal_desc = view.tv_proposal_desc!!
        private val rl_bodylayout = view.rl_bodylayout!!
        private val tv_reply = view.tv_reply!!
        private val tv_timeleft = view.tv_timeleft!!
        private val tv_title = view.tv_title!!
      //  private val tv_exchange_title = view.tv_exchange_title!!
      //  private val tv_i_want = view.tv_i_want!!
       // private val tv_iwill_offer = view.tv_iwill_offer!!

        private var builder: SpannableStringBuilder? = null

        fun bindProposalData(viewHolder: ProposalViewHolder?, p1: Int, mProposalMesg: ProposalMessageData, iProposalCellClick: IProposalCallBack) {


           btn_yes.text = Utils.getText(itemView.context, StringConstant.accept)
           btn_no.text = Utils.getText(itemView.context, StringConstant.decline)
           tv_reply.text = Utils.getText(itemView.context, StringConstant.chat_reply)
           var iwant = Utils.getText(itemView.context, StringConstant.str_tv_wants)
           var iwill = Utils.getText(itemView.context, StringConstant.str_tv_and_will_offer)

            tv_username.text = "${mProposalMesg.proposal_from_user_firstName.toString()} ${mProposalMesg.proposal_from_user_lastName.toString()}"





            if (mProposalMesg.exchange_post_id == "0") {
                var strw = mProposalMesg.set_quote
                val newStr = strw!!.replaceFirst("PER", "")
                var sss = "<font color='#3a3a3a'> "+iwill +" </font> " +"\""+mProposalMesg.exchange_post_title + " "+Utils.getText(itemView.context, StringConstant.str_tetoota_points)+"\""
//                var within =  "<font color='#3a3a3a'> "+ mProposalMesg.proposal_time+ "</font> "
//                tv_title.text = (Html.fromHtml("<font color='#3a3a3a'>"+iwant+"</font> "+mProposalMesg.title + sss+ within))
                tv_title.text = (Html.fromHtml("<font color='#3a3a3a'>"+iwant + " 1 "+newStr+ " of "+"</font> "+"\""+mProposalMesg.title+"\"" + sss))
            } else {
                var strw = mProposalMesg.set_quote
                val newStr = strw!!.replaceFirst("PER", "")
                var sss = "<font color='#3a3a3a'> "+iwill+ " 1 "+ newStr +" of "+ " </font> "+"\""+mProposalMesg.exchange_post_title+"\""
                var within =  "<font color='#3a3a3a'> "+ mProposalMesg.proposal_time+ "</font> "
//                tv_title.text = (Html.fromHtml("<font color='#3a3a3a'>"+iwant+"</font> "+mProposalMesg.title + sss+ within))
                tv_title.text = (Html.fromHtml("<font color='#3a3a3a'>"+iwant+"</font> "+"\""+mProposalMesg.title+"\"" +sss+"\""))

            }

          /*  tv_title.text = (Html.fromHtml("<font color='#3a3a3a'>"+iwant+"</font> "+mProposalMesg.title))

            if (mProposalMesg.exchange_post_id == "0") {
                tv_exchange_title.text = (Html.fromHtml("<font color='#3a3a3a'>"+iwill+"</font> "+mProposalMesg.exchange_post_title + " Tetoota points"))
            } else {
                tv_exchange_title.text = (Html.fromHtml("<font color='#3a3a3a'>"+iwill+"</font> "+mProposalMesg.exchange_post_title))
            }
            tv_with_in.text = mProposalMesg.proposal_time*/


            val str = Utils.getText(itemView.context, StringConstant.proposal_time_left)
            tv_timeleft.text = str.replace("%d", ""+mProposalMesg.hours_left)

           // val strReplace = str.replace("%d", "%s")
           // tv_timeleft.text = String.format(strReplace, mProposalMesg.hours_left)
           // tv_timeleft.text =  ("You have "+mProposalMesg.hours_left+" hours to accept this proposal")
            tv_time.text = mProposalMesg.proposal_duration + " " + Utils.getText(itemView.context, StringConstant.proposal_ago)

/*
            Glide.with(itemView.context)
                    .load(Utils.getUrl(itemView.context, mProposalMesg.proposal_from_user_profile_img!!))
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .into(iv_user_pic)
*/

            if (mProposalMesg.proposal_from_user_profile_img!!.isEmpty()) {
                iv_user_pic.setImageResource(R.drawable.user_placeholder);
            } else {

                Picasso.get().load(mProposalMesg.proposal_from_user_profile_img).into(iv_user_pic)

            }

            btn_yes.setOnClickListener({
                iProposalCellClick.cellProposalClick("btnYes", mProposalMesg, btn_yes, p1)
            })

            tv_reply.setOnClickListener(View.OnClickListener {
                iProposalCellClick.cellProposalClick("reply", mProposalMesg, tv_reply, p1)
            })

            btn_no.setOnClickListener({
                iProposalCellClick.cellProposalClick("btnNo", mProposalMesg, btn_no, p1)
            })
            cv_main.setOnClickListener({
                iProposalCellClick.cellProposalClick("cvMain", mProposalMesg, cv_main, p1)
            })
        }
    }
}