package com.tetoota.fragment.inbox

//import kotlinx.android.synthetic.main.message_view_row.view.*
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tetoota.R
import com.tetoota.fragment.inbox.fragment.InboxFragment
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.login.LoginDataResponse
import kotlinx.android.synthetic.main.inbox_list.view.*
import kotlinx.android.synthetic.main.product_list.view.*

/**
 * Created by charchit.kasliwal on 09-08-2017.
 */
class InboxListAdapter(private var mContext: Context, private var personData: LoginDataResponse,
                       private var mProposalList: MutableList<ProposalMessageData> = mutableListOf(),
                       private var mMessageList: ArrayList<ChatHistoryDataResponse> =
                               ArrayList<ChatHistoryDataResponse>(), private var iMessageClick: IMessageRowClick,
                       private var iAdapterClickListener: IAdapterClickListener, var screenHeight: Int)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), MessageListAdapter.MessageRowCallBack,
        ProposalListAdapter.IProposalCallBack {
    lateinit var inboxFragmentcs: InboxFragment
    var recycleView: RecyclerView? = null
    private val PROPOSAL_VIEW = 0
    private val MESSAGE_LIST_VIEW = 1
    lateinit var mMessageListAdapter: MessageListAdapter
    var isFirstTime: Boolean = true

    fun getFragmentObject(inboxFragment: InboxFragment) {
        this.inboxFragmentcs = inboxFragment
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == PROPOSAL_VIEW) { // LIST VIEW HOLDER
            val v = LayoutInflater.from(parent.context).inflate(R.layout.product_list, parent, false)
            return ProposalViewHolder(v)
        } else if (viewType == MESSAGE_LIST_VIEW) { // LIST VIEW HOLDER
            val v = LayoutInflater.from(parent.context).inflate(R.layout.inbox_list, parent, false)
            return MessageViewHolder(v)
        }
        return onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, p1: Int) {
        if (viewHolder is ProposalViewHolder) {
            var mMessageListAdapter = ProposalListAdapter(mProposalList as ArrayList<ProposalMessageData>,
                    this)
            viewHolder.product_list.layoutManager = LinearLayoutManager(mContext)
            viewHolder.product_list.setHasFixedSize(true)
            //  viewHolder.product_list.adapter = mMessageListAdapter
        } else if (viewHolder is MessageViewHolder) {
            println("Bind View holder $p1")
            mMessageListAdapter = MessageListAdapter(mMessageList, this, personData)
            recycleView = viewHolder.rv_inbox_list
            viewHolder.rv_inbox_list.layoutManager = LinearLayoutManager(mContext)
            viewHolder.rv_inbox_list.setHasFixedSize(true)
            viewHolder.rv_inbox_list.adapter = mMessageListAdapter

            if (isFirstTime) {
                isFirstTime = false
                inboxFragmentcs.callSwipe()
            }
        }
    }

    fun isPositionProposal(): Int {
        return 0
    }

    fun isPositionMessages(): Int {
        return 1
    }

    fun setElements(mProposalList: ArrayList<ProposalMessageData>) {
        this.mProposalList = mProposalList
        notifyDataSetChanged()
    }

    fun setInboxHistroyElement(mMessageList: ArrayList<ChatHistoryDataResponse>) {
        this.mMessageList = mMessageList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return 2
    }

    /**
     * the item position
     * getItem View Type
     */
    override fun getItemViewType(position: Int): Int {
        if (isPositionProposal() == position) {
            return PROPOSAL_VIEW
        } else if (isPositionMessages() == position) {
            return MESSAGE_LIST_VIEW
        }
        return MESSAGE_LIST_VIEW
    }

    class ProposalViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val product_list = view.product_list!!
    }

    class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rv_inbox_list = view.rv_inbox_list!!
    }

    /**
     * Message Cell Item Click
     *
     */
    override fun iCellItemClick(mMesgData: ChatHistoryDataResponse, mView: View, p1: Int, isCompleteDealButtonClick: Boolean, isCancelDealButtonClick: Boolean) {
        iMessageClick.messageItemClick(mMesgData, mView, p1, isCompleteDealButtonClick, isCancelDealButtonClick)
    }

    /**
     * Proposal Click Call Back
     * mViewClickType : type of click (btn_yes,btn_no, cellRowClick)
     */
    override fun cellProposalClick(mViewClickType: String, cellRow: ProposalMessageData, mView: View, p1: Int) {
        iAdapterClickListener.cellItemClick(mViewClickType, cellRow, mView, p1)
    }

    fun setItem(pos: Int?) {
        if (pos != null) {
            if (mProposalList != null && mProposalList.size > 0) {
                mProposalList.removeAt(pos)
                notifyItemRemoved(pos)
                notifyItemRangeRemoved(pos, itemCount)
            }
        }
    }

    fun setItemCancel(pos: Int?) {
        if (pos != null) {
            mMessageList.removeAt(pos)
            notifyItemRemoved(pos)
            notifyItemRangeRemoved(pos, itemCount)
        }
    }


    interface IAdapterClickListener {
        fun cellItemClick(mViewClickType: String, mProposalMesgData: ProposalMessageData, mView: View, p1: Int): Unit
    }

    interface IMessageRowClick {
        fun messageItemClick(mViewClickType: ChatHistoryDataResponse, mView: View, p1: Int, isCompleteDealButtonClick: Boolean, isCancelButtonClick: Boolean): Unit
    }


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
//        recycleView = recyclerView
    }

}