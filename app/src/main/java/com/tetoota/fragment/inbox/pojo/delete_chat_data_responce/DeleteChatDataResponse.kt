package com.tetoota.fragment.inbox.pojo.delete_chat_data_responce

data class DeleteChatDataResponse(
	val data: List<Any?>? = null,
	val meta: Meta? = null
)
