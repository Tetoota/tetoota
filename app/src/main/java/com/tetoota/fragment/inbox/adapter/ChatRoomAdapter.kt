package com.tetoota.fragment.inbox.adapter


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.details.ProductDetailActivity
import com.tetoota.fragment.inbox.pojo.OneToOneChatDataResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.service_product.FullImageActivity
import com.tetoota.userdetail.UserDetailActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.chat_item_self.view.*
import org.jetbrains.anko.onClick


/**
 * Created by charchit.kasliwal on 27-09-2017.
 * Property of HITESHI INFOTECH
 */
class ChatRoomAdapter(private var mContext: Context, private var personData: LoginDataResponse,
                      private var mMessageList: ArrayList<OneToOneChatDataResponse>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val SELF = 1
    private val OTHER = 2
    private val ADMIN = 3

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // view type is to identify where to render the chat message
        // left or right
        val itemView: View
        Log.i(javaClass.name, "=============onCreateViewHolder=/=")
        if (viewType == SELF) {
            // self message
            itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.chat_item_self, parent, false)
            return ViewHolder(itemView)
        } else if (viewType == ADMIN) {
            // self message
            itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.chat_item_admin, parent, false)
            return AdminViewHolder(itemView)
        } else {
            // others message
            itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.chat_item_other, parent, false)
            return ReciverViewHolder(itemView)
        }
    }

    fun setElement(mMessageList: ArrayList<OneToOneChatDataResponse>) {
        this.mMessageList = mMessageList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mMessageList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        Log.i(javaClass.name, "=============onBindViewHolder=/=")
        if (holder is ViewHolder) {
            var message: OneToOneChatDataResponse = mMessageList[position]
//            Log.i(javaClass.name, "=============ViewHolder==" + message.msg_type)
            holder.message.text = message.chat_message
            val mesgTimeStamp = Utils.getTimeStamp(mContext, message.chatMesgDateTime!!)
            holder.timestamp.text = Utils.getTimeAgo(mesgTimeStamp, mContext)

            if (personData.profile_image!!.isEmpty()) {
                holder.iv_user.setImageResource(R.drawable.user_placeholder)
            } else {
                Picasso.get().load(personData.profile_image!!).into(holder.iv_user)
            }

            if (personData.profile_image!!.isEmpty()) {
                //  mContext.toast("Profile image is not available")
            } else {
                holder.iv_user.onClick {
                    val intent = FullImageActivity.newMainIntent(mContext)
                    intent!!.putExtra("profile_image", personData.profile_image!!)
                    ActivityStack.getInstance(mContext)
                    mContext.startActivity(intent)
                }
            }

/*
            Glide.with(mContext)
                    .load(Utils.getUrl(mContext, personData.profile_image!!))
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .into(holder.iv_user)
*/
        } else if (holder is AdminViewHolder) {
            var message: OneToOneChatDataResponse = mMessageList[position]
//            Log.i(javaClass.name, "=============ViewHolder=/=" + message.msg_type)
            holder.message.text = message.chat_message
            val mesgTimeStamp = Utils.getTimeStamp(mContext, message.chatMesgDateTime!!)
            holder.timestamp.text = Utils.getTimeAgo(mesgTimeStamp, mContext)

            if (message.msg_type.equals("Admin", true)) {
                Picasso.get().load(R.drawable.iv_tetoota_icon).into(holder.iv_user)
            }

            if (personData.profile_image!!.isEmpty()) {
                //  mContext.toast("Profile image is not available")

            } else {
                holder.iv_user.onClick {
                    val intent = FullImageActivity.newMainIntent(mContext)
                    intent!!.putExtra("profile_image", personData.profile_image!!)
                    ActivityStack.getInstance(mContext)
                    mContext.startActivity(intent)

                }
            }

/*
            Glide.with(mContext)
                    .load(Utils.getUrl(mContext, personData.profile_image!!))
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .into(holder.iv_user)
*/
        } else if (holder is ReciverViewHolder) {
            var message: OneToOneChatDataResponse = mMessageList[position]
//            Log.i(javaClass.name, "=============ReciverViewHolder==" + message.msg_type)
            holder.message.text = message.chat_message
            val mesgTimeStamp = Utils.getTimeStamp(mContext, message.chatMesgDateTime!!)
            holder.timestamp.text = Utils.getTimeAgo(mesgTimeStamp, mContext)
            if (personData.user_id == message.msg_send_user_id) {
                if (message.msg_type.equals("Admin", true)) {
                    Picasso.get().load(R.drawable.iv_tetoota_icon).into(holder.iv_user)
                } else if (message.msg_recieved_profile_img!!.isEmpty()) {
                    holder.iv_user.setImageResource(R.drawable.user_placeholder)
                } else {
                    Picasso.get().load(message.msg_recieved_profile_img!!).into(holder.iv_user)
                }

                if (message.msg_recieved_profile_img!!.isEmpty()) {
                    //  mContext.toast("Profile image is not available")

                } else {
                    holder.iv_user.onClick {
                        val intent = FullImageActivity.newMainIntent(mContext)
                        intent!!.putExtra("profile_image", message.msg_recieved_profile_img!!)
                        ActivityStack.getInstance(mContext)
                        mContext.startActivity(intent)

                    }
                }
/*
                Glide.with(mContext)
//                        .load("http://tetoota.hiteshi.com/assets/images/profile_image/"+message.msg_recieved_profile_img)
                        .load(Utils.getUrl(mContext, ""+message.msg_recieved_profile_img ,"_thumb"))
                        .centerCrop()
                        .placeholder(R.drawable.user_placeholder)
                        .into(holder.iv_user)
*/
            } else {

                if (message.msg_sender_profile_img!!.isEmpty()) {
                    holder.iv_user.setImageResource(R.drawable.user_placeholder)
                } else {

                    Picasso.get().load(message.msg_sender_profile_img!!).into(holder.iv_user)

                }
                if (message.msg_sender_profile_img!!.isEmpty()) {
                    // mContext.toast("Profile image is not available")

                } else {
                    holder.iv_user.onClick {
                        val intent = UserDetailActivity.newMainIntent(mContext)
                        intent!!.putExtra("cvMain", message.msg_send_user_id)
                        mContext.startActivity(intent)

                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
//        Log.i(javaClass.name, "=============getItemViewType=/=")
        val message = mMessageList[position]
//        Log.i(javaClass.name, "=============ViewHolder99==" + position + "   " + message.msg_send_user_id + "    " + message.msg_type + "    " + message.msg_reciever_user_id)
        return if (Utils.loadPrefrence(Constant.USER_ID, "", mContext).toString() == message.msg_send_user_id) {
            SELF
        } else if (message.msg_type.equals("Admin", ignoreCase = true)) {
            ADMIN
        } else
            position
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var message = view.message!!
        var iv_user = view.iv_user!!
        var timestamp = view.timestamp!!
    }

    class AdminViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var message = view.message!!
        var iv_user = view.iv_user!!
        var timestamp = view.timestamp!!
    }

    class ReciverViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var message = view.message!!
        var iv_user = view.iv_user!!
        var timestamp = view.timestamp!!
    }
}
