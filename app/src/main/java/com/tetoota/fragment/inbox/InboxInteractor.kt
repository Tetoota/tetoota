package com.tetoota.fragment.inbox

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.tetoota.TetootaApplication
import com.tetoota.fragment.inbox.pojo.ChatHistoryResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatResponse
import com.tetoota.fragment.inbox.pojo.ProposalApiResponse
import com.tetoota.fragment.inbox.pojo.SendChatMesg
import com.tetoota.proposal.IncludeProposalsResponse
import com.tetoota.proposal.ProposalByIdData
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by charchit.kasliwal on 09-08-2017.
 */
class InboxInteractor {
    var call: Call<ProposalApiResponse>? = null
    var mInboxApiListener: InboxContract.InboxApiListener

    constructor(mInboxApiListener: InboxContract.InboxApiListener) {
        this.mInboxApiListener = mInboxApiListener
    }

    fun getAllProposalApiCalling(mActivity: Activity, mUserId: String) {
        call = TetootaApplication.getHeader()
                .getProposalMessageList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId)
        call!!.enqueue(object : Callback<ProposalApiResponse> {
            override fun onResponse(call: Call<ProposalApiResponse>?, response: Response<ProposalApiResponse>?) {
                println("Success")
                var mWalkThroughData: ProposalApiResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.data != null) {
                        if (response.body()?.data?.size!! > 0) {
                            mInboxApiListener.onApiSuccess(response.body()!!.data as ArrayList<ProposalMessageData>, "success")
                        } else {
                            mInboxApiListener.onApiFailure("No Records Found")
                        }
                    }
                } else {
                    mInboxApiListener.onApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ProposalApiResponse>?, t: Throwable?) {
                mInboxApiListener.onApiFailure(t?.message.toString())
            }
        })
    }

    fun getChatHistoryData(mActivity: Activity, mUserId: String, type: String) {
        var call: Call<ChatHistoryResponse> = TetootaApplication.getHeader()
                .getChatHistory(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId, type)
        call.enqueue(object : Callback<ChatHistoryResponse> {
            override fun onResponse(call: Call<ChatHistoryResponse>?, response: Response<ChatHistoryResponse>?) {
                println("Success")
                if (response?.code() == 200) {
                    if (response.body()?.data != null) {
                        mInboxApiListener.chatHistorySuccess(response.body()!!.data)
                    } else {
                        mInboxApiListener.chatHistoryFailure(response.message())
                    }
                } else {
                    mInboxApiListener.chatHistoryFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ChatHistoryResponse>?, t: Throwable?) {
                mInboxApiListener.chatHistoryFailure(t?.message.toString())
            }
        })
    }


    fun sendChatMesg(mActivity: Activity, msg_send_user_id: String, msg_recieved_user_id: String
                     , post_id: String, chat_message: String, proposal_id: String,type:String) {
        var call: Call<SendChatMesg> = TetootaApplication.getHeader()
                .sendChatMesg(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), msg_send_user_id!!,
                        msg_recieved_user_id, post_id, chat_message, proposal_id,type)
        call.enqueue(object : Callback<SendChatMesg> {
            override fun onResponse(call: Call<SendChatMesg>?, response: Response<SendChatMesg>?) {
                println("Success")
                if (response?.code() == 200) {
                    if (response.body()?.data != null) {
                        mInboxApiListener.chatSendMesgSuccess(response.message())
                    } else {
                        mInboxApiListener.chatSendMesgFailure(response.message())
                    }
                } else {
                    mInboxApiListener.onApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<SendChatMesg>?, t: Throwable?) {
                mInboxApiListener.onApiFailure(t?.message.toString())
            }
        })
    }


    /**
     *
     */
    fun oneToOneChatRecord(mActivity: Activity, msg_send_user_id: String, msg_recieved_user_id: String,
                           user_id: String, type: String,by:String) {
        var call: Call<OneToOneChatResponse> = TetootaApplication.getHeader()
                .getOneToOneChatRecord(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), msg_send_user_id,
                        msg_recieved_user_id, user_id, type,by)
        call.enqueue(object : Callback<OneToOneChatResponse> {
            override fun onResponse(call: Call<OneToOneChatResponse>?, response: Response<OneToOneChatResponse>?) {
                println("Success")
                if (response?.code() == 200) {
                    if (response.body()?.data != null) {
                        mInboxApiListener.oneToOneSuccess("success", response.body()!!.data)
                    } else {
                        mInboxApiListener.oneToOneFailure("failure")
                    }
                } else {
                    mInboxApiListener.oneToOneFailure("failure")
                }
            }

            override fun onFailure(call: Call<OneToOneChatResponse>?, t: Throwable?) {
                mInboxApiListener.oneToOneFailure(t?.message.toString())
            }
        })
    }


    fun getProposalAllStage(mActivity: Activity, proposalFrom: String, proposalTo: String,
                            postId: String, proposalID: String, type: String,userId:String,by:String) {
        var call: Call<ResponseBody> = TetootaApplication.getHeader()
                .GetProposalAllStage(Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        proposalFrom, proposalTo, postId, proposalID, type,userId,by)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mInboxApiListener.getStageFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                var statusCode = 0
                var chatStatus = ""
                var data: ProposalByIdData = ProposalByIdData()
                if (response?.code() == 200) {
                    try {
                        var jobj = JSONObject(response.body()!!.string())

                        var metaJobj = jobj.getJSONObject("meta")
                        if (metaJobj.has("code"))
                            statusCode = metaJobj.getInt("code")
                        if (metaJobj.has("chat_status"))
                            chatStatus = metaJobj.getString("chat_status")
                        if (jobj.has("data")) {
                            val json = JSONTokener(jobj.getString("data")).nextValue()
                            if (json is JSONObject) {
                                var s = jobj.getString("data")
                                val parser = JsonParser()
                                val mJson = parser.parse(s)
                                val gson = Gson()
                                data = gson.fromJson(mJson, ProposalByIdData::class.java)
                            }
                        }
                        if (statusCode == 200 && chatStatus != null) {
                            var data = ProposalByIdData()
                            mInboxApiListener.getStageSuccess("success", chatStatus, data)
                        } else if (statusCode == 204) {
                            if (chatStatus != null) {
                                mInboxApiListener.getStageSuccess("success", chatStatus, data)
                            } else {
                                mInboxApiListener.getStageFailure("failure")
                            }
                        } else {
                            mInboxApiListener.getStageFailure("failure")
                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace();
                    } catch (e: JSONException) {
                        e.printStackTrace();
                    }
                } else {
                    mInboxApiListener.getStageFailure("failure")
                }
            }

        })
    }



    fun getProposalFoodAllStage(mActivity: Activity, proposalFrom: String, proposalTo: String,
                            postId: String, proposalID: String, type: String,userId:String) {
        var call: Call<ResponseBody> = TetootaApplication.getHeader()
                .GetProposalFoodAllStage(Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), proposalFrom, proposalTo, postId, proposalID, type,userId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mInboxApiListener.getStageFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                var statusCode = 0
                var chatStatus = ""
                var data: ProposalByIdData = ProposalByIdData()
                if (response?.code() == 200) {
                    try {
                        var jobj = JSONObject(response.body()!!.string())

                        var metaJobj = jobj.getJSONObject("meta")
                        if (metaJobj.has("code"))
                            statusCode = metaJobj.getInt("code")
                        if (metaJobj.has("chat_status"))
                            chatStatus = metaJobj.getString("chat_status")
                        if (jobj.has("data")) {
                            val json = JSONTokener(jobj.getString("data")).nextValue()
                            if (json is JSONObject) {
                                var s = jobj.getString("data")
                                val parser = JsonParser()
                                val mJson = parser.parse(s)
                                val gson = Gson()
                                data = gson.fromJson(mJson, ProposalByIdData::class.java)
                            }
                        }
                        if (statusCode == 200 && chatStatus != null) {
                            var data = ProposalByIdData()
                            mInboxApiListener.getStageSuccess("success", chatStatus, data)
                        } else if (statusCode == 204) {
                            if (chatStatus != null) {
                                mInboxApiListener.getStageSuccess("success", chatStatus, data)
                            } else {
                                mInboxApiListener.getStageFailure("failure")
                            }
                        } else {
                            mInboxApiListener.getStageFailure("failure")
                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace();
                    } catch (e: JSONException) {
                        e.printStackTrace();
                    }
                } else {
                    mInboxApiListener.getStageFailure("failure")
                }
            }

        })
    }


    fun proposalYes(mActivity: Activity, postId: String, proposalTo: String, proposalFrom: String) {
        var call: Call<ResponseBody> = TetootaApplication.getHeader()
                .ProposalYes(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        postId, proposalTo, proposalFrom)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mInboxApiListener.getStageFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                var statusCode = 0
                var chatStatus = ""
                var data: ProposalByIdData = ProposalByIdData()
                Log.i(javaClass.name, "====================888   " + response?.code())
                if (response?.code() == 200) {
                    try {
                        var jobj = JSONObject(response.body()!!.string())

                        var metaJobj = jobj.getJSONObject("meta")
                        if (metaJobj.has("code"))
                            statusCode = metaJobj.getInt("code")
                        if (metaJobj.has("chat_status"))
                            chatStatus = metaJobj.getString("chat_status")
                        if (jobj.has("data")) {
                            val json = JSONTokener(jobj.getString("data")).nextValue()
                            if (json is JSONObject) {
                                var s = jobj.getString("data")
                                val parser = JsonParser()
                                val mJson = parser.parse(s)
                                val gson = Gson()
                                data = gson.fromJson(mJson, ProposalByIdData::class.java)
                            }
                        }
                        if (statusCode == 200 && chatStatus != null) {
                            var data = ProposalByIdData()
                            mInboxApiListener.getProposalYes("success")
                        } else if (statusCode == 204) {
                            if (chatStatus != null) {
                                mInboxApiListener.getProposalYes("success")
                            } else {
                                mInboxApiListener.getProposalFailure("failure")
                            }
                        } else {
                            mInboxApiListener.getProposalFailure("failure")
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace();
                    }
                } else {
                    mInboxApiListener.getProposalFailure("failure")
                }
            }

        })
    }
}
