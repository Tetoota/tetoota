package com.tetoota.fragment.inbox

import android.app.Activity
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatDataResponse
import com.tetoota.message.GetStageResponse
import com.tetoota.proposal.ProposalByIdData

/**
 * Created by charchit.kasliwal on 09-08-2017.
 */

class InboxPresenter : InboxContract.Presenter, InboxContract.InboxApiListener {


    var mInboxContract: InboxContract.View

    private val mInboxInteractor: InboxInteractor by lazy {

        InboxInteractor(this)
    }

    constructor(mInboxContract: InboxContract.View) {
        this.mInboxContract = mInboxContract
    }

    override fun getProposalData(mActivity: Activity, userId: String) {

        mInboxInteractor.getAllProposalApiCalling(mActivity, userId)
    }

    override fun getChatHistoryData(mActivity: Activity, userId: String, type: String) {
        mInboxInteractor.getChatHistoryData(mActivity, userId, type)
    }

    override fun getOneToOneChatData(mActivity: Activity, msg_send_user_id: String,
                                     msg_rec_user_id: String, user_id: String, post_id: String, type: String,by:String) {
        mInboxInteractor.oneToOneChatRecord(mActivity, msg_send_user_id, msg_rec_user_id, user_id, type,by)
    }

    override fun sendMesg(mActivity: Activity, msg_send_user_id: String?,
                          msg_rec_user_id: String, post_id: String, chat_mesg: String,
                          proposal_id: String, type: String) {
        if (msg_send_user_id != null) {
            mInboxInteractor.sendChatMesg(mActivity, msg_send_user_id, msg_rec_user_id, post_id, chat_mesg, proposal_id, type)

        }
    }


    override fun onApiSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        mInboxContract.ongetAllProposalSuccess(mProposalMesgData, message)

    }

    override fun onApiFailure(message: String) {
        mInboxContract.ongetAllProposalFailure(message)
    }

    override fun chatHistorySuccess(mChatHistoryResponse: ArrayList<ChatHistoryDataResponse?>?) {
        super.chatHistorySuccess(mChatHistoryResponse)
        mInboxContract.onChatHistorySuccess(mChatHistoryResponse as ArrayList<ChatHistoryDataResponse>)
    }

    override fun chatHistoryFailure(message: String) {
        super.chatHistoryFailure(message)

        mInboxContract.onChatHistoryFailure(message)

    }


    override fun chatSendMesgSuccess(message: String) {
        super.chatSendMesgSuccess(message)
        mInboxContract.onChatMesgSuccess(message)
    }

    override fun chatSendMesgFailure(message: String) {
        super.chatSendMesgFailure(message)
        mInboxContract.onChatMesgFailure(message)
    }


    override fun oneToOneSuccess(message: String, mChatList: ArrayList<OneToOneChatDataResponse?>?) {
        super.oneToOneSuccess(message, mChatList)

        mInboxContract.onOneToOneSuccess(message, mChatList as ArrayList<OneToOneChatDataResponse>)

    }

    override fun oneToOneFailure(message: String) {
        mInboxContract.onOneToOneFailure(message)
    }

    override fun getStageData(mActivity: Activity, proposal_from: String, proposal_to: String, post_id: String, proposal_id: String, type: String, userId: String, screenFrom: String) {
        mInboxInteractor.getProposalAllStage(mActivity, proposal_from, proposal_to, post_id, proposal_id, type, userId, screenFrom)
    }

    override fun getStageFoodData(mActivity: Activity, proposal_from: String, proposal_to: String, post_id: String, proposal_id: String, type: String, userId: String) {
        mInboxInteractor.getProposalFoodAllStage(mActivity, proposal_from, proposal_to, post_id, proposal_id, type, userId)
    }


    override fun getProposalYes(mActivity: Activity, postId: String, proposalTo: String, proposalFrom: String) {
        mInboxInteractor.proposalYes(mActivity, postId, proposalTo, proposalFrom)
    }

    override fun getStageSuccess(message: String, chatStatus: String, data: ProposalByIdData) {
        super.getStageSuccess(message, chatStatus, data)
        mInboxContract.onGetStageSuccess(message, chatStatus, data)
    }

    override fun getStageFailure(message: String) {
        super.getStageFailure(message)
    }
}