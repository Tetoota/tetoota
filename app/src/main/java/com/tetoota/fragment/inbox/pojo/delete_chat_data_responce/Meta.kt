package com.tetoota.fragment.inbox.pojo.delete_chat_data_responce

data class Meta(
	val code: Int? = null,
	val message: String? = null,
	val status: Boolean? = null
)
