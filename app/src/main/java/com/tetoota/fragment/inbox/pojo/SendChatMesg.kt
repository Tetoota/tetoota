package com.tetoota.fragment.inbox.pojo
import com.tetoota.network.errorModel.Meta
data class SendChatMesg(
	val data: List<Any?>? = null,
	val meta: Meta? = null
)
