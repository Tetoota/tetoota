package com.tetoota.fragment.inbox

import android.app.Activity
import com.tetoota.fragment.inbox.pojo.ChatHistoryDataResponse
import com.tetoota.fragment.inbox.pojo.OneToOneChatDataResponse
import com.tetoota.message.GetStageResponse
import com.tetoota.proposal.ProposalByIdData

/**
 * Created by charchit.kasliwal on 09-08-2017.
 */
class InboxContract {
    interface View {
        fun ongetAllProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String)
        fun ongetAllProposalFailure(message: String)
        fun onChatMesgSuccess(message: String) {}
        fun onChatMesgFailure(message: String) {}
        fun onChatHistorySuccess(mChatHistoryResponse: ArrayList<ChatHistoryDataResponse>) {}
        fun onChatHistoryFailure(message: String) {}
        fun onOneToOneSuccess(message: String, mChatList: ArrayList<OneToOneChatDataResponse>) {}
        fun onOneToOneFailure(message: String) {}
        fun onGetProposalYes(message: String) {}
        fun onGetStageSuccess(message: String, chatStatus: String, data: ProposalByIdData)
        fun onGetStageFailure(message: String)
    }

    internal interface Presenter {
        fun getProposalData(mActivity: Activity, userId: String)
        fun getChatHistoryData(mActivity: Activity, userId: String,type:String)
        fun getOneToOneChatData(mActivity: Activity, msg_send_user_id: String,
                                msg_rec_user_id: String, user_id: String, post_id: String,type:String,by:String)

        fun sendMesg(mActivity: Activity, msg_send_user_id: String?,
                     msg_rec_user_id: String, post_id: String, chat_mesg: String,
                     proposal_id: String,type:String)

        fun getStageData(mActivity: Activity,
                         proposal_from: String, proposal_to: String, post_id: String,
                         proposal_id: String,type:String,userId:String,by:String)

        fun getStageFoodData(mActivity: Activity,
                         proposal_from: String, proposal_to: String, post_id: String,
                         proposal_id: String,type:String,userId:String)

        fun getProposalYes(mActivity: Activity, postId: String, proposalTo: String, proposalFrom: String)

    }

    interface InboxApiListener {
        fun chatHistorySuccess(mChatHistoryResponse: ArrayList<ChatHistoryDataResponse?>?) {}
        fun chatHistoryFailure(message: String) {}
        fun chatSendMesgSuccess(message: String) {}
        fun chatSendMesgFailure(message: String) {}
        fun onApiSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String)
        fun onApiFailure(message: String)
        fun oneToOneSuccess(message: String, mChatList: ArrayList<OneToOneChatDataResponse?>?) {}
        fun oneToOneFailure(message: String) {}
        fun getProposalYes(message: String) {}
        fun getProposalFailure(message: String) {}
        fun getStageSuccess(message: String, chatStatus: String, data: ProposalByIdData) {}
        fun getStageFailure(message: String) {}
    }
}