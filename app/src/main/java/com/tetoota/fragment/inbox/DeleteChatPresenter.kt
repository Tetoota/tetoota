package com.tetoota.fragment.inbox

import android.app.Activity

/**
 * Created by jitendra.nandiya on 11-12-2017.
 */
class DeleteChatPresenter : DeleteChatContract.Presenter, DeleteChatContract.DeleteChatApiResult {
    var mDeleteChatView: DeleteChatContract.View

    val mFavoriteInteractor : DeleteChatInteractor by lazy {
        DeleteChatInteractor(this)
    }

    constructor(mFavoriteView: DeleteChatContract.View) {
        this.mDeleteChatView = mFavoriteView
    }

    override fun getDeleteChatData(mActivity: Activity, conver_id: Int?, position : Int?) {
        mFavoriteInteractor.getDeleteChat(mActivity, conver_id, position)
    }

    override fun onDeleteChatApiSuccess(mString: String, position : Int) {
        mDeleteChatView.onDeleteChatSuccessResponce(mString, position)
    }

    override fun onDeleteChatApiFailure(mString: String, isServerError: Boolean) {
        mDeleteChatView.onDeleteChatFailureResponce(mString, isServerError)
    }
}