package com.tetoota.fragment.inbox;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

/**
 * Created by jitendra.nandiya on 29-11-2017.
 */

public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener listener;

    public RecyclerItemTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            if(viewHolder.getItemViewType()==1){
                final View foregroundView = ((MessageListAdapter.MessageListHolder) viewHolder).getRl_mainlayout();
                getDefaultUIUtil().onSelected(foregroundView);
            }
//            final View foregroundView = ((MessageListAdapter.MessageListHolder) viewHolder).getRl_mainlayout();
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                int actionState, boolean isCurrentlyActive) {
        final View foregroundView = ((MessageListAdapter.MessageListHolder) viewHolder).getRl_mainlayout();
        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
//        final View backgroundView = ((MessageListAdapter.MessageListHolder) viewHolder).getView_background();
//        backgroundView.setVisibility(View.GONE);
        final View foregroundView = ((MessageListAdapter.MessageListHolder) viewHolder).getRl_mainlayout();
        getDefaultUIUtil().clearView(foregroundView);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
//        final View backgroundView = ((MessageListAdapter.MessageListHolder) viewHolder).getView_background();
//        backgroundView.setVisibility(View.VISIBLE);
        final View foregroundView = ((MessageListAdapter.MessageListHolder) viewHolder).getRl_mainlayout();
        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
