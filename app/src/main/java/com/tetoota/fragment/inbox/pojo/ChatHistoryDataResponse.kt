package com.tetoota.fragment.inbox.pojo

import android.os.Parcel
import android.os.Parcelable

data class ChatHistoryDataResponse(
        val msg_sender_profile_img: String? = null,
        val msg_reciever_profile_img: String? = null,
        val tetoota_points_only: String? = null,
        val msg_recieved_user_id: Int? = null,
        val tetoota_points: String? = null,
        val post_title_reciever: String? = null,
        val last_conversation_time: String? = null,
        val msg_send_user_id: Int? = null,
        val msg_sender_name: String? = null,
        val msg_reciever_name: String? = null,
        val chat_message: String? = null,
        val proposal_id: String? = null,
        val post_id: String? = null,
        val conver_id: Int? = null,
        val post_title: String? = null,
        val proposal_status: String? = null,
        val trading_status: String? = null,
        val proposal_from: Int? = null,
        val proposal_to: Int? = null,
        val exchange_post_type: String? = null,
        val cancelProposal: Int? = null,
        val msg_unread_count: Int? = null,
        val receiver_trading_status: String? = null,
        val post_image: String? = "")
    : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(msg_sender_profile_img)
        writeString(msg_reciever_profile_img)
        writeString(tetoota_points_only)
        writeValue(msg_recieved_user_id)
        writeString(tetoota_points)
        writeString(post_title_reciever)
        writeString(last_conversation_time)
        writeValue(msg_send_user_id)
        writeString(msg_sender_name)
        writeString(msg_reciever_name)
        writeString(chat_message)
        writeString(proposal_id)
        writeString(post_id)
        writeValue(conver_id)
        writeString(post_title)
        writeString(proposal_status)
        writeString(trading_status)
        writeValue(proposal_from)
        writeValue(proposal_to)
        writeString(exchange_post_type)
        writeValue(cancelProposal)
        writeValue(msg_unread_count)
        writeString(receiver_trading_status)
        writeString(post_image)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ChatHistoryDataResponse> = object : Parcelable.Creator<ChatHistoryDataResponse> {
            override fun createFromParcel(source: Parcel): ChatHistoryDataResponse = ChatHistoryDataResponse(source)
            override fun newArray(size: Int): Array<ChatHistoryDataResponse?> = arrayOfNulls(size)
        }
    }
}