package com.tetoota.fragment.home

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.customviews.ReportConversationDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.fragment.dashboard.HomeContract
import com.tetoota.fragment.dashboard.HomePresenter
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.nearby.NearByRecyclerAdapter
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.service_product.FullImageActivity
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_nearby.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.toast

class TrendingActivity : BaseActivity(), HomeContract.View, ServiceContract.View, NearByRecyclerAdapter.IAdapterClick,
        NearByRecyclerAdapter.PaginationAdapterCallback, ReportConversationDialog.IDialogListener,
        ProfileCompletionDialog.IDialogListener {
    override fun onWishListApiSuccessResult(mCategoriesList: List<Any?>?, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (cellRow.is_favourite == 0) {
            cellRow.is_favourite = 1
            this.toast("Marked Favorite")
        } else {
            cellRow.is_favourite = 0
            this.toast("Marked Unfavorite")
        }
        mNearByRecyclerAdapter.notifyItemChanged(p1)
    }

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInvalidTextAPiFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@TrendingActivity)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@TrendingActivity)
    }

    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
        Utils.hideSoftKeyboard(this)
        if (message == "report") {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mHomePresenter.report(this, mServiceId, mMesgDesc)
        }
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            this.toast(message)
        } else {
            this.toast(message)
        }
    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this)) {
                val intent = ProfileDetailActivity.newMainIntent(this)
                ActivityStack.getInstance(this)
                startActivity(intent)
            } else {
                this.toast(Utils.getText(this, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    var linearLayoutManager: LinearLayoutManager? = null
    private lateinit var mNearByRecyclerAdapter: NearByRecyclerAdapter
    private val EDITSERVICEPRODUCT_REQUEST: Int = 13
    var menu: Menu? = null
    private var context: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trending)
        context = applicationContext
        fetchTrendingList()
        initToolbar()
        initViews()
    }

    private fun initViews() {
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        view_list.setHasFixedSize(true)
        view_list.itemAnimator = DefaultItemAnimator()
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        view_list.layoutManager = linearLayoutManager
        mNearByRecyclerAdapter = NearByRecyclerAdapter(this, iAdapterClickListener = this,
                iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        view_list.adapter = mNearByRecyclerAdapter
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        toolbar_title.text = Utils.getText(this, StringConstant.str_trending)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    private fun fetchTrendingList() {
        if (Utils.haveNetworkConnection(this)) {
            progress_view.visibility = View.VISIBLE
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mHomePresenter.getTrendingList(this, personData.user_id.toString())
        } else {
            this.toast(Utils.getText(this, StringConstant.str_check_internet))
            progress_view.visibility = View.GONE
        }
    }

    override fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        val mProductData = cellRow as ServicesDataResponse
        if (mString == "favorite") {
            showPopupMenu(mView, mProductData, mAttributeValue, p1)
        } else if (mString == "cellClick") {
            this.toast("Under Development")
        } else if (mString == "shareClick") {
        } else if (mString.equals("Cell Item click")) {
            if (Utils.haveNetworkConnection(this)) {
                val intent = Intent(this, ProductDetailActivity::class.java)
                intent.putExtra("mProductData", mProductData)
                intent.putExtra("productType", "services")
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    val transitionActivityOptions: ActivityOptions =
                            ActivityOptions.makeSceneTransitionAnimation(this,
                                    mView, "iv_image")
                    startActivity(intent, transitionActivityOptions.toBundle())
                    // startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST, transitionActivityOptions.toBundle())
                    this.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                } else {
                    startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST)
                    //startActivity(intent)
                    this.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                }
            } else {
                this.toast(Utils.getText(this, StringConstant.str_check_internet))
            }
        }
    }

    private fun showPopupMenu(view: View, mProductData: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val popup = PopupMenu(this, view)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        if (mAttributeValue == "0") {
            favItem?.title = Utils.getText(this, StringConstant.home_unfavourite)
        } else {
            favItem?.title = Utils.getText(this, StringConstant.favorite)
        }
        menu?.findItem(R.id.menu_share)!!.title = Utils.getText(this, StringConstant.share_text)
        menu?.findItem(R.id.menu_report_this)!!.title = Utils.getText(this, StringConstant.report_this)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
                    var url: String = ""
                    if (mProductData.image != null && mProductData.image_thumb != null) {
                        url = Utils.getUrl(this, mProductData.image, mProductData.image_thumb, false)
                    } else {
                        url = Utils.getUrl(this, mProductData.image!!)
                    }
                    // shareProduct(mProductData.title!!, url)
                    shareService(mProductData.title!!, url)
                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this)) {
                        mServicePresenter.markFavorite(this, mProductData, mAttributeValue, p1)
                    } else {
                        this.toast(Utils.getText(this, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    ReportConversationDialog(this, Constant.DIALOG_LOGIN_FAILURE_ALERT, mProductData.id.toString(),
                            this@TrendingActivity, getString(R.string.str_report_issue)).show()
                }
            }
            true
        }
        popup.show()
    }

    fun shareService(mTitle: String, mImageUrl: String) {
        try {
            val text = mTitle
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher),
                        null, null))
            } catch (e: NullPointerException) {
            }
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
//            shareIntent.type = "image/*"
//            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            //  startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
            startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: android.content.ActivityNotFoundException) {
        }
    }

/*
    fun shareProduct(mTitle: String, mImageUrl: String) {
        val text = mTitle
        val pictureUri = Uri.parse(mImageUrl)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share images..."))
    }
*/*/


    override fun onResume() {
        super.onResume()
      //  fetchTrendingList()

    }

    override fun retryPageLoad() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this) * .2f).toInt()
        return getTopMarginH
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@TrendingActivity)
        finish()
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, FullImageActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        progress_view.visibility = View.GONE
       // loadItemsLayout_recyclerView.visibility = View.GONE
        if (mServiceData != null && mServiceData.size > 0) {
            rl_nrf.visibility = View.GONE
            mNearByRecyclerAdapter.addAll(mServiceData as List<ServicesDataResponse?>)
            mNearByRecyclerAdapter.notifyDataSetChanged()
        } else {
            rl_nrf.visibility = View.VISIBLE
        }
    }


    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
