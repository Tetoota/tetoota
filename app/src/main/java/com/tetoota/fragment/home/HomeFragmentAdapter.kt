package com.tetoota.fragment.dashboardnew

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.google.android.gms.maps.model.LatLng
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.fragment.dashboard.DashboardSliderDataResponse
import com.tetoota.fragment.dashboard.HomeDataResponse
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.home.TrendingActivity
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.help.HelpActivity
import com.tetoota.service_product.FullImageActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.bottom_view_row.view.*
import kotlinx.android.synthetic.main.dashboard_list_row.view.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.util.*

/**
 * Created by charchit.kasliwal on 24-07-2017.
 */
class HomeFragmentAdapter(val mContext: Context, var mTrendingList: List<HomeDataResponse> = ArrayList<HomeDataResponse>(),
                          var mDashSliderResp: List<DashboardSliderDataResponse> = ArrayList<DashboardSliderDataResponse>(),
                          var mRecentActivityResp: List<DataItem> = ArrayList<DataItem>(),
                          var iAdapterClickListener: IAdapterClickListener, var screenHeight: Int)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TRENDING_VIEW = 0
    private val SERVICES_VIEW = 1
    private val PRODUCTS_VIEW = 2
    private val FOOTER_VIEW = 3
    private var Message: String = "";
//    var handler = Handler()
//    var handlerFadout = Handler()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType === TRENDING_VIEW) { // LIST VIEW HOLDER
            val v = LayoutInflater.from(parent.context).inflate(R.layout.trending_data_row, parent, false)
            return TrendingViewHolder(v)
        } else if (viewType === SERVICES_VIEW) { // LIST VIEW HOLDER
            val v = LayoutInflater.from(parent.context).inflate(R.layout.dashboard_list_row, parent, false)
            return ServiceViewHolder(v)
        } else if (viewType === PRODUCTS_VIEW) { // LIST VIEW HOLDER
            val v = LayoutInflater.from(parent.context).inflate(R.layout.dashboard_list_row, parent, false)
            return ProductViewHolder(v)
        } else if (viewType === FOOTER_VIEW) { // LIST VIEW HOLDER
            val v = LayoutInflater.from(parent.context).inflate(R.layout.bottom_view_row, parent, false)
            return FooterViewHolder(v)
        }
        return onCreateViewHolder(parent, viewType)

    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, p1: Int) {
        if (mTrendingList.size != 0 && mTrendingList != null) {
            if (viewHolder is TrendingViewHolder) {
                if (mTrendingList.get(0).trending?.size!! > 0 && mTrendingList.get(0).trending!!.get(0)!!.id != null) {
                    println("Tredning ${mTrendingList[0].trending!!.size}")
                    mTrendingList[0].trending as ArrayList
                    viewHolder.bindTrendingData(viewHolder as TrendingViewHolder?, p1, mTrendingList[0].trending!![0] as ServicesDataResponse, screenHeight, iAdapterClickListener, mContext)
                }
            } else if (viewHolder is ServiceViewHolder) {
                // TODO
                if (mTrendingList.get(0).services?.size!! > 0 && mTrendingList.get(0).services!!.get(0)!!.id != null) {
                    viewHolder.bindServiceData(viewHolder as ServiceViewHolder?, p1,
                            mTrendingList[0].services!![0] as ServicesDataResponse, screenHeight, iAdapterClickListener, mContext)
                }
                // viewHolder?.bindServiceData(viewHolder as ServiceViewHolder?,p1, mServicesList[p1], screenHeight,iAdapterClickListener)
            } else if (viewHolder is ProductViewHolder) {
                // TODO
                if (mTrendingList.get(0).products?.size!! > 0 && (mTrendingList.get(0).products!!.get(0)!!.id != null)) {
                    viewHolder.bindProductViewHolder(viewHolder as ProductViewHolder?, p1,
                            mTrendingList[0].products!![0] as ServicesDataResponse, screenHeight, iAdapterClickListener, mContext)
                }
            }
        }
        if (viewHolder is FooterViewHolder) {
            // TODO
            if (mRecentActivityResp != null) {
                viewHolder.bindFooterData(viewHolder as FooterViewHolder?, iAdapterClickListener, mRecentActivityResp, mContext)
            }
        }

    }

    private fun isPositionTrending(position: Int): Boolean {
        return position == 0
    }

    private fun isPositionServices(position: Int): Boolean {
        return position == 1
    }

    private fun isPositionProducts(position: Int): Boolean {
        return position == 2
    }

    private fun isPositionFooter(position: Int): Boolean {
        return position == 3
    }

    fun setElements(mHomeList: ArrayList<Any>, mDashSliderResp: ArrayList<DashboardSliderDataResponse>) {
        this.mTrendingList = mHomeList as ArrayList<HomeDataResponse>
        this.mDashSliderResp = mDashSliderResp
    }

    fun setElementsRecent(mHomeList: List<DataItem>) {
        this.mRecentActivityResp = mHomeList as ArrayList<DataItem>
    }

    fun setElementsRecentMessage(message: String) {
        this.Message = message

    }

    private fun getLastPosition() = if (mTrendingList.lastIndex == -1) 0 else mTrendingList.lastIndex

    override fun getItemCount(): Int {
        return mTrendingList.size + 3
    }

    fun removeAll() {
//        for (i : int =0 ; i < mProductList.size ;i++) {
//            mProductList.remove(position)
//            notifyItemRemoved(position)
//        }
//        for (position in mProductList.indices) {
//            mProductList.remove(position)
//            notifyItemRemoved(position)
//        }
        val size = mTrendingList.size
        if (size > 0) {
//            for (i in 0 until size) {
//                mProductList.remove(0)
//            }
            this.mTrendingList;
            notifyItemRangeRemoved(0, size)
        }
    }


    override fun getItemViewType(position: Int): Int {
        when (position) {
            0 -> {
                if (isPositionTrending(position))
                    return TRENDING_VIEW
            }

            1 -> {
                if (isPositionServices(position))
                    return SERVICES_VIEW
            }

            2 -> {
                if (isPositionProducts(position))
                    return PRODUCTS_VIEW
            }

            3 -> {
                if (isPositionFooter(position))
                    return FOOTER_VIEW
            }
            else -> return TRENDING_VIEW

        }
        return null!!
    }

    class TrendingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mImageView = view.iv_product_service!!
        // val tv_time_date = view.tv_time_date!!
        val title = view.title!!
        val tv_description = view.tv_description!!
        val tv_reviews = view.tv_reviews!!
        val rtbProductRating = view.rtbProductRating!!
        val iv_user = view.iv_user!!
        val rl_maincontent = view.rl_maincontent!!
        val iv_three_dot = view.iv_three_dot!!
        val iv_share = view.iv_share!!
        val tv_tetoota = view.tv_tetoota!!
        val tv_distance = view.tv_distance!!
        val rl_distance = view.rl_distance!!
        val tv_user_count = view.tv_user_count!!
        val tv_see_all = view.tv_see_all!!
        val rl_toplayout = view.rl_toplayout!!
        private var tetootaApplication: TetootaApplication? = null

        fun bindTrendingData(viewHolder: TrendingViewHolder?, p1: Int, mTrendingDataResponse: ServicesDataResponse, height: Int, iAdapterClickListener: IAdapterClickListener, context: Context) {
            // tv_services_product.text = "Trending"
            tetootaApplication = context.getApplicationContext() as TetootaApplication

            title.text = mTrendingDataResponse.title
            tv_description.text = mTrendingDataResponse.content
            val context = context;
            // tv_review.text = "${mTrendingDataResponse.reviews} " + Utils.getText(context, StringConstant.home_rating_review)
            tv_see_all.text = Utils.getText(context, StringConstant.str_tv_see_all)

            tv_reviews.text = (mTrendingDataResponse.reviews?.toFloat().toString())
            rtbProductRating.rating = (mTrendingDataResponse.reviews!!.toFloat())

            tv_user_count.text = ("(" + mTrendingDataResponse.total_usercount + " " + Utils.getText(context, StringConstant.home_rating_review) + ")")

//            mImageView.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height)
            var url: String = ""

/*
            if (mTrendingDataResponse!!.image != null && mTrendingDataResponse!!.image_thumb != null) {

                url = Utils.getUrl(itemView.context, mTrendingDataResponse!!.image!!, mTrendingDataResponse!!.image_thumb!!, false)
            } else {
                url = Utils.getUrl(itemView.context, mTrendingDataResponse!!.image!!)
            }
*/

            //  mImageView.loadUrl(url)

            if (mTrendingDataResponse.image!!.isEmpty()) {
                mImageView.setImageResource(R.drawable.queuelist_place_holder)
            } else {

                Picasso.get().load(mTrendingDataResponse.image).into(mImageView)
            }

            tv_tetoota.text = mTrendingDataResponse.tetoota_points.toString()

            // var lat = Utils.loadPrefrence(Constant.USER_LOC_LAT, "0.0", itemView.context)
            // var long = Utils.loadPrefrence(Constant.USER_LOC_LONG, "0.0", itemView.context)


            if (tetootaApplication!!.myLatitude == 0.0 && tetootaApplication!!.myLongitude == 0.0) {
                rl_distance.visibility == View.GONE
            } else {
                rl_distance.visibility == View.VISIBLE

                try {
                    tv_distance.text = String.format("%.2f", Utils.checkDistance(LatLng(tetootaApplication!!.myLatitude.toDouble(), tetootaApplication!!.myLongitude.toDouble()),
                            LatLng(mTrendingDataResponse.Lat as Double, mTrendingDataResponse.Long as Double))).toString() + "Km"
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            /* if (mTrendingDataResponse.is_favourite == 1) {
                 iv_three_dot.setImageResource(R.drawable.ic_favorite)
             } else {
                 iv_three_dot.setImageResource(R.drawable.iv_unlike)
             }*/

            if (mTrendingDataResponse.profile_image!!.isEmpty()) {
                iv_user.setImageResource(R.drawable.user_placeholder)
            } else {

                Picasso.get().load(mTrendingDataResponse.profile_image).into(iv_user)
            }
/*
            Glide.with(itemView.context)
                    .load(Utils.getUrl(itemView.context, mTrendingDataResponse.profile_image!!))
                    .centerCrop()
                    .dontAnimate()
                    .placeholder(R.drawable.user_placeholder)
                    .into(iv_user)
*/


            if (mTrendingDataResponse.profile_image.isEmpty()) {
                // context.toast("Profile image is not available")

            } else {
                iv_user.onClick {
                    val intent = FullImageActivity.newMainIntent(context)
                    intent!!.putExtra("profile_image", mTrendingDataResponse.profile_image)
                    ActivityStack.getInstance(context)
                    context.startActivity(intent)

                }
            }


            rl_toplayout.onClick {
                if (Utils.haveNetworkConnection(context)) {
                    val intent = Intent(context, TrendingActivity::class.java)
                    //intent.putExtra("mTrendingData",mTrendingDataResponse)
                    context.startActivity(intent)

                } else {
                    context.toast(Utils.getText(context, StringConstant.str_check_internet))
                }

            }

            rl_maincontent.onClick {
                iAdapterClickListener.cellItemClick("trending", "Cell Item click", mTrendingDataResponse, "", rl_maincontent, position)
            }

            iv_share.onClick {
                iAdapterClickListener.cellItemClick("trending", "shareClick", mTrendingDataResponse, "", iv_share, p1)
            }

            iv_three_dot.onClick {
                if (mTrendingDataResponse.is_favourite == 1) {
                    iAdapterClickListener.cellItemClick("trending", "favorite", mTrendingDataResponse, "0", iv_three_dot, p1)
                } else {
                    iAdapterClickListener.cellItemClick("trending", "favorite", mTrendingDataResponse, "1", iv_three_dot, p1)
                }
            }
        }

/*
        fun ImageView.loadUrl(url: String) {
            if (url != "") {
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, url!!))
                        .centerCrop()
                        .placeholder(R.drawable.queuelist_place_holder)
                        .error(R.drawable.queuelist_place_holder)
                        .into(mImageView)
            }
        }
*/
    }

    class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mImageView = view.iv_product_service!!
        val title = view.title!!
        val tvName = view.tvName!!
        val tv_reviews = view.tv_reviews!!
        val iv_user = view.iv_user!!
        val tv_services_product = view.tv_services_product!!
        val tv_description = view.tv_description!!
        val rl_maincontent = view.rl_maincontent!!
        val iv_three_dot = view.iv_three_dot!!
        val tv_tetoota = view.tv_tetoota!!
        val iv_share = view.iv_share!!
        val tv_distance = view.tv_distance
        val rl_distance = view.rl_distance
        val rl_toplayout = view.rl_toplayout!!
        val rtbProductRating = view.rtbProductRating!!
        val tv_see_all = view.tv_see_all!!
        val tv_user_count = view.tv_user_count!!
        private var tetootaApplication: TetootaApplication? = null
        fun bindProductViewHolder(viewHolder: ProductViewHolder?, p1: Int, mServicesDataResponse: ServicesDataResponse,
                                  height: Int, iAdapterClickListener: IAdapterClickListener, context: Context) {
            tetootaApplication = context.getApplicationContext() as TetootaApplication
            // tv_services_product.text = "Products"
            tv_services_product.text = Utils.getText(context, StringConstant.product)
            tv_see_all.text = Utils.getText(context, StringConstant.str_tv_see_all)

            title.text = mServicesDataResponse.title

            if (mServicesDataResponse.user_first_name != null && mServicesDataResponse.user_last_name != null)
                tvName.text = mServicesDataResponse.user_first_name + " " + mServicesDataResponse.user_last_name
            else {
                if (mServicesDataResponse.user_first_name != null)
                    tvName.text = mServicesDataResponse.user_first_name
                else if (mServicesDataResponse.user_last_name != null)
                    tvName.text = mServicesDataResponse.user_last_name
            }

            iv_three_dot.visibility = View.GONE
            tv_tetoota.text = mServicesDataResponse.tetoota_points.toString()
            // tv_reviews.text = "${mServicesDataResponse.reviews} " + Utils.getText(context, StringConstant.home_rating_review)
            tv_reviews.text = (mServicesDataResponse.reviews?.toFloat().toString())
            tv_user_count.text = ("(" + mServicesDataResponse.total_usercount + " " + Utils.getText(context, StringConstant.home_rating_review) + ")")

            tv_description.text = mServicesDataResponse.content
            if (mServicesDataResponse.reviews != null) {
                rtbProductRating.rating = (mServicesDataResponse.reviews.toFloat())
//                mImageView.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height)
                //  var url: String = "";
/*
                if (mServicesDataResponse!!.image != null && mServicesDataResponse!!.image_thumb != null) {
                    url = Utils.getUrl(itemView.context, mServicesDataResponse!!.image!!, mServicesDataResponse!!.image_thumb!!, false)
                } else {
                    url = Utils.getUrl(itemView.context, mServicesDataResponse!!.image!!)
                }
*/
                //  mImageView.loadUrl(url)

                if (mServicesDataResponse.image!!.isEmpty()) {
                    mImageView.setImageResource(R.drawable.queuelist_place_holder)
                } else {

                    Picasso.get().load(mServicesDataResponse.image!!).into(mImageView)
                }



                if (mServicesDataResponse.profile_image!!.isEmpty()) {
                    iv_user.setImageResource(R.drawable.user_placeholder)
                } else {

                    Picasso.get().load(mServicesDataResponse.profile_image).into(iv_user)
                }

/*
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, mServicesDataResponse.profile_image!!))
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.user_placeholder)
                        .into(iv_user)
*/
            }
            /* if(mServicesDataResponse.is_favourite == 1){R.
                 iv_like.setImageResource(R.drawable.ic_favorite)
             }else{
                 iv_like.setImageResource(R.drawable.iv_unlike)
             }*/
            // var lat = Utils.loadPrefrence(Constant.USER_LOC_LAT, "0.0", itemView.context)
            //  var long = Utils.loadPrefrence(Constant.USER_LOC_LONG, "0.0", itemView.context)

            if (tetootaApplication!!.myLatitude == 0.0 && tetootaApplication!!.myLongitude == 0.0) {
                rl_distance.visibility == View.GONE
            } else {
                rl_distance.visibility == View.VISIBLE

                try {
                    tv_distance.text = String.format("%.2f", Utils.checkDistance(LatLng(tetootaApplication!!.myLatitude.toDouble(), tetootaApplication!!.myLongitude.toDouble()),
                            LatLng(mServicesDataResponse.Lat as Double, mServicesDataResponse.Long as Double))) + "Km"
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            if (mServicesDataResponse.profile_image!!.isEmpty()) {
                // context.toast("Profile image is not available")

            } else {
                iv_user.onClick {
                    val intent = FullImageActivity.newMainIntent(context)
                    intent!!.putExtra("profile_image", mServicesDataResponse.profile_image)
                    ActivityStack.getInstance(context)
                    context.startActivity(intent)

                }
            }

/*
            iv_user.onClick {
                val intent = FullImageActivity.newMainIntent(context)
                intent!!.putExtra("profile_image", mServicesDataResponse.profile_image)
                ActivityStack.getInstance(context)
                context.startActivity(intent)

            }
*/

            rl_maincontent.onClick {
                println("Cell Item click")
                iAdapterClickListener.cellItemClick("product", "Cell Item click", mServicesDataResponse, "", rl_maincontent, p1)
            }

            iv_share.onClick {
                iAdapterClickListener.cellItemClick("product", "shareClick", mServicesDataResponse, "", iv_share, p1)
            }
            rl_toplayout.onClick {
                iAdapterClickListener.cellItemClick("product", "seeAll", mServicesDataResponse, "", rl_maincontent, p1)
            }

            iv_three_dot.onClick {
                if (mServicesDataResponse.is_favourite == 1) {
                    iAdapterClickListener.cellItemClick("product", "favorite", mServicesDataResponse, "0", iv_three_dot, p1)
                } else {
                    iAdapterClickListener.cellItemClick("product", "favorite", mServicesDataResponse, "1", iv_three_dot, p1)
                }
            }

        }

/*
        fun ImageView.loadUrl(url: String) {
            if (url != "") {
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, url!!))
                        .centerCrop()
                        .placeholder(R.drawable.queuelist_place_holder)
                        .error(R.drawable.queuelist_place_holder)
                        .into(mImageView)
            }
        }
*/
    }

    class ServiceViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mImageView = view.iv_product_service!!
        //  val tv_time_date = view.tv_time_date!!
        val iv_user = view.iv_user!!
        val title = view.title!!
        val tv_services_product = view.tv_services_product!!
        val tv_description = view.tv_description!!
        val iv_three_dot = view.iv_three_dot!!
        val tv_tetoota = view.tv_tetoota!!
        val iv_share = view.iv_share!!
        val tv_distance = view.tv_distance
        val rl_distance = view.rl_distance
        val tv_reviews = view.tv_reviews!!
        val rtbProductRating = view.rtbProductRating!!
        val rl_maincontent = view.rl_maincontent!!
        val rl_toplayout = view.rl_toplayout!!
        val tv_see_all = view.tv_see_all!!
        val tvName = view.tvName!!
        val tv_user_count = view.tv_user_count!!

        private var tetootaApplication: TetootaApplication? = null
        fun bindServiceData(viewHolder: ServiceViewHolder?, p1: Int, mServicesDataResponse: ServicesDataResponse,
                            height: Int, iAdapterClickListener: IAdapterClickListener, context: Context) {
            tetootaApplication = context.getApplicationContext() as TetootaApplication

            // Log.e("myLatitude",""+ tetootaApplication!!.myLatitude)
            // Log.e("myLongitude",""+ tetootaApplication!!.myLongitude)
            tv_services_product.text = Utils.getText(context, StringConstant.services)
            tv_see_all.text = Utils.getText(context, StringConstant.str_tv_see_all)

            // tv_services_product.text = "Services"
            var lat = Utils.loadPrefrence(Constant.USER_LOC_LAT, "0.0", itemView.context)
            var long = Utils.loadPrefrence(Constant.USER_LOC_LONG, "0.0", itemView.context)
            title.text = mServicesDataResponse.title
            if (mServicesDataResponse.user_first_name != null && mServicesDataResponse.user_last_name != null)
                tvName.text = mServicesDataResponse.user_first_name + " " + mServicesDataResponse.user_last_name
            else {
                if (mServicesDataResponse.user_first_name != null)
                    tvName.text = mServicesDataResponse.user_first_name
                else if (mServicesDataResponse.user_last_name != null)
                    tvName.text = mServicesDataResponse.user_last_name
            }
            iv_three_dot.visibility = View.GONE
            tv_tetoota.text = mServicesDataResponse.tetoota_points.toString()
            //  tv_reviews.text = "${mServicesDataResponse.reviews} " + Utils.getText(context, StringConstant.home_rating_review)
            tv_reviews.text = (mServicesDataResponse.reviews?.toFloat().toString())
            tv_user_count.text = ("(" + mServicesDataResponse.total_usercount + " " + Utils.getText(context, StringConstant.home_rating_review) + ")")

            tv_description.text = mServicesDataResponse.content
            if (mServicesDataResponse.reviews != null) {
                tv_reviews.text = mServicesDataResponse.reviews
                rtbProductRating.rating = (mServicesDataResponse.reviews.toFloat())
                //   tv_distance.text =
//                mImageView.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height)
                /*  var url: String = "";
                  if (mServicesDataResponse!!.image != null && mServicesDataResponse!!.image_thumb != null) {
                      url = Utils.getUrl(itemView.context, mServicesDataResponse!!.image!!, mServicesDataResponse!!.image_thumb!!, false)
                  } else {
                      url = Utils.getUrl(itemView.context, mServicesDataResponse!!.image!!)
                  }
                  mImageView.loadUrl(url)*/

                if (mServicesDataResponse.image!!.isEmpty()) {
                    mImageView.setImageResource(R.drawable.queuelist_place_holder);
                } else {

                    Picasso.get().load(mServicesDataResponse.image!!).into(mImageView)
                }


                if (mServicesDataResponse.profile_image!!.isEmpty()) {
                    iv_user.setImageResource(R.drawable.user_placeholder);
                } else {

                    Picasso.get().load(mServicesDataResponse.profile_image).into(iv_user)
                }

/*
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, mServicesDataResponse.profile_image!!))
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.user_placeholder)
                        .into(iv_user)
*/
            }


            if (tetootaApplication!!.myLatitude == 0.0 && tetootaApplication!!.myLongitude == 0.0) {
                rl_distance.visibility == View.GONE
            } else {
                rl_distance.visibility == View.VISIBLE

                try {

                    tv_distance.text = String.format("%.2f", Utils.checkDistance(LatLng(tetootaApplication!!.myLatitude.toDouble(), tetootaApplication!!.myLongitude.toDouble()),
                            LatLng(mServicesDataResponse.Lat as Double, mServicesDataResponse.Long as Double))) + "Km"

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            if (mServicesDataResponse.profile_image!!.isEmpty()) {
                // context.toast("Profile image is not available")

            } else {
                iv_user.onClick {
                    val intent = FullImageActivity.newMainIntent(context)
                    intent!!.putExtra("profile_image", mServicesDataResponse.profile_image)
                    ActivityStack.getInstance(context)
                    context.startActivity(intent)

                }
            }


/*
            iv_user.onClick {
                val intent = FullImageActivity.newMainIntent(context)
                intent!!.putExtra("profile_image", mServicesDataResponse.profile_image)
                ActivityStack.getInstance(context)
                context.startActivity(intent)

            }
*/

            rl_maincontent.onClick {
                iAdapterClickListener.cellItemClick("service", "Cell Item click", mServicesDataResponse, "", rl_maincontent, p1)
            }
            rl_toplayout.onClick {
                iAdapterClickListener.cellItemClick("service", "seeAll", mServicesDataResponse, "", rl_maincontent, p1)
            }

            iv_share.onClick {
                iAdapterClickListener.cellItemClick("service", "shareClick", mServicesDataResponse, "", mImageView, p1)
            }

            iv_three_dot.onClick {
                if (mServicesDataResponse.is_favourite == 1) {
                    iAdapterClickListener.cellItemClick("service", "favorite", mServicesDataResponse, "0", iv_three_dot, p1)
                } else {
                    iAdapterClickListener.cellItemClick("service", "favorite", mServicesDataResponse, "1", iv_three_dot, p1)
                }
            }

        }

/*
        fun ImageView.loadUrl(url: String) {
            if (url != "") {
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, url!!))
                        .centerCrop()
                        .placeholder(R.drawable.queuelist_place_holder)
                        .error(R.drawable.queuelist_place_holder)
                        .into(mImageView)
            }
        }
*/
    }

    class FooterViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_chat_with_us = view.tv_chat_with_us
        val ll_top = view.ll_top
        val tv_top = view.tv_top
        val tv_description = view.tv_desc!!
        var i: Int = 0

        fun bindFooterData(viewHolder: FooterViewHolder?, iAdapterClickListener: IAdapterClickListener
                           , mRecentActivityResp: List<DataItem>, context: Context) {
//            val handler = Handler()

            tv_top.text = Utils.getText(context, StringConstant.str_tv_top)

            val animationOut = AnimationUtils.loadAnimation(itemView.context, R.anim.fadeout)
            tv_description.startAnimation(animationOut)

            val animationIn = AnimationUtils.loadAnimation(itemView.context, R.anim.fadein)
            tv_description.startAnimation(animationIn)

            animationOut.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {
                }

                override fun onAnimationEnd(animation: Animation?) {
                    tv_description.startAnimation(animationIn)
                }

                override fun onAnimationStart(animation: Animation?) {
                    if (i < mRecentActivityResp.size) {
                        tv_description.text = mRecentActivityResp[i].message
                    }
                    if (i < 9) {
                        i++
                    } else {
                        i = 0
                    }
                }
            })

            animationIn.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {
                }

                override fun onAnimationEnd(animation: Animation?) {
                    tv_description.startAnimation(animationOut)
                }

                override fun onAnimationStart(animation: Animation?) {
                    if (i < mRecentActivityResp.size) {
                        tv_description.text = mRecentActivityResp[i].message
                    }
                }
            })

            /*handler.postDelayed(object : Runnable {
               override fun run() {
                   //Do something after 100ms
                   val animationIn = AnimationUtils.loadAnimation(itemView.context, R.anim.fadeout)
                   tv_description.startAnimation(animationIn)

//                    val animationOut = AnimationUtils.loadAnimation(itemView.context, R.anim.fadein)
//                    tv_description.startAnimation(animationOut)
                   if(i<9)
                   {
                       i++
                   }
                   else{
                       i=0
                   }
                   tv_description.text = mRecentActivityResp[i].message
                   Log.i("value of I handler", i.toString());
                   handler.postDelayed(this, 4000)
               }
           }, 0)

           handlerFadout.postDelayed(object : Runnable {
               override fun run() {
                   //Do something after 100ms
                   val animationOut = AnimationUtils.loadAnimation(itemView.context, R.anim.fadein)
                   tv_description.startAnimation(animationOut)
                   if(i<9)
                   {
                       i++
                   }
                   else{
                       i=0
                   }
//                    val animationIn = AnimationUtils.loadAnimation(itemView.context, R.anim.fadeout)
//                    tv_description.startAnimation(animationIn)
                   tv_description.text = mRecentActivityResp[i].message
                   Log.i("value of handlerFadout", i.toString());
                   handlerFadout.postDelayed(this, 4000)
               }
           }, 2000)*/

            Log.i("value of I", i.toString())

//            tv_description.text = Message
            tv_chat_with_us.setOnClickListener(View.OnClickListener {
                val intent = HelpActivity.newMainIntent(itemView.context)
                itemView.context.startActivity(intent)
            })

            ll_top.setOnClickListener(View.OnClickListener {
                iAdapterClickListener.cellItemClick("top", "top", "", "", ll_top, 2)
            })
        }

    }


    interface IAdapterClickListener {
        fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int): Unit
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        Log.i("remove call back", "remove handler here")
//        handler.removeCallbacks(null)
//        handlerFadout.removeCallbacks(null)
    }
}


