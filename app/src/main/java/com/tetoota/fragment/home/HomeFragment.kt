package com.tetoota.fragment.home

import android.Manifest
import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.*
import android.view.animation.AlphaAnimation
import android.widget.*
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.appintro.DashboardSliderAdapter
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.customviews.StartConversationDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.DashboardFragment
import com.tetoota.fragment.dashboard.*
import com.tetoota.fragment.dashboardnew.HomeFragmentAdapter
import com.tetoota.fragment.home.data.ContactUploadRequest
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.floating_add_filter_layout.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.view_pager_row.*

import org.jetbrains.anko.onClick

import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HomeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : BaseFragment(), HomeContract.View, ViewPager.OnPageChangeListener, HomeFragmentAdapter.IAdapterClickListener,
        StartConversationDialog.IDialogListener, ProfileCompletionDialog.IDialogListener {

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = ProfileDetailActivity.newMainIntent(this.activity!!)
                ActivityStack.getInstance(this.activity!!)
                startActivity(intent)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    var isRefresh: Boolean? = false
    var mQuery: String? = null
    var currentPage = 1
    var isListLastPage = false
    var exchangePostType: String? = ""

    private val EDITSERVICEPRODUCT_REQUEST: Int = 13
    private var intent: Intent? = null
    private var tetootaApplication: TetootaApplication? = null
    private val ADDPOST_REQUEST: Int = 14
    var isTest: Boolean? = false


    private lateinit var mHomeFragmentAdapter: HomeFragmentAdapter
    private var mDataList: List<DataItem> = emptyList()


    val mAdapter: DashboardSliderAdapter by lazy {
        DashboardSliderAdapter(this!!.activity!!)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@HomeFragment)
    }
    // TODO: Rename and change types of parameters
    private var dotsCount: Int = 0
    private var mParentListener: OnChildFragmentInteractionListener? = null
    //    private var mFilterListener: OnFilterAppliedListener? = null
    private var menu: Menu? = null

    var dots = arrayOfNulls<ImageView>(dotsCount)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear()
        super.onPrepareOptionsMenu(menu)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
//        getAllRecentActivityList()
        // Inflate the layout for this fragment

        tetootaApplication = activity!!.applicationContext as TetootaApplication
        TetootaApplication.setTextValuesHashMap(Utils.getLangByCode(activity,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", activity)))

        var view = inflater.inflate(R.layout.fragment_home, container, false)


        return view
    }

    /****************get count************************/

    fun getCountData(): Unit {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this.activity!!)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mHomePresenter.getIncompleteProposalData(this.activity!!, personData.user_id.toString(), tetootaApplication!!.userMacAddress)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
        }
    }

    /***************************************/
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pager_introduction.adapter = mAdapter
        pager_introduction.currentItem = 0
        pager_introduction.addOnPageChangeListener(this@HomeFragment)

        view_list.layoutManager = LinearLayoutManager(context)
        view_list.setHasFixedSize(true)
        mHomeFragmentAdapter = HomeFragmentAdapter(activity!!, iAdapterClickListener = this,
                screenHeight = heightCalculation())
        view_list.isNestedScrollingEnabled = false
//        val tv = this.findViewById(R.id.tv_marqueeText) as TextView
//        tv_marqueeText.text = Utils.getText(context,StringConstant.email_us)

//        tv_marqueeText.isSelected = true
//        tv_marqueeText.setSelectAllOnFocus(true)
//        tv_marqueeText.maxLines = 1
        //  buildGoogleApiClient()
        //  getLocationLatLng()

        getCountData()
        getAllDashboardList()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Body of your click handler
        getAllRecentActivityList()
        setMultiLanguageText()
        setAlpha()
        /*  var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
            toolTitle.visibility = View.VISIBLE
            toolTitle.text = "Tetoota"
    */

//        ll_addpost.onClick {
        //val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
//            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//            if (personData.complete_percentage!!.toInt() < 50) {
//                ProfileCompletionDialog(activity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
//                        this@HomeFragment, getString(R.string.err_msg_mobile_number_limit)).show()
//            } else {
//                val intent = AddPostRequestActivity.newMainIntent(this!!.activity!!)
//                intent!!.putExtra("Tab", "serviceTab")
//                ActivityStack.getInstance(this!!.activity!!)
////                startActivity(intent)
//                activity?.startActivityForResult(intent, ADDPOST_REQUEST)
//            }
//        }
    }

    private fun setAlpha() {
        val alpha = AlphaAnimation(0.5f, 0.5f) // change values as you want
        alpha.setDuration(0) // Make animation instant
        alpha.setFillAfter(true) // Tell it to persist after the animation ends
        filter.startAnimation(alpha)
        iv_filter.startAnimation(alpha)
        tv_add.startAnimation(alpha)
        iv_add.startAnimation(alpha)
    }

    private fun appVersionCheck() {
        var requestUserId = Utils.loadPrefrence(Constant.VERSION, "", context)
        if (requestUserId.equals(true)) {

        }
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .3f).toInt()
        return getTopMarginH
    }

    override fun onStop() {
        super.onStop()
        if (HomeFragment().isVisible) {
            pager_introduction.stopAutoScroll()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (HomeFragment().isVisible) {
            pager_introduction.stopAutoScroll()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        println("On Destroy View")
        if (!DashboardFragment().isVisible) {
            pager_introduction.stopAutoScroll()
        }
    }

    private fun setUiPageViewController() {

        if (isTest == true) {

        } else {
            //Log.e("elseee ","" + isTest)
            dots = arrayOfNulls(dotsCount)
            for (i in 0..dotsCount - 1) {
                dots[i] = ImageView(activity)
                dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
                val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(4, 0, 4, 0)
                viewPagerCountDots.addView(dots[i], params)
            } // Commit Testing
            dots[0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
        }

    }

    private fun setMultiLanguageText() {
        tv_add.text = Utils.getText(context, StringConstant.str_addpost)
        filter.text = Utils.getText(context, StringConstant.str_filter)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(p0: Int) {
        for (i in 0..dotsCount - 1) {
            try {
                dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
                dots[p0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
                /*if (p0 + 1 == dotsCount) {
                } else {
                    // btn_next.visibility = View.VISIBLE
                }*/
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    override fun cellItemClick(mViewType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        if (mViewType == "top") {
            Log.i("ONTOP", "ONTOP")
//            nv_view.fullScroll(View.FOCUS_UP)
//            //             view_list.smoothScrollToPosition(0)

//            nv_view.scrollTo(0, 0)
        } else if (mViewType == "trending") {
            val mProductData = cellRow as ServicesDataResponse
            if (mString == "favorite") {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    mHomePresenter.markFavorite(this.activity!!, mProductData, mAttributeValue, p1)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            } else if (mString == "cellClick") {
                activity!!.toast("Under Development")
            } else if (mString == "shareClick") {
                showPopupMenu(mView, mProductData, mAttributeValue, p1)
            } else if (mString.equals("Cell Item click")) {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = Intent(context, ProductDetailActivity::class.java)
                    intent.putExtra("mProductData", mProductData)
                    intent.putExtra("productType", "services")
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        val transitionActivityOptions: ActivityOptions =
                                ActivityOptions.makeSceneTransitionAnimation(activity,
                                        mView, "iv_image")
                        //  startActivity(intent, transitionActivityOptions.toBundle())
                        startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST, transitionActivityOptions.toBundle())

                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    } else {
                        //  startActivity(intent)
                        startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST)

                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    }
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
        } else if (mViewType == "service") {
            val mProductData = cellRow as ServicesDataResponse
            if (mString == "favorite") {
                showPopupMenu(mView, mProductData, mAttributeValue, p1)
            } else if (mString == "cellClick") {
                activity!!.toast("Under Development")
            } else if (mString == "shareClick") {
                var url: String = ""
                if (mProductData.image != null && mProductData.image_thumb != null) {
                    url = Utils.getUrl(context, mProductData.image, mProductData.image_thumb, false)
                } else {
                    url = Utils.getUrl(context, mProductData.image!!)
                }
                shareProduct(mProductData.title!!, url)
            } else if (mString == "seeAll") {
                mParentListener?.messageFromChildToParent("service")

            } else if (mString.equals("Cell Item click")) {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = Intent(context, ProductDetailActivity::class.java)
                    intent.putExtra("mProductData", mProductData)
                    intent.putExtra("productType", "services")
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        val transitionActivityOptions: ActivityOptions =
                                ActivityOptions.makeSceneTransitionAnimation(activity,
                                        mView, "iv_image")
                        //  startActivity(intent, transitionActivityOptions.toBundle())
                        startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST, transitionActivityOptions.toBundle())

                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    } else {
                        //  startActivity(intent)
                        startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST)

                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    }
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
        } else if (mViewType == "product") {
            val mProductData = cellRow as ServicesDataResponse
            if (mString == "favorite") {
                showPopupMenu(mView, mProductData, mAttributeValue, p1)
            } else if (mString == "cellClick") {
                activity!!.toast("Under Development")
            } else if (mString == "shareClick") {
                var url: String = "";
                if (mProductData.image != null && mProductData.image_thumb != null) {
                    url = Utils.getUrl(context, mProductData.image, mProductData.image_thumb, false)
                } else {
                    url = Utils.getUrl(context, mProductData.image!!)
                }
                shareProduct(mProductData.title!!, url)
            } else if (mString == "seeAll") {
                mParentListener?.messageFromChildToParent("product")
            } else if (mString.equals("Cell Item click")) {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = Intent(context, ProductDetailActivity::class.java)
                    intent.putExtra("mProductData", mProductData)
                    intent.putExtra("productType", "product")
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        val transitionActivityOptions: ActivityOptions =
                                ActivityOptions.makeSceneTransitionAnimation(activity,
                                        mView, "iv_image")
                        // startActivity(intent, transitionActivityOptions.toBundle())
                        startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST, transitionActivityOptions.toBundle())

                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    } else {
                        // startActivity(intent)
                        startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST)

                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    }
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
        }
    }

    override fun onDashboardApiSuccessResult(mSliderList: ArrayList<Any>, message: String) {
        if (message == "sliderList") {
            dotsCount = 0
            progress_view.visibility = View.GONE
            rl_viewpager.visibility = View.VISIBLE
            val mDashBoardSliderDataResponse: ArrayList<Any> = mSliderList
            dotsCount = mSliderList.size
            setUiPageViewController()
            mAdapter.setElements(mDashBoardSliderDataResponse as ArrayList<DashboardSliderDataResponse>)
            mAdapter.notifyDataSetChanged()
            pager_introduction.interval = 2000
            pager_introduction.startAutoScroll()
            pager_introduction.currentItem = Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % mSliderList.size

        } else if (message == "homeScreenList") {
            println("Home Screen List PushNotificationDataResponse")
            val mHomeScreenList = mSliderList as ArrayList<HomeDataResponse>
        }
    }

    override fun onDashboardApiFailureResult(message: String) {
        if (activity != null) {
            progress_view.visibility = View.GONE
            activity?.toast(message)
        }
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (cellRow.is_favourite == 0) {
            cellRow.is_favourite = 1
            activity?.toast("Marked Favorite")
        } else {
            cellRow.is_favourite = 0
            activity?.toast("Marked Unfavorite")
        }
        Utils.savePreferences(Constant.VERSION, cellRow.version!!, this.context!!)
        mHomeFragmentAdapter.notifyItemChanged(p1)
    }

    /**
     *
     */

    fun getAllDashboardList(): Unit {
/*
        if (ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
*/
        if (Utils.haveNetworkConnection(this.activity!!)) {
            progress_view.visibility = View.VISIBLE
            Log.e("homeloc1", "" + tetootaApplication!!.myLatitude)
            Log.e("homeloc2", " " + tetootaApplication!!.myLongitude)
            mHomePresenter.getDashboardSliderData(this.activity!!, tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude)
        } else {
            activity?.toast(Utils.getText(context, StringConstant.str_check_internet))
            progress_view.visibility = View.GONE
        }
    }

    fun shareProduct(mTitle: String, mImageUrl: String) {
        val text = mTitle
        val pictureUri = Uri.parse(mImageUrl)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share images..."))
    }

    /**
     * Method To Create Popup Menu
     * for share, Favorite, Report
     */
    private fun showPopupMenu(view: View, mProductData: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val popup = PopupMenu(activity, view)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        if (mAttributeValue == "0") {
            favItem?.title = Utils.getText(context, StringConstant.home_unfavourite)
        } else {
            favItem?.title = Utils.getText(context, StringConstant.favorite)
        }
        menu?.findItem(R.id.menu_share)!!.title = Utils.getText(context, StringConstant.share_text)
        menu?.findItem(R.id.menu_report_this)!!.title = Utils.getText(context, StringConstant.report_this)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
                    var url: String = ""
                    if (mProductData.image != null && mProductData.image_thumb != null) {
                        url = Utils.getUrl(context, mProductData.image, mProductData.image_thumb, false)
                    } else {
                        url = Utils.getUrl(context, mProductData.image!!)
                    }
                    shareProduct(mProductData.title!!, url)
                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this.activity!!)) {
                        mHomePresenter.markFavorite(this.activity!!, mProductData, mAttributeValue, p1)
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    StartConversationDialog(context, Constant.DIALOG_LOGIN_FAILURE_ALERT, mProductData.id.toString(),
                            this@HomeFragment, getString(R.string.str_report_issue), intent!!.putExtra("productType", ""), exchangePostType!!).show()
                }
            }
            true
        }
        popup.show()
    }

    override fun onDashboardApiSuccessResult(mCategoriesList: ArrayList<Any>, arrayList: ArrayList<Any>, message: String) {
        println("DATA")
        if (view != null) {
            progress_view.visibility = View.GONE
            //       hideProgressDialog()
            mHomeFragmentAdapter = HomeFragmentAdapter(activity!!, iAdapterClickListener = this,
                    screenHeight = heightCalculation())
            view_list.adapter = mHomeFragmentAdapter
            rl_viewpager.visibility = View.VISIBLE
            val mDashBoardSliderDataResponse: ArrayList<Any> = arrayList
            dotsCount = arrayList.size
            setUiPageViewController()
            mAdapter.setElements(mDashBoardSliderDataResponse as ArrayList<DashboardSliderDataResponse>)
            mAdapter.notifyDataSetChanged()
            pager_introduction.interval = 4000
            pager_introduction.startAutoScroll()
            pager_introduction.currentItem = Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % arrayList.size

            mHomeFragmentAdapter.setElements(mCategoriesList, arrayList as ArrayList<DashboardSliderDataResponse>)
            if (mDataList.isNotEmpty()) {
                mHomeFragmentAdapter.setElementsRecent(mDataList)
                mHomeFragmentAdapter.notifyDataSetChanged()
            }

            val isContactUploaded = Utils.loadPrefrence(Constant.CONTACTS_UPLOADED, "en", this.activity!!)
            // getAllContacts()

            if (isContactUploaded == null || isContactUploaded.equals("")) {
                if (ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {
                    return
                }
                showProgressDialog("${Utils.getText(this!!.activity!!, StringConstant.please_wait)}")
                Thread(Runnable {
                    //  getAllContacts()
                }).start()
            }
        }
    }

    fun uploadContacts(contactUploadRequest: ContactUploadRequest) {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this.activity!!)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            var jsonString: String = ""
            mHomePresenter.uploadContacts(this.activity!!, personData.user_id!!, jsonString, contactUploadRequest)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            hideProgressDialog()
        }
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        //Toast.makeText(this.activity!!, "Contacts uploaded", Toast.LENGTH_SHORT).show()
        Utils.savePreferences(Constant.CONTACTS_UPLOADED, "yes", this.activity!!)
        hideProgressDialog()
    }

    override fun onContactUploadedFailure(message: String?) {
        hideProgressDialog()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        mParentListener = null
    }

    interface OnChildFragmentInteractionListener {
        fun messageFromChildToParent(mLastCommitFragment: String)
    }

    fun setmListener(mParentListener: OnChildFragmentInteractionListener) {
        this.mParentListener = mParentListener
    }

    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
        Utils.hideSoftKeyboard(this.activity!!)
        if (message == "report") {
            showProgressDialog(Utils.getText(context, StringConstant.please_wait))
            mHomePresenter.report(this.activity!!, mServiceId, mMesgDesc)
        }
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            activity?.toast(message)
        } else {
            activity?.toast(message)
        }
    }

    private fun getAllContacts() {

        val contactVOList = ArrayList<ContactDataPojo>()
        var contactVO: ContactDataPojo
        val contentResolver = context!!.getContentResolver()
        val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC")

        if (cursor!!.getCount() > 0) {
            while (cursor.moveToNext()) {

                val hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)))
                if (hasPhoneNumber > 0) {
                    val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                    val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))

                    contactVO = ContactDataPojo()
                    contactVO.setContactName(name)

                    val phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf<String>(id), null)
                    if (null != phoneCursor) {
                        if (phoneCursor.moveToNext()) {
                            val phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            contactVO.setContactNumber(phoneNumber)
                        }
                        phoneCursor.close()
                    }
                    val emailCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                            arrayOf<String>(id), null)
                    if (null != phoneCursor) {
                        while (emailCursor!!.moveToNext()) {
                            val emailId = emailCursor!!.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                        }
                    }
                    contactVOList.add(contactVO)


                }
            }
//            val contactAdapter = AllContactsAdapter(contactVOList, context)
            Log.e("ContactNumber ", "***** " + contactVOList.get(0).getContactNumber())
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            val contactjsonobject = JSONObject()
            contactjsonobject.put("user_id", personData.user_id)
            val array = JSONArray()
            for (contacts in contactVOList) {
                val contactObject = JSONObject()
                contactObject.put("name", contacts.getContactName())
                contactObject.put("number", contacts.getContactNumber())
                array.put(contactObject)
            }

            contactjsonobject.put("contacts", array)
            // Log.i("CCCCCCCCCCCCCCCCCCCCCC", contactjsonobject.toString())
            val contacts = ContactUploadRequest()
            contacts.userId = personData.user_id
            contacts.contacts = array
            //  uploadContacts(contacts)

            this.activity!!.runOnUiThread(java.lang.Runnable {
                uploadContacts(contacts)
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EDITSERVICEPRODUCT_REQUEST) {
                refreshData()
            }
        } else {
            refreshData()
        }


/*
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ADDPOST_REQUEST){
                if (data?.getStringExtra("ADDPOST_REQUEST").equals("SERVICE")){


                }
            }
        }
*/
    }
/*
    public fun refreshData(){
        currentPage = 1
        isListLastPage = false
        clearList()
        callWishListApi(false)
    }
*/

    fun getAllRecentActivityList(): Unit {
/*
        if (ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this!!.activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
*/
        if (Utils.haveNetworkConnection(this.activity!!)) {
            progress_view.visibility = View.VISIBLE
            mHomePresenter.getRecentActivityData(this.activity!!)
        } else {
            activity?.toast(Utils.getText(context, StringConstant.str_check_internet))
            progress_view.visibility = View.GONE
        }
    }

    /******************************recently activity response*********************/

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
        progress_view.visibility = View.GONE
        mHomeFragmentAdapter = HomeFragmentAdapter(activity!!, iAdapterClickListener = this,
                screenHeight = heightCalculation())
        view_list.adapter = mHomeFragmentAdapter
        rl_viewpager.visibility = View.VISIBLE
        this.mDataList = mDataList;
        mHomeFragmentAdapter.setElementsRecent(mDataList)
        mHomeFragmentAdapter.notifyDataSetChanged()

    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        progress_view.visibility = View.GONE
    }

    /******************************invalid text response*********************/

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {}

    override fun onInvalidTextAPiFailure(message: String?) {
    }


    fun clearList() {
        //  mHomeFragmentAdapter.removeAll()
        mHomeFragmentAdapter.notifyDataSetChanged()
    }

    fun refreshData() {
        currentPage = 1
        isListLastPage = false
        clearList()
        isTest = true

        Log.e("tag ", "refreshData ")
        getAllDashboardList()
    }

    /*******************notification count****************************/
    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        if (mProposalMesgData != null) {
            tetootaApplication?.count = mProposalMesgData.size
            // Log.e("iff##### count " ,""+ mProposalMesgData.size)

        } else {
            tetootaApplication!!.count = 0
            //  Log.e("elsee#####",""+ mProposalMesgData.size)
        }
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        // Log.e("######",""+ message)
        tetootaApplication!!.count = 0
    }
    /***********************************************/
} //Required empty public constructor
