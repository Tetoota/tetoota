package com.tetoota.fragment.home

/**
 * Created by jitendra.nandiya on 28-11-2017.
 */
class ContactDataPojo {
    private var ContactImage: String? = null
    private var ContactName: String? = null
    private var ContactNumber: String? = null

    fun getContactImage(): String? {
        return ContactImage
    }

    fun setContactImage(contactImage: String) {
        this.ContactImage = ContactImage
    }

    fun getContactName(): String? {
        return ContactName
    }

    fun setContactName(contactName: String) {
        ContactName = contactName
    }

    fun getContactNumber(): String? {
        return ContactNumber
    }

    fun setContactNumber(contactNumber: String) {
        ContactNumber = contactNumber
    }
}