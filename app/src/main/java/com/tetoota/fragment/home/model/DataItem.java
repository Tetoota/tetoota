package com.tetoota.fragment.home.model;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("message")
	private String message;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"message = '" + message + '\'' + 
			"}";
		}
}