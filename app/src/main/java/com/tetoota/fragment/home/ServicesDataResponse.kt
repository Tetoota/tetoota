package com.tetoota.fragment.dashboard

import android.os.Parcel
import android.os.Parcelable


data class ServicesDataResponse(val image: String? = null,
                                val image_thumb: String? = "",
                                val post_type: String? = "",
                                val address: String? = null,
                                val total_usercount: String? = null,
                                val post_address: String? = null,
                                val user_last_name: String? = null,
                                val describe_yourself: String? = null,
                                val city: String? = null,
                                val review_avg: String? = null,
                                val quality_service: String? = null,
                                val friendliness: String? = null,
                                val all_images: List<String?>? = null,
                                val all_images_thumb: List<String?>? = null,
                                val availability: String? = null,
                                val trading_preference: String? = null,
                                val creation_date: String? = null,
                                var is_favourite: Int? = null,
                                val title: String? = null,
                                val set_quote: String? = null,
                                val content: String? = null,
                                val tetoota_points: String? = null,
                                val qualification: String? = null,
                                val user_first_name: String? = null,
                                val profile_image: String? = null,
                                val reviews: String? = null,
                                val Long: Double? = null,
                                val id: Int? = null,
                                val user_id: Int? = null,
                                val category_ids: String? = null,
                                val category_name: String? = null,
                                val Lat: Double? = null,
                                val virtual_service: String? = null,
                                val total_record: Int? = null,
                                val sub_category_ids: String? = null,
                                val sub_category_name: String? = null,
                                val version: String? = null,
                                val whatsapp: String? = null,
                                val facebook: String? = null,
                                val gmail: String? = null,
                                val linkedin: String? = null,
                                val instagram: String? = null,
                                val allowed_services: String? = null
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            ArrayList<String?>().apply { source.readList(this as List<*>, String::class.java.classLoader) },
            ArrayList<String?>().apply { source.readList(this as List<*>, String::class.java.classLoader) },
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readValue(Double::class.java.classLoader) as Double?,
            source.readString(),
            source.readValue(Int::class.java.classLoader) as Int?,
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )


    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(image)
        writeString(image_thumb)
        writeString(post_type)
        writeString(address)
        writeString(total_usercount)
        writeString(post_address)
        writeString(user_last_name)
        writeString(describe_yourself)
        writeString(city)
        writeString(review_avg)
        writeString(quality_service)
        writeString(friendliness)
        writeList(all_images)
        writeList(all_images_thumb)
        writeString(availability)
        writeString(trading_preference)
        writeString(creation_date)
        writeValue(is_favourite)
        writeString(title)
        writeString(set_quote)
        writeString(content)
        writeString(tetoota_points)
        writeString(qualification)
        writeString(user_first_name)
        writeString(profile_image)
        writeString(reviews)
        writeValue(Long)
        writeValue(id)
        writeValue(user_id)
        writeString(category_ids)
        writeString(category_name)
        writeValue(Lat)
        writeString(virtual_service)
        writeValue(total_record)
        writeString(sub_category_ids)
        writeString(sub_category_name)
        writeString(version)
        writeString(whatsapp)
        writeString(facebook)
        writeString(gmail)
        writeString(linkedin)
        writeString(instagram)
        writeString(allowed_services)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ServicesDataResponse> = object : Parcelable.Creator<ServicesDataResponse> {
            override fun createFromParcel(source: Parcel): ServicesDataResponse = ServicesDataResponse(source)
            override fun newArray(size: Int): Array<ServicesDataResponse?> = arrayOfNulls(size)
        }
    }
}
