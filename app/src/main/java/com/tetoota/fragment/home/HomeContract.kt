package com.tetoota.fragment.dashboard

import android.app.Activity
import com.tetoota.fragment.home.data.ContactUploadRequest
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData

/**
 * Created by charchit.kasliwal on 05-06-2017.
 */
class HomeContract {

    interface View {
        fun onDashboardApiSuccessResult(mCategoriesList: ArrayList<Any>, message: String) {}
        fun onDashboardApiSuccessResult(mCategoriesList: ArrayList<Any>, arrayList: ArrayList<Any>, message: String) {}
        fun onDashboardApiFailureResult(message: String) {}
        fun ongetIncompleteProposalSuccess(mProposalMesgData : ArrayList<ProposalMessageData>, message: String)
        fun ongetIncompleteProposalFailure(message: String)
        fun ongetTrendingSuccess(mServiceData : ArrayList<ServicesDataResponse>, message: String)
        fun ongetTrendingFailure(message: String)
        fun onReportApiSuccess(key: String, message: String) {}
        fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {}
        fun onContactUploadedSuccess(contactUploadResponse : ContactUploadResponse?)
        fun onContactUploadedFailure(message: String?)
        fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>)
        fun onRecentactivityAPiFailureResult(message: String?)
        fun onInvalidTextAPiSuccess(mDataList: List<DataItem>)
        fun onInvalidTextAPiFailure(message: String?)
    }

    internal interface Presenter {
        fun getDashboardData(mActivity: Activity)
        fun getData(mActivity: Activity, myLatitude: Double, myLongitude: Double)
        fun getDashboardSliderData(mActivity: Activity, myLatitude: Double, myLongitude: Double)
        fun report(mActivity: Activity, mServiceId: String, mMesgDesc: String)
        fun markFavorite(mActivity: Activity, mProductId: ServicesDataResponse, mAttributeValue: String, p1: Int)
        fun uploadContacts(mActivity: Activity, user_id : String,contactJson : String, contactUploadRequest: ContactUploadRequest)
        fun getRecentActivityData(mActivity: Activity)
        fun getIncompleteProposalData(mActivity : Activity,userId : String,userMacAddress : String)
        fun getTrendingList(mActivity : Activity,userId : String)
        fun getInvalidText(mActivity: Activity)

    }

    interface dashboardApIListener {
        fun onDashboardAPiSuccess(mDataList: ArrayList<Any>, arrayList: ArrayList<Any>, message: String)
        fun onDashboardAPiSuccess(mDataList: ArrayList<Any>, message: String)
        fun onDashboardApiFailure(message: String)
        fun onReportApiSuccess(key: String, message: String) {}
        fun onFavoriteApiSuccess(message: String, cellRow: ServicesDataResponse, p1: Int)
        fun onContactUploadedSuccess(contactUploadResponse : ContactUploadResponse?)
        fun onContactUploadedFailure(message: String?)
        fun onRecentactivityAPiSuccess(message: List<DataItem>)
        fun onRecentactivityAPiFailure(message: String?)
        fun onIncompleteApiSuccess(mProposalMesgData : List<ProposalMessageData>,message: String)
        fun onTrendingApiSuccess(mServiceData : ArrayList<ServicesDataResponse>, message: String)
        fun onTrendingApiFailure(message: String)
        fun onIncompleteApiFailure(message: String)
        fun onInvalidTextAPiSuccess(message: List<DataItem>)
        fun onInvalidTextAPiFailure(message: String?)
    }
}
