package com.tetoota.fragment.dashboard

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.home.data.ContactUploadRequest
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.RecentactivityResponse
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.inbox.pojo.ProposalApiResponse
import com.tetoota.login.LoginDataResponse
import com.tetoota.service_product.UserServiceResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 05-06-2017.
 */
class HomeInteractor {
    val sliderList: String? = "sliderList"
    var mDashboardApiListener: HomeContract.dashboardApIListener

    constructor(mCategoriesApiListener: HomeContract.dashboardApIListener) {
        this.mDashboardApiListener = mCategoriesApiListener
    }

    /**
     * Method to GEt Call API From Dashboard Slide DATA
     */
    fun getDashboardSlideData(mActivity: Activity, myLatitude: Double, myLongitude: Double) {
        var call: Call<DashSliderResponse> = TetootaApplication.getHeader()
                .dashBoardSlider(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity))
        call.enqueue(object : Callback<DashSliderResponse> {
            override fun onResponse(call: Call<DashSliderResponse>?,
                                    response: Response<DashSliderResponse>?) {
                var mDashboardSliderData: DashSliderResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                      //  getListData(mActivity, mDashboardSliderData?.data as ArrayList<Any>, myLatitude, myLongitude)
                        mDashboardApiListener.onDashboardAPiSuccess(mDashboardSliderData?.data as ArrayList<Any>, "sliderList")
                    } else {
                        mDashboardApiListener.onDashboardApiFailure(response.body()?.meta?.message.toString())
                    }
                } else {
                    mDashboardApiListener.onDashboardApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<DashSliderResponse>?, t: Throwable?) {
                mDashboardApiListener.onDashboardApiFailure(t?.message.toString())
            }
        })
    }

    /**
     *
     */
    fun report(mActivity: Activity, service_id: String, report_desc: String) {
        val call: Call<FavoriteResponse> = TetootaApplication.getHeader()
                .reportServiceApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        Utils.loadPrefrence(Constant.USER_ID, "", mActivity), service_id, report_desc)
        call.enqueue(object : Callback<FavoriteResponse> {
            override fun onFailure(call: Call<FavoriteResponse>?, t: Throwable?) {
                mDashboardApiListener.onReportApiSuccess("fail", t?.message.toString())
            }

            override fun onResponse(call: Call<FavoriteResponse>?,
                                    response: Response<FavoriteResponse>?) {
                var mHomeResponse: FavoriteResponse? = response?.body()
                if (response?.code() == 200) {
                    mDashboardApiListener.onReportApiSuccess("success", mHomeResponse?.meta?.message.toString())

                } else {
                    mDashboardApiListener.onReportApiSuccess("fail", mHomeResponse?.meta?.message.toString())
                }
            }

        })
    }

    /**
     * Method to Get Dashboard PushNotificationDataResponse of Home Tab
     */
/*
    fun getListData(mActivity: Activity, arrayList: ArrayList<Any>){
        val call: Call<HomeResponse> = TetootaApplication.getHeader()
                .dashBoardHomeTabApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),
                        Utils.loadPrefrence(Constant.USER_ID,"",mActivity))

        call.enqueue(object : Callback<HomeResponse> {
            override fun onFailure(call: Call<HomeResponse>?, t: Throwable?) {
                mDashboardApiListener.onDashboardApiFailure(t?.message.toString())
            }
            override fun onResponse(call: Call<HomeResponse>?,
                                    response: Response<HomeResponse>?) {
                var mHomeResponse : HomeResponse? = response?.body()
                Log.e("+++mHomeResponse+++",""+ mHomeResponse)
                if(response?.code() == 200){
                    if(response?.body()?.data?.size!! > 0){
                        mDashboardApiListener.
                                onDashboardAPiSuccess(mHomeResponse?.data as ArrayList<Any>,arrayList, "homeScreenList")
                    }else{ mDashboardApiListener.
                            onDashboardApiFailure(response?.body()?.meta?.message.toString()) }
                }else{ mDashboardApiListener.
                        onDashboardApiFailure(response?.body()?.meta?.message.toString()) }
            }

        })
    }
*/
    fun getListData(mActivity: Activity, arrayList: ArrayList<Any>, myLatitude: Double, myLongitude: Double) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        val call: Call<HomeResponse> = TetootaApplication.getHeader()
                .dashBoardHomeTabApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        Utils.loadPrefrence(Constant.USER_ID, personData.user_id!!, mActivity), myLatitude.toString(), myLongitude.toString())
        call.enqueue(object : Callback<HomeResponse> {
            override fun onFailure(call: Call<HomeResponse>?, t: Throwable?) {
                mDashboardApiListener.onDashboardApiFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<HomeResponse>?,
                                    response: Response<HomeResponse>?) {
                Log.e("mlattitude", " kachori" + myLatitude)
                Log.e("mlattitude", " kachori" + myLongitude)
                var mHomeResponse: HomeResponse? = response?.body()
                // Log.e("+++mHomeResponse+++",""+ mHomeResponse)
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        mDashboardApiListener.onDashboardAPiSuccess(mHomeResponse?.data as ArrayList<Any>, arrayList, "homeScreenList")
                    } else {
                        mDashboardApiListener.onDashboardApiFailure(response.body()?.meta?.message.toString())
                    }
                } else {
                    mDashboardApiListener.onDashboardApiFailure(response?.body()?.meta?.message.toString())
                }
            }

        })
    }

    fun getIncompleteProposalApiCalling(mActivity: Activity, mUserId: String, userMacAddress: String) {
        var call = TetootaApplication.getHeader()
                .getIncompleteProposal(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId, userMacAddress)
        call!!.enqueue(object : Callback<ProposalApiResponse> {
            override fun onResponse(call: Call<ProposalApiResponse>?, response: Response<ProposalApiResponse>?) {
                println("Success")
                var mIncompleteProposal: ProposalApiResponse? = response?.body()
                //Log.e("@@@@@@",""+ mIncompleteProposal)

                if (response?.code() == 200) {
                    if (response.body()?.data != null) {
                        if (response.body()?.data?.size!! > 0) {
                            mDashboardApiListener.onIncompleteApiSuccess(mIncompleteProposal?.data as List<ProposalMessageData>, "success")
                        } else {
                            mDashboardApiListener.onIncompleteApiFailure("No Records Found")
                        }
                    }
                } else {
                    mDashboardApiListener.onDashboardApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<ProposalApiResponse>?, t: Throwable?) {
                mDashboardApiListener.onDashboardApiFailure(t?.message.toString())
            }
        })
    }

    fun markFavorites(mActivity: Activity, cellRow: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val call: Call<FavoriteResponse> = TetootaApplication.getHeader()
                .favoriteApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), cellRow.id.toString(),
                        Utils.loadPrefrence(Constant.USER_ID, "", mActivity), "POST_FAV", mAttributeValue)
        call.enqueue(object : Callback<FavoriteResponse> {
            override fun onFailure(call: Call<FavoriteResponse>?, t: Throwable?) {
                println("Failure API REsponse")
                mDashboardApiListener.onDashboardApiFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<FavoriteResponse>?,
                                    response: Response<FavoriteResponse>?) {
                println("Success API REsponse")
                var mFavoriteResponse: FavoriteResponse? = response?.body()
                if (response?.code() == 200) {
                    mDashboardApiListener.onFavoriteApiSuccess("favorite", cellRow, p1)
                } else {
                    mDashboardApiListener.onDashboardApiFailure(mFavoriteResponse?.meta?.message.toString())
                }
            }

        })

    }

    fun uploadContacts(mActivity: Activity, user_id: String, uploadJson: String, contactUploadRequest: ContactUploadRequest) {
        val call: Call<ContactUploadResponse> = TetootaApplication.getHeader()
                .postUserContacts(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity)
                        , contactUploadRequest)
        call.enqueue(object : Callback<ContactUploadResponse> {
            override fun onFailure(call: Call<ContactUploadResponse>?, t: Throwable?) {
                println("Failure API REsponse")
                mDashboardApiListener.onContactUploadedFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<ContactUploadResponse>?,
                                    response: Response<ContactUploadResponse>?) {
                println("Success API REsponse")
                var mContactUploadResponse: ContactUploadResponse? = response?.body()
                if (response?.code() == 200 && mContactUploadResponse?.meta?.status == true) {
                    mDashboardApiListener.onContactUploadedSuccess(mContactUploadResponse)
                } else {
                    mDashboardApiListener.onContactUploadedFailure(mContactUploadResponse?.meta?.message)
                }
            }

        })
    }

    fun getRecentActivity(mActivity: Activity) {
        val call: Call<RecentactivityResponse> = TetootaApplication.getHeader()
                .getRecentactivity(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity))
        call.enqueue(object : Callback<RecentactivityResponse> {
            override fun onFailure(call: Call<RecentactivityResponse>?, t: Throwable?) {
                println("Failure API REsponse")
                mDashboardApiListener.onContactUploadedFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<RecentactivityResponse>?,
                                    response: Response<RecentactivityResponse>?) {
                var mrecentactivityResponse: RecentactivityResponse? = response?.body()
//                println("Success API REsponse getRecentActivity "+mrecentactivityResponse.toString())
                if (response?.code() == 200 && mrecentactivityResponse?.meta?.status == true) {
                    mDashboardApiListener.onRecentactivityAPiSuccess(mrecentactivityResponse.data)
                } else {
                    mDashboardApiListener.onRecentactivityAPiFailure(mrecentactivityResponse?.meta?.message)
                }
            }

        })
    }

    fun getInvalidText(mActivity: Activity) {
        val call: Call<RecentactivityResponse> = TetootaApplication.getHeader()
                .getInvalidText(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity))
        call.enqueue(object : Callback<RecentactivityResponse> {
            override fun onFailure(call: Call<RecentactivityResponse>?, t: Throwable?) {
                println("Failure API REsponse")
                mDashboardApiListener.onContactUploadedFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<RecentactivityResponse>?,
                                    response: Response<RecentactivityResponse>?) {
                println("Success API REsponse")
                var mrecentactivityResponse: RecentactivityResponse? = response?.body()
                if (response?.code() == 200 && mrecentactivityResponse?.meta?.status == true) {
                    mDashboardApiListener.onInvalidTextAPiSuccess(mrecentactivityResponse.data)
                } else {
                    mDashboardApiListener.onInvalidTextAPiFailure(mrecentactivityResponse?.meta?.message)
                }
            }
        })
    }

    fun getTrendingList(mActivity: Activity, mUserId: String) {
        var call: Call<UserServiceResponse> = TetootaApplication.getHeader()
                .getTrendingNowList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId)
        call.enqueue(object : Callback<UserServiceResponse> {
            override fun onResponse(call: Call<UserServiceResponse>?, response: Response<UserServiceResponse>?) {
                println("Success")
                if (response?.code() == 200) {
                    if (response.body()?.data != null) {
                        mDashboardApiListener.onTrendingApiSuccess(response.body()!!.data as ArrayList<ServicesDataResponse>, "")
                    } else {
                        mDashboardApiListener.onTrendingApiFailure(response.message())
                    }
                } else {
                    mDashboardApiListener.onTrendingApiFailure(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                mDashboardApiListener.onTrendingApiFailure(t?.message.toString())
            }
        })
    }
}