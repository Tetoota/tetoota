package com.tetoota.fragment.home.data

import com.google.gson.annotations.SerializedName
import org.json.JSONArray

data class ContactUploadRequest(

	@field:SerializedName("user_id")
	var userId: String? = null,

	@field:SerializedName("contacts")
	var contacts: JSONArray? = null

)