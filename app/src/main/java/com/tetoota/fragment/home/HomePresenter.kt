package com.tetoota.fragment.dashboard

import android.app.Activity
import com.tetoota.fragment.home.data.ContactUploadRequest
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData

/**
 * Created by charchit.kasliwal on 05-06-2017.
 */
class HomePresenter : HomeContract.Presenter, HomeContract.dashboardApIListener {

    override fun getTrendingList(mActivity: Activity, userId: String) {
        mHomeInteractor.getTrendingList(mActivity, userId)
    }

    var mHomeContract: HomeContract.View
    private val mHomeInteractor: HomeInteractor by lazy {
        HomeInteractor(this)
    }

    constructor(mHomeContract: HomeContract.View) {
        this.mHomeContract = mHomeContract
    }

    override fun getDashboardData(mActivity: Activity) {
        // mHomeInteractor.getDashboardData(mActivity)
    }

    override fun getData(mActivity: Activity, myLatitude: Double, myLongitude: Double) {
        mHomeInteractor.getDashboardSlideData(mActivity, myLatitude, myLongitude)
    }

    override fun getIncompleteProposalData(mActivity: Activity, userId: String,userMacAddress : String) {
        mHomeInteractor.getIncompleteProposalApiCalling(mActivity, userId,userMacAddress)
    }

    override fun report(mActivity: Activity, mServiceId: String, mMesgDesc: String) {
        mHomeInteractor.report(mActivity, mServiceId, mMesgDesc)
    }

    override fun getDashboardSliderData(mActivity: Activity, myLatitude: Double, myLongitude: Double) {
        mHomeInteractor.getDashboardSlideData(mActivity,myLatitude,myLongitude)
    }

    override fun markFavorite(mActivity: Activity, cellRow: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        mHomeInteractor.markFavorites(mActivity, cellRow, mAttributeValue, p1)
    }


    override fun onDashboardAPiSuccess(mDataList: ArrayList<Any>, message: String) {
        mHomeContract.onDashboardApiSuccessResult(mDataList, message)
    }

    override fun onDashboardAPiSuccess(mDataList: ArrayList<Any>, arrayList: ArrayList<Any>, message: String) {
        mHomeContract.onDashboardApiSuccessResult(mDataList, arrayList, message)
    }

    override fun onFavoriteApiSuccess(message: String, cellRow: ServicesDataResponse, p1: Int) {
        mHomeContract.favoriteApiResult(message, cellRow, p1)
    }

    override fun onDashboardApiFailure(message: String) {
        mHomeContract.onDashboardApiFailureResult(message)
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        mHomeContract.onReportApiSuccess(key, message)
    }

    override fun uploadContacts(mActivity: Activity, userId: String, contactJson: String, contactUploadRequest: ContactUploadRequest) {
        mHomeInteractor.uploadContacts(mActivity, userId, contactJson, contactUploadRequest)
    }
    /*override fun uploadContacts(mActivity: Activity, user_id: String, contactJson: String, contactUploadRequest: ContactUploadRequest) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }*/

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        mHomeContract.onContactUploadedSuccess(contactUploadResponse)
    }

    override fun onContactUploadedFailure(message: String?) {
        mHomeContract.onContactUploadedFailure(message)
    }


    override fun onRecentactivityAPiSuccess(message: List<DataItem>) {
        mHomeContract.onRecentactivityAPiSuccessResult(message)
    }

    override fun onRecentactivityAPiFailure(message: String?) {
        mHomeContract.onRecentactivityAPiFailureResult(message)
    }

    override fun getRecentActivityData(mActivity: Activity) {
        mHomeInteractor.getRecentActivity(mActivity)
    }
    override fun getInvalidText(mActivity: Activity) {
        mHomeInteractor.getInvalidText(mActivity)
    }
    override fun onTrendingApiSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        mHomeContract.ongetTrendingSuccess(mServiceData, message)
    }

    override fun onTrendingApiFailure(message: String) {
        mHomeContract.ongetTrendingFailure(message)
    }

    override fun onIncompleteApiSuccess(mProposalMesgData: List<ProposalMessageData>, message: String) {
        mHomeContract.ongetIncompleteProposalSuccess(mProposalMesgData as ArrayList<ProposalMessageData>, message)
    }

    override fun onIncompleteApiFailure(message: String) {
        mHomeContract.ongetIncompleteProposalFailure(message)
    }

    override fun onInvalidTextAPiSuccess(message: List<DataItem>) {
        mHomeContract.onInvalidTextAPiSuccess(message)
    }

    override fun onInvalidTextAPiFailure(message: String?) {
        mHomeContract.onInvalidTextAPiFailure(message)
    }
}