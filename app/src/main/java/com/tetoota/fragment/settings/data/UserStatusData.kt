package com.tetoota.fragment.settings.data

import com.google.gson.annotations.SerializedName
import com.tetoota.fragment.corporateLogin.data.Meta

class UserStatusData {
    @field:SerializedName("meta")
    val meta: Meta? = null

    @field:SerializedName("data")
    val data: String? = null
}