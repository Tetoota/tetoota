package com.tetoota.fragment.settings
import com.tetoota.network.errorModel.Meta
data class UserVisibilityResponse(
		val data: UserVisibilityDataResponse? = null,
		val meta: Meta? = null
)
