package com.tetoota.fragment.settings

//import com.tetoota.main.PushNotificationSetttings
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.addrequest.BottomSheetDialog
import com.tetoota.fragment.BaseFragment
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.pushnotificationsetting.PushNotificationSetttings
import com.tetoota.pushnotificationsetting.PushSettingsContract
import com.tetoota.pushnotificationsetting.PushSettingsPresenter
import com.tetoota.selectlanguage.LanguageDataResponse
import com.tetoota.selectlanguage.SelectLanguageContract
import com.tetoota.selectlanguage.SelectLanguagePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_settings.*
import org.jetbrains.anko.toast


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SettingsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SettingsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SettingsFragment : BaseFragment(), SelectLanguageContract.View, View.OnClickListener, BottomSheetDialog.IBottomSheetListener,
        PushSettingsContract.View {


    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var isServicePreferred: Boolean = false
    private var mParam2: String? = null
    var mAll: String = ""
    private var mListener: OnFragmentInteractionListener? = null
    private var mList: OnFragmentInteraction? = null
    private val mSelectLanguagePresenter: SelectLanguagePresenter by lazy {
        SelectLanguagePresenter(this@SettingsFragment)
    }
    private val mProposalPresenter: PushSettingsPresenter by lazy {
        PushSettingsPresenter(this@SettingsFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_settings, container, false)

        return view
    }


    fun newInstance(): SettingsFragment {
        val fragment = SettingsFragment()
        return fragment
    }

    private fun setMultiLanguageText() {
        change_language.text = Utils.getText(context, StringConstant.change_language)
        change_current_locn.text = Utils.getText(context, StringConstant.change_current_locn)
        tv_sendemail.text = Utils.getText(context, StringConstant.send_email)
        tv_visible.text = Utils.getText(context, StringConstant.visible)
        tv_pushnotification.text = Utils.getText(context, StringConstant.push_notification)
        updateToggle()

        val companyId = Utils.loadPrefrence(Constant.COMPANY_ID, "0", activity)
        if (!companyId.equals("0")) {
            ll_public.visibility = View.VISIBLE
        }


    }

    private fun updateToggle() {
        Log.i(javaClass.name, "===================" + Utils.getVisible(context!!))
        if (Utils.getVisible(context!!).equals("0")) {
            isServicePreferred = false
            iv_toggle.setImageResource(R.drawable.tv_toggle_off)
        } else {
            isServicePreferred = true
            iv_toggle.setImageResource(R.drawable.tv_toggle_on)
        }
    }

    private fun updateStausToggle() {
        val status = Utils.loadPrefrence(Constant.USER_STATUS, "Private", activity)
        if (status.equals("Private")) {
            //iv_public.setImageResource(R.drawable.tv_toggle_off)
            tv_public.setText("Public")
        } else {
            // iv_public.setImageResource(R.drawable.tv_toggle_on)
            tv_public.setText("Private")
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ll_changelanguage.setOnClickListener(this)
        ll_pushnotifications.setOnClickListener(this)
        ll_visible.setOnClickListener(this)
        iv_toggle.setOnClickListener(this)
        iv_public.setOnClickListener(this)
        setMultiLanguageText()
        updateStausToggle()
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
        mList = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteraction) {
            //init the listener
            mList = context as OnFragmentInteraction
        } else {
            throw RuntimeException(context.toString() + " must implement InteractionListener")
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment SettingsFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): SettingsFragment {
            val fragment = SettingsFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onClick(v: View) {
        when (v) {
            ll_pushnotifications -> {
                val intent = PushNotificationSetttings.newMainIntent(this.activity!!)
                ActivityStack.getInstance(this.activity!!)
                startActivity(intent)
            }
            iv_toggle -> {
                if (isServicePreferred) {
                    mAll = "0"
                } else {
                    mAll = "1"
                }
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    mProposalPresenter.setUserVisibiltySettings(this.activity!!, personData.user_id.toString(), mAll)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
            ll_changelanguage -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                    mSelectLanguagePresenter.getLanguageData()
                    /* BottomSheetDialog.newInstance1(mTredingList as ArrayList<TradingDataResponse>,
                             this@SettingsFragment, "trading")
                     BottomSheetDialog().show(activity.supportFragmentManager, "dialog")*/
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }

            iv_public -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                    val status = Utils.loadPrefrence(Constant.USER_STATUS, "Private", activity)
                    if (status.equals("Public")) {
                        mProposalPresenter.setUserPublicPrivateSettings(this.activity!!, "Private")
                    } else {
                        mProposalPresenter.setUserPublicPrivateSettings(this.activity!!, "Public")
                    }


                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
        }
    }

    /*override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        activity.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }*/

    override fun dataClick(mTredingDataResponse: Any, type: String) {
        val mLanguageData = mTredingDataResponse as LanguageDataResponse
        Utils.savePreferences(Constant.USER_SELECTED_LANG, mLanguageData.lang_code,
                this.activity!!)
        if (Utils.haveNetworkConnection(this.activity!!)) {
            mSelectLanguagePresenter.getSelectedLanguageData(this.activity!!)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
        }

    }

    override fun onSelectLanguageSuccessResult(mCategoriesList: List<LanguageDataResponse>?, message: String) {
        hideProgressDialog()
        BottomSheetDialog.newInstance2(mCategoriesList as ArrayList<LanguageDataResponse>,
                this@SettingsFragment, "language")
        BottomSheetDialog().show(activity!!.supportFragmentManager, "dialog")
    }

    override fun onSelectLanguageFailureResult(message: String) {
    }

    override fun onSelectedLanguageSuccessResult(message: String) {
        super.onSelectedLanguageSuccessResult(message)
        if (message == "success") {
            setMultiLanguageText()
            mList?.messageFromParentFragmentToActivity("update")
            val i = Intent(activity, MainActivity::class.java)
            //  i.putExtra("lang",true)
            startActivity(i)
        } else {

        }
    }

    override fun pushNotificationSuccess(response: String, message: String) {
        hideProgressDialog()
        Utils.setVisible(mAll, context!!)
        Log.i(javaClass.name, "===================pushNotificationSuccess " + Utils.getVisible(context!!))
        updateToggle()
    }

    override fun pushNotificationFailure(response: String, message: String) {
        hideProgressDialog()
        // FAilure
    }

    interface OnFragmentInteraction {
        fun messageFromParentFragmentToActivity(myString: String)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear()
        super.onPrepareOptionsMenu(menu)
    }

    override fun onUserStatusSuccess(response: String, message: String) {
        Utils.savePreferences(Constant.USER_STATUS, response, this!!.activity!!)
        updateStausToggle()
        hideProgressDialog()
    }

    override fun onUserStatusFailure(response: String, message: String) {
        hideProgressDialog()
    }
}
