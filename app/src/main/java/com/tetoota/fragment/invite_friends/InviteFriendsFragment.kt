package com.tetoota.fragment.invite_friends

//import com.facebook.share.widget.ShareDialog
import android.app.Activity
import android.app.AlertDialog
import android.content.ComponentName
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.annotation.VisibleForTesting
import android.util.Log
import android.view.View
import android.widget.Toast
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.share.Sharer
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import com.facebook.share.widget.ShareDialog
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.customviews.AlertDialogManager
import com.tetoota.socialmedias.facebook.AlertDialogCallBack
import com.tetoota.utility.Constant
import com.tetoota.utility.Constant.constant.ANDROID_MIN_VERSION
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.invite_friends.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

/**
 * Created by jitendra.nandiya on 11-08-2017.
 */
class InviteFriendsFragment : BaseActivity(), View.OnClickListener, InviteFriendsContract.View, AlertDialogCallBack {
    var loginManager: LoginManager? = null
    lateinit var callbackManager: CallbackManager;
    var shareDialog: ShareDialog? = null
    var inviteFriend_text: String = ""
    var inviteFriend_link: String = "http://tetoota.com/referral/"
    //    lateinit var newDeepLink: Uri
    lateinit var shortLink: Uri

    // var inviteFriend_playLink: String = "https://play.google.com/store/apps/details?id=com.tetoota"
    //var inviteFriend_playLink : String = "http://tetoota.com/referral/index/"
    // var inviteFriend_link : String = "http://tetootacom.app.link/"
    override fun onSuccess(id: String?, `object`: Any?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCancel() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val REQUEST_TYPE_FACEBOOK = 1
    private val alert = AlertDialogManager()
    override fun onReferralSuccess(mString: String, mReferralCode: String) {
        if (null != progress_view)
            progress_view.visibility = View.GONE
        if (!mReferralCode.equals("")) {
            Utils.savePreferences(Constant.CALLREFERRALCODE, "true", this!!)
            Utils.savePreferences(Constant.REFERRALCODE, mReferralCode, this!!)
            tv_referralCode.text = mReferralCode
        }
    }

    companion object {
        private const val TAG = "MainActivity"
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, InviteFriendsFragment::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onReferralFailureResponse(mString: String, isServerError: Boolean) {
        if (null != progress_view)
            progress_view.visibility = View.GONE
    }

    val mReferralPresenter: InviteFriendsPresenter by lazy {
        InviteFriendsPresenter(this@InviteFriendsFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.invite_friends)
//        setHasOptionsMenu(true)
        // Initialize facebook SDK.
        FacebookSdk.sdkInitialize(this!!.getApplicationContext())
        // Create a callbackManager to handle the login responses.
        callbackManager = CallbackManager.Factory.create()
//        var permissionNeeds: ArrayList<String> = arrayListOf()
//        permissionNeeds.add("publish_actions")
//        //this loginManager helps you eliminate adding a LoginButton to your UI
//        loginManager = LoginManager.getInstance();
//
//        loginManager!!.logInWithPublishPermissions(this, permissionNeeds);
//
//        loginManager!!.registerCallback(callbackManager, loginCallback);

        shareDialog = ShareDialog(this)
        // this part is optional
        shareDialog!!.registerCallback(callbackManager, callback)

//        setActionBarToolbar()
        initViews()
        setMultiLanguageText()
        getReferaalCodeApiCall()

        // Validate that the developer has set the app code.
        validateAppCode()

        // Create a deep link and display it in the UI


    }

    fun getReferaalCodeApiCall() {
        if (Utils.haveNetworkConnection(this!!)) {
            progress_view.visibility = View.VISIBLE
            mReferralPresenter.getUserFavoriteList(this!!)
        } else {
            //progress_view.visibility = View.GONE
            toast(Utils.getText(this@InviteFriendsFragment, Utils.getText(this@InviteFriendsFragment, StringConstant.str_check_internet)))
        }
    }

    private fun setImageShareOnFacebook() {
        val image = BitmapFactory.decodeResource(resources, R.drawable.app_icon_facebook)
        val photo = SharePhoto.Builder()
                .setBitmap(image)
//                .setCaption("#Tutorialwing")
                .build()
        val content = SharePhotoContent.Builder()
                .addPhoto(photo)
                .build()
//        fb_share_button.shareContent = content
        //  shareDialog?.show(content, ShareDialog.Mode.AUTOMATIC);
        shareDialog?.show(content);
//        ShareApi.share(content, null);
//        fb_share_button.setShareContent(content)
    }

    /*  private fun setImageTextShareOnFacebook() {
  //        val shareLinkContent = ShareLinkContent.Builder()
  //        shareLinkContent.setContentTitle("Tetooota")
  //                .setContentDescription("Hello Friends....")
  ////                .setImageUrl(Uri.parse())
  //                .build()
  //        if (!ShareDialog.canShow(ShareLinkContent::class.java)) {
  //            return
  //        }
  //        shareDialog?.show(shareLinkContent,ShareDialog.Mode.AUTOMATIC);

          *//*val linkContent = ShareLinkContent.Builder()
                .setContentTitle("Android Facebook Integration and Login Tutorial")
                .setImageUrl(Uri.parse("https://www.studytutorial.in/wp-content/uploads/2017/02/FacebookLoginButton-min-300x136.png"))
                .setContentDescription("This tutorial explains how to integrate Facebook and Login through Android Application")
                .setContentUrl(Uri.parse("https://www.studytutorial.in/android-facebook-integration-and-login-tutorial"))
                .build();
        shareDialog?.show(linkContent);*//*
//        var languageMap = Utils.Utils.getLangByCode(activity,
//                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", activity))


        var ref = tv_referralCode.text.toString().trim()
        var newUrl = inviteFriend_text.replace("@", ref)
        val linkContent = ShareLinkContent.Builder()
                .setContentTitle(newUrl + "\n" + StringConstant.reffralCodeUrl + tv_referralCode.text.toString().trim())
                .setImageUrl(Uri.parse("android.resource://com.tetoota/drawable/iv_tetoota_icon"))
//                .setContentDescription("This tutorial explains how to integrate Facebook and Login through Android Application")
                .setContentUrl(Uri.parse("http://tetoota.com/referral/"))
                // .setContentUrl(Uri.parse("https://www.studytutorial.in/android-facebook-integration-and-login-tutorial"))
                .build();
        shareDialog?.show(linkContent);

    }*/

    public fun getLocalBitmapUri(): Uri {
        val image = BitmapFactory.decodeResource(resources, R.drawable.iv_tetoota_icon)
        var bmpUri: Uri? = null
        try {
            var file: File = File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png")
            file.getParentFile().mkdirs()
            var out: FileOutputStream = FileOutputStream(file)
            image?.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.close()
            bmpUri = Uri.fromFile(file)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri!!
    }

//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
//                              savedInstanceState: Bundle?): View? {
//        // Inflate the layout for this fragment
//        val view = inflater.inflate(R.layout.invite_friends, container, false)
//        return view
//    }

    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
        // tv_friendJoinsEarns.text = Utils.getText(this@InviteFriendsFragment, StringConstant.friends_join) + "up to 100"
        tv_friendJoinsEarns.text = Utils.getText(this@InviteFriendsFragment, StringConstant.friends_join)
        // tv_friendTradeYouEarn.text = Utils.getText(this@InviteFriendsFragment, StringConstant.friend_trade_you_earn) + " 50"
        tv_friendTradeYouEarn.text = Utils.getText(this@InviteFriendsFragment, StringConstant.friend_trade_you_earn)
        tv_shareYourReferralCode.text = Utils.getText(this@InviteFriendsFragment, StringConstant.share_referral)
        btn_ShareAndInviteFrnd.text = Utils.getText(this@InviteFriendsFragment, StringConstant.share_invite_friends)
        tv_sms.text = Utils.getText(this@InviteFriendsFragment, StringConstant.sms)
        tv_facebook.text = Utils.getText(this@InviteFriendsFragment, StringConstant.facebook)
        tv_whatsapp.text = Utils.getText(this@InviteFriendsFragment, StringConstant.whats_app)
        tv_email.text = Utils.getText(this@InviteFriendsFragment, StringConstant.email)
        inviteFriend_text = Utils.getText(this@InviteFriendsFragment, StringConstant.inviteFriend_text)
    }

//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//    }

/*override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
    activity.menuInflater.inflate(R.menu.dashboard_menu, menu)
    menu?.findItem(R.id.action_search)?.isVisible = true
    super.onCreateOptionsMenu(menu, inflater)
}*/

    private fun initViews() {
        toolbar_title.text = Utils.getText(this@InviteFriendsFragment, StringConstant.etf_Refer_your_friend_and_earn)
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        rl_sms.setOnClickListener(this)
        rl_email.setOnClickListener(this)
        rl_whatsapp.setOnClickListener(this)
        btn_ShareAndInviteFrnd.setOnClickListener(this)
        rl_facebook.setOnClickListener(this)
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        setActionBarToolbar()
//        initViews()
//        setMultiLanguageText()
//        getReferaalCodeApiCall()
//    }

//    override fun onAttach(this@InviteFriendsFragment: Context?) {
//        super.onAttach(this@InviteFriendsFragment)
//    }

//    override fun onDetach() {
//        super.onDetach()
//    }

//    override fun onDestroyView() {
//        super.onDestroyView()
//        mReferralPresenter.cancelRequest(this.activity!!, "services")
//    }

    override fun onPause() {
        super.onPause()
        mReferralPresenter.cancelRequest(this, "services")

    }
//    companion object {
//        // TODO: Rename parameter arguments, choose names that match
//        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//        private val ARG_PARAM1 = "param1"
//        private val ARG_PARAM2 = "param2"
//        var iFragmentOpenCloseListener: IFragmentOpenCloseListener? = null
//
//        /**
//         * Use this factory method to create a new instance of
//         * this fragment using the provided parameters.
//
//         * @param param1 Parameter 1.
//         * *
//         * @param param2 Parameter 2.
//         * *
//         * @return A new instance of fragment InviteFriends.
//         */
//        // TODO: Rename and change types and number of parameters
////        fun newInstance(param1: String, param2: String, iFragmentOpenCloseListenerLocal: IFragmentOpenCloseListener): InviteFriendsFragment {
////            iFragmentOpenCloseListener = iFragmentOpenCloseListenerLocal
////            val fragment = InviteFriendsFragment()
////            val args = Bundle()
////            args.putString(ARG_PARAM1, param1)
////            args.putString(ARG_PARAM2, param2)
////            fragment.arguments = args
////            return fragment
////        }
////
////        // TODO: Rename and change types and number of parameters
////        fun newInstance(param1: String, param2: String): InviteFriendsFragment {
////            val fragment = InviteFriendsFragment()
////            val args = Bundle()
////            args.putString(ARG_PARAM1, param1)
////            args.putString(ARG_PARAM2, param2)
////            fragment.arguments = args
////            return fragment
////        }
//    }

//    private fun setActionBarToolbar() {
//        if (this@InviteFriendsFragment.arguments!!.getString(ARG_PARAM2).equals("profileScreen")) {
//            iFragmentOpenCloseListener!!.onFragmentOpenClose("profile", "invite_friends", "")
//        } else if (this@InviteFriendsFragment.arguments!!.getString(ARG_PARAM2).equals("earnTetootaPoints")) {
//            iFragmentOpenCloseListener!!.onFragmentOpenClose("earnTetootaPoints", "invite_friends", "")
//        }
//    }

    override fun onResume() {
        super.onResume()
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_ShareAndInviteFrnd -> {
                if (!tv_referralCode.text.equals("")) {
//                    shareDeepLink(newDeepLink.toString())
                    mProgressDialog?.show()
                    val newDeepLink = buildDeepLink(Uri.parse(inviteFriend_link + tv_referralCode.text.toString().trim()), ANDROID_MIN_VERSION)
                    Log.e("LOADDEEP", newDeepLink.toString());
//                    sendSms()
                } else {
//                    openDailog()
                }
            }
            rl_sms -> {
                if (!tv_referralCode.text.equals("")) {
//                    shareDeepLink(newDeepLink.toString())
                    mProgressDialog?.show()
                    val newDeepLink = buildDeepLink(Uri.parse(inviteFriend_link + tv_referralCode.text.toString().trim()), ANDROID_MIN_VERSION)
                    Log.e("LOADDEEP", newDeepLink.toString());
//                    sendSms()
                } else {
//                    openDailog()
                }
            }
            rl_email -> {
                if (!tv_referralCode.text.equals("")) {
                    sendEmail()
                } else {
//                    openDailog()
                }
            }
            rl_whatsapp -> {
                if (!tv_referralCode.text.equals("")) {
//                    openWhatsApp()
                    sendOnWhatsapp()
                } else {
//                    openDailog()
                }
            }
            rl_facebook -> {
                if (!tv_referralCode.text.equals("")) {
                    var permissionNeeds: ArrayList<String> = arrayListOf()
                    permissionNeeds.add("publish_actions")
//                    permissionNeeds.add("email,public_profile")
                    //this loginManager helps you eliminate adding a LoginButton to your UI
                    loginManager = LoginManager.getInstance();
                    loginManager!!.logInWithPublishPermissions(this, permissionNeeds)
                    loginManager!!.registerCallback(callbackManager, loginCallback);
                } else {
//                    openDailog()
                }
            }
        }
    }

    private fun openDailog() {
        val alertDialog = AlertDialog.Builder(this@InviteFriendsFragment).create() //Read Update
        alertDialog.setTitle("No Data")
        alertDialog.setMessage("Please try again...")
        alertDialog.setButton("Ok", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                alertDialog.dismiss()
            }
        })
        alertDialog.show()  //<-- See This!
    }

    private fun setImageTextShareOnFacebook() {
        var ref = tv_referralCode.text.toString().trim()
        val linkContent = ShareLinkContent.Builder()
                // .setContentTitle("Tetoota App")
                // .setImageUrl(Uri.parse("android.resource://com.tetoota/drawable/iv_tetoota_icon"))
                .setContentUrl(Uri.parse("http://tetoota.com/"))
                .build();
        shareDialog?.show(linkContent);

    }


    private fun sendSms() {
        try {
            var ref = tv_referralCode.text.toString().trim()
            var newUrl = inviteFriend_text.replace("@", ref)
            val uri = Uri.parse("smsto:")
            val it = Intent(Intent.ACTION_SENDTO, uri)
            it.putExtra("sms_body", newUrl + "\n" + inviteFriend_link + tv_referralCode.text.toString().trim())
            // it.putExtra("sms_body", inviteFriend_text+"\n"+ tv_referralCode.text.toString().trim())
            // it.putExtra("sms_body", newUrl)
            startActivityForResult(it, 1)

            // startActivity(Intent.createChooser(it, ""))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun sendEmail() {
        var ref = tv_referralCode.text.toString().trim()
        var newUrl = inviteFriend_text.replace("@", ref)
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
//        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("some@email.address"))
        intent.putExtra(Intent.EXTRA_SUBJECT, "subject")
        // intent.putExtra(Intent.EXTRA_TEXT, inviteFriend_text+"\n"+Utils.getText(this@InviteFriendsFragment,StringConstant.reffralCodeUrl)+tv_referralCode.text.toString().trim())
        intent.putExtra(Intent.EXTRA_TEXT, newUrl + "\n" + inviteFriend_link + tv_referralCode.text.toString().trim())
        startActivity(Intent.createChooser(intent, ""))
    }

    private fun sendOnWhatsapp() {
        var ref = tv_referralCode.text.toString().trim()
        var newUrl = inviteFriend_text.replace("@", ref)
        val text = newUrl + "\n" + inviteFriend_link + tv_referralCode.text.toString().trim()

        //  val uri = Uri.parse("android.resource://com.tetoota/drawable/iv_tetoota_icon")
        val shareIntent = Intent(android.content.Intent.ACTION_SEND)
//        shareIntent.type = "image/*"

        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)


        // shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        val pm: PackageManager = this@InviteFriendsFragment!!.getPackageManager()
        var activityList = pm.queryIntentActivities(shareIntent, 0)
        var isWhatappInstall = false
        for (app: ResolveInfo in activityList) {
            if ((app.activityInfo.name).contains("com.whatsapp")) {
                var activity = app.activityInfo
                var name = ComponentName(activity.applicationInfo.packageName, activity.name)
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
                shareIntent.setComponent(name)
                startActivity(shareIntent)
                isWhatappInstall = true
                break
            }
        }
        if (!isWhatappInstall) {
            Toast.makeText(this@InviteFriendsFragment, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                    .show()
        }
    }

    /*   fun openWhatsApp() {
           val pm: PackageManager = this@InviteFriendsFragment!!.getPackageManager()
           try {
               val waIntent = Intent(Intent.ACTION_SEND)
               waIntent.type = "text/plain"
              // val text = inviteFriend_text +"\n"+Utils.getText(this@InviteFriendsFragment,StringConstant.reffralCodeUrl)+tv_referralCode.text.toString().trim()
               val text = inviteFriend_text +"\n"+inviteFriend_link+tv_referralCode.text.toString().trim()

               val info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)
               //Check if package exists or not. If not then code
               //in catch block will be called
               waIntent.`package` = "com.whatsapp"

               waIntent.putExtra(Intent.EXTRA_TEXT, text)
               startActivity(Intent.createChooser(waIntent, "Share with"))
           } catch (e: PackageManager.NameNotFoundException) {
               Toast.makeText(this@InviteFriendsFragment, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                       .show()
           }
       }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_TYPE_FACEBOOK -> alert.showAlertDialog(this@InviteFriendsFragment, "Success",
                        "Facebook data Shared Successfully", false, true, this, "1")
                1 ->
                    Log.e("Success", "Success")
            }
        }
//        else {
//            alert.showAlertDialog(this@InviteFriendsFragment, "Error!!",
//                    "Login failed. Please try again later.", false, false, this, "0")
//        }
    }

    private val callback = object : FacebookCallback<Sharer.Result> {
        override fun onSuccess(result: Sharer.Result) {
            Log.v("FACEBOOK CALLBACE", "Successfully posted")
            toast("Posted Successfully")
        }

        override fun onCancel() {
            Log.v("FACEBOOK CALLBACE", "Sharing cancelled")
            toast("Sharing cancelled")
        }

        override fun onError(error: FacebookException) {
            Log.v("FACEBOOK CALLBACE", error.message)
            toast(error.message!!)
        }
    }

    private val loginCallback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            Log.v("FACEBOOK LOGIN_CALLBACE", "Successfully Login")
            toast("Login Successfully")
            // setImageShareOnFacebook()
            setImageTextShareOnFacebook()
        }

        override fun onCancel() {
            Log.v("FACEBOOK LOGIN_CALLBACE", "Login cancelled")
            //    toast("Login cancelled")
            setImageTextShareOnFacebook()

        }

        override fun onError(error: FacebookException) {
            Log.v("FACEBOOK LOGIN_CALLBACE", error.message)
            toast(error.message!!)
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//        menu?.clear()
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    override fun onPrepareOptionsMenu(menu: Menu?) {
//        menu?.clear();
//        super.onPrepareOptionsMenu(menu)
//    }

    /*fun getNetworkClass(): String {
        var mTelephonyManager: TelephonyManager = this@InviteFriendsFragment?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager;
        var networkType: Int = mTelephonyManager.getNetworkType()
        when (networkType) {
            TelephonyManager.NETWORK_TYPE_GPRS -> return "2G"
            TelephonyManager.NETWORK_TYPE_EDGE -> return "2G"
            TelephonyManager.NETWORK_TYPE_CDMA -> return "2G"
            TelephonyManager.NETWORK_TYPE_1xRTT -> return "2G"
            TelephonyManager.NETWORK_TYPE_IDEN -> return "2G"
            TelephonyManager.NETWORK_TYPE_UMTS -> return "3G"
            TelephonyManager.NETWORK_TYPE_EVDO_0 -> return "3G"
            TelephonyManager.NETWORK_TYPE_EVDO_A -> return "3G"
            TelephonyManager.NETWORK_TYPE_HSDPA -> return "3G"
            TelephonyManager.NETWORK_TYPE_HSUPA -> return "3G"
            TelephonyManager.NETWORK_TYPE_HSPA -> return "3G"
            TelephonyManager.NETWORK_TYPE_EVDO_B -> return "3G"
            TelephonyManager.NETWORK_TYPE_EHRPD -> return "3G"
            TelephonyManager.NETWORK_TYPE_HSPAP -> return "3G"
            TelephonyManager.NETWORK_TYPE_LTE -> return "4G"
            else -> { // Note the block
                return "Unknown"
            }
        }
    }*/

    /**
     * Build a Firebase Dynamic Link.
     * https://firebase.google.com/docs/dynamic-links/android/create#create-a-dynamic-link-from-parameters
     *
     * @param deepLink the deep link your app will open. This link must be a valid URL and use the
     * HTTP or HTTPS scheme.
     * @param minVersion the `versionCode` of the minimum version of your app that can open
     * the deep link. If the installed app is an older version, the user is taken
     * to the Play store to upgrade the app. Pass 0 if you do not
     * require a minimum version.
     * @return a [Uri] representing a properly formed deep link.
     */
    @VisibleForTesting
    fun buildDeepLink(deepLink: Uri, minVersion: Int): Uri {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)

        // Set dynamic link parameters:
        //  * URI prefix (required)
        //  * Android Parameters (required)
        //  * Deep link
        // [START build_dynamic_link]
        val builder = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix(uriPrefix)
                .setAndroidParameters(DynamicLink.AndroidParameters.Builder()
                        .setMinimumVersion(minVersion)
                        .build())
                .setIosParameters(
                        DynamicLink.IosParameters.Builder("com.tetoota")
                                .setAppStoreId("1402450812")
                                .setMinimumVersion("1.2")
                                .build())
                .setLink(deepLink)

        // Build the dynamic link
        val link = builder
//                .buildDynamicLink()
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                .addOnSuccessListener { result ->
                    // Short link created
                    shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    Log.e(TAG, "" + shortLink)
                    shareService(this, shortLink.toString())
                }.addOnFailureListener {
                    Log.e(TAG, "FAIL")
                }
        // [END build_dynamic_link]
        // Return the dynamic link as a URI
        return Uri.parse("")
    }

    private fun shareDeepLink(deepLink: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link")
        intent.putExtra(Intent.EXTRA_TEXT, deepLink)

        startActivity(intent)
    }

    fun shareService(context: Context?, deepLink: String) {
        mProgressDialog?.dismiss()
        try {
            var ref = tv_referralCode.text.toString().trim()
            var newUrl = inviteFriend_text.replace("@", ref)
            val text = newUrl + "\n" + deepLink

            Log.e(TAG, ref + "\n\n" + "newUrl" + inviteFriend_text.replace("@", ref) + "\n\n" + text)
//            val urlToShare = StringConstant.app_link
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(context.getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            //  Log.e("imageUri - ","" + imageUri)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            //  shareIntent.type = "image/*"
            // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
//            context?.startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun validateAppCode() {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)
        if (uriPrefix.contains("YOUR_APP")) {
            AlertDialog.Builder(this)
                    .setTitle("Invalid Configuration")
                    .setMessage("Please set your Dynamic Links domain in app/build.gradle")
                    .setPositiveButton(android.R.string.ok, null)
                    .create().show()
        }
    }

}
