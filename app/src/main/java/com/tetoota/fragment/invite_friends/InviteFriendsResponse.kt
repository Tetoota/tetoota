package com.tetoota.fragment.invite_friends

data class InviteFriendsResponse(
	val data: String? = null,
	val meta: Meta? = null
)
