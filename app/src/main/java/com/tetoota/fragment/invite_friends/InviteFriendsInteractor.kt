package com.tetoota.fragment.invite_friends

import android.app.Activity
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class InviteFriendsInteractor {
    var call : Call<InviteFriendsResponse>? = null
    var mInviteFriendsListener : InviteFriendsContract.ReferralCodeApiResult

    constructor(mFavoriteListener: InviteFriendsContract.ReferralCodeApiResult) {
        this.mInviteFriendsListener = mFavoriteListener
    }

    fun getREferralCode(mActivity: Activity){
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData  = Gson().fromJson(json, LoginDataResponse::class.java)
        call = TetootaApplication.getHeader()
                .getReferralCode(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),
                        personData.user_id!!)
        call!!.enqueue(object : Callback<InviteFriendsResponse> {
            override fun onResponse(call: Call<InviteFriendsResponse>?,
                                    response: Response<InviteFriendsResponse>?) {
                var mDashboardSliderData : InviteFriendsResponse? = response?.body()
                if(response?.code() == 200){
                    if(response.body()?.meta?.status!!){
                        if (mDashboardSliderData != null) {
                            mInviteFriendsListener.onReferralCodeApiSuccess("success", mDashboardSliderData.data as String)
                        }
                    }else{
                        mInviteFriendsListener.onReferralCodeApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                }else{
                    mInviteFriendsListener.onReferralCodeApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }
            override fun onFailure(call: Call<InviteFriendsResponse>?, t: Throwable?) {
                mInviteFriendsListener.onReferralCodeApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun cancelREquest(mActivity: Activity, mCancelRequest: String) {
//        if (mCancelRequest.equals("userService")) {
            call?.cancel()
//        } else {
//            mWishListCall?.cancel()
//        }
    }
}