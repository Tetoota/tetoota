package com.tetoota.fragment.postonsocialmedia

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class SharePointsContract {
    interface View {
        fun onSharePointsSuccess(mString: String, mFavoriteList: String): Unit
        fun onSharePointsFailureResponse(mString: String, isServerError: Boolean): Unit
    }

    interface Presenter {
        fun getSharePointsCouponsCode(mActivity: Activity, mReferralCode: String?, instagram: String)
    }

    interface SharePointsApiResult {
        fun onSharePointApiSuccess(mString: String, mReferralCode: String): Unit
        fun onShareApiFailure(mString: String, isServerError: Boolean): Unit
    }
}