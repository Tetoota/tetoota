package com.tetoota.fragment.postonsocialmedia

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class SharePointsPresenter : SharePointsContract.Presenter, SharePointsContract.SharePointsApiResult {
    var mFavoriteView: SharePointsContract.View

    val mFavoriteInteractor: SharePointsInteractor by lazy {
        SharePointsInteractor(this)
    }

    constructor(mFavoriteView: SharePointsContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun getSharePointsCouponsCode(mActivity: Activity, user_id: String?, type: String) {
        mFavoriteInteractor.getRedeemCoupons(mActivity, user_id, type)
    }

    override fun onSharePointApiSuccess(mString: String, mReferralCode: String) {
        mFavoriteView.onSharePointsSuccess(mString, mReferralCode)
    }

    override fun onShareApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onSharePointsFailureResponse(mString, isServerError)
    }
}