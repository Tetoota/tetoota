package com.tetoota.fragment.postonsocialmedia

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.share.Sharer
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import com.facebook.share.widget.ShareDialog
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Constant.constant.Facebook
import com.tetoota.utility.Constant.constant.Instagram
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_post_social_media.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast


class PostSocialMedia : BaseActivity(), View.OnClickListener, SharePointsContract.View {

    override fun onSharePointsFailureResponse(mString: String, isServerError: Boolean) {
    }

    override fun onSharePointsSuccess(mString: String, mFavoriteList: String) {
        openDailog("Success", mFavoriteList)
    }

    private fun openDailog(header: String, mResponseMsg: String) {
        val alertDialog = AlertDialog.Builder(context).create() //Read Update
//        alertDialog.setTitle(header)
        alertDialog.setMessage(mResponseMsg)
        alertDialog.setButton("Ok", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                alertDialog.dismiss()
            }
        })
        alertDialog.show()  //<-- See This!
    }

    val mSharePointsPresenter: SharePointsPresenter by lazy {
        SharePointsPresenter(this)
    }

    var shareDialog: ShareDialog? = null
    var context: Context? = null
    var loginManager: LoginManager? = null
    lateinit var callbackManager: CallbackManager;
    val INSTA: Int = 111
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_social_media)

        context = this
        rl_facebook
        callbackManager = CallbackManager.Factory.create()
        shareDialog = ShareDialog(this)
        shareDialog!!.registerCallback(callbackManager, callback)
        initViews()

        // Validate that the developer has set the app code.
//        validateAppCode()
//
//        // Create a deep link and display it in the UI
//        val newDeepLink = buildDeepLink(Uri.parse(DEEP_LINK_URL), 0)
    }

    private fun initViews(): Unit {

        toolbar_title.text = Utils.getText(context, StringConstant.post_social_media)
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        tv_friendTradeYouEarn.text= Utils.getText(context, StringConstant.post_social_media_text)
        btn_ShareAndInviteFrnd.text= Utils.getText(context, StringConstant.share_with)
        rl_facebook.setOnClickListener(this)
        rl_instagram.setOnClickListener(this)
        rl_linkedin.setOnClickListener(this)
        rl_twitter.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v) {
            rl_facebook -> {
//                shareOnFb();
//                shareService(this)

                var permissionNeeds: ArrayList<String> = arrayListOf()
//                permissionNeeds.add("publish_actions")
                loginManager = LoginManager.getInstance();
                loginManager!!.logInWithPublishPermissions(this, permissionNeeds)
                loginManager!!.registerCallback(callbackManager, loginCallback);
            }
            rl_instagram -> {
                //  sendEmail()

                shareOnInstagram()
//                shareService(this)
            }
            rl_linkedin -> {
                //  sendOnWhatsapp()
                shareService(this)
            }
            rl_twitter -> {
                Utils.shareService(this)
            }
        }
    }

    fun shareService(context: Context?) {
        try {
            val urlToShare = StringConstant.app_link
            val json = com.tetoota.utility.Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            val postString: String = personData.first_name + "is sharing talent on " + context?.resources?.getString(R.string.app_name) + " free skill exchange app." + " Download now\n" + urlToShare
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(context.getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            //  Log.e("imageUri - ","" + imageUri)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, postString)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            //  shareIntent.type = "image/*"
            // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
//            context?.startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    fun getSharePointApi(type: String) {
//        Utils.hideSoftKeyboard(this)
        if (Utils.haveNetworkConnection(this)) {
//            progress_view.visibility = View.VISIBLE
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                mSharePointsPresenter.getSharePointsCouponsCode(this, personData?.user_id, type)
        } else {
            toast(Utils.getText(context, StringConstant.str_check_internet))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)

        if (requestCode == INSTA) {
            Log.e("success", "success")
            getSharePointApi(Instagram)
        }
        if (resultCode == Activity.RESULT_OK) {
            if (data != null && data.component != null && !TextUtils.isEmpty(data.component!!.flattenToShortString())) {
                val appName = data.component!!.flattenToShortString()
                // Now you know the app being picked.
                // data is a copy of your launchIntent with this important extra info added.

                // Start the selected activity
                startActivity(data)
            }
            when (requestCode) {
                100 -> Log.e("success", "success")
                else -> Log.e("fail", "fail")
            }
        }
//        else {
//            alert.showAlertDialog(this@InviteFriendsFragment, "Error!!",
//                    "Login failed. Please try again later.", false, false, this, "0")
//        }
    }


    private fun shareOnFb() {
        val urlToShare = "https://bit.ly/2MCyOW0"
        var intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare)

// See if official Facebook app is found
        var facebookAppFound = false
        val matches = packageManager.queryIntentActivities(intent, 0)
        for (info in matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                intent.setPackage(info.activityInfo.packageName)
                facebookAppFound = true
                break
            }
        }

// As fallback, launch sharer.php in a browser
        if (!facebookAppFound) {
            val sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=$urlToShare"
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl))
        }

        startActivity(intent)
    }


    private fun setImageTextShareOnFacebook() {

        var imageUri: Uri? = null
        try {
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(getResources(), R.drawable.share_social),
                    null, null));
        } catch (e: NullPointerException) {
        }

        val icon = BitmapFactory.decodeResource(context!!.getResources(),
                R.drawable.share_social)
//        val backgroundAssetUri = R.drawable.share_social
        val attributionLinkUrl = "https://play.google.com/store/apps/details?id=com.tetoota&hl=en_IN";
        val photo = SharePhoto.Builder()
                .setBitmap(icon)
                .build();
        val content = SharePhotoContent.Builder()
                .addPhoto(photo)
//                .setContentUrl(Uri.parse(attributionLinkUrl))
                .build()

//        val linkContent = ShareLinkContent.Builder()
//                .setQuote("Hi")
//                .setImageUrl(imageUri)
//                .setContentUrl(Uri.parse(attributionLinkUrl))
//                .build();

        shareDialog?.show(content);

    }

    private fun sendSms() {
        try {
            /*   var ref = tv_referralCode.text.toString().trim()
               var newUrl = inviteFriend_text.replace("@", ref)
               val uri = Uri.parse("smsto:")
               val it = Intent(Intent.ACTION_SENDTO, uri)
               it.putExtra("sms_body", newUrl + "\n" + inviteFriend_link + tv_referralCode.text.toString().trim())
               startActivity(it)*/

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun sendEmail() {
        /*    var ref = tv_referralCode.text.toString().trim()
            var newUrl = inviteFriend_text.replace("@", ref)
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "message/rfc822"
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("some@email.address"))
            intent.putExtra(Intent.EXTRA_SUBJECT, "subject")
            intent.putExtra(Intent.EXTRA_TEXT, newUrl + "\n" + inviteFriend_link + tv_referralCode.text.toString().trim())
            startActivity(Intent.createChooser(intent, ""))*/
    }

    //        private fun sendOnWhatsapp() {
//            var ref = tv_referralCode.text.toString().trim()
//            var newUrl = inviteFriend_text.replace("@", ref)
//            val text = newUrl + "\n" + inviteFriend_link + tv_referralCode.text.toString().trim()
//            val shareIntent = Intent(android.content.Intent.ACTION_SEND)
//            shareIntent.type = "text/plain"
//            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
//            val pm: PackageManager = context!!.getPackageManager()
//            var activityList = pm.queryIntentActivities(shareIntent, 0)
//            var isWhatappInstall = false
//            for (app: ResolveInfo in activityList) {
//                if ((app.activityInfo.name).contains("com.whatsapp")) {
//                    var activity = app.activityInfo
//                    var name = ComponentName(activity.applicationInfo.packageName, activity.name)
//                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
//                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
//                    shareIntent.setComponent(name)
//                    startActivity(shareIntent)
//                    isWhatappInstall = true
//                    break
//                }
//            }
//            if (!isWhatappInstall) {
//                Toast.makeText(this@InviteFriendsFragment.context, "WhatsApp not Installed", Toast.LENGTH_SHORT)
//                        .show()
//            }
//        }
//    private fun shareOnInstagram() {
//        val shareIntent = Intent(android.content.Intent.ACTION_SEND)
//        shareIntent.type = "text/plain"
//        shareIntent.putExtra(Intent.EXTRA_TEXT, "LOL")
//        val pm: PackageManager = context!!.getPackageManager()
//        var activityList = pm.queryIntentActivities(shareIntent, 0)
//        var isWhatappInstall = false
//        for (app: ResolveInfo in activityList) {
//            Log.e(TAG,"HIHI "+app.activityInfo.name);
//            if ((app.activityInfo.name).contains("instagram")) {
//                var activity = app.activityInfo
//                var name = ComponentName(activity.applicationInfo.packageName, activity.name)
//                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
//                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                shareIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
//                shareIntent.setComponent(name)
//                startActivity(shareIntent)
//                isWhatappInstall = true
//                break
//            }
//        }
//        if (!isWhatappInstall) {
//            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
//                    .show()
//        }
//    }

    private fun shareOnInstagram() {

        // Define image asset URI and attribution link URL
        var imageUri: Uri? = null
        try {
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(getResources(), R.drawable.share_social),
                    null, null));
        } catch (e: NullPointerException) {
        }

//        val backgroundAssetUri = R.drawable.share_social
        val attributionLinkUrl = "https://play.google.com/store/apps/details?id=com.tetoota&hl=en_IN";

// Instantiate implicit intent with ADD_TO_STORY action,
// background asset, and attribution link
        val intent = Intent("com.instagram.share.ADD_TO_STORY");

// Instantiate implicit intent with ADD_TO_STORY action,
// sticker asset, background colors, and attribution link
        intent.putExtra("content_url", attributionLinkUrl)
        intent.setDataAndType(imageUri, "image/png");
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra("content_url", attributionLinkUrl);

// Instantiate activity and verify it will resolve implicit intent
        if (getPackageManager().resolveActivity(intent, 0) != null) {
            startActivityForResult(intent, INSTA);
        }

//        val shareIntent = Intent(android.content.Intent.ACTION_SEND)
//        shareIntent.type = "text/plain"
//        shareIntent.putExtra(Intent.EXTRA_TEXT, "LOL")
//        val pm: PackageManager = context!!.getPackageManager()
//        var activityList = pm.queryIntentActivities(shareIntent, 0)
//        var isWhatappInstall = false
//        for (app: ResolveInfo in activityList) {
//            Log.e(TAG, "HIHI " + app.activityInfo.name);
//            if ((app.activityInfo.name).contains("instagram")) {
//                var activity = app.activityInfo
//                var name = ComponentName(activity.applicationInfo.packageName, activity.name)
//                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
//                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                shareIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
//                shareIntent.setComponent(name)
//                startActivity(shareIntent)
//                isWhatappInstall = true
//                break
//            }
//        }
//        if (!isWhatappInstall) {
//            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
//                    .show()
//        }
    }

//    private fun shareOnInstagram() {
////        var ref = tv_referralCode.text.toString().trim()
////        var newUrl = inviteFriend_text.replace("@", ref)
////        val text = newUrl + "\n" + inviteFriend_link + tv_referralCode.text.toString().trim()
//        var imageUri: Uri? = null
//        try {
//            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(this.getResources(), R.drawable.share_social),
//                    null, null));
//        } catch (e: NullPointerException) {
//        }
//
//        val shareIntent = Intent(android.content.Intent.ACTION_SEND)
////        shareIntent.type = "text/plain"
////        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
//        val pm: PackageManager = context!!.getPackageManager()
//        var activityList = pm.queryIntentActivities(shareIntent, 0)
//        var isWhatappInstall = false
//        for (app: ResolveInfo in activityList) {
//            if ((app.activityInfo.name).contains("instagram")) {
//                var activity = app.activityInfo
//                var name = ComponentName(activity.applicationInfo.packageName, activity.name)
//                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
//                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                shareIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
//                shareIntent.action = Intent.ACTION_SEND
//                shareIntent.setType("*/png");
//                shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
//                shareIntent.setComponent(name)
//                startActivity(shareIntent)
//                isWhatappInstall = true
//                break
//            }
//        }
//        if (!isWhatappInstall) {
//            Toast.makeText(this, "instagram not Installed", Toast.LENGTH_SHORT)
//                    .show()
//        }
//
//    }

    private val callback = object : FacebookCallback<Sharer.Result> {
        override fun onSuccess(result: Sharer.Result) {
            Log.v("FACEBOOK CALLBACE", "Successfully posted")
            context!!.toast("Posted Successfully")
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)

            getSharePointApi(Facebook)
        }

        override fun onCancel() {
            Log.v("FACEBOOK CALLBACE", "Sharing cancelled")
            context!!.toast("Sharing cancelled")
        }

        override fun onError(error: FacebookException) {
            Log.v("FACEBOOK CALLBACE", error.message)
            context!!.toast(error.message!!)
        }
    }
    private val loginCallback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            Log.v("FACEBOOK LOGIN_CALLBACE", "Successfully Login")
            context!!.toast("Login Successfully")
            // setImageShareOnFacebook()
            setImageTextShareOnFacebook()
        }

        override fun onCancel() {
            Log.v("FACEBOOK LOGIN_CALLBACE", "Login cancelled")
            //    activity!!.toast("Login cancelled")
            setImageTextShareOnFacebook()

        }

        override fun onError(error: FacebookException) {
            Log.v("FACEBOOK LOGIN_CALLBACE", error.message)
            context!!.toast(error.message!!)
        }
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val DEEP_LINK_URL = "https://kotlin.example.com/deeplinks"
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, PostSocialMedia::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@PostSocialMedia)
        finish()
    }

//    /**
//     * Build a Firebase Dynamic Link.
//     * https://firebase.google.com/docs/dynamic-links/android/create#create-a-dynamic-link-from-parameters
//     *
//     * @param deepLink the deep link your app will open. This link must be a valid URL and use the
//     * HTTP or HTTPS scheme.
//     * @param minVersion the `versionCode` of the minimum version of your app that can open
//     * the deep link. If the installed app is an older version, the user is taken
//     * to the Play store to upgrade the app. Pass 0 if you do not
//     * require a minimum version.
//     * @return a [Uri] representing a properly formed deep link.
//     */
//    @VisibleForTesting
//    fun buildDeepLink(deepLink: Uri, minVersion: Int): Uri {
//        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)
//
//        // Set dynamic link parameters:
//        //  * URI prefix (required)
//        //  * Android Parameters (required)
//        //  * Deep link
//        // [START build_dynamic_link]
//        val builder = FirebaseDynamicLinks.getInstance()
//                .createDynamicLink()
//                .setDomainUriPrefix(uriPrefix)
//                .setAndroidParameters(DynamicLink.AndroidParameters.Builder()
//                        .setMinimumVersion(minVersion)
//                        .build())
//                .setLink(deepLink)
//
//        // Build the dynamic link
//        val link = builder.buildDynamicLink()
//        // [END build_dynamic_link]
//        Log.e("tetootadeeplink", link.toString())
//        // Return the dynamic link as a URI
//        return link.uri
//    }
//
//    private fun shareDeepLink(deepLink: String) {
//        val intent = Intent(Intent.ACTION_SEND)
//        intent.type = "text/plain"
//        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link")
//        intent.putExtra(Intent.EXTRA_TEXT, deepLink)
//
//        startActivity(intent)
//    }
//
//    private fun validateAppCode() {
//        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)
//        if (uriPrefix.contains("https://tetoota.page.link")) {
//            AlertDialog.Builder(applicationContext)
//                    .setTitle("Invalid Configuration")
//                    .setMessage("Please set your Dynamic Links domain in app/build.gradle")
//                    .setPositiveButton(android.R.string.ok, null)
//                    .create().show()
//        }
//    }

}
