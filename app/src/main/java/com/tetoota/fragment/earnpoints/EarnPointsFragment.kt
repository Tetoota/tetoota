package com.tetoota.fragment.earnpoints

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.*
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.addrequest.BottomSheetDialog
import com.tetoota.addrequest.TradingDataResponse
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.couponcode.CouponCodeActivity
import com.tetoota.fragment.invite_friends.InviteFriendsFragment
import com.tetoota.fragment.postonsocialmedia.PostSocialMedia
import com.tetoota.fragment.profile.OptionalFeldByUser.OptionalProfileUser
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.fragment.reviewrating.ReviewRatingActivity
import com.tetoota.listener.IFragmentOpenCloseListener
import com.tetoota.proposal.ProposalsContract
import com.tetoota.proposal.ProposalsPresenter
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.earn_points.*

/**
 * Created by jitendra.nandiya on 14-08-2017.
 */
class EarnPointsFragment : BaseFragment(), View.OnClickListener, ProfileCompletionDialog.IDialogListener,
        BottomSheetDialog.IBottomSheetListener, ProposalsContract.View, IFragmentOpenCloseListener {
    override fun onFoodListUnListSuccessResult(message: String?) {
    }

    override fun onFoodCompleteSuccessResult(message: String?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.earn_points, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
    }

    private fun initViews() {
        rl_earnTetootaPoints.setOnClickListener(this)
        rl_completeProfile.setOnClickListener(this)
        rl_postonsocial.setOnClickListener(this)
        rl_rating.setOnClickListener(this)
        rl_redeemCode.setOnClickListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMultiLanguageText()
    }

    /*override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        activity.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }*/

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    companion object {
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        var iFragmentOpenCloseListener: IFragmentOpenCloseListener? = null

        fun newMainIntent(context: Context, iFragmentOpenCloseListenerLocal: IFragmentOpenCloseListener): EarnPointsFragment {
//            val intent = Intent(context, EarnPointsFragment::class.java).apply {
//                iFragmentOpenCloseListener = iFragmentOpenCloseListenerLocal
//                flags = Intent.FLAG_ACTIVITY_NEW_TASK
//                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            }
//            return intent
            iFragmentOpenCloseListener = iFragmentOpenCloseListenerLocal
            val fragment = EarnPointsFragment()
            return fragment
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onClick(v: View?) {
        when (v) {
            rl_earnTetootaPoints -> {
                openInviteFriendsFragment()
            }
            rl_completeProfile -> {
                activity?.let {
                    val intent = Intent(it, OptionalProfileUser::class.java)
                    it.startActivity(intent)
                }

//                ProfileCompletionDialog(context, Constant.DIALOG_LOGIN_FAILURE_ALERT,
//                        this@EarnPointsFragment, getString(R.string.err_msg_mobile_number_limit)).show()
            }
            rl_postonsocial -> {
                openPostSocialMediaFragment()
            }
            rl_rating -> {
                openReviewRatingActivity()
            }
            rl_redeemCode -> {
                /*   //Log.i(javaClass.name, "==================")
                   val mInviteFriendsFragment = CouponCodeFragment.newInstance()
                   activity!!.supportFragmentManager.inTransaction {
                       add(R.id.frame, mInviteFriendsFragment, "invite_friends")
                       addToBackStack("EarnPointsFragment")
                   }
   */
                CouponActivity()
            }
        }
    }


    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out)
        fragmentTransaction.func()
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commitAllowingStateLoss()
    }

    private fun openInviteFriendsFragment() {
//        val mInviteFriendsFragment =
//                InviteFriendsFragment.newInstance("key", "earnTetootaPoints", this@EarnPointsFragment)
//        (activity as MainActivity).navItemIndex = 3
//        (activity as MainActivity).supportFragmentManager.inTransaction { replace(R.id.frame, mInviteFriendsFragment, "invite_friends") }

        val intent = InviteFriendsFragment.newMainIntent(this.activity!!)
        ActivityStack.getInstance(this.activity!!)
        startActivity(intent)

    }

    private fun openPostSocialMediaFragment() {
        val intent = PostSocialMedia.newMainIntent(this.activity!!)
        ActivityStack.getInstance(this.activity!!)
        startActivity(intent)

    }

    private fun openReviewRatingActivity() {
        val intent = ReviewRatingActivity.newMainIntent(this.activity!!)
        ActivityStack.getInstance(this.activity!!)
        startActivity(intent)

    }

    private fun CouponActivity() {
        val intent = CouponCodeActivity.newMainIntent(this.activity!!)
        ActivityStack.getInstance(this.activity!!)
        startActivity(intent)

    }



    override fun dataClick(mTredingDataResponse: Any, type: String) {
        if (type == "category") {
            val mTradingData = mTredingDataResponse as String

        } else {
            val mTradingData = mTredingDataResponse as TradingDataResponse
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        super.onPrepareOptionsMenu(menu)
    }

    override fun onProfileData(param: String, message: String) {
        if (message == "Yes") {
            if (Utils.haveNetworkConnection(this.context!!)) {
                val intent = ProfileDetailActivity.newMainIntent(this.context!!)
                ActivityStack.getInstance(this.context!!)
                startActivity(intent)
            } else {
//                showSnackBar(resources.getString(R.string.str_check_internet))
            }
        } else {

        }
    }

    override fun onFragmentOpenClose(fromFragment: String, toFragment: String, tag: String) {
        iFragmentOpenCloseListener!!.onFragmentOpenClose("earnTetootaPoints", "invite_friends", "")
    }

    override fun onProposalsApiSuccessResult(message: String?) {
    }

    override fun onProposalsFailureResult(message: String) {
    }

    private val mReviewPresenter: ProposalsPresenter by lazy {
        ProposalsPresenter(this@EarnPointsFragment)
    }

    private fun setMultiLanguageText() {
        tv_newCouponcode.text = Utils.getText(context, StringConstant.howto_earn_points)
        tv_unlockCoupon.text = Utils.getText(context, StringConstant.etf_Refer_your_friend_and_earn) + " "
        tv_completeProfile.text = Utils.getText(context, StringConstant.str_complete_profile)
        tv_unlockCoupon1.text = Utils.getText(context, StringConstant.post_social_media)
        tv_completeProfile1.text = Utils.getText(context, StringConstant.offer_more)
        tv_redeemCodeProfile1.text = Utils.getText(context, StringConstant.redeem_coupons)
    }
}