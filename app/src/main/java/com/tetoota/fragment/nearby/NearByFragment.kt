package com.tetoota.fragment.nearby

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityOptions
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.annotation.VisibleForTesting
import android.support.v4.app.ActivityCompat
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.animation.AlphaAnimation
import android.widget.PopupMenu
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.ShortDynamicLink
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.customviews.ReportConversationDialog
import com.tetoota.customviews.StartConversationDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.dashboard.HomeContract
import com.tetoota.fragment.dashboard.HomePresenter
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.login.LoginDataResponse
import com.tetoota.service_product.ServiceContract
import com.tetoota.service_product.ServicePresenter
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.floating_add_filter_layout.*
import kotlinx.android.synthetic.main.fragment_nearby.*
import kotlinx.android.synthetic.main.fragment_nearby.loadItemsLayout_recyclerView
import kotlinx.android.synthetic.main.fragment_nearby.progress_view
import kotlinx.android.synthetic.main.fragment_nearby.rl_nrf
import kotlinx.android.synthetic.main.fragment_nearby.view_list
import kotlinx.android.synthetic.main.fragment_wishlist.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

/**
 * Created by charchit.kasliwal on 09-10-2017.
 */
class NearByFragment : BaseFragment(), NearByContract.View, ServiceContract.View, NearByRecyclerAdapter.IAdapterClick,
        NearByRecyclerAdapter.PaginationAdapterCallback,
        ProfileCompletionDialog.IDialogListener,
        StartConversationDialog.IDialogListener, ReportConversationDialog.IDialogListener, HomeContract.View {
    var link: String = "http://tetoota.com/service/?"
    lateinit var shortLink: Uri

    override fun onInvalidTextAPiSuccess(mDataList: List<com.tetoota.fragment.home.model.DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInvalidTextAPiFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
        Utils.hideSoftKeyboard(this.activity!!)
        if (message == "report") {
            showProgressDialog(Utils.getText(context, StringConstant.please_wait))
            mHomePresenter.report(this.activity!!, mServiceId, mMesgDesc)
        }
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            activity!!.toast(message)
        } else {
            activity!!.toast(message)
        }
    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = ProfileDetailActivity.newMainIntent(this.activity!!)
                ActivityStack.getInstance(this.activity!!)
                startActivity(intent)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    /***********************************************/

    var visibleItemCount: Int = 0
    var pastVisiblesItems: Int = 0
    var totalItemCount: Int = 0
    private var intent: Intent? = null
    var exchangePostType: String? = ""
    private val EDITSERVICEPRODUCT_REQUEST: Int = 13
    public var isRefresh: Boolean? = false
    public var isSearch: Boolean? = false
    public var mQuery: String? = null
    var currentPage = 1
    var limit = 10
    /*********************************************/
    private var tetootaApplication: TetootaApplication? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    var mLastLocation: Location? = null
    private val ADDPOST_REQUEST: Int = 14
    private var isLoad = false
    var isListLastPage = false
    private var TOTAL_PAGES = 1
    var menu: Menu? = null
    var linearLayoutManager: LinearLayoutManager? = null

    private lateinit var mNearByRecyclerAdapter: NearByRecyclerAdapter

    private val mNearByPresenter: NearByPresenter by lazy {
        NearByPresenter(this@NearByFragment)
    }

    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@NearByFragment)
    }

    private val mServicePresenter: ServicePresenter by lazy {
        ServicePresenter(this@NearByFragment)
    }

    override fun retryPageLoad() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        val mProductData = cellRow as ServicesDataResponse
        if (mString == "favorite") {
            /*if (Utils.haveNetworkConnection(activity)) {
                if (Utils.haveNetworkConnection(activity)) {
                    if (Utils.haveNetworkConnection(activity)) {
//                        mServicePresenter.markFavorite(activity, mProductData, mAttributeValue, p1)
                    } else {
                        activity.toast(resources.getString(R.string.str_check_internet))
                    }
                } else {
                    activity.toast("Please check Internet connetivity")
                }
            } else {
                activity.toast("Please check Internet connetivity")
            }*/
            showPopupMenu(mView, mProductData, mAttributeValue, p1)
        } else if (mString == "cellClick") {
            activity!!.toast("Under Development")
        } else if (mString == "shareClick") {
        } else if (mString.equals("Cell Item click")) {
            if (Utils.haveNetworkConnection(this!!.activity!!)) {
                val intent = Intent(context, ProductDetailActivity::class.java)
                intent.putExtra("mProductData", mProductData)
                intent.putExtra("productType", "services")
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    val transitionActivityOptions: ActivityOptions =
                            ActivityOptions.makeSceneTransitionAnimation(activity,
                                    mView, "iv_image")
                    //  startActivity(intent, transitionActivityOptions.toBundle())
                    startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST, transitionActivityOptions.toBundle())
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                } else {
                    startActivityForResult(intent, EDITSERVICEPRODUCT_REQUEST)
                    //startActivity(intent)
                    activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                }
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        }
    }

    override fun onNearByApiSuccessResult(mNearByDataList: List<Any?>?, message: String) {
        progress_view.visibility = View.GONE
        enableWindowTouch(this.activity!!)
        hideErrorView()
        loadItemsLayout_recyclerView.visibility = View.GONE
        if (activity != null && NearByFragment() != null) {
            if (mNearByDataList != null && mNearByDataList.size > 0) {
                rl_nrf.visibility = View.GONE
                mNearByRecyclerAdapter.addAll(mNearByDataList as List<ServicesDataResponse?>)
                mNearByRecyclerAdapter.notifyDataSetChanged()
            } else {
                rl_nrf.visibility = View.VISIBLE
            }
        } else {
            rl_nrf.visibility = View.VISIBLE
        }
        isLoad = mNearByDataList!!.size >= 10
    }

    override fun onApiFailureResult(message: String, isServerError: Boolean) {
        if (view != null) {
            isLoad = false
            progress_view.visibility = View.GONE
            enableWindowTouch(this.activity!!)
            loadItemsLayout_recyclerView.visibility = View.GONE
            if (activity != null && NearByFragment() != null) {
                if (isServerError) {
                    isListLastPage = true
                }
                if (TOTAL_PAGES != 1) {
                    showErrorView(message)
                }
            }
        }
    }

    override fun favoriteApiResult(message: String, cellRow: DataItem, p1: Int) {
//        if (cellRow.is_favourite == 0) {
//            cellRow.is_favourite = 1
//            activity?.toast("Mark Favorite")
//        } else {
//            cellRow.is_favourite = 0
//            activity?.toast("Mark Unfavorite")
//        }
        mNearByRecyclerAdapter.notifyItemChanged(p1)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        validateAppCode()
//        buildGoogleApiClient()
        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_nearby, container, false)
        tetootaApplication = context!!.applicationContext as TetootaApplication
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (view != null) {
//            locationEnable(activity, mLocationRequest!!, mGoogleApiClient!!)
            initViews(view)
            setMultiLanguageText()
//            progress_view.visibility = View.VISIBLE
            getLastLocation()
            setAlpha()
            /*   var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
               toolTitle.visibility = View.VISIBLE
               toolTitle.text = "Tetoota"
   */
            ll_addpost.onClick {
                //                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
//                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//                if (personData.complete_percentage!!.toInt() < 50) {
//                    ProfileCompletionDialog(activity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
//                            this@NearByFragment, getString(R.string.err_msg_mobile_number_limit)).show()
//                } else {
//                    val intent = AddPostRequestActivity.newMainIntent(this!!.activity!!)
//                    intent!!.putExtra("Tab", "serviceTab")
//                    ActivityStack.getInstance(this!!.activity!!)
////                    startActivity(intent)
//                    startActivityForResult(intent, ADDPOST_REQUEST)
//                }
            }
        }
    }

    /**
     * Method to Share Product
     * TODO Deep linking
     */

    fun shareService(mTitle: String, mImageUrl: String) {
        try {
            val text = mTitle
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, text)
            //     shareIntent.type = "image/*"
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
//            shareIntent.type = "image/*"
//            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            //  startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
            startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: android.content.ActivityNotFoundException) {
        }
    }

/*
    fun shareProduct(mTitle: String, mImageUrl: String) {
        val text = mTitle
        val pictureUri = Uri.parse(mImageUrl)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share images..."))
    }
*/*/

    /**
     * Method To Create Popup Menu
     * for share, Favorite, Report
     */
    private fun showPopupMenu(view: View, mProductData: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val popup = PopupMenu(activity, view)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        if (mAttributeValue == "0") {
            favItem?.title = Utils.getText(context, StringConstant.home_unfavourite)
        } else {
            favItem?.title = Utils.getText(context, StringConstant.favorite)
        }
        menu?.findItem(R.id.menu_share)!!.title = Utils.getText(context, StringConstant.share_text)
        menu?.findItem(R.id.menu_report_this)!!.title = Utils.getText(context, StringConstant.report_this)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
//                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
//                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//                    val postString: String = personData.first_name +
//                            " is offering " + '"' + mProductData.title!! + '"' + " on \n http://tetoota.com/app"
//                    var url: String = "";
//                    if (mProductData.image != null && mProductData.image_thumb != null) {
//                        url = Utils.getUrl(context, mProductData.image, mProductData.image_thumb, false)
//                    } else {
//                        url = Utils.getUrl(context, mProductData.image!!)
//                    }
//                    // shareProduct(mProductData.title!!, url)
//                    shareService(postString, url)


                    val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
                    val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                    val postString: String = personData.first_name + " is offering " + '"' + mProductData?.title!! + '"' + " on http://tetoota.com"
                    var url: String = "";
                    if (mProductData!!.image != null && mProductData!!.image_thumb != null) {
                        url = Utils.getUrl(activity, mProductData!!.image!!, mProductData!!.image_thumb!!, false)
                    } else {
                        url = Utils.getUrl(activity, mProductData!!.image!!)
                    }
//                    shareService(postString, url)
                    var gson = Gson()
                    var jsonString = gson.toJson(mProductData)
                    Log.e("jsonString", jsonString + "");
                    val newDeepLink = buildDeepLink(link + jsonString, postString, Constant.ANDROID_MIN_VERSION)


                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this.activity!!)) {
                        mServicePresenter.markFavorite(this.activity!!, mProductData, mAttributeValue, p1)
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    ReportConversationDialog(context, Constant.DIALOG_LOGIN_FAILURE_ALERT, mProductData.id.toString(),
                            this@NearByFragment, getString(R.string.str_report_issue)).show()
                }
            }
            true
        }
        popup.show()
    }

    private fun setMultiLanguageText() {
        tv_add.text = Utils.getText(context, StringConstant.str_addpost)
        filter.text = Utils.getText(context, StringConstant.str_filter)
    }

    private fun setAlpha() {
        val alpha = AlphaAnimation(0.5f, 0.5f) // change values as you want
        alpha.setDuration(0) // Make animation instant
        alpha.setFillAfter(true) // Tell it to persist after the animation ends
        filter.startAnimation(alpha)
        tv_add.startAnimation(alpha)
        iv_filter.startAnimation(alpha)
        iv_add.startAnimation(alpha)
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient?.lastLocation?.addOnCompleteListener(this.activity!!) { task ->
                if (task.isSuccessful && task.result != null) {
                    mLastLocation = task.result
                    Utils.savePreferences(Constant.USER_LOC_LAT, mLastLocation?.latitude.toString(), this.activity!!)
                    Utils.savePreferences(Constant.USER_LOC_LONG, mLastLocation?.longitude.toString(), this.activity!!)
//                if(ServiceFragment.value == "haveUserId"){
//                    fetchWishListData(currentPage!!, false)
//                }else{
//                    fetchAllServiceData(currentPage!!, false)
//                }
                    fetchNearByData()
                } else {
                    //Log.w(TAG, "getLastLocation:exception", task.exception)
                }
            }
        } else {
            fetchNearByData()
        }
    }

    /**
     *
     * @param view the view
     */
    private fun initViews(view: View) {
        setupRecyclerView()
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            view_list.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    public fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            view_list.visibility = View.GONE
            err_title.visibility = View.GONE
        }
    }

    private fun setupRecyclerView() {
        view_list.setHasFixedSize(true)
        view_list.itemAnimator = DefaultItemAnimator()
        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view_list.layoutManager = linearLayoutManager
        mNearByRecyclerAdapter = NearByRecyclerAdapter(activity!!, iAdapterClickListener = this,
                iPaginationAdapterCallback = this,
                screenHeight = heightCalculation())
        view_list.adapter = mNearByRecyclerAdapter

        view_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                visibleItemCount = linearLayoutManager?.getChildCount()!!
                totalItemCount = linearLayoutManager?.getItemCount()!!
                pastVisiblesItems = linearLayoutManager?.findFirstVisibleItemPosition()!!
                var lastVisiblesItems = linearLayoutManager?.findLastVisibleItemPosition()!!
                if (lastVisiblesItems == totalItemCount - 1 && visibleItemCount + pastVisiblesItems >= totalItemCount && isLoad) {
                    currentPage += 1
                    Log.e(javaClass.name, "currentPage" + currentPage)
                    loadItemsLayout_recyclerView.visibility = View.VISIBLE
                    getLastLocation()
                    isLoad = false
                }
            }
        })
    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .2f).toInt()
        return getTopMarginH
    }

    public fun fetchNearByData() {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            if (!isLoad)
                progress_view.visibility = View.VISIBLE
            //   var lat = Utils.loadPrefrence(Constant.USER_LOC_LAT, "0.0", context)
            //  var long = Utils.loadPrefrence(Constant.USER_LOC_LONG, "0.0", context)
            //  mNearByPresenter.getNearByData(this!!.activity!!, lat.toDouble(), long.toDouble())
            mNearByPresenter.getNearByData(this.activity!!, tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude, currentPage)
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            progress_view.visibility = View.GONE
        }
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (view != null) {
            if (cellRow.is_favourite == 0) {
                cellRow.is_favourite = 1
                activity?.toast("Marked Favorite")
            } else {
                cellRow.is_favourite = 0
                activity?.toast("Marked Unfavorite")
            }
            mNearByRecyclerAdapter.notifyItemChanged(p1)
        }
    }

    /***************************************/

    fun clearList() {
        mNearByRecyclerAdapter.removeAll()
        mNearByRecyclerAdapter.notifyDataSetChanged()
    }

    public fun refreshData() {
        currentPage = 1
        isListLastPage = false
        clearList()
        fetchNearByData()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EDITSERVICEPRODUCT_REQUEST) {
                refreshData()
            }
        } else {
            refreshData()
        }
    }

    private fun validateAppCode() {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)
        if (uriPrefix.contains("YOUR_APP")) {
            AlertDialog.Builder(activity)
                    .setTitle("Invalid Configuration")
                    .setMessage("Please set your Dynamic Links domain in app/build.gradle")
                    .setPositiveButton(android.R.string.ok, null)
                    .create().show()
        }
    }

    /**
     * Build a Firebase Dynamic Link.
     * https://firebase.google.com/docs/dynamic-links/android/create#create-a-dynamic-link-from-parameters
     *
     * @param deepLink the deep link your app will open. This link must be a valid URL and use the
     * HTTP or HTTPS scheme.
     * @param minVersion the `versionCode` of the minimum version of your app that can open
     * the deep link. If the installed app is an older version, the user is taken
     * to the Play store to upgrade the app. Pass 0 if you do not
     * require a minimum version.
     * @return a [Uri] representing a properly formed deep link.
     */
    @VisibleForTesting
    fun buildDeepLink(jsonData: String, postString: String, minVersion: Int): Uri {
        val uriPrefix = getString(R.string.dynamic_links_uri_prefix)

        // Set dynamic link parameters:
        //  * URI prefix (required)
        //  * Android Parameters (required)
        //  * Deep link
        // [START build_dynamic_link]
        val builder = FirebaseDynamicLinks.getInstance()
                .createDynamicLink()
                .setDomainUriPrefix(uriPrefix)
                .setAndroidParameters(DynamicLink.AndroidParameters.Builder()
                        .setMinimumVersion(minVersion)
                        .build())
                .setIosParameters(
                        DynamicLink.IosParameters.Builder("com.tetoota")
                                .setAppStoreId("1402450812")
                                .setMinimumVersion("1.2")
                                .build())
                .setLink(Uri.parse(jsonData))

        // Build the dynamic link
        val link = builder
//                .buildDynamicLink()
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT)
                .addOnSuccessListener { result ->
                    // Short link created
                    shortLink = result.shortLink
                    val flowchartLink = result.previewLink
                    Log.e("flowchartLink", "" + shortLink)
                    shareService(activity, postString, shortLink.toString())
                }.addOnFailureListener {
                    Log.e("addOnFailureListener", "FAIL")
                }
        shareService(activity, postString, link.toString())

        // [END build_dynamic_link]
        // Return the dynamic link as a URI
        return Uri.parse("")
    }

    private fun shareDeepLink(deepLink: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link")
        intent.putExtra(Intent.EXTRA_TEXT, deepLink)

        startActivity(intent)
    }


    fun shareService(context: Context?, postString: String, deepLink: String) {
        mProgressDialog?.dismiss()
        try {
//            var ref = tv_referralCode.text.toString().trim()
//            var newUrl = inviteFriend_text.replace("@", ref)
//            val text = newUrl + "\n" + deepLink
//            Log.e(InviteFriendsFragment.TAG, ref + "\n\n" + "newUrl" + inviteFriend_text.replace("@", ref) + "\n\n" + text)
//            val urlToShare = StringConstant.app_link
            var imageUri: Uri? = null
            try {
                imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context!!.contentResolver, BitmapFactory.decodeResource(context.getResources(), R.drawable.share_social),
                        null, null));
            } catch (e: NullPointerException) {
            }
            //  Log.e("imageUri - ","" + imageUri)

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setType("text/plain");
//            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.putExtra(Intent.EXTRA_TEXT, postString + "\n" + deepLink)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
            //  shareIntent.type = "image/*"
            // shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(Intent.createChooser(shareIntent, "Share images..."), 100)
//            context?.startActivity(Intent.createChooser(shareIntent, "Share images..."))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    /*********************************************/
    override fun onWishListApiSuccessResult(mCategoriesList: List<Any?>?, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mNearByPresenter.cancelRequest(this.activity!!)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<com.tetoota.fragment.home.model.DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
