package com.tetoota.fragment.nearby

import android.app.Activity
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.service_product.UserServiceResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jitendra.nandiya on 07-11-2017.
 */
class NearByInteractor {
    var call : Call<UserServiceResponse>? = null
    var mServiceApiListener: NearByContract.serviceApiListener

    constructor(mServiceApiListener: NearByContract.serviceApiListener) {
        this.mServiceApiListener = mServiceApiListener
    }

    @Synchronized
    fun getNearByList(mActivity: Activity, latitude: Double?, longitude: Double?,pageNumber:Int?) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        call = TetootaApplication.getHeader()
                .getNearByList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        personData.user_id!!,latitude!!, longitude!!,pageNumber!!)
        call!!.enqueue(object : Callback<UserServiceResponse> {
            override fun onResponse(call: Call<UserServiceResponse>?,
                                    response: Response<UserServiceResponse>?) {
                var mDashboardSliderData: UserServiceResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        if (mDashboardSliderData != null) {
                            mServiceApiListener.onNearByApiSuccess(mDashboardSliderData.data, "wishlist")
                        }
                    } else {
                        mServiceApiListener.onApiFailure(response.body()?.meta?.message.toString(), false)
                    }
                } else {
                    mServiceApiListener.onApiFailure(response?.body()?.meta?.message.toString(), true)
                }
            }

            override fun onFailure(call: Call<UserServiceResponse>?, t: Throwable?) {
                mServiceApiListener.onApiFailure(t?.message.toString(), true)
            }
        })
    }

    fun cancelREquest(mActivity: Activity) {
            call?.cancel()
    }
}