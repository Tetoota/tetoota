package com.tetoota.fragment.nearby

import android.app.Activity
/**
 * Created by jitendra.nandiya on 07-11-2017.
 */
class NearByPresenter : NearByContract.Presenter, NearByContract.serviceApiListener{
    var mNearByContract: NearByContract.View
    private val mNearByInteractor : NearByInteractor by lazy {
        NearByInteractor(this)
    }

    constructor(mNearByContract: NearByContract.View) {
        this.mNearByContract = mNearByContract
    }

    override fun getNearByData(mActivity: Activity) {
        // mNearByInteractor.getWishListdata(mActivity)
    }

    override fun cancelRequest(mActivity: Activity) {
        mNearByInteractor.cancelREquest(mActivity)
    }

    override fun onNearByApiFailure(message: String, isServerError: Boolean) {
        super.onNearByApiFailure(message, isServerError)
        mNearByContract.onNearByApiFailure(message,isServerError)
    }

    /**
     * the mActivity
     * the user post type "Services"
     */
//    override fun getServicesData(mActivity: Activity, postType: String, pagination : Int?) {
//        mNearByInteractor.getServiceList(mActivity,postType,pagination)
//    }

    override fun getNearByData(mActivity: Activity, letitude : Double?, longitude : Double?,pageNumber:Int?) {
        mNearByInteractor.getNearByList(mActivity, letitude , longitude,pageNumber)
    }

    override fun onNearByApiSuccess(mDataList: List<Any?>?, message: String) {
        mNearByContract.onNearByApiSuccessResult(mDataList,message)
    }

    override fun onFavoriteApiSuccess(message: String, cellRow: DataItem, p1: Int) {
        mNearByContract.favoriteApiResult(message,cellRow,p1)
    }

    override fun onApiFailure(message: String, isServerError: Boolean) {
        mNearByContract.onApiFailureResult(message, isServerError)
    }

    /*override fun markFavorite(mActivity: Activity, cellRow : ServicesDataResponse, mAttributeValue : String, p1 : Int) {
        mNearByInteractor.markFavorites(mActivity,cellRow,mAttributeValue,p1)
    }*/
}