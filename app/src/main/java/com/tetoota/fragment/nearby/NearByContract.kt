package com.tetoota.fragment.nearby

import android.app.Activity

/**
 * Created by jitendra.nandiya on 07-11-2017.
 */
class NearByContract {
    interface View {
        fun onNearByApiSuccessResult(mNearByDataList: List<Any?>?, message: String)
        fun onNearByApiFailure(message: String, isServerError: Boolean) {}
        fun onApiFailureResult(message: String, isServerError: Boolean)
        fun favoriteApiResult(message: String, cellRow: DataItem, p1: Int)
    }

    internal interface Presenter {
        fun getNearByData(mActivity: Activity)
        fun getNearByData(mActivity: Activity, letitude: Double?, longitude: Double?, pageNumber: Int?)
        fun cancelRequest(mActivity: Activity)
    }

    interface serviceApiListener {
        fun onNearByApiSuccess(mDataList: List<Any?>?, message: String)
        fun onApiFailure(message: String, isServerError: Boolean)
        fun onNearByApiFailure(message: String, isServerError: Boolean) {}
        fun onFavoriteApiSuccess(message: String, cellRow: DataItem, p1: Int)
    }
}