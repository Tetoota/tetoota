package com.tetoota.fragment.nearby

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.maps.model.LatLng
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.fragment.dashboard.ServicesDataResponse
import com.tetoota.service_product.FullImageActivity
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.dashboard_list_row.view.*
import kotlinx.android.synthetic.main.layout_loading_item.view.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

/**
 * Created by jitendra.nandiya on 07-11-2017.
 */
class NearByRecyclerAdapter(val mContext: Context, var mProductList: MutableList<Any?> = ArrayList<Any?>(),
                            var iAdapterClickListener: IAdapterClick,
                            var iPaginationAdapterCallback: PaginationAdapterCallback,
                            var screenHeight: Int)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1
    private var isLoadingAdded = false
    private var retryPageLoad = false
    private var errorMsg: String? = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType === VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.dashboard_list_row, parent, false)
            return ViewHolder(view)
        } else if (viewType === VIEW_TYPE_LOADING) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_loading_item, parent, false)
            return LoadingViewHolder(view)
        }
        // return null
        return onCreateViewHolder(parent, viewType)

    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, p1: Int) {
        if (viewHolder is ViewHolder) {
            val mServiceList = mProductList as List<ServicesDataResponse>
            viewHolder.bindServiceData(viewHolder as ViewHolder?, p1, mServiceList[p1], screenHeight, iAdapterClickListener, mContext)
        } else if (viewHolder is LoadingViewHolder) {
            viewHolder.bindLoadinData(retryPageLoad, errorMsg!!, iPaginationAdapterCallback)
        }
    }

    override fun getItemCount(): Int {
        return if (mProductList == null) 0 else mProductList.size
    }

    fun getTotalRecord(): Int? {
        if (mProductList.size > 0) {
            return (mProductList as List<ServicesDataResponse>)[0].total_record
        } else {
            return -1
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (position == mProductList.size - 1 && isLoadingAdded) VIEW_TYPE_LOADING else VIEW_TYPE_ITEM
    }

    fun setElements(mProductList: MutableList<ServicesDataResponse?>, isOnRefreshLoaded: Boolean) {
        this.mProductList = mProductList as MutableList<Any?>
        notifyDataSetChanged()
    }

    fun addAll(moveResults: List<ServicesDataResponse?>) {
        for (result in moveResults) {
            add(result)
        }
    }

    fun remove(r: ServicesDataResponse) {
        val position = mProductList.indexOf(r)
        if (position > -1) {
            mProductList.remove(position)
            notifyItemRemoved(position)
        }
    }

    fun addLoadingFooter(totalPage: Int) {
        isLoadingAdded = totalPage != 1
        add(ServicesDataResponse())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = mProductList.size - 1
        val result = getItem(position)
        if (result != null) {
            mProductList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): ServicesDataResponse {
        return mProductList[position] as ServicesDataResponse
    }

    private fun add(result: Any?) {
        mProductList.add(result)
        notifyItemInserted(mProductList.size - 1)
    }

    private fun removed(result: Any?) {
        mProductList.remove(result)
        notifyItemRemoved(mProductList.size - 1)
    }

    fun showRetry(show: Boolean, errorMsg: String?) {
        retryPageLoad = show
        notifyItemChanged(mProductList.size - 1)
        if (errorMsg != null) this.errorMsg = errorMsg
    }

    fun removeAll() {
//        for (i : int =0 ; i < mProductList.size ;i++) {
//            mProductList.remove(position)
//            notifyItemRemoved(position)
//        }
//        for (position in mProductList.indices) {
//            mProductList.remove(position)
//            notifyItemRemoved(position)
//        }
        val size = mProductList.size
        if (size > 0) {
//            for (i in 0 until size) {
//                mProductList.remove(0)
//            }
            this.mProductList.clear();
            notifyItemRangeRemoved(0, size)
        }
    }


    private fun getLastPosition() = if (mProductList.lastIndex == -1) 0 else mProductList.lastIndex

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mImageView = view.iv_product_service!!
        val title = view.title!!
        val tvName = view.tvName!!
        val tv_reviews = view.tv_reviews!!
        val rtbProductRating = view.rtbProductRating!!
        val iv_user = view.iv_user!!
        val tv_services_product = view.tv_services_product!!
        val tv_description = view.tv_description!!
        val rl_maincontent = view.rl_maincontent!!
        val iv_three_dot = view.iv_three_dot!!
        val tv_tetoota = view.tv_tetoota!!
        val iv_share = view.iv_share!!
        val tv_distance = view.tv_distance
        val rl_distance = view.rl_distance
        val rl_toplayout = view.rl_toplayout!!
        val tv_user_count = view.tv_user_count!!
        private var tetootaApplication: TetootaApplication? = null
        fun bindServiceData(viewHolder: ViewHolder?, p1: Int, mServiceData: ServicesDataResponse,
                            height: Int, iAdapterClickListener: IAdapterClick, context: Context) {
            tetootaApplication = context.applicationContext as TetootaApplication
            tv_services_product.text = "Products"
            rl_toplayout.visibility = View.GONE
            tv_services_product.visibility = View.GONE
            title.text = mServiceData.title
            tvName.text = mServiceData.user_first_name + " " + mServiceData.user_last_name

            rtbProductRating.rating = mServiceData.reviews!!.toFloat()


          //  tv_reviews.text = "${mServiceData.reviews} " + Utils.getText(context, StringConstant.home_rating_review)
            tv_reviews.text = (mServiceData.reviews.toString())

            if(tv_reviews.text.length>=4)
            {
                tv_reviews.text=tv_reviews.text.substring(0,3)
            }
            //Log.e("e","==================="+ tv_reviews.text)

            tv_user_count.text = ("(" + mServiceData.total_usercount +" "+ Utils.getText(context, StringConstant.home_rating_review) +")")

            tv_tetoota.text = mServiceData.tetoota_points.toString()
            tv_description.text = mServiceData.content
            //   var lat = Utils.loadPrefrence(Constant.USER_LOC_LAT, "0.0", itemView.context)
            //   var long = Utils.loadPrefrence(Constant.USER_LOC_LONG, "0.0", itemView.context)

            if (tetootaApplication!!.myLatitude == 0.0 && tetootaApplication!!.myLongitude == 0.0)
            {
                rl_distance.visibility == View.GONE
            }else
            {
                rl_distance.visibility == View.VISIBLE

                try {
                    tv_distance.text = String.format("%.2f", Utils.checkDistance(LatLng(tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude),
                            LatLng(mServiceData.Lat as Double, mServiceData.Long as Double))) + "Km"
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            //  mImageView.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height + 180)

            if (mServiceData.image!!.isEmpty()) {
                mImageView.setImageResource(R.drawable.queuelist_place_holder);
            } else {

                Picasso.get().load(mServiceData.image).into(mImageView)

            }


/*
            if (mServiceData.image != null && mServiceData.image_thumb != null) {
                mImageView.loadUrl(Utils.getUrl(itemView.context, mServiceData!!.image!!, mServiceData!!.image_thumb!!, false))
            } else{
                mImageView.loadUrl(Utils.getUrl(itemView.context, mServiceData!!.image!!))
            }
*/

            if (mServiceData.profile_image!!.isEmpty()) {
                iv_user.setImageResource(R.drawable.user_placeholder);
            } else {

                Picasso.get().load(mServiceData.profile_image).into(iv_user)

            }

/*
            Glide.with(itemView.context)
                    .load(Utils.getUrl(itemView.context, mServiceData.profile_image!!))
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(iv_user)
*/


            if (mServiceData.profile_image.isEmpty())
            {
               // context.toast("Profile image is not available")

            }else {
                iv_user.onClick {
                    val intent = FullImageActivity.newMainIntent(context)
                    intent!!.putExtra("profile_image", mServiceData.profile_image)
                    ActivityStack.getInstance(context)
                    context.startActivity(intent)

                }
            }

/*
            iv_user.onClick {
                val intent = FullImageActivity.newMainIntent(context)
                intent!!.putExtra("profile_image", mServiceData.profile_image)
                ActivityStack.getInstance(context)
                context.startActivity(intent)

            }
*/

            rl_maincontent.onClick {
                iAdapterClickListener.cellItemClick("service", "Cell Item click", mServiceData, "", rl_maincontent, p1)
            }
            iv_share.onClick {
                iAdapterClickListener.cellItemClick("service", "shareClick", mServiceData, "", mImageView, p1)
            }

            iv_three_dot.onClick {
                if (mServiceData.is_favourite == 1) {
                    iAdapterClickListener.cellItemClick("service", "favorite", mServiceData, "0", iv_three_dot, p1)
                } else {
                    iAdapterClickListener.cellItemClick("service", "favorite", mServiceData, "1", iv_three_dot, p1)
                }
            }
        }

        fun ImageView.loadUrl(url: String) {
            if (url != "") {
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, url))
                        .centerCrop()
                        .placeholder(R.drawable.queuelist_place_holder)
                        .error(R.drawable.queuelist_place_holder)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(mImageView)
            }
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val loadmore_progress = itemView.loadmore_progress!!
        val loadmore_retry = itemView.loadmore_retry!!
        val loadmore_errortxt = itemView.loadmore_errortxt!!
        val loadmore_errorlayout = itemView.loadmore_errorlayout!!

        fun bindLoadinData(retryPageLoad: Boolean, errorMsg: String, mCallback: PaginationAdapterCallback) {
            if (retryPageLoad) {
                loadmore_errorlayout.visibility = View.VISIBLE
                loadmore_progress.visibility = View.GONE
                loadmore_errortxt.text = errorMsg
            } else {
                loadmore_errorlayout.visibility = View.GONE
                loadmore_progress.visibility = View.VISIBLE
            }
            loadmore_retry.onClick {
                // showRetry(false, null,retryPageLoad)
                mCallback.retryPageLoad()
            }
            loadmore_errorlayout.onClick {
                // showRetry(false, null)
                mCallback.retryPageLoad()
            }
        }
    }

    interface IAdapterClick {
        fun cellItemClick(mViewClickType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int): Unit
    }

    interface PaginationAdapterCallback {
        fun retryPageLoad()
    }
}
