package com.tetoota.fragment

//import android.widget.SearchView
import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatTextView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.maps.android.SphericalUtil
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.cropImage.BackableFragment
import com.tetoota.customviews.ProfileCompletionDialog
import com.tetoota.filter.FilterActivity
import com.tetoota.fragment.dashdemo.ViewPagerAdapter
import com.tetoota.fragment.home.HomeFragment
import com.tetoota.fragment.profile.ProfileDetailActivity
import com.tetoota.homescreen.DashboardPagerStateAdapter
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.floating_add_filter_layout.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HomeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 * // TODO API CALL CANCEL IF FRAGMENT CLOSE IN RETROFIT
 */
class DashboardFragment : BackableFragment(), ProfileCompletionDialog.IDialogListener, LocationListener,
        GoogleApiClient.ConnectionCallbacks, HomeFragment.OnChildFragmentInteractionListener,
        GoogleApiClient.OnConnectionFailedListener {
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var currentLatitude: Double = 0.toDouble()
    private var currentLongitude: Double = 0.toDouble()
    private val FILTER_REQUEST: Int = 11;
    internal var oldLattitude = 0.0
    internal var oldLongitude = 0.0
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var screenHeight: Int = 0
    var adapter: DashboardPagerStateAdapter? = null
    var adapter1: ViewPagerAdapter? = null
    var currentTabSelected: Int? = 0
    private val ADDPOST_REQUEST: Int = 14

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    private fun resetFilterData() {
        Utils.savePreferences(Constant.CATEGORY_ID, "", this.context!!)
        Utils.savePreferences(Constant.SUBCATEGORIES_ID, "", this.context!!)
        Utils.savePreferences(Constant.TOPRATED, "no", this.context!!)
        Utils.savePreferences(Constant.CITY, "", this.context!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Body of your click handler
        if (mGoogleApiClient != null) {
            locationEnable(this.activity!!, mLocationRequest!!, mGoogleApiClient!!)
        }
        resetFilterData()

        tab_layout_categories.tabMode = TabLayout.MODE_FIXED
        tab_layout_categories.tabGravity = TabLayout.GRAVITY_CENTER
        tab_layout_categories.setTabTextColors(ContextCompat.getColor(this.activity!!, R.color.color_user_name_text), ContextCompat.getColor(this.activity!!, R.color.color_user_name_text1))
        tab_layout_categories.setSelectedTabIndicatorColor(ContextCompat.getColor(this.activity!!, R.color.color_user_name_text))

        for (category in prepareDummyData()) {
            val tabItem = tab_layout_categories.newTab()
            tabItem.text = category
            tab_layout_categories.addTab(tabItem)
        }
        tab_layout_categories.getTabAt(0)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.all_selector)
        tab_layout_categories.getTabAt(1)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.earn_points_selector)
        tab_layout_categories.getTabAt(2)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.wishlist_selector)
        tab_layout_categories.getTabAt(3)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.nearby_selector)
        tab_layout_categories.getTabAt(4)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.whats_new_selector)

        initViews(view)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }

        if (ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient()
        }
    }

    override fun onResume() {
        super.onResume()
        if (mGoogleApiClient != null) {
            if (!mGoogleApiClient?.isConnected!!) {
                mGoogleApiClient!!.connect()
            }
        }
    }

    /**
     * Initialize the google api client
     */
    @Synchronized
    private fun buildGoogleApiClient() {
        mLocationRequest = LocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this.activity!!)
                .addConnectionCallbacks(this@DashboardFragment)
                .addOnConnectionFailedListener(this@DashboardFragment)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_dashboard, container, false)

        return view
    }

    private fun setAlpha(isFull: Boolean) {
        if (isFull) {
            val alpha = AlphaAnimation(1f, 1f) // change values as you want
            alpha.setDuration(0) // Make animation instant
            alpha.setFillAfter(true) // Tell it to persist after the animation ends
            filter.startAnimation(alpha)
            iv_filter.startAnimation(alpha)
        } else {
            val alpha = AlphaAnimation(0.5f, 0.5f) // change values as you want
            alpha.setDuration(0) // Make animation instant
            alpha.setFillAfter(true) // Tell it to persist after the animation ends
            filter.startAnimation(alpha)
            iv_filter.startAnimation(alpha)
        }
    }

    private fun initViews(view: View?) {
        pagerAdapter(view)
    }

    private fun pagerAdapter(view: View?) {


        home_screen_viewpager.offscreenPageLimit = 4
        adapter = DashboardPagerStateAdapter(context!!, activity!!.supportFragmentManager, tab_layout_categories.tabCount, this@DashboardFragment)
        // adapter = ViewPagerAdapter(context, tab_layout_categories.tabCount, childFragmentManager, this@DashboardFragment)
        home_screen_viewpager.adapter = adapter
        tab_layout_categories.setupWithViewPager(home_screen_viewpager)

        tab_layout_categories.getTabAt(0)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.all_selector)
        tab_layout_categories.getTabAt(1)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.earn_points_selector)
        tab_layout_categories.getTabAt(2)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.wishlist_selector)
        tab_layout_categories.getTabAt(3)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.nearby_selector)
        tab_layout_categories.getTabAt(4)?.icon = ContextCompat.getDrawable(this.activity!!, R.drawable.whats_new_selector)

        //home_screen_viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tab_layout_categories))
        tab_layout_categories.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                println("Selected Tab")
                //  Log.e("onTabSelected ", " = " + tab.position)

                currentTabSelected = tab.position
                try {
                    resetFilterData()
                    refreshListData(tab.position)
                    setTool(tab.position)

                } catch (e: Exception) {
                }

                tab_layout_categories.getTabAt(0)?.icon = ContextCompat.getDrawable(context!!, R.drawable.all_selector)
                tab_layout_categories.getTabAt(1)?.icon = ContextCompat.getDrawable(context!!, R.drawable.earn_points_selector)
                tab_layout_categories.getTabAt(2)?.icon = ContextCompat.getDrawable(context!!, R.drawable.wishlist_selector)
                tab_layout_categories.getTabAt(3)?.icon = ContextCompat.getDrawable(context!!, R.drawable.nearby_selector)
                tab_layout_categories.getTabAt(4)?.icon = ContextCompat.getDrawable(context!!, R.drawable.whats_new_selector)

                if (currentTabSelected == 0) {
                    ll_add.visibility = View.VISIBLE
                    ll_addpost.onClick {
                        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
                        val personData = Gson().fromJson(json, LoginDataResponse::class.java)

                        val intent = AddPostRequestActivity.newMainIntent(requireContext())
                        intent!!.putExtra("Tab", "serviceTab")
                        ActivityStack.getInstance(requireContext())
//                startActivity(intent)
                        activity?.startActivityForResult(intent, ADDPOST_REQUEST)
                        /* if (personData.complete_percentage!!.toInt() < 50) {
                             ProfileCompletionDialog(activity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                                     this@ServiceFragment, getString(R.string.err_msg_mobile_number_limit)).show()
                         } else {
                             val intent = AddPostRequestActivity.newMainIntent(this.activity!!)
                             intent!!.putExtra("Tab", "serviceTab")
                             ActivityStack.getInstance(this.activity!!)
             //                startActivity(intent)
                             activity?.startActivityForResult(intent, ADDPOST_REQUEST)
                         }*/
                    }
                    ll_filter.onClick {
                        val intent = FilterActivity.newMainIntent(requireContext())
                        ActivityStack.getInstance(requireContext())
//            startActivity(intent)
                        startActivityForResult(intent, FILTER_REQUEST)
                    }

                } else {
                    ll_add.visibility = View.GONE
                }
                //  home_screen_viewpager.currentItem = tab.position

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                //   Log.e("onTabUnselected ", " = " + tab.position)
                println("UnSelected Tab")

            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                // Log.e("onTabReselected ", " = " + tab.position)
                println(" ReSelected Tab")
            }
        })
    }

    override fun onBackButtonPressed() {
        if (home_screen_viewpager.getCurrentItem() == 0) {
            activity!!.onBackPressed()
            // Toast.makeText(mContext, "fragment back if", Toast.LENGTH_SHORT).show();
        } else {
            // Toast.makeText(mContext, "fragment back else", Toast.LENGTH_SHORT).show();
            if (fragmentManager!!.getBackStackEntryCount() >= 1) {
                fragmentManager!!.popBackStack()

            }
            setTool(0)
            home_screen_viewpager.adapter = adapter
            adapter!!.notifyDataSetChanged()

        }
    }


    fun setTool(position: Int) {
        if (position == 1 || position == 2 || position == 3) {
            // Log.e("setTool", "if " + position)
            var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
            toolTitle.text = ""
        } else {
            //  Log.e("setTool", "else " + position)
            var toolTitle = activity!!.findViewById<AppCompatTextView>(R.id.toolbar_title) as AppCompatTextView
            toolTitle.text = "Tetoota"

        }
    }


    private fun refreshListData(position: Int) {
        if (position != null) {
            if ((position == 0 && adapter!!.fragmentHome!!.isRefresh!!)) {
                adapter!!.fragmentHome!!.currentPage = 1
                adapter!!.fragmentHome!!.isListLastPage = false
                adapter!!.fragmentHome!!.clearList()
                adapter!!.fragmentHome!!.isRefresh = false
                adapter!!.fragmentHome!!.mQuery = ""
                adapter!!.fragmentHome!!.getAllDashboardList()

            } else if ((position == 1 && adapter!!.fragmentService!!.isRefresh!!) || adapter!!.fragmentService!!.isSearch!!) {
                adapter!!.fragmentService!!.currentPage = 1
                adapter!!.fragmentService!!.isListLastPage = false
                adapter!!.fragmentService!!.clearList()
                adapter!!.fragmentService!!.mQuery = ""
                adapter!!.fragmentService!!.isSearch = false
                adapter!!.fragmentService!!.isRefresh = false
                adapter!!.fragmentService!!.getLastLocation(false)
            } else if ((position == 2 && adapter!!.fragmentProduct!!.isRefresh!!) || adapter!!.fragmentProduct!!.isSearch!!) {
                adapter!!.fragmentProduct!!.currentPage = 1
                adapter!!.fragmentProduct!!.isListLastPage = false
                adapter!!.fragmentProduct!!.clearList()
                adapter!!.fragmentProduct!!.isSearch = false
                adapter!!.fragmentProduct!!.mQuery = ""
                adapter!!.fragmentProduct!!.isRefresh = false
                adapter!!.fragmentProduct!!.callApi(false)
            } else if ((position == 3 && adapter!!.fragmentWishlist!!.isRefresh!!) || adapter!!.fragmentWishlist!!.isSearch!!) {
                adapter!!.fragmentWishlist!!.currentPage = 1
                adapter!!.fragmentWishlist!!.isListLastPage = false
                adapter!!.fragmentWishlist!!.clearList()
                adapter!!.fragmentWishlist!!.isRefresh = false
                adapter!!.fragmentWishlist!!.isSearch = false
                adapter!!.fragmentWishlist!!.mQuery = ""
                adapter!!.fragmentWishlist!!.callWishListApi(false)
            } else if ((position == 4 && adapter!!.fragmentNearBy!!.isRefresh!!)) {
                adapter!!.fragmentNearBy!!.currentPage = 1
                adapter!!.fragmentNearBy!!.isListLastPage = false
                adapter!!.fragmentNearBy!!.clearList()
                adapter!!.fragmentNearBy!!.isRefresh = false
                adapter!!.fragmentNearBy!!.mQuery = ""
                adapter!!.fragmentNearBy!!.fetchNearByData()
            }

        } else {
            Log.e("refreshListData ", "else : ")
        }
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): DashboardFragment {
            val fragment = DashboardFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onProfileData(param: String, message: String) {
        if (message.equals("Yes")) {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                val intent = ProfileDetailActivity.newMainIntent(this.activity!!)
                ActivityStack.getInstance(this.activity!!)
                startActivity(intent)
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        } else {

        }
    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .3f).toInt()
        return getTopMarginH
    }

    private fun prepareDummyData(): ArrayList<String> {
        val mDataList = ArrayList<String>()
//        mDataList.add("${Utils.getText(context, StringConstant.home)}")
//        mDataList.add("${Utils.getText(context, StringConstant.services)}")
//        mDataList.add("${Utils.getText(context, StringConstant.product)}")
//        mDataList.add("${Utils.getText(context, StringConstant.wishlist)}")
//        mDataList.add("${Utils.getText(context, StringConstant.near_by)}")
        mDataList.add("${Utils.getText(context, StringConstant.home)}")
        mDataList.add("${Utils.getText(context, StringConstant.earn_points)}")
        mDataList.add("${Utils.getText(context, StringConstant.wishlist)}")
        mDataList.add("${Utils.getText(context, StringConstant.near_by)}")
        mDataList.add("${Utils.getText(context, StringConstant.whats_new)}")
        return mDataList
    }

    override fun onLocationChanged(location: Location?) {
        currentLatitude = location!!.latitude
        currentLongitude = location.longitude
        val d = SphericalUtil.computeDistanceBetween(
                LatLng(oldLattitude, oldLongitude), LatLng(location.latitude, location.longitude))
        if (d >= 500.00 || oldLattitude == 0.0) {
            oldLattitude = currentLatitude
            oldLongitude = currentLongitude
            //  getAllDashboardList()
        }
    }

    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient?.isConnected!!) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                        this)
                mGoogleApiClient?.disconnect()
            }
        }
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient?.isConnected!!) {
                stopLocationUpdates()
            }
        }
    }

    private fun stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this)
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest.create()
        if (null != activity && ActivityCompat.checkSelfPermission(this.activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    fun locationEnable(context: Activity, mLocationRequest: LocationRequest, mGoogleApiClient: GoogleApiClient) {
        // Setting API to check the GPS on device is enabled or not
        val builder = LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)
        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            //                final LocationSettingsStates = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                    try {
                        //  getAllDashboardList()
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(
                                context,
                                100)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can
            // initialize location requests here.
            // Location settings are not satisfied. However, we have no way
            // to fix the settings so we won't show the dialog.
        }
    }

    override fun messageFromChildToParent(mLastCommitFragment: String) {
        if (mLastCommitFragment == "service") {
            home_screen_viewpager.currentItem = 1
        } else if (mLastCommitFragment == "product") {
            home_screen_viewpager.currentItem = 2
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == FILTER_REQUEST) {
                if (currentTabSelected == 1) {
                    adapter!!.fragmentService!!.isRefresh = true
                } else if (currentTabSelected == 2) {
                    adapter!!.fragmentProduct!!.isRefresh = true
                } else if (currentTabSelected == 3) {
                    adapter!!.fragmentWishlist!!.isRefresh = true
                }
            }
        } else {
            resetFilterData()
        }
    }
}