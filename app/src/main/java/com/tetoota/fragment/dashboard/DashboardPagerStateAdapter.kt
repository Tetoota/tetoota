package com.tetoota.homescreen

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.tetoota.fragment.earnpoints.EarnPointsFragment
import com.tetoota.fragment.home.HomeFragment
import com.tetoota.fragment.nearby.NearByFragment
import com.tetoota.marketplace.MarketPlaceFragment
import com.tetoota.service_product.ProductFragment
import com.tetoota.service_product.ServiceFragment
import com.tetoota.service_product.TrendingFragment
import com.tetoota.service_product.WishlistFragment
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils

/**
 * Created by charchit.kasliwal on 28-08-2017.
 */
class DashboardPagerStateAdapter(var mContext: Context, fm: FragmentManager, var NumOfTabs: Int,
                                 var iChildClickListener: HomeFragment.OnChildFragmentInteractionListener) : FragmentStatePagerAdapter(fm) {
    var fragmentHome: HomeFragment? = null
    var fragmentService: ServiceFragment? = null
    var fragmentEarn: EarnPointsFragment? = null
    var fragmentProduct: ProductFragment? = null
    var fragmentWishlist: WishlistFragment? = null
    var fragmentMarketPlace: MarketPlaceFragment? = null
    var fragmentNearBy: NearByFragment? = null
    var fragmentTrending: TrendingFragment?=null
    override fun getCount(): Int {
        return NumOfTabs
    }

    override fun getItem(position: Int): Fragment? {
        println("Tab Position Adapter $position")
        when (position) {
            0 -> {
                fragmentService = ServiceFragment.newInstance("haveNotUserId", "no", true)
                return fragmentService
//                fragmentHome = HomeFragment()
//                fragmentHome!!.setmListener(iChildClickListener)
////                fragment.setFilterListener(iFilterListener)
//                val tab1 = fragmentHome
//                return tab1
            }
            1 -> {
                fragmentEarn = EarnPointsFragment()
                return fragmentEarn
            }
            2 -> {
                fragmentMarketPlace = MarketPlaceFragment()
                return fragmentMarketPlace
            }
            3 -> {
                fragmentNearBy = NearByFragment()
                return fragmentNearBy
            }
            4 -> {
                fragmentTrending = TrendingFragment.newInstance("Trending", "no", true)
                return fragmentTrending
            }

            else -> return null
        }
    }


    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0) {
            title = Utils.getText(mContext, StringConstant.home)
            // title = "Home1";
        } else if (position == 1) {
            title = Utils.getText(mContext, StringConstant.earn_points)
            // title = "Services1";

        } else if (position == 2) {
            title = Utils.getText(mContext, StringConstant.str_marketplace)
            //title = "Products1";

        } else if (position == 3) {
            title = Utils.getText(mContext, StringConstant.near_by)
            // title = "Wishlist1";

        } else if (position == 4) {
            title = Utils.getText(mContext, StringConstant.str_whats_new)
            //title = "Nearby1";

        }
        return title
    }


    var mFilterListener: OnFilterAppliedListener? = null

    interface OnFilterAppliedListener {
        fun filterData(mTopRated: String, mCity: String)
    }

    fun updateFreagment() {
        this.notifyDataSetChanged()
    }
}