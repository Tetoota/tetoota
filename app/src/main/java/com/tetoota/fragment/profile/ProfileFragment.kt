package com.tetoota.fragment.profile

//import kotlinx.android.synthetic.main.nav_header_layout.*
import android.app.AlertDialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.annotation.VisibleForTesting
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.*
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.customviews.CustomSignOutDialog
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.couponcode.Meta
import com.tetoota.fragment.favorites.FavoritesFragment
import com.tetoota.fragment.invite_friends.InviteFriendsFragment
import com.tetoota.listener.IFragmentOpenCloseListener
import com.tetoota.login.LoginDataResponse
import com.tetoota.logout.LogoutDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.message.ServicesTabLayout
import com.tetoota.pointssummary.PointsSummaryActivity
import com.tetoota.reviews.ViewReviewsActivity
import com.tetoota.selectlanguage.SelectLanguageActivity
import com.tetoota.service_product.FullImageActivity
import com.tetoota.service_product.ServiceProductActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ProfileFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : BaseFragment(), View.OnClickListener, CustomSignOutDialog.IDialogListener, IFragmentOpenCloseListener {
    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    var getScreenHeight: Int = 0
    private lateinit var mView: View
    var register: MenuItem? = null
    private var tetootaApplication: TetootaApplication? = null
//    var iFragmentOpenCloseListener: IFragmentOpenCloseListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
        initToolBar()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        super.onPrepareOptionsMenu(menu)
    }

    fun initToolBar() {
        setHasOptionsMenu(true)
        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.setDisplayShowCustomEnabled(false) //disable a custom view inside the actionbar
        actionBar?.setDisplayShowTitleEnabled(false)
    }


    /*override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        activity.menuInflater.inflate(R.menu.dashboard_menu, menu)
        menu?.findItem(R.id.action_search)?.isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }*/

    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
        tv_pts_summary.text = Utils.getText(context, StringConstant.point_summary)
        tv_signout.text = Utils.getText(context, StringConstant.sign_out)
        tv_invites.text = Utils.getText(context, StringConstant.invite_friends)
        tv_reviews.text = Utils.getText(context, StringConstant.my_reviews)
        tv_favorites.text = Utils.getText(context, StringConstant.favourite)
        tv_editprofile.text = Utils.getText(context, StringConstant.edit_profile)
        my_service_product.text = Utils.getText(context, StringConstant.my_services)
        tv_wishlist.text = Utils.getText(context, StringConstant.my_wishlist)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        tetootaApplication = context?.getApplicationContext() as TetootaApplication
        this.mView = view!!
        return view
    }

    override fun onResume() {
        super.onResume()
        setDataOnUi()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        enableWindowTouch(this.activity!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        enableWindowTouch(this.activity!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ll_favorite.setOnClickListener(this@ProfileFragment)
        ll_history.setOnClickListener(this@ProfileFragment)
        ll_reviews.setOnClickListener(this@ProfileFragment)
        ll_services_product.setOnClickListener(this@ProfileFragment)
        ll_wishlist.setOnClickListener(this@ProfileFragment)
        tv_editprofile.setOnClickListener(this@ProfileFragment)
        tv_signout.setOnClickListener(this@ProfileFragment)
        ll_invites.setOnClickListener(this@ProfileFragment)
        //  iv_profile.setOnClickListener(this@ProfileFragment)
        // Start a coroutine
        setMultiLanguageText()
        progress_view.visibility = View.VISIBLE
        disableWindowTouch(this.activity!!)
        try {
            val pInfo = activity!!.packageManager.getPackageInfo(activity!!.packageName, 0)
            val version = pInfo.versionName
            tv_version.text = "Version " + version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun setDataOnUi() {
        try {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", activity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            if (personData.review_avg != null) {
                try {
                    /*
         * For custom color only using layerdrawable to fill the star colors
         */
                    /*val stars = user_reviews
                            .getProgressDrawable() as LayerDrawable
                    stars.getDrawable(2).setColorFilter(Color.parseColor("#0BB807"),
                            PorterDuff.Mode.SRC_ATOP) // for filled stars
                    stars.getDrawable(1).setColorFilter(Color.parseColor("#fff000"),
                            PorterDuff.Mode.SRC_ATOP) // for half filled stars
                    stars.getDrawable(0).setColorFilter(Color.parseColor("#DADADA"),
                            PorterDuff.Mode.SRC_ATOP) // for empty stars*/
                    user_reviews.rating = personData.review_avg.toFloat()
//                    user_reviews.rating = 3.5f
                } catch (e: Exception) {
                    println("Crash")
                    e.printStackTrace()
                }

            }
            if (personData.first_name != "" || personData.last_name != "") {
                tv_name.text = "${personData.first_name} ${personData.last_name}"
            } else {
                tv_name.visibility = View.GONE
            }
            if (personData.designation != "") {
                tv_designation.text = personData.designation
            } else {
                tv_designation.visibility = View.GONE
            }
            if (personData.review_avg != "") {

                tv_review_avg.text = personData.review_avg?.toFloat().toString()
            } else {
                tv_review_avg.visibility = View.GONE
            }

            if (personData.complete_percentage != "" || personData.complete_percentage != null) {
                personData.complete_percentage?.toFloat()!! / 100
                tv_complete_per.text = "${personData.complete_percentage}%"
                holoCircularProgressBar.progress = personData.complete_percentage.toFloat() / 100
            } else {
                tv_complete_per.text = "0%"
                holoCircularProgressBar.progress = 0f
            }


/*
            Glide.with(activity)
                    .load(Utils.getUrl(activity, personData.profile_image!!))
                    .placeholder(R.drawable.user_placeholder)
                    .centerCrop()
                    .into(iv_profile)
*/

            if (personData.profile_image!!.isEmpty()) {
                iv_profile.setImageResource(R.drawable.user_placeholder);
            } else {

                Picasso.get().load(personData.profile_image).into(iv_profile)
            }

            if (personData.profile_image!!.isEmpty()) {
                //  activity!!.toast("Profile image is not available")

            } else {
                iv_profile.onClick {
                    val intent = FullImageActivity.newMainIntent(this.activity!!)
                    intent!!.putExtra("profile_image", personData.profile_image)
                    ActivityStack.getInstance(this.activity!!)
                    startActivity(intent)
                }
            }
            progress_view.visibility = View.GONE
            enableWindowTouch(this.activity!!)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        var iFragmentOpenCloseListener: IFragmentOpenCloseListener? = null
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment ProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String, iFragmentOpenCloseListenerLocal: IFragmentOpenCloseListener): ProfileFragment {
            iFragmentOpenCloseListener = iFragmentOpenCloseListenerLocal
            val fragment = ProfileFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            ll_favorite -> {
                openFavoritesFragment()
            }
            ll_reviews -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    tetootaApplication!!.isCheck = "AllReview"
                    val intent = ViewReviewsActivity.newMainIntent(this.activity!!)
                    ActivityStack.getInstance(this.activity!!)
                    startActivity(intent)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
            ll_history -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = PointsSummaryActivity.newMainIntent(this.activity!!)
                    ActivityStack.getInstance(this.activity!!)
                    startActivity(intent)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
            ll_services_product -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = ServiceProductActivity.newMainIntent(this.activity!!)
                    intent?.putExtra("Tab", "serviceTab")
                    intent?.putExtra("HideReport", "HideReport")
                    ActivityStack.getInstance(this.activity!!)
//                    startActivity(intent)
                    startActivityForResult(intent, 13)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
            ll_wishlist -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = ServiceProductActivity.newMainIntent(this.activity!!)
                    intent?.putExtra("Tab", "wishlistTab")
                    ActivityStack.getInstance(this.activity!!)
//                    startActivity(intent)
                    startActivityForResult(intent, 13)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
            tv_editprofile -> {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = ProfileDetailActivity.newMainIntent(this.activity!!)
                    ActivityStack.getInstance(this.activity!!)
                    startActivity(intent)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
            tv_signout -> {
                CustomSignOutDialog(activity, Constant.DIALOG_LOGIN_FAILURE_ALERT,
                        this@ProfileFragment, getString(R.string.err_msg_mobile_number_limit)).show()
            }
            ll_invites -> {
                openInviteFriendsFragment()
            }
            else -> {
                println("Id Not Match")
            }
        }
    }

    private fun openFavoritesFragment() {
        if (Utils.haveNetworkConnection(this.activity!!)) {
            val mFavoriteFragment = FavoritesFragment.newInstance("key", "profileScreen", this@ProfileFragment)
         //   (activity as MainActivity).navItemIndex = 3
            (activity as MainActivity).supportFragmentManager.inTransaction { replace(R.id.frame, mFavoriteFragment, "favorites") }
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
        }
    }

    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out)
        fragmentTransaction.func()
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commitAllowingStateLoss()
    }

    private fun openInviteFriendsFragment() {
        val intent = InviteFriendsFragment.newMainIntent(this.activity!!)
        ActivityStack.getInstance(this.activity!!)
        startActivity(intent)
//        val mInviteFriendsFragment =
//                InviteFriendsFragment.newInstance("key", "profileScreen", this@ProfileFragment)
//        (activity as MainActivity).navItemIndex = 3
//        (activity as MainActivity).supportFragmentManager.inTransaction{replace(R.id.frame, mInviteFriendsFragment,"invite_friends")}
    }

    override fun onYes(param: String, message: String) {
        showProgressDialog(Utils.getText(context, StringConstant.please_wait))
        userLogoutApi()

    }

    override fun onFragmentOpenClose(fromFragment: String, toFragment: String, tag: String) {
        if (fromFragment.equals("profile") && toFragment.equals("favorites")) {
            iFragmentOpenCloseListener!!.onFragmentOpenClose("profile", "favorites", "")
        } else if (fromFragment.equals("profile") && toFragment.equals("invite_friends")) {
            iFragmentOpenCloseListener!!.onFragmentOpenClose(fromFragment, toFragment, "")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
     * @param mActivity the m activity
     */
    fun userLogoutApi() {
        var call: Call<LogoutDataResponse> = TetootaApplication.getHeader()
                .userLogout(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", activity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", activity),
                        Utils.loadPrefrence(Constant.USER_ID, "en", activity))
        call.enqueue(object : Callback<LogoutDataResponse> {
            override fun onFailure(call: Call<LogoutDataResponse>?, t: Throwable?) {
                println("On Failure")
                hideProgressDialog()
            }

            override fun onResponse(call: Call<LogoutDataResponse>?, response: Response<LogoutDataResponse>?) {
//                var logoutApiResponse: LogoutDataResponse? = response?.body()
                hideProgressDialog()
                if (response?.code() == 200) {
                    var deviceId: String? = null
                    val notificationManager = activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.cancelAll()
                    //AccountKit.logOut()
                    if (Utils.getPrefFcmToken(activity!!) != null && Utils.getPrefFcmToken(activity!!).length > 0) {
                        deviceId = Utils.getPrefFcmToken(activity!!)
                    } else {
                        deviceId = "Not Found"
                    }
                    Utils.clearAllPrefrences(activity!!)
                    Utils.setFcmToken(deviceId, activity!!)
                    ActivityStack.cleareAll()
                    activity!!.startActivity<SelectLanguageActivity>()
                    activity!!.finish()
                } else if (response?.code() == 401) {
                    if (response?.body() == null) {
                        try {
                            val jObjError = JSONObject(response.errorBody()?.string())
                            val achualdata: JSONObject = jObjError.getJSONObject("meta")
                            val gson = Gson()
                            var mError: Meta = Meta()
                            val error = gson.fromJson(achualdata.toString(), Meta::class.java)
                            activity!!.toast(error.message!!)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        })
    }



}// Required empty public constructor
