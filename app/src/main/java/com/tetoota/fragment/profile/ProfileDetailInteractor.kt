package com.tetoota.fragment.profile

import android.app.Activity
import android.app.Service
import android.util.Log
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

/**
 * Created by charchit.kasliwal on 18-07-2017.
 */
class ProfileDetailInteractor {

    var mprofileApiListener: ProfileDetailContract.profileApiListener

    constructor(mprofileApiListener: ProfileDetailContract.profileApiListener) {
        this.mprofileApiListener = mprofileApiListener
    }

    fun profileDataApiCall(mActivity: Activity, personData: LoginDataResponse) {
        // Log.e("FFFFFFFFFFFFFFF",""+ personData)
        var call: Call<ProfileResponse> = TetootaApplication.getHeader()
                .profileDetailApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        personData.auth_token!!,
                        personData.user_id!!)
        call.enqueue(object : Callback<ProfileResponse> {
            override fun onResponse(call: Call<ProfileResponse>?,
                                    response: Response<ProfileResponse>?) {
                println("Success")
                var mProfileData: ProfileResponse? = response?.body()
                //   Log.e("mProfileDataaaaaa",""+ mProfileData)
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        Utils.savePreferences(Constant.IS_VISIBLE,
                                mProfileData?.data?.get(0)?.is_service.toString(), mActivity)

                        mprofileApiListener.onProfileApISuccess(mProfileData?.data, "userProfileData")

                    } else {
                        mprofileApiListener.onProfileApiFailure(response.body()?.meta?.message.toString(), "profileFailure")
                    }
                } else {
                    mprofileApiListener.onProfileApiFailure(response?.body()?.meta?.message.toString(), "profileFailure")
                }
            }

            override fun onFailure(call: Call<ProfileResponse>?, t: Throwable?) {
                mprofileApiListener.onProfileApiFailure(t?.message.toString(), "profileFailure")
            }
        })
    }


/*
    fun getIncompleteProposalApiCalling(mActivity : Activity, mUserId : String){
       var call = TetootaApplication.getHeader()
                .getIncompleteProposal(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity), mUserId!!)
        call!!.enqueue(object : Callback<ProposalApiResponse> {
            override fun onResponse(call: Call<ProposalApiResponse>?, response: Response<ProposalApiResponse>?) {
                println("Success")
                var mIncompleteProposal : ProposalApiResponse? = response?.body()
                Log.e("@@@@@@",""+ mIncompleteProposal)

                if(response?.code() == 200){
                    if(response.body()?.data != null){
                        if(response.body()?.data?.size!! > 0){
                            Log.e("@@@@@@@@@@@@@@@@@@",""+ mIncompleteProposal)
                            mprofileApiListener.onIncompleteApiSuccess(response.body()!!.data as ArrayList<ProposalMessageData>,"success")
                        }else{
                            mprofileApiListener.onIncompleteApiFailure("No Records Found")
                        }
                    }
                }else{
                    mprofileApiListener.onProfileApiFailure(response?.body()?.meta?.message.toString(),"profileFailure")
                }
            }
            override fun onFailure(call: Call<ProposalApiResponse>?, t: Throwable?) {
                mprofileApiListener.onProfileApiFailure(t?.message.toString(),"profileFailure")
            }
        })
    }
*/


    fun prepareFilePart(mActivity: Activity, partName: String, fileUri: String): MultipartBody.Part {
        //File creating from selected URL
        val file = File(fileUri)
        // create RequestBody instance from file
        val requestFile = RequestBody.create(MediaType.parse("image*//**//*"), file)
        val body = MultipartBody.Part.createFormData("post_image_attached[]", file.name, requestFile)
        // Log.e("*****body*****",""+ body)
        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
    }

    fun updateUserProfileAPI(mActivity: Activity, data: HashMap<String, RequestBody>, mImagePath: String?) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", mActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)

        if (mImagePath.equals("")) {

            var call: Call<ProfileResponse> = TetootaApplication.getHeader().profileUpdateApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                    Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                    personData.auth_token!!, data)
            call!!.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(call: Call<ProfileResponse>?,
                                        response: Response<ProfileResponse>?) {
                    var mProfileData: ProfileResponse? = response?.body()
                    if (response?.code() == 200) {
                        if (response.body()?.data?.size!! > 0) {
                            val json = Gson().toJson(response.body()?.data!![0])
                            Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json, mActivity)
                            mprofileApiListener.onProfileApiFailure(response.body()?.meta?.message.toString(), "profileUpdate")
                        } else {
                            mprofileApiListener.onProfileApiFailure(response.body()?.meta?.message.toString(), "profileFailure")
                        }
                    } else {
                        mprofileApiListener.onProfileApiFailure(response?.body()?.meta?.message.toString(), "profileFailure")
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>?, t: Throwable?) {
                    mprofileApiListener.onProfileApiFailure(t?.message.toString(), "profileFailure")
                }
            })
        } else {
            var call: Call<ProfileResponse> = TetootaApplication.getHeader()
                    .profileUpdateApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                            Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                            personData.auth_token!!, data, prepareFilePart(mActivity, "post_image_attached[]", mImagePath!!))
            call.enqueue(object : Callback<ProfileResponse> {
                override fun onResponse(call: Call<ProfileResponse>?,
                                        response: Response<ProfileResponse>?) {
                    var mProfileData: ProfileResponse? = response?.body()
                    if (response?.code() == 200) {
                        if (response.body()?.data?.size!! > 0) {

                            val json = Gson().toJson(response.body()?.data!![0])
                            Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json, mActivity)
                            //   Log.e("jsonnnnnnnnnnnnnnn",""+ json)
                            //   Log.e("mActivity",""+ mActivity)
                            mprofileApiListener.onProfileApiFailure(response.body()?.meta?.message.toString(), "profileUpdate")
                        } else {
                            mprofileApiListener.onProfileApiFailure(response.body()?.meta?.message.toString(), "profileFailure")
                        }
                    } else {
                        mprofileApiListener.onProfileApiFailure(response?.body()?.meta?.message.toString(), "profileFailure")
                    }
                }

                override fun onFailure(call: Call<ProfileResponse>?, t: Throwable?) {
                    mprofileApiListener.onProfileApiFailure(t?.message.toString(), "profileFailure")
                }
            })
        }
    }

    fun profileDataApiCallForService(mService: Service, personData: LoginDataResponse) {
        var call: Call<ProfileResponse> = TetootaApplication.getHeader()
                .profileDetailApi(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPreference(Constant.USER_SELECTED_LANG, "en", mService),
                        personData.auth_token!!,
                        personData.user_id!!)
        call.enqueue(object : Callback<ProfileResponse> {
            override fun onResponse(call: Call<ProfileResponse>?,
                                    response: Response<ProfileResponse>?) {
                println("Success")
                var mProfileData: ProfileResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        mprofileApiListener.onProfileApISuccess(mProfileData?.data, "userProfileData")
                    } else {
                        mprofileApiListener.onProfileApiFailure(response.body()?.meta?.message.toString(), "profileFailure")
                    }
                } else {
                    mprofileApiListener.onProfileApiFailure(response?.body()?.meta?.message.toString(), "profileFailure")
                }
            }

            override fun onFailure(call: Call<ProfileResponse>?, t: Throwable?) {
                mprofileApiListener.onProfileApiFailure(t?.message.toString(), "profileFailure")
            }
        })
    }

    fun showUserDetailService(mActivity: Activity, mUserId: String) {
        var call: Call<ProfileResponse> = TetootaApplication.getHeader()
                .showUserDetail(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId)
        call.enqueue(object : Callback<ProfileResponse> {
            override fun onResponse(call: Call<ProfileResponse>?,
                                    response: Response<ProfileResponse>?) {
                println("Success")
                var mProfileData: ProfileResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.data?.size!! > 0) {
                        mprofileApiListener.onShowUserApISuccess(mProfileData?.data, "userProfileData")
                    } else {
                        mprofileApiListener.onShowUserApiFailure(response.body()?.meta?.message.toString(), "profileFailure")
                    }
                } else {
                    mprofileApiListener.onShowUserApiFailure(response?.body()?.meta?.message.toString(), "profileFailure")
                }
            }

            override fun onFailure(call: Call<ProfileResponse>?, t: Throwable?) {
                mprofileApiListener.onShowUserApiFailure(t?.message.toString(), "profileFailure")
            }
        })
    }

    fun updateOptionalInformation(mActivity: Activity, mUserId: String, email: String, aadhar_number: String, categories_ids: String) {
        var call: Call<ResponseBody> = TetootaApplication.getHeader()
                .updateOptionalInformation(Constant.CONSTANT_ADMIN,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), mUserId, email, aadhar_number, categories_ids)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>?,
                                    response: Response<ResponseBody>?) {
                Log.d("responseeeeee", response!!.body().toString())
                if (response?.code() == 200) {
                    if (response.isSuccessful) {
                        mprofileApiListener.onOptionalInformationApiSuccessResult("Suceess")
                    } else {
                        mprofileApiListener.onOptionalInformationApiSuccessResult("failure")
                    }
                } else {
                    mprofileApiListener.onOptionalInformationApiSuccessResult("failure")
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                mprofileApiListener.onOptionalInformationApiSuccessResult("failure")
            }
        })
    }

    //---------------------------------------------------------------------------------------------------------------//
    fun profileSendEmailAadhar(mActivity: Activity, mUserId: String, email: String, aadhar_number: String, type: String) {
        var call: Call<ResponseBody> = TetootaApplication.getHeader()
                .verifyEmailAadhar(Constant.CONSTANT_ADMIN,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity), email, mUserId, type, aadhar_number)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>?,
                                    response: Response<ResponseBody>?) {
                Log.d("responseeeeee", "" + Gson().toJson(response?.body()))
                if (response?.code() == 200) {
                    if (response.isSuccessful) {
                        mprofileApiListener.onProfileSendEmailApiSuccessResult("Suceess")
                    } else {
                        mprofileApiListener.onProfileSendEmailApiFailureResult("failure")
                    }
                } else {
                    mprofileApiListener.onProfileSendEmailApiFailureResult("failure")
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                mprofileApiListener.onProfileSendEmailApiFailureResult("failure")
            }
        })
    }
}