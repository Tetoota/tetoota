package com.tetoota.fragment.profile

import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.webkit.URLUtil
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.face.FaceDetector
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.addrequest.AddPostRequestActivity
import com.tetoota.cropImage.CropImage
import com.tetoota.cropImage.InternalStorageContentProvider
import com.tetoota.cropImage.MarshmallowPermission
import com.tetoota.cropImage.Utility
import com.tetoota.login.LoginDataResponse
import com.tetoota.socialmedias.AlertDialogManager
import com.tetoota.socialmedias.ConnectionDetector
import com.tetoota.socialmedias.SocialMediaProfilePojo
import com.tetoota.socialmedias.facebook.FacebookActivity
import com.tetoota.socialmedias.google.GoogleActivity
import com.tetoota.socialmedias.instagram.InstagramCallback
import com.tetoota.socialmedias.instagram.InstagramHandler
import com.tetoota.socialmedias.linkedin.LinkedinActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.activity_profile_detail.*
import kotlinx.android.synthetic.main.activity_profile_detail.edt_service
import kotlinx.android.synthetic.main.activity_profile_detail.ll_text_count
import kotlinx.android.synthetic.main.activity_profile_detail.tv_service
import kotlinx.android.synthetic.main.activity_profile_detail.txt_count
import kotlinx.android.synthetic.main.fragment_add__service.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import okhttp3.RequestBody
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class ProfileDetailActivity : BaseActivity(), ProfileDetailContract.View, View.OnClickListener,
        InstagramCallback {
    override fun onProfileSendEmailApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
    }


    var mImagePath: String? = null
    var register: MenuItem? = null
    private val GOOGLE_LOGIN: Int = 101
    private val FACEBOOK_LOGIN: Int = 103
    private val GOOGLE_LOGOUT: Int = 201
    private val LINKEDIN_LOGOUT: Int = 205
    private val TWITTER_LOGIN: Int = 102
    private val REQUEST_TYPE_LINKEDIN: Int = 105
    var PLACE_AUTOCOMPLETE_REQUEST_CODE = 207
    private var isUserGmailAuth: Boolean = false
    private var isUserLinkedAuth: Boolean = false
    private var isUserInstaAuth: Boolean = false
    private var isUserFbAuth: Boolean = false
    private var isUserWhatAuth: Boolean = true
    private var profileImage: String? = "";

    /***************************************/
    private var context: Context? = null
    val REQUEST_CAMERA_IMAGE = 1034
    private val REQUEST_GALLERY_IMAGE = 1063
    private var photoFile: File? = null
    private var resultUri: Uri? = null
    private var marshmallowPermission: MarshmallowPermission? = null
    private var saveFile: File? = null
    private val mFileTemp = File(Environment.getExternalStorageDirectory().toString() + "/photo.jpg")
    val REQUEST_CODE_GALLERY = 0x1
    val REQUEST_CODE_TAKE_PICTURE = 0x2
    val REQUEST_CODE_CROP_IMAGE = 0x3
    private var encoded = ""
    private var isFace = false
    /****************************************/

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        com.tetoota.fragment.profile.ProfileDetailPresenter(this@ProfileDetailActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_detail)
        setToolbar()
        activityUIIntializer()
        setMultiLanguageText()
        enableEditText()
        context = this;
        marshmallowPermission = MarshmallowPermission(context);

        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)

        if (Utils.haveNetworkConnection(this@ProfileDetailActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ProfileDetailActivity)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileData(this@ProfileDetailActivity, personData)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }

        edt_service.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val length = edt_service.length()
                val convert = length.toString()
                txt_count.setText(convert)
                if (edt_service.length() >= 30) {
                    ll_text_count.visibility = View.VISIBLE
                } else {
                    ll_text_count.visibility = View.VISIBLE
                }


            }

            override fun afterTextChanged(s: Editable) {}
        })


    }

    override fun onStart() {
        super.onStart()
        // Register this fragment to listen to event.
    }

    /**
     * Method To Set Multilanguage TExt
     */
    private fun setMultiLanguageText() {
        tv_firstname.text = Utils.getText(this, StringConstant.first_name)
        edt_name.hint = Utils.getText(this, StringConstant.first_name)
        tv_last.text = Utils.getText(this, StringConstant.last_name)
        edt_last.hint = Utils.getText(this, StringConstant.last_name)
        tv_email.text = Utils.getText(this, StringConstant.email_id)
        edt_email.hint = Utils.getText(this, StringConstant.str_email)
        //  tv_quality.text = Utils.getText(this,StringConstant.str_quality)
        //  tv_response_time.text = Utils.getText(this,StringConstant.str_responsetime)
        //   tv_prefrence.text = Utils.getText(this, StringConstant.str_preference)
        //   edt_prefrences.hint = Utils.getText(this, StringConstant.str_preference)
        tv_personalinfor.text = Utils.getText(this, StringConstant.str_personal_info)
        tv_city.text = Utils.getText(this, StringConstant.str_city)
        edt_city.hint = Utils.getText(this, StringConstant.str_city)
        tv_optionalinfor.text = Utils.getText(this, StringConstant.str_optional_info)
        tv_aadhar.text = Utils.getText(this, StringConstant.aadhar_num)
        edt_aadhar.hint = Utils.getText(this, StringConstant.aadhar_num)
        // tv_friendlines.text = Utils.getText(this,StringConstant.str_friendlines)
        tv_service.text = Utils.getText(this, StringConstant.myprofile_desctext)
        edt_service.hint = Utils.getText(this, StringConstant.myprofile_write_yourself)
        tv_mediaurl.text = Utils.getText(this, StringConstant.myprofile_websitetext)
        tv_address.text = Utils.getText(this, StringConstant.address)
        edt_address.hint = Utils.getText(this, StringConstant.address)
        // tv_badge.text = Utils.getText(this,StringConstant.str_badege)
        tv_contact.text = Utils.getText(this, StringConstant.contact_number)
        tv_complete_profile.text = Utils.getText(this, StringConstant.str_complete_profile)
        tv_verification.text = Utils.getText(this, StringConstant.str_verification)
        tv_sync_phone.text = Utils.getText(this, StringConstant.str_sync_wid_phone)
        tv_verified.text = Utils.getText(this, StringConstant.myprofile_verified)
        tv_three_steps.text = Utils.getText(this, StringConstant.PD_just_3_steps_away)
        tv_editprofile.text = Utils.getText(this, StringConstant.myprofile_edit)
        edt_contact.hint = Utils.getText(this, StringConstant.PD_hint_enter_contact_no)
        save_next.text = Utils.getText(this, StringConstant.str_save)
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        tv_editprofile.setOnClickListener(this)
        save_next.setOnClickListener(this)
        edt_city.setOnClickListener(this)
        rl_city.setOnClickListener(this)
        iv_profile.setOnClickListener(this)
        toolbar_title.text = Utils.getText(this, StringConstant.myprofile_title)
        iv_close.onClick { onBackPressed() }
    }

    private fun activityUIIntializer() {
        iv_insta.setOnClickListener(this)
        iv_linkedin.setOnClickListener(this)
        iv_email.setOnClickListener(this)
        iv_fb.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        //Picasso.get().load(this.outputImageFile()).resize(400, 400).into(iv_profile)
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ProfileDetailActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Utils.hideSoftKeyboard(this)
        ActivityStack.removeActivity(this@ProfileDetailActivity)
        finish()
    }

    fun enableEditText() {
        tv_editprofile.visibility = View.VISIBLE
        save_next.visibility = View.VISIBLE
        edt_name.isEnabled = true
        edt_last.isEnabled = true
        edt_aadhar.isEnabled = true
        // edt_prefrences.isEnabled = true
        edt_service.isEnabled = true
        edt_city.isEnabled = true
        edt_address.isEnabled = true
        edt_mediaurl.isEnabled = true
        iv_insta.isEnabled = true
        rl_city.isEnabled = true
        iv_email.isEnabled = true
        iv_linkedin.isEnabled = true
        iv_fb.isEnabled = true
    }

    private fun getBadgePoint(totalPoint: Float) {
//        var badgePoint : Float =  totalPoint/3
        if (totalPoint > 2.5) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                iv_badge.setImageDrawable(getResources().getDrawable(R.drawable.profilebadge, getApplicationContext().getTheme()));
            } else {
                iv_badge.setImageDrawable(getResources().getDrawable(R.drawable.profilebadge))
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                iv_badge.setImageDrawable(getResources().getDrawable(R.drawable.profilebadge_low, getApplicationContext().getTheme()));
            } else {
                iv_badge.setImageDrawable(getResources().getDrawable(R.drawable.profilebadge_low))
            }
        }
    }

    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ProfileDetailActivity)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        if (message != null) {
            profileImage = personData.profile_image!!
/*
            Glide.with(this@ProfileDetailActivity)
                    .load(Utils.getUrl(this@ProfileDetailActivity, personData.profile_image!!))
                    .placeholder(R.drawable.user_placeholder)
                    .centerCrop()
                    .into(iv_profile)
*/

            if (personData.profile_image!!.isEmpty()) {
                iv_profile.setImageResource(R.drawable.user_placeholder);
            } else {

                Picasso.get().load(personData.profile_image).into(iv_profile)

            }


            val profileData: ProfileDataResponse = message[0]!!
//          user_reviews.rating = profileData.review_avg!!.toFloat()
            // quality_reviews.rating = profileData.quality_service!!.toFloat()
            // friendlines_reviews.rating = profileData.friendliness!!.toFloat()

            //  Log.e("gggggggggggggggggg",""+profileData.review_avg!!.toFloat() )
            getBadgePoint(profileData.review_avg!!.toFloat())
            edt_name.setText(profileData.first_name)
            edt_last.setText(profileData.last_name)
            edt_email.setText(profileData.email)
            edt_contact.text = personData.user_mobile_number
            edt_aadhar.setText(profileData.aadhar_number)
            //  edt_prefrences.setText(profileData.preferences)
            edt_service.setText(profileData.describe_yourself)
            edt_city.setText(profileData.city)
            tv_percentage.text = " ${profileData.complete_percentage.toString()}%"
            edt_address.setText(profileData.address)
            edt_mediaurl.setText(profileData.media_url)


            if (profileData.profile_image!!.isEmpty()) {
                iv_profile.setImageResource(R.drawable.user_placeholder);
            } else {

                Picasso.get().load(profileData.profile_image).into(iv_profile)

            }

/*
            Glide.with(this@ProfileDetailActivity)
                    .load(Utils.getUrl(this@ProfileDetailActivity, profileData.profile_image!!))
                    .placeholder(R.drawable.user_placeholder)
                    .centerCrop()
                    .into(iv_profile)
*/

            var profileRemain = (100.minus(profileData.complete_percentage!!))
            profile_complete.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, java.lang.Float.parseFloat(profileData.complete_percentage.toString()))
            profile_complete1.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, java.lang.Float.parseFloat(profileRemain.toString()))
            profile_complete.setBackgroundResource(R.color.color_green_color)
            profile_complete1.setBackgroundResource(R.color.color_grey_color)
            profile_complete.setTextColor(Utils.getColor(this@ProfileDetailActivity, R.color.color_green_color))
            profile_complete1.setTextColor(Utils.getColor(this@ProfileDetailActivity, R.color.color_grey_color))

            if (profileData.sync_with_phone?.whatsapp != null) {
                isUserWhatAuth = profileData.sync_with_phone.whatsapp
            } else {
                isUserWhatAuth = false
            }
            if (profileData.sync_with_phone?.facebook != null) {
                isUserFbAuth = profileData.sync_with_phone.facebook
            } else {
                isUserFbAuth = false
            }
            if (profileData.sync_with_phone!!.gmail != null) {
                isUserGmailAuth = profileData.sync_with_phone.gmail!!
            } else {
                isUserGmailAuth = false
            }
            if (profileData.sync_with_phone.linkedin != null) {
                isUserLinkedAuth = profileData.sync_with_phone.linkedin
            } else {
                isUserLinkedAuth = false
            }
            if (profileData.sync_with_phone.instagram != null) {
                isUserInstaAuth = profileData.sync_with_phone.instagram
            } else {
                isUserInstaAuth = false
            }
/*
            if(isUserWhatAuth){
                iv_call.setBackgroundDrawable(resources.getDrawable(R.drawable.call_selected))
            }else{
                iv_call.setBackgroundDrawable(resources.getDrawable(R.drawable.call_unselected))
            }
*/

            if (isUserFbAuth) {
                iv_fb.setBackgroundDrawable(resources.getDrawable(R.drawable.facebook_selected))
            } else {
                iv_fb.setBackgroundDrawable(resources.getDrawable(R.drawable.facebook_unselected))
            }
            if (isUserGmailAuth) {
                iv_email.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_selected))
            } else {
                iv_email.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_unselected))
            }
            if (isUserLinkedAuth) {
                iv_linkedin.setBackgroundDrawable(resources.getDrawable(R.drawable.linkedin_selected))
            } else {
                iv_linkedin.setBackgroundDrawable(resources.getDrawable(R.drawable.linkedin_unselected))
            }
            if (isUserInstaAuth) {
                iv_insta.setBackgroundDrawable(resources.getDrawable(R.drawable.instagram_selected))
            } else {
                iv_insta.setBackgroundDrawable(resources.getDrawable(R.drawable.instagram_unselected))
            }
        }
        hideProgressDialog()

    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
        hideProgressDialog()
        Log.i(javaClass.name, "===================" + message + " apiCallMethod   " + apiCallMethod)
        onBackPressed()
//        if (apiCallMethod.equals("profileUpdate")) {
//            // showSnackBar(message)
//            // Utility.showAlert(this,message)
////            val intent = AddPostRequestActivity.newMainIntent(this)
////            intent?.putExtra("Tab", "serviceTab")
////            ActivityStack.getInstance(this)
//////                    startActivity(intent)
////            startActivityForResult(intent, 13)
//
//        } else {
//            showSnackBar(message)
//        }
//        if (apiCallMethod.equals("profileUpdate")) {
//            if (Utils.haveNetworkConnection(this@ProfileDetailActivity)) {
//                showProgressDialog(Utils.getText(this, StringConstant.please_wait))
//                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ProfileDetailActivity)
//                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
//                mProfileDetailPresenter.getUserProfileData(this@ProfileDetailActivity, personData)
//            } else {
//                showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
//            }
//        }
    }

    private var profileUpdateListnuer: ProfileDetailActivity.profilePicUpdate? = null

    interface profilePicUpdate {
        fun OnProfileUpdate(userModal: ProfileDataResponse)
    }

    fun selectImage() {
        val items = arrayOf<CharSequence>(Utils.getText(this, StringConstant.take_photo)
                , Utils.getText(this, StringConstant.choose_from_library), Utils.getText(this, StringConstant.cancel))
        var builder: AlertDialog.Builder = AlertDialog.Builder(this@ProfileDetailActivity)
        builder.setTitle(Utils.getText(this, StringConstant.add_photo_header))
        builder.setItems(items) { dialog, item ->
            if (items[item].equals(Utils.getText(this, StringConstant.take_photo))) {
                // cameraIntent()
                //  onLaunchCamera()
                takePicture()
            } else if (items[item].equals(Utils.getText(this, StringConstant.choose_from_library))) {
                // galleryIntent()
                // pickFromGallery()
                openGallery()
            } else if (items[item].equals(Utils.getText(this, StringConstant.cancel))) {
                dialog.dismiss()

            }
        }
        builder.show()
    }

/*
    private fun cameraIntent() {
        val outputFileUri = getCaptureImageOutputUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        */
/* if (outputFileUri != null)
             intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)*//*

        startActivityForResult(intent, CAMERA_REQUEST)
    }
*/

    /**
     * Get URI to image received from capture by camera.
     */

/*
    private fun getCaptureImageOutputUri(): Uri {
        var outputFileUri: Uri? = null
        val getImage = this@ProfileDetailActivity.externalCacheDir
        if (getImage != null) {
            outputFileUri = Uri.fromFile(File(getImage.path, "profile.png"))
        }
        return outputFileUri!!
    }
*/


    // private fun galleryIntent() {
    //  val intent = Intent()
    //  intent.type = "image/*"
    //   intent.action = Intent.ACTION_GET_CONTENT//
    //   startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_REQUEST)
    // }


    private fun validate(): Boolean {
        val firstName = edt_name.text.toString().trim()
        val lastName = edt_last.text.toString().trim()
//        val city = edt_city.text.toString().trim()
        val about_yourself = edt_service.text.toString().trim()

        if (firstName == null || firstName.equals("")) {
            // toast(Utils.getText(this, StringConstant.myprofile_firstname_alert))
            edt_name.setFocusable(true);
            edt_name.setFocusableInTouchMode(true);
            edt_name.requestFocus();
            edt_name.setError(Utils.getText(this, StringConstant.myprofile_firstname_alert))
            return false
        } else if (lastName == null || lastName.equals("")) {
            // toast(Utils.getText(this, StringConstant.myprofile_lastname_alert))
            edt_last.setFocusable(true);
            edt_last.setFocusableInTouchMode(true);
            edt_last.requestFocus();
            edt_last.setError(Utils.getText(this, StringConstant.myprofile_lastname_alert))

            return false
        }
//        else if (city == null || city.equals("")) {
//            toast(Utils.getText(this, StringConstant.myprofile_address_alert))
//          /*  edt_city.setFocusable(true);
//            edt_city.setFocusableInTouchMode(true);
//            edt_city.requestFocus();
//            edt_city.setError(Utils.getText(this, StringConstant.myprofile_address_alert))
//            edt_service.setFocusableInTouchMode(false);*/
//            return false
//        }
        /* else if (edt_email.text.toString().trim() == null || edt_email.text.toString().trim().isEmpty()) {
             toast(Utils.getText(this, StringConstant.str_email))
             return false
         } */
        else if (about_yourself == null || about_yourself.equals("") || about_yourself.length <= 30) {
            // toast(Utils.getText(this, StringConstant.myprofile_write_yourself_min50))
            edt_service.setFocusable(true);
            edt_service.setFocusableInTouchMode(true);
            edt_service.requestFocus();
            edt_service.setError(Utils.getText(this, StringConstant.myprofile_write_yourself_min30))
            return false
        } else if (profileImage == null || profileImage.equals("")) {
            toast(Utils.getText(this, StringConstant.myprofile_profileimage_alert))
            return false
        } else if (edt_email.text.toString().trim() != "") {
            if (!Utility.isValidEmailAddress(edt_email.text.toString().trim())) {
                edt_email.setError("Please enter valid Email ID")
                return false
            }
        }

//        if(webUrl == null || webUrl.equals(""))
//        {
//            toast(Utils.getText(this,StringConstant.myprofile_write_yourself))
//            return false
//        }
//        else{
//            if(checkValidUrl(webUrl)){
//                toast(Utils.getText(this,StringConstant.myprofile_write_yourself))
//            }
//            return checkValidUrl(webUrl)
//        }
//        if(address == null || address.equals("")){
//            toast(Utils.getText(this,StringConstant.myprofile_address_alert))
//            return false
//        }


        return true
    }

    fun checkValidUrl(url: String): Boolean {
        return Patterns.WEB_URL.matcher(url).matches()
        return URLUtil.isValidUrl(url)
    }

    override fun onClick(v: View?) {
        when (v) {
            edt_city -> {
                var intent: Intent =
                        PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .build(this@ProfileDetailActivity)
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
            }
            rl_city -> {
                var intent: Intent =
                        PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .build(this@ProfileDetailActivity)
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
            }

            iv_profile -> {
                selectImage()
            }

            tv_editprofile -> {
                selectImage()
            }
            save_next -> {
                if (!validate()) {
                    hideProgressDialog()
                    println("Device")
                } else {
//                    if (Utils.haveNetworkConnection(this@ProfileDetailActivity)) {
//                        showProgressDialog("Please wait..")
                    if (Utils.haveNetworkConnection(this@ProfileDetailActivity)) {
                        showProgressDialog(Utils.getText(this, StringConstant.please_wait))
                        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@ProfileDetailActivity)
                        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                        val map = hashMapOf<String, RequestBody>()
                        val requestBodyUserId = Utils.createPartFromString(personData.user_id!!)
                        val requestBodyFirstName = Utils.createPartFromString(edt_name.text.toString().trim())
                        val requestBodyLastName = Utils.createPartFromString(edt_last.text.toString().trim() + "")
                        val requestBodyEmail = Utils.createPartFromString(edt_email.text.toString().trim())
                        val designation = Utils.createPartFromString("")
                        val aadhar_number = Utils.createPartFromString(edt_aadhar.text.toString().trim())
                        val preferences = Utils.createPartFromString("")
                        val describe_yourself = Utils.createPartFromString(edt_service.text.toString().trim())
//                        val city = Utils.createPartFromString(edt_city.text.toString().trim())
                        val media_url = Utils.createPartFromString(edt_mediaurl.text.toString().trim())
                        val sync_with_phonefacebook = Utils.createPartFromString(isUserFbAuth.toString())
                        val sync_with_phonewhatsapp = Utils.createPartFromString("true")
                        val sync_with_phonegmail = Utils.createPartFromString(isUserGmailAuth.toString())
                        val sync_with_phonelinkedin = Utils.createPartFromString(isUserLinkedAuth.toString())
                        val sync_with_phoneinsta = Utils.createPartFromString(isUserInstaAuth.toString())
                        val address = Utils.createPartFromString(edt_address.text.toString().trim())
                        map.put("user_id", requestBodyUserId)
                        map.put("first_name", requestBodyFirstName)
                        map.put("last_name", requestBodyLastName)
                        map.put("email", requestBodyEmail)
                        map.put("designation", designation)
                        map.put("aadhar_number", aadhar_number)
                        map.put("preferences", preferences)
                        map.put("describe_yourself", describe_yourself)
//                        map.put("city", city)
                        map.put("media_url", media_url)
                        map.put("sync_with_phone[0][facebook]", sync_with_phonefacebook)
                        map.put("sync_with_phone[0][whatsapp]", sync_with_phonewhatsapp)
                        map.put("sync_with_phone[0][gmail]", sync_with_phonegmail)
                        map.put("sync_with_phone[0][linkedin]", sync_with_phonelinkedin)
                        map.put("sync_with_phone[0][instagram]", sync_with_phoneinsta)
                        map.put("address", address)
                        if (mImagePath != null) {
                            mProfileDetailPresenter.updateUserProfileData(this@ProfileDetailActivity, map, mImagePath)
                        } else {
                            mProfileDetailPresenter.updateUserProfileData(this@ProfileDetailActivity, map, "")
                        }

                    } else {
                        showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                    }
//                    } else {
//                        showSnackBar(resources.getString(R.string.str_check_internet))
//                    }
                }
            }

            iv_insta -> {
                //TODO INSTA
                if (!isUserInstaAuth) {
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        InstagramHandler.getInstance().loginToInstagram(this@ProfileDetailActivity)
                    } else {
                        AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                                Utils.getText(this@ProfileDetailActivity, StringConstant.internet_connection_title),
                                Utils.getText(this@ProfileDetailActivity, StringConstant.str_check_internet),
                                false, false)
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                            Utils.getText(this, StringConstant.instagram_already_connected_title),
                            Utils.getText(this, StringConstant.instagram_already_connected_message),
                            false, false)
                }
            }

            iv_linkedin -> {
                //TODO LINKEDIN
                if (!isUserLinkedAuth) {
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        val intent = Intent(this@ProfileDetailActivity, LinkedinActivity::class.java)
                        intent.putExtra("extra", "login")
                        startActivityForResult(intent, REQUEST_TYPE_LINKEDIN)
                    } else {
                        AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                                Utils.getText(this@ProfileDetailActivity, StringConstant.internet_connection_title),
                                Utils.getText(this@ProfileDetailActivity, StringConstant.str_check_internet),
                                false, false)
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                            Utils.getText(this, StringConstant.linkedin_already_connected_title),
                            Utils.getText(this, StringConstant.linkedin_already_connected_message),
                            false, false)
                }
            }

            iv_email -> {
                if (!isUserGmailAuth) {
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        val intent = Intent(this@ProfileDetailActivity, GoogleActivity::class.java)
                        intent.putExtra("extra", "login")
                        startActivityForResult(intent, GOOGLE_LOGIN)
                    } else {
                        AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                                Utils.getText(this@ProfileDetailActivity, StringConstant.internet_connection_title),
                                Utils.getText(this@ProfileDetailActivity, StringConstant.str_check_internet),
                                false, false)
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                            Utils.getText(this, StringConstant.email_already_connected_title),
                            Utils.getText(this, StringConstant.email_already_connected_message),
                            false, false)
                }
            }
            iv_fb -> {
                if (!isUserFbAuth) {
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        // TODO: Add facebook related code here
                        val intent = Intent(this@ProfileDetailActivity, FacebookActivity::class.java)
                        intent.putExtra("extra", "login")
                        startActivityForResult(intent, FACEBOOK_LOGIN)
                    } else {
                        AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                                Utils.getText(this@ProfileDetailActivity, StringConstant.internet_connection_title),
                                Utils.getText(this@ProfileDetailActivity, StringConstant.str_check_internet),
                                false, false)
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                            Utils.getText(this, StringConstant.facebook_already_connected_title),
                            Utils.getText(this, StringConstant.facebook_already_connected_message),
                            false, false)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Log.e("requestCode ","" + requestCode)
        var bitmap: Bitmap

        if (requestCode == FACEBOOK_LOGIN) {
            isUserFbAuth = true
            if (isUserFbAuth) {
                iv_fb.setBackgroundDrawable(resources.getDrawable(R.drawable.facebook_selected))
            } else {
                iv_fb.setBackgroundDrawable(resources.getDrawable(R.drawable.facebook_unselected))
            }
        }
        if (requestCode == GOOGLE_LOGIN) {
            isUserGmailAuth = true
            if (isUserGmailAuth) {
                iv_email.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_selected))
            } else {
                iv_email.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_unselected))
            }
        }

        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                PLACE_AUTOCOMPLETE_REQUEST_CODE -> {
                    if (resultCode == Activity.RESULT_OK) {
                        var place: Place = PlaceAutocomplete.getPlace(this@ProfileDetailActivity, data)
                        edt_city.setText(place.name)
                    } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                        var status: Status = PlaceAutocomplete.getStatus(this@ProfileDetailActivity, data)
                        // TODO: Handle the error
                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        // The user canceled the operation.
                    }
                }
                /*******************imageeeee end****************************/

                /******************************new image code*********************/

                REQUEST_CODE_GALLERY -> {
                    try {
                        val selectedImageUri = data!!.getData()
                        val s1 = data!!.getDataString()
                        //String s1 = selectedImageUri.getPath();
                        Log.e("GetPath", s1)
                        Log.e("OK", "" + selectedImageUri!!)
                        var selectedImagePath = getPath(selectedImageUri)
                        if (selectedImagePath == null && s1 != null) {
                            selectedImagePath = s1.replace("file://".toRegex(), "")
                        }
                        val intent = Intent(this, CropImage::class.java)
                        intent.putExtra(CropImage.IMAGE_PATH, selectedImagePath)
                        intent.putExtra(CropImage.SCALE, true)
                        intent.putExtra(CropImage.ASPECT_X, 3)
                        intent.putExtra(CropImage.ASPECT_Y, 3)
                        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE)
                        return
                    } catch (e: Exception) {
                        Log.e("eeeee", "Error while creating temp file", e)
                    }
                }

                REQUEST_CODE_TAKE_PICTURE -> {
                    startCropImage()
                }
                REQUEST_CODE_CROP_IMAGE -> {
                    mImagePath = data!!.getStringExtra(CropImage.IMAGE_PATH)
                    Log.e("ImagePath", mImagePath)
                    bitmap = BitmapFactory.decodeFile(mImagePath)
                    val faceDetector = FaceDetector.Builder(applicationContext).setTrackingEnabled(false)
                            .build()
                    var frame = Frame.Builder().setBitmap(bitmap).build();
                    var faces = faceDetector.detect(frame);
//                    Log.i(javaClass.name, "=========================" + faces.size())
                    if (faces.size() > 0) {
                        iv_profile.setImageBitmap(bitmap)
                        val byteArrayOutputStream = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
                        val byteArray = byteArrayOutputStream.toByteArray()
                        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
                        profileImage = data!!.getStringExtra(CropImage.IMAGE_PATH)
                    } else {
                        profileImage = ""
                        Toast.makeText(context, "Face Not Detected", Toast.LENGTH_LONG).show()
                    }
                }


                GOOGLE_LOGIN, TWITTER_LOGIN -> {
                    val socialMediaProfilePojo = data!!.getParcelableExtra<SocialMediaProfilePojo>("result")
                    // Log.e("socialMediaProfilePojo","" + socialMediaProfilePojo)

                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        if (socialMediaProfilePojo.profileId != null &&
                                socialMediaProfilePojo.profileId != "") {
                            isUserGmailAuth = true
                            Log.e(" iff  isUserGmailAuth", "" + isUserGmailAuth)

                            if (isUserGmailAuth) {
                                iv_email.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_selected))
                            } else {
                                iv_email.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_unselected))
                            }
                            logoutGmailUser()
                            println("OnActi ${socialMediaProfilePojo.profileId}")
                        }
                    } else {
                        // Log.e(" elsee isUserGmailAuth","" + isUserGmailAuth)

                        AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                                Utils.getText(this@ProfileDetailActivity, StringConstant.internet_connection_title),
                                Utils.getText(this@ProfileDetailActivity, StringConstant.str_check_internet),
                                false, false)
                    }
                }

                GOOGLE_LOGOUT -> {
                    println("Logout SuccessFully")
                }
                LINKEDIN_LOGOUT -> {
                    println("Logout SuccessFully")
                }
                REQUEST_TYPE_LINKEDIN -> {
                    // TODO
                    val socialMediaProfilePojo = data!!.getParcelableExtra<SocialMediaProfilePojo>("result")
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        if (socialMediaProfilePojo.profileId != null &&
                                socialMediaProfilePojo.profileId != "") {
                            logoutLinkedInUser()
                            isUserLinkedAuth = true
                            println("OnActi ${socialMediaProfilePojo.profileId}")
                        }
                    } else {
                        AlertDialogManager().showAlertDialog(this@ProfileDetailActivity,
                                Utils.getText(this@ProfileDetailActivity, StringConstant.internet_connection_title),
                                Utils.getText(this@ProfileDetailActivity, StringConstant.str_check_internet),
                                false, false)
                    }
                }

            }
        }
    }

    private fun logoutLinkedInUser() {
        val intent = Intent(this@ProfileDetailActivity, LinkedinActivity::class.java)
        intent.putExtra("extra", "logout")
        startActivityForResult(intent, LINKEDIN_LOGOUT)
    }

    private fun logoutGmailUser() {
        val intent = Intent(this@ProfileDetailActivity, GoogleActivity::class.java)
        intent.putExtra("extra", "logout")
        startActivityForResult(intent, GOOGLE_LOGOUT)
    }


    override fun returnLoginDetails(socialMediaProfilePojo: SocialMediaProfilePojo?) {
        //  isUSerInstaAuth = true
        if (socialMediaProfilePojo != null) {
            if (socialMediaProfilePojo.profileId != null && socialMediaProfilePojo.profileId != "") {
                InstagramHandler.getInstance().logoutInstagram()
                isUserInstaAuth = true
                if (isUserInstaAuth) {
                    iv_insta.setBackgroundDrawable(resources.getDrawable(R.drawable.instagram_selected))
                } else {
                    iv_insta.setBackgroundDrawable(resources.getDrawable(R.drawable.instagram_unselected))
                }
            }
        }
    }
    /*private fun checkValidUrl(url : String){
        URLUtil.isValidUrl(url)
    }*/

    /*******************image code start*********************************/

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        if (permissions.size == 0) {
            return
        }
        var allPermissionsGranted = true
        if (grantResults.size > 0) {
            for (grantResult in grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    allPermissionsGranted = false
                    break
                }
            }
        }
        if (!allPermissionsGranted) {
            var somePermissionsForeverDenied = false
            for (permission in permissions) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    //denied
                    Log.e("denied", permission)
                } else {
                    if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                        //allowed
                        Log.e("allowed", permission)
                    } else {
                        //set to never ask again
                        Log.e("set to never ask again", permission)
                        somePermissionsForeverDenied = true
                    }
                }
            }
            if (somePermissionsForeverDenied) {
                val alertDialogBuilder = AlertDialog.Builder(context)
                alertDialogBuilder.setTitle("Permissions Required")
                        .setMessage(context!!.getString(R.string.permission_message))

                        .setPositiveButton(context!!.getString(R.string.settings)) { dialog, which ->
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", context!!.getPackageName(), null))
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            context!!.startActivity(intent)
                        }
                        .setNegativeButton(context!!.getString(R.string.cancel)) { dialog, which ->
                            // CameraIntentTestActivity.this.finish();
                        }
                        .setCancelable(false)
                        .create()
                        .show()
            }
        } else {

            when (requestCode) {
                MarshmallowPermission.REQUEST_CAMERA_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onLaunchCamera()
                }
                MarshmallowPermission.REQUEST_READ_EXTERNAL_STORAGE_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery()
                }

                MarshmallowPermission.REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveCroppedImage()
                }
            }
        }

    }

    fun onLaunchCamera() {

        if (marshmallowPermission!!.check_camera_Permission()) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            photoFile = getPhotoFileUri()

            val fileProvider = FileProvider.getUriForFile(this, context!!.getString(R.string.file_provider_authorities), photoFile!!)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider)

            if (intent.resolveActivity(context!!.getPackageManager()) != null) {
                this.startActivityForResult(intent, REQUEST_CAMERA_IMAGE)

            }
        }

    }

    fun pickFromGallery() {
        if (marshmallowPermission!!.check_read_external_storage_permission()) {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_PICK
            this.startActivityForResult(Intent.createChooser(intent, context!!.getString(R.string.label_select_picture)), REQUEST_GALLERY_IMAGE)
        }
    }

    private fun getPhotoFileUri(): File {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = timeStamp + context!!.getString(R.string.photo_jpg)

        /*Use this if you did not want to repeat images*/
        val photoFileName = "photo.jpg"

        val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), context!!.getString(R.string.app_name))
        // File mediaStorageDir = new File(getCacheDir(),getString(R.string.app_name));

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.e("", "failed to create directory")
        }

        return File(mediaStorageDir.path + File.separator + imageFileName)

    }

    private fun saveCroppedImage() {

        if (marshmallowPermission!!.check_write_Permission()) {
            try {
                copyFileToDownloads(resultUri!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    @Throws(Exception::class)
    private fun copyFileToDownloads(croppedFileUri: Uri) {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = timeStamp + context!!.getString(R.string.photo_jpg)

        val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), context!!.getString(R.string.app_name))

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.e("", "failed to create directory")
        }
        saveFile = File(mediaStorageDir, imageFileName)

        val inStream = FileInputStream(File(croppedFileUri.path))
        val outStream = FileOutputStream(saveFile)
        val inChannel = inStream.channel
        val outChannel = outStream.channel
        inChannel.transferTo(0, inChannel.size(), outChannel)
        inStream.close()
        outStream.close()

        mImagePath = this.outputImageFile().path.toString()
        profileImage = this.outputImageFile().path.toString()
        Picasso.get().load(this.outputImageFile()).resize(300, 300).centerCrop().into(iv_profile)

    }

    fun outputImageFile(): File {
        return saveFile!!
    }

    /*******************image code end*********************************/

    private fun openGallery() {

        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY)
    }

    private fun startCropImage() {

        val intent = Intent(this, CropImage::class.java)
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath())
        intent.putExtra(CropImage.SCALE, true)
        intent.putExtra(CropImage.ASPECT_X, 3)
        intent.putExtra(CropImage.ASPECT_Y, 3)
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE)
    }

    private fun takePicture() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        try {
            var mImageCaptureUri: Uri? = null
            val state = Environment.getExternalStorageState()
            if (Environment.MEDIA_MOUNTED == state) {
                //  mImageCaptureUri = Uri.fromFile(mFileTemp);

                mImageCaptureUri = FileProvider.getUriForFile(this@ProfileDetailActivity,
                        getString(R.string.file_provider_authority),
                        mFileTemp)

            } else {
                /*
                     * The solution is taken from here:
					 * http://stackoverflow.com/questions
					 * /10042695/how-to-get-camera-result-as-a-uri-in-data-folder
					 */
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri)
            intent.putExtra("return-data", true)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE)
        } catch (e: ActivityNotFoundException) {

            // Log.d(TAG, "cannot take picture", e);
        }

    }

    fun getPath(uri: Uri): String? {

        try {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            Log.e("OK 1", "" + projection)
            val cursor = managedQuery(uri, projection, null, null, null)
            Log.e("OK 2", "" + cursor!!)
            if (cursor == null) {
                return null

            }
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            Log.e("OK 3", "" + column_index)
            cursor!!.moveToFirst()
            Log.e("OK 4", "" + cursor!!.getString(column_index))
            return cursor!!.getString(column_index)
        } catch (e: Exception) {
            Toast.makeText(this, "Image is too big in resolution please try again", Toast.LENGTH_LONG).show()
            return null
        }

    }


    /******************* new image code end*********************************/

}