package com.tetoota.fragment.profile.OptionalFeldByUser
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.categories.SelectCategoriesActivity
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.main.MainActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_optional_profile_user.*
import kotlinx.android.synthetic.main.activity_optional_profile_user.edt_aadhar
import kotlinx.android.synthetic.main.activity_optional_profile_user.edt_address
import kotlinx.android.synthetic.main.activity_optional_profile_user.edt_city
import kotlinx.android.synthetic.main.activity_optional_profile_user.edt_email
import kotlinx.android.synthetic.main.activity_optional_profile_user.iv_email
import kotlinx.android.synthetic.main.activity_optional_profile_user.rl_city
import kotlinx.android.synthetic.main.activity_optional_profile_user.tv_aadhar
import kotlinx.android.synthetic.main.activity_optional_profile_user.tv_address
import kotlinx.android.synthetic.main.activity_optional_profile_user.tv_city
import kotlinx.android.synthetic.main.activity_optional_profile_user.tv_email
import kotlinx.android.synthetic.main.activity_profile_detail.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast
import android.R.attr.data
import android.util.Log
import android.widget.Toast
import com.tetoota.socialmedias.AlertDialogManager
import com.tetoota.socialmedias.ConnectionDetector
import com.tetoota.socialmedias.SocialMediaProfilePojo
import com.tetoota.socialmedias.facebook.FacebookActivity
import com.tetoota.socialmedias.google.GoogleActivity
import com.tetoota.socialmedias.instagram.InstagramCallback
import com.tetoota.socialmedias.instagram.InstagramHandler
import com.tetoota.socialmedias.linkedin.LinkedinActivity
import com.tetoota.utility.Verhoeff
import org.jetbrains.anko.act
import java.lang.Exception
import kotlinx.android.synthetic.main.activity_optional_profile_user.iv_fb as iv_fb1
import kotlinx.android.synthetic.main.activity_optional_profile_user.iv_linkedin as iv_linkedin1
import kotlinx.android.synthetic.main.activity_profile_detail.iv_insta as iv_insta1
import android.R.attr.data
import android.text.TextUtils
import android.util.Patterns
import com.google.gson.Gson
import com.tetoota.login.LoginDataResponse

class OptionalProfileUser : BaseActivity(), ProfileDetailContract.View, View.OnClickListener,
        InstagramCallback {
//---------------------------------------------------------------------------------------------------------------//
    override fun onProfileSendEmailApiSuccessResult(message: String) {
        hideProgressDialog()
        if(isVerify){
            edt_emailVerify.setEnabled(true);
            edt_email.isFocusableInTouchMode = true
            edt_email.isFocusable = true
            Utils.savePreferences(Constant.OPTIONAL_INFORMATION_EMAIL, edt_email.text.toString(), this)
            Utils.savePreferencesBoolean(Constant.Email_Verify, true, this@OptionalProfileUser)
            Toast.makeText(this@OptionalProfileUser, "Verification Sent to your Email", Toast.LENGTH_SHORT).show()
            //Toast.makeText(this@OptionalProfileUser, "verification send to your email", Toast.LENGTH_SHORT).show()
        }else{
            edt_emailVerify.setEnabled(true);
            edt_email.isFocusableInTouchMode = true
            edt_email.isFocusable = true
            Utils.savePreferences(Constant.OPTIONAL_INFORMATION_EMAIL, edt_email.text.toString(), this)
            Utils.savePreferencesBoolean(Constant.Email_Verify, true, this@OptionalProfileUser)
            //Toast.makeText(this@OptionalProfileUser, "verification send to your email", Toast.LENGTH_SHORT).show()
            Toast.makeText(this@OptionalProfileUser, "Aadhar Verified", Toast.LENGTH_SHORT).show()
        }
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onProfileSendEmailApiFailureResult(message: String) {
        //Toast.makeText(this@OptionalProfileUser, message, Toast.LENGTH_SHORT).show()
        hideProgressDialog()
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        hideProgressDialog()
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@OptionalProfileUser)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        if (message != null) {
            val profileData: ProfileDataResponse = message[0]!!
            if (!TextUtils.isEmpty(profileData.email_verify)) {
                isEmailverify = profileData.email_verify!!.toBoolean()
                if (isEmailverify) {
                    edt_emailVerify.setEnabled(false);
                    edt_email.isFocusableInTouchMode = false
                    edt_email.isFocusable = false
                    edt_email.setText(profileData.email.toString())
                }
                Log.i(javaClass.name, "=================EmailP1" + profileData.email_verify)
                Log.i(javaClass.name, "=================EmailP2" + isEmailverify)
            }
            if (!TextUtils.isEmpty(profileData.aadhar_verify)) {
                isAadharverify = profileData.aadhar_verify!!.toBoolean()
                if (isAadharverify) {
                    edt_AadharVerify.setEnabled(false);
                    edt_aadhar.isFocusableInTouchMode = false
                    edt_aadhar.isFocusable = false
                    edt_aadhar.setText(profileData.aadhar_number.toString())
                }
                Log.i(javaClass.name, "=================AadharP1" + profileData.aadhar_verify)
                Log.i(javaClass.name, "=================AadharP2" + isAadharverify)
            }
        }
    }
    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
        hideProgressDialog()
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        hideProgressDialog()
    }
    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
        hideProgressDialog()
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onOptionalInformationApiSuccessResult(message: String) {
        hideProgressDialog()
        Utils.savePreferences(Constant.OPTIONAL_INFORMATION_EMAIL, edt_email.text.toString(), this)
        Utils.savePreferences(Constant.OPTIONAL_INFORMATION_ADHAR, edt_aadhar.text.toString().trim(), this)
        Utils.savePreferences(Constant.OPTIONAL_INFORMATION_CATEGORY, selectCategoryValues.text.toString(), this)
        var intent = Intent(this@OptionalProfileUser, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onOptionalInformationApiFailureResult(message: String) {
        this@OptionalProfileUser!!.toast(message)
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        com.tetoota.fragment.profile.ProfileDetailPresenter(this@OptionalProfileUser)
    }
//---------------------------------------------------------------------------------------------------------------//
    private var context: Context? = null
    private var userInterestIds = "";
    private val GOOGLE_LOGIN: Int = 101
    private val FACEBOOK_LOGIN: Int = 103
    private val REQUEST_TYPE_LINKEDIN: Int = 105
    private var isUserGmailAuth: Boolean = false
    private var isUserLinkedAuth: Boolean = false
    private var isUserInstaAuth: Boolean = false
    private var isUserFbAuth: Boolean = false
    private var isEmailverify: Boolean = false
    private var isAadharverify: Boolean = false
    private var isVerify: Boolean = false
    companion object constant {
        val OPTIONAL_USER_CODE_INTEREST = 1000
        val OPTIONAL_USER_INTEREST_RESULT = "result"
        val OPTIONAL_USER_INTEREST_RESULT_IDS = "result_ids"
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this;
        setContentView(R.layout.activity_optional_profile_user)
        activityUIIntializer()
        setMultiLanguageText()
        enableEditText()
        showadata()
    }
//---------------------------------------------------------------------------------------------------------------//
    fun showadata() {
        if (Utils.haveNetworkConnection(this@OptionalProfileUser)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this@OptionalProfileUser)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileData(this@OptionalProfileUser, personData)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }
//---------------------------------------------------------------------------------------------------------------//
    private fun activityUIIntializer() {
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        toolbar_title.text = Utils.getText(this, StringConstant.str_complete_profile)
        iv_close.onClick { onBackPressed() }
        selectCategory.text = Utils.getText(this, StringConstant.select_your_interest)
        tv_verification_op.text = Utils.getText(this, StringConstant.str_verification)
        selectCategory.setOnClickListener(this)
        btn_submit_req.setOnClickListener(this)
        iv_insta.setOnClickListener(this)
        iv_linkedin.setOnClickListener(this)
        iv_email.setOnClickListener(this)
        iv_fb.setOnClickListener(this)
        edt_emailVerify.setOnClickListener(this)
        edt_AadharVerify.setOnClickListener(this)
        selectCategoryValues.setText(Utils.loadPrefrence(Constant.OPTIONAL_INFORMATION_CATEGORY, "", this))
    }
//---------------------------------------------------------------------------------------------------------------//
    private fun setMultiLanguageText() {
        tv_email.text = Utils.getText(this, StringConstant.email_id)
        edt_email.hint = Utils.getText(this, StringConstant.str_email)
        tv_city.text = Utils.getText(this, StringConstant.str_city)
        edt_city.hint = Utils.getText(this, StringConstant.str_city)
        tv_aadhar.text = Utils.getText(this, StringConstant.aadhar_num)
        edt_aadhar.hint = Utils.getText(this, StringConstant.aadhar_num)
        tv_address.text = Utils.getText(this, StringConstant.address)
        edt_address.hint = Utils.getText(this, StringConstant.address)
        btn_submit_req.text = Utils.getText(this, StringConstant.str_save)
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }
//---------------------------------------------------------------------------------------------------------------//
    fun enableEditText() {
        edt_aadhar.isEnabled = true
        edt_city.isEnabled = true
        edt_address.isEnabled = true
        rl_city.isEnabled = true
        iv_email.isEnabled = true
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onClick(v: View?) {
        when (v) {
            iv_insta -> {
                //TODO INSTA
                if (!isUserInstaAuth) {
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        InstagramHandler.getInstance().loginToInstagram(this@OptionalProfileUser)
                    } else {
                        AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                                Utils.getText(this@OptionalProfileUser, StringConstant.internet_connection_title),
                                Utils.getText(this@OptionalProfileUser, StringConstant.str_check_internet),
                                false, false)
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                            Utils.getText(this, StringConstant.instagram_already_connected_title),
                            Utils.getText(this, StringConstant.instagram_already_connected_message),
                            false, false)
                }
            }
//---------------------------------------------------------------------------------------------------------------//
            iv_linkedin -> {
                //TODO LINKEDIN
                if (!isUserLinkedAuth) {
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        val intent = Intent(this@OptionalProfileUser, LinkedinActivity::class.java)
                        intent.putExtra("extra", "login")
                        startActivityForResult(intent, REQUEST_TYPE_LINKEDIN)
                    } else {
                        AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                                Utils.getText(this@OptionalProfileUser, StringConstant.internet_connection_title),
                                Utils.getText(this@OptionalProfileUser, StringConstant.str_check_internet),
                                false, false)
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                            Utils.getText(this, StringConstant.linkedin_already_connected_title),
                            Utils.getText(this, StringConstant.linkedin_already_connected_message),
                            false, false)
                }
            }
//---------------------------------------------------------------------------------------------------------------//
            iv_email -> {
                if (!isUserGmailAuth) {
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        val intent = Intent(this@OptionalProfileUser, GoogleActivity::class.java)
                        intent.putExtra("extra", "login")
                        startActivityForResult(intent, GOOGLE_LOGIN)
                    } else {
                        AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                                Utils.getText(this@OptionalProfileUser, StringConstant.internet_connection_title),
                                Utils.getText(this@OptionalProfileUser, StringConstant.str_check_internet),
                                false, false)
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                            Utils.getText(this, StringConstant.email_already_connected_title),
                            Utils.getText(this, StringConstant.email_already_connected_message),
                            false, false)
                }
            }
//---------------------------------------------------------------------------------------------------------------//
            iv_fb -> {
                if (!isUserFbAuth) {
                    if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                        // TODO: Add facebook related code here
                        val intent = Intent(this@OptionalProfileUser, FacebookActivity::class.java)
                        intent.putExtra("extra", "login")
                        startActivityForResult(intent, FACEBOOK_LOGIN)
                    } else {
                        AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                                Utils.getText(this@OptionalProfileUser, StringConstant.internet_connection_title),
                                Utils.getText(this@OptionalProfileUser, StringConstant.str_check_internet),
                                false, false)
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                            Utils.getText(this, StringConstant.facebook_already_connected_title),
                            Utils.getText(this, StringConstant.facebook_already_connected_message),
                            false, false)
                }
            }
//---------------------------------------------------------------------------------------------------------------//
            selectCategory -> {
                var intent = Intent(this@OptionalProfileUser, SelectCategoriesActivity::class.java)
                startActivityForResult(intent, OPTIONAL_USER_CODE_INTEREST)
            }
            btn_submit_req -> {
                validate()
            }
            edt_emailVerify -> {
                emailValidate()
            }
            edt_AadharVerify -> {
                AadharVerifyvalidate()
            }
        }
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun returnLoginDetails(socialMediaProfilePojo: SocialMediaProfilePojo?) {
        // isUSerInstaAuth = true
        if (socialMediaProfilePojo != null) {
            if (socialMediaProfilePojo.profileId != null && socialMediaProfilePojo.profileId != "") {
                InstagramHandler.getInstance().logoutInstagram()
                isUserInstaAuth = true
                if (isUserInstaAuth) {
                    iv_insta.setBackgroundDrawable(resources.getDrawable(R.drawable.instagram_selected))
                } else {
                    iv_insta.setBackgroundDrawable(resources.getDrawable(R.drawable.instagram_unselected))
                }
            }
        }
    }
//---------------------------------------------------------------------------------------------------------------//
    private fun emailValidate() {
        if (edt_email.text.toString().trim().isEmpty()) {
            edt_email.setFocusable(true);
            edt_email.setFocusableInTouchMode(true);
            edt_email.requestFocus();
            edt_email.setError(Utils.getText(this, StringConstant.str_email))
        } else if (!edt_email.text.toString().contains("@")) {
            edt_email.setFocusable(true);
            edt_email.setFocusableInTouchMode(true);
            edt_email.requestFocus();
            edt_email.setError("Invalid Email")
        } else {
            isVerify=true
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mProfileDetailPresenter.profileSendEmailAadhar(this@OptionalProfileUser, Utils.loadPrefrence(Constant.USER_ID, "", this@OptionalProfileUser), edt_email.text.toString(), "", "email")
        }
    }
//---------------------------------------------------------------------------------------------------------------//
    private fun validate() {
        if (!edt_email.text.toString().trim().isEmpty()&&!edt_email.text.toString().contains("@")) {
            edt_email.setFocusable(true);
            edt_email.setFocusableInTouchMode(true);
            edt_email.requestFocus();
            edt_email.setError("Invalid Email")
        }
        else
        {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mProfileDetailPresenter.updateOptionalInformationApi(this@OptionalProfileUser, Utils.loadPrefrence(Constant.USER_ID, "", this@OptionalProfileUser),
                    edt_email.text.toString().trim(), edt_aadhar.text.toString().trim(), userInterestIds)
        }
    }
//---------------------------------------------------------------------------------------------------------------//
    private fun AadharVerifyvalidate() {
        if (edt_aadhar.text.toString().trim().isEmpty()) {
            edt_aadhar.setFocusable(true);
            edt_aadhar.setFocusableInTouchMode(true);
            edt_aadhar.requestFocus();
            edt_aadhar.setError(Utils.getText(this, StringConstant.aadhar_num))
        } else if (!Verhoeff.ValidateVerhoeff(edt_aadhar.text.toString().trim())){
            edt_aadhar.setFocusable(true);
            edt_aadhar.setFocusableInTouchMode(true);
            edt_aadhar.requestFocus();
            edt_aadhar.setError("Please enter valid Aadhar card number")
        } else {
            isVerify=false
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mProfileDetailPresenter.profileSendEmailAadhar(this@OptionalProfileUser, Utils.loadPrefrence(Constant.USER_ID, "", this@OptionalProfileUser), "", edt_aadhar.text.toString(), "aadhar")
        }
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == OPTIONAL_USER_CODE_INTEREST) {
            if (resultCode == Activity.RESULT_OK) {
                userInterestIds = data!!.getStringExtra(OPTIONAL_USER_INTEREST_RESULT_IDS)
                selectCategoryValues.setText(data!!.getStringExtra(OPTIONAL_USER_INTEREST_RESULT))
                //Log.e("resultString", "" + userInterestIds)
            }
        }
        if (requestCode == FACEBOOK_LOGIN) {
            isUserFbAuth = true
            if (isUserFbAuth) {
                iv_fb.setBackgroundDrawable(resources.getDrawable(R.drawable.facebook_selected))
            } else {
                iv_fb.setBackgroundDrawable(resources.getDrawable(R.drawable.facebook_unselected))
            }
        }
        if (requestCode == GOOGLE_LOGIN) {
            isUserGmailAuth = true
            if (isUserGmailAuth) {
                iv_email.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_selected))
            } else {
                iv_email.setBackgroundDrawable(resources.getDrawable(R.drawable.mail_unselected))
            }
        }
        if (requestCode == REQUEST_TYPE_LINKEDIN) {
            try {
                // TODO
                val socialMediaProfilePojo = data?.getParcelableExtra<SocialMediaProfilePojo>("result")
                if (ConnectionDetector(applicationContext).isConnectingToInternet) {
                    if (socialMediaProfilePojo?.profileId != null &&
                            socialMediaProfilePojo?.profileId != "") {
                        // logoutLinkedInUser()
                        isUserLinkedAuth = true
                        println("OnActi ${socialMediaProfilePojo.profileId}")
                    }
                } else {
                    AlertDialogManager().showAlertDialog(this@OptionalProfileUser,
                            Utils.getText(this@OptionalProfileUser, StringConstant.internet_connection_title),
                            Utils.getText(this@OptionalProfileUser, StringConstant.str_check_internet),
                            false, false)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
//---------------------------------------------------------------------------------------------------------------//
}