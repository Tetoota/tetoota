package com.tetoota.fragment.profile

import android.app.Activity
import android.app.Service
import com.tetoota.login.LoginDataResponse
import okhttp3.RequestBody


/**
 * Created by charchit.kasliwal on 18-07-2017.
 */
class ProfileDetailPresenter : ProfileDetailContract.profileApiListener
        , ProfileDetailContract.Presenter {
//---------------------------------------------------------------------------------------------------------------//
    override fun profileSendEmailAadhar(mActivity: Activity, mUserId: String, email: String, aadhar_number: String, type: String) {
        mProfileInteractor.profileSendEmailAadhar(mActivity, mUserId, email, aadhar_number, type)
    }

    override fun onProfileSendEmailApiSuccessResult(message: String) {
        mProfileContract.onProfileSendEmailApiSuccessResult(message)
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
        mProfileContract.onProfileSendEmailApiFailureResult(message)
    }
//---------------------------------------------------------------------------------------------------------------//
    override fun updateOptionalInformationApi(mActivity: Activity, mUserId: String, email: String, aadhar_number: String, categories_ids: String) {
        mProfileInteractor.updateOptionalInformation(mActivity, mUserId, email, aadhar_number, categories_ids)

    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
        mProfileContract.onOptionalInformationApiSuccessResult(message)
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
        mProfileContract.onOptionalInformationApiFailureResult(message)
    }


    var mProfileContract: ProfileDetailContract.View
    private val mProfileInteractor: ProfileDetailInteractor by lazy {
        com.tetoota.fragment.profile.ProfileDetailInteractor(this)
    }

    constructor(mProfileContract: ProfileDetailContract.View) {
        this.mProfileContract = mProfileContract
    }

    override fun getUserProfileData(mActivity: Activity, personData: LoginDataResponse) {
        if (mActivity != null) {
            mProfileInteractor.profileDataApiCall(mActivity, personData)
        }
    }

    override fun getUserProfileDataForService(mService: Service, personData: LoginDataResponse) {
        if (mService != null) {
            mProfileInteractor.profileDataApiCallForService(mService, personData)
        }
    }
    override fun getUserDetailShow(mActivity : Activity, mUserId : String) {
        if (mActivity != null) {
            mProfileInteractor.showUserDetailService(mActivity, mUserId)
        }
    }

    override fun updateUserProfileData(mActivity: Activity, data: HashMap<String, RequestBody>, mImagePath: String?) {
        if (mActivity != null) {
            mProfileInteractor.updateUserProfileAPI(mActivity, data, mImagePath)
        }
    }

    override fun onProfileApISuccess(profileData: List<ProfileDataResponse?>?, mesg: String) {
        mProfileContract.onProfileApiSuccessResult(profileData, mesg)
    }

    override fun onProfileApiFailure(message: String, apiCallMethod: String) {
        mProfileContract.onProfileApiFailureResult(message, apiCallMethod)
    }
    override fun onShowUserApISuccess(profileData: List<ProfileDataResponse?>?, mesg: String) {
        mProfileContract.onShowUserApiSuccessResult(profileData, mesg)
    }

    override fun onShowUserApiFailure(message: String, apiCallMethod: String) {
        mProfileContract.onShowUserApiFailureResult(message, apiCallMethod)
    }


}