package com.tetoota.fragment.corporateLogin

import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.tetoota.TetootaApplication
import com.tetoota.fragment.corporateLogin.data.CompanyDataResponse
import com.tetoota.fragment.corporateLogin.data.SubmitCorporateLoginResponse
import com.tetoota.fragment.dashboard.DashSliderResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class CorporateLoginInteractor {

    var mCorporateLoginApIListener : CorporateLoginContract.CorporateLoginApIListener
    constructor(mCorporateLoginApIListener: CorporateLoginContract.CorporateLoginApIListener) {
        this.mCorporateLoginApIListener = mCorporateLoginApIListener
    }

    /**
     * Method to GEt Call API From Company DATA
     */
    fun getCompanyData(mActivity : Activity){
        var call: Call<CompanyDataResponse> = TetootaApplication.getHeader()
                .getCompany(Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),"0")
        call.enqueue(object : Callback<CompanyDataResponse> {
            override fun onResponse(call: Call<CompanyDataResponse>?,
                                    response: Response<CompanyDataResponse>?) {
                Log.d("response", ""+Gson().toJson(response?.body()))
                var  companyDataResponse=response?.body();
                if(response?.code() == 200){
                    if(response.body()?.data?.size!! > 0){
                        mCorporateLoginApIListener.onCorporateLoginSuccessResult(companyDataResponse?.data)
                      }else{
                        mCorporateLoginApIListener.onCorporateLoginApiFailureResult(response.body()?.meta?.message.toString())
                    }
                }else{
                    mCorporateLoginApIListener.onCorporateLoginApiFailureResult(response?.body()?.meta?.message.toString())
                }
            }

            override fun onFailure(call: Call<CompanyDataResponse>?, t: Throwable?) {
                try {
                    mCorporateLoginApIListener.onCorporateLoginApiFailureResult(t?.message.toString())
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        })
    }

    fun submitCorporateLogin(mActivity : Activity, userId : String, companyId : String, companyCode : String){
        var call: Call<SubmitCorporateLoginResponse> = TetootaApplication.getHeader()
                .submitCorporate(Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN,"",mActivity),userId, companyId, companyCode)
        call.enqueue(object : Callback<SubmitCorporateLoginResponse> {
            override fun onResponse(call: Call<SubmitCorporateLoginResponse>?,
                                    response: Response<SubmitCorporateLoginResponse>?) {
                Log.d("response", ""+Gson().toJson(response?.body()))
                var  submitCorporateLoginResponse=response?.body();
                if(response?.code() == 200){
                    if(response.body()?.data!=null){
                        submitCorporateLoginResponse?.data?.companyId?.let { Utils.savePreferences(Constant.COMPANY_ID, it, mActivity) }
                        mCorporateLoginApIListener.onCorporateLoginSubmitSuccessResult(true)

                    }else{
                        mCorporateLoginApIListener.onCorporateLoginSubmitSuccessResult(false)
                    }
                }else{
                    mCorporateLoginApIListener.onCorporateLoginSubmitSuccessResult(false)
                }
            }

            override fun onFailure(call: Call<SubmitCorporateLoginResponse>?, t: Throwable?) {
                mCorporateLoginApIListener.onCorporateLoginSubmitSuccessResult(false)
            }
        })
    }

}