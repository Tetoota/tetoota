package com.tetoota.fragment.corporateLogin.data

import com.google.gson.annotations.SerializedName

class CompanyDataResponse {

    @field:SerializedName("data")
    val data: List<CompanyDataItem?>? = null

    @field:SerializedName("meta")
    val meta: Meta? = null
}