package com.tetoota.fragment.corporateLogin.data

import com.google.gson.annotations.SerializedName

class SubmitCorporateLoginItem {

    @field:SerializedName("user_id")
    var userId: String? = null

    @field:SerializedName("company_id")
    var companyId: String? = null
}