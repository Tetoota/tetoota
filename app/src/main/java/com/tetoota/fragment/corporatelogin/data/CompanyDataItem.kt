package com.tetoota.fragment.corporateLogin.data

import com.google.gson.annotations.SerializedName

class CompanyDataItem {
    @field:SerializedName("id")
    var id: String? = null

    @field:SerializedName("name")
    var name: String? = null

    @field:SerializedName("code")
    var code: String? = null
}