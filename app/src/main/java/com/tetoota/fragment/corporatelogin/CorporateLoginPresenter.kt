package com.tetoota.fragment.corporateLogin

import android.app.Activity
import com.tetoota.fragment.corporateLogin.data.CompanyDataItem
import java.lang.Exception

class CorporateLoginPresenter : CorporateLoginContract.Presenter, CorporateLoginContract.CorporateLoginApIListener {
    override fun onCorporateLoginSubmitSuccessResult(result: Boolean) {
        mCorporateLoginContract.onCorporateLoginSubmitSuccessResult(result)
    }

    override fun submitCorporateLogin(mActivity: Activity, userId: String, companyId: String, companyCode: String) {
        mCorporateLoginInteractor.submitCorporateLogin(mActivity, userId, companyId, companyCode)
    }

    override fun onCorporateLoginSuccessResult(mCompanyList: List<CompanyDataItem?>?) {
        mCompanyList?.let { mCorporateLoginContract.onCorporateLoginSuccessResult(it) }
    }


    var mCorporateLoginContract: CorporateLoginContract.View
    private val mCorporateLoginInteractor: CorporateLoginInteractor by lazy {
        CorporateLoginInteractor(this)
    }

    constructor(mCorporateLoginContract: CorporateLoginContract.View) {
        this.mCorporateLoginContract = mCorporateLoginContract
    }


    override fun onCorporateLoginApiFailureResult(message: String) {
    }


    override fun getCompanyData(mActivity: Activity) {
        mCorporateLoginInteractor.getCompanyData(mActivity)
    }
}