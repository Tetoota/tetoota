package com.tetoota.fragment.corporateLogin

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.tetoota.R
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.corporateLogin.data.CompanyDataItem
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_corporate_login.*
import android.widget.Toast
import com.tetoota.main.MainActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_corporate_login.view.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.toast

class CorporateLoginFragment : BaseFragment(), CorporateLoginContract.View {

    //var companyList=ArrayList<CompanyDataItem>()
    private lateinit var companyList: List<CompanyDataItem>
    private var companyId: String? = null;

    private val mCorporateLoginPresenter: CorporateLoginPresenter by lazy {
        CorporateLoginPresenter(this@CorporateLoginFragment)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_corporate_login, container, false)

        showProgressDialog(Utils.getText(context, StringConstant.please_wait))
        mCorporateLoginPresenter.getCompanyData(this.activity!!)


        view.corporateLogincountryCode.hint = Utils.getText(this.activity!!, StringConstant.company_code)
        view.corporateLoginSubmitBtn.text = Utils.getText(this.activity!!, StringConstant.str_submit)
        return view
    }

    override fun onCorporateLoginSuccessResult(mCompanyList: List<CompanyDataItem?>) {
        hideProgressDialog()
        companyList = mCompanyList as List<CompanyDataItem>;
        var list = ArrayList<String>();
        list.add( Utils.getText(this.activity!!, StringConstant.select_company))
        for (value in mCompanyList) {
            /*   value?.name?.let { list.add(it) }*/
            list.add(value.name!!)
        }
        val userAdapter = ArrayAdapter(activity!!, android.R.layout.simple_spinner_dropdown_item, list)
        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        corporateLoginSpinner.setSelection(userAdapter.getCount())
        corporateLoginSpinner.adapter = userAdapter

        corporateLoginSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (position > 0) {
                    companyId = companyList.get(position - 1).id

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        })

        corporateLoginSubmitBtn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                setUpValidation()
            }
        })
    }

    public fun newInstance(): CorporateLoginFragment {
        val fragment = CorporateLoginFragment()
        return fragment
    }

    private fun setUpValidation() {
        if (companyId == null) {
            Toast.makeText(activity, Utils.getText(context, StringConstant.str_corporate_select_validation), Toast.LENGTH_SHORT)
                    .show()
        } else if (TextUtils.isEmpty(corporateLogincountryCode.text.toString())) {
            corporateLogincountryCode.setError(Utils.getText(context, StringConstant.str_select_company_code))
        } else {
            if (Utils.haveNetworkConnection(this.activity!!)) {
                showProgressDialog(Utils.getText(context, StringConstant.please_wait))
                mCorporateLoginPresenter.submitCorporateLogin(this.activity!!, Utils.loadPrefrence(Constant.USER_ID, "", activity), companyId!!, corporateLogincountryCode.text.toString())
            } else {
                activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
            }
        }
    }

    override fun onCorporateLoginSubmitSuccessResult(result: Boolean) {
        hideProgressDialog()
        if (result) {
            hideProgressDialog()
            val i = Intent(activity, MainActivity::class.java)
            //  i.putExtra("lang",true)
            startActivity(i)
            activity?.finish()
        } else {
            Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT)
                    .show()
        }
    }
}