package com.tetoota.fragment.corporateLogin

import android.app.Activity
import com.tetoota.fragment.corporateLogin.data.CompanyDataItem

class CorporateLoginContract {
    interface View {
        fun onCorporateLoginSuccessResult(mCompanyList: List<CompanyDataItem?>) {}
        fun onCorporateLoginSubmitSuccessResult(result: Boolean) {}
        fun onCorporateLoginApiFailureResult(message: String) {}
    }

    internal interface Presenter {
        fun getCompanyData(mActivity: Activity)
        fun submitCorporateLogin(mActivity : Activity, userId : String, companyId : String, companyCode : String)
    }

    interface CorporateLoginApIListener {
        fun onCorporateLoginSuccessResult(mCompanyList: List<CompanyDataItem?>?)
        fun onCorporateLoginSubmitSuccessResult(result: Boolean)
        fun onCorporateLoginApiFailureResult(message: String)

    }
}