package com.tetoota.fragment.corporateLogin.data

import com.google.gson.annotations.SerializedName

class   SubmitCorporateLoginResponse {

    @field:SerializedName("data")
    val data: SubmitCorporateLoginItem? = null

    @field:SerializedName("meta")
    val meta: Meta? = null
}