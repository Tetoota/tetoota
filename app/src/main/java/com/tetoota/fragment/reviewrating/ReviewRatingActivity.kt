package com.tetoota.fragment.reviewrating

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.fragment.postonsocialmedia.PostSocialMedia
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_review_rating.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

class ReviewRatingActivity :  BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_rating)

        initViews()

    }
    private fun initViews(): Unit {
        toolbar_title.text = Utils.getText(this, StringConstant.offer_more)
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        tv_friendTradeYouEarn.text=Utils.getText(this, StringConstant.offer_more_text1)
        tv_friendTradeYouEarn_sec.text=Utils.getText(this, StringConstant.offer_more_text2)
        offer_more_text1.text=Utils.getText(this, StringConstant.offer_more_text_first)
        offer_more_text2.text=Utils.getText(this, StringConstant.offer_more_text_second)
    }
    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ReviewRatingActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@ReviewRatingActivity)
        finish()
    }
}
