package com.tetoota.fragment.tabs

import android.Manifest
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.view.ViewCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupMenu
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.appintro.DashboardSliderAdapter
import com.tetoota.customviews.StartConversationDialog
import com.tetoota.details.ProductDetailActivity
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.DashboardFragment
import com.tetoota.fragment.dashboard.*
import com.tetoota.fragment.dashboardnew.HomeFragmentAdapter
import com.tetoota.fragment.home.data.ContactUploadResponse
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.fragment.inbox.ProposalMessageData
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.nv_view
import kotlinx.android.synthetic.main.fragment_home.progress_view
import kotlinx.android.synthetic.main.fragment_home.view_list
import kotlinx.android.synthetic.main.fragment_wishlist.*
import kotlinx.android.synthetic.main.view_pager_row.*
import org.jetbrains.anko.toast

class HomeFragment : BaseFragment(), HomeContract.View, ViewPager.OnPageChangeListener,
        HomeFragmentAdapter.IAdapterClickListener,  StartConversationDialog.IDialogListener {
    private var tetootaApplication: TetootaApplication? = null

    override fun onInvalidTextAPiSuccess(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onInvalidTextAPiFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingSuccess(mServiceData: ArrayList<ServicesDataResponse>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetTrendingFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalSuccess(mProposalMesgData: ArrayList<ProposalMessageData>, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun ongetIncompleteProposalFailure(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private lateinit var mHomeFragmentAdapter: HomeFragmentAdapter
    val mAdapter: DashboardSliderAdapter by lazy {
        DashboardSliderAdapter(this!!.activity!!)
    }
    private val mHomePresenter: HomePresenter by lazy {
        HomePresenter(this@HomeFragment)
    }
    // TODO: Rename and change types of parameters
    private var dotsCount: Int = 0
    private var mParentListener: OnChildFragmentInteractionListener? = null
    private var menu: Menu? = null
    private var intent: Intent? = null
    var exchangePostType: String? = ""

    var dots = arrayOfNulls<ImageView>(dotsCount)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        tetootaApplication = activity as TetootaApplication

        TetootaApplication.setTextValuesHashMap(Utils.getLangByCode(activity,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", activity)))

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pager_introduction.adapter = mAdapter
        pager_introduction.currentItem = 0
        pager_introduction.addOnPageChangeListener(this@HomeFragment)
        view_list.layoutManager = LinearLayoutManager(context)
        view_list.setHasFixedSize(true)
        view_list.isNestedScrollingEnabled = false
        mHomeFragmentAdapter = HomeFragmentAdapter(activity!!, iAdapterClickListener = this,
                screenHeight = heightCalculation())
        getAllDashboardList()
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"


        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    /**
     * Method for height calculation
     */
    private fun heightCalculation(): Int {
        val getTopMarginH = (Utils.getScreenHeight(this.activity!!) * .3f).toInt()
        return getTopMarginH
    }

    override fun onStop() {
        super.onStop()
        if (HomeFragment().isVisible) {
            pager_introduction.stopAutoScroll()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (HomeFragment().isVisible) {
            pager_introduction.stopAutoScroll()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        println("On Destroy View")
        if (!DashboardFragment().isVisible) {
            pager_introduction.stopAutoScroll()
        }
    }

    private fun setUiPageViewController() {
        // dotsCount = mAdapter.count
        dots = arrayOfNulls(dotsCount)
        for (i in 0..dotsCount - 1) {
            dots[i] = ImageView(activity)
            dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
            val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(4, 0, 4, 0)
            viewPagerCountDots.addView(dots[i], params)
        } // Commit Testing
        dots[0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(p0: Int) {
        for (i in 0..dotsCount - 1) {
            try {
                dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
                dots[p0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
                if (p0 + 1 == dotsCount) {
                } else {
                    // btn_next.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun cellItemClick(mViewType: String, mString: String, cellRow: Any, mAttributeValue: String, mView: View, p1: Int) {
        if (mViewType == "top") {
            nv_view.fullScroll(View.FOCUS_UP)
            // view_list.smoothScrollToPosition(0)
        } else if (mViewType == "trending") {
            val mProductData = cellRow as ServicesDataResponse
            if (mString == "favorite") {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    mHomePresenter.markFavorite(this.activity!!, mProductData, mAttributeValue, p1)
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            } else if (mString == "cellClick") {
                activity!!.toast("Under Development")
            } else if (mString == "shareClick") {
                showPopupMenu(mView, mProductData, mAttributeValue, p1)
            }
        } else if (mViewType == "service") {
            val mProductData = cellRow as ServicesDataResponse
            if (mString == "favorite") {
                showPopupMenu(mView, mProductData, mAttributeValue, p1)
            } else if (mString == "cellClick") {
                activity!!.toast("Under Development")
            } else if (mString == "shareClick") {
                var url: String = "";
                if (mProductData.image != null && mProductData.image_thumb != null) {
                    url = Utils.getUrl(this.activity!!, mProductData.image, mProductData.image_thumb, false)
                } else {
                    url = Utils.getUrl(this.activity!!, mProductData.image!!)
                }
                shareProduct(mProductData.title!!, url)
            } else if (mString == "seeAll") {
                mParentListener?.messageFromChildToParent("service")
            } else if (mString.equals("Cell Item click")) {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = Intent(context, ProductDetailActivity::class.java)
                    intent.putExtra("mProductData", mProductData)
                    intent.putExtra("productType", "services")
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        val transitionActivityOptions: ActivityOptions =
                                ActivityOptions.makeSceneTransitionAnimation(activity,
                                        mView, "iv_image")
                        startActivity(intent, transitionActivityOptions.toBundle())
                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    } else {
                        startActivity(intent)
                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    }
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
        } else if (mViewType == "product") {
            val mProductData = cellRow as ServicesDataResponse
            if (mString == "favorite") {
                showPopupMenu(mView, mProductData, mAttributeValue, p1)
            } else if (mString == "cellClick") {
                activity!!.toast("Under Development")
            } else if (mString == "shareClick") {
                var url: String = "";
                if (mProductData.image != null && mProductData.image_thumb != null) {
                    url = Utils.getUrl(this.activity!!, mProductData.image, mProductData.image_thumb, false)
                } else {
                    url = Utils.getUrl(this.activity!!, mProductData.image!!)
                }
                shareProduct(mProductData.title!!, url)
            } else if (mString == "seeAll") {
                mParentListener?.messageFromChildToParent("product")
            } else if (mString.equals("Cell Item click")) {
                if (Utils.haveNetworkConnection(this.activity!!)) {
                    val intent = Intent(context, ProductDetailActivity::class.java)
                    intent.putExtra("mProductData", mProductData)
                    intent.putExtra("productType", "product")
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        val transitionActivityOptions: ActivityOptions =
                                ActivityOptions.makeSceneTransitionAnimation(activity,
                                        mView, "iv_image")
                        startActivity(intent, transitionActivityOptions.toBundle())
                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    } else {
                        startActivity(intent)
                        activity!!.overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    }
                } else {
                    activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                }
            }
        }
    }

    override fun onDashboardApiSuccessResult(mSliderList: ArrayList<Any>, message: String) {
        if (message == "sliderList") {
            progress_view.visibility = View.GONE
            rl_viewpager.visibility = View.VISIBLE
            val mDashBoardSliderDataResponse: ArrayList<Any> = mSliderList
            dotsCount = mSliderList.size
            setUiPageViewController()
            mAdapter.setElements(mDashBoardSliderDataResponse as ArrayList<DashboardSliderDataResponse>)
            mAdapter.notifyDataSetChanged()
            pager_introduction.interval = 2000
            pager_introduction.startAutoScroll()
            pager_introduction.currentItem = Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % mSliderList.size

        } else if (message == "homeScreenList") {
            println("Home Screen List PushNotificationDataResponse")
            val mHomeScreenList = mSliderList as ArrayList<HomeDataResponse>
        }
    }

    override fun onDashboardApiFailureResult(message: String) {
        if (activity != null) {
            progress_view.visibility = View.GONE
            activity?.toast(message)
        }
    }

    override fun favoriteApiResult(message: String, cellRow: ServicesDataResponse, p1: Int) {
        if (cellRow.is_favourite == 0) {
            cellRow.is_favourite = 1
            activity?.toast("Mark Favorite")
        } else {
            cellRow.is_favourite = 0
            activity?.toast("Mark Unfavorite")
        }
        Utils.savePreferences(Constant.VERSION, cellRow.version!!, this!!.context!!)
        mHomeFragmentAdapter.notifyItemChanged(p1)
    }

    /**
     *
     */
    fun getAllDashboardList(): Unit {
        if (ActivityCompat.checkSelfPermission(this.activity!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this!!.activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        if (Utils.haveNetworkConnection(this.activity!!)) {
            progress_view.visibility = View.VISIBLE
            mHomePresenter.getDashboardSliderData(this.activity!!, tetootaApplication!!.myLatitude, tetootaApplication!!.myLongitude )
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
        }
    }

    fun shareProduct(mTitle: String, mImageUrl: String) {
        val text = mTitle
        val pictureUri = Uri.parse(mImageUrl)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.putExtra(Intent.EXTRA_STREAM, pictureUri)
        shareIntent.type = "image/*"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(shareIntent, "Share images..."))
    }

    /**
     * Method To Create Popup Menu
     * for share, Favorite, Report
     */
    private fun showPopupMenu(view: View, mProductData: ServicesDataResponse, mAttributeValue: String, p1: Int) {
        val popup = PopupMenu(activity, view)
        popup.menuInflater.inflate(R.menu.item_popup_row, popup.menu)
        this.menu = popup.menu
        val favItem = menu?.findItem(R.id.menu_favorite)
        if (mAttributeValue == "0") {
            favItem?.title = Utils.getText(context, StringConstant.home_unfavourite)
        } else {
            favItem?.title = Utils.getText(context, StringConstant.favorite)
        }
        menu?.findItem(R.id.menu_share)!!.title = Utils.getText(context, StringConstant.share_text)
        menu?.findItem(R.id.menu_report_this)!!.title = Utils.getText(context, StringConstant.report_this)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_share -> {
                    var url: String = "";
                    if (mProductData.image != null && mProductData.image_thumb != null) {
                        url = Utils.getUrl(this.activity!!, mProductData.image, mProductData.image_thumb, false)
                    } else {
                        url = Utils.getUrl(this.activity!!, mProductData.image!!)
                    }
                    shareProduct(mProductData.title!!, url)
                }
                R.id.menu_favorite -> {
                    if (Utils.haveNetworkConnection(this.activity!!)) {
                        mHomePresenter.markFavorite(this.activity!!, mProductData, mAttributeValue, p1)
                    } else {
                        activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
                    }
                }
                R.id.menu_report_this -> {
                    StartConversationDialog(context, Constant.DIALOG_LOGIN_FAILURE_ALERT, mProductData.id.toString(),
                            this@HomeFragment, getString(R.string.str_report_issue), intent!!.putExtra("productType", ""), exchangePostType!!).show()
                }
            }
            true
        }
        popup.show()
    }

    override fun onDashboardApiSuccessResult(mCategoriesList: ArrayList<Any>, arrayList: ArrayList<Any>, message: String) {
        println("DATA")
        if (view != null) {
            progress_view.visibility = View.GONE
            //       hideProgressDialog()
            mHomeFragmentAdapter = HomeFragmentAdapter(activity!!, iAdapterClickListener = this,
                    screenHeight = heightCalculation())
            view_list.adapter = mHomeFragmentAdapter
            rl_viewpager.visibility = View.VISIBLE
            val mDashBoardSliderDataResponse: ArrayList<Any> = arrayList
            dotsCount = arrayList.size
            setUiPageViewController()
            mAdapter.setElements(mDashBoardSliderDataResponse as ArrayList<DashboardSliderDataResponse>)
            mAdapter.notifyDataSetChanged()
            pager_introduction.interval = 2000
            pager_introduction.startAutoScroll()
            pager_introduction.currentItem = Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % arrayList.size

            mHomeFragmentAdapter.setElements(mCategoriesList, arrayList as ArrayList<DashboardSliderDataResponse>)
            mHomeFragmentAdapter.notifyDataSetChanged()
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        mParentListener = null
    }


    interface OnChildFragmentInteractionListener {
        fun messageFromChildToParent(mLastCommitFragment: String)
    }

    fun setmListener(mParentListener: OnChildFragmentInteractionListener) {
        this.mParentListener = mParentListener
    }

    override fun onYesPress(param: String, message: String, mServiceId: String, mMesgDesc: String) {
        Utils.hideSoftKeyboard(this.activity!!)
        if (message == "report") {
            showProgressDialog(Utils.getText(context, StringConstant.please_wait))
            mHomePresenter.report(this.activity!!, mServiceId, mMesgDesc)
        }
    }

    override fun onReportApiSuccess(key: String, message: String) {
        super.onReportApiSuccess(key, message)
        hideProgressDialog()
        if (key == "success") {
            activity!!.toast(message)
        } else {
            activity!!.toast(message)
        }
    }

    override fun onContactUploadedSuccess(contactUploadResponse: ContactUploadResponse?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onContactUploadedFailure(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiSuccessResult(mDataList: List<DataItem>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onRecentactivityAPiFailureResult(message: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}// Required empty public constructor
