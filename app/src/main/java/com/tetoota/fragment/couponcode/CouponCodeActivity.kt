package com.tetoota.fragment.couponcode

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.databinding.DataBindingUtil.setContentView
import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.coupon_code.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.toast

class CouponCodeActivity: BaseActivity(), RedeemCouponsContract.View, View.OnClickListener, ProfileDetailContract.View {
    override fun onProfileSendEmailApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
    }
    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, CouponCodeActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }
    override fun onClick(p0: View?) {
        when (p0) {
            btn_applyCouponCode -> {
                if (validation()) {
                    getRedeemCouponsCodeApiCall()
                }
            }
        }
    }

    fun validation(): Boolean {
        if (et_referralCode.text.toString().isNullOrEmpty()) {
            toast(Utils.getText(this, StringConstant.enter_referral_code_alert))
            return false
        }
        return true
    }

    override fun onRedeemCouponsSuccess(mString: String, mResponseMessage: String) {
//        progress_view.visibility = View.GONE
        et_referralCode.setText("")
        openDailog("Success", mResponseMessage)
        callUserDetailAPI()
    }

    override fun onRedeemCouponsFailureResponse(mString: String, isServerError: Boolean) {
        progress_view.visibility = View.GONE
        openDailog("Failure", mString)
    }

    val mRedeemCouponsPresenter: RedeemCouponsPresenter by lazy {
        RedeemCouponsPresenter(this@CouponCodeActivity)
    }

    fun getRedeemCouponsCodeApiCall() {
        Utils.hideSoftKeyboard(this)
        if (Utils.haveNetworkConnection(this)) {
            progress_view.visibility = View.VISIBLE
            mRedeemCouponsPresenter.getRedeemCouponsCode(this, et_referralCode.text.toString())
        } else {
            toast(Utils.getText(this, StringConstant.str_check_internet))
        }
    }
    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@CouponCodeActivity)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.coupon_code)
        setMultiLanguageText()
        btn_applyCouponCode.setOnClickListener(this)

    }


    private fun setMultiLanguageText() {
        et_referralCode.hint = Utils.getText(this, StringConstant.placeholder_et_referralCode)
        btn_applyCouponCode.text = Utils.getText(this, StringConstant.applyCouponCode)
        tv_newCouponcode.text = Utils.getText(this, StringConstant.new_couponcode)
        tv_unlockCoupon.text = Utils.getText(this, StringConstant.unblock_coupon)//+Utils.getText(context,StringConstant.each)
        //  et_referralCode.hint = Utils.getText(context,StringConstant.enter_referral_code)
    }
    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        progress_view.visibility = View.GONE
        val profileData: ProfileDataResponse = message?.get(0)!!
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        personData.tetoota_points = profileData.tetoota_points
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this)
    }
    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun callUserDetailAPI() {
        if (Utils.haveNetworkConnection(this)) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileData(this, personData)
        }
    }

    private fun openDailog(header: String, mResponseMsg: String) {
        val alertDialog = AlertDialog.Builder(this).create() //Read Update
//        alertDialog.setTitle(header)
        alertDialog.setMessage(mResponseMsg)
        alertDialog.setButton("Ok", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                alertDialog.dismiss()
            }
        })
        alertDialog.show()  //<-- See This!
    }
    override fun onResume() {
        super.onResume()
        initViews()
    }
    private fun initViews() {
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        toolbar_title.text = Utils.getText(this, StringConstant.redeem_coupons)
    }
}