package com.tetoota.fragment.couponcode

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import com.google.gson.Gson
import com.tetoota.R
import com.tetoota.fragment.BaseFragment
import com.tetoota.fragment.profile.ProfileDataResponse
import com.tetoota.fragment.profile.ProfileDetailContract
import com.tetoota.fragment.profile.ProfileDetailPresenter
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.coupon_code.*
import org.jetbrains.anko.toast

/**
 * Created by charchit.kasliwal on 17-10-2017.
 */
class CouponCodeFragment : BaseFragment(), RedeemCouponsContract.View, View.OnClickListener, ProfileDetailContract.View {
    override fun onProfileSendEmailApiSuccessResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProfileSendEmailApiFailureResult(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOptionalInformationApiSuccessResult(message: String) {
    }

    override fun onOptionalInformationApiFailureResult(message: String) {
    }

    override fun onShowUserApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
    }

    override fun onShowUserApiFailureResult(message: String, apiCallMethod: String) {
    }

    private val mProfileDetailPresenter: ProfileDetailPresenter by lazy {
        ProfileDetailPresenter(this@CouponCodeFragment)
    }

    override fun onClick(p0: View?) {
        when (p0) {
            btn_applyCouponCode -> {
                if (validation()) {
                    getRedeemCouponsCodeApiCall()
                }
            }
        }
    }

    fun validation(): Boolean {
        if (et_referralCode.text.toString().isNullOrEmpty()) {
            context?.toast(Utils.getText(context, StringConstant.enter_referral_code_alert))
            return false
        }
        return true
    }

    override fun onRedeemCouponsSuccess(mString: String, mResponseMessage: String) {
//        progress_view.visibility = View.GONE
        et_referralCode.setText("")
        openDailog("Success", mResponseMessage)
        callUserDetailAPI()
    }

    override fun onRedeemCouponsFailureResponse(mString: String, isServerError: Boolean) {
        progress_view.visibility = View.GONE
        openDailog("Failure", mString)
    }

    val mRedeemCouponsPresenter: RedeemCouponsPresenter by lazy {
        RedeemCouponsPresenter(this@CouponCodeFragment)
    }

    fun getRedeemCouponsCodeApiCall() {
        Utils.hideSoftKeyboard(this.activity!!)
        if (Utils.haveNetworkConnection(this.activity!!)) {
            progress_view.visibility = View.VISIBLE
            mRedeemCouponsPresenter.getRedeemCouponsCode(this.activity!!, et_referralCode.text.toString())
        } else {
            activity!!.toast(Utils.getText(context, StringConstant.str_check_internet))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.coupon_code, container, false)
        return view

    }

    private fun setMultiLanguageText() {
        et_referralCode.hint = Utils.getText(context, StringConstant.placeholder_et_referralCode)
        btn_applyCouponCode.text = Utils.getText(context, StringConstant.applyCouponCode)
        tv_newCouponcode.text = Utils.getText(context, StringConstant.new_couponcode)
        tv_unlockCoupon.text = Utils.getText(context, StringConstant.unblock_coupon)//+Utils.getText(context,StringConstant.each)
        //  et_referralCode.hint = Utils.getText(context,StringConstant.enter_referral_code)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMultiLanguageText()
        btn_applyCouponCode.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.clear();
        super.onPrepareOptionsMenu(menu)
    }

    override fun onProfileApiSuccessResult(message: List<ProfileDataResponse?>?, mesg: String) {
        progress_view.visibility = View.GONE
        val profileData: ProfileDataResponse = message?.get(0)!!
        val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", context)
        val personData = Gson().fromJson(json, LoginDataResponse::class.java)
        personData.tetoota_points = profileData.tetoota_points
        val json1 = Gson().toJson(personData)
        Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json1, this.context!!)
    }

    override fun onProfileApiFailureResult(message: String, apiCallMethod: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun callUserDetailAPI() {
        if (Utils.haveNetworkConnection(this.context!!)) {
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this.context!!)
            val personData = Gson().fromJson(json, LoginDataResponse::class.java)
            mProfileDetailPresenter.getUserProfileData(this.activity!!, personData)
        }
    }

    private fun openDailog(header: String, mResponseMsg: String) {
        val alertDialog = AlertDialog.Builder(context).create() //Read Update
//        alertDialog.setTitle(header)
        alertDialog.setMessage(mResponseMsg)
        alertDialog.setButton("Ok", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                alertDialog.dismiss()
            }
        })
        alertDialog.show()  //<-- See This!
    }
}