package com.tetoota.database;

import android.provider.BaseColumns;


public final class IDbKey {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private IDbKey() {
    }

    /* Inner class that defines the table contents */
    public static class LanguageEntry implements BaseColumns {
        public static final String TABLE_NAME = "LanguageMaster";
        public static final String COLUMN_NAME_id = "id";
        public static final String COLUMN_NAME_langCode = "langCode";                            //1
        public static final String COLUMN_NAME_labelkey = "labelkey";                            //2
        public static final String COLUMN_NAME_labelVal = "labelVal";                            //3
        public static final String COLUMN_NAME_updatedDate = "updatedDate";                      //4

    }

}
