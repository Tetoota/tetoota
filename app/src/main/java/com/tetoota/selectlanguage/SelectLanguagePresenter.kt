package com.tetoota.selectlanguage
import android.app.Activity

/**
 * Created by charchit.kasliwal on 01-06-2017.
 */
class SelectLanguagePresenter : SelectLanguageContract.Presenter,
     SelectLanguageContract.ApiListener{
    var mLanguageContractView : SelectLanguageContract.View

    private val mSelectLanguageInteractor : SelectLanguageInteractor by lazy {
        SelectLanguageInteractor(this)
    }

    constructor(mLanguageContractView: SelectLanguageContract.View){
        this.mLanguageContractView = mLanguageContractView
    }
    override fun getLanguageData() {
        mSelectLanguageInteractor.getLanguageDataFromApi()
    }

    override fun getSelectedLanguageData(mActivity : Activity) {
        mSelectLanguageInteractor.getSelectedLanguageData(mActivity)
    }
    override fun onLanguageApiSuccess(mDataList: List<LanguageDataResponse>, message: String) {
        mLanguageContractView.onSelectLanguageSuccessResult(mDataList,message)
    }

    override fun onLanguageApiFailure(message: String) {
        mLanguageContractView.onSelectLanguageFailureResult(message)
    }

    override fun onSelectedLanguageSuccess(message: String) {
        super.onSelectedLanguageSuccess(message)
        mLanguageContractView.onSelectedLanguageSuccessResult(message)
    }

}