package com.tetoota.selectlanguage

/**
 * Created by charchit.kasliwal on 13-10-2017.
 */
open class SelectedLanguageMeta {
    var code: String? = null
    var status: String? = null
    var message: String? = null
}