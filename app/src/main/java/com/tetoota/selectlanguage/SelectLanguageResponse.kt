package com.tetoota.selectlanguage

import android.os.Parcel
import android.os.Parcelable
import com.tetoota.network.errorModel.Meta

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class SelectLanguageResponse(var meta: Meta,
                             var data: List<LanguageDataResponse>) : Parcelable {
    constructor(source: Parcel) : this(
            source.readParcelable<Meta>(Meta::class.java.classLoader)!!,
            source.createTypedArrayList(LanguageDataResponse.CREATOR)!!
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(meta, 0)
        writeTypedList(data)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<SelectLanguageResponse> = object : Parcelable.Creator<SelectLanguageResponse> {
            override fun createFromParcel(source: Parcel): SelectLanguageResponse = SelectLanguageResponse(source)
            override fun newArray(size: Int): Array<SelectLanguageResponse?> = arrayOfNulls(size)
        }
    }
}
