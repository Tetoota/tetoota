package com.tetoota.selectlanguage

import android.app.Activity

/**
 * Created by charchit.kasliwal on 01-06-2017.
 */
class SelectLanguageContract {
    interface View {
        fun onSelectLanguageSuccessResult(mCategoriesList: List<LanguageDataResponse>?, message: String)
        fun onSelectLanguageFailureResult(message: String)
        fun onSelectedLanguageSuccessResult(message: String){}
    }

    internal interface Presenter {
        fun getLanguageData()
        fun getSelectedLanguageData(mActivity : Activity){}
    }

    interface ApiListener {
        fun onLanguageApiSuccess(mDataList: List<LanguageDataResponse>, message: String)
        fun onLanguageApiFailure(message: String)
        fun onSelectedLanguageSuccess(message: String){}
    }

}