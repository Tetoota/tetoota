package com.tetoota.selectlanguage

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class LanguageDataResponse(var lang_id: String, var lang_name: String, var lang_code: String) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString().toString(),
            source.readString().toString(),
            source.readString().toString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(lang_id)
        writeString(lang_name)
        writeString(lang_code)
    }

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<LanguageDataResponse> = object : Parcelable.Creator<LanguageDataResponse> {
            override fun createFromParcel(source: Parcel): LanguageDataResponse = LanguageDataResponse(source)
            override fun newArray(size: Int): Array<LanguageDataResponse?> = arrayOfNulls(size)
        }
    }
}