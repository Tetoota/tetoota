package com.tetoota.selectlanguage

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.appintro.WalkThroughActivity
import com.tetoota.fragment.help.ShowWebViewActivity
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_select_language.*
import kotlinx.android.synthetic.main.error_layout.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity

class SelectLanguageActivity : BaseActivity(), SelectLanguageContract.View, SelectLanguageAdapter.IAdapterClickListener {
//    lateinit var languageMap : HashMap<String, String>
    var mScreenWidth: Int = 0
    var mScreenHeight: Int = 0
    private val SPLASH_TIME_OUT = 3000
    var mLocalLangList = ArrayList<LanguageDataResponse>()
    lateinit var mSelectLanguageAdapter: SelectLanguageAdapter
    private val mSelectLanguagePresenter: SelectLanguagePresenter by lazy {
        SelectLanguagePresenter(this@SelectLanguageActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_language)
        initViews()

        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        //Log.w(TAG, "getInstanceId failed", task.exception)
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token
                    Utils.setFcmToken(token, this)

                    Log.d("tokennn", token)
                    println("token $token")
                    // Log and toast
                    //val msg = getString(R.string.msg_token_fmt, token)
                    //Log.d(TAG, msg)
                   // Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                })


    }

    override fun onResume() {
        super.onResume()
        mSelectLanguagePresenter.getLanguageData()
     /*   if (Utils.haveNetworkConnection(this@SelectLanguageActivity)) {
            if(Utils.getText(this,StringConstant.please_wait).equals("")){
                showProgressDialog("Please wait..")
            }else{
                showProgressDialog(Utils.getText(this,StringConstant.please_wait))
            }

        } else {
            if(Utils.getText(this,StringConstant.str_check_internet).equals("")){
                showErrorView("Please check internet connection")
                showSnackBar("Please check internet connection")
            }else {
                showErrorView(Utils.getText(this, StringConstant.str_check_internet))
                showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
            }
        }*/
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    /**
     * Method to initialize All UI Controls
     */

    private fun initViews() {
//        languageMap = Utils.Utils.getLangByCode(this,
//                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", this))
        mScreenWidth = Utils.getScreenWidth(this) / 8 + 5
        mSelectLanguageAdapter = SelectLanguageAdapter(iAdapterClickListener = this)
        rl_language_list.layoutManager = LinearLayoutManager(this@SelectLanguageActivity)
        rl_language_list.setHasFixedSize(false)
        rl_language_list.adapter = mSelectLanguageAdapter

      //  setMultiLanguageText()
        btn_next.onClick {
            if (languageValidation()) {
                var s: String = Utils.getPrefFcmToken(this@SelectLanguageActivity)
                println("Device Id $s")
                Log.d("Device", s)
                if(Utils.getText(this,StringConstant.please_wait).equals("")){
                    showProgressDialog("Please wait..")
                }else{
                    showProgressDialog(Utils.getText(this,StringConstant.please_wait))
                }
                onNextButtonClick()
            } else showSnackBar("Please select Language")
        }

        tv_terms_text.onClick {
            val intent = ShowWebViewActivity.newMainIntent(this@SelectLanguageActivity)
            ActivityStack.getInstance(this@SelectLanguageActivity)
            //intent?.putExtra("webUrl", webPageUrl)
            intent?.putExtra("pageHeading", "Terms and Conditions")
            startActivity(intent)        }



        error_btn_retry.onClick {
            if (Utils.haveNetworkConnection(this@SelectLanguageActivity)) {
                if(Utils.getText(this,StringConstant.please_wait).equals("")){
                    showProgressDialog("Please wait..")
                }else{
                    showProgressDialog(Utils.getText(this,StringConstant.please_wait))
                }
                mSelectLanguagePresenter.getLanguageData()
            } else {
                if(Utils.getText(this,StringConstant.str_check_internet).equals("")){
                    showErrorView("Please check internet connection")
                    showSnackBar("Please check internet connection")
                }else {
                    showErrorView(Utils.getText(this, StringConstant.str_check_internet))
                    showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
                }
            }
        }

        edt_searchlanguage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().length == 0) {
                    mSelectLanguageAdapter.setElements(mLocalLangList)
                    mSelectLanguageAdapter.notifyDataSetChanged()
                } else {
                    var mFilterLangList: ArrayList<LanguageDataResponse>?
                            = ArrayList()
                    for (item in mLocalLangList) {
                        if (item.lang_name.toLowerCase().contains(s.toString().toLowerCase())) {
                            mFilterLangList?.add(item)
                        }
                    }
                    mSelectLanguageAdapter.setElements(mFilterLangList!!)
                    mSelectLanguageAdapter.notifyDataSetChanged()
                    // mSelectLanguageAdapter.filter(s.toString())
                }
            }
        })
    }

    /**
     * Function On Click on Next Button
     */
    private fun onNextButtonClick() {
        selectLangAPI()
    }

    private fun selectLangAPI() {
        if (Utils.haveNetworkConnection(this@SelectLanguageActivity)) {
            val selectedLanguate = edt_searchlanguage.text.toString().trim()
                mSelectLanguagePresenter.getSelectedLanguageData(this@SelectLanguageActivity)

        } else {
            if(Utils.getText(this,StringConstant.str_check_internet).equals("")){
                showSnackBar("Please check internet connection")
            }else {
                showSnackBar(Utils.getText(this,StringConstant.str_check_internet))
            }
        }
    }

    /**
     * Function to Check Edit Text Value and Length
     */
    fun languageValidation(): Boolean {
        if(Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"",this).equals("")){
            return false
        }
        return true
    }

    override fun onSelectLanguageSuccessResult(mLanguageList: List<LanguageDataResponse>?,
                                               message: String): Unit {
        hideErrorView()
        hideProgressDialog()
        this.mLocalLangList = mLanguageList as ArrayList<LanguageDataResponse>
        mSelectLanguageAdapter.setElements(mLanguageList)
        mSelectLanguageAdapter.notifyDataSetChanged()
        rl_language_list.visibility = View.VISIBLE
        if(mLocalLangList.size > 0){
            edt_searchlanguage.setText(mLocalLangList.get(0).lang_name.trim())
            Utils.savePreferences(Constant.USER_SELECTED_LANG, mLocalLangList.get(0).lang_code,
                    this@SelectLanguageActivity)
            Utils.hideSoftKeyboard(this@SelectLanguageActivity)
            mSelectLanguageAdapter.setElements(mLocalLangList)
            mSelectLanguageAdapter.notifyDataSetChanged()
        }
    }

    override fun onSelectedLanguageSuccessResult(message: String) {
        super.onSelectedLanguageSuccessResult(message)
        hideProgressDialog()

        Log.e("ffffffffffff","ffffffffffffff" + message)
        if (message == "success") {
            startActivity<WalkThroughActivity>()
            overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
        } else {
            showSnackBar("please try again later")
        }
    }

    override fun onSelectLanguageFailureResult(message: String): Unit {
        hideProgressDialog()
        showErrorView(message)
        showSnackBar(message)
    }

    override fun cellItemClick(mString: String, cellRow: Any) {
        var mLanguageDataResp = cellRow as LanguageDataResponse
        edt_searchlanguage.setText(mLanguageDataResp.lang_name.trim())
        Utils.savePreferences(Constant.USER_SELECTED_LANG, mLanguageDataResp.lang_code,
                this@SelectLanguageActivity)
        Utils.hideSoftKeyboard(this@SelectLanguageActivity)
        mSelectLanguageAdapter.setElements(mLocalLangList)
        mSelectLanguageAdapter.notifyDataSetChanged()
        rl_language_list.visibility = View.VISIBLE
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            rl_maincontent.visibility = View.VISIBLE
        }
    }


    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    private fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            rl_maincontent.visibility = View.GONE
        }
    }

}
