package com.tetoota.listener

/**
 * Created by jitendra.nandiya on 17-08-2017.
 */
interface IFragmentOpenCloseListener {
    abstract fun onFragmentOpenClose(fromFragment: String,toFragment: String, tag : String)
}