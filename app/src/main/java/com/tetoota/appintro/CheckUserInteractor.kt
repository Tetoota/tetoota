package com.tetoota.appintro

import android.app.Activity
import android.provider.Settings
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.categories.DeepLinkingResponse
import com.tetoota.login.LoginResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckUserInteractor {
    var mCheckUserApiListener: CheckUserContract.CheckUserApiListener
    val API_SUCCESS_VALUE = 101
    val API_FAILURE_VALUE = 202

    constructor(mCheckUserApiListener: CheckUserContract.CheckUserApiListener) {
        this.mCheckUserApiListener = mCheckUserApiListener
    }

    /**
     * @param mActivity the m activity
     */
    fun checkUserLoginFromAPI12(mActivity: Activity, mMobileNumber : String) {
        var deviceId : String = ""
        if (Utils.getPrefFcmToken(mActivity) != null && Utils.getPrefFcmToken(mActivity).length > 0) {
            deviceId  = Utils.getPrefFcmToken(mActivity);
        } else {
            deviceId = "No Found"
        }
        var call: Call<LoginResponse> = TetootaApplication.getHeader()
                .checkUser(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),
                        Constant.DEVICE_TYPE, Settings.Secure.getString(mActivity.contentResolver,
                        Settings.Secure.ANDROID_ID),deviceId, mMobileNumber)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                println("On Failure")
                mCheckUserApiListener.onLoginSuccess12(API_SUCCESS_VALUE,t?.message.toString())
            }

            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                var mLoginListData: LoginResponse? = response?.body()
                //onLogin(LoginType.PHONE,mActivity)
                Utils.savePreferences(Constant.USER_AUTH_TOKEN,
                        response?.body()?.data?.auth_token!!,mActivity)
                Utils.savePreferences(Constant.ALL_PUSH_NOTIFICATION,
                        response.body()?.data?.all_notification.toString(),mActivity)
                Utils.savePreferences(Constant.PROPOSAL_PUSH_NOTIFICATION,
                        response.body()?.data?.proposal_notification.toString(),mActivity)
                Utils.savePreferences(Constant.CHAT_PUSH_NOTIFICATION,
                        response.body()?.data?.chat_notification.toString(),mActivity)
                Utils.savePreferences(Constant.IS_VISIBLE,
                        response.body()?.data?.is_visible.toString(),mActivity)
                val json = Gson().toJson(response.body()?.data)
                Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json, mActivity)
                Utils.savePreferences(Constant.IS_USER_LOGGED_IN,"true",mActivity)
                Utils.savePreferences(Constant.USER_ID, response.body()?.data?.user_id!!,mActivity)
                mCheckUserApiListener.onLoginSuccess12(API_SUCCESS_VALUE,
                        mLoginListData?.meta?.message.toString())
            }
        })
    }
    fun makeDeepLinking(mActivity: Activity, authToken : String, user_id : String, user_ip : String){
        var call: Call<DeepLinkingResponse> = TetootaApplication.getHeader()
                .doDeepLinking(Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity)
                        , user_id, user_ip)
        call.enqueue(object : Callback<DeepLinkingResponse> {
            override fun onFailure(call: Call<DeepLinkingResponse>?, t: Throwable?) {
                println("On Failure")
                mCheckUserApiListener.onDeepLinkingResult()
            }

            override fun onResponse(call: Call<DeepLinkingResponse>?, response: Response<DeepLinkingResponse>?) {
                mCheckUserApiListener.onDeepLinkingResult()
            }
        })
    }
}