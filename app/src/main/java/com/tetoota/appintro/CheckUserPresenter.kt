package com.tetoota.appintro

import android.app.Activity

class CheckUserPresenter : CheckUserContract.Presenter, CheckUserContract.CheckUserApiListener {

    var mCheckUserView : CheckUserContract.View
    private val mCheckUserInteractor : CheckUserInteractor by lazy {
        CheckUserInteractor(this)
    }

    constructor(mCheckUserView: CheckUserContract.View){
        this.mCheckUserView = mCheckUserView
    }


    override fun checkUserLoginFromAPI12(mActivity: Activity, number: String) {
        mCheckUserInteractor.checkUserLoginFromAPI12(mActivity,number)
    }

    override fun onLoginSuccess12(message: Int, mesg: String) {
        mCheckUserView.onLoginResult(message,mesg)
    }



    override fun doDeepLinking(mActivity: Activity, authToken : String, user_id : String, user_ip : String) {
        mCheckUserInteractor.makeDeepLinking(mActivity, authToken, user_id, user_ip)
    }

    override fun onDeepLinkingResult() {
        mCheckUserView.onDeepLinkingResult()
    }
}