package com.tetoota.appintro

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.annotation.ColorInt
import android.support.v4.content.ContextCompat
import android.util.SparseArray

import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by charchit.kasliwal on 03-07-2017.
 */
class WalkThroughInteractor  {
    var mWalkThroughListener: WalkThroughContract.ApiListener
 //   private val permissionsListeners = SparseArray<OnCompleteListener>()
  //  private var nextPermissionsRequestCode = 4000
    private val FRAMEWORK_REQUEST_CODE = 1

    constructor(mWalkThroughListener: WalkThroughContract.ApiListener) {
        this.mWalkThroughListener = mWalkThroughListener
    }

    /**
     * Get All Walk Through PushNotificationDataResponse UserVisibilityResponse
     */
    fun getAllWalkThroughApiData(mActivity : Activity) {
        var call: Call<WalkThroughResponse> = TetootaApplication.getHeader()
                .getSlidesList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG,"en",mActivity),"Android")
        call.enqueue(object : Callback<WalkThroughResponse> {
            override fun onResponse(call: Call<WalkThroughResponse>?, response: Response<WalkThroughResponse>?) {
                println("Success")
                var mWalkThroughData : WalkThroughResponse? = response?.body()
                if(response?.code() == 200){
                    if(response.body()?.data?.size!! > 0){
                        mWalkThroughListener.onWalkThroughApiSuccess(mWalkThroughData?.data as List<WalkThroughDataResponse>, "success")

                    }else{
                        mWalkThroughListener.onWalkThroughApiFailure(response?.body()?.meta?.message.toString())
                    }
                }else{
                    mWalkThroughListener.onWalkThroughApiFailure(response?.body()?.meta?.message.toString())
                }
            }
            override fun onFailure(call: Call<WalkThroughResponse>?, t: Throwable?) {
                println("failure")
                mWalkThroughListener.onWalkThroughApiFailure(t?.message.toString())
            }
        })

    }


  /*  fun openAccountKit(mActivity: Activity){
        onLogin(LoginType.PHONE,mActivity)
    }*/

/*
    fun onLogin(loginType : LoginType, mActivity: Activity) : Unit{
        @ColorInt val primaryColor = ContextCompat.getColor(mActivity, R.color.colorPrimary)
        val intent = Intent(mActivity, AccountKitActivity::class.java)
        val configurationBuilder = AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE,
                AccountKitActivity.ResponseType.TOKEN)
        val uiManager = SkinManager(SkinManager.Skin.CONTEMPORARY, primaryColor)
         configurationBuilder.setUIManager(uiManager)
        val configuration = configurationBuilder.build()
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configuration)
        var completeListener: OnCompleteListener = object :OnCompleteListener {
            override fun onComplete() {
                mActivity.startActivityForResult(intent, FRAMEWORK_REQUEST_CODE)
            }
        }
        when(loginType){
            LoginType.EMAIL -> println("Not Yet Implemented")
            LoginType.PHONE ->{
                if (configuration.isReceiveSMSEnabled) {
                    val receiveSMSCompleteListener = completeListener
                    completeListener = object : OnCompleteListener {
                        override fun onComplete() {
                            requestPermissions(
                                    Manifest.permission.RECEIVE_SMS,
                                    R.string.permissions_receive_sms_title,
                                    R.string.permissions_receive_sms_message,
                                    receiveSMSCompleteListener,mActivity)
                        }
                    }
                }
            }
        }
        completeListener.onComplete()
    }
*/
/*
    private interface OnCompleteListener {
        fun onComplete()
    }
    private fun requestPermissions(
            permission: String,
            rationaleTitleResourceId: Int,
            rationaleMessageResourceId: Int,
            listener: OnCompleteListener?, mActivity: Activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            listener?.onComplete()
            return
        }
        checkRequestPermissions(
                permission,
                rationaleTitleResourceId,
                rationaleMessageResourceId,
                listener,mActivity)
    }*/

/*
    @TargetApi(Build.VERSION_CODES.M)
    private fun checkRequestPermissions(
            permission: String,
            rationaleTitleResourceId: Int,
            rationaleMessageResourceId: Int,
            listener: OnCompleteListener?,mActivity: Activity) {
        if (mActivity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            listener?.onComplete()
            return
        }*/

     //   val requestCode = nextPermissionsRequestCode++
      //  permissionsListeners.put(requestCode, listener)

/*        if (mActivity.shouldShowRequestPermissionRationale(permission)) {
            AlertDialog.Builder(mActivity)
                    .setTitle(rationaleTitleResourceId)
                    .setMessage(rationaleMessageResourceId)
                    .setPositiveButton(android.R.string.yes) { dialog, which ->
                        mActivity.requestPermissions(arrayOf(permission), requestCode) }
                    .setNegativeButton(android.R.string.no) { dialog, which ->
                        // ignore and clean up the listener
                        permissionsListeners.remove(requestCode)
                    }
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
        } else {
            mActivity.requestPermissions(arrayOf(permission), requestCode)
        }*/

}