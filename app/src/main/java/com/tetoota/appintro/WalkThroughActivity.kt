package com.tetoota.appintro

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginDataResponse
import com.tetoota.main.MainActivity
import com.tetoota.newaddphoneverify.EnterMobileNumber
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.activity_walk_through.*


class WalkThroughActivity : BaseActivity(), WalkThroughContract.View, CheckUserContract.View,
        ViewPager.OnPageChangeListener, View.OnClickListener {

    //Test
    var isFromHelp: Boolean = false
    private val FRAMEWORK_REQUEST_CODE = 1
    private var dotsCount: Int = 0
    var number: String? = null
    var dots = arrayOfNulls<ImageView>(dotsCount)
    val mAdapter: WalkThroughAdapter by lazy {
        WalkThroughAdapter(this@WalkThroughActivity)
    }

    private val mWalkThroughPresenter: WalkThroughPresenter by lazy {
        WalkThroughPresenter(this)
    }
    private val mCheckUserPresenter: CheckUserPresenter by lazy {
        CheckUserPresenter(this@WalkThroughActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE)
/*
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
*/
        setContentView(R.layout.activity_walk_through)

//        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)

        val bundle = intent.extras
        if (null != bundle && bundle.containsKey("isFromHelp")) {
            isFromHelp = bundle.getBoolean("isFromHelp")
        } else {
            isFromHelp = false
        }
        TetootaApplication.setTextValuesHashMap(Utils.getLangByCode(this,
                Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", this)))

        btn_left_slide.visibility = View.INVISIBLE
        initviews()

//        btn_left_slide.setOnClickListener(View.OnClickListener {
//            var tab = pager_introduction.getCurrentItem()
//            if (tab > 0) {
//                tab--
//                pager_introduction.setCurrentItem(tab)
//            } else if (tab == 0) {
//                pager_introduction.setCurrentItem(tab)
//            }
//        })
//
//        btn_right_slide.setOnClickListener(View.OnClickListener {
//            var tab = pager_introduction.getCurrentItem()
//            tab++
//            pager_introduction.setCurrentItem(tab)
//        })

    }


    override fun onResume() {
        super.onResume()
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, WalkThroughActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

/*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("Login Activity On Activity Result")
        if (requestCode != FRAMEWORK_REQUEST_CODE) {
            return
        }
        val toastMessage: String
        val loginResult: AccountKitLoginResult = AccountKit.loginResultWithIntent(data)!!
        if (loginResult == null || loginResult.wasCancelled()) {
            toastMessage = "Login Cancelled"
        } else if (loginResult.error != null) {
            toastMessage = loginResult.error!!.errorType.message
            println("Error Comes")
            //  showErrorActivity(loginResult.error)
        } else {
            val accessToken = loginResult.accessToken
            val authorizationCode = loginResult.authorizationCode
            val tokenRefreshIntervalInSeconds = loginResult.tokenRefreshIntervalInSeconds
            if (accessToken != null) {

                toastMessage = "Success:" + accessToken.accountId
                +tokenRefreshIntervalInSeconds
                println("Error Comes ${loginResult.finalAuthorizationState}")

                val intent = SelectCategoriesActivity.newMainIntent(this@WalkThroughActivity)
                ActivityStack.getInstance(this@WalkThroughActivity)
                ActivityStack.cleareAll()
                startActivity(intent)
                finish()


            } else if (authorizationCode != null) {
                val intent = SelectCategoriesActivity.newMainIntent(this@WalkThroughActivity)
                ActivityStack.getInstance(this@WalkThroughActivity)
                ActivityStack.cleareAll()
                startActivity(intent)
                finish()

            } else {
                toastMessage = "Unknown response type"
            }
        }*/


    override fun onLoginResult(message: Int, mesgDesc: String) {
        //  hideProgressDialog()
        showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
        if (message == com.tetoota.utility.Constant.API_SUCCESS_VALUE) {
            hideProgressDialog()
            if (Utils.haveNetworkConnection(this)) {
                // showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
                val json = Utils.loadPrefrence(com.tetoota.utility.Constant.LOGGED_IN_USER_DATA, "", this)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                //  Log.e("ddddddddddddddddd", "" + personData)

                if (personData != null) {
                    val intent = MainActivity.newMainIntent(this@WalkThroughActivity)
                    // ActivityStack.getInstance(this@WalkThroughActivity)
                    // overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
                    ActivityStack.cleareAll()
                    startActivity(intent)
                    finish()
                } else {
                    val intent = EnterMobileNumber.newMainIntent(this@WalkThroughActivity)
                    // ActivityStack.getInstance(this@WalkThroughActivity)
                    ActivityStack.cleareAll()
                    startActivity(intent)
                    finish()
                }


/*
                mCheckUserPresenter.doDeepLinking(this@WalkThroughActivity, Utils.loadPrefrence(com.tetoota.utility.Constant.USER_AUTH_TOKEN
                        , "", this@WalkThroughActivity), personData.user_id!!
                        , Utils.getIPAddress(true))
*/
            } else {
                moveToNextScreen()
            }
        } else if (message == com.tetoota.utility.Constant.API_FAILURE_VALUE) {
            println("Failure")
        } else {
            println("Unkknown")
        }
    }

    override fun onDeepLinkingResult() {
        hideProgressDialog()
        moveToNextScreen()
    }

    fun moveToNextScreen() {
        var mobile_number = number.toString()
        val intent = MainActivity.newMainIntent(this)
        ActivityStack.getInstance(this@WalkThroughActivity)
        overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
        ActivityStack.cleareAll()
        startActivity(intent)
        finish()
        println("Success")
    }

    private fun initviews() {
        pager_introduction.adapter = mAdapter
        pager_introduction.currentItem = 0
        pager_introduction.addOnPageChangeListener(this)
        btn_next.setOnClickListener(this@WalkThroughActivity)
        if (isFromHelp)
            btn_next.text = Utils.getText(this@WalkThroughActivity, StringConstant.str_walthrough_back)
        else
            btn_next.text = Utils.getText(this, StringConstant.str_skip)
        if (Utils.haveNetworkConnection(this@WalkThroughActivity)) {
            showProgressDialog(Utils.getText(this, StringConstant.please_wait))
            mWalkThroughPresenter.getWalkThroughData(this@WalkThroughActivity)
        } else {
            showSnackBar(Utils.getText(this, StringConstant.str_check_internet))
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_next -> {
                //  mWalkThroughPresenter.openAccountKit(LoginType.PHONE, this@WalkThroughActivity)
                if (!isFromHelp) {
                    val intent = Intent(this, EnterMobileNumber::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                } else {
                    onBackPressed()
                }
            }
        }
    }

    private fun setUiPageViewController() {
        // dotsCount = mAdapter.count
        dots = arrayOfNulls(dotsCount)
        for (i in 0..dotsCount - 1) {
            dots[i] = ImageView(this)
            dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
            val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(4, 0, 4, 0)
            viewPagerCountDots.addView(dots[i], params)
        } // Commit Testing
        dots[0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
    }

    override fun onPageScrollStateChanged(p0: Int) {
    }

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
    }

    override fun onPageSelected(p0: Int) {
        for (i in 0..dotsCount - 1) {
            dots[i]?.setImageDrawable(resources.getDrawable(R.drawable.nonselected_walkthrough_item))
        }
        dots[p0]?.setImageDrawable(resources.getDrawable(R.drawable.select_walkthorugh_item))
        if (p0 + 1 == dotsCount && isFromHelp) {
            btn_next.visibility = View.VISIBLE
        } else {
            // btn_next.visibility = View.GONE
            btn_next.visibility = View.VISIBLE

        }
        toggleArrowVisibility(p0 == 0, p0 == dotsCount - 1)

    }

    fun toggleArrowVisibility(isAtZeroIndex: Boolean, isAtLastIndex: Boolean) {
        if (!isFromHelp) {
            if (isAtZeroIndex) {
                btn_left_slide.setVisibility(View.INVISIBLE)
                btn_next.text = "SKIP"

            } else {
                btn_left_slide.setVisibility(View.VISIBLE)
            }
            if (isAtLastIndex) {
                btn_next.text = Utils.getText(this@WalkThroughActivity, StringConstant.str_get_started)
                btn_right_slide.setVisibility(View.INVISIBLE)
                //  btn_right_slide.setVisibility(View.VISIBLE)
                /*  btn_right_slide.setOnClickListener(View.OnClickListener
            {
                mWalkThroughPresenter.openAccountKit(LoginType.PHONE, this@WalkThroughActivity)
            })*/
            } else {
                btn_right_slide.setVisibility(View.VISIBLE)

            }
        } else {
            btn_next.text =  Utils.getText(this@WalkThroughActivity, StringConstant.str_walthrough_back)
        }

    }

    override fun onImageSlideSuccessResult(mWalkThroughList: List<WalkThroughDataResponse>?, message: String) {
        hideProgressDialog()
        dotsCount = mWalkThroughList?.size!!
        setUiPageViewController()
        mAdapter.setElements(mWalkThroughList as ArrayList<WalkThroughDataResponse>)
        mAdapter.notifyDataSetChanged()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


//        val intent = SelectCategoriesActivity.newMainIntent(this@WalkThroughActivity)
//        ActivityStack.getInstance(this@WalkThroughActivity)
//        ActivityStack.cleareAll()
//        startActivity(intent)
//        finish()


        /*   println("Login Activity On Activity Result")
           if (requestCode != FRAMEWORK_REQUEST_CODE) {
               return
           }
           val toastMessage: String
           val loginResult: AccountKitLoginResult = AccountKit.loginResultWithIntent(data)!!
           if (loginResult == null || loginResult.wasCancelled()) {
               toastMessage = "Login Cancelled"
           } else if (loginResult.error != null) {
               toastMessage = loginResult.error!!.errorType.message
               println("Error Comes")
               //  showErrorActivity(loginResult.error)
           } else {
               val accessToken = loginResult.accessToken
               val authorizationCode = loginResult.authorizationCode
               val tokenRefreshIntervalInSeconds = loginResult.tokenRefreshIntervalInSeconds
               if (accessToken != null) {
                   toastMessage = "Success:" + accessToken.accountId
                   +tokenRefreshIntervalInSeconds
                   println("Error Comes ${loginResult.finalAuthorizationState}")


                  *//* AccountKit.getCurrentAccount(object : AccountKitCallback<Account> {
                    override fun onSuccess(account: Account) {
                        val string: String = account.phoneNumber.phoneNumber
                        number = account.phoneNumber.phoneNumber
                       // Log.e("number+++++++", "" + number)
                        println("Phone Number $string")
                        // showSnackBar(number.toString())
                        mCheckUserPresenter.checkUserLoginFromAPI12(this@WalkThroughActivity, number.toString())

                    }

                    override fun onError(error: AccountKitError) {
                        showSnackBar(error.errorType.message)
                    }
                })*//*

                *//*  val intent = SelectCategoriesActivity.newMainIntent(this@WalkThroughActivity)
                  ActivityStack.getInstance(this@WalkThroughActivity)
                  // Log.e("kamallllllllllll",""+ loginResult)
                  ActivityStack.cleareAll()
                  startActivity(intent)
                  finish()*//*

                // showHelloActivity(loginResult.finalAuthorizationState)
            } else if (authorizationCode != null) {
                val intent = SelectCategoriesActivity.newMainIntent(this@WalkThroughActivity)
                ActivityStack.getInstance(this@WalkThroughActivity)
                ActivityStack.cleareAll()
                startActivity(intent)
                finish()
                // showHelloActivity(authorizationCode, loginResult.finalAuthorizationState)*/


    }

    override fun onImageSlideFailureResult(message: String) {
        hideProgressDialog()
        showSnackBar(message)
    }

    public override fun onDestroy() {
        super.onDestroy()
    }
}
