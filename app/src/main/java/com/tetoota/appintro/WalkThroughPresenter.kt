package com.tetoota.appintro
import android.app.Activity


/**
 * Created by charchit.kasliwal on 03-07-2017.
 */
class WalkThroughPresenter : WalkThroughContract.Presenter,
 WalkThroughContract.ApiListener{
    var mWalkThroughView : WalkThroughContract.View

    constructor(mWalkThroughView: WalkThroughContract.View){
        this.mWalkThroughView = mWalkThroughView
    }

    private val mWalkThroughInteractor : WalkThroughInteractor by lazy {
        WalkThroughInteractor(this)
    }

    override fun getWalkThroughData(mActivity : Activity) {
        mWalkThroughInteractor.getAllWalkThroughApiData(mActivity)
    }

   /* override fun openAccountKit(loginType : LoginType,mActivity : Activity) {
        mWalkThroughInteractor.openAccountKit(mActivity)
    }*/

    override fun onWalkThroughApiSuccess(mDataList: List<WalkThroughDataResponse>, message: String) {
        mWalkThroughView.onImageSlideSuccessResult(mDataList,message)
    }

    override fun onWalkThroughApiFailure(message: String) {
        mWalkThroughView.onImageSlideFailureResult(message)
    }
}