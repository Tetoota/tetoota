package com.tetoota.appintro

import android.app.Activity


/**
 * Created by charchit.kasliwal on 03-07-2017.
 */
class WalkThroughContract {
    interface View {
        fun onImageSlideSuccessResult(mCategoriesList: List<WalkThroughDataResponse>?, message: String)
        fun onImageSlideFailureResult(message: String)
    }

    internal interface Presenter {
        fun getWalkThroughData(mActivity : Activity)
       // fun openAccountKit(loginType : LoginType, mActivity: Activity)
    }

    interface ApiListener {
        fun onWalkThroughApiSuccess(mDataList: List<WalkThroughDataResponse>, message: String)
        fun onWalkThroughApiFailure(message: String)
    }
}