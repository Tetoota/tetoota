package com.tetoota.appintro

import android.app.Activity


class CheckUserContract {
    interface View {
        fun onLoginResult(message: Int,mesgDesc : String)
        fun onDeepLinkingResult()
    }

    internal interface Presenter {
        fun checkUserLoginFromAPI12(mActivity: Activity, number: String)
        fun doDeepLinking(mActivity: Activity, authToken : String, user_id : String, user_ip : String)
    }

    interface CheckUserApiListener {
        fun onLoginSuccess12(message: Int, mesg : String)
        fun onDeepLinkingResult()
    }
}