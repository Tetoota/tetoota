package com.tetoota.appintro
import com.tetoota.network.errorModel.Meta

data class WalkThroughResponse( val meta: Meta? = null,
        val data: List<WalkThroughDataResponse>?)
