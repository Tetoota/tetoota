package com.tetoota.pushnotificationsetting

import android.app.Activity

/**
 * Created by charchit.kasliwal on 27-10-2017.
 */
class PushSettingsContract {

    interface View {
        fun pushNotificationSuccess(response : String, message : String)
        fun pushNotificationFailure(response: String,  message : String)
        fun onUserStatusSuccess(response: String,  message : String)
        fun onUserStatusFailure(response: String,  message : String)

    }

    interface Presenter {
        fun setPushNotificationSetting(mActivity : Activity,all_push : String, chat_push : String, proposal_push : String)
        fun setUserVisibiltySettings(mActivity: Activity, mUserId : String, mIsUserVisible : String)
        fun setUserPublicPrivateSettings(mActivity: Activity, mStatus : String)
    }

    interface pushNotificationListener{
        fun onPushNotificationSuccess(response : String, message : String)
        fun onPushNotificationFailure(response: String,  message : String)
        fun onUserStatusSuccess(response: String,  message : String)
        fun onUserStatusFailure(response: String,  message : String)

    }
}