package com.tetoota.fragment.settings

data class PushNotificationDataResponse(
	val proposal_notification: String? = null,
	val all_notification: String? = null,
	val chat_notification: String? = null
)
