package com.tetoota.pushnotificationsetting
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.layout_push_notification.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

/**
 * Created by charchit.kasliwal on 23-10-2017.
 */
class PushNotificationSetttings : BaseActivity(), View.OnClickListener, PushSettingsContract.View {
    override fun onUserStatusSuccess(response: String, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onUserStatusFailure(response: String, message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var isAllPushNotification: Boolean = true
    private var isChatNotification : Boolean = true
    private var isProposalNotification : Boolean = true
    private val mProposalPresenter : PushSettingsPresenter by lazy {
        PushSettingsPresenter(this@PushNotificationSetttings)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_push_notification)
        initViews()
    }

    override fun onClick(p0: View?) {
        when(p0){
            iv_toggle -> {
               if(isAllPushNotification){
                    isAllPushNotification = false
                    isChatNotification = false
                    isProposalNotification = false

               }else{
                   isAllPushNotification = true
                   isChatNotification = true
                   isProposalNotification = true
               }
                callPushNotificationAPI()
            }
            chat_toggle -> {
                if(isChatNotification){
                    isChatNotification = false
                    if(isAllPushNotification){
                        isAllPushNotification = false
                    }
                }else{
                    isChatNotification = true
                }
                if(isProposalNotification && isChatNotification){
                    isAllPushNotification = true
                }

                if(Utils.haveNetworkConnection(this@PushNotificationSetttings)){
                  //  mProposalPresenter.setPushNotificationSetting(this@PushNotificationSetttings,)
                }else{
                    showSnackBar(Utils.getText(this,StringConstant.str_check_internet))
                }
                callPushNotificationAPI()
            }
            proposal_toggle -> {
                if(isProposalNotification){
                    isProposalNotification = false
                    if(isAllPushNotification){
                        isAllPushNotification = false
                        iv_toggle.setImageResource(R.drawable.tv_toggle_off)
                    }
                    proposal_toggle.setImageResource(R.drawable.tv_toggle_off)
                }else{
                    isProposalNotification = true
                    proposal_toggle.setImageResource(R.drawable.tv_toggle_on)
                }
                if(isProposalNotification && isChatNotification){
                    isAllPushNotification = true
                    iv_toggle.setImageResource(R.drawable.tv_toggle_on)
                }
                callPushNotificationAPI()
            }
        }
    }

    private fun initViews() {
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        iv_close.onClick { onBackPressed() }
        toolbar_title.text = "Push Notification"
        iv_toggle.setOnClickListener(this)
        chat_toggle.setOnClickListener(this)
        proposal_toggle.setOnClickListener(this)
        setToggleButton()
        setMultiLanguageText()
    }

    private fun setToggleButton() {
        if(Utils.loadPrefrence(Constant.ALL_PUSH_NOTIFICATION,"1",this@PushNotificationSetttings) == "0"){
            isAllPushNotification = false
            iv_toggle.setImageResource(R.drawable.tv_toggle_off)
        }else{
            isAllPushNotification = true
            iv_toggle.setImageResource(R.drawable.tv_toggle_on)
        }
        if(Utils.loadPrefrence(Constant.PROPOSAL_PUSH_NOTIFICATION,"1",this@PushNotificationSetttings) == "0"){
            isProposalNotification = false
            proposal_toggle.setImageResource(R.drawable.tv_toggle_off)
        }else{
            isProposalNotification = true
            proposal_toggle.setImageResource(R.drawable.tv_toggle_on)
        }
        if(Utils.loadPrefrence(Constant.CHAT_PUSH_NOTIFICATION,"1",this@PushNotificationSetttings) == "0"){
            isChatNotification = false
            chat_toggle.setImageResource(R.drawable.tv_toggle_off)
        }else{
            isChatNotification = true
            chat_toggle.setImageResource(R.drawable.tv_toggle_on)
        }
    }

    private fun setMultiLanguageText() {
        tv_chatpush.text = Utils.getText(this,StringConstant.tv_chatpush)
        tv_proposal_push.text = Utils.getText(this,StringConstant.tv_proposal_push)
        tv_allpush.text = Utils.getText(this,StringConstant.tv_allpush)
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, PushNotificationSetttings::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@PushNotificationSetttings)
        finish()
    }

    private fun callPushNotificationAPI(){
        if(Utils.haveNetworkConnection(this@PushNotificationSetttings)){
            showProgressDialog(Utils.getText(this,StringConstant.please_wait))
            var mAll : String = if(isAllPushNotification) "1" else "0"
            var mProposal : String = if(isProposalNotification) "1" else "0"
            var mCaht : String = if(isChatNotification) "1" else "0"
            mProposalPresenter.setPushNotificationSetting(this@PushNotificationSetttings,mAll,mProposal,mCaht)
        }else{
            showSnackBar(Utils.getText(this,StringConstant.str_check_internet))
        }

    }

    override fun pushNotificationSuccess(response: String, message: String) {
        setToggleButton()
        hideProgressDialog()
        showSnackBar(message)
    }

    override fun pushNotificationFailure(response: String, message: String) {
        hideProgressDialog()
        showSnackBar(message)
    }
}