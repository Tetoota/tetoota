package com.tetoota.reviews

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.squareup.picasso.Picasso
import com.tetoota.ActivityStack
import com.tetoota.R
import com.tetoota.TetootaApplication
import com.tetoota.service_product.FullImageActivity
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.view_reviews_cell.view.*
import org.jetbrains.anko.onClick
import java.text.DecimalFormat


/**
 * Created by charchit.kasliwal on 19-06-2017.
 */
class ViewReviewAdapter(val mContext: Context, var mHomeList: MutableList<Any?> = ArrayList<Any?>(),
                        val iAdapterClickListener: IAdapterClickListener, var screenHeight: Int) : RecyclerView.Adapter<ViewReviewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.view_reviews_cell, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        val mServiceList = mHomeList as List<ViewReviewsDataResponse>
        viewHolder.bindHomeData(viewHolder, p1, mServiceList[p1], screenHeight, mContext)
    }

    fun setElements(mHomeList: MutableList<ViewReviewsDataResponse>) {
        this.mHomeList = mHomeList as MutableList<Any?>
        notifyDataSetChanged()
    }

    fun addAll(moveResults: List<ViewReviewsDataResponse?>) {
        for (result in moveResults) {
            add(result)
        }
    }

    private fun add(result: Any?) {
        this.mHomeList.add(result)
        notifyItemInserted(mHomeList.size - 1)
    }

    private fun getLastPosition() = if (mHomeList.lastIndex == -1) 0 else mHomeList.lastIndex

    override fun getItemCount(): Int {
        return mHomeList.size
    }

    interface IAdapterClickListener {
        fun cellItemClick(mString: String, cellRow: Any): Unit
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val mImageView = view.iv_product!!
        //  val tv_name = view.tv_name!!
        val tv_username = view.tv_username!!
        val tv_title = view.tv_title!!
        val tv_time = view.tv_time!!
        val tv_reviews = view.tv_reviews!!
        val tv_description = view.tv_description!!
        val user_reviews = view.user_reviews!!
        val ll_mainlayout_layout = view.ll_mainlayout_layout!!
        private var tetootaApplication: TetootaApplication? = null
        fun bindHomeData(viewHolder: ViewHolder?, p1: Int, s: ViewReviewsDataResponse, height: Int, context: Context) {
            // ll_mainlayout_layout.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height)
            tetootaApplication = context.getApplicationContext() as TetootaApplication
            //  mImageView.loadUrl(s.comment_rev_profileImage!!)
            //  tv_name.text = s.title

            if (s.comment_rev_profileImage!!.isEmpty()) {
                mImageView.setImageResource(R.drawable.user_placeholder);
            } else {

                Picasso.get().load(s.comment_rev_profileImage).placeholder(R.drawable.user_placeholder).into(mImageView)
            }

            if (s.comment_rev_profileImage.isEmpty()) {
                // context.toast("Profile image is not available")

            } else {
                mImageView.onClick {
                    val intent = FullImageActivity.newMainIntent(context)
                    intent!!.putExtra("profile_image", s.comment_rev_profileImage)
                    ActivityStack.getInstance(context)
                    context.startActivity(intent)

                }
            }


            if (tetootaApplication!!.isCheck.equals("ServiceReview")) {
                tv_title.visibility = View.GONE
                tv_username.text = s.commit_rev_user_name
            } else if (tetootaApplication!!.isCheck.equals("AllReview")) {
                tv_title.visibility = View.VISIBLE
                tv_username.text = s.commit_rev_user_name
                tv_title.text = s.title
            }
            // tv_username.text = s.commit_rev_user_name

            tv_description.text = s.review_message


            if (s.average_badges != null) {
                user_reviews.rating = (s.average_badges).toFloat()
                tv_reviews.text = "Reviews "+roundTwoDecimals((s.average_badges).toFloat())

            }
            val mesgTimeStamp = Utils.getTimeStamp(itemView.context, s.creation_date!!)
            tv_time.text = Utils.getTimeAgo(mesgTimeStamp, itemView.context)


        }

        fun roundTwoDecimals(d: Float): Float {
            val twoDForm = DecimalFormat("#.##")
            return java.lang.Float.valueOf(twoDForm.format(d))
        }

/*
        fun ImageView.loadUrl(url: String) {
            if(url != "" && url != null){
                Glide.with(itemView.context)
                        .load(Utils.getUrl(itemView.context, url!!))
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.user_placeholder)
                        .error(R.drawable.user_placeholder)
                        .into(mImageView)
            }else{
                Glide.with(itemView.context)
                        .load("")
                        .centerCrop()
                        .dontAnimate()
                        .placeholder(R.drawable.user_placeholder)
                        .error(R.drawable.user_placeholder)
                        .into(mImageView)
            }
        }
*/
    }


}