package com.tetoota.reviews
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.google.gson.Gson
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import io.intercom.android.sdk.Intercom
import kotlinx.android.synthetic.main.activity_view_reviews.*
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

class ViewReviewsActivity : BaseActivity(),ViewReviewsContract.View,
        ViewReviewAdapter.IAdapterClickListener {
    private lateinit var mReviewAdapter: ViewReviewAdapter
    private var isLoad = false
    var linearLayoutManager: LinearLayoutManager? = null
    var context : Context? = null
    private val mReviewPresenter : ViewReviewPresenter by lazy {
        com.tetoota.reviews.ViewReviewPresenter(this@ViewReviewsActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_reviews)
        context = this

        Intercom.client().handlePushMessage()
        Intercom.client().setLauncherVisibility(Intercom.Visibility.GONE)
        initToolbar()
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rl_reviewslist.layoutManager = linearLayoutManager
        rl_reviewslist.setHasFixedSize(true)
        mReviewAdapter = ViewReviewAdapter(this.applicationContext,iAdapterClickListener = this, screenHeight = heightCalculation(.15f))
        rl_reviewslist.adapter = mReviewAdapter
        val extras = intent.extras

        if (extras != null) {
            if (extras.containsKey("post_id")) {
                getViewReviewsDataById(extras.getString("post_id")!!)
            } else {
                getViewReviewsData()
            }
        }
    }

    private fun initToolbar() : Unit {
          iv_close.visibility = View.VISIBLE
          toolbar_title.text = "View Review"
          iv_close.onClick {
             onBackPressed()
          }
    }
    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, ViewReviewsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
        ActivityStack.removeActivity(this@ViewReviewsActivity)
        finish()
    }


    private fun getViewReviewsData(){
        if(Utils.haveNetworkConnection(this@ViewReviewsActivity)){
            showProgressDialog(Utils.getText(this,StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData  = Gson().fromJson(json, LoginDataResponse::class.java)
            mReviewPresenter.getViewReviewData(this@ViewReviewsActivity,personData)
        }else{
            showErrorView(Utils.getText(this,StringConstant.str_check_internet))
        }
    }

    private fun getViewReviewsDataById(postId: String) {
        if(Utils.haveNetworkConnection(this@ViewReviewsActivity)){
            showProgressDialog(Utils.getText(this,StringConstant.please_wait))
            val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
            val personData  = Gson().fromJson(json, LoginDataResponse::class.java)
            mReviewPresenter.getViewReviewDataById(this@ViewReviewsActivity,postId)
        }else{
            showErrorView(Utils.getText(this,StringConstant.str_check_internet))
        }
    }

    override fun onViewReviewApiSuccessResult(mReviewList: ArrayList<ViewReviewsDataResponse>, message: String) {
        hideProgressDialog()
        hideErrorView()
        mReviewAdapter.setElements(mReviewList)
        mReviewAdapter.notifyDataSetChanged()
    }

    override fun onViewReviewFailureResult(message: String, isServerError: Boolean) {
        hideProgressDialog()
        showErrorView(message)
    }

    override fun cellItemClick(mString: String, cellRow: Any) {
    }

    private fun hideErrorView() {
        if (error_layout.visibility === View.VISIBLE) {
            error_layout.visibility = View.GONE
            rl_reviewslist.visibility = View.VISIBLE
            err_title.visibility = View.GONE
        }
    }

    /**
     * @param throwable required for [.fetchErrorMessage]
     * *
     * @return
     */
    private fun showErrorView(throwable: String) {
        if (error_layout.visibility === View.GONE) {
            error_layout.visibility = View.VISIBLE
            error_txt_cause.text = throwable
            rl_reviewslist.visibility = View.GONE
            err_title.visibility = View.GONE
        }
    }
}
