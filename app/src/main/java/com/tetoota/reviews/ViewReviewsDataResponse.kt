package com.tetoota.reviews

data class ViewReviewsDataResponse(
		val post_image: String? = null,
		val latitude: Double? = null,
		val friendliness: Float? = null,
		val review_message: String? = null,
		val description: String? = null,
		val availability: String? = null,
		val trading_preference: Any? = null,
		val creation_date: String? = null,
		val title: String? = null,
		val set_quote: String? = null,
		val qualification: String? = null,
		val average_badges: String? = null,
		val post_id: Any? = null,
		val user_id: Any? = null,
		val share_service_platform: String? = null,
		val quality_services: Double? = null,
		val post_type: String? = null,
		val response_time: Float? = null,
		val set_tetoota_points: Any? = null,
		val categories_ids: Any? = null,
		val longitude: Double? = null,
		val virtual_service: String? = null,
		val comment_rev_profileImage: String? = null,
		val commit_rev_user_name: String? =null
)
