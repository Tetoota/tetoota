package com.tetoota.reviews

import android.app.Activity
import com.tetoota.login.LoginDataResponse


/**
 * Created by charchit.kasliwal on 19-06-2017.
 */
class ViewReviewsContract {

    interface View {
        fun onViewReviewApiSuccessResult(mCategoriesList: ArrayList<ViewReviewsDataResponse>, message: String)
        fun onViewReviewFailureResult(message: String, isServerError: Boolean)
    }

    internal interface Presenter {
        fun getViewReviewData(mActivity : Activity, personData : LoginDataResponse)
        fun getViewReviewDataById(mActivity : Activity, postId : String)
    }

    interface ReviewApiListener {
        fun onReviewAPiSuccess(mDataList: List<ViewReviewsDataResponse>, message: String)
        fun onReviewApiFailure(message: String, isServerError: Boolean)
    }
}