package com.tetoota.categories

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.text.SpannableString
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import com.tetoota.ActivityStack
import com.tetoota.BaseActivity
import com.tetoota.R
import com.tetoota.fragment.profile.OptionalFeldByUser.OptionalProfileUser
import com.tetoota.main.MainActivity
import com.tetoota.newaddphoneverify.PreferenceHelper
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_select_categories.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.onClick

class SelectCategoriesActivity : BaseActivity(), View.OnClickListener, CategoriesContract.View,
        CategoriesAdapter.IAdapterClickListener, SelectedCategoriesAdapter.ISelectedAdapterClickListener {
    override fun onMobileSuccess(message: Int, mesgDesc: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var preferenceHelper: PreferenceHelper? = null
    var mCategoriesResponse = ArrayList<CategoriesDataResponse>()
    var number: String? = null
    //    lateinit var languageMap: HashMap<String, String>
    var mSelectedCategoriesResponse = ArrayList<CategoriesDataResponse>()
    var isCheckboxChecked: Boolean = false
    lateinit var mCategoriesAdapter: CategoriesAdapter
    lateinit var mSelectedCategoriesAdapter: SelectedCategoriesAdapter


    var gridLayoutManager: GridLayoutManager? = null
    private val mCategoriesPresenter: CategoriesPresenter by lazy {
        CategoriesPresenter(this@SelectCategoriesActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_categories1)
        initViews()
        btn_next.setOnClickListener(this@SelectCategoriesActivity)
    }

    private fun initViews() {
        iv_close.visibility = View.VISIBLE
        iv_close.setBackgroundDrawable(resources.getDrawable(R.drawable.ic_arrow_back_white_24dp))
        toolbar_title.text=Utils.getText(this, StringConstant.select_your_interest)
        iv_close.onClick { onBackPressed() }

        preferenceHelper = PreferenceHelper(this)
        mCategoriesAdapter = com.tetoota.categories.CategoriesAdapter(iAdapterClickListener = this)
        mSelectedCategoriesAdapter = com.tetoota.categories.SelectedCategoriesAdapter(iAdapterClickListener = this)
        gridLayoutManager = GridLayoutManager(this, 2)
        rl_category_list.layoutManager = gridLayoutManager
        rl_category_list.setHasFixedSize(false)
        rl_category_list.adapter = mCategoriesAdapter

        setMultiLanguageText()
    }

    private fun setMultiLanguageText() {
        tv_select_categories.text = Utils.getText(this, StringConstant.str_please_select_categories)
        btn_next.text = Utils.getText(this, StringConstant.str_next)
        val tempString = Utils.getText(this, StringConstant.help_term_condition)
        val spanString = SpannableString(tempString)
        spanString.setSpan(UnderlineSpan(), 0, spanString.length, 0)
        spanString.setSpan(StyleSpan(Typeface.BOLD), 0, spanString.length, 0)
        spanString.setSpan(StyleSpan(Typeface.ITALIC), 0, spanString.length, 0)
    //    tv_terms_text.text = spanString
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_next -> {
                val result = StringBuilder()
                val resultValues = StringBuilder()

                if (mSelectedCategoriesResponse.size > 0) {
                    for (item in mSelectedCategoriesResponse.indices) {
                        result.append(mSelectedCategoriesResponse[item].category_id)
                        result.append(",")
                        resultValues.append(mSelectedCategoriesResponse[item].category_name)
                        resultValues.append(", ")

                    }
                    Log.e("resultString",""+result);
                    if (result.endsWith(",")) {
                        var resultnew = result.substring(0, result.length - 1)
                        var resultNewValues = resultValues.substring(0, resultValues.length - 1)
                        val returnIntent = Intent()
                        returnIntent.putExtra(OptionalProfileUser.OPTIONAL_USER_INTEREST_RESULT_IDS,resultnew)
                        returnIntent.putExtra(OptionalProfileUser.OPTIONAL_USER_INTEREST_RESULT,resultNewValues)
                        setResult(Activity.RESULT_OK, returnIntent)
                        finish()
                       // println("Result $result $resultnew")
                    }
                } else {
                    showSnackBar(Utils.getText(this, StringConstant.add_service_category_alert))
                }
            }
        }
    }

    /**
     * On Resume
     */
    override fun onResume() {
        super.onResume()
        if (Utils.haveNetworkConnection(this@SelectCategoriesActivity)) {
            showProgressDialog("${Utils.getText(this, StringConstant.please_wait)}")
            mCategoriesPresenter.getCategoriesData(this@SelectCategoriesActivity)
        } else {
            //showErrorView("${Utils.getText(this, StringConstant.str_check_internet)}")
        }
    }

    companion object {
        fun newMainIntent(context: Context): Intent? {
            val intent = Intent(context, SelectCategoriesActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    override fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String) {
        hideProgressDialog()
        // showSnackBar(message)
        this.mCategoriesResponse = mCategoriesList as ArrayList<CategoriesDataResponse>
        mCategoriesAdapter.setElements(mCategoriesList)
        mCategoriesAdapter.notifyDataSetChanged()
        rl_category_list.visibility = View.VISIBLE
    }

    override fun onCatergoriesFailureResult(message: String) {
        hideProgressDialog()
       // showErrorView(message)
        showSnackBar(message)
    }

    override fun cellItemClick(mString: Int, cellRow: Any) {
        var mCategoryData = cellRow as CategoriesDataResponse
        if (!mCategoryData.isSelected) {
            mCategoryData.isSelected = true
            mSelectedCategoriesResponse.add(mCategoryData)
        } else {
            mCategoryData.isSelected = false
            mSelectedCategoriesResponse.remove(mCategoryData)
        }
        notifyAdapter()
    }

    override fun cellSelectedItemClick(mString: Int, cellRow: Any) {
        var mCategoryData = cellRow as CategoriesDataResponse
        mCategoryData.isSelected = false
        mSelectedCategoriesResponse.remove(mCategoryData)
        notifyAdapter()
    }

    fun notifyAdapter() {
        mSelectedCategoriesAdapter.setElements(mSelectedCategoriesResponse)
        mSelectedCategoriesAdapter.notifyDataSetChanged()
        mCategoriesAdapter.setElements(mCategoriesResponse)
        mCategoriesAdapter.notifyDataSetChanged()
    }

    override fun onLoginResult(message: Int, mesgDesc: String) {
       // hideProgressDialog()

    }

    override fun onDeepLinkingResult() {
        hideProgressDialog()
        moveToNextScreen()
    }

    fun moveToNextScreen() {
        val intent = MainActivity.newMainIntent(this)
        ActivityStack.getInstance(this@SelectCategoriesActivity)
        overridePendingTransition(R.transition.push_left_in, R.transition.push_left_out)
        ActivityStack.cleareAll()
        startActivity(intent)
        finish()
        println("Success")
    }
}
