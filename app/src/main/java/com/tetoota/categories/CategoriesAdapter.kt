package com.tetoota.categories

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.tetoota.R
import kotlinx.android.synthetic.main.activity_profile_detail.*
import kotlinx.android.synthetic.main.select_categories_row.view.*
import org.jetbrains.anko.gray
import org.jetbrains.anko.onClick
import org.jetbrains.anko.textColor
import java.util.*

/**
 * Created by charchit.kasliwal on 03-07-2017.
 */
class CategoriesAdapter(var mCategoriesDataResponse: ArrayList<CategoriesDataResponse>
                        = ArrayList<CategoriesDataResponse>(), val iAdapterClickListener: IAdapterClickListener)
    : RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.select_categories_row, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, p1: Int) {
        viewHolder.bindHomeData(viewHolder, p1, mCategoriesDataResponse[p1], iAdapterClickListener)
    }

    fun setElements(mSelectLanguageList: ArrayList<CategoriesDataResponse>) {
        this.mCategoriesDataResponse = mSelectLanguageList
    }

    private fun getLastPosition() = if (mCategoriesDataResponse.lastIndex == -1) 0 else mCategoriesDataResponse.lastIndex

    override fun getItemCount(): Int {
        return mCategoriesDataResponse.size
    }

    interface IAdapterClickListener {
        fun cellItemClick(mString: Int, cellRow: Any): Unit
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_language_text = view.tv_language_text!!
        val rl_maincontent = view.rl_maincontent!!
       // val ll_maincontent = view.ll_maincontent
        val iv_checkbox = view.iv_checkbox!!
        val iv_icon = view.iv_icon!!

        fun bindHomeData(viewHolder: ViewHolder?, p1: Int, mCategoryData: CategoriesDataResponse, iAdapterClickListener: IAdapterClickListener) {
            tv_language_text.text = mCategoryData.category_name

            if (mCategoryData.category_icon!!.isEmpty()) {
                iv_icon.setImageResource(R.drawable.cat_other_icon)
            } else {

                Picasso.get().load("http://app.tetoota.com/tetoota/assets/images/categories/" + mCategoryData.category_icon).into(iv_icon)

            }

         /*   if (!mCategoryData.isSelected) {
                tv_language_text.setTextColor(Color.parseColor("#595959"));
                iv_icon.setBorderColorResource(R.color.grey_medium)
            } else {
                tv_language_text.setTextColor(Color.parseColor("#e78e6b"));
                iv_icon.setBorderColorResource(R.color.colorPrimaryDark)
            }*/


            if(!mCategoryData.isSelected){
                iv_checkbox.setBackgroundDrawable(itemView.context.resources.getDrawable(R.drawable.iv_un_box_large))
                tv_language_text.setTextColor(Color.parseColor("#4b4b4b"));
            }else{
                iv_checkbox.setBackgroundDrawable(itemView.context.resources.getDrawable(R.drawable.iv_chk_box_large))
                tv_language_text.setTextColor(Color.parseColor("#e78e6b"));
            }

         /*   ll_maincontent.onClick {
                iAdapterClickListener.cellItemClick(p1, mCategoryData)
            }*/

            rl_maincontent.onClick {
                iAdapterClickListener.cellItemClick(p1, mCategoryData)
            }

        }

    }

}