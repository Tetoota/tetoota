package com.tetoota.categories

import android.app.Activity

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class CategoriesContract {
    interface View {
        fun onCategoriesSuccessResult(mCategoriesList: List<CategoriesDataResponse>, message: String)
        fun onCatergoriesFailureResult(message: String)
        fun onLoginResult(message: Int,mesgDesc : String)
        fun onMobileSuccess(message: Int,mesgDesc : String)
        fun onDeepLinkingResult()
    }

    internal interface Presenter {
        fun getCategoriesData(mActivity : Activity)
        fun checkUserLoginFromAPI(mActivity: Activity,number: String, mUserCategories : String,referral_string: String)
        fun mobileRegisterwithverify(mActivity: Activity,number: String,otp: String, mUserCategories : String,referral_string: String)
        fun enterMobile(mActivity: Activity,number: String)
        fun postremindernew(mActivity: Activity,user_id: String, proposal_id: String)
        fun doDeepLinking(mActivity: Activity, authToken : String, user_id : String, user_ip : String)
    }

    interface CategoriesApiListener {
        fun onCategoriesApiSuccess(mDataList: List<CategoriesDataResponse>, message: String)
        fun onCategoriesApiFailure(message: String)
        fun onLoginSuccess(message: Int, mesg : String)
        fun onMobileSuccess(message: Int, mesg : String)
        fun onDeepLinkingResult()
    }
}