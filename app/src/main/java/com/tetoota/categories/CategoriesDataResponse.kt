package com.tetoota.categories

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
data class CategoriesDataResponse(val category_image: String?,val category_icon: String?, val category_id: Int,
                                  val category_name: String, val language: String, var isSelected: Boolean = false) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString().toString(),
            source.readString().toString(),
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(category_image)
        writeString(category_icon)
        writeInt(category_id)
        writeString(category_name)
        writeString(language)
        writeInt((if (isSelected) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CategoriesDataResponse> = object : Parcelable.Creator<CategoriesDataResponse> {
            override fun createFromParcel(source: Parcel): CategoriesDataResponse = CategoriesDataResponse(source)
            override fun newArray(size: Int): Array<CategoriesDataResponse?> = arrayOfNulls(size)
        }
    }
}
