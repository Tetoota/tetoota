package com.tetoota.categories

import android.app.Activity

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class CategoriesPresenter : CategoriesContract.Presenter, CategoriesContract.CategoriesApiListener {

    var mCategoriesView: CategoriesContract.View
    private val mCategoriesInteractor: CategoriesInteractor by lazy {
        com.tetoota.categories.CategoriesInteractor(this)
    }

    constructor(mCategoriesView: CategoriesContract.View) {
        this.mCategoriesView = mCategoriesView
    }

    override fun getCategoriesData(mActivity: Activity) {
        mCategoriesInteractor.getAllCategoriesData(mActivity)
    }

    override fun checkUserLoginFromAPI(mActivity: Activity, number: String, mUserCategories: String, referral_string: String) {
        mCategoriesInteractor.checkUserLoginFromAPI(mActivity, number, mUserCategories, referral_string)
    }


    override fun mobileRegisterwithverify(mActivity: Activity, number: String, otp: String, mUserCategories: String, referral_string: String) {
        mCategoriesInteractor.mobileRegisterwithverify(mActivity, number, otp, mUserCategories, referral_string)
    }


    override fun enterMobile(mActivity: Activity, number: String) {
        mCategoriesInteractor.enterMobile(mActivity, number)
    }


    override fun postremindernew(mActivity: Activity, user_id: String, proposal_id: String) {
        mCategoriesInteractor.postremindernew(mActivity, user_id, proposal_id)
    }



    override fun onMobileSuccess(message: Int, mesg: String) {
        mCategoriesView.onMobileSuccess(message, mesg)
    }


    override fun onCategoriesApiSuccess(mDataList: List<CategoriesDataResponse>, message: String) {
        mCategoriesView.onCategoriesSuccessResult(mDataList, message)
    }

    override fun onCategoriesApiFailure(message: String) {
        mCategoriesView.onCatergoriesFailureResult(message)
    }

    override fun onLoginSuccess(message: Int, mesg: String) {
        mCategoriesView.onLoginResult(message, mesg)
    }

    override fun doDeepLinking(mActivity: Activity, authToken: String, user_id: String, user_ip: String) {
        mCategoriesInteractor.makeDeepLinking(mActivity, authToken, user_id, user_ip)
    }

    override fun onDeepLinkingResult() {
        mCategoriesView.onDeepLinkingResult()
    }
}