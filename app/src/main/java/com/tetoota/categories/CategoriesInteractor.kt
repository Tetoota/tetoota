package com.tetoota.categories

import android.app.Activity
import android.provider.Settings
import android.util.Log
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.login.LoginResponse
import com.tetoota.newaddphoneverify.MobileResponse
import com.tetoota.newaddphoneverify.PreferenceHelper
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class CategoriesInteractor {
    var mCategoriesApiListener: CategoriesContract.CategoriesApiListener
    val API_SUCCESS_VALUE = 101
    val API_FAILURE_VALUE = 202

    constructor(mCategoriesApiListener: CategoriesContract.CategoriesApiListener) {
        this.mCategoriesApiListener = mCategoriesApiListener

    }

    fun getAllCategoriesData(mActivity: Activity): Unit {
        var call: Call<CategoriesResponse> = TetootaApplication.getHeader()
                .getCategoriesList(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity))
        call.enqueue(object : Callback<CategoriesResponse> {
            override fun onFailure(call: Call<CategoriesResponse>?, t: Throwable?) {
                println("Review API RESPONSE Failure ${t?.message.toString()}")
                mCategoriesApiListener.onCategoriesApiFailure(t?.message.toString())
            }

            override fun onResponse(call: Call<CategoriesResponse>?, response: Response<CategoriesResponse>?) {
                var mCategoriesListData: CategoriesResponse? = response?.body()
                println("UserVisibilityResponse Code ${response?.code()}")
                if (response?.code() == 200) {
                    if (mCategoriesListData?.data!!.size > 0) {
                        mCategoriesApiListener.onCategoriesApiSuccess(mCategoriesListData.data, "success")
                    } else {
                        mCategoriesApiListener.onCategoriesApiFailure(response.body()?.meta?.message.toString())
                    }
                } else {
                    mCategoriesApiListener.onCategoriesApiFailure(response?.body()?.meta?.message.toString())
                }
            }
        })
    }

    /**
     * @param mActivity the m activity
     */
    fun checkUserLoginFromAPI(mActivity: Activity, mMobileNumber: String, mUserCategories: String, referral_string: String) {
        var deviceId: String = ""
        if (Utils.getPrefFcmToken(mActivity) != null && Utils.getPrefFcmToken(mActivity).length > 0) {
            deviceId = Utils.getPrefFcmToken(mActivity);
        } else {
            deviceId = "No Found"
        }
        var call: Call<LoginResponse> = TetootaApplication.getHeader()
                .userRegistration(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Constant.DEVICE_TYPE, Settings.Secure.getString(mActivity.contentResolver,
                        Settings.Secure.ANDROID_ID), deviceId,
                        mUserCategories, mMobileNumber, referral_string)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                println("On Failure")
                mCategoriesApiListener.onLoginSuccess(API_SUCCESS_VALUE, t?.message.toString())
            }

            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                var mLoginListData: LoginResponse? = response?.body()
                //onLogin(LoginType.PHONE,mActivity)

                Utils.savePreferences(Constant.USER_AUTH_TOKEN,
                        response!!.body()?.data?.auth_token!!, mActivity)
                Utils.savePreferences(Constant.ALL_PUSH_NOTIFICATION,
                        response.body()?.data?.all_notification.toString(), mActivity)
                Utils.savePreferences(Constant.PROPOSAL_PUSH_NOTIFICATION,
                        response.body()?.data?.proposal_notification.toString(), mActivity)
                Utils.savePreferences(Constant.CHAT_PUSH_NOTIFICATION,
                        response.body()?.data?.chat_notification.toString(), mActivity)
                Utils.savePreferences(Constant.IS_VISIBLE,
                        response.body()?.data?.is_visible.toString(), mActivity)
                val json = Gson().toJson(response.body()?.data)
                Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json, mActivity)
                Utils.savePreferences(Constant.IS_USER_LOGGED_IN, "true", mActivity)
                Utils.savePreferences(Constant.USER_ID, response.body()?.data?.user_id!!, mActivity)
                mCategoriesApiListener.onLoginSuccess(API_SUCCESS_VALUE,
                        mLoginListData?.meta?.message.toString())
            }
        })
    }


    fun mobileRegisterwithverify(mActivity: Activity, mMobileNumber: String, mOtp: String, mUserCategories: String, referral_string: String) {
        var deviceId: String = ""
        if (Utils.getPrefFcmToken(mActivity) != null && Utils.getPrefFcmToken(mActivity).length > 0) {
            deviceId = Utils.getPrefFcmToken(mActivity);
        } else {
            deviceId = "No Found"
        }
        var call: Call<LoginResponse> = TetootaApplication.getHeader()
                .userRegistrationMobile(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Constant.DEVICE_TYPE, Settings.Secure.getString(mActivity.contentResolver,
                        Settings.Secure.ANDROID_ID), deviceId,
                        mUserCategories, mMobileNumber, mOtp, referral_string)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                println("On Failure"+t?.message.toString())
                mCategoriesApiListener.onLoginSuccess(API_SUCCESS_VALUE, t?.message.toString())
            }

            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                var mLoginListData: LoginResponse? = response?.body()
                //onLogin(LoginType.PHONE,mActivity)
                println("onResponse"+response.toString())
                Utils.savePreferences(Constant.USER_AUTH_TOKEN,
                        response!!.body()?.data?.auth_token!!, mActivity)
                Utils.savePreferences(Constant.ALL_PUSH_NOTIFICATION,
                        response.body()?.data?.all_notification.toString(), mActivity)
                Utils.savePreferences(Constant.PROPOSAL_PUSH_NOTIFICATION,
                        response.body()?.data?.proposal_notification.toString(), mActivity)
                Utils.savePreferences(Constant.CHAT_PUSH_NOTIFICATION,
                        response.body()?.data?.chat_notification.toString(), mActivity)
                Utils.savePreferences(Constant.IS_VISIBLE,
                        response.body()?.data?.is_visible.toString(), mActivity)
                val json = Gson().toJson(response.body()?.data)
                Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json, mActivity)
                Utils.savePreferences(Constant.IS_USER_LOGGED_IN, "true", mActivity)
                Utils.savePreferences(Constant.USER_ID, response.body()?.data?.user_id!!, mActivity)
                mCategoriesApiListener.onLoginSuccess(API_SUCCESS_VALUE,
                        mLoginListData?.meta?.message.toString())
            }
        })
    }


    fun enterMobile(mActivity: Activity, mMobileNumber: String) {
        var deviceId: String = ""
        if (Utils.getPrefFcmToken(mActivity) != null && Utils.getPrefFcmToken(mActivity).length > 0) {
            deviceId = Utils.getPrefFcmToken(mActivity);
        } else {
            deviceId = "No Found"
        }
        var call: Call<ResponseBody> = TetootaApplication.getHeader()
                .userEnterMobile(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Constant.DEVICE_TYPE, Settings.Secure.getString(mActivity.contentResolver,
                        Settings.Secure.ANDROID_ID), deviceId, mMobileNumber)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                println("On Failure")
                mCategoriesApiListener.onMobileSuccess(API_SUCCESS_VALUE, t?.message.toString())
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
//                Log.e("Hi","response.body()?.data?.phone_number!! "+ response?.body()?.string())
//                var mLoginListData: MobileResponse? = response?.body()
//                val json = Gson().toJson( response?.body()?.string())
                var json = JSONObject(response?.body()?.string())
                //  Utils.savePreferences(Constant.LOGGED_IN_USER_DATA, json, mActivity)
                // Utils.savePreferences(Constant.IS_USER_LOGGED_IN, "true", mActivity)
//                Log.e("hhhhhhh","response.body()?.data?.phone_number!! "+ response.body()?.data?.phone_number!!)
                Utils.savePreferences(Constant.MOBILE_NUMBER, json.getJSONObject("data").getString("phone_number"), mActivity)
                mCategoriesApiListener.onMobileSuccess(API_SUCCESS_VALUE,
                       "OTP Send Successfully")
            }
        })
    }


/*
    fun enterMobile(mActivity: Activity, mMobileNumber: String) {
        var deviceId: String = ""
        if (Utils.getPrefFcmToken(mActivity) != null && Utils.getPrefFcmToken(mActivity).length > 0) {
            deviceId = Utils.getPrefFcmToken(mActivity);
        } else {
            deviceId = "No Found"
        }
        var call: Call<ResponseBody> = TetootaApplication.getHeader()
                .userEnterMobile(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Constant.DEVICE_TYPE, Settings.Secure.getString(mActivity.contentResolver,
                        Settings.Secure.ANDROID_ID), deviceId, mMobileNumber)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                println("On Failure")
                mCategoriesApiListener.onMobileSuccess(API_SUCCESS_VALUE, t?.message.toString())
            }
            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                var resp = response?.body()

               // var jsonObj = JSONObject(resp)
                var js = resp.getJSONObject("data")
                var mobile = js.getString("phone_number")
                PreferenceHelper.PREF_NAME = mobile

            }
        })
    }
*/

    fun postremindernew(mActivity: Activity, user_id: String?, proposal_id: String) {
        var deviceId: String = ""
        if (Utils.getPrefFcmToken(mActivity) != null && Utils.getPrefFcmToken(mActivity).length > 0) {
            deviceId = Utils.getPrefFcmToken(mActivity);
        } else {
            deviceId = "No Found"
        }
        var call: Call<ResponseBody> = TetootaApplication.getHeader()
                .postremindernew(Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD,
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Constant.DEVICE_TYPE, Settings.Secure.getString(mActivity.contentResolver,
                        Settings.Secure.ANDROID_ID), deviceId, user_id!!, proposal_id)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                println("On Failure")
                mCategoriesApiListener.onMobileSuccess(API_SUCCESS_VALUE, t?.message.toString())
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                var json = JSONObject(response?.body()?.string())
                Utils.savePreferences(Constant.MOBILE_NUMBER, json.getJSONObject("data").getString("phone_number"), mActivity)
                mCategoriesApiListener.onMobileSuccess(API_SUCCESS_VALUE,
                        "OTP Send Successfully")
            }
        })
    }


    fun makeDeepLinking(mActivity: Activity, authToken: String, user_id: String, user_ip: String) {
        var call: Call<DeepLinkingResponse> = TetootaApplication.getHeader()
                .doDeepLinking(Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity)
                        , user_id, user_ip)
        call.enqueue(object : Callback<DeepLinkingResponse> {
            override fun onFailure(call: Call<DeepLinkingResponse>?, t: Throwable?) {
                println("On Failure")
                mCategoriesApiListener.onDeepLinkingResult()
            }

            override fun onResponse(call: Call<DeepLinkingResponse>?, response: Response<DeepLinkingResponse>?) {
                mCategoriesApiListener.onDeepLinkingResult()
            }
        })
    }
}