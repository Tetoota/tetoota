package com.tetoota.categories

import com.google.gson.annotations.SerializedName
import com.tetoota.network.errorModel.Meta

data class DeepLinkingResponse(

	@field:SerializedName("data")
	val data: List<Any?>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)