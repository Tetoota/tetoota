package com.tetoota.fragment.scanandpay

import android.app.Activity

/**
 * Created by jitendra.nandiya on 15-11-2017.
 */
class PaymentContract {
    interface View {
        fun onPaymentSuccess(mString: String, mFavoriteList: String): Unit
        fun onPaymentFailureResponse(mString: String, isServerError: Boolean): Unit
    }

    interface Presenter {
        fun getPayment(mActivity: Activity, user_id : String, receiver_id : String, points : String)
    }

    interface PaymentApiResult {
        fun onPaymentApiSuccess(mString: String, mReferralCode: String): Unit
        fun onPaymentApiFailure(mString: String, isServerError: Boolean): Unit
    }
}