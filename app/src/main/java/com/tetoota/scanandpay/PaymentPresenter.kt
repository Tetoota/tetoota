package com.tetoota.fragment.scanandpay

import android.app.Activity

class PaymentPresenter : PaymentContract.Presenter, PaymentContract.PaymentApiResult {
    var mFavoriteView: PaymentContract.View

    val mFavoriteInteractor: PaymentInteractor by lazy {
        PaymentInteractor(this)
    }

    constructor(mFavoriteView: PaymentContract.View) {
        this.mFavoriteView = mFavoriteView
    }

    override fun getPayment(mActivity: Activity, user_id: String, receiver_id: String, points: String) {
        mFavoriteInteractor.getPayments(mActivity, user_id, receiver_id, points)
    }

    override fun onPaymentApiSuccess(mString: String, mReferralCode: String) {
        mFavoriteView.onPaymentSuccess(mString, mReferralCode)
    }

    override fun onPaymentApiFailure(mString: String, isServerError: Boolean) {
        mFavoriteView.onPaymentFailureResponse(mString, isServerError)
    }
}