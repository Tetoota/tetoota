package com.tetoota.scanandpay

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.tetoota.R
import com.tetoota.customviews.ThankYouDialog
import com.tetoota.fragment.scanandpay.PaymentContract
import com.tetoota.fragment.scanandpay.PaymentPresenter
import com.tetoota.login.LoginDataResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.StringConstant
import com.tetoota.utility.Utils
import kotlinx.android.synthetic.main.activity_scanned.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.jetbrains.anko.toast


class ScannedActivity : AppCompatActivity(), PaymentContract.View {
    override fun onPaymentSuccess(mString: String, mFavoriteList: String) {
        ThankYouDialog(this@ScannedActivity, Constant.DIALOG_LOGIN_FAILURE_ALERT, "cancel").show()
    }
//-------------------------------------------------------------------------------------------------//
    override fun onPaymentFailureResponse(mString: String, isServerError: Boolean) {
        toast(mString)
    }
//-------------------------------------------------------------------------------------------------//
    private val mProfileDetailPresenter: PaymentPresenter by lazy {
        PaymentPresenter(this@ScannedActivity)
    }
//-------------------------------------------------------------------------------------------------//
    companion object {
        private const val SCANNED_STRING: String = "scanned_string"
        fun getScannedActivity(callingClassContext: Context,
                               encryptedString: String): Intent {
            return Intent(callingClassContext, ScannedActivity::class.java)
                    .putExtra(SCANNED_STRING, encryptedString)
        }
    }
//-------------------------------------------------------------------------------------------------//
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanned)
        if (intent.getStringExtra(SCANNED_STRING) == null)
            throw RuntimeException("No encrypted String found in intent")
        Log.e("SCANNED_STRING", intent.getStringExtra(SCANNED_STRING));

        var mStr: String = intent.getStringExtra(SCANNED_STRING)
        var recever_details: List<String> = mStr.split(",").map { it.trim() }
        initToolbar()
//        val decryptedString = EncryptionHelper.getInstance().getDecryptionString(intent.getStringExtra(SCANNED_STRING))
//        val userObject = Gson().fromJson(decryptedString, UserObject::class.java)
//        scannedFullNameTextView.text = userObject.fullName
//        scannedAgeTextView.text = userObject.age.toString()
        //Log.i(javaClass.name, "==================  " + recever_details.size)
        if(recever_details.size<2)
        {
            onBackPressed()
            Toast.makeText(this, "Please Input a Valid QR", Toast.LENGTH_SHORT).show();
        }
       /* else
        {
        }*/
        tv_vendor_name.setText(recever_details.get(recever_details.size-1))
        save_next.text = Utils.getText(this, StringConstant.str_pay);

        save_next.setOnClickListener(View.OnClickListener {
            if (edt_points.text.toString().trim().length > 0) {
                val json = Utils.loadPrefrence(Constant.LOGGED_IN_USER_DATA, "", this)
                val personData = Gson().fromJson(json, LoginDataResponse::class.java)
                mProfileDetailPresenter.getPayment(this, personData.user_id!!, recever_details.get(1), edt_points.text.toString().trim())

            } else {
                edt_points.setError("Please enter tetoota points")
            }
        })
    }
//-------------------------------------------------------------------------------------------------//
    private fun initToolbar() {
        setSupportActionBar(toolbar)
        toolbar_title.text = Utils.getText(this, StringConstant.str_scan_pay)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar.setNavigationOnClickListener(View.OnClickListener {
            // perform whatever you want on back arrow click
            onBackPressed()
        })
    }
//-------------------------------------------------------------------------------------------------//
}
