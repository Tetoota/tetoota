package com.tetoota.fragment.scanandpay

import android.app.Activity
import com.google.gson.Gson
import com.tetoota.TetootaApplication
import com.tetoota.fragment.couponcode.coupon_code.Meta
import com.tetoota.fragment.couponcode.coupon_code.PaymentResponse
import com.tetoota.utility.Constant
import com.tetoota.utility.Utils
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


class PaymentInteractor {
    var call: Call<PaymentResponse>? = null
    var mInviteFriendsListener: PaymentContract.PaymentApiResult

    constructor(mFavoriteListener: PaymentContract.PaymentApiResult) {
        this.mInviteFriendsListener = mFavoriteListener
    }

    fun getPayments(mActivity: Activity, user_id: String, receiver_id: String, points: String) {
        call = TetootaApplication.getHeader()
                .payment(
                        Utils.loadPrefrence(Constant.USER_SELECTED_LANG, "en", mActivity),
                        Utils.loadPrefrence(Constant.USER_AUTH_TOKEN, "", mActivity),
                        user_id, receiver_id, points)
        call!!.enqueue(object : Callback<PaymentResponse> {
            override fun onResponse(call: Call<PaymentResponse>?,
                                    response: Response<PaymentResponse>?) {
                var mPaymentData: PaymentResponse? = response?.body()
                if (response?.code() == 200) {
                    if (response.body()?.meta?.status!!) {
                        if (mPaymentData != null) {
                            mInviteFriendsListener.onPaymentApiSuccess("success", mPaymentData.meta!!.message as String)
                        }
                    } else {
                        mInviteFriendsListener.onPaymentApiFailure(response?.body()?.meta?.message.toString(), false)
                    }
                } else {
                    if (response?.body() == null) {
                        try {
                            val jObjError = JSONObject(response?.errorBody()?.string())
                            val achualdata: JSONObject = jObjError.getJSONObject("meta")
                            val gson = Gson()
                            var mError: Meta = Meta()
                            val error = gson.fromJson(achualdata.toString(), Meta::class.java)
                            mInviteFriendsListener.onPaymentApiFailure(error.message!!, true)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        /* var mPaymentData : ResponseBody? = response?.errorBody()
                         val gson = GsonBuilder().create()
                         var mError = Meta()
                         try {
                           var  mError: String? = mPaymentData.
                             Log.d("nik",mError.toString());
                         } catch (e: IOException) {
                             // handle failure to read error
                             e.printStackTrace();
                         }*/
                    }
                }
            }

            override fun onFailure(call: Call<PaymentResponse>?, t: Throwable?) {
                mInviteFriendsListener.onPaymentApiFailure(t?.message.toString(), true)
            }
        })
    }
}