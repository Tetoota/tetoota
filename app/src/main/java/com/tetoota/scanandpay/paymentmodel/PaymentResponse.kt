package com.tetoota.fragment.couponcode.coupon_code

import com.google.gson.annotations.SerializedName

data class PaymentResponse(

	@field:SerializedName("data")
	val data: Any? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null
)