package com.tetoota

import android.content.Context
import android.content.pm.PackageManager
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.util.Base64
import android.util.Log
import com.tetoota.fragment.home.model.DataItem
import com.tetoota.listener.ApiInterface
import com.tetoota.network.RetryableCallback
import com.tetoota.network.ServiceGenerator
import com.tetoota.utility.Constant
import io.intercom.android.sdk.Intercom
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

/**
 * Created by charchit.kasliwal on 29-06-2017.
 */
class TetootaApplication : MultiDexApplication() {

    // public val AF_DEV_KEY = "GcxBkANbsbVrhrp8AxXXWb"
    public var myLatitude: Double = 0.toDouble()
    public var myLongitude: Double = 0.toDouble()
    public var isCheck: String = ""
    public var isCheckFragment: String = ""
    public var userMacAddress: String = ""
    public var isFirstTime: String = ""
    public var count: Int = 0
    public var userid: String = ""
    public var tetootapoint: String = ""

    public var mDataList: List<DataItem>? = null


    override fun onCreate() {
        Intercom.initialize(this, Constant.INTERCOM_APIKEY, Constant.INTERCOM_APIID)
        Intercom.client().registerUnidentifiedUser()
        MultiDex.install(this)

        //  AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate()
    }

    companion object TetootaApplication {
        val DEFAULT_RETRIES = 3
        private var apiInterface: ApiInterface? = null
        private var apiGoogleInterface: ApiInterface? = null
        private var apiGoogleTranslateInterface: ApiInterface? = null
        private var activityVisible: Boolean = false
        private var converstationId: Int = 0
        private var textValuesHashmap: HashMap<String, String>? = HashMap()

        fun getTextValuesHashMap(): HashMap<String, String>? {
            return textValuesHashmap
        }

        fun setTextValuesHashMap(hashmap: HashMap<String, String>?) {
            textValuesHashmap = hashmap
        }

        fun getHeader(): ApiInterface {
            ServiceGenerator.changeApiBaseUrl(true)
            if (apiInterface == null) {
                // create upload service client
                apiInterface = ServiceGenerator.createService(ApiInterface::class.java,
                        Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD)
                return apiInterface as ApiInterface
            } else {
                return apiInterface as ApiInterface
            }
        }

        fun getHeaderGoogleTranslate(): ApiInterface {
            ServiceGenerator.changeApiBaseUrl(false)
            if (apiGoogleTranslateInterface == null) {
                // create upload service client
                apiGoogleTranslateInterface = ServiceGenerator.createServiceForGoogleTranslate(ApiInterface::class.java)
                return apiGoogleTranslateInterface as ApiInterface
            } else {
                return apiGoogleTranslateInterface as ApiInterface
            }
        }

        /* fun getHeaderForGooglePlaces(): ApiInterface {
             ServiceGenerator.changeApiBaseUrl(false)
 //            if (apiInterface == null) {
             apiGoogleInterface = ServiceGenerator.createService(ApiInterface::class.java,
                     Constant.CONSTANT_ADMIN, Constant.CONSTACT_PWD)
             return apiGoogleInterface as ApiInterface
 //            } else {
 //                return apiInterface as ApiInterface
 //            }
         }*/

        fun isActivityVisible(): Boolean {
            return activityVisible
        }

        fun activityResumed() {
            activityVisible = true
        }

        fun activityPaused() {
            activityVisible = false
        }

        fun setConversationId(conversationId: Int) {
            this.converstationId = converstationId
        }

        fun getConversationId(): Int {
            return converstationId
        }

        fun <T> enqueueWithRetry(call: Call<T>, retryCount: Int, callback: Callback<T>) {
            call.enqueue(object : RetryableCallback<T>(call, retryCount) {

                override fun onFinalResponse(call: Call<T>, response: Response<T>) {
                    callback.onResponse(call, response)
                }

                override fun onFinalFailure(call: Call<T>, t: Throwable) {
                    callback.onFailure(call, t)
                }
            })
        }

        fun <T> enqueueWithRetry(call: Call<T>, callback: Callback<T>) {
            enqueueWithRetry(call, DEFAULT_RETRIES, callback)
        }

        fun isCallSuccess(response: Response<*>): Boolean {
            val code = response.code()
            return code >= 200 && code < 400
        }
    }

    /**
     * Method that prints hash key.
     */

/*
    fun printHashKey() {
        try {
            val info = packageManager.getPackageInfo(
                    "com.tutorialwing.androidfacebookshareimagetutorial",
                    PackageManager.GET_SIGNATURES)

            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }
    }
*/


    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
        MultiDex.install(this)
    }

//    /**
//     * Gets the default [Tracker] for this [Application].
//     * @return tracker
//     */
//    @Synchronized
//    fun getDefaultTracker(): Tracker {
//        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
//        if (sTracker == null) {
//            sTracker = sAnalytics?.newTracker(R.xml.global_tracker)
//        }
//        return sTracker!!
//    }


}